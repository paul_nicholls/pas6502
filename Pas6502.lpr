program Pas6502;

{$MODE Delphi}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

//{$apptype console}

uses
  {Vcl.Forms,}
  form_main in 'units\form_main.pas' {Pas6502_Form},
  uParser in 'units\uParser.pas',
  uparserpas6502 in 'units\uparserpas6502.pas',
  uExpressions in 'units\uExpressions.pas',
  form_options in 'units\form_options.pas' {OKRightDlg},
  uCodeInsight in 'units\uCodeInsight.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TPas6502_Form, Pas6502_Form);
  Application.CreateForm(TOptionsDialog, OptionsDialog);
  Application.Run;
end.
