@echo off
rem set mkimage="utilities\mkimg.exe"
rem set srcprogname="bbc_example.asm"
rem set kickassemfile="C:\utilities\KickAssembler\KickAss.jar"
rem set progname=BBCTEST
rem set progaddr=1D00

@echo on
del myfiles\%progname%.ssd
del myfiles\!BOOT
del myfiles\%progname%.inf
del myfiles\filelist.txt

java.exe -jar "%kickassemfile%" "%srcprogname%" -o "myfiles\%progname%" -binfile

(
echo *BASIC
echo *RUN %progname%
)> myfiles\!BOOT

(
echo %progname% FFFF%progaddr% FFFF%progaddr%
)> myfiles\%progname%.inf

(
echo !BOOT
echo %progname%
)> myfiles\filelist.txt

"%mkimage%" -fs DFS -size 200K %progname%.ssd myfiles -opt 3 -i@myfiles\filelist.txt -title "%progname%" -pad
