@echo off
rem set applecommander="C:\programming\c64\pas6502\utilities\apple\AppleCommander-ac-1.5.0.jar"
rem set srcprogname="hello.asm"
rem set kickassemfile="C:\programming\c64\pas6502\utilities\KickAssembler\KickAss.jar"
rem set progname=HELLO

del *.bin
del *.dsk
del *.sym

@echo on
java.exe -jar "%kickassemfile%" "%srcprogname%" -o "%progname%.bin" -binfile

copy "C:\programming\c64\pas6502\utilities\apple\loader.txt" loader.bas
copy "C:\programming\c64\pas6502\utilities\apple\apple.dsk" %progname%.dsk

%enableboot%type loader.bas | java -jar %applecommander% -bas %progname%.dsk HELLO
type %progname%.bin | java -jar %applecommander% -p %progname%.dsk TEST B %origin%

