program stackTest;

function fib(n : Word) : Word; cdecl;
begin
  if n < 2 then
    Result := n
  else begin
    Result := fib(n - 1);
    Result := Result + fib(n - 2);
  end;
end;

var
  n : Byte;
begin
  n := fib(4);
  
  WriteLn(n);
end.