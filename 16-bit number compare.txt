;16-bit number comparison...
;
         lda #>x               ;MSB of 1st number
         cmp #>y               ;MSB of 2nd number
         bcc islower           ;X < Y
;
         bne ishigher          ;X > Y
;
         lda #<x               ;LSB of 1st number
         cmp #<y               ;LSB of 2nd number
         bcc islower           ;X < Y
;
         beq issame            ;X = Y
;
         bne ishigher          ;X > Y
         
  lda x+1
  cmp y+1
  bne !+
  lda x+0
  cmp y+0
!: 
  bcc lower
  bne higher
  beq same

         
procedure test; assembler;
asm
  lda #>x               //MSB of 1st number
  cmp #>y               //MSB of 2nd number
  bne check1           //X < Y

  lda #<x               //LSB of 1st number
  cmp #<y               //LSB of 2nd number
  beq isEqual            //X = Y
 
check1:
  bcc isLower
  
isHigher:               //X > Y
  lda #'>'
  sta 1024
  rts

isLower:
  lda #'<'
  sta 1024
  rts
  
isEqual:
  lda #'='
  sta 1024
  rts
end;

procedure test2; assembler;
asm
  lda #>x
  cmp #>y
  bne ahead
  lda #<x
  cmp #<y
ahead:
  bcc isLower
  beq isSame
  bne isHigher
  rts
  
isLower:
  lda #'<'
  sta 1024
  rts
isHigher:
  lda #'>'
  sta 1024
  rts
isSame:
  lda #'='
  sta 1024
  rts
end;

procedure isLess; assembler;
asm
  lda #>x
  cmp #>y
  bne !ahead+
  lda #<x
  cmp #<y
!ahead:
  bcc !isTrue+
  lda #$00
  jmp !+
  
!isTrue:
  lda #$ff
  jmp !+
!:
end;

procedure notEqual; assembler;
asm
  lda #>x
  cmp #>y
  bne !isTrue+
  lda #<x
  cmp #<y
  bne !isTrue+
!isFalse:
  lda #$00
  rts
!isTrue:
  lda #$ff
end;

procedure equal; assembler;
asm
  lda #>x
  cmp #>y
  bne !isFalse+
  lda #<x
  cmp #<y
  bne !isFalse+
!isTrue:
  lda #$ff
  rts
!isFalse:
  lda #$00
  rts
!check:
end;

