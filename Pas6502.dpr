program Pas6502;

{$IFDEF FPC}
  {$MODE Delphi}
{$ENDIF}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------
{$apptype console}
uses
  {$IFnDEF FPC}
  System.Classes,
  Vcl.Forms,
  {$ELSE}
  {$ENDIF }
  form_main in 'units\form_main.pas' {Pas6502_Form},
  uParser in 'units\uParser.pas',
  form_options in 'units\form_options.pas' {OKRightDlg},
  uCodeInsight in 'units\uCodeInsight.pas',
  uAST in 'units\uAST.pas',
  uLexer in 'units\uLexer.pas',
  uToken in 'units\uToken.pas',
  uParserAST in 'units\uParserAST.pas',
  uASTto6502 in 'units\uASTto6502.pas',
  uSymbolTable in 'units\uSymbolTable.pas',
  usesStringWriter in 'units\usesStringWriter.pas',
  uASM in 'units\uASM.pas',
  uThreeAddressCode in 'units\uThreeAddressCode.pas',
  uCodeGen_6502 in 'units\uCodeGen_6502.pas',
  form_gotoLine in 'units\form_gotoLine.pas' {GotoLineDialog},
  form_help in 'units\form_help.pas' {help_Form},
  uFragments in 'units\uFragments.pas',
  uConstPropagation in 'units\uConstPropagation.pas',
  uPeepholeOptimiser in 'units\uPeepholeOptimiser.pas',
  form_codeInsightOptions in 'units\form_codeInsightOptions.pas' {codeInsightOptions},
  uTargets in 'units\uTargets.pas',
  frame_findReplace in 'units\frame_findReplace.pas' {frm_findReplace: TFrame},
  form_findReplace in 'units\form_findReplace.pas' {findReplaceDialog},
  form_ideThemes in 'units\form_ideThemes.pas' {ideThemes_Dialog};

{$R *.res}
begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TPas6502_Form, Pas6502_Form);
  Application.CreateForm(TOptionsDialog, OptionsDialog);
  Application.CreateForm(Thelp_Form, help_Form);
  Application.CreateForm(TfindReplaceDialog, findReplaceDialog);
  Application.Run;
end.
