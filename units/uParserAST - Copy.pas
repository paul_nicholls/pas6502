unit uParserAST;

{$ifdef fpc}
{$MODE Delphi}
{$endif}
// ------------------------------------------------------------------------
// This file is part of Pas6502.
//
// You can find Pas6502 here:
// https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
// Pas6502 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pas6502 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
// ------------------------------------------------------------------------

interface

uses
  System.SysUtils, System.Classes, uLexer, uToken, uAST, uSymbolTable;

const
  MIN_POINTER_ADDRESS = $02;
  MAX_POINTER_ADDRESS = $7F;

type
  TParseException = class(Exception);

  TParserAST = class
  private
    Flines: TStringList;
    Flexer: TLexer;
    Ftoken: TToken;
    FerrorMsg: AnsiString;
    FprogAST: TProgram;
    FpointerAddress: Integer;

    FcurrentScope: TscopedSymbolTable;
    FparsingUnit: Boolean;
    FprojectPath: AnsiString;
    FloopLevel: Integer;

    procedure error(aMsg: AnsiString);
    function accept(S: TTokenType): Boolean;
    function expect(S: TTokenType): Boolean;
    procedure nextToken;

    function factor: TAST;
    function signedFactor: TAST;
    function term: TAST;
    function simpleExpression: TAST;
    function expression: TAST;

    function asmCompound: TAsmCompound;
    function compound: TCompound;

    procedure expandRecordVarToSymbolTable(aVarDecl: TVarDecl);

    function formalParams(var paramCount: Integer): TParamArray;
    procedure procDecl(aBlock: TBlock);
    procedure funcDecl(aBlock: TBlock);
    procedure macroDecl(aBlock: TBlock);
    function getPointerAddress: Byte;
    procedure doTypedConstDecl_array(aConstName: AnsiString; aBlock: TBlock);
    procedure constDecls(aBlock: TBlock);
    procedure typeDecls(aBlock: TBlock);
    procedure varDecls(aBlock: TBlock);

    procedure unitDecls(aProg: TProgram; aUnitName: AnsiString);
    function block(aBlockName: AnsiString; aRetType: TTokenType;
      aIsFunc: Boolean; aIsMacro: Boolean): TBlock;
    function asmBlock(aBlockName: AnsiString; aIsAbsoluteAsmProc: Boolean;
      aRetType: TTokenType; aIsFunc: Boolean; aIsMacro: Boolean): TAsmBlock;
    function doSetupRasterIRQ: TAST;
    function doEndIRQ: TAST;
    function statement: TAST;
    function doPoke: TPoke;
    function doIf: TIfThen;
    function doWhile: TWhile;
    function doFor: TFor;
    function doBuiltInProc(aType: TTokenType; aName: AnsiString)
      : TBuiltinProcedure;
    function doAssign(aVariable: TVariable): TAST;
    function doDecInc(decIncToken : TToken) : TAST;
    function doCall(aProcName: AnsiString): TAST;
    function doFuncCall(aFuncName: AnsiString): TAST;
    function doMacroCall(aMacroName: AnsiString): TAST;

    procedure doUnit;
    procedure loadUnit(aProg: TProgram; aUnitName: AnsiString);
    procedure parseUsesClause(aProg: TProgram);
    procedure extractMacroNamesFromSource(aAsmFileName: AnsiString);
    procedure parseImport(aProg: TProgram; aBlock: TBlock);
    procedure registerImportFile(aProg: TProgram; aFileName: AnsiString);
    procedure parseImportBinary(aConstName: AnsiString);
    function doProgram: TProgram;
    procedure registerC64Stuff;
  public
    constructor Create;
    destructor Destroy;

    function parse(aStream: TStream; aParsingUnit: Boolean): Boolean; overload;
    function parse(aFileName: AnsiString; aParsingUnit: Boolean)
      : Boolean; overload;
    function parseUnit(aProgram: TProgram; aScope: TscopedSymbolTable;
      aFileName: AnsiString): Boolean;
    function parseAsmFile(aProgram: TProgram; aScope: TscopedSymbolTable;
      aFileName: AnsiString): Boolean;

    property errorMsg: AnsiString read FerrorMsg;

    property progAST: TProgram read FprogAST;
    property projectPath: AnsiString read FprojectPath write FprojectPath;
    property pointerAddress: Integer read FpointerAddress write FpointerAddress;
  end;

implementation

uses
  System.IOUtils, uASM;

const
  BUILT_IN_PROCS = [ttWriteLn, ttWrite];

  TARGET_TYPE = [ttC64, ttBBC];

  MACHINE_ACORN_TYPES = [ttBBC];

  PROCS_C64 = [ttWriteLn, ttWrite];

  PROCS_BBC = [ttWriteLn, ttWrite];

constructor TParserAST.Create;
begin
  Flines := TStringList.Create;

  Flexer := TLexer.Create;

  FcurrentScope := nil;
  FprogAST := nil;

  FloopLevel := 0;

  FpointerAddress := MIN_POINTER_ADDRESS;
end;

destructor TParserAST.Destroy;
begin
  Flines.Free;
  Flexer.Free;
end;

procedure TParserAST.error(aMsg: AnsiString);
begin
  raise TParseException.Create(aMsg + Format(' at line: %d/ col: %d',
    [Flexer.tokenRow, Flexer.tokenCol - 1]));
end;

function TParserAST.accept(S: TTokenType): Boolean;
begin
  Result := False;

  if (Ftoken.tType = S) then
  begin
    Ftoken := Flexer.getNextToken;

    Result := True;
  end
end;

function TParserAST.expect(S: TTokenType): Boolean;
begin
  Result := accept(S);

  if not Result then
    error('Unexpected symbol: found "' + Ftoken.tValue + '"');
end;

procedure TParserAST.nextToken;
begin
  Ftoken := Flexer.getNextToken;
end;

function TParserAST.factor: TAST;
var
  unaryToken: TToken;
  token: TToken;
  node: TAST;
  isUnary: Boolean;
  isPointer: Boolean;
  sym: TSymbol;
  funcSym: TFuncSymbol;
  intSize: Integer;
  e: TAST;
  lengthEnabled: Boolean;
begin
  isUnary := False;
  isPointer := False;

  if (Ftoken.tType = ttAt) then
  begin
    isPointer := True;
    nextToken;
  end;

  unaryToken := Ftoken;

  if (unaryToken.tType in [ttPlus, ttMinus]) then
  begin
    if isPointer then
      error('Factor: Incompatible with @ operation');

    isUnary := (unaryToken.tType = ttMinus);

    nextToken;
  end;

  token := Ftoken;

  if accept(ttLength) then
  begin
    if isPointer then
      error('Factor: Incompatible with @ operation');

    expect(ttLParen);

    lengthEnabled := False;

    token := Ftoken;

    e := simplify(simpleExpression);

    expect(ttRParen);

    if not e.isVariable then
      error('length() only works on a variable');

    sym := FcurrentScope.lookup(TVariable(e).token.tValue);

    if (sym is TConstSymbol) then
    begin
      if sym.symType <> nil then
      begin
        lengthEnabled := (sym.symType.symName = 'string') or
          (sym.symType.symName = 'ascii') or (sym.symType.symName = 'petscii');
      end;
      lengthEnabled := lengthEnabled or (TConstSymbol(sym).isArray);
    end
    else if (sym is TVarSymbol) then
    begin
      if sym.symType <> nil then
      begin
        lengthEnabled := (sym.symType.symName = 'string') or
          (sym.symType.symName = 'ascii') or (sym.symType.symName = 'ascii');
      end;
      // lengthEnabled := lengthEnabled or (TVarSymbol(sym).isArray);
    end;

    if not lengthEnabled then
      error('length(): incompatable type "' + sym.symType.symName + '" found');

    if TConstSymbol(sym).isArray then
      // is array so return actual array length
      node := TNumber.Create(TConstSymbol(sym).len)
    else
      // return the base string variable so location e[0] will be read
      node := TVariable.Create(token, 1);
  end
  else if accept(ttTrue) then
  begin
    if isPointer then
      error('Factor: Immediate value incompatible with @ operation');

    node := TNumber.CreateBool(True);
  end
  else if accept(ttFalse) then
  begin
    if isPointer then
      error('Factor: Immediate value incompatible with @ operation');

    node := TNumber.CreateBool(False);
  end
  else if accept(ttString) then
  begin
    if isPointer then
      error('Factor: String incompatible with @ operation');

    node := TString.Create(token);
  end
  else if accept(ttIdent) then
  begin
    while accept(ttPeriod) do
    begin
      token.tValue := token.tValue + '.' + Ftoken.tValue;
      expect(ttIdent);
    end;

    sym := FcurrentScope.lookup(Ftoken.tValue);

    if LowerCase(token.tValue) = 'result' then
      token.tValue := LowerCase(token.tValue);

    sym := FcurrentScope.lookup(token.tValue);

    if (sym = nil) then
      error('Factor: Undeclared identifier "' + token.tValue + '"');

//    if TVarSymbol(sym).isFuncResult then
//    begin
//      // is a function result so process accordingly
//      // return a
//
//      node := TVariable.Create('result', TVarSymbol(sym).size);
//      TVariable(node).isFuncResult := True;
//
//      Result := node;
//      Exit;
//    end;

    if (sym.symClass = scFunc) then
    begin
      if isPointer then
        error('Factor: Function incompatible with @ operation');

      // is a function call
      node := doFuncCall(sym.symName);
    end
    else
    if (sym.symClass = scConst) then
    begin
      if (TConstSymbol(sym).symType = nil) then
      begin
        // is normal const, so substitute const value
        case TConstSymbol(sym).value.tType of
          ttIntNum:
            node := TNumber.Create(TConstSymbol(sym).value);
          ttString:
            node := TString.Create(TConstSymbol(sym).value);
        end;
      end
      else
      begin
        // is typed const, treat as normal variable or pointer/array
        node := TVariable.Create(token, sym.size);

        if accept(ttLBracket) then
        begin
          if isPointer then
            error('Factor: Array incompatible with @ operation');

          TVariable(node).arrayParam := simplify(simpleExpression);
          expect(ttRBracket);
        end;
      end;
    end
    else
    begin
      // normal variable or pointer/array
      node := TVariable.Create(token, sym.size);

      if accept(ttLBracket) then
      begin
        if isPointer then
          error('Factor: Array incompatible with @ operation');

        TVariable(node).arrayParam := simplify(simpleExpression);
        expect(ttRBracket);
      end
      else if accept(ttCaret) then
      begin
        if isPointer then
          error('Factor: Pointer incompatible with @ operation');

        if LowerCase(sym.symType.symName) <> 'pointer' then
          error('Can only deref a pointer type variable');

        if accept(ttLBracket) then
        begin
          if LowerCase(sym.symType.symName) <> 'pointer' then
            error('Can only use deref''d pointer type variable as an array');

          TVariable(node).arrayParam := simplify(simpleExpression);
          if (TVariable(node).arrayParam.size > 1) or
            not(TVariable(node).arrayParam.isLeaf) or
            not(TVariable(node).arrayParam.isInteger or TVariable(node)
            .arrayParam.isVariable) then
            error('Deref pointer: array index must be size of 1 and be a number/variable');

          expect(ttRBracket);
        end;

        TVariable(node).deref := True;
      end;
    end;
  end
  else if (accept(ttIntNum)) then
  begin
    if isPointer then
      error('Factor: "' + token.tValue + '" incompatible with @ operation');

    node := TNumber.Create(token);
  end
  else if (accept(ttLParen)) then
  begin
    if isPointer then
      error('Factor: Immediate value incompatible with @ operation');

    node := simplify(expression());
    expect(ttRParen);
  end
  else
  begin
    error('factor: syntax error; found "' + Ftoken.tValue + '" ');
    nextToken();
  end;

  if isUnary then
    Result := simplify(TUnaryOp.Create(uToken.token(ttNeg, 'neg'), node))
  else
    Result := node;

  if isPointer then
  begin
    // writeLn('Pointer to "'+TVariable(node).token.tValue+'"');
    TVariable(node).isPointer := True;
  end;
end;

function TParserAST.signedFactor: TAST;
var
  token: TToken;
begin
  token := Ftoken;

  if accept(ttNot) then
    Result := simplify(TUnaryOp.Create(token, factor))
  else
    Result := factor;
end;

function TParserAST.term: TAST;
var
  token: TToken;
  node: TAST;
  right: TAST;
begin
  node := signedFactor;
  token := Ftoken;

  while (Ftoken.tType in [ttTimes, ttSlash, ttDiv, ttMod, ttAnd, ttShl,
    ttShr]) do
  begin
    token := Ftoken;
    nextToken();

    right := signedFactor;

    node := simplify(TBinOp.Create(node, token, right));
  end;

  Result := node;
end;

function TParserAST.simpleExpression: TAST;
var
  token: TToken;
  node: TAST;
  right: TAST;
begin
  node := term;
  token := Ftoken;

  while (Ftoken.tType in [ttPlus, ttMinus, ttOr, ttXor]) do
  begin
    token := Ftoken;
    nextToken();

    right := term;

    node := simplify(TBinOp.Create(node, token, right));
  end;

  Result := node;
end;

function isRelop(const aTokenType: TTokenType): Boolean;
begin
  Result := aTokenType in [ttEql, ttNeq, ttLss, ttLeq, ttGtr, ttGeq];
end;

function TParserAST.expression: TAST;
var
  token: TToken;
  node: TAST;
begin
  node := simplify(simpleExpression);

  if isRelop(Ftoken.tType) then
  begin
    token := Ftoken;
    nextToken;

    node := simplify(TBinOp.Create(node, token, simplify(simpleExpression)));
  end;

  Result := simplify(node);
end;

function TParserAST.doPoke: TPoke;
var
  address: TAST;
  value: TAST;
begin
  Result := TPoke.Create;

  expect(ttLParen);
  Result.address := simplify(simpleExpression);
  expect(ttComma);
  Result.expr := simplify(simpleExpression);
  expect(ttRParen);
end;

function TParserAST.doIf: TIfThen;
begin
  Result := TIfThen.Create;

  Result.condition := simplify(expression);

  expect(ttThen);

  Result.trueBranch := statement;
  Result.elseBranch := nil;

  if accept(ttElse) then
  begin
    Result.elseBranch := statement;
  end;
end;

function TParserAST.doWhile: TWhile;
begin
  Result := TWhile.Create;

  Result.condition := simplify(expression);

  expect(ttDo);

  Inc(FloopLevel);
  Result.statement := statement;
  Dec(FloopLevel);
end;

function TParserAST.doFor: TFor;
var
  variable: TAST;
  startValue: TAST;
  endValue: TAST;
  step: TNumber;
  incrementor: TBinOp;
begin
  // for variable := startValue to/downto endValue do
  Result := TFor.Create;

  variable := simplify(simpleExpression);

  if (not variable.isVariable) then
    error('For loop: initializer must assign to a variable');

  expect(ttBecomes);

  startValue := simplify(simpleExpression);

  Result.initializer := TAssign.Create(variable, startValue);

  if accept(ttTo) then
    step := TNumber.Create(1)
  else
  begin
    expect(ttDownTo);
    step := TNumber.Create(-1);
  end;

  // to/downto value
  endValue := simplify(simpleExpression);

  expect(ttDo);

  Inc(FloopLevel);
  Result.statement := statement;
  Dec(FloopLevel);

  if (step.value = 1) then
    Result.condition := TBinOp.Create(variable, token(ttLeq, '<='), endValue)
  else
    Result.condition := TBinOp.Create(variable, token(ttGeq, '>='), endValue);

  incrementor := TBinOp.Create(variable, token(ttPlus, '+'), step);

  Result.incrementer := TAssign.Create(variable, incrementor);
end;

function TParserAST.doBuiltInProc(aType: TTokenType; aName: AnsiString)
  : TBuiltinProcedure;
var
  paramCount: Integer;
  count: Integer;
begin
  expect(aType);

  if (aType in PROCS_C64) and (FprogAST.target <> ttC64) then
    error('To use procedure "' + aName + '" include "c64" in uses clause');

  case aType of
    ttWriteLn:
      paramCount := -1;
    ttWrite:
      paramCount := -1;
  else
    paramCount := 0;
  end;

  Result := TBuiltinProcedure.Create(aType);
  Result.procName := aName;

  count := 0;

  if accept(ttLParen) then
  begin
    if (Ftoken.tType <> ttRParen) then
    begin
      Result.params.Add(simplify(expression));
      Inc(count);

      while accept(ttComma) do
      begin
        Result.params.Add(simplify(expression));
        Inc(count);
      end;
    end;
    expect(ttRParen);
  end;

  if (paramCount <> -1) then
    if (count <> paramCount) then
      error(Format('Procedure "%s" expected %d params, found %d params',
        [aName, paramCount, count]));
end;

function TParserAST.doAssign(aVariable: TVariable): TAST;
var
  sym: TSymbol;
  arrayParam: TAST;
begin
  sym := FcurrentScope.lookup(aVariable.token.tValue);

  if (sym.symClass = scConst) then
    error('Cannot assign to left-hand-side!');

  if accept(ttCaret) then
  begin
    if LowerCase(sym.symType.symName) <> 'pointer' then
      error('Can only deref pointer types');

    aVariable.deref := True;
  end;

  expect(ttBecomes);

  Result := TAssign.Create(aVariable, simplify(expression));
end;

function TParserAST.doDecInc(decIncToken : TToken) : TAST;
var
  assignVar  : TAST;
  expr       : TAST;
begin
  expect(ttLParen);

  assignVar := simplify(simpleExpression);

  if not assignVar.isVariable then
    error('Illegal left-hand assignment expression in dec/inc call');

  if accept(ttComma) then begin
  // has optional, overriding +- value
    expr := simplify(simpleExpression);
  end
  else
  // default to 1 as altering value
    expr := TNumber.Create(Token(ttInteger,1));

  case decIncToken.tType of
    ttDec : Result := TAssign.Create(assignVar,TBinOp.Create(assignVar,Token(ttMinus,'-'),expr));
    ttInc : Result := TAssign.Create(assignVar,TBinOp.Create(assignVar,Token(ttPlus,'+'),expr));
  end;

  expect(ttRParen);
end;

function TParserAST.doCall(aProcName: AnsiString): TAST;
var
  sym: TProcSymbol;
  param: TAST;
  paramCount: Integer;
begin
  Result := TCall.Create;

  TCall(Result).procName := aProcName;

  sym := TProcSymbol(FcurrentScope.lookup(aProcName));

  if (sym = nil) then
    error('call: Undeclared procedure name "' + aProcName + '" found');

  sym.refCount := sym.refCount + 1;

  paramCount := 0;

  if accept(ttLParen) then
  begin
    if (Ftoken.tType <> ttRParen) then
    begin
      // at least one param passed
      param := simplify(expression);

      Inc(paramCount);

      TCall(Result).params.Add(param);

      // do others while a comma is found
      while accept(ttComma) do
      begin
        param := simplify(expression);

        Inc(paramCount);

        TCall(Result).params.Add(param);
      end;
    end;
    expect(ttRParen);
  end;

  if (paramCount <> sym.paramCount) then
    error('call: Parameter count mismatch;  found ' + IntToStr(paramCount) +
      ', expected ' + IntToStr(sym.paramCount));
end;

function TParserAST.doMacroCall(aMacroName: AnsiString): TAST;
var
  sym: TMacroSymbol;
  param: TAST;
  paramCount: Integer;
begin
  Result := TMacroCall.Create;

  TMacroCall(Result).macroName := aMacroName;

  sym := TMacroSymbol(FcurrentScope.lookup(aMacroName));

  if (sym = nil) then
    error('call: Undeclared macro name "' + aMacroName + '" found');

  if sym.symClass <> scMacro then
    error('Macro call: "' + aMacroName + '" is not a macro');

  paramCount := 0;

  if accept(ttLParen) then
  begin
    if (Ftoken.tType <> ttRParen) then
    begin
      // at least one param passed
      param := simplify(expression);

      if not(param.isVariable or param.isInteger or param.isString) then
        error('Macro parameters can only be a variable, integer or string');

      Inc(paramCount);

      if param.isInteger then
        TMacroCall(Result).params.Add(TNumber(param).token.tValue)
      else if param.isVariable then
        TMacroCall(Result).params.Add(TVariable(param).token.tValue)
      else
        TMacroCall(Result).params.Add('"' + TString(param).token.tValue + '"');

      // do others while a comma is found
      while accept(ttComma) do
      begin
        param := simplify(expression);

        if not(param.isVariable or param.isInteger or param.isString) then
          error('Macro parameters can only be a variable, integer or string');

        Inc(paramCount);

        if param.isInteger then
          TMacroCall(Result).params.Add(TNumber(param).token.tValue)
        else if param.isVariable then
          TMacroCall(Result).params.Add(TVariable(param).token.tValue)
        else
          TMacroCall(Result).params.Add('"' + TString(param)
            .token.tValue + '"');
      end;
    end;
    expect(ttRParen);
  end;

  if sym.paramCount <> paramCount then
    error(Format('Macro call: found %d parameters, expected %d',
      [paramCount, sym.paramCount]));
end;

function TParserAST.doFuncCall(aFuncName: AnsiString): TAST;
var
  sym: TFuncSymbol;
  param: TAST;
  paramCount: Integer;
begin
  Result := TFuncCall.Create;

  uASM.enterFunction;

  TFuncCall(Result).funcName := aFuncName;

  sym := TFuncSymbol(FcurrentScope.lookup(aFuncName));

  if (sym = nil) then
    error('call: Undeclared function name "' + aFuncName + '" found');

  TFuncCall(Result).funcType := sym.funcType;
  TFuncCall(Result).size     := sym.symType.size;

  sym.refCount := sym.refCount + 1;

  paramCount := 0;

  if accept(ttLParen) then
  begin
    if (Ftoken.tType <> ttRParen) then
    begin
      // at least one param passed
      param := simplify(expression);

      Inc(paramCount);

      TFuncCall(Result).params.Add(param);

      // do others while a comma is found
      while accept(ttComma) do
      begin
        param := simplify(expression);

        Inc(paramCount);

        TFuncCall(Result).params.Add(param);
      end;
    end;
    expect(ttRParen);
  end;

  if (paramCount <> sym.paramCount) then
    error('call: Parameter count mismatch;  found ' + IntToStr(paramCount) +
      ', expected ' + IntToStr(sym.paramCount));

  uASM.exitFunction;
end;

function TParserAST.doSetupRasterIRQ: TAST;
var
  irq: TSetupRasterIRQ;
  sym: TProcSymbol;
  e: TAST;
begin
  irq := TSetupRasterIRQ.Create;

  Result := irq;

  expect(ttLParen);

  e := simplify(simpleExpression);

  if not e.isVariable then
    error('SetupRasterIRQ: param 1 must be an identifier');

  sym := TProcSymbol(FcurrentScope.lookup(TVariable(e).token.tValue));

  if sym = nil then
    error('SetupRasterIRQ: Undeclared identifier "' + TVariable(e)
      .token.tValue + '"');

  if sym.irqType <> ttRasterIRQ then
    error('SetupRasterIRQ: routine "' + TVariable(e).token.tValue +
      '" is not a raster interrupt routine');

  sym.refCount := sym.refCount + 1;

  irq.routineName := TVariable(e).token.tValue;

  expect(ttComma);
  e := simplify(simpleExpression);

  if not e.isInteger then
    error('SetupRasterIRQ: param 2 must be an integer');

  irq.rasterLine := TNumber(e).token.tValue;
  expect(ttRParen);
end;

function TParserAST.doEndIRQ: TAST;
var
  irq: TEndIRQ;
  sym: TProcSymbol;
  e: TAST;
begin
  irq := TEndIRQ.Create;

  Result := irq;

  expect(ttLParen);
  e := simplify(simpleExpression);

  if not e.isVariable then
    error('EndIRQ: param 1 must be an identifier');

  sym := TProcSymbol(FcurrentScope.lookup(TVariable(e).token.tValue));

  if sym = nil then
    error('EndIRQ: Undeclared identifier "' + TVariable(e).token.tValue + '"');

  if sym.irqType <> ttRasterIRQ then
    error('EndIRQ: routine "' + TVariable(e).token.tValue +
      '" is not a raster interrupt routine');

  sym.refCount := sym.refCount + 1;

  irq.routineName := TVariable(e).token.tValue;
  irq.useSameRaster := True;

  if accept(ttComma) then
  begin
    e := simplify(simpleExpression);

    if not e.isInteger then
      error('EndIRQ: param 2 must be an integer');

    irq.rasterLine := TNumber(e).token.tValue;
    irq.useSameRaster := False;
  end;
  expect(ttRParen);
end;

function TParserAST.statement: TAST;
var
  token      : TToken;
  assignVar  : TAST;
  expr       : TAST;
  incDecExpr : TAST;
  sym        : TSymbol;
begin
  token := Ftoken;

  if (token.tType = ttIdent) then
  begin
    assignVar := simplify(simpleExpression);

    if not (assignVar is TVariable) then error('statement: expected an assignment variable, procedure call, or macro call.');

    if (Ftoken.tType in [ttSemiColon, ttLParen]) then
    begin
      // found a ';' or '(' so must be a "call" or macro "call" statement...
      sym := FcurrentScope.lookup(TVariable(assignVar).token.tValue);

      if sym = nil then
        error('Undeclared procedure/macro name "' + TVariable(assignVar)
          .token.tValue + '"');

      if sym.symClass = scFunc then
        error('Function "' + TVariable(assignVar).token.tValue +
          '" must be part of an assignment statement');

      if sym.symClass = scProc then
        Result := doCall(TVariable(assignVar).token.tValue)
      else
        Result := doMacroCall(TVariable(assignVar).token.tValue);
    end
    else
    begin
      if not assignVar.isVariable then
        error('Illegal left-hand assignment expression or procedure call name');

      // not a call, so must be an assignment
      Result := doAssign(TVariable(assignVar));
    end;
  end
  else if accept(ttExit) then
  begin
    Result := TExit.Create;
  end
  else if accept(ttBreak) then
  begin
    if FloopLevel = 0 then error('Must be inside a loop to break out of!');

    Result := TBreak.Create;
  end
  else if (Ftoken.tType = ttBegin) then
  begin
    Result := compound;
  end
  else if (Ftoken.tType = ttAsm) then
  begin
    Result := asmCompound;
  end
  else if accept(ttDec) or accept(ttInc) then begin
  // is a inc/dec command so process that
    Result := doDecInc(token);
  end
  else if accept(ttPoke) then
    Result := doPoke
  else if (accept(ttIf)) then
    Result := doIf
  else if (accept(ttWhile)) then
    Result := doWhile
  else if (accept(ttFor)) then
    Result := doFor
    // else if accept(ttWriteLn) then
    // Result := doWriteLn
  else if (Ftoken.tType in BUILT_IN_PROCS) then
    Result := doBuiltInProc(Ftoken.tType, Ftoken.tValue)
  else if accept(ttSetupRasterIRQ) then
  begin
    Result := doSetupRasterIRQ;
  end
  else if accept(ttEndIRQ) then
  begin
    Result := doEndIRQ;
  end
  else if (Ftoken.tType = ttSemiColon) then
    // no statement here so return empty one!
    Result := TEmpty.Create
  else
  begin
    error('statement: syntax error; found "' + Ftoken.tValue + '"');
    nextToken();
  end;
end;

function TParserAST.compound: TCompound;
begin
  expect(ttBegin);

  Result := TCompound.Create;

  repeat
    if (Ftoken.tType = ttEnd) then
      break;

    Result.children.Add(statement);

  until not(accept(ttSemiColon));

  expect(ttEnd);
end;

function TParserAST.asmCompound: TAsmCompound;
var
  i: Integer;
  asmStart: Integer;
  asmEnd: Integer;
begin
  Result := TAsmCompound.Create;
  expect(ttAsm);
  asmStart := Flexer.tokenRow;

  while (Ftoken.tType <> ttEOF) and (Ftoken.tType <> ttEnd) do
  begin
    nextToken;
  end;

  if (Ftoken.tType = ttEnd) then
    asmEnd := Flexer.tokenRow - 1;

  nextToken;

  for i := asmStart to asmEnd do
    Result.asmStatements.Add(Flines.Strings[i - 1]);
end;

function TParserAST.getPointerAddress: Byte;
begin
  if (FpointerAddress > MAX_POINTER_ADDRESS) then
    error('Exceeded max pointer address $"' + IntToHex(MIN_POINTER_ADDRESS,
      2) + '"');

  Result := FpointerAddress;
  // inc twice due to 2 byte pointers
  Inc(FpointerAddress);
  Inc(FpointerAddress);
end;

procedure appendStr(var aList: TStringArray; aVal: AnsiString);
var
  i: Integer;
begin
  i := Length(aList);

  SetLength(aList, i + 1);
  aList[i] := aVal;
end;

procedure appendNumber(var aList: TNumberArray; aVal: TNumber);
var
  i: Integer;
begin
  i := Length(aList);

  SetLength(aList, i + 1);
  aList[i] := aVal;
end;

procedure TParserAST.doTypedConstDecl_array(aConstName: AnsiString;
  aBlock: TBlock);
var
  constType: TSymbol;
  constTypeName: TToken;
  e: TAST;
  count: Integer;
  loRange: Integer;
  hiRange: Integer;
  values: TNumberArray;
  constSym: TConstSymbol;

  function getValue: TAST;
  begin
    Result := simplify(simpleExpression);

    if not Result.isInteger then
      error('Const decl: array values must be numeric');
    if Result.size > constType.size then
      error('Const decl: array value too large');
  end;

begin
  expect(ttLBracket);
  // get array lo range
  e := simplify(simpleExpression);

  if not e.isInteger then
    error('Const decl: array type low index must be a number');

  loRange := TNumber(e).value;

  if loRange <> 0 then
    error('Const decl: array type low index must = 0');

  expect(ttRange);

  // get array hi range
  e := simplify(simpleExpression);

  if not e.isInteger then
    error('Const decl: array type high index must be a number');

  hiRange := TNumber(e).value;

  if hiRange < loRange then
    error('Const decl: array type hi index must  >= lo index');

  expect(ttRBracket);

  expect(ttOf);

  constTypeName := Ftoken;

  if not(constTypeName.tType in [ttByte, ttInteger]) then
    error('Const decl: array types must be numeric, found type "' +
      constTypeName.tValue + '"');

  constType := FcurrentScope.lookup(constTypeName.tValue);

  expect(constTypeName.tType);

  expect(ttEql);

  expect(ttLParen);

  count := 1;
  SetLength(values, 0);

  appendNumber(values, TNumber(getValue));

  while accept(ttComma) do
  begin
    appendNumber(values, TNumber(getValue));
    Inc(count);
  end;

  if (count <> hiRange + 1) then
    error('Typed const: array; value count <> range');

  expect(ttRParen);

  constSym := TConstSymbol.Create(aConstName, constType);
  constSym.isArray := True;
  constSym.len := count;

  FcurrentScope.insert(constSym);

  // add this const decl to the block's list
  aBlock.declarations.Add(TConstDecl.Create(aConstName, values, constType));
  expect(ttSemiColon);
end;

procedure TParserAST.constDecls(aBlock: TBlock);
var
  constName: AnsiString;
  constValue: TToken;
  sym: TSymbol;
  constTypeName: TToken;
  value: TToken;
  e: TAST;
  constType: TSymbol;
begin
  repeat
    constName := Ftoken.tValue;

    sym := FcurrentScope.lookup(constName, True);

    if (sym <> nil) then
      error('Const decl: identifier "' + constName +
        '" already exists in scope "' + FcurrentScope.scopeName + '"');

    expect(ttIdent);

    if accept(ttColon) then
    begin
      // is a typed constant
      // name : type = value
      if accept(ttArray) then
        // is array constant
        doTypedConstDecl_array(constName, aBlock)
      else
      begin
        // other typed constant; number, string
        constTypeName := Ftoken;

        if not(constTypeName.tType in [ttByte, ttInteger, ttString, ttAscii,
          ttPetscii]) then
          error('Const decl: array types must be numeric, string, or ascii, found type "'
            + constTypeName.tValue + '"');

        constType := FcurrentScope.lookup(constTypeName.tValue);

        if constType = nil then
          error('Const decl: Unknown const type "' + constTypeName.tValue +
            '" found');

        expect(constTypeName.tType);

        expect(ttEql);

        // get expression and simplify; if not a number, throw error

        e := simplify(expression);

        if constTypeName.tType in [ttString, ttAscii, ttPetscii] then
        begin
          if not(e.isString) then
            error('Const declaration: expected a string value!');

          constValue := TString(e).token;
        end
        else
        begin
          if not(e.isInteger) then
            error('Const declaration: expected a number value!');

          constValue := TNumber(e).token;
        end;

        expect(ttSemiColon);

        FcurrentScope.insert(TConstSymbol.Create(constName, constValue,
          constType));

        // add this const decl to the block's list
        aBlock.declarations.Add(TConstDecl.Create(constName, constValue,
          constType));
      end;
    end
    else
    begin
      // normal constant; number, string
      expect(ttEql);

      if accept(ttImportBinary) then
        parseImportBinary(constName)
      else
      begin
        // get expression and simplify; if not a number, throw error

        e := simplify(expression);

        if not(e.isInteger or e.isString) then
          error('Const declaration must equate to a number or string!');

        constValue := TNumber(e).token;

        expect(ttSemiColon);

        FcurrentScope.insert(TConstSymbol.Create(constName, constValue, nil));

        // add this const decl to the block's list
        aBlock.declarations.Add(TConstDecl.Create(constName, constValue, nil));
      end;
    end;
  until (Ftoken.tType <> ttIdent);
end;

procedure TParserAST.typeDecls(aBlock: TBlock);
var
  typeName: AnsiString;
  varName: AnsiString;
  varType: TToken;
  sym: TSymbol;
  varSym: TSymbol;
  rSymbol: TRecordSymbol;
  vars: array of AnsiString;
  i: Integer;

  procedure appendVar(aName: AnsiString);
  begin
    i := Length(vars);
    SetLength(vars, i + 1);
    vars[i] := aName;
  end;

begin
  typeName := Ftoken.tValue;

  rSymbol := TRecordSymbol.Create(typeName);

  if not FcurrentScope.insert(rSymbol, True) then
    error('type decl: "' + typeName + '" already exists');

  expect(ttIdent);
  expect(ttEql);
  expect(ttRecord);

  repeat
    // get var name for this section of record
    SetLength(vars, 0);
    varName := Ftoken.tValue;
    appendVar(varName);
    expect(ttIdent);

    while accept(ttComma) do
    begin
      varName := Ftoken.tValue;
      appendVar(varName);
      expect(ttIdent);
    end;

    expect(ttColon);

    // get var type for this section of record " : <type>"
    varType := Ftoken;

    sym := FcurrentScope.lookup(varType.tValue);

    if (sym = nil) then
      error('Record; Var decl: Unsupported var type "' + varType.tValue + '"');

    if not(varType.tType in [ttByte, ttBoolean, ttPointer, ttWord, ttDWord,
      ttString]) then
    begin
      if not(sym is TRecordSymbol) then
        error('Record; Var decl: Unsupported var type "' +
          varType.tValue + '"');
    end;

    expect(varType.tType);

    // add children var decls
    for i := 0 to Length(vars) - 1 do
    begin
      varSym := varSymbol(varType.tValue, sym);

      FcurrentScope.insert(varSym);

      rSymbol.vars.Add(TVarDecl.Create(vars[i], varSym, '', ''))
    end;

    expect(ttSemiColon);

  until Ftoken.tType = ttEnd;

  expect(ttEnd);
  expect(ttSemiColon);
end;

var
  recVarName: array of AnsiString;

procedure resetRecVarName;
begin
  SetLength(recVarName, 0);
end;

procedure pushRecVarName(aName: AnsiString);
var
  i: Integer;
begin
  i := Length(recVarName);
  SetLength(recVarName, i + 1);
  recVarName[i] := aName;
end;

procedure popRecVarName;
begin
  SetLength(recVarName, High(recVarName));
end;

procedure TParserAST.expandRecordVarToSymbolTable(aVarDecl: TVarDecl);
var
  i: Integer;
  varName: AnsiString;
begin
  if (aVarDecl.varSym <> nil) and (aVarDecl.varSym.symType is TRecordSymbol)
  then
  begin
    // is record so emit record header and children var decls
    pushRecVarName(aVarDecl.varName);

    for i := 0 to TRecordSymbol(aVarDecl.varSym.symType).vars.count - 1 do
      expandRecordVarToSymbolTable
        (TVarDecl(TRecordSymbol(aVarDecl.varSym.symType).vars.Items[i]));

    popRecVarName;
  end
  else
  begin
    // add this fully qualified var name into the symbol table
    // so it can be found by factor
    varName := '';
    for i := 0 to Length(recVarName) - 1 do
    begin
      varName := varName + recVarName[i];
      if i < Length(recVarName) - 1 then
        varName := varName + '.';
    end;
    varName := varName + '.' + aVarDecl.varName;

    FcurrentScope.insert(varSymbol(varName,
      FcurrentScope.lookup(aVarDecl.varSym.symName)));

    for i := 0 to aVarDecl.varSym.size - 1 do
    begin
      FcurrentScope.insert(varSymbol(varName + '.b' + IntToStr(i),
        FcurrentScope.lookup('byte')));
    end;
  end;
end;

procedure TParserAST.varDecls(aBlock: TBlock);
var
  i, j: Integer;
  varList: TStringArray;
  address: AnsiString;
  varSym: TSymbol;
  varType: TSymbol;
  e: TAST;
  defaultValue: AnsiString;
  variable: TVarDecl;
  isArray: Boolean;
  arrayType: TTokenType;
  arraySize: Integer;
begin
  SetLength(varList, 0);
  defaultValue := '';

  repeat
    if (FcurrentScope.lookup(Ftoken.tValue, True) <> nil) then
      error('Var decl: identifier "' + Ftoken.tValue +
        '" already exists in scope "' + FcurrentScope.scopeName + '"');

    appendStr(varList, Ftoken.tValue);

    expect(ttIdent);
  until not(accept(ttComma));

  expect(ttColon);

  isArray := False;

  if accept(ttArray) then
  begin
    // parse array Var decl
    isArray := True;
    expect(ttLBracket);

    e := simplify(simpleExpression);

    if not e.isInteger then
      error('Var decl: array low bound must be a number');

    if (e.toString <> '0') then
      error('Var decl: array low bound must = "0"');

    expect(ttRange);

    e := simplify(simpleExpression);

    if not e.isInteger then
      error('Var decl: array high bound must be a number');

    if (TNumber(e).value < 0) then
      error('Var decl: array high bound must >= 0');

    arraySize := TNumber(e).value + 1;

    expect(ttRBracket);

    expect(ttOf);
  end;

  // var type
  varType := FcurrentScope.lookup(Ftoken.tValue);

  if (varType = nil) then
    error('Var decl: Unsupported var type "' + Ftoken.tValue + '"');

  if isArray and (Ftoken.tType = ttString) then
    error('Var decl: array type cannot be a "string"');

  if not(Ftoken.tType in [ttBoolean, ttByte, ttPointer, ttWord, ttDWord,
    ttString]) then
  begin
    if not(varType is TRecordSymbol) then
      error('Var decl: Unsupported var type "' + Ftoken.tValue + '"');
  end;

  arrayType := Ftoken.tType;

  if (Ftoken.tType = ttPointer) then
  begin
    address := '$' + IntToHex(getPointerAddress, 2);

    if isArray then
      // skip other array pointer values for other pointers
      for i := 1 to arraySize - 1 do
        getPointerAddress;
  end
  else
    address := '';

  expect(Ftoken.tType);

  if accept(ttAbsolute) then
  begin
    if isArray or (address <> '') then
      error('Cannot use absolute on pointer variables/array types');

    e := simplify(simpleExpression);
    if not(e.isInteger or e.isVariable) then
      error('Absolute: Only supports number/variable absolute addresses');

    if e.isInteger then
      address := TNumber(e).token.tValue
    else
      address := TVariable(e).token.tValue;
  end;

  if accept(ttEql) then
  begin
    if isArray then
      error('Var decl: arrays cannot have a default value');

    // has a default value
    if address <> '' then
      error('Var decl: default values cannot be used on absolute or pointer variables');

    if Length(varList) > 1 then
      error('Var decl: only single var decls can have default values');

    e := simplify(simpleExpression);

    if not e.isInteger then
      error('Var decl: default value must be a number');

    defaultValue := TNumber(e).token.tValue;
  end;

  expect(ttSemiColon);

  // generate var decls and add to the block's list
  for i := 0 to High(varList) do
  begin
    // insert base var decl
    varSym := varSymbol(varList[i], varType);
    TVarSymbol(varSym).isArray := isArray;
    TVarSymbol(varSym).arraySize := arraySize;

    FcurrentScope.insert(varSym);

    variable := TVarDecl.Create(varList[i], varSym, address, defaultValue);
    variable.isArray := isArray;
    variable.arraySize := arraySize;
    variable.isPointer := arrayType = ttPointer;

    aBlock.declarations.Add(variable);

    // add all declarations for this variable, including sub-parts
    // for multi-byte variables
    // a: byte = a.b0, b: integer = b.b0, b.b1

    if varType.symName <> 'string' then
    begin
      if (varType is TRecordSymbol) then
      begin
        // expand this record var to the symbol table
        resetRecVarName;
        expandRecordVarToSymbolTable(variable);
      end;

      for j := 0 to varType.size - 1 do
      begin
        varSym := varSymbol(varList[i] + '.b' + IntToStr(j),
          FcurrentScope.lookup('byte'));

        FcurrentScope.insert(varSym);
      end;
    end;
  end;
end;

function TParserAST.formalParams(var paramCount: Integer): TParamArray;
const
  PARAM_VAR_PREFIX = [ttConst, ttVar];

var
  token: TToken;
  paramVars: array of AnsiString;
  paramSym: TSymbol;
  paramType: TSymbol;
  i, j: Integer;
begin
  // exit early if empty () and no parameters
  if (Ftoken.tType = ttRParen) then
    Exit;

  repeat
    SetLength(paramVars, 0);
    repeat
      // consume and ignore 'const' and 'var' parameter prefixes
      if (Ftoken.tType in PARAM_VAR_PREFIX) then
        nextToken;

      Inc(paramCount);

      paramSym := FcurrentScope.lookup(Ftoken.tValue, True);

      if (paramSym <> nil) then
        error('Procedure "' + FcurrentScope.scopeName +
          '": Duplicate param identifier "' + Ftoken.tValue + '" found');

      i := Length(paramVars);
      SetLength(paramVars, i + 1);
      paramVars[i] := Ftoken.tValue;
      expect(ttIdent);
    until not accept(ttComma);

    expect(ttColon);

    paramType := FcurrentScope.lookup(Ftoken.tValue);

    if (paramType = nil) then
      error('Procedure "' + FcurrentScope.scopeName +
        '": Undefined param var type "' + Ftoken.tValue + '" found');

    for i := 0 to Length(paramVars) - 1 do
    begin
      j := Length(Result);
      SetLength(Result, j + 1);
      Result[j] := TParam.Create(paramVars[i], paramType);
    end;

    // skip type
    nextToken;
  until not accept(ttSemiColon);
end;

procedure TParserAST.macroDecl(aBlock: TBlock);
var
  i, j: Integer;
  macroName: AnsiString;
  params: TStringArray;
  macroScope: TscopedSymbolTable;
  macroSym: TMacroSymbol;
  sym: TSymbol;
  paramCount: Integer;
  thisMacro: TMacroDecl;
  loReg: AnsiString;
  hiReg: AnsiString;
begin
  if (FcurrentScope.scopeLevel > 1) then
    error('Macro decl: Macros can only be defined in the global scope');

  macroName := Ftoken.tValue;

  sym := FcurrentScope.lookup(macroName, True);

  if sym <> nil then
    error('Macro decl: redeclared identifier "' + macroName + '"');

  expect(ttIdent);

  paramCount := 0;

  SetLength(params, 0);

  if accept(ttLParen) then
  begin
    if Ftoken.tType <> ttRParen then
    begin
      appendStr(params, Ftoken.tValue);
      if (Ftoken.tType <> ttIdent) then
        error('Macro decl: parameters must be identifiers only');
      nextToken;
      Inc(paramCount);

      while accept(ttComma) do
      begin
        appendStr(params, Ftoken.tValue);
        if (Ftoken.tType <> ttIdent) then
          error('Macro decl: parameters must be identifiers only');
        nextToken;
        Inc(paramCount);
      end;
    end;

    expect(ttRParen);
  end;

  macroSym := TMacroSymbol.Create(macroName, params);

  FcurrentScope.insert(macroSym);

  macroSym.macroType := ttMacro;
  macroSym.paramCount := paramCount;

  expect(ttSemiColon);

  if accept(ttAssembler) then
  begin
    macroSym.macroType := ttAssembler;
    expect(ttSemiColon);
  end;

  if (macroSym.macroType = ttMacro) then
    thisMacro := TMacroDecl.Create(macroName, params,
      block(macroName, ttEOF, False, True), macroScope)
  else if (macroSym.macroType = ttAssembler) then
    thisMacro := TMacroDecl.Create(macroName, params,
      asmBlock(macroName, False, ttEOF, False, True), macroScope);

  aBlock.declarations.Add(thisMacro);
end;

procedure TParserAST.procDecl(aBlock: TBlock);
var
  i, j: Integer;
  procName: AnsiString;
  params: TParamArray;
  procScope: TscopedSymbolTable;
  procSym: TProcSymbol;
  sym: TProcSymbol;
  paramCount: Integer;
  thisProc: TProcDecl;
  loReg: AnsiString;
  hiReg: AnsiString;
  paramVar: TVarDecl;
begin
  procName := Ftoken.tValue;

  procSym := TProcSymbol.Create(procName, []);

  sym := TProcSymbol(FcurrentScope.lookup(procName, True));

  if (sym <> nil) and (sym.isFwdDecl) then
  begin
    // FcurrentScope.replace(procSym,True);
    FreeAndNil(procSym);
    procSym := sym;
    procSym.isFwdDecl := False;
  end
  else if not FcurrentScope.insert(procSym, True) then
    error('procDecl: Redeclared identifier "' + procName + '"');

  procScope := TscopedSymbolTable.Create(procName, FcurrentScope.scopeLevel + 1,
    FcurrentScope);
  FcurrentScope := procScope;

  expect(ttIdent);

  paramCount := 0;

  if accept(ttLParen) then
  begin
    params := formalParams(paramCount);

    expect(ttRParen);
  end;

  procSym.procType := ttProc;

  if accept(ttExternal) then
  begin
    if not(Ftoken.tType in [ttIntNum, ttIdent]) then
      error('External proc address: Only supports number/identifier addresses');

    procSym.procType := ttAssembler;
    procSym.procAddress := Ftoken.tValue;

    expect(Ftoken.tType);
  end
  else
  begin
    expect(ttSemiColon);

    procSym.irqType := ttNone;

    if accept(ttRasterIRQ) then
    begin
      if paramCount > 0 then
        error('Proc Decl: raster irq routines cannot have parameters');
      procSym.irqType := ttRasterIRQ;

      expect(ttSemiColon);
    end;

    if accept(ttAssembler) then
    begin
      procSym.procType := ttAssembler;

      expect(ttSemiColon);
    end;
  end;

  procSym.paramCount := paramCount;

  if accept(ttForward) then
  begin
    procSym.isFwdDecl := True;
  end
  else
  begin
    // not a forward declaration
    // insert these paramVar : paramType pairs into the current scope
    // including multi-byte parts
    for i := 0 to Length(params) - 1 do
    begin
      // only write non-assembler procedure params as variables in scope

      if (procSym.procType = ttProc) or not isAsmParam(params[i].paramName,
        params[i].paramType, loReg, hiReg) then
      begin
        FcurrentScope.insert(varSymbol(params[i].paramName,
          params[i].paramType));

        for j := 0 to params[i].paramType.size - 1 do
          FcurrentScope.insert(varSymbol(params[i].paramName + '.b' +
            IntToStr(j), FcurrentScope.lookup('byte')));
      end;
    end;

    if (procSym.procType = ttProc) then
      thisProc := TProcDecl.Create(procName, params,
        block(procName, ttByte, False, False), procScope)
    else if (procSym.procType = ttAssembler) then
      thisProc := TProcDecl.Create(procName, params,
        asmBlock(procName, procSym.procAddress <> '', ttByte, False, False),
        procScope);

    SetLength(procSym.params, Length(params));

    for i := Length(params) - 1 downto 0 do
    begin
      procSym.params[i] := TVarSymbol(varSymbol(params[i].paramName,
        params[i].paramType));

      if (procSym.procType = ttProc) or not isAsmParam(params[i].paramName,
        params[i].paramType, loReg, hiReg) then
      begin
        paramVar := TVarDecl.Create(params[i].paramName,
          params[i].paramType, '', '');
        if (params[i].paramType.symName = 'pointer') then
        begin
          paramVar.isPointer := True;
          paramVar.address := '$' + IntToHex(getPointerAddress, 2);
        end;
        thisProc.block.declarations.insert(0, paramVar);
      end;
    end;

    aBlock.declarations.Add(thisProc);
  end;

  // restore previous scope
  FcurrentScope := procScope.enclosingScope;
end;

procedure TParserAST.funcDecl(aBlock: TBlock);
var
  i, j: Integer;
  funcName: AnsiString;
  params: TParamArray;
  funcScope: TscopedSymbolTable;
  funcSym: TFuncSymbol;
  funcResult: TVarSymbol;
  paramCount: Integer;
  thisFunc: TFuncDecl;
  loReg: AnsiString;
  hiReg: AnsiString;
  retType: TTokenType;
  retValue : AnsiString;
  varSym   : TVarSymbol;
begin
  funcName := Ftoken.tValue;

  funcSym := TFuncSymbol.Create(funcName, []);

  FcurrentScope.insert(funcSym);

  funcScope := TscopedSymbolTable.Create(funcName, FcurrentScope.scopeLevel + 1,
    FcurrentScope);
  FcurrentScope := funcScope;

  expect(ttIdent);

  paramCount := 0;

  if accept(ttLParen) then
  begin
    params := formalParams(paramCount);

    expect(ttRParen);
  end;

  funcSym.funcType := ttFunc;

  expect(ttColon);

  retType  := Ftoken.tType;
  retValue := LowerCase(Ftoken.tValue);

  if (not (retType in [ttByte,ttWord])) then
    error('Func decl: only supports "byte/word" return type, found "' +
      Ftoken.tValue + '"');

  funcSym.symType := FcurrentScope.lookup(Ftoken.tValue);

  nextToken;

  if accept(ttExternal) then
  begin
    if (Ftoken.tType <> ttIntNum) then
      error('External func address: Only supports number addresses');

    funcSym.funcType := ttAssembler;
    funcSym.funcAddress := Ftoken.tValue;

    expect(ttIntNum);
  end
  else
  begin
    expect(ttSemiColon);

    if accept(ttAssembler) then
    begin
      funcSym.funcType := ttAssembler;
      expect(ttSemiColon);
    end;
  end;

  // insert these paramVar : paramType pairs into the current scope
  // including multi-byte parts
  for i := 0 to Length(params) - 1 do
  begin
    // only write non-assembler funcedure params as variables in scope

    if (funcSym.funcType = ttFunc) or not isAsmParam(params[i].paramName,
      params[i].paramType, loReg, hiReg) then
    begin
      FcurrentScope.insert(varSymbol(params[i].paramName, params[i].paramType));

      for j := 0 to params[i].paramType.size - 1 do
        FcurrentScope.insert(varSymbol(params[i].paramName + '.b' + IntToStr(j),
          FcurrentScope.lookup('byte')));
    end;
  end;

  funcSym.paramCount := paramCount;

  if funcSym.funcAddress = '' then
  begin
    // add result type to this function body
    funcResult := TVarSymbol(varSymbol('result', funcSym.symType));
    funcResult.isFuncResult := True;
    FcurrentScope.insert(funcResult,true);

    for j := 0 to FcurrentScope.lookup(retValue).size - 1 do
    begin
      FcurrentScope.insert(varSymbol('result.b' + IntToStr(j),
          FcurrentScope.lookup('byte')));
    end;
  end;

  if (funcSym.funcType = ttFunc) then
    thisFunc := TFuncDecl.Create(funcName, params,
      block(funcName, retType, funcSym.funcAddress <> '', False), funcScope)
  else if (funcSym.funcType = ttAssembler) then
    thisFunc := TFuncDecl.Create(funcName, params,
      asmBlock(funcName, funcSym.funcAddress <> '', retType,
      funcSym.funcAddress <> '', False), funcScope);

  SetLength(funcSym.params, Length(params));

  for i := Length(params) - 1 downto 0 do
  begin
    funcSym.params[i] := TVarSymbol(varSymbol(params[i].paramName,
      params[i].paramType));

    if (funcSym.funcType = ttFunc) or not isAsmParam(params[i].paramName,
      params[i].paramType, loReg, hiReg) then
      thisFunc.block.declarations.insert(0, TVarDecl.Create(params[i].paramName,
        params[i].paramType, '', ''));
  end;

  if funcSym.funcType <> ttAssembler then begin
//    for j := 0 to FcurrentScope.lookup(retValue).symType.size - 1 do
//      FcurrentScope.insert(varSymbol('result.b' + IntToStr(j),
//        FcurrentScope.lookup('byte')));

    thisFunc.block.declarations.insert(0, TVarDecl.Create('result',FcurrentScope.lookup(retValue),'',''));
  end;

//  thisFunc.block.declarations.insert(0, TVarDecl.Create('result',
//    FcurrentScope.lookup('byte'), '', ''));
  aBlock.declarations.Add(thisFunc);

  // restore previous scope
  FcurrentScope := funcScope.enclosingScope;
end;

function TParserAST.block(aBlockName: AnsiString; aRetType: TTokenType;
  aIsFunc: Boolean; aIsMacro: Boolean): TBlock;
var
  token: TToken;
  sym: TSymbol;
begin
  Result := TBlock.Create;
  Result.blockName := aBlockName;
  Result.retType := aRetType;
  Result.isFunc := aIsFunc;
  Result.isMacro := aIsMacro;

//  if aIsFunc then
//  begin
//    case aRetType of
//      ttByte:
//        sym := FcurrentScope.lookup('byte');
//      // ttWord  : FcurrentScope.insert(varSymbol());
//    end;
//
//    FcurrentScope.insert(varSymbol('result', sym));
//    // add result variable decl
//    Result.declarations.Add(TVarDecl.Create('result', sym, '', ''));
//  end;

  if not aIsMacro then
  begin
    repeat
      if (accept(ttConst)) then
      begin
        constDecls(Result)
      end;

      if accept(ttImport) then
      begin
        parseImport(FprogAST, Result);
      end;

      if accept(ttType) then
      begin
        repeat
          typeDecls(Result);
        until Ftoken.tType <> ttIdent;
      end;

      if (accept(ttVar)) then
      begin
        repeat
          if (Ftoken.tType = ttVar) then
            nextToken;

          varDecls(Result);
        until (Ftoken.tType <> ttIdent) and (Ftoken.tType <> ttVar);
      end;

      while (Ftoken.tType in [ttProc, ttFunc, ttMacro]) do
      begin
        token := Ftoken;
        nextToken;
        case token.tType of
          ttProc:
            procDecl(Result);
          ttFunc:
            funcDecl(Result);
          ttMacro:
            macroDecl(Result);
        end;

        expect(ttSemiColon);
      end;
    until not(Ftoken.tType in [ttConst, ttType, ttVar, ttProc, ttFunc, ttMacro,
      ttImport]);
  end;

  Result.compoundStatement := compound;
end;

function TParserAST.asmBlock(aBlockName: AnsiString;
  aIsAbsoluteAsmProc: Boolean; aRetType: TTokenType; aIsFunc: Boolean;
  aIsMacro: Boolean): TAsmBlock;
var
  i: Integer;
  asmStart: Integer;
  asmEnd: Integer;
  sym: TSymbol;
begin
  Result := TAsmBlock.Create;
  Result.blockName := aBlockName;
  Result.retType := aRetType;
  Result.isFunc := aIsFunc;
  Result.isMacro := aIsMacro;

//  if aIsFunc then
//  begin
//    case aRetType of
//      ttByte:
//        sym := FcurrentScope.lookup('byte');
//      // ttWord  : FcurrentScope.insert(varSymbol());
//    end;
//
//    FcurrentScope.insert(varSymbol('result', sym));
//    // add result variable decl
//    Result.declarations.Add(TVarDecl.Create('result', sym, '', ''));
//  end;
//
  if aIsAbsoluteAsmProc then
    Exit;

  if not aIsMacro then
  begin
    repeat
      if (accept(ttConst)) then
      begin
        constDecls(Result)
      end;

      if accept(ttType) then
      begin
        repeat
          typeDecls(Result);
        until Ftoken.tType <> ttIdent;
      end;

      if (accept(ttVar)) then
      begin
        repeat
          if (Ftoken.tType = ttVar) then
            nextToken;

          varDecls(Result);
        until (Ftoken.tType <> ttIdent) and (Ftoken.tType <> ttVar);
      end;
    until not(Ftoken.tType in [ttConst, ttType, ttVar]);
  end;

  expect(ttAsm);
  asmStart := Flexer.tokenRow;

  while (Ftoken.tType <> ttEOF) and (Ftoken.tType <> ttEnd) do
  begin
    nextToken;
  end;

  if (Ftoken.tType = ttEnd) then
    asmEnd := Flexer.tokenRow - 1;

  nextToken;

  for i := asmStart to asmEnd do
    Result.asmStatements.Add(Flines.Strings[i - 1]);
end;

procedure TParserAST.unitDecls(aProg: TProgram; aUnitName: AnsiString);
var
  unitBlock: TUnitBlock;
  token: TToken;
begin
  unitBlock := TUnitBlock.Create;
  unitBlock.blockName := aUnitName;

  aProg.unitBlocks.Add(unitBlock);

  while (Ftoken.tType <> ttEOF) do
  begin
    repeat
      if accept(ttType) then
      begin
        repeat
          typeDecls(unitBlock);
        until Ftoken.tType <> ttIdent;
      end;

      if accept(ttImport) then
      begin
        parseImport(FprogAST, unitBlock);
      end;

      if (accept(ttConst)) then
      begin
        constDecls(unitBlock)
      end;

      if (accept(ttVar)) then
      begin
        repeat
          if (Ftoken.tType = ttVar) then
            nextToken;

          varDecls(unitBlock);
        until (Ftoken.tType <> ttIdent) and (Ftoken.tType <> ttVar);
      end;

      if (Ftoken.tType in [ttProc, ttFunc, ttMacro]) then
      begin
        token := Ftoken;
        nextToken;
        case token.tType of
          ttProc:
            procDecl(unitBlock);
          ttFunc:
            funcDecl(unitBlock);
          ttMacro:
            macroDecl(unitBlock);
        end;

        expect(ttSemiColon);
      end;
      // else
      // nextToken;
    until not(Ftoken.tType in [ttType, ttConst, ttVar, ttProc, ttFunc, ttMacro,
      ttImport]);
  end;
end;

procedure TParserAST.doUnit;
var
  unitName: AnsiString;
begin
  expect(ttUnit);

  unitName := Ftoken.tValue;
  expect(Ftoken.tType);
  expect(ttSemiColon);

  if accept(ttUses) then
    parseUsesClause(FprogAST);

  unitDecls(FprogAST, unitName);
end;

procedure TParserAST.loadUnit(aProg: TProgram; aUnitName: AnsiString);
var
  unitFileName: AnsiString;
  parserPath: AnsiString;
  parser: TParserAST;
  unitSym: TUnitSymbol;
begin
  unitSym := TUnitSymbol.Create(aUnitName);
  FcurrentScope.insert(unitSym);

  parserPath := ExtractFilePath(ParamStr(0));

  unitFileName := FprojectPath + aUnitName + '.pas';

  if not FileExists(unitFileName) then
    unitFileName := parserPath + 'include\' + aUnitName + '.pas';

  if not FileExists(unitFileName) then
    error('Unit "' + aUnitName + '" not found');

  parser := TParserAST.Create;
  try
    parser.pointerAddress := FpointerAddress;

    parser.projectPath := projectPath;
    if not parser.parseUnit(aProg, FcurrentScope, unitFileName) then
      raise TParseException.Create(parser.errorMsg);

    FpointerAddress := parser.pointerAddress;
  finally
    parser.Free;
  end;
end;

procedure TParserAST.parseUsesClause(aProg: TProgram);
var
  unitName: AnsiString;
  unitSym: TSymbol;
begin
  if (Ftoken.tType in TARGET_TYPE) then
  begin
    if aProg.target <> Ftoken.tType then
      error('Unit "' + Ftoken.tValue + '" does not match target type');
  end;

  unitName := Ftoken.tValue;

  expect(Ftoken.tType);

  unitSym := FcurrentScope.lookup(unitName);

  if (unitSym = nil) then
    // unit not loaded yet so do so
    loadUnit(aProg, unitName);

  while accept(ttComma) do
  begin
    if (Ftoken.tType in TARGET_TYPE) then
    begin
      if aProg.target <> Ftoken.tType then
        error('Machine type unit missmatch');
    end;

    unitName := Ftoken.tValue;

    expect(ttIdent);

    unitSym := FcurrentScope.lookup(unitName);

    if (unitSym = nil) then
      // unit not loaded yet so do so
      loadUnit(aProg, unitName);
  end;

  expect(ttSemiColon);
end;

function foundMacroInfo(aLine: String; var aMacroName: AnsiString;
  var aParamCount: Integer): Boolean;
const
  cHeader = '.macro';

var
  p: Integer;
  lPos, rPos: Integer;
  splitted: TArray<String>;
begin
  Result := False;

  p := Pos(cHeader, aLine);

  if p <= 0 then
    Exit;

  Result := True;

  aLine := Trim(Copy(aLine, p + Length(cHeader), Length(aLine)));

  lPos := Pos('(', aLine);
  rPos := Pos(')', aLine);

  aMacroName := Trim(Copy(aLine, 1, Pos('(', aLine) - 1));

  aParamCount := 0;

  if (rPos > lPos + 1) then
  begin
    aLine := StringReplace(Trim(Copy(aLine, lPos + 1, rPos - lPos - 1)), ' ',
      '', [rfReplaceAll]);

    splitted := aLine.Split([',']);

    aParamCount := Length(splitted);
  end;
end;

procedure TParserAST.extractMacroNamesFromSource(aAsmFileName: AnsiString);
var
  macroName: AnsiString;
  asmFileName: AnsiString;
  parser: TParserAST;
  paramCount: Integer;
  macroSym: TMacroSymbol;
  line: AnsiString;
  i: Integer;
begin
  for i := 0 to Flines.count - 1 do
  begin
    line := Flines.Strings[i];

    if foundMacroInfo(line, macroName, paramCount) then
    begin
      macroSym := TMacroSymbol.Create(macroName, []);
      macroSym.paramCount := paramCount;

      FcurrentScope.insert(macroSym);
    end;
  end;
  { while Ftoken.tType <> ttEOF do begin
    if accept(ttPeriod) then begin
    if accept(ttMacro) then begin
    macroName := Ftoken.tValue;
    writeLn('Found macro: "'+macroName+'"');
    expect(ttIdent);

    paramCount := 0;
    expect(ttLParen);

    if Ftoken.tType <> ttRParen then begin
    Inc(paramCount);
    nextToken;
    while accept(ttComma) do begin
    Inc(paramCount);
    nextToken;
    end;
    end;

    expect(ttRParen);

    macroSym := TMacroSymbol.Create(macroName,[]);
    macroSym.paramCount := paramCount;

    FcurrentScope.insert(macroSym);
    end
    end;

    nextToken;
    end; }
end;

procedure TParserAST.parseImport(aProg: TProgram; aBlock: TBlock);
var
  asmFileName: AnsiString;
  parserPath: AnsiString;
  parser: TParserAST;
begin
  asmFileName := Ftoken.tValue;

  expect(ttString);
  expect(ttSemiColon);

  parserPath := ExtractFilePath(ParamStr(0));

  if not FileExists(asmFileName) then
  begin
    asmFileName := FprojectPath + ExtractFileName(asmFileName);

    if not FileExists(asmFileName) then
    begin
      asmFileName := parserPath + 'include\' + ExtractFileName(asmFileName);

      if not FileExists(asmFileName) then
        error('Import: Source "' + ExtractFileName(asmFileName) +
          '" not found');
    end;
  end;

  parser := TParserAST.Create;
  try
    parser.projectPath := projectPath;
    if not parser.parseAsmFile(aProg, FcurrentScope, asmFileName) then
      raise TParseException.Create(parser.errorMsg);
  finally
    parser.Free;
  end;

  aBlock.declarations.Add(TImportSource.Create(asmFileName));
end;

procedure TParserAST.registerImportFile(aProg: TProgram; aFileName: AnsiString);
var
  parser: TParserAST;
  asmFileName: AnsiString;
begin
  asmFileName := ExtractFilePath(ParamStr(0)) + 'include\' +
    ExtractFileName(aFileName);

  parser := TParserAST.Create;
  try
    parser.projectPath := projectPath;
    if not parser.parseAsmFile(aProg, FcurrentScope, asmFileName) then
      raise TParseException.Create(parser.errorMsg);
  finally
    parser.Free;
  end;

  aProg.imports.Add(asmFileName);
end;

procedure TParserAST.parseImportBinary(aConstName: AnsiString);
var
  importName: AnsiString;
  constType: TSymbol;
  constSym: TConstSymbol;
  procSym: TProcSymbol;
  count: Integer;
  binary: TImportBinary;
  e: TAST;
  fs: TFileStream;
  path: AnsiString;
begin
  {
    *=$a000 "Titlescreen Music"
    titleMusic:
    .import binary "audio\ALBINO_4.sid", $7e // get rid of sid header (skip $7e bytes)
  }

  binary := TImportBinary.Create;
  binary.name := aConstName;
  binary.address := '';
  binary.byteSkip := '0';

  expect(ttLParen);

  binary.fileName := Ftoken.tValue;

  binary.isSid := Pos('.sid', LowerCase(binary.fileName)) > 0;

  path := binary.fileName;

  if not TPath.IsPathRooted(path) then
    path := FprojectPath + path;

  fs := TFileStream.Create(path, fmOpenRead);
  try
    binary.fileSize := fs.size;
  finally
    fs.Free;
  end;

  expect(ttString);

  if accept(ttComma) then
  begin
    e := simplify(simpleExpression);

    if not e.isInteger then
      error('Import binary: byte skip value must be a number');

    binary.byteSkip := TNumber(e).token.tValue;
  end;

  expect(ttRParen);

  if accept(ttAbsolute) then
  begin
    e := simplify(simpleExpression);

    if not e.isInteger then
      error('Import binary: absolute address value must be a number');
    binary.address := TNumber(e).token.tValue;
  end;

  constType := FcurrentScope.lookup('byte');

  constSym := TConstSymbol.Create(aConstName, constType);
  constSym.isArray := True;
  constSym.len := count;

  FcurrentScope.insert(constSym);

  // add .size parameter for this bin
  constSym := TConstSymbol.Create(aConstName + '.size',
    FcurrentScope.lookup('word'));

  FcurrentScope.insert(constSym);

  if binary.isSid then
  begin
    // do sid stuff for this bin
    procSym := TProcSymbol.Create(aConstName + '.init', []);
    procSym.procType := ttImportBinary;
    procSym.procAddress := '';

    FcurrentScope.insert(procSym);

    procSym := TProcSymbol.Create(aConstName + '.play', []);
    procSym.procType := ttImportBinary;
    procSym.procAddress := '';

    FcurrentScope.insert(procSym);
  end;

  if binary.address = '' then
    FprogAST.binImports.Add(binary)
  else
    FprogAST.deferredBinImports.Add(binary);

  expect(ttSemiColon);
end;

function TParserAST.doProgram: TProgram;
var
  progName: AnsiString;
  globalScope: TscopedSymbolTable;
begin
  expect(ttProgram);

  uASM.resetFuncResultReg;

  progName := Ftoken.tValue;

  expect(ttIdent);

  expect(ttSemiColon);

  globalScope := TscopedSymbolTable.Create(progName, 1, nil);
  FcurrentScope := globalScope;

  Result := TProgram.Create(progName, nil, globalScope);

  FprogAST := Result;

  // check for 'target = <target type>;'
  // where <target type> = c64,bbc
  // default to c64

  FprogAST.target := ttC64;

  if accept(ttTarget) then
  begin
    expect(ttEql);
    if not(Ftoken.tType in TARGET_TYPE) then
      error('Unsupported machine type "' + Ftoken.tValue + '"');

    FprogAST.target := Ftoken.tType;
    expect(Ftoken.tType);
    expect(ttSemiColon);
  end;

  FprogAST.loadAddress := '';

  // check for '* = <load addressaddress>;'
  if accept(ttTimes) then
  begin
    // parse load address value for project
    expect(ttEql);
    FprogAST.loadAddress := StrIntToHex(Ftoken.tValue);
    expect(ttIntNum);

    expect(ttSemiColon);
  end;

  registerImportFile(Result, '6502_rtl.asm');
  registerImportFile(Result, 'stack_rtl.asm');

  if (FprogAST.target in MACHINE_ACORN_TYPES) and (FprogAST.loadAddress = '')
  then
    error('This machine type must have include a load address "* = <load addressaddress>;"');

  if accept(ttUses) then
    parseUsesClause(Result);

  Result.block := block(progName, ttByte, False, False);

  expect(ttPeriod);
end;

procedure TParserAST.registerC64Stuff;
begin
  // self.register
end;

function TParserAST.parse(aStream: TStream; aParsingUnit: Boolean): Boolean;
var
  buffer: AnsiString;
begin
  Result := False;

  SetLength(buffer, aStream.size);

  aStream.Seek(0, soFromBeginning);
  aStream.Read(buffer[1], aStream.size);

  Flines.Text := buffer;

  Flexer.init(aStream);

  Ftoken := Flexer.getNextToken;

  try
    FerrorMsg := '';

    FprogAST := doProgram;

    Result := True;
  except
    on e: Exception do
    begin
      FerrorMsg := e.Message;
    end;
  end;
end;

function TParserAST.parse(aFileName: AnsiString; aParsingUnit: Boolean)
  : Boolean;
var
  fs: TFileStream;
begin

  fs := TFileStream.Create(aFileName, fmOpenRead);
  try
    Result := parse(fs, aParsingUnit);
  finally
    fs.Free;
  end;
end;

function TParserAST.parseUnit(aProgram: TProgram; aScope: TscopedSymbolTable;
  aFileName: AnsiString): Boolean;
var
  fs: TFileStream;
  buffer: AnsiString;
begin
  Result := False;

  FparsingUnit := True;
  FprogAST := aProgram;
  FcurrentScope := aScope;

  fs := TFileStream.Create(aFileName, fmOpenRead);
  try
    try
      SetLength(buffer, fs.size);

      fs.Seek(0, soFromBeginning);
      fs.Read(buffer[1], fs.size);

      Flines.Text := buffer;

      Flexer.init(fs);

      Ftoken := Flexer.getNextToken;

      doUnit;

      Result := True;
    except
      on e: Exception do
      begin
        FerrorMsg := aFileName + ': ' + e.Message;
      end;
    end;
  finally
    fs.Free;
  end;
end;

function TParserAST.parseAsmFile(aProgram: TProgram; aScope: TscopedSymbolTable;
  aFileName: AnsiString): Boolean;
var
  fs: TFileStream;
  buffer: AnsiString;
begin
  Result := False;

  FprogAST := aProgram;
  FcurrentScope := aScope;

  fs := TFileStream.Create(aFileName, fmOpenRead);
  try
    try
      SetLength(buffer, fs.size);

      fs.Seek(0, soFromBeginning);
      fs.Read(buffer[1], fs.size);

      Flines.Text := buffer;

      Flexer.init(fs);

      Ftoken := Flexer.getNextToken;

      extractMacroNamesFromSource(aFileName);

      Result := True;
    except
      on e: Exception do
      begin
        FerrorMsg := aFileName + ': ' + e.Message;
      end;
    end;
  finally
    fs.Free;
  end;
end;

end.
