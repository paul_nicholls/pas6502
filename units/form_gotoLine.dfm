object GotoLineDialog: TGotoLineDialog
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = 'Goto Line Number'
  ClientHeight = 87
  ClientWidth = 253
  Color = clBtnFace
  ParentFont = True
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 8
    Top = 8
    Width = 137
    Height = 65
    Shape = bsFrame
  end
  object Label1: TLabel
    Left = 16
    Top = 13
    Width = 84
    Height = 13
    Caption = 'Enter line number'
  end
  object OKBtn: TButton
    Left = 164
    Top = 8
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 164
    Top = 38
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object lineNumber_Edit: TEdit
    Left = 16
    Top = 32
    Width = 81
    Height = 21
    TabOrder = 2
    Text = '0'
    OnKeyPress = lineNumber_EditKeyPress
  end
end
