unit uTargets;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  uToken;

function getTargetName(aTargetType : TTokenType) : AnsiString;

implementation

function getTargetName(aTargetType : TTokenType) : AnsiString;
begin
  case aTargetType of
    ttC64       : Result := 'c64';
    ttC128      : Result := 'c128';
    ttBBC       : Result := 'bbc';
    ttNES       : Result := 'nes';
    ttAppleii   : Result := 'appleii';
    ttAtari8Bit : Result := 'atari8bit';
    ttVic20     : Result := 'vic20';
    ttPET       : Result := 'pet';
  else
    Result := ''; // unknown target
  end;
end;

end.
