unit uCodeGen_6502;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  uAST,usesStringWriter,System.SysUtils,uSymbolTable;

const
  cA_REGISTER  = '_A_REG_';
  cX_REGISTER  = '_X_REG_';
  cY_REGISTER  = '_Y_REG_';
  cAX_REGISTER = '_AX_REG_';
  cXA_REGISTER = '_XA_REG_';

type
  TCodeGenException = class(Exception);

  TCodeGen_6502 = Class
  private
    fPntr : AnsiString;
  protected
    Fwriter         : TsesStringWriter;
    FdoingCondition : Boolean;
    FindentValue    : Integer;
    FcurrentScope   : TScopedSymbolTable;

    procedure error(aMsg : AnsiString);

    function  indent : AnsiString;

    procedure emitCompare_16(left,right : TAST; lower,same,higher : Boolean);

    procedure shiftLeft_0808(src : TAST; shift : Byte; dst : TAST);
    procedure shiftLeft_0816(src : TAST; shift : Byte; dst : TAST);
    procedure shiftLeft_1616(src : TAST; shift : Byte; dst : TAST);
    procedure shiftLeft_08var(src,shift,dst : TAST);

    procedure shiftRight_0808(src : TAST; shift : Byte; dst : TAST);
    procedure shiftRight_0816(src : TAST; shift : Byte; dst : TAST);
    procedure shiftRight_1616(src : TAST; shift : Byte; dst : TAST);
    procedure shiftRight_1608(src : TAST; shift : Byte; dst : TAST);
    procedure shiftRight_2408(src : TAST; shift : Byte; dst : TAST);
    procedure shiftRight_08var(src,shift,dst : TAST);
    procedure shiftRight_24(v : TAST; shift : Byte);

    procedure add_080808(src1,src2,dst: TAST);
    procedure add_080816(src1,src2,dst: TAST);
    procedure add_081616(src1,src2,dst: TAST);
    procedure add_161616(src1,src2,dst: TAST);
    procedure add_nm(src1 : TAST; s1Size : Integer; src2 : TAST; s2Size : Integer; dst : TAST; dSize : Integer);

    procedure sub_080808(src1,src2,dst: TAST);
    procedure sub_080816(src1,src2,dst: TAST);
    procedure sub_081616(src1,src2,dst: TAST);
    procedure sub_161616(src1,src2,dst: TAST);
    procedure sub_nm(src1 : TAST; s1Size : Integer; src2 : TAST; s2Size : Integer; dst : TAST; dSize : Integer);

    procedure absoluteAddress_read0808(src,dst : TAST);
    procedure absoluteAddress_read0816(src,dst : TAST);

    procedure absoluteAddress_write0808(src,dst : TAST);

    procedure writeIndexed_080808(src,dst,index : TAST);
    procedure writeIndexed_081608(src,dst,index : TAST);
    procedure writeIndexed_080816(src,dst,index : TAST);
    procedure writeIndexed_081616(src,dst,index : TAST);
    procedure writeIndexed_161616(src,dst,index : TAST);
    procedure writeIndexed_160816(src,dst,index : TAST);

    procedure writeIndexedPointer_080808(src,dst,index : TAST);
    procedure writeIndexedPointer_081608(src,dst,index : TAST);

    procedure readIndexed_080808(src,dst,index : TAST);
    procedure readIndexed_080816(src,dst,index : TAST);
    procedure readIndexed_081608(src,dst,index : TAST);
    procedure readIndexed_081616(src,dst,index : TAST);
    procedure readIndexed_161608(src,dst,index : TAST);
    procedure readIndexed_161616(src,dst,index : TAST);
    procedure readIndexed_160816(src,dst,index : TAST);

    procedure readIndexedPointer_080808(src,dst,index : TAST);
    procedure readIndexedPointer_081608(src,dst,index : TAST);

    procedure readWriteIndexedPointer_080808(src,srcIndex,dst,dstIndex : TAST);

    procedure loadReg_08(src : TAST; loReg,hiReg : AnsiString);
    procedure loadReg_16(src : TAST; loReg,hiReg : AnsiString);

    procedure copy_nm(src : TAST; sSize : Integer; dst : TAST; dSize : Integer);
    procedure copy_0808(src,dst : TAST);
    procedure copy_0816(src,dst : TAST);
    procedure copy_1616(src,dst : TAST);
    procedure copy_1608(src,dst : TAST);
    procedure copyAddress_1616(src,dst : TAST);

    procedure mul_080808(src1,src2,dst: TAST);
    procedure mul_080816(src1,src2,dst: TAST);

    procedure and_080808(src1,src2,dst: TAST);
    procedure and_081616(src1,src2,dst: TAST);
    procedure and_161616(src1,src2,dst: TAST);
    procedure and_080816(src1,src2,dst: TAST);

    procedure or_080808(src1,src2,dst: TAST);
    procedure or_081616(src1,src2,dst: TAST);
    procedure or_161616(src1,src2,dst: TAST);
    procedure or_080816(src1,src2,dst: TAST);

    procedure xor_080808(src1,src2,dst: TAST);
    procedure xor_081616(src1,src2,dst: TAST);
    procedure xor_161616(src1,src2,dst: TAST);
    procedure xor_080816(src1,src2,dst: TAST);

    procedure neg_0808(right,dst : TAST);
    procedure neg_0816(right,dst : TAST);
    procedure neg_1616(right,dst : TAST);
    procedure neg_2424(right,dst : TAST);

    procedure not_0808(right,dst : TAST);
    procedure not_0816(right,dst : TAST);

    procedure eql_080808(src1,src2,dst: TAST);
    procedure eql_080816(src1,src2,dst: TAST);
    procedure eql_081616(src1,src2,dst: TAST);
    procedure eql_161616(src1,src2,dst: TAST);
    procedure eql_160816(src1,src2,dst: TAST);

    procedure neq_080808(src1,src2,dst: TAST);
    procedure neq_080816(src1,src2,dst: TAST);
    procedure neq_161616(src1,src2,dst: TAST);
    procedure neq_160816(src1,src2,dst: TAST);
    procedure neq_160808(src1,src2,dst: TAST);

    procedure lss_080808(src1,src2,dst: TAST);
    procedure lss_080816(src1,src2,dst: TAST);
    procedure lss_160816(src1,src2,dst: TAST);
    procedure lss_161616(src1,src2,dst: TAST);
    procedure lss_081616(src1,src2,dst: TAST);

    procedure geq_160808(src1,src2,dst: TAST);
    procedure geq_160816(src1,src2,dst: TAST);
    procedure geq_161616(src1,src2,dst: TAST);
    procedure geq_080816(src1,src2,dst: TAST);

    procedure gtr_080808(src1,src2,dst: TAST);
    procedure gtr_080816(src1,src2,dst: TAST);
    procedure gtr_161616(src1,src2,dst : TAST);
    procedure gtr_160816(src1,src2,dst : TAST);
    procedure leq_080808(src1,src2,dst: TAST);
    procedure leq_080816(src1,src2,dst: TAST);
    procedure leq_081616(src1,src2,dst: TAST);
    procedure leq_160816(src1,src2,dst: TAST);
    procedure leq_160808(src1,src2,dst: TAST);

    procedure inc_08(dst: TAST);
    procedure inc_16(dst: TAST);
    procedure inc_24(dst: TAST);
    procedure dec_08(dst: TAST);
    procedure dec_16(dst: TAST);
    procedure dec_24(dst: TAST);
  public
    sourceLine : Integer;

    constructor Create(aWriter : TsesStringWriter; aTempPointer : AnsiString);

    function  arrayIndexCanBe8Bit(valueAST : TAST) : Boolean;
    class function getValue_b0(valueAST : TAST) : AnsiString;
    class function getValue_b1(valueAST : TAST) : AnsiString;
    class function getValue_b2(valueAST : TAST) : AnsiString;
    class function getValue_bn(valueAST : TAST; n : Integer) : AnsiString;

    function  shiftLeft(src,shift,dst : TAST) : Boolean;
    function  shiftRight(src,shift,dst : TAST) : Boolean;
    function  doInc(dst : TAST) : Boolean;
    function  doDec(dst : TAST) : Boolean;
    function  doAdd(left,right,dst : TAST) : Boolean;
    function  doSub(left,right,dst : TAST) : Boolean;
    function  absoluteAddress_write(src,dst : TAST) : Boolean;
    function  absoluteAddress_read(src,dst : TAST) : Boolean;
    function  writeIndexed_value(dst,index,src : TAST) : Boolean;
    function  readIndexed_value(dst,index,src : TAST) : Boolean;
    function  readWriteIndexed_value(dst,dstIndex,src,srcIndex : TAST) : Boolean;
    function  doCopy(src,dst: TAST) : Boolean;
    function  loadRegister(src : TAST; loReg,hiReg : AnsiString) : Boolean;
    function  doMul(left,right,dst : TAST) : Boolean;
    function  doAnd(left,right,dst : TAST) : Boolean;
    function  doOr(left,right,dst : TAST) : Boolean;
    function  doXor(left,right,dst : TAST) : Boolean;
    function  doNeg(right,dst : TAST) : Boolean;
    function  doNot(right,dst : TAST) : Boolean;
    function  doEql(left,right,dst : TAST) : Boolean;
    function  doNeq(left,right,dst : TAST) : Boolean;
    function  doLss(left,right,dst : TAST) : Boolean;
    function  doGtr(left,right,dst : TAST) : Boolean;
    function  doLeq(left,right,dst : TAST) : Boolean;
    function  doGeq(left,right,dst : TAST) : Boolean;

    property indentValue    : Integer                      write FindentValue;
    property doingCondition : Boolean read FdoingCondition write FdoingCondition;
  end;

function  isDerefed(ast : TAST) : Boolean;

implementation

uses
  System.Math,uThreeAddressCode;

function  isDerefed(ast : TAST) : Boolean;
begin
  Result := False;

  if not ast.isVariable then Exit;

  Result := TVariable(ast).deref;
end;

constructor TCodeGen_6502.Create(aWriter : TsesStringWriter; aTempPointer : AnsiString);
begin
  Fwriter := aWriter;

  fPntr := aTempPointer;
end;

procedure TCodeGen_6502.error(aMsg: AnsiString);
begin
  if sourceLine = -1 then
    aMsg := 'Code generation: '+aMsg + '.'
  else
    aMsg := 'Code generation: '+aMsg + ' near line '+IntToStr(sourceLine) + '.';

  aMsg := aMsg + #13#10#13#10 + 'Suggestion: simplify expression(s), or contact support (twitter.com/syntaxerrorsoft) to fix.';

  raise TCodeGenException.Create(aMsg);
end;

function  TCodeGen_6502.arrayIndexCanBe8Bit(valueAST : TAST) : Boolean;
var
  sym   : TSymbol;
  value : AnsiString;
begin
  Result := False;
  value := valueAST.toString;
(*
  sym := FcurrentScope.lookup(value);

  if (sym is TConstSymbol) then begin
    if (TConstSymbol(sym).isArray)   then begin
      Result := (TConstSymbol(sym).len * TConstSymbol(sym).symType.size) <= 256;
      Exit;
    end;
  end;

  if (sym is TVarSymbol) then begin
    if TVarSymbol(sym).address <> '' then begin
      Result := False;
      Exit;
    end;

    if TVarSymbol(sym).isArray then begin
      Result := TVarSymbol(sym).arraySize <= 256;
      Exit;
    end;
*)
end;

class function TCodeGen_6502.getValue_b0(valueAST : TAST) : AnsiString;
var
  value : AnsiString;
begin
  value := valueAST.toString;

  if value[1] = '!' then
  // read/write from number address
    Result := Copy(value,2,Length(value))
  else
  if value[1] = '^' then
  // is a pointer value so return the pointer reference instead
    Result := '('+Copy(value,2,Length(value))+')'
  else
  if value[1] = '@' then
  // is @value so return the address instead
    Result := '<'+Copy(value,2,Length(value))
  else
  if (value[1] in ['-','0'..'9','%','$']) then
  // immediate value constant
    Result := '#<'+value
  else
  // read/write from variable address
    Result := value + ' + 0';
end;

class function TCodeGen_6502.getValue_b1(valueAST : TAST) : AnsiString;
var
  value : AnsiString;
begin
  value := valueAST.toString;

  if value[1] = '!' then
    Result := Copy(value,2,Length(value))
  else
  if value[1] = '@' then
  // is @value so return the address instead
    Result := '>'+Copy(value,2,Length(value))
  else
  if (value[1] in ['0'..'9','%','$']) then
  // immediate value constant
    Result := '#>'+value
  else
  // read/write from variable address
    Result := value + ' + 1';
end;

class function TCodeGen_6502.getValue_b2(valueAST : TAST) : AnsiString;
var
  value : AnsiString;
begin
  value := valueAST.toString;

  if (value[1] in ['0'..'9','%','$']) then
  // immediate value constant
    Result := '('+value+' >> 16)'
  else
  // read/write from variable address
    Result := value + ' + 2';
end;

class function TCodeGen_6502.getValue_bn(valueAST : TAST; n : Integer) : AnsiString;
var
  value : AnsiString;
begin
  value := valueAST.toString;

  if n = 0 then begin
  // low byte <
    if value[1] = '!' then
    // read/write from number address
      Result := Copy(value,2,Length(value))
    else
    if value[1] = '^' then
    // is a pointer value so return the pointer reference instead
      Result := '('+Copy(value,2,Length(value))+')'
    else
    if value[1] = '@' then
    // is @value so return the address instead
      Result := '<'+Copy(value,2,Length(value))
    else
    if (value[1] in ['-','0'..'9','%','$']) then
    // immediate value constant
      Result := '#<'+value
    else
    // read/write from variable address
      Result := value + ' + '+IntToStr(n);
  end
  else
  // Medium byte  >
  if n = 1 then begin
    if value[1] = '!' then
      Result := Copy(value,2,Length(value))
    else
    if value[1] = '@' then
    // is @value so return the address instead
      Result := '>'+Copy(value,2,Length(value))
    else
    if (value[1] in ['0'..'9','%','$']) then
    // immediate value constant
      Result := '#>'+value
    else
    // read/write from variable address
      Result := value + ' + '+IntToStr(n);
  end
  else begin
  // higher bytes
    if (value[1] in ['0'..'9','%','$']) then
    // immediate value constant
      Result := '#('+value+' >> '+IntToStr(n * 8) + ')'
    else
    // read/write from variable address
      Result := value + ' + '+IntToStr(n);
  end;
end;

function  TCodeGen_6502.indent : AnsiString;
begin
  Result := StringOfChar(' ',FindentValue);
end;

function  TCodeGen_6502.shiftLeft(src,shift,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s << %s',[dst.toString,src.toString,shift.toString]);

  if (dst.size = 1) and (shift.isInteger) then
    shiftLeft_0808(src,TNumber(shift).value and $ff,dst)
  else
  if (dst.size = 1) and (shift.isVariable) then
    shiftLeft_08var(src,shift,dst)
  else
  if (src.size = 1) and (shift.isInteger) and (dst.size = 2) then
    shiftLeft_0816(src,TNumber(shift).value and $ff,dst)
  else
  if (src.size = 2) and (shift.isInteger) and (dst.size = 2) then
    shiftLeft_1616(src,TNumber(shift).value and $ff,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) shl %s(%d-bit)" not supported',[dst.toString,dst.size*8,src.toString,src.size*8,shift.toString,shift.size*8]));

  Result := True;
end;

function  TCodeGen_6502.shiftRight(src,shift,dst : TAST) : Boolean;
begin
  Result := False;

//  writeLn(Format('// %s := %s >> %s',[dst.toString,src.toString,shift.toString]));
  Fwriter.writeLn(indent+'// %s := %s >> %s',[dst.toString,src.toString,shift.toString]);

  if (dst.size = 1) and (shift.isInteger) and (src.size = 1) then
    shiftRight_0808(src,TNumber(shift).value and $ff,dst)
  else
  if (dst.size = 1) and (shift.isVariable) then
    shiftRight_08var(src,shift,dst)
  else
  if (src.size = 1) and (shift.isInteger) and (dst.size = 2) then
    shiftRight_0816(src,TNumber(shift).value and $ff,dst)
  else
  if (src.size = 2) and (shift.isInteger) and (dst.size = 2) then
    shiftRight_1616(src,TNumber(shift).value and $ff,dst)
  else
  if (src.size = 2) and (shift.isInteger) and (dst.size = 1) then
    shiftRight_1608(src,TNumber(shift).value and $ff,dst)
  else
  if (src.size = 3) and (shift.isInteger) and (src.toString = dst.toString) then
    shiftRight_24(src,TNumber(shift).value)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) shr %s(%d-bit)" not supported',[dst.toString,dst.size*8,src.toString,src.size*8,shift.toString,shift.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doInc(dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// inc %s',[dst.toString]);

  if dst.size = 1 then
    inc_08(dst)
  else
  if dst.size = 2 then
    inc_16(dst)
  else
  if dst.size = 3 then
    doAdd(dst,TNumber.Create(1),dst)
//    inc_24(dst.toString)
  else
    error(Format('"inc %s(%d-bit)" not supported',[dst.toString,dst.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doDec(dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// dec %s',[dst.toString]);

  if dst.size = 1 then
    dec_08(dst)
  else
  if dst.size = 2 then
    dec_16(dst)
  else
  if dst.size = 3 then
    doSub(dst,TNumber.Create(1),dst)
//    dec_24(dst.toString)
  else
    error(Format('"dec %s(%d-bit)" not supported',[dst.toString,dst.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doAdd(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s + %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 3) then
    add_nm(left,left.size,right,right.size,dst,dst.size)
  else
  if (dst.size = 1) then
    add_080808(left,right,dst)
  else
  if (left.size = 1) and (right.size = 1) and (dst.size = 2) then
    add_080816(left,right,dst)
  else
  if (left.size = 2) and (right.size = 2) and (dst.size = 2) then
    add_161616(left,right,dst)
  else
  if (left.size = 1) and (right.size = 2) and (dst.size = 2) then
    add_081616(left,right,dst)
  else
  if (left.size = 2) and (right.size = 1) and (dst.size = 2) then
    add_081616(right,left,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) + %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doSub(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s - %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 3) then
    sub_nm(left,left.size,right,right.size,dst,dst.size)
  else
  if (dst.size = 1) then
    sub_080808(left,right,dst)
  else
  if (left.size = 1) and (right.size = 1) and (dst.size = 2) then
    sub_080816(left,right,dst)
  else
  if (left.size = 2) and (right.size = 2) and (dst.size = 2) then
    sub_161616(left,right,dst)
  else
  if (left.size = 1) and (right.size = 2) and (dst.size = 2) then
    sub_081616(left,right,dst)
  else
  if (left.size = 2) and (right.size = 1) and (dst.size = 2) then
    sub_081616(right,left,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) - %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.absoluteAddress_write(src,dst : TAST) : Boolean;
begin
  if (dst.size = 1) then
    absoluteAddress_write0808(src,dst)
   else
    error(Format('"%s(%d-bit) := %s(%d-bit)" not supported',[dst.toString,src.toString,src.size*8]));


  Result := True;
end;

function  TCodeGen_6502.absoluteAddress_read(src,dst : TAST) : Boolean;
begin
  if (dst.size = 1) then
    absoluteAddress_read0808(src,dst)
  else
  if (dst.size = 2) then
    absoluteAddress_read0816(src,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit)" not supported',[dst.toString,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen_6502.writeIndexed_value(dst,index,src : TAST) : Boolean;
var
  c : AnsiString;
  v : Integer;
begin
  Result := False;

  if (src.size = 1) and (src.isString) then begin
    v := charToCBMscreenCode(src.toString[1]);
    src := TNumber.Create(v);
  end;

  if (dst.size = 1) and (index.size = 1) and isDerefed(dst) then begin
//    c := IntToStr(charToCBMscreenCode(src.toString[1]));
    writeIndexedPointer_080808(src,dst,index);
  end
  else
  if (dst.size = 1) and (index.size = 1) and isDerefed(dst) then
    writeIndexedPointer_080808(src,dst,index)
  else
  if (dst.size = 2) and (index.size = 1) and isDerefed(dst) then begin
//    c := IntToStr(charToCBMscreenCode(src.toString[1]));
    writeIndexedPointer_081608(src,dst,index);
  end
  else
  if (dst.size = 2) and (index.size = 1) and isDerefed(dst) then
    writeIndexedPointer_081608(src,dst,index)
  else
{  if (dst.size = 1) and (index.size = 2) and (src.isString) then begin
//    c := IntToStr(charToCBMscreenCode(src.toString[1]));
    writeIndexed_080816(c,dst.toString,index.toString);
  end
  else}
  if (dst.size = 1) and (index.size = 1) and (src.size = 1) then
    writeIndexed_080808(src,dst,index)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 2) then
    writeIndexed_160816(src,dst,index)
  else
  if (dst.size = 2) and (index.size = 1) and (src.size = 1) then
    writeIndexed_081608(src,dst,index)
  else
  if (dst.size = 1) and (index.size = 1) and (src.size = 2) then
    writeIndexed_080808(src,dst,index)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 1) then
    writeIndexed_080816(src,dst,index)
  else
  if (dst.size = 2) and (index.size = 2) and (src.size = 1) then
    writeIndexed_081616(src,dst,index)
  else
  if (dst.size = 2) and (index.size = 2) and (src.size = 2) then
    writeIndexed_161616(src,dst,index)
  else
    error(Format('"%s(%d-bit)[%s(%d-bit)] := %s(%d-bit)" not supported',[dst.toString,dst.size*8,index.toString,index.size*8,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen_6502.readIndexed_value(dst,index,src : TAST) : Boolean;
var
  c : AnsiString;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s[%s]',[dst.toString,src.toString,index.toString]);

  if (dst.size = 1) and (index.size = 1) and isDerefed(src) then
    readIndexedPointer_080808(src,dst,index)
  else
  if (dst.size = 2) and (index.size = 1) and isDerefed(src) then
    readIndexedPointer_081608(src,dst,index)
  else
  if (dst.size = 1) and (index.size = 1) and (src.size in [0,1]) then
    readIndexed_080808(src,dst,index)
  else
  if (dst.size = 2) and (index.size = 1) and (src.size in [0,1]) then
    readIndexed_081608(src,dst,index)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 1) then
    readIndexed_080816(src,dst,index)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 2) then
    readIndexed_160816(src,dst,index)
  else
  if (dst.size = 2) and (index.size = 2) and (src.size = 1) then
    readIndexed_081616(src,dst,index)
  else
  if (dst.size = 2) and (index.size in [1,2]) and (src.size = 2) then
    readIndexed_161616(src,dst,index)
  else
{  if (dst.size = 2) and (index.size = 1) and (src.size = 2) then
    readIndexed_161608(src.toString,dst.toString,index.toString)
  else}
    error(Format('"%s(%d-bit) := %s(%d-bit)[%s(%d-bit)]" not supported',[dst.toString,dst.size*8,src.toString,src.size*8,index.toString,index.size*8]));

  Result := True;
end;

function  TCodeGen_6502.readWriteIndexed_value(dst,dstIndex,src,srcIndex : TAST) : Boolean;
var
  c : AnsiString;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s[%s] := %s[%s]',[dst.toString,dstIndex.toString,src.toString,srcIndex.toString]);

  if isDerefed(dst) and (dstIndex.size = 1) and isDerefed(src) and (srcIndex.size = 1) then
    readWriteIndexedPointer_080808(src,srcIndex,dst,dstIndex)
  else
    error(Format('"%s(%d-bit)^[%s(%d-bit)] := %s(%d-bit)^[%s(%d-bit)]" not supported',[dst.toString,dst.size*8,dstIndex.toString,dstIndex.size*8,src.toString,src.size*8,srcIndex.toString,srcIndex.size*8]));

  Result := True;
end;

function  isPointer(v : TAST) : boolean;
begin
  Result := False;

  if not v.isVariable then exit;

  Result := TVariable(v).isVarAddress;
end;

function  TCodeGen_6502.doCopy(src,dst: TAST) : Boolean;
var
  c      : AnsiString;
  srcStr : AnsiString;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s',[dst.toString,src.toString]);

  if (src.isString) then begin
  // convert value from string to a screencode
    src := TNumber.Create(charToCBMscreenCode(src.toString[1]));
  end;

  if (dst.size = 2) and isPointer(src) then begin
    copyAddress_1616(src,dst);
  end
  else
  if (dst.size = 3) then begin
    copy_nm(src,src.size,dst,dst.size);
  end
  else
  if (dst.size = 1) and (src.size = 1) then begin
    copy_0808(src,dst);
  end
  else
  if (dst.size = 1) and (src.size = 2) then begin
    copy_1608(src,dst);
  end
  else
  if (dst.size = 2) and (src.size = 1) then begin
    copy_0816(src,dst);
  end
  else
  if (dst.size = 2) and (src.size = 2) then begin
    copy_1616(src,dst);
  end
  else
    error(Format('"%s(%d-bit) := %s(%d-bit)" not supported',[dst.toString,dst.size*8,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen_6502.loadRegister(src : TAST; loReg,hiReg : AnsiString) : Boolean;
begin
  Result := False;

  if (src.size = 1) or (hiReg = '') then
    loadReg_08(src,loReg,hiReg)
  else
  if (src.size = 2) and (hiReg <> '') then
    loadReg_16(src,loReg,hiReg)
  else
    error(Format('Register(s) %s,%s := "%s(%d-bit)" not supported',[loReg,hiReg,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doMul(left,right,dst : TAST) : Boolean;
var
  v : Byte;
begin
  if (left.size = 1) and (left.isString) then begin
    v := charToCBMscreenCode(left.toString[1]);
    left := TNumber.Create(v);
  end;

  if (right.size = 1) and (right.isString) then begin
    v := charToCBMscreenCode(right.toString[1]);
    right := TNumber.Create(v);
  end;

  if (left.size = 1) and (right.size = 1) and (dst.size = 1) then
    mul_080808(left,right,dst)
  else
  if (left.size = 1) and (right.size = 1) and (dst.size = 2) then
    mul_080816(left,right,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) * %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doAnd(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s & %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then
    and_080808(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 2) then
    and_081616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    and_081616(right,left,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    and_161616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    and_080816(left,right,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) & %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doOr(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s | %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then
    or_080808(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 2) then
    or_081616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    or_081616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    or_161616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    or_080816(left,right,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) | %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doXor(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s ^ %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then //and (left.size = 1) and (right.size = 1) then
    xor_080808(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    xor_161616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    xor_080816(left,right,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) ^ %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doNeg(right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := neg %s',[dst.toString,right.toString]);

  if (dst.size = 1) then
    neg_0808(right,dst)
  else
  if (dst.size = 2) and (right.size = 1) then
    neg_0816(right,dst)
  else
  if (dst.size = 2) and (right.size = 2) then
    neg_1616(right,dst)
  else
  if (dst.size = 3) and (right.size = 3) then
    neg_2424(right,dst)
  else
    error(Format('"%s(%d-bit) := neg %s(%d-bit)" not supported',[dst.toString,dst.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doNot(right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := not %s',[dst.toString,right.toString]);

  if (dst.size = 1) and (right.size = 1) then
    not_0808(right,dst)
  else
  if (dst.size = 2) and (right.size = 1) then
    not_0816(right,dst)
  else
    error(Format('"%s(%d-bit) := not %s(%d-bit)" not supported',[dst.toString,dst.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doEql(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s = %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and ((left.size = 1) or (right.size = 1)) then
    eql_080808(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    eql_080816(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    eql_161616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    eql_160816(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 2) then
    eql_081616(left,right,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) = %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doNeq(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s <> %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and (left.size = 2) and (right.size = 1) then
    neq_160808(left,right,dst)
  else
//  if (dst.size = 1) and ((left.size = 1) or (right.size = 1)) then
//    neq_080808(left,right,dst)
//  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    neq_080816(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    neq_161616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    neq_160816(left,right,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) <> %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doLss(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s < %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and ((left.size = 1) or (right.size = 1)) then
    lss_080808(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    lss_080816(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    lss_161616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 2) then
    lss_081616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    lss_160816(left,right,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) < %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doGtr(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s > %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and (left.size = 1) and (right.size = 1) then
    gtr_080808(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    gtr_080816(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    gtr_161616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    gtr_160816(left,right,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) > %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doLeq(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s <= %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    leq_080816(left,right,dst)
  else
  if (dst.size = 1) and (left.size = 2) and (right.size = 1) then
    leq_160808(left,right,dst)
  else
  if (dst.size = 1) and (left.size = 1) and (right.size = 1) then
    leq_080808(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    leq_160816(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 2) then
    leq_081616(left,right,dst)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) <= %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doGeq(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s >= %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and (left.size = 2) and (right.size = 1) then
    geq_160808(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    geq_160816(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    geq_161616(left,right,dst)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    geq_080816(left,right,dst)
  else
  error(Format('"%s(%d-bit) := %s(%d-bit) >= %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

procedure TCodeGen_6502.emitCompare_16(left,right : TAST; lower,same,higher : Boolean);
// leaves result in A reg (0 = false, 255 = true)
begin
  Fwriter.writeLn(indent + 'lda ' + getValue_b1(left));
  Fwriter.writeLn(indent + 'cmp ' + getValue_b1(right));
  Fwriter.writeLn(indent + 'bne !+');
  Fwriter.writeLn(indent + 'lda ' + getValue_b0(left));
  Fwriter.writeLn(indent + 'cmp ' + getValue_b0(right));
  Fwriter.writeLn('!:');

  if lower then
    Fwriter.writeLn(indent + 'bcc !+ // is <')
  else
    Fwriter.writeLn(indent + 'bcc !isFalse+ // is < (false)');

  if higher then
    Fwriter.writeLn(indent + 'bne !+ // is >')
  else
    Fwriter.writeLn(indent + 'bne !isFalse+ // is > (false)');

  if same then
    Fwriter.writeLn(indent + 'beq !+ // is =')
  else
    Fwriter.writeLn(indent + 'beq !isFalse+ // is = (false)');

  Fwriter.writeLn('!isTrue:');
  Fwriter.writeLn(indent + 'lda #$ff');
  Fwriter.writeLn(indent + 'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent + 'lda #$00');
  Fwriter.writeLn('!:');
end;

procedure TCodeGen_6502.shiftLeft_0808(src : TAST; shift : Byte; dst : TAST);
// 8     8      8
// dst := src << shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'asl');
  end;

  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.shiftLeft_0816(src : TAST; shift : Byte; dst : TAST);
// 16     8      8
// dst := src << shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b0(src),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_b1(dst)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'asl %s; rol %s',[getValue_b0(dst),getValue_b1(dst)]);
  end;
end;

procedure TCodeGen_6502.shiftLeft_1616(src : TAST; shift : Byte; dst : TAST);
// 16     16     16
// dst := src << shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b0(src),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b1(src),getValue_b1(dst)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'asl %s; rol %s',[getValue_b0(dst),getValue_b1(dst)]);
  end;
end;

procedure TCodeGen_6502.shiftLeft_08var(src,shift,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'ldx %s',[getValue_b0(shift)]);
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'asl');
  Fwriter.writeLn(indent+'dex');
  Fwriter.writeLn(indent+'bne !-');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.shiftRight_0808(src : TAST; shift : Byte; dst : TAST);
// 8      8      8
// dst := src >> shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'lsr');
  end;

  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.shiftRight_0816(src : TAST; shift : Byte; dst : TAST);
// 16     8      8
// dst := src >> shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b0(src),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_b1(dst)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'lsr %s; ror %s',[getValue_b1(dst),getValue_b0(dst)]);
  end;
end;

procedure TCodeGen_6502.shiftRight_1616(src : TAST; shift : Byte; dst : TAST);
// 16     16      8
// dst := src >> shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b0(src),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b1(src),getValue_b1(dst)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'lsr %s; ror %s',[getValue_b1(dst),getValue_b0(dst)]);
  end;
end;

procedure TCodeGen_6502.shiftRight_24(v : TAST; shift : Byte);
// v >> shift
var
  i : Integer;
begin
  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'lsr %s; ror %s; ror %s',[getValue_b2(v),getValue_b1(v),getValue_b0(v)]);
  end;
end;

procedure TCodeGen_6502.shiftRight_1608(src : TAST; shift : Byte; dst : TAST);
const cTEMP = '_TEMP_';
// 8      16      8
// dst := src >> shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s+0',[getValue_b0(src),cTEMP]);
  Fwriter.writeLn(indent+'lda %s; sta %s+1',[getValue_b1(src),cTEMP]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'lsr %s+1; ror %s+0',[cTEMP,cTEMP]);
  end;

  Fwriter.writeLn(indent+'lda %s; sta %s',[cTEMP,getValue_b0(dst)]);
end;

procedure TCodeGen_6502.shiftRight_2408(src : TAST; shift : Byte; dst : TAST);
const cTEMP = '_TEMP_';
// 8      24      8
// dst := src >> shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s+0',[getValue_b0(src),cTEMP]);
  Fwriter.writeLn(indent+'lda %s; sta %s+1',[getValue_b1(src),cTEMP]);
  Fwriter.writeLn(indent+'lda %s; sta %s+2',[getValue_b2(src),cTEMP]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'lsr %s+1; ror %s+0',[cTEMP,cTEMP]);
  end;

  Fwriter.writeLn(indent+'lda %s; sta %s',[cTEMP,getValue_b0(dst)]);
end;

procedure TCodeGen_6502.shiftRight_08var(src,shift,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'ldx %s',[getValue_b0(shift)]);
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'lsr');
  Fwriter.writeLn(indent+'dex');
  Fwriter.writeLn(indent+'bne !-');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.add_nm(src1 : TAST; s1Size : Integer; src2 : TAST; s2Size : Integer; dst : TAST; dSize : Integer);
var
  i,s,o : Integer;
  v   : TAST;
begin
  s := 0;
  o := 0;
  Fwriter.writeLn(indent+'clc');

  for i := 1 to dSize do begin
    if i <= s1Size then
      Fwriter.write(indent+'lda '+getValue_bn(src1,o)+'; ')
    else
      Fwriter.write(indent+'lda #0; ');

    if i <= s2Size then
      Fwriter.write('adc '+getValue_bn(src2,o)+'; ')
    else
      Fwriter.write('adc #0; ');

    Fwriter.writeLn('sta '+getValue_bn(dst,o));
    Inc(o);
    s := s + 8;
  end;
end;

procedure TCodeGen_6502.sub_nm(src1 : TAST; s1Size : Integer; src2 : TAST; s2Size : Integer; dst : TAST; dSize : Integer);
var
  i,s,o : Integer;
  v   : TAST;
begin
  s := 0;
  o := 0;
  Fwriter.writeLn(indent+'sec');

  for i := 1 to dSize do begin
    if i <= s1Size then
      Fwriter.write(indent+'lda '+getValue_bn(src1,o)+'; ')
    else
      Fwriter.write(indent+'lda #0; ');

    if i <= s2Size then
      Fwriter.write('sbc '+getValue_bn(src2,o)+'; ')
    else
      Fwriter.write('sbc #0; ');

    Fwriter.writeLn('sta '+getValue_bn(dst,o));
    Inc(o);
    s := s + 8;
  end;
end;

procedure TCodeGen_6502.add_161616(src1,src2,dst: TAST);
begin
  if src2.toString = cA_REGISTER then begin
  // add src1 to src2 to preserve A-reg value
    Fwriter.writeLn(indent+'clc');
    Fwriter.writeLn(indent+'adc %s',[getValue_b0(src1)]);
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);

    Fwriter.writeLn(indent+'lda #0');
    Fwriter.writeLn(indent+'adc %s',[getValue_b1(src1)]);
    Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
  end
  else
  if src1.toString = cA_REGISTER then begin
  // add src2 to src1 to preserve A-reg value
    Fwriter.writeLn(indent+'clc');
    Fwriter.writeLn(indent+'adc %s',[getValue_b0(src2)]);
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);

    Fwriter.writeLn(indent+'lda #0');
    Fwriter.writeLn(indent+'adc %s',[getValue_b1(src2)]);
    Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
  end
  else begin
    Fwriter.writeLn(indent+'clc');
    Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
    Fwriter.writeLn(indent+'adc %s',[getValue_b0(src2)]);
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);

    Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
    Fwriter.writeLn(indent+'adc %s',[getValue_b1(src2)]);
    Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
  end;
end;

procedure TCodeGen_6502.add_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'clc; lda %s; adc %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
//  Fwriter.writeLn(indent+'adc %s',[]);
//  Fwriter.writeLn(indent+'sta %s',[]);
end;

procedure TCodeGen_6502.add_080816(src1,src2,dst: TAST);
begin
  if src2.toString = cA_REGISTER then begin
  // src2 = A-reg so reverse addition to preserve A-reg
    Fwriter.writeLn(indent+'clc');
    Fwriter.writeLn(indent+'adc %s',[getValue_b0(src1)]);
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
    Fwriter.writeLn(indent+'lda #0');
    Fwriter.writeLn(indent+'adc #0');
    Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
  end
  else
  if src1.toString = cA_REGISTER then begin
    Fwriter.writeLn(indent+'clc');
    Fwriter.writeLn(indent+'adc %s',[getValue_b0(src2)]);
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
    Fwriter.writeLn(indent+'lda #0');
    Fwriter.writeLn(indent+'adc #0');
    Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
  end
  else begin
    Fwriter.writeLn(indent+'clc');
    Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
    Fwriter.writeLn(indent+'adc %s',[getValue_b0(src2)]);
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
    Fwriter.writeLn(indent+'lda #0');
    Fwriter.writeLn(indent+'adc #0');
    Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
  end;
end;

procedure TCodeGen_6502.add_081616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'adc %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);

  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'adc %s',[getValue_b1(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.sub_161616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);

  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_b1(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.sub_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.sub_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.sub_081616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);

  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_b1(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.absoluteAddress_read0808(src,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda '+getValue_b0(src));
  Fwriter.writeLn(indent+'sta '+getValue_b0(dst));
end;

procedure TCodeGen_6502.absoluteAddress_read0816(src,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda '+getValue_b0(src));
  Fwriter.writeLn(indent+'sta '+getValue_b0(dst));
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta '+getValue_b1(dst));
end;

procedure TCodeGen_6502.absoluteAddress_write0808(src,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda '+getValue_b0(src));
  Fwriter.writeLn(indent+'sta '+getValue_b0(dst));
end;

procedure TCodeGen_6502.writeIndexed_080808(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta %s,y',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.writeIndexed_081608(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta %s,y',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'iny');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta %s,y',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.writeIndexed_080816(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[dst.toString,getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[dst.toString,getValue_b1(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta ($fb),y');
end;

procedure TCodeGen_6502.writeIndexed_081616(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[dst.toString,getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[dst.toString,getValue_b1(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta ($fb),y');
  Fwriter.writeLn(indent+'iny');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta ($fb),y');
end;

procedure TCodeGen_6502.writeIndexed_161616(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[dst.toString,getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[dst.toString,getValue_b1(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta ($fb),y');
  Fwriter.writeLn(indent+'iny');
  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src)]);
  Fwriter.writeLn(indent+'sta ($fb),y');
end;

procedure TCodeGen_6502.writeIndexed_160816(src: TAST; dst: TAST; index: TAST);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[dst.toString,getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[dst.toString,getValue_b1(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta ($fb),y');
end;

procedure TCodeGen_6502.writeIndexedPointer_080808(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta %s,y',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.writeIndexedPointer_081608(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta %s,y',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.readWriteIndexedPointer_080808(src,srcIndex,dst,dstIndex : TAST);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_b0(srcIndex)]);
  Fwriter.writeLn(indent+'lda %s,y',[getValue_b0(src)]);
  if srcIndex <> dstIndex then
  // only add second index if they are different to save memory/cycles
    Fwriter.writeLn(indent+'ldy %s',[getValue_b0(dstIndex)]);
  Fwriter.writeLn(indent+'sta %s,y',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.readIndexedPointer_080808(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda %s,y',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.readIndexedPointer_081608(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda %s,y',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.readIndexed_080808(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy %s; lda %s,y; sta %s',[getValue_b0(index),getValue_b0(src),getValue_b0(dst)]);
//  Fwriter.writeLn(indent+'ldy %s',[getValue_b0(index)]);
//  Fwriter.writeLn(indent+'lda %s,y',[getValue_b0(src)]);
//  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.readIndexed_081608(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda %s,y',[getValue_b0(src)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;
//CodeGen: "_r_0(16-bit) := screenRow(16-bit)[y(8-bit)]" not supported
procedure TCodeGen_6502.readIndexed_161608(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy %s',[index]);
  Fwriter.writeLn(indent+'lda %s,y; sta %s',[getValue_b0(src),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0;   sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.readIndexed_080816(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[src.toString,getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[src.toString,getValue_b1(index)]);
  Fwriter.writeLn(indent+'lda ($fb),y');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.readIndexed_081616(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[src.toString,getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[src.toString,getValue_b1(index)]);
  Fwriter.writeLn(indent+'lda ($fb),y');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.readIndexed_161616(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[src.toString,getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[src.toString,getValue_b1(index)]);
  Fwriter.writeLn(indent+'lda ($fb),y');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'iny');
  Fwriter.writeLn(indent+'lda ($fb),y');
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.readIndexed_160816(src,dst,index : TAST);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[src.toString,getValue_b0(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[src.toString,getValue_b1(index)]);
  Fwriter.writeLn(indent+'lda ($fb),y');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.loadReg_08(src : TAST; loReg,hiReg : AnsiString);
begin
  Fwriter.writeLn(indent+'ld%s %s',[loReg,getValue_b0(src)]);
end;

procedure TCodeGen_6502.loadReg_16(src : TAST; loReg,hiReg : AnsiString);
begin
  Fwriter.writeLn(indent+'ld%s %s',[loReg,getValue_b0(src)]);
  Fwriter.writeLn(indent+'ld%s %s',[hiReg,getValue_b1(src)]);
end;

procedure TCodeGen_6502.copy_nm(src : TAST; sSize : Integer; dst : TAST; dSize : Integer);
var
  i,s,o : Integer;
  v   : TAST;
begin
  s := 0;
  o := 0;
  for i := 1 to dSize do begin
    if i <= sSize then begin
      if (i = 1) then begin
        if (src.toString = cX_REGISTER) then
          Fwriter.write(indent+'txa; ')
        else
        if (src.toString = cY_REGISTER) then
          Fwriter.write(indent+'tya; ')
        else
        if (src.toString = cA_REGISTER) then begin
        // write nothing as already loaded
          Fwriter.write(indent);
        end
        else
          Fwriter.write(indent+'lda '+getValue_bn(src,o)+'; ')
      end
      else
      // bytes > 1
        Fwriter.write(indent+'lda '+getValue_bn(src,o)+'; ');
    end
    else
      Fwriter.write(indent+'lda #0; ');

    Fwriter.writeLn('sta '+getValue_bn(dst,o));
    Inc(o);
    s := s + 8;
  end;

end;

procedure TCodeGen_6502.copy_0808(src,dst : TAST);
begin
  if (src.toString = cA_REGISTER) then
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)])
  else
  if (src.toString = cX_REGISTER) then
    Fwriter.writeLn(indent+'stx %s',[getValue_b0(dst)])
  else
  if (src.toString = cY_REGISTER) then
    Fwriter.writeLn(indent+'sty %s',[getValue_b0(dst)])
  else
    Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b0(src),getValue_b0(dst)]);
end;

procedure TCodeGen_6502.copy_0816(src,dst : TAST);
begin
  if (src.toString = cA_REGISTER) then begin
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
    Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_b1(dst)]);
  end
  else
  if (src.toString = cX_REGISTER) then begin
    Fwriter.writeLn(indent+'stx %s',[getValue_b0(dst)]);
    Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_b1(dst)]);
  end
  else
  if (src.toString = cY_REGISTER) then begin
    Fwriter.writeLn(indent+'sty %s',[getValue_b0(dst)]);
    Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_b1(dst)]);
  end
  else begin
    Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b0(src),getValue_b0(dst)]);
    Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_b1(dst)]);
  end;
end;

procedure TCodeGen_6502.copy_1608(src,dst : TAST);
begin
  if (src.toString = cAX_REGISTER) then
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)])
  else
    Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b0(src),getValue_b0(dst)]);
end;

procedure TCodeGen_6502.copy_1616(src,dst : TAST);
begin
  if (src.toString = cAX_REGISTER) then begin
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
    Fwriter.writeLn(indent+'stx %s',[getValue_b1(dst)]);
  end
  else begin
    Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b0(src),getValue_b0(dst)]);
    Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_b1(src),getValue_b1(dst)]);
  end;
end;

procedure TCodeGen_6502.copyAddress_1616(src,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda #%s; sta %s',[getValue_b0(src),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #%s; sta %s',[getValue_b1(src),getValue_b1(dst)]);
end;

procedure TCodeGen_6502.mul_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; ldx %s; jsr mul_8x8; lda mul_product_lo; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
end;

procedure TCodeGen_6502.mul_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; ldx %s; jsr mul_8x8; lda mul_product_lo; sta %s; lda mul_product_hi; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst),getValue_b1(dst)]);
end;

procedure TCodeGen_6502.and_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; and %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
end;

procedure TCodeGen_6502.and_081616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; and %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0; and %s; sta %s',[getValue_b1(src2),getValue_b1(dst)]);
end;

procedure TCodeGen_6502.and_161616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; and %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda %s; and %s; sta %s',[getValue_b1(src1),getValue_b1(src2),getValue_b1(dst)]);
end;

procedure TCodeGen_6502.and_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; and %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.or_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; ora %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
end;

procedure TCodeGen_6502.or_081616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; ora %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0; ora %s; sta %s',[getValue_b1(src2),getValue_b1(dst)]);
end;

procedure TCodeGen_6502.or_161616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; ora %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda %s; ora %s; sta %s',[getValue_b1(src1),getValue_b1(src2),getValue_b1(dst)]);
end;

procedure TCodeGen_6502.or_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; ora %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0; ora %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.xor_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; eor %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
end;

procedure TCodeGen_6502.xor_081616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; eor %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0; eor %s; sta %s',[getValue_b1(src2),getValue_b1(dst)]);
end;

procedure TCodeGen_6502.xor_161616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; eor %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda %s; eor %s; sta %s',[getValue_b1(src1),getValue_b1(src2),getValue_b1(dst)]);
end;

procedure TCodeGen_6502.xor_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s; eor %s; sta %s',[getValue_b0(src1),getValue_b0(src2),getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.neg_0808(right,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(right)]);
  Fwriter.writeLn(indent+'eor $ff');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'adc #1');

  if dst.toString <> cA_REGISTER then
    Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.neg_0816(right,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(right)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_b1(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.neg_1616(right,dst : TAST);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(right)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_b1(right)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.neg_2424(right,dst : TAST);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(right)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_b1(right)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_b2(right)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b2(dst)]);
end;

procedure TCodeGen_6502.not_0808(right,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(right)]);
  Fwriter.writeLn(indent+'eor $ff');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.not_0816(right,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(right)]);
  Fwriter.writeLn(indent+'eor $ff');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.eql_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.eql_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

{
procedure equal; assembler;
asm
  lda #>x
  cmp #>y
  bne !isFalse+
  lda #<x
  cmp #<y
  bne !isFalse+
!isTrue:
  lda #$ff
  rts
!isFalse:
  lda #$00
  rts
!check:
end;

}
procedure TCodeGen_6502.eql_081616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'cmp %s',[getValue_b1(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.eql_161616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b1(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.eql_160816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
  Fwriter.writeLn(indent+'cmp #0');
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

{
procedure notEqual; assembler;
asm
  lda #>x
  cmp #>y
  bne !isTrue+
  lda #<x
  cmp #<y
  bne !isTrue+
!isFalse:
  lda #$00
  rts
!isTrue:
  lda #$ff
end;
}
procedure TCodeGen_6502.neq_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'beq !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.neq_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'beq !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.neq_161616(src1,src2,dst: TAST);
begin
  emitCompare_16(src1,src2,true,false,true);
//  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
//  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
//  Fwriter.writeLn(indent+'bne !isTrue+');
//  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
//  Fwriter.writeLn(indent+'cmp %s',[getValue_b1(src2)]);
//  Fwriter.writeLn(indent+'bne !isTrue+');
//  Fwriter.writeLn(indent+'// is false');
//  Fwriter.writeLn(indent+'lda #$00');
//  Fwriter.writeLn(indent+'jmp !+');
//  Fwriter.writeLn('!isTrue:');
//  Fwriter.writeLn(indent+'lda #$ff');
//  Fwriter.writeLn('!:');
//  Fwriter.writeLn(indent+'sec');
//  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
//  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(src2)]);
//  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
//  Fwriter.writeLn(indent+'sbc %s',[getValue_b1(src2)]);
//
//  Fwriter.writeLn(indent+'beq !isFalse+');
//  Fwriter.writeLn(indent+'// true');
//  Fwriter.writeLn(indent+'lda #$ff');
//  Fwriter.writeLn(indent+'jmp !+');
//  Fwriter.writeLn('!isFalse:');
//  Fwriter.writeLn(indent+'lda #$00');
//  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.neq_160816(src1,src2,dst: TAST);
begin
  if src2.toString = '0' then begin
    Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
    Fwriter.writeLn(indent+'bne !isTrue+');
    Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
    Fwriter.writeLn(indent+'bne !isTrue+');
    Fwriter.writeLn(indent+'// is false');
    Fwriter.writeLn(indent+'lda #$00');
    Fwriter.writeLn(indent+'jmp !+');
    Fwriter.writeLn('!isTrue:');
    Fwriter.writeLn(indent+'lda #$ff');
    Fwriter.writeLn('!:');
  end
  else begin
    Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
    Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
    Fwriter.writeLn(indent+'bne !isTrue+');
    Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
    Fwriter.writeLn(indent+'bne !isTrue+');
    Fwriter.writeLn(indent+'// is false');
    Fwriter.writeLn(indent+'lda #$00');
    Fwriter.writeLn(indent+'jmp !+');
    Fwriter.writeLn('!isTrue:');
    Fwriter.writeLn(indent+'lda #$ff');
    Fwriter.writeLn('!:');
  end;
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.neq_160808(src1,src2,dst: TAST);
begin
  if src2.toString = '0' then begin
    Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
    Fwriter.writeLn(indent+'bne !isTrue+');
    Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
    Fwriter.writeLn(indent+'bne !isTrue+');
    Fwriter.writeLn(indent+'// is false');
    Fwriter.writeLn(indent+'lda #$00');
    Fwriter.writeLn(indent+'jmp !+');
    Fwriter.writeLn('!isTrue:');
    Fwriter.writeLn(indent+'lda #$ff');
    Fwriter.writeLn('!:');
  end
  else begin
    Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
    Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
    Fwriter.writeLn(indent+'bne !isTrue+');
    Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
    Fwriter.writeLn(indent+'bne !isTrue+');
    Fwriter.writeLn(indent+'// is false');
    Fwriter.writeLn(indent+'lda #$00');
    Fwriter.writeLn(indent+'jmp !+');
    Fwriter.writeLn('!isTrue:');
    Fwriter.writeLn(indent+'lda #$ff');
    Fwriter.writeLn('!:');
  end;
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.lss_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'bcs !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.lss_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'bcs !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.lss_160816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
  Fwriter.writeLn(indent+'sbc #0');

  Fwriter.writeLn(indent+'bmi !isTrue+');
  Fwriter.writeLn(indent+'  lda #$00');
  Fwriter.writeLn(indent+'  jmp !+');

  Fwriter.writeLn(indent+'!isTrue:');
  Fwriter.writeLn(indent+'  lda #$ff');
  Fwriter.writeLn(indent+'  jmp !+');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.lss_081616(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent + 'lda #0');
  Fwriter.writeLn(indent + 'cmp ' + getValue_b1(src2));
  Fwriter.writeLn(indent + 'bne !+');
  Fwriter.writeLn(indent + 'lda ' + getValue_b0(src1));
  Fwriter.writeLn(indent + 'cmp ' + getValue_b0(src2));
  Fwriter.writeLn('!:');

  Fwriter.writeLn(indent + 'bcc !+ // is <');

  Fwriter.writeLn(indent + 'bne !isFalse+ // is > (false)');

  Fwriter.writeLn(indent + 'beq !isFalse+ // is = (false)');

  Fwriter.writeLn('!isTrue:');
  Fwriter.writeLn(indent + 'lda #$ff');
  Fwriter.writeLn(indent + 'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent + 'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.lss_161616(src1,src2,dst: TAST);
begin
  emitCompare_16(src1,src2,true,false,false);
//  Fwriter.writeLn(indent+'sec');
//  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
//  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
//  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
//  Fwriter.writeLn(indent+'sbc %s',[getValue_hi(src2)]);
//
//  Fwriter.writeLn(indent+'bcs !isFalse+');
//  Fwriter.writeLn(indent+'// true');
//  Fwriter.writeLn(indent+'lda #$ff');
//  Fwriter.writeLn(indent+'jmp !+');
//  Fwriter.writeLn('!isFalse:');
//  Fwriter.writeLn(indent+'lda #$00');
//  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.geq_160808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
  Fwriter.writeLn(indent+'sbc #0');

  Fwriter.writeLn(indent+'bccLong !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.geq_160816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
  Fwriter.writeLn(indent+'sbc #0');

  Fwriter.writeLn(indent+'bccLong !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.geq_161616(src1,src2,dst: TAST);
begin
  emitCompare_16(src1,src2,false,false,true);
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.geq_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'bcc !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.gtr_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'bcc !isFalse+');
  Fwriter.writeLn(indent+'beq !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.gtr_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'bcc !isFalse+');
  Fwriter.writeLn(indent+'beq !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.gtr_160816(src1,src2,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b1(src2)]);
  Fwriter.writeLn(indent+'bne !+');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'bcc !isFalse+ // is lower');
  Fwriter.writeLn(indent+'bne !isTrue+ // is higher');
  Fwriter.writeLn(indent+'beq !isFalse+ // is same');
  Fwriter.writeLn('!isTrue:');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.gtr_161616(src1,src2,dst : TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b1(src2)]);
  Fwriter.writeLn(indent+'bne !+');
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'bcc !isFalse+ // is lower');
  Fwriter.writeLn(indent+'bne !isTrue+ // is higher');
  Fwriter.writeLn(indent+'beq !isFalse+ // is same');
  Fwriter.writeLn('!isTrue:');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.leq_080808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'gtr !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
end;

procedure TCodeGen_6502.leq_080816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'gtr !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

{
  lda #>x               //MSB of 1st number
  cmp #0                //MSB of 2nd number
  bne check1           //X < Y

  lda #<x               //LSB of 1st number
  cmp #<y               //LSB of 2nd number
  beq !isTrue+  //isEqual            //X = Y

check1:
  bcc !isTrue+  //isLower

  lda #$00
  jmp !+

!isTrue:
  lda #$ff
!:
}
procedure TCodeGen_6502.leq_160816(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'  lda %s',[getValue_b1(src1)]);
  Fwriter.writeLn(indent+'  cmp #0                //MSB of 2nd number');
  Fwriter.writeLn(indent+'  bne !check1+           //X < Y');

  Fwriter.writeLn(indent+'  lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'  cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'  beq !isTrue+  //isEqual            //X = Y');

  Fwriter.writeLn('!check1:');
  Fwriter.writeLn(indent+'  bcc !isTrue+  //isLower');

  Fwriter.writeLn(indent+'  lda #$00');
  Fwriter.writeLn(indent+'  jmp !+');

  Fwriter.writeLn('!isTrue:');
  Fwriter.writeLn(indent+'  lda #$ff');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;


procedure TCodeGen_6502.leq_081616(src1,src2,dst: TAST);
begin

   Fwriter.writeLn(indent+'  lda #0                //MSB of 1st number');
   Fwriter.writeLn(indent+'  cmp %s                //MSB of 2nd number',[getValue_b1(src2)]);
   Fwriter.writeLn(indent+'  bcc !isTrue+          //X < Y');

   Fwriter.writeLn(indent+'  bne !isFalse+         //X > Y');

   Fwriter.writeLn(indent+'  lda %s                //LSB of 1st number',[getValue_b0(src1)]);
   Fwriter.writeLn(indent+'  cmp %s                //LSB of 2nd number',[getValue_b0(src2)]);
   Fwriter.writeLn(indent+'  bcc !isTrue+          //X < Y');

   Fwriter.writeLn(indent+'  beq !isTrue+          //X = Y');

   Fwriter.writeLn(indent+'  bne !isFalse+         //X > Y');
   Fwriter.writeLn('!isTrue:');
   Fwriter.writeLn(indent+'  lda #$ff; jmp !+');
   Fwriter.writeLn('!isFalse:');
   Fwriter.writeLn(indent+'  lda #$00');
   Fwriter.writeLn('!:');
   Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
   Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.leq_160808(src1,src2,dst: TAST);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_b1(src1)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'lda %s',[getValue_b0(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_b0(src2)]);
  Fwriter.writeLn(indent+'gtr !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_b0(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_b1(dst)]);
end;

procedure TCodeGen_6502.inc_08(dst: TAST);
begin
  Fwriter.writeLn(indent+'inc '+getValue_b0(dst));
end;

procedure TCodeGen_6502.inc_16(dst: TAST);
begin
  Fwriter.writeLn(indent+'inc '+getValue_b0(dst));
  Fwriter.writeLn(indent+'bne !+');
  Fwriter.writeLn(indent+'inc '+getValue_b1(dst));
  Fwriter.writeLn(indent+'!:');
end;

procedure TCodeGen_6502.inc_24(dst: TAST);
begin
  Fwriter.writeLn(indent+'inc '+getValue_b0(dst));
  Fwriter.writeLn(indent+'bne !+');
  Fwriter.writeLn(indent+'inc '+getValue_b1(dst));
  Fwriter.writeLn(indent+'!:');
  Fwriter.writeLn(indent+'bne !+');
  Fwriter.writeLn(indent+'inc '+getValue_b2(dst));
  Fwriter.writeLn(indent+'!:');
end;

procedure TCodeGen_6502.dec_08(dst: TAST);
begin
  Fwriter.writeLn(indent+'dec '+getValue_b0(dst));
end;

procedure TCodeGen_6502.dec_16(dst: TAST);
begin
  Fwriter.writeLn(indent+'dec '+getValue_b0(dst));
  Fwriter.writeLn(indent+'bne !+');
  Fwriter.writeLn(indent+'dec '+getValue_b1(dst));
  Fwriter.writeLn(indent+'!:');
end;

procedure TCodeGen_6502.dec_24(dst: TAST);
begin
  Fwriter.writeLn(indent+'dec '+getValue_b0(dst));
  Fwriter.writeLn(indent+'bne !+');
  Fwriter.writeLn(indent+'dec '+getValue_b1(dst));
  Fwriter.writeLn(indent+'!:');
  Fwriter.writeLn(indent+'bne !+');
  Fwriter.writeLn(indent+'dec '+getValue_b2(dst));
  Fwriter.writeLn(indent+'!:');
end;

end.

