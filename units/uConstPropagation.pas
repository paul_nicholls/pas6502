unit uConstPropagation;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  System.Classes,uAST;

type
  TConstantPropagationList = class(TObject)
  private
     FconstList : TStringList;
  public
    constructor create;
    destructor  destroy;

    procedure addConstant(name : AnsiString; value : TAST);
    function  foundConstant(name : AnsiString; var value : TAST) : Boolean;
    procedure deleteConstant(name : AnsiString);

    procedure clear;
  end;

implementation

constructor TConstantPropagationList.create;
begin
  FconstList := TStringList.Create;
end;

destructor  TConstantPropagationList.destroy;
begin
  FconstList.Free;
end;

procedure TConstantPropagationList.addConstant(name : AnsiString; value : TAST);
var
  index : Integer;
begin
  index := FconstList.IndexOfName(name);

  if index <> -1 then begin
  // found so replace it
    TAST(FconstList.Objects[index]).Free;
    FconstList.Objects[index] := value;
  end
  else begin
  // not found so add it
    FconstList.AddObject(name,value);
  end;
end;

function  TConstantPropagationList.foundConstant(name : AnsiString; var value : TAST) : Boolean;
var
  index : Integer;
begin
  Result := False;

  index := FconstList.IndexOfName(name);

  if index <> -1 then Exit;

  value := TAST(FconstList.Objects[index]);

  Result := True;
end;

procedure TConstantPropagationList.deleteConstant(name : AnsiString);
var
  index : Integer;
begin
  index := FconstList.IndexOfName(name);

  if index <> -1 then Exit;

  TAST(FconstList.Objects[index]).Free;

  FconstList.Delete(index);
end;

procedure TConstantPropagationList.clear;
begin
  FconstList.Clear;
end;

end.
