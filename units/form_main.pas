unit form_main;
//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------
interface
//-----------------------------------------------------------------------------
// uses SynEdit components found here:
// https://github.com/SynEdit/SynEdit
//-----------------------------------------------------------------------------
uses
  Clipbrd,Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,uParser, Vcl.ExtCtrls,
  Vcl.StdCtrls, SynEdit,  SynHighlighterAsm, SynEditHighlighter,
  SynEditCodeFolding, SynHighlighterPas,{uExpressions,} Vcl.StdActns,
  System.Actions, Vcl.ActnList, Vcl.Menus, System.ImageList, Vcl.ImgList,System.Types,System.StrUtils,form_options,
  uCodeInsight, Vcl.ComCtrls, uToken,uAST,form_help,System.Contnrs,
  SynCompletionProposal,uSymbolTable,uASTto6502, form_findReplace, Vcl.ToolWin;

const
  cMAX_MRU_FILES    = 10;

  MODE_FIND         = 0;
  MODE_FIND_REPLACE = 1;
  FIND_REPLACE_MIN  = 50;
  FIND_REPLACE_MAX  = 84;

  FONT_MIN          = 5;
  FONT_MAX          = 14;

  FONT_NAME         = 'consolas';

type
  TFileInfo = class
    fileName : AnsiString;
    isDirty  : Boolean;
    isNew    : Boolean;
    srcMemo  : TSynEdit;

    constructor Create(aFileName : AnsiString; aIsDirty,aIsNew : Boolean);
    destructor  destroy;
  end;

  TmruFile = class
    fileName : AnsiString;
  end;

  TPas6502_Form = class(TForm)
    split_Panel: TPanel;
    SynPasSyn: TSynPasSyn;
    Splitter2: TSplitter;
    GroupBox1: TGroupBox;
    Error_Memo: TMemo;
    MainMenu: TMainMenu;
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    FileSaveAs1: TFileSaveAs;
    FileExit1: TFileExit;
    ImageList1: TImageList;
    File1: TMenuItem;
    Open1: TMenuItem;
    SaveAs1: TMenuItem;
    Exit1: TMenuItem;
    Options1: TMenuItem;
    OptionsAction: TAction;
    CodeInsight_PopupMenu: TPopupMenu;
    CodeInsight_Timer: TTimer;
    New_Action: TAction;
    NewProject1: TMenuItem;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    NewUnit1: TMenuItem;
    Edit1: TMenuItem;
    Find1: TMenuItem;
    Tab_PopupMenu: TPopupMenu;
    ClosePage1: TMenuItem;
    Save1: TMenuItem;
    PageControl_SourceCode: TPageControl;
    TabSheet1: TTabSheet;
    SourceCode_SynMemo: TSynEdit;
    StatusBar_SourceCodeStatus: TStatusBar;
    Gotolinenumber1: TMenuItem;
    menuItem_tutorials: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    MenuItem_openRecent: TMenuItem;
    Project1: TMenuItem;
    Compile1: TMenuItem;
    CompileandRun1: TMenuItem;
    Paths1: TMenuItem;
    Panel1: TPanel;
    PackFile_CheckBox: TCheckBox;
    GroupBox3: TGroupBox;
    AsmOutput_Memo: TSynEdit;
    Splitter3: TSplitter;
    Showcodesuggestions1: TMenuItem;
    Timer1: TTimer;
    Codeinsight1: TMenuItem;
    codeInsight_ComboBox: TComboBox;
    FindandReplace1: TMenuItem;
    SearchAgain1: TMenuItem;
    popup_Timer: TTimer;
    identifierLink_Timer: TTimer;
    gotoDefinition_Panel: TPanel;
    prevSrc_MenuItem: TMenuItem;
    nextSrc_MenuItem: TMenuItem;
    SynPasSyn1: TSynPasSyn;
    FontSizePlus_MenuItem: TMenuItem;
    FontSizeMinus_MenuItem: TMenuItem;
    Label1: TLabel;
    fontSize_Edit: TEdit;
    toggleCommentLines_MenuItem: TMenuItem;
    LineIndentdecrease_MenuItem: TMenuItem;
    LineIndentincrease_MenuItem: TMenuItem;
    Label3: TLabel;
    theme_ComboBox: TComboBox;
    procedure Compile_ButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SourceCode_SynMemoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SourceCode_SymMemoStatusChanged(Sender: TObject; Changes: TSynStatusChanges);
    procedure CompileAndRun_ButtonClick(Sender: TObject);
    procedure OptionsActionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CodeInsight_TimerTimer(Sender: TObject);
    procedure New_ActionExecute(Sender: TObject);
    procedure Open1Click(Sender: TObject);
    procedure SaveAs1Click(Sender: TObject);
    procedure NewUnit1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Find1Click(Sender: TObject);
    procedure PageControl_SourceCodeContextPopup(Sender: TObject;
      MousePos: TPoint; var Handled: Boolean);
    procedure ClosePage1Click(Sender: TObject);
    procedure Save1Click(Sender: TObject);
    procedure Gotolinenumber1Click(Sender: TObject);
    procedure Exit1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Splitter2CanResize(Sender: TObject; var NewSize: Integer;
      var Accept: Boolean);
    procedure Compile1Click(Sender: TObject);
    procedure CompileandRun1Click(Sender: TObject);
    procedure Paths1Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SourceCode_SynMemoClick(Sender: TObject);
    procedure codeInsight_ComboBoxKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Showcodesuggestions1Click(Sender: TObject);
    procedure Codeinsight1Click(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FindandReplace1Click(Sender: TObject);
    procedure SearchAgain1Click(Sender: TObject);
    procedure SourceCode_SynMemoMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure identifierLink_TimerTimer(Sender: TObject);
    procedure prevSrc_MenuItemClick(Sender: TObject);
    procedure nextSrc_MenuItemClick(Sender: TObject);
    procedure SourceCode_SynMemoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormActivate(Sender: TObject);
    procedure FontSizePlus_MenuItemClick(Sender: TObject);
    procedure FontSizeMinus_MenuItemClick(Sender: TObject);
    procedure toggleCommentLines_MenuItemClick(Sender: TObject);
    procedure LineIndentdecrease_MenuItemClick(Sender: TObject);
    procedure LineIndentincrease_MenuItemClick(Sender: TObject);
    procedure theme_ComboBoxChange(Sender: TObject);
  private
    { Private declarations }
    FthemeName            : AnsiString;
    FKickAssemblerJarFile : AnsiString;
    FViceX64ExeFile       : AnsiString;
    FViceXPETExeFile      : AnsiString;
    FViceXvicExeFile      : AnsiString;
    FNESExeFile           : AnsiString;
    FViceX128ExeFile      : AnsiString;
    FExomizerExeFile      : AnsiString;
    FBBCEmulatorFile      : AnsiString;
    FAtari8BitEmulatorFile: AnsiString;
    FAppleIIEmulatorFile  : AnsiString;
    FProjectFileName      : AnsiString;
    FprojectInfo          : TFileInfo;
    FCodeInsightData      : TCodeInsightData;
    FSelPos               : integer;
    Fwarnings             : AnsiString;
    FmruFiles             : TObjectList;
    ffindReplace          : TfindReplaceDialog;

    fSourceInfo           : TStringList;
    identifierLink        : TSourceInfo;

    FsrcHistory        : TSourceInfoArray;
    FsrcHistoryIndex   : Integer;
    FsrcHistoryCount   : Integer;
    FctrlIsDown        : Boolean;
    FfontSize          : Integer;

    procedure setSourceCodeFontSize(pageControl : TPageControl);
    procedure gotoSource(offset : Integer; fileName : String);
    procedure clearSrcHistory;
    procedure pushSrcHistory(scrollPos,offset : Integer; fileName : String);
    function  popSrcHistory : TSourceInfoRec;

    procedure prevSrcHistory;
    procedure nextScrPosHistory;

    procedure saveTheme(themeName : AnsiString);
    procedure loadTheme(themeName : AnsiString);
    procedure loadThemes;
    procedure loadUnit(fileName : String);
    function  getSourceInfoByName(name : String) : TSourceInfo;
    procedure clearSourceInfo;
    procedure addSourceInfoByName(name : String; symbol : TSymbol; srcOffset : Integer; srcFile : String);
    function  expandWordAt(col : Integer; line : String) : String;
    procedure tutorialClick(sender : TObject);
    procedure mruFilesClick(sender : TObject);
    procedure toggleCommenttedLines;
    procedure indentLines(increaseIndent : Boolean);
    procedure loadTutorialFilesList;
    procedure loadMostRecentlyUsedFilesList;
    procedure updateMostRecentlyUsedFilesList(fileName : AnsiString);
    procedure saveMostRecentlyUsedFilesList;
    procedure CodeInsight_OnClick(Sender: TObject);

    function  getSourceCodeMemo(useSrcCodeMemoOnly : Boolean) : TSynEdit;

    procedure getInsightDataFromSource;
    procedure onLineUpdate(aCurrentLexerLine : Integer);
    procedure updateCodeInsightData(aCurrentScope : TScopedSymbolTable; aCurrentFile : String);
    procedure OnScopeUpdate(aCurrentScope : TscopedSymbolTable; aCurrentFile : AnsiString);

    function  getMatchingInsightData(dataArray : TStringDynArray) : TCodeInsightData;
    procedure GetCodeInsightDataFromLine(line : AnsiString; column : Integer; var insightData : TStringDynArray);
    procedure SetCaption(ACaption : AnsiString);
    function StartProcess(ExeName: AnsiString; CmdLineArgs: AnsiString = '';
      ShowWindow: boolean = True; WaitForFinish: boolean = False): integer;
    procedure copyImportsToProjectFolder(ProjectFolder : AnsiString; aProg : TProgram);
    function  generateDiscImage(aOutputPath,aProgName : AnsiString; aProg : TProgram) : Boolean;
    procedure convertBinToXEX(address : AnsiString; srcFile,dstFile : AnsiString);
    function  compileAtari8BittoXEX(aProg : TProgram) : Boolean;
    function  compileAndCreateBBCDiskImage(aProg : TProgram) : Boolean;
    function  compileAndCreateAppleIIDiskImage(aProg : TProgram) : Boolean;
    function  CreateAndCompileAsmFile(aProg : TProgram) : Boolean;
    function  PackCompiledProgram : Boolean;
    procedure ExecuteCompiledAsmFile(aProg : TProgram);
    procedure removeDoubleBlankLines(asmStream : TMemoryStream);
    procedure removeUnusedRoutines(asmStream : TMemoryStream; parser : TObject);
    procedure CompileProgram(ExecuteProgram : Boolean);
  public
    { Public declarations }
  end;
var
  Pas6502_Form: TPas6502_Form;

implementation

{$R *.dfm}
uses Math,Masks,ShellApi,IniFiles,
     dprocess,  // this is the TProcess unit from FPC, now ported to delphi
     uParserAST,
     form_gotoLine,form_codeInsightOptions;

const
  cIncludesFolder = 'include\';

  cExamplesFolder = 'examples\';

function  posToLine(text : AnsiString; p : Integer) : Integer;
// converts position in text to line number (from 0)
var
  i : Integer;
begin
  Result := 0;

  if (text = '') or (p = -1) then begin
    Result := -1;
    Exit;
  end;


  for i := 0 to p do if (Text[i] = #13) then Inc(Result);
end;

procedure OutLn(s: AnsiString); overload;
begin
  Pas6502_Form.Error_Memo.lines.add(s);
end;
procedure OutLn(s: AnsiString; i: integer); overload;
begin
  outln(s + inttostr(i));
end;
procedure DoLog(s: AnsiString);
begin
  OutLn('Log: '+s);
end;
function RunProcess(const Binary: AnsiString; args: TStrings): boolean;
const
  BufSize = 1024;
var
  p: TProcess;
  // Buf: AnsiString;  // L505 note: must use ansistring
  Buf: ansistring; //
  Count: integer;
  i: integer;
  LineStart: integer;
  // OutputLine: AnsiString;  //L505 note: must use ansistring
  OutputLine: ansistring; //
begin
  p := TProcess.Create(nil);
  try
    p.Executable := Binary;
    p.Options := [poUsePipes,
                  poStdErrToOutPut];
//    p.CurrentDirectory := ExtractFilePath(p.Executable);
    p.ShowWindow := swoHIDE {ShowNormal};
    p.Parameters.Assign(args);
//    DoLog('Running command '+ p.Executable +' with arguments: '+ p.Parameters.Text);
    p.Execute;
    { Now process the output }
    OutputLine:='';
    SetLength(Buf,BufSize);
    repeat
      if (p.Output<>nil) then
      begin
        // Count:=p.Output.Read(Buf[1],Length(Buf));
        Count:=p.Output.Read(pchar(Buf)^, BufSize);  //L505 changed to pchar because of unicodestring
        // outln('DEBUG: len buf: ', length(buf));
      end
      else
        Count:=0;
      LineStart:=1;
      i:=1;
      while i<=Count do
      begin
        // L505
        //if Buf[i] in [#10,#13] then
        if CharInSet(Buf[i], [#10,#13]) then
        begin
          OutputLine:=OutputLine+Copy(Buf,LineStart,i-LineStart);
          outln(OutputLine);
          OutputLine:='';
          // L505
          //if (i<Count) and (Buf[i+1] in [#10,#13]) and (Buf[i]<>Buf[i+1]) then
          if (i<Count) and (CharInset(Buf[i], [#10,#13])) and (Buf[i]<>Buf[i+1]) then
            inc(i);
          LineStart:=i+1;
        end;
        inc(i);
      end;
      OutputLine:=Copy(Buf,LineStart,Count-LineStart+1);
    until Count=0;
    if OutputLine <> '' then
      outln(OutputLine);
//  else
//    outln('DEBUG: empty line');
    p.WaitOnExit;
    Result := p.ExitStatus = 0;
    if not Result then
      outln('Command '+ p.Executable +' failed with exit code: ', p.ExitStatus);
  finally
    FreeAndNil(p);
  end;
end;
const
{$ifdef MSWINDOWS}prog = 'cmd';{$endif}
{$ifdef MACOS}prog = 'ls';{$endif}
var args: TStringList;
{$ifdef MSWINDOWS}
procedure AddArg(arg : AnsiString);
begin
  args.add(arg);
end;
{$endif}
procedure ClearArgs;
begin
  args.Clear;
end;

constructor TFileInfo.Create(aFileName: AnsiString; aIsDirty,aIsNew: Boolean);
begin
  fileName := aFileName;
  isDirty  := aIsDirty;
  isNew    := aIsNew;
end;

destructor TFileInfo.destroy;
begin
  srcMemo.Free;
end;

procedure SetClipboardText(const Text: WideString);
// source:
// https://stackoverflow.com/questions/21707127/delphi-7-how-to-copy-non-latin-text-to-clipboard-convert-to-unicode
var
  Count: Integer;
  Handle: HGLOBAL;
  Ptr: Pointer;
begin
  Count := (Length(Text)+1)*SizeOf(WideChar);
  Handle := GlobalAlloc(GMEM_MOVEABLE, Count);
  Try
    Win32Check(Handle<>0);
    Ptr := GlobalLock(Handle);
    Win32Check(Assigned(Ptr));
    Move(PWideChar(Text)^, Ptr^, Count);
    GlobalUnlock(Handle);
    Clipboard.SetAsHandle(CF_UNICODETEXT, Handle);
  Except
    GlobalFree(Handle);
    raise;
  End;
end;

procedure TPas6502_Form.CodeInsight_OnClick(Sender: TObject);
var
  InsertionPos  : TBufferCoord;
  InsertionText : WideString;
  i             : Integer;
  srcCodeMemo   : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(true);

  InsertionText := TMenuItem(Sender).Caption;

  i := Pos(WideString(':'),InsertionText);

  if i > 1 then
  // typed part
    InsertionText := Copy(InsertionText,i+2,Length(InsertionText));

  InsertionPos  := srcCodeMemo.CaretXY;
  srcCodeMemo.SetFocus;
  SetClipboardText(InsertionText);
  srcCodeMemo.PasteFromClipboard;
//  srcCodeMemo.InsertBlock(InsertionPos,InsertionPos,PWideChar(InsertionText),True);
end;

procedure TPas6502_Form.toggleCommentLines_MenuItemClick(Sender: TObject);
begin
  toggleCommenttedLines;
end;

procedure TPas6502_Form.onLineUpdate(aCurrentLexerLine : Integer);
var
  srcCodeMemo   : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(true);

  if aCurrentLexerLine = srcCodeMemo.CaretXY.Line then
    WriteLn('source code line = ',aCurrentLexerLine);

end;

function  getSymbolInfo(aSym : TSymbol) : AnsiString;
var
  symClass : TSymbolClass;
begin
  Result := '';

  if aSym = nil then Exit;

  symClass := aSym.symClass;

//  if aSym.symType <> nil then
//    symClass := aSym.symType.symClass;

  case aSym.symClass of
    scTypedPointer : Result := ' typed pointer';
    scCustomType   : Result := ' custom type';
    scVar          : Result := ' variable';
    scConst        : Result := ' constant';
    scRecordType   : Result := ' record type';
    scProc         : Result := ' procedure';
    scFunc         : Result := ' function';
    scBuiltinFunc  : Result := ' function';
    scMacro        : Result := ' macro';
//    scUnit         : Result := ' // ';
  else
    Result := ' (variable)';
  end;
end;

procedure addRecordVarToCodeInsight(aFullyQualifiedName : AnsiString; aSym : TSymbol; aCodeInsightData : TCodeInsightData);
// add the record var a.b.r.dfd... to the code insight for lookup
var
  i       : Integer;
  data    : TCodeInsightData;
  names   : TStringDynArray;
  name    : AnsiString;
  symSize : Integer;
begin
  symSize := aSym.size;
  if aSym.symType <> nil then symSize := aSym.size;

  names := SplitString(aFullyQualifiedName,'.');

  for i := 0 to High(names) do  begin
    name := names[i];

    if i = 0 then
      data := aCodeInsightData.AddDataByName(name,aSym.symType)
    else
      data := data.AddDataByName(name,aSym.symType);

    if data.parent = nil then
      data.setOffsetAndFile(aSym,aSym.srcOffset,aSym.srcfile);

  end;

  if symSize in [2..4] then begin
    data := data.GetDataByName(name);

    for i  := 0 to symSize - 1 do
      data.AddDataByName('b'+IntToStr(i),aSym.symType);

    if symSize in [3,4] then begin
      data.AddDataByName('loword',aSym.symType);
      data.AddDataByName('hiword',aSym.symType);
    end;
  end;
end;

procedure TPas6502_Form.updateCodeInsightData(aCurrentScope : TScopedSymbolTable; aCurrentFile : String);
var
  i,j     : Integer;
  sym     : TSymbol;
  symSize : Integer;
  symInfo : AnsiString;
  data    : TCodeInsightData;
begin

  for i := 0 to aCurrentScope.count - 1 do begin
    sym := aCurrentScope.getSymbol(i);

    if not sym.isPrivate then begin
      addSourceInfoByName(sym.name,sym,sym.srcOffset,sym.srcFile);

      symSize := sym.size;
      if sym.symType <> nil then symSize := sym.size;


      if sym.symClass in [scVar,scConst,scProc,scFunc,scBuiltinFunc] then begin
        if pos('.',sym.symName) = 0 then begin
        // normal name so just add it
          data := FcodeInsightData.AddDataByName(sym.symName,sym.symType);
          data.setOffsetAndFile(sym,sym.srcOffset,sym.srcFile);

          if symSize in [2,3,4] then begin
            for j := 0 to symSize - 1 do
                data.AddDataByName('b' + IntToStr(j),aCurrentScope.lookup('byte'));

            if symSize >= 3 then begin
              data.AddDataByName('loword',aCurrentScope.lookup('word'));
              data.AddDataByName('hiword',aCurrentScope.lookup('word'));
            end;
          end;

        end
        else begin
          // symbol is a qualified name with dots so separate and add
          addRecordVarToCodeInsight(sym.symName,sym,FcodeInsightData);
        end;
      end;
    end;
  end;

  if aCurrentScope.enclosingScope <> nil then
    updateCodeInsightData(aCurrentScope.enclosingScope,aCurrentFile);
end;

procedure TPas6502_Form.OnScopeUpdate(aCurrentScope : TScopedSymbolTable; aCurrentFile : AnsiString);
begin
  if codeInsight_ComboBox.Visible then
    // don't update code insight when already viewing data
    Exit;

  FcodeInsightData.clear;

  updateCodeInsightData(aCurrentScope,aCurrentFile);


//  writeln(format('"%s": %s',[aCurrentScope.scopeName,aCurrentFile]));
//
//  aCurrentScope.debugPrint;
end;

function  TPas6502_Form.getMatchingInsightData(dataArray : TStringDynArray) : TCodeInsightData;
var
  i    : Integer;
  data : TCodeInsightData;
begin
  Result := nil;

  i := 1;
  data := FCodeInsightData.GetDataByName(dataArray[0]);
  while i < Length(dataArray) do begin
    if data <> nil then data := data.GetDataByName(dataArray[i]);
    Inc(i);
  end;
  Result := data;
end;

procedure TPas6502_Form.GetCodeInsightDataFromLine(line : AnsiString; column : Integer; var insightData : TStringDynArray);
var
  i,j  : Integer;
  data : AnsiString;
begin
  SetLength(insightData,0);
  if line = '' then Exit;

  // get all data before '.' character to include valid identifiers
  i := column - 1;
  data := '';
  while (i > 0) do begin
    if line[i] in [#13,#10,' ','(',';'] then begin
      data := Copy(line,i+1,(column-1)-(i+1)+1);
      break;
    end;
    dec(i);
  end;

//  while (i > 0) and (line[i] in ['a'..'z','A','Z','0'..'9','_','.']) do begin
//    data := line[i] + data;
//    dec(i);
//  end;
//
//  while (i > 0) do begin
//    if line[i] in  [' ','(',';'] then begin
////      line := Copy(line,i,Length(line));
//      line := Copy(line,i+1,column-1);
//      Break;
//    end;
//    Dec(i);
//  end;
  line := data;
  if line = '' then begin
    SetLength(insightData,0);
    Exit;
  end;

  if (Pos('[',line) > 0) and (Pos(']',line) > 0) then begin
  // found [] in the line so remove it
    i := Pos('[',line);

    j := Length(line);

    while line[j] <> ']' do
      Dec(j);

    Delete(line,i,(j-i)+1);
  end
  else
  if (Pos('^',line) > 0) then begin
  // found ^ so remove it
    Delete(line,Pos('^',line),1);
  end;

  insightData := SplitString(line,'.');
  for i := 0 to High(insightData) do
    insightData[i] := Trim(insightData[i]);
  if insightData[High(insightData)] = '' then
    SetLength(insightData,High(insightData));
end;

procedure TPas6502_Form.Gotolinenumber1Click(Sender: TObject);
var
  gotoDialog : TGotoLineDialog;
  coord      : TBufferCoord;
  line       : Integer;
begin
  gotoDialog := TGotoLineDialog.Create(nil);
  try
    if gotoDialog.ShowModal = mrOk then begin
      line := StrToInt('0'+gotoDialog.lineNumber_Edit.Text);

      if line < getSourceCodeMemo(false).Lines.Count then begin
        coord.Char := 1;
        coord.Line := line;

        getSourceCodeMemo(false).CaretXY := coord;
      end;
    end;
  finally
    gotoDialog.Free;
  end;
end;

procedure TPas6502_Form.identifierLink_TimerTimer(Sender: TObject);
begin
//  getSourceCodeMemo(true).Cursor := crIBeam;
  identifierLink_Timer.Enabled := False;
end;

procedure toggleLineComment(var line : AnsiString);
begin
  if (Copy(line,1,2) = '//') then
  // has comment so remove
    line := Copy(line,3,Length(line))
  else
  // has no comment so add
    line := '//' + line;
end;

procedure TPas6502_Form.toggleCommenttedLines;
var
  line1,line2 : Integer;
  lineStr     : AnsiString;

  selStart,selEnd : Integer;
  srcCodeMemo   : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(true);

  selStart := srcCodeMemo.SelStart;
  line1 := posToLine(srcCodeMemo.Text,selStart);

  if srcCodeMemo.SelLength = 0 then begin
    lineStr := srcCodeMemo.Lines.Strings[line1];

    toggleLineComment(lineStr);

    srcCodeMemo.Lines.Strings[line1] := lineStr;
    Exit;
  end;

  selEnd   := srcCodeMemo.SelEnd;

  line2 := posToLine(srcCodeMemo.Text,selEnd);

  if line1 = -1 then Exit;

  repeat
    lineStr := srcCodeMemo.Lines.Strings[line1];

    toggleLineComment(lineStr);

    srcCodeMemo.Lines.Strings[line1] := lineStr;

    Inc(line1);
  until (line1 > line2);
end;

procedure indentLine(var line : AnsiString; increaseIndent : Boolean);
var
  c : Integer;
begin
  if line = '' then Exit;

  if increaseIndent then begin
    line := '  '+line;
    Exit;
  end;

  c := 0;
  while line[1] = ' ' do begin
    Delete(line,1,1);
    Inc(c);
    if c = 2 then Break;
  end;
end;

procedure TPas6502_Form.indentLines(increaseIndent : Boolean);
var
  line1,line2 : Integer;
  lineStr     : AnsiString;

  selStart,selEnd : Integer;
  srcCodeMemo   : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(true);

  selStart := srcCodeMemo.SelStart;
  line1 := posToLine(srcCodeMemo.Text,selStart);

  if srcCodeMemo.SelLength = 0 then begin
    lineStr := srcCodeMemo.Lines.Strings[line1];

    indentLine(lineStr,increaseIndent);

    srcCodeMemo.Lines.Strings[line1] := lineStr;
    Exit;
  end;

  selEnd   := srcCodeMemo.SelEnd;

  line2 := posToLine(srcCodeMemo.Text,selEnd);

  if line1 = -1 then Exit;

  if (line1 = line2) and (Trim(srcCodeMemo.Lines.Strings[line1]) = '') then Exit;

  while (line1 < line2) and (Trim(srcCodeMemo.Lines.Strings[line1]) = '') do begin
    Inc(line1);
  end;

  while (line2 > line1) and (Trim(srcCodeMemo.Lines.Strings[line2]) = '') do begin
    Dec(line2);
  end;

  repeat
    lineStr := srcCodeMemo.Lines.Strings[line1];

    indentLine(lineStr,increaseIndent);

    srcCodeMemo.Lines.Strings[line1] := lineStr;

    Inc(line1);
  until (line1 > line2);
end;

procedure TPas6502_Form.LineIndentdecrease_MenuItemClick(Sender: TObject);
begin
  indentLines(False);
end;

procedure TPas6502_Form.LineIndentincrease_MenuItemClick(Sender: TObject);
begin
  indentLines(True);
end;

procedure TPas6502_Form.ClosePage1Click(Sender: TObject);
begin
  if PageControl_SourceCode.ActivePageIndex = 0 then Exit;

  TFileInfo(TTabSheet(PageControl_SourceCode.ActivePage).Tag).Free;

  TTabSheet(PageControl_SourceCode.ActivePage).Free;
end;

procedure TPas6502_Form.Codeinsight1Click(Sender: TObject);
var
  codeInsightOptions : TcodeInsightOptions;
begin
  codeInsightOptions := TcodeInsightOptions.Create(nil);
  try
    codeInsightOptions.codeInsightTimer_MaskEdit.Text := IntToStr(CodeInsight_Timer.Interval);

    if codeInsightOptions.ShowModal = mrOk then begin
      CodeInsight_Timer.Interval := StrToInt('0'+codeInsightOptions.codeInsightTimer_MaskEdit.Text);
    end;
  finally
    codeInsightOptions.Free;
  end;
end;

procedure TPas6502_Form.codeInsight_ComboBoxKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  InsertionPos  : TBufferCoord;
  InsertionText : WideString;
  i             : Integer;
  srcCodeMemo   : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(true);

  if (key <> VK_RETURN) then Exit;

  codeInsight_ComboBox.visible := False;
  srcCodeMemo.SetFocus;

  InsertionText := codeInsight_ComboBox.Text;

  i := Pos(WideString(' '),InsertionText);

  if i > 1 then
  // typed part
    InsertionText := Copy(InsertionText,1,i - 1);

// InsertionPos  := srcCodeMemo.CaretXY;
//  srcCodeMemo.InsertBlock(InsertionPos,InsertionPos,PWideChar(InsertionText),True);
//  srcCodeMemo.SetFocus;
  SetClipboardText(InsertionText);
  srcCodeMemo.PasteFromClipboard;
  srcCodeMemo.Invalidate;
end;

procedure TPas6502_Form.NewUnit1Click(Sender: TObject);
var
  tabSheet : TTabSheet;
  synMemo  : TSynEdit;
  fileInfo : TFileInfo;
begin
  tabSheet := TTabSheet.Create(PageControl_SourceCode);
  tabSheet.Caption := 'Untitled.pas';
  tabSheet.PageControl := PageControl_SourceCode;

//  synMemo := newCodeEditor(tabSheet,tabSheet);

  synMemo := TSynEdit.Create(tabSheet);
  synMemo.Parent            := tabSheet;
  synMemo.Align             := alClient;
  synMemo.Options           := SourceCode_SynMemo.Options;// synEdit.Options + [eoAutoIndent,eoSmartTabs,eoTabsToSpaces];
  synMemo.ScrollBars        := ssBoth;
  synMemo.Highlighter       := SynPasSyn;
  synMemo.Font.Size         := 10;
  synMemo.Font.Name         := FONT_NAME;//'courier New';
  synMemo.Gutter := SourceCode_SynMemo.Gutter;
//  synEdit.Gutter.Visible    := True;
  synMemo.WantReturns       := True;
  synMemo.WantTabs          := True;
  synMemo.OnMouseMove       := SourceCode_SynMemoMouseMove;
  synMemo.OnClick           := SourceCode_SynMemoClick;
  synMemo.Color             := sourceCode_SynMemo.Color;

  PageControl_SourceCode.Invalidate;

  // set active page to newly added page
  PageControl_SourceCode.ActivePageIndex := PageControl_SourceCode.PageCount - 1;

  fileInfo := TFileInfo.Create(tabSheet.Caption,True,true);
  fileInfo.srcMemo := synMemo;

  TTabSheet(PageControl_SourceCode.ActivePage).Tag := Integer(fileInfo);
end;

procedure TPas6502_Form.New_ActionExecute(Sender: TObject);
var
  i        : Integer;
  fileInfo : TFileInfo;
  srcCodeMemo   : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(true);

  FCodeInsightData.Clear;

  fileInfo := TFileInfo(TTabSheet(PageControl_SourceCode.Pages[0]));
  FProjectFileName := 'Untitled.dpr';
  SetCaption(FProjectFileName);
  srcCodeMemo.Clear;
  srcCodeMemo.Tag := 1;

  fileInfo.isDirty  := False;
  fileInfo.fileName := FProjectFileName;

  // clear other tabs
  for i := 1 to PageControl_SourceCode.PageCount - 1 do
    PageControl_SourceCode.Pages[i].Free;
end;

function  TPas6502_Form.getSourceCodeMemo(useSrcCodeMemoOnly : Boolean) : TSynEdit;
begin
  if not useSrcCodeMemoOnly and AsmOutput_Memo.Focused then
    Result := AsmOutput_Memo
  else
  if PageControl_SourceCode.ActivePageIndex = 0 then
    Result := SourceCode_SynMemo
  else
    Result := TSynEdit(PageControl_SourceCode.ActivePage.Controls[0]);
end;

procedure TPas6502_Form.CodeInsight_TimerTimer(Sender: TObject);
var
  apos               : TPoint;
  caretxy            : TDisplayCoord;
  line               : AnsiString;
  codeInsightStrings : TStringDynArray;
  codeInsightData    : TCodeInsightData;
  insightData        : TCodeInsightData;
  data               : TCodeInsightData;
  insightStrings     : TStringDynArray;
  i                  : Integer;
  NewItem            : TMenuItem;
  dataInfo           : AnsiString;
  maxLength          : Integer;

  srcCodeMemo        : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(true);

  if srcCodeMemo.Text = '' then Exit;

  getInsightDataFromSource;

  CodeInsight_Timer.Enabled := False;
  caretxy.Column := srcCodeMemo.CaretX;
  caretxy.Row    := srcCodeMemo.CaretY;

  line := srcCodeMemo.LineText;

  if line = '' then Exit;

  if (line[caretxy.Column - 1] <> '.') then

  // not at a period right now so exit
    Exit;

  GetCodeInsightDataFromLine(line,caretxy.Column - 1,codeInsightStrings);

  if Length(codeInsightStrings) = 0 then Exit;

  insightData := getMatchingInsightData(codeInsightStrings);

  if insightData = nil then Exit;

  codeInsight_ComboBox.Clear;

  maxLength := 0;

  for i := 0 to FcodeInsightData.getCount - 1 do begin
    data := FcodeInsightData.getData(i);

    if Length(data.Name) > maxLength then
      maxLength := Length(data.Name);
  end;

  for i := 0 to insightData.getCount - 1 do begin
    data := insightData.getData(i);

    dataInfo := data.Name + StringOfChar(' ',maxLength-Length(data.Name)) + getSymbolInfo(TSymbol(data.extraData));
//    dataInfo := data.Name;//getSymbolInfo(TSymbol(data.extraData)) +

    codeInsight_ComboBox.Items.Add(dataInfo);
  //codeInsightStrings := insightData.GetDataAsArray;

//  if length(codeInsightStrings) = 0 then Exit;

    //codeInsight_ComboBox.Items.Add(codeInsightStrings[i]);
  end;

  if codeInsight_ComboBox.Items.Count = 0 then Exit;

//  apos := SourceCode_SynMemo.ClientToScreen(SourceCode_SynMemo.RowColumnToPixels(caretxy));
  apos := self.ScreenToClient(srcCodeMemo.ClientToScreen(srcCodeMemo.RowColumnToPixels(caretxy)));

  codeInsight_ComboBox.Left := apos.X;
  codeInsight_ComboBox.Top  := apos.Y;
  codeInsight_ComboBox.Visible := True;
  codeInsight_ComboBox.ItemIndex := 0;
  codeInsight_ComboBox.Invalidate;
  codeInsight_ComboBox.SetFocus;

  Exit;

  SetLength(insightStrings,0);
  insightStrings := FCodeInsightData.GetDataAsArray;
  i := 0;
  repeat
    insightData := FCodeInsightData.GetDataByName(codeInsightStrings[i]);
    if (insightData <> Nil) then begin
      insightStrings := insightData.GetDataAsArray;
    end
    else begin
      SetLength(insightStrings,0);
      Break;
    end;
    Inc(i);
  until i > High(codeInsightStrings);

//  apos := srcCodeMemo.ClientToScreen(srcCodeMemo.RowColumnToPixels(caretxy));
  apos := self.ScreenToClient(srcCodeMemo.ClientToScreen(srcCodeMemo.RowColumnToPixels(caretxy)));

  CodeInsight_PopupMenu.Items.Clear;
  for i := 0 to High(insightStrings) do begin
    NewItem := TMenuItem.Create(Nil);
    NewItem.Caption := insightStrings[i];
    NewItem.OnClick := CodeInsight_OnClick;
    CodeInsight_PopupMenu.Items.Add(NewItem);
  end;
  CodeInsight_PopupMenu.Popup(apos.X,apos.y);
end;

procedure TPas6502_Form.Compile1Click(Sender: TObject);
begin
  PageControl_SourceCode.ActivePage := PageControl_SourceCode.Pages[0];
  // put focus back onto source code memo
  SourceCode_SynMemo.SetFocus;
  CompileProgram(false);
end;

procedure TPas6502_Form.CompileandRun1Click(Sender: TObject);
begin
  PageControl_SourceCode.ActivePage := PageControl_SourceCode.Pages[0];
  // put focus back onto source code memo
  SourceCode_SynMemo.SetFocus;
  CompileProgram(true);
end;

procedure TPas6502_Form.CompileAndRun_ButtonClick(Sender: TObject);
begin
  PageControl_SourceCode.ActivePage := PageControl_SourceCode.Pages[0];
  // put focus back onto source code memo
  SourceCode_SynMemo.SetFocus;
  CompileProgram(true);
end;
procedure TPas6502_Form.Compile_ButtonClick(Sender: TObject);
begin
  PageControl_SourceCode.ActivePage := PageControl_SourceCode.Pages[0];
  // put focus back onto source code memo
  SourceCode_SynMemo.SetFocus;
  CompileProgram(false);
end;

procedure TPas6502_Form.copyImportsToProjectFolder(ProjectFolder : AnsiString; aProg : TProgram);
const
  cImportNames : array[0..1] of AnsiString = ('.asm','.ini');

var
  i : Integer;
  srcFile : AnsiString;
  dstFile : AnsiString;
  a       : DWord;
  name    : AnsiString;
begin
  name := ExtractFilePath(ParamStr(0))+'include\platform\'+getTargetName(aProg.target)+'\'+getTargetName(aProg.target);

  for i := 0 to High(cImportNames) do begin
    srcFile := name + cImportNames[i];

    if FileExists(srcFile) then begin
      dstFile := ExtractFilePath(FprojectFileName)+ExtractFileName(srcFile);

      CopyFile(PWideChar(srcFile),
               PWideChar(dstFile),False);
    end;
  end;
end;

procedure TPas6502_Form.convertBinToXEX(address : AnsiString; srcFile,dstFile : AnsiString);
var
  srcStream,dstStream : TFileStream;
  codeStart           : Word;
  codeEnd             : Word;
  srcBuffer           : Array of Byte;
  srcSize             : Word;
  value               : Byte;
begin
  srcStream := TFileStream.Create(srcFile,fmOpenRead);
  try
    srcSize := srcStream.Size;

    // skip address bit
    SetLength(srcBuffer,srcSize);
    srcStream.Read(srcBuffer[0],srcSize);
  finally
    srcStream.Free;
  end;

  dstStream := TFileStream.Create(dstFile,fmCreate);
  try
    // write $ffff to the stream
    value := $ff;
    dstStream.Write(value,1);
    dstStream.Write(value,1);

    codeStart := StrToInt(address);
    codeEnd   := codeStart + srcSize - 1;

    // write the code start address to the stream
    value := lo(codeStart);
    dstStream.Write(value,1);
    value := hi(codeStart);
    dstStream.Write(value,1);

    // write the code end address to the stream
    value := lo(codeEnd);
    dstStream.Write(value,1);
    value := hi(codeEnd);
    dstStream.Write(value,1);

    // write the program binary to the stream
    dstStream.Write(srcBuffer[0],srcSize);
  finally
    dstStream.Free;
  end;
end;

function  TPas6502_Form.compileAtari8BittoXEX(aProg : TProgram) : Boolean;
var
  compileBatFile : TStringList;
  mkImageFile    : AnsiString;
  asmFileName    : AnsiString;
  binFileName    : AnsiString;
  fileNameNoExt  : AnsiString;
  ProjectFolder  : AnsiString;
  progName       : AnsiString;
  outputPath     : AnsiString;
  fileInfo       : TStringList;
begin
  asmFileName   := ChangeFileExt(FProjectFileName,'.asm');
  binFileName   := ChangeFileExt(FProjectFileName,'.bin');
  binFileName   := StringReplace(binFileName,' ','_',[rfReplaceAll]);
  fileNameNoExt := ChangeFileExt(FProjectFileName,'');
  ProjectFolder := ExtractFilePath(FProjectFileName);
  copyImportsToProjectFolder(ProjectFolder,aProg);
  AsmOutput_Memo.Lines.SaveToFile(asmFileName);

  ClearArgs;
  AddArg('"'+FKickAssemblerJarFile+'"');
  AddArg('"'+asmFileName+'"');
  AddArg('"'+binFileName+'"');
  Result := RunProcess(extractFilePath(paramStr(0))+'compileBin.bat',args);

  if not FileExists(binFileName) then begin
    Error_Memo.Lines.Add('-----------------------------------------------------------');
    Error_Memo.Lines.Add(Format('.bin file "%s" not created, check for errors in .asm file!',[binFileName]));
    Error_Memo.Lines.Add('-----------------------------------------------------------');
    Exit;
  end;

  convertBinToXEX('$'+aProg.loadAddress,binFileName,fileNameNoExt+'.xex');
end;

function  TPas6502_Form.generateDiscImage(aOutputPath,aProgName : AnsiString; aProg : TProgram) : Boolean;
var
  progFileName : AnsiString;
  fileInfo     : TStringList;
  path         : AnsiString;
begin
  //aOutputPath := StringReplace(aOutputPath,'\\','\',[rfReplaceAll]);

  fileInfo := TStringList.Create;
  try
    // create the .inf file
    fileInfo.Add(aProgName+' FFFF'+aProg.loadAddress+' FFFF'+aProg.loadAddress);
    fileInfo.SaveToFile(aOutputPath+'myfiles\'+aProgName+'.inf');

    // create the "filelist.txt" file
    fileInfo.Clear;
    fileInfo.Add('!BOOT');
    fileInfo.Add(aProgName);
    fileInfo.SaveToFile(aOutputPath+'myfiles\'+'filelist.txt');

    // create the "!BOOT" file
    fileInfo.Clear;
    fileInfo.Add('*BASIC');
    fileInfo.Add('*RUN '+aProgName);
    fileInfo.SaveToFile(aOutputPath+'myfiles\!BOOT');

    // create the "make-ssd.bat" file
    fileInfo.Clear;
    fileInfo.Add(ExtractFilePath(ParamStr(0))+'utilities\mkimg.exe -fs DFS -size 200K '+aProgName+'.ssd myfiles -opt 3 -i@myfiles\filelist.txt -title "'+aProgName+'" -pad');
    fileInfo.SaveToFile(aOutputPath+'make-ssd.bat');

    // create the "run-sdd.bat" file
    fileInfo.Clear;
    fileInfo.Add(FBBCEmulatorFile+' "'+aOutputPath + aProgName+'.ssd"');
    fileInfo.SaveToFile(aOutputPath+'run-ssd.bat');
  finally
    fileInfo.Free;
  end;
  // generate the .ssd file
  ClearArgs;
  GetDir(0,path);
  ChDir(aOutputPath);
  DeleteFile(aOutputPath+'myfiles\'+aProgName+'.ssd');
  Result := RunProcess(aOutputPath+'make-ssd.bat',args);

  ChDir(ExtractFilePath(ParamStr(0)));

end;

function  TPas6502_Form.compileAndCreateBBCDiskImage(aProg : TProgram) : Boolean;
var
  compileBatFile : TStringList;
  mkImageFile    : AnsiString;
  asmFileName    : AnsiString;
  ProjectFolder  : AnsiString;
  progName       : AnsiString;
  outputPath     : AnsiString;
  fileInfo       : TStringList;
begin
  asmFileName   := ChangeFileExt(FProjectFileName,'.asm');
  mkImageFile   := ExtractFilePath(ParamStr(0))+'utilities\mkimg.exe';
  ProjectFolder := ExtractFilePath(FProjectFileName);
  progName      := ExtractFileName(ChangeFileExt(UpperCase(FProjectFileName),''));
  progName      := StringReplace(UpperCase(progName),' ','',[rfReplaceAll]);
  // max filename length of 7 characters
  progName      := LeftStr(progName,Min(7,Length(progName)));

  outputPath := ExtractFilePath(asmFileName)+progName+'\';
  // create the outputpath and sub-folders
  ForceDirectories(outputPath);
  ForceDirectories(outputPath+'myfiles');

  copyImportsToProjectFolder(ProjectFolder,aProg);
  AsmOutput_Memo.Lines.SaveToFile(asmFileName);

  compileBatFile := TStringList.Create;
  try
    compileBatFile.LoadFromFile(ExtractFilePath(ParamStr(0))+'utilities\compileAndCreateBBCDiskImage_skeleton.bat');

    // replace the variables with the expected values
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%kickassemfile%',FKickAssemblerJarFile,[rfReplaceAll]);
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%mkimage%',mkImageFile,[rfReplaceAll]);
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%srcprogname%',asmFileName,[rfReplaceAll]);
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%progname%',progName,[rfReplaceAll]);
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%progaddr%',aProg.loadAddress,[rfReplaceAll]);

    // save in new location
    compileBatFile.SaveToFile(outputPath+'compileAndCreateBBCDiskImage.bat');
    clearArgs;
    ChDir(outputPath);
    Result := RunProcess(outputPath+'compileAndCreateBBCDiskImage.bat',args);

    // create the "run-sdd.bat" file
    fileInfo := TStringList.Create;
    try
      fileInfo.Clear;
      fileInfo.Add(FBBCEmulatorFile+' "'+outputPath+progName + '.ssd"');
      fileInfo.SaveToFile(outputPath+'run-ssd.bat');
    finally
      fileInfo.Free;
    end;
  finally
    compileBatFile.Free;
  end;

end;

function  TPas6502_Form.compileAndCreateAppleIIDiskImage(aProg : TProgram) : Boolean;
var
  compileBatFile     : TStringList;
  appleCommanderFile : AnsiString;
  asmFileName        : AnsiString;
  ProjectFolder      : AnsiString;
  progName           : AnsiString;
  outputPath         : AnsiString;
  origin             : AnsiString;
  enableboot         : AnsiString;
  originInt          : Integer;
  fileInfo           : TStringList;
begin
  asmFileName        := ChangeFileExt(FProjectFileName,'.asm');
  appleCommanderFile := ExtractFilePath(ParamStr(0))+'utilities\apple\AppleCommander-ac-1.5.0.jar';
  ProjectFolder      := ExtractFilePath(FProjectFileName);
  progName           := ExtractFileName(ChangeFileExt(UpperCase(FProjectFileName),''));
  progName           := StringReplace(UpperCase(progName),' ','',[rfReplaceAll]);
  // max filename length of 7 characters
  progName      := LeftStr(progName,Min(7,Length(progName)));

  originInt     := StrToInt('$'+aProg.loadAddress);// - 2;

  origin        := '0x'+IntToHex(originInt,4);

  enableboot := '';

//  if aProg.target = ttAppleii then
//    enableboot := 'rem ';

  outputPath := ExtractFilePath(asmFileName);//+progName+'\';
  // create the outputpath and sub-folders
  ForceDirectories(outputPath);
//  ForceDirectories(outputPath+'myfiles');

  copyImportsToProjectFolder(ProjectFolder,aProg);
  AsmOutput_Memo.Lines.SaveToFile(asmFileName);

  compileBatFile := TStringList.Create;
  try
    compileBatFile.LoadFromFile(ExtractFilePath(ParamStr(0))+'utilities\compileAndCreateAppleIIDiskImage_skeleton.bat');

    // replace the variables with the expected values
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%kickassemfile%',FKickAssemblerJarFile,[rfReplaceAll]);
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%applecommander%',appleCommanderFile,[rfReplaceAll]);
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%srcprogname%',asmFileName,[rfReplaceAll]);
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%progname%',progName,[rfReplaceAll]);
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%origin%',origin,[rfReplaceAll]);
    compileBatFile.Text := StringReplace(compileBatFile.Text,'%enableboot%',enableboot,[rfReplaceAll]);

    // save in new location
    compileBatFile.SaveToFile(outputPath+'compileAndCreateAppleIIDiskImage.bat');
    clearArgs;
    ChDir(outputPath);
    Result := RunProcess(outputPath+'compileAndCreateAppleIIDiskImage.bat',args);

    // create the "run-sdd.bat" file
    fileInfo := TStringList.Create;
    try
      fileInfo.Clear;
      fileInfo.Add(FAppleIIEmulatorFile+' "'+outputPath+progName + '.dsk"');
      fileInfo.SaveToFile(outputPath+'run-dsk.bat');
    finally
      fileInfo.Free;
    end;
  finally
    compileBatFile.Free;
  end;
end;

const
  ATARI_8BIT = [
    ttAtari8Bit
//    ttAtari400,
//    ttAtari800,
//    ttAtari1200XL,
//    ttAtari5200,
//    ttAtariXEGS,
//    ttAtariXL,
//    ttAtariXE
  ];

function  TPas6502_Form.CreateAndCompileAsmFile(aProg : TProgram) : Boolean;
var
  asmFileName   : AnsiString;
  ProjectFolder : AnsiString;
  progName      : AnsiString;
  outputPath    : AnsiString;
  fileInfo      : TStringList;
begin
  if aProg.target in [ttAppleII] then begin
    Result := compileAndCreateAppleIIDiskImage(aProg);
    Exit;
  end
  else
  if aProg.target in [ttBBC] then begin
    Result := compileAndCreateBBCDiskImage(aProg);
    Exit;
  end
  else
  if aProg.target in ATARI_8BIT then begin
    Result := compileAtari8BittoXEX(aProg);
    Exit;
  end;

  asmFileName  := ChangeFileExt(FProjectFileName,'.asm');
  ProjectFolder := ExtractFilePath(FProjectFileName);
  copyImportsToProjectFolder(ProjectFolder,aProg);
  AsmOutput_Memo.Lines.SaveToFile(asmFileName);

  ClearArgs;
  AddArg('"'+FKickAssemblerJarFile+'"');
  AddArg('"'+asmFileName+'"');

  if aProg.target = ttNES then
    Result := RunProcess(extractFilePath(paramStr(0))+'compileNES.bat',args)
  else
    Result := RunProcess(extractFilePath(paramStr(0))+'compile.bat',args);
end;

function  TPas6502_Form.PackCompiledProgram : Boolean;
var
  prgFileName    : AnsiString;
  packedFileName : AnsiString;
  ProjectFolder  : AnsiString;
begin
  prgFileName    := ChangeFileExt(FProjectFileName,'.prg');
  packedFileName := ChangeFileExt(FProjectFileName,'.exo');
  ClearArgs;
  AddArg('"'+FExomizerExeFile+'"');
  AddArg('"'+prgFileName+'"');
  AddArg('"'+packedFileName+'"');
  AddArg('"'+ExtractFilePath(FProjectFileName)+'"');
  AddArg('"'+ChangeFileExt(ExtractFileName(FProjectFileName),'')+'"');
  Result := RunProcess(extractFilePath(paramStr(0))+'pack.bat',args);
end;

procedure TPas6502_Form.PageControl_SourceCodeContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
var
  p : TPoint;
begin
  if PageControl_SourceCode.ActivePageIndex = 0 then
  // can't close project tab
    Exit;

  p := PageControl_SourceCode.ClientToScreen(Point(MousePos.X, MousePos.Y));

  if htOnItem in PageControl_SourceCode.GetHitTestInfoAt(MousePos.X, MousePos.Y) then
    Tab_PopupMenu.Popup(p.X, p.Y);
end;

procedure TPas6502_Form.Paths1Click(Sender: TObject);
var
  IniFile : TIniFile;
begin
  if (OptionsDialog.ShowModal = mrOK) then begin
    FAtari8BitEmulatorFile := OptionsDialog.Atari8BitFile_Edit.Text;
    FBBCEmulatorFile       := OptionsDialog.BBC_EmulatorFile_Edit.Text;
    FViceX64ExeFile        := OptionsDialog.X64File_Edit.Text;
    FViceXvicExeFile       := OptionsDialog.vic20File_Edit.Text;
    FViceX128ExeFile       := OptionsDialog.X128File_Edit.Text;
    FNESExeFile            := OptionsDialog.NESfile_Edit.Text;
    FAppleIIEmulatorFile   := OptionsDialog.AppleIIFile_Edit.Text;
    FViceXPETExeFile       := OptionsDialog.petFile_Edit.Text;

    IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Pas6502.ini');

    IniFile.WriteString('setup','Atari8BitEmulatorFileName' ,'"'+FAtari8BitEmulatorFile+'"');
    IniFile.WriteString('setup','BBCMicroEmulatorFileName'  ,'"'+FBBCEmulatorFile+'"');
    IniFile.WriteString('setup','VICEEmulatorFileName'      ,'"'+FViceX64ExeFile+'"');
    IniFile.WriteString('setup','VICEvic20EmulatorFileName' ,'"'+FViceXvicExeFile+'"');
    IniFile.WriteString('setup','VICE128EmulatorFileName'   ,'"'+FViceX128ExeFile+'"');
    IniFile.WriteString('setup','NESemulatorFileName'       ,'"'+FNESExeFile+'"');
    IniFile.WriteString('setup','AppleiiEmulatorFileName'   ,'"'+FAppleIIEmulatorFile+'"');
    IniFile.WriteString('setup','VICEPETEmulatorFileName'   ,'"'+FViceXPETExeFile+'"');

    IniFile.WriteString('setup','KickAssemblerFileName'     ,'"'+FKickAssemblerJarFile+'"');
    IniFile.WriteString('setup','ExomizerFileName'          ,'"'+FExomizerExeFile+'"');

    IniFile.Free;
  end;
end;

procedure TPas6502_Form.ExecuteCompiledAsmFile(aProg : TProgram);
var
  prgFileName : AnsiString;
  outputPath  : AnsiString;
  progName    : AnsiString;
  execBatch   : AnsiString;
  msg         : AnsiString;
begin
  if (aProg.target in [ttAppleII]) then begin
    if not FileExists(FAppleIIEmulatorFile) then begin
      msg := Format('"%s" not found'#13#10,[FAppleIIEmulatorFile]);
      msg := 'Please use "Options/paths" to configure where the emulators are found:'#13#10 + msg;

      ShowMessage(msg);
      Exit;
    end;
    progName    := ExtractFileName(ChangeFileExt(UpperCase(FProjectFileName),''));
    progName    := StringReplace(UpperCase(progName),' ','',[rfReplaceAll]);
    // max filename length of 7 characters
    progName      := LeftStr(progName,Min(7,Length(progName)));

    outputPath := ExtractFilePath(FProjectFileName);//+progName+'\';
    execBatch  := outputPath + 'run-dsk.bat';

    prgFileName := outputPath + progName + '.dsk';

    ClearArgs;
//    AddArg(prgFileName);
    RunProcess(execBatch,args);
//    ShellExecute(Handle, 'open', PChar('"'+execBatch+'"'), nil, nil, SW_SHOW);
  end
  else
  if (aProg.target in [ttBBC]) then begin
    if not FileExists(FBBCEmulatorFile) then begin
      msg := Format('"%s" not found'#13#10,[FBBCEmulatorFile]);
      msg := 'Please use "Options/paths" to configure where the emulators are found:'#13#10 + msg;

      ShowMessage(msg);
      Exit;
    end;
    progName    := ExtractFileName(ChangeFileExt(UpperCase(FProjectFileName),''));
    progName    := StringReplace(UpperCase(progName),' ','',[rfReplaceAll]);
    // max filename length of 7 characters
    progName      := LeftStr(progName,Min(7,Length(progName)));

    outputPath := ExtractFilePath(FProjectFileName)+progName+'\';
    execBatch  := outputPath + 'run-ssd.bat';

    prgFileName := outputPath + progName + '.ssd';

    ClearArgs;
//    AddArg(prgFileName);
    RunProcess(execBatch,args);
//    ShellExecute(Handle, 'open', PChar('"'+execBatch+'"'), nil, nil, SW_SHOW);
  end
  else
  if (aProg.target in ATARI_8BIT) then begin
    if not FileExists(FAtari8BitEmulatorFile) then begin
      msg := Format('"%s" not found'#13#10,[FAtari8BitEmulatorFile]);
      msg := 'Please use "Options/paths" to configure where the emulators are found:'#13#10 + msg;

      ShowMessage(msg);
      Exit;
    end;
    prgFileName  := ChangeFileExt(FProjectFileName,'.xex');
    ClearArgs;

    AddArg('"'+FAtari8BitEmulatorFile+'"');
    AddArg('"'+prgFileName+'"');
    RunProcess(ExtractFilePath(ParamStr(0))+'executeXEX.bat',args);
  end
  else
  if (aProg.target = ttC128) then begin
    if not FileExists(FViceX128ExeFile) then begin
      msg := Format('"%s" not found'#13#10,[FViceX128ExeFile]);
      msg := 'Please use "Options/paths" to configure where the emulators are found:'#13#10 + msg;

      ShowMessage(msg);
      Exit;
    end;
    prgFileName  := ChangeFileExt(FProjectFileName,'.prg');
//    if PackFile_CheckBox.Checked then prgFileName  := ChangeFileExt(FProjectFileName,'.exo');
    ClearArgs;
    AddArg('"'+FViceX128ExeFile+'"');
    AddArg('"'+prgFileName+'"');
    RunProcess(ExtractFilePath(ParamStr(0))+'executePrg.bat',args);
  end
  else
  if (aProg.target = ttPET) then begin
    if not FileExists(FViceXPETExeFile) then begin
      msg := Format('"%s" not found'#13#10,[FViceXPETExeFile]);
      msg := 'Please use "Options/paths" to configure where the emulators are found:'#13#10 + msg;

      ShowMessage(msg);
      Exit;
    end;
    prgFileName  := ChangeFileExt(FProjectFileName,'.prg');
//    if PackFile_CheckBox.Checked then prgFileName  := ChangeFileExt(FProjectFileName,'.exo');
    ClearArgs;
    AddArg('"'+FViceXPETExeFile+'"');
    AddArg('"'+prgFileName+'"');
    RunProcess(ExtractFilePath(ParamStr(0))+'executePrg.bat',args);
  end
  else
  if (aProg.target = ttVic20) then begin
    if not FileExists(FViceXvicExeFile) then begin
      msg := Format('"%s" not found'#13#10,[FViceXvicExeFile]);
      msg := 'Please use "Options/paths" to configure where the emulators are found:'#13#10 + msg;

      ShowMessage(msg);
      Exit;
    end;
    prgFileName  := ChangeFileExt(FProjectFileName,'.prg');
//    if PackFile_CheckBox.Checked then prgFileName  := ChangeFileExt(FProjectFileName,'.exo');
    ClearArgs;
    AddArg('"'+FViceXvicExeFile+'"');
    AddArg('"'+prgFileName+'"');
    RunProcess(ExtractFilePath(ParamStr(0))+'executePrg.bat',args);
  end
  else
  if (aProg.target = ttNES) then begin
    if not FileExists(FNESExeFile) then begin
      msg := Format('"%s" not found'#13#10,[FNESExeFile]);
      msg := 'Please use "Options/paths" to configure where the emulators are found:'#13#10 + msg;

      ShowMessage(msg);
      Exit;
    end;
    prgFileName  := ChangeFileExt(FProjectFileName,'.nes');
    prgFileName  := StringReplace(prgFileName,' ','_',[rfReplaceAll]);

//    if PackFile_CheckBox.Checked then prgFileName  := ChangeFileExt(FProjectFileName,'.exo');
    ClearArgs;
    AddArg('"'+FNESExeFile+'"');
    AddArg('"'+prgFileName+'"');
    RunProcess(ExtractFilePath(ParamStr(0))+'executeNES.bat',args);
  end
  else begin
  // do c64 target
    if not FileExists(FViceX64ExeFile) then begin
      msg := Format('"%s" not found'#13#10,[FViceX64ExeFile]);
      msg := 'Please use "Options/paths" to configure where the emulators are found:'#13#10 + msg;

      ShowMessage(msg);
      Exit;
    end;
    prgFileName  := ChangeFileExt(FProjectFileName,'.prg');
//    if PackFile_CheckBox.Checked then prgFileName  := ChangeFileExt(FProjectFileName,'.exo');
    ClearArgs;
    AddArg('"'+FViceX64ExeFile+'"');
    AddArg('"'+prgFileName+'"');
    RunProcess(ExtractFilePath(ParamStr(0))+'executePrg.bat',args);
//    ShellExecute(Handle, PChar('open'), PChar('"'+FViceX64ExeFile+'"'), PChar('-autoload "'+prgFileName+'"'), nil, SW_SHOW);
//    ShellExecute(Handle, 'open', PChar('"'+FViceX64ExeFile+'"'), PChar('-autostartprgmode 1 "'+prgFileName+'"'), nil, SW_SHOW);
  end;
end;
procedure TPas6502_Form.Exit1Click(Sender: TObject);
begin
  halt;
end;

{procedure TPas6502_Form.CompileProgram(ExecuteProgram : Boolean);
var
  srcStream : TMemoryStream;
  asmStream : TMemoryStream;
  parser    : TBaseParser;
  progAST   : TAST;
  codeGen   : TASTto6502;
  cursor    : TCursor;
begin
  if not FileExists(FKickAssemblerJarFile) or not FileExists(FViceX64ExeFile) or not FileExists(FExomizerExeFile) then begin
    ShowMessage('Use "Options" to configure where the Kick Assembler, VICE emulator, and Exomizer files are found!');
    Exit;
  end;
  AsmOutput_Memo.Clear;
  Error_Memo.Text := '';
  cursor := Screen.Cursor;
  try
    Screen.Cursor := crHourGlass;
    parser := TParserPas6502v2.Create(SourceCode_SynMemo.Lines,False);
    try
      srcStream := TMemoryStream.Create;
      asmStream := TMemoryStream.Create;
      try
        SourceCode_SynMemo.Lines.SaveToStream(srcStream);
        parser.ProjectPath := ExtractFilePath(FProjectFileName);
        parser.ParserPath  := ExtractFilePath(ParamStr(0));
        if not(parser.ParseProgram(srcStream,asmStream)) then
          Error_Memo.Text := parser.ErrorMsg
        else begin
          Error_Memo.Text := 'Compile successful!';
          Error_Memo.Lines.Add('');
          AsmOutput_Memo.Clear;
          AsmOutput_Memo.Lines.LoadFromStream(asmStream);
          if (CreateAndCompileAsmFile) then begin
            if PackFile_CheckBox.Checked then PackCompiledProgram;
            if ExecuteProgram then ExecuteCompiledAsmFile;
          end;
        end;
      finally
        srcStream.Free;
        asmStream.Free;
      end;
    finally
      parser.Free;
    end;
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

function  symbolIsVolatile(aName : AnsiString; symbolTable : TScopedSymbolTable) : Boolean;
var
  sym : TSymbol;
begin
  Result := true;

  sym := symbolTable.lookup(aName);

  if (sym is TProcSymbol) and TProcSymbol(sym).isVolatile then Exit;
  if (sym is TFuncSymbol) and TFuncSymbol(sym).isVolatile then Exit;

  Result := False;
end;

procedure TPas6502_Form.removeDoubleBlankLines(asmStream : TMemoryStream);
var
  i,line,p    : Integer;
  asmList     : TStringList;
  optimised   : AnsiString;
  name        : AnsiString;
  asmText     : AnsiString;
  line1,line2 : AnsiString;
  notFound    : Boolean;
  sum         : TSymbol;
  isVolatile  : Boolean;
begin
  asmList := TStringList.Create;

  try
    // load the asm stream into the list ready for line deletion
    asmStream.Seek(0,soFromBeginning);
    asmList.Clear;
    asmList.LoadFromStream(asmStream);

    optimised := '';

    i := 0;
    while i < asmList.count - 2 do begin
      line1 := trim(asmList.Strings[i]);
      line2 := trim(asmList.Strings[i+1]);

      while (line1 = '') and (line2 = '') do begin
        asmList.Delete(i+1);

        line2 := trim(asmList.Strings[i+1]);
      end;

      Inc(i);
    end;

  finally
    asmStream.Clear;
    asmList.SaveToStream(asmStream);
    asmList.Free;
  end;
end;

procedure TPas6502_Form.removeUnusedRoutines(asmStream : TMemoryStream; parser : TObject);
var
  i,line,p    : Integer;
  info        : TRoutineInfo;
  asmList     : TStringList;
  optimised   : AnsiString;
  name        : AnsiString;
  asmText     : AnsiString;
  notFound    : Boolean;
  sum         : TSymbol;
  isVolatile  : Boolean;
begin
  asmList := TStringList.Create;

  try
    // load the asm stream into the list ready for line deletion
    asmStream.Seek(0,soFromBeginning);
    asmList.Clear;
    asmList.LoadFromStream(asmStream);

    optimised := '';

    for i := routines_getCount - 1 downto 0 do begin
      info := routines_getInfo(i);

      isVolatile := symbolIsVolatile(routines_getName(i),TParserAST(parser).scope);

      if not isVolatile and (info.refCount = 0) and (info.startLine <> -1) and (info.endLine <> -1)  then begin
//        if (info.startLine <> -1) and (info.endLine <> -1)  then begin
          if optimised = '' then
            Error_Memo.Lines.Add('Removing redundant routines stage starting...'+#13#10);

          optimised := optimised + Format('Removing unused routine "%s" from the asm file...'+#13#10,[routines_getName(i)]);

          for line := info.endLine downto info.startLine do
            asmList.Delete(line);
        end;
     end;

    if optimised <> '' then Error_Memo.Lines.Add(optimised);
  finally
    asmStream.Clear;
    asmList.SaveToStream(asmStream);
    asmList.Free;
  end;
end;

procedure TPas6502_Form.CompileProgram(ExecuteProgram : Boolean);
const
  cCompileSuccessful = 'Code parsing stage successful...';
var
  srcStream : TMemoryStream;
  asmStream : TMemoryStream;
  parser    : TParserAST;
  progAST   : TAST;
  codeGen   : TASTto6502;
  cursor    : TCursor;
  compiled  : Boolean;
  i,p       : Integer;
  asmLine   : AnsiString;
  fragment  : AnsiString;
  msg       : AnsiString;
begin
  if SourceCode_SynMemo.Lines.Count = 0 then begin
    Error_Memo.Text := 'No program to compile!!';
    Exit;
  end;

  AsmOutput_Memo.Clear;
  Error_Memo.Text := '';
  cursor := Screen.Cursor;
  try
    Screen.Cursor := crHourGlass;

    FCodeInsightData.clear;

    routines_init;
    routines_clear;

    parser := TParserAST.Create(FCodeInsightData,onScopeUpdate);
    parser.projectPath := ExtractFilePath(FProjectFileName);
    parser.currentFile := FProjectFileName;
    parser.srcLine     := getSourceCodeMemo(true).CaretY;  //SourceCode_SynMemo.CaretY;
    try
      srcStream := TMemoryStream.Create;
      asmStream := TMemoryStream.Create;
      try
        SourceCode_SynMemo.Lines.SaveToStream(srcStream);
        compiled := True;
        if not(parser.parse(srcStream,false)) then begin
          Error_Memo.Text := parser.errorMsg;
          compiled        := False;
        end
        else begin
          Error_Memo.Text := cCompileSuccessful;
          Error_Memo.Lines.Add('');

          codeGen := TASTto6502.Create;
          parser.progAST.fileName := ChangeFileExt(ExtractFileName(FprojectFileName),'');
          try
            Fwarnings := '';

            codeGen.genCode(parser.progAST,asmStream,routines_getList,Fwarnings);
          except
            on E : Exception do begin
              Error_Memo.Text := E.Message;
              compiled        := False;
            end;
          end;

          if compiled then begin
            Error_Memo.Lines.Add('Assembly generation successfull...');
            removeUnusedRoutines(asmStream,parser);
            removeDoubleBlankLines(asmStream);

            asmStream.Seek(0,soFromBeginning);
            AsmOutput_Memo.Clear;
            AsmOutput_Memo.Lines.LoadFromStream(asmStream);

            if (CreateAndCompileAsmFile(parser.progAST)) then begin
              Error_Memo.Lines.Add('Program file generation starting...');

              if PackFile_CheckBox.Checked then PackCompiledProgram;
              if ExecuteProgram then ExecuteCompiledAsmFile(parser.progAST);
            end;
          end;
        end;
      finally
        for i := AsmOutput_Memo.Lines.Count - 1 downto 0 do begin
          asmLine := AsmOutput_Memo.Lines[i];

          p := Pos('// fragment ',asmLine);

          if p > 0 then begin
            fragment := AsmOutput_Memo.Lines[i];

            fragment := Copy(fragment,p + 3,Length(fragment));

            if Pos(fragment,Fwarnings) = 0 then Fwarnings := Fwarnings + fragment+#13#10;
          end;
        end;

        if Fwarnings <> '' then
          Error_Memo.Lines.Add(#13#10+'*******************************'+#13#10+'WARNINGS:'+#13#10+Fwarnings);
        srcStream.Free;
        asmStream.Free;
      end;
    finally
      parser.Free;
    end;
  finally
    Screen.Cursor := Cursor;
    routines_cleanup;
  end;
end;

procedure TPas6502_Form.SearchAgain1Click(Sender: TObject);
begin
//  ffindReplace.Options := ffindReplace.Options + [froDown];
//  ffindReplace.ExecuteFind;
end;

procedure TPas6502_Form.SetCaption(ACaption : AnsiString);
begin
  Caption := 'Pas6502 Compiler - "' + ExtractFileName(ACaption) + '"';
  PageControl_SourceCode.ActivePage.Caption := ExtractFileName(ACaption);
end;

procedure TPas6502_Form.Showcodesuggestions1Click(Sender: TObject);
var
  suggestions : TStringDynArray;
  apos        : TPoint;
  caretxy     : TDisplayCoord;
  i           : Integer;
  data        : TCodeInsightData;
  dataInfo    : AnsiString;
  maxLength   : Integer;
  srcCodeMemo   : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(true);

  if srcCodeMemo.Text = '' then Exit;

  getInsightDataFromSource;

  caretxy.Column := srcCodeMemo.CaretX;
  caretxy.Row    := srcCodeMemo.CaretY;

  suggestions := FcodeInsightData.GetDataAsArray;

  codeInsight_ComboBox.Clear;

  maxLength := 0;

  for i := 0 to FcodeInsightData.getCount - 1 do begin
    data := FcodeInsightData.getData(i);

    if Length(data.Name) > maxLength then
      maxLength := Length(data.Name);
  end;

  for i := 0 to FcodeInsightData.getCount - 1 do begin
    data := FcodeInsightData.getData(i);

    dataInfo := data.Name + StringOfChar(' ',maxLength-Length(data.Name)) +'(' +getSymbolInfo(TSymbol(data.extraData))+')';

    codeInsight_ComboBox.Items.Add(dataInfo);
  //codeInsightStrings := insightData.GetDataAsArray;

//  if length(codeInsightStrings) = 0 then Exit;

    //codeInsight_ComboBox.Items.Add(codeInsightStrings[i]);
  end;
//  for i := 0 to High(suggestions) do begin
//    codeInsight_ComboBox.Items.Add(suggestions[i]);
//  end;

//  apos := SourceCode_SynMemo.ClientToScreen(SourceCode_SynMemo.RowColumnToPixels(caretxy));
//  apos := srcCodeMemo.RowColumnToPixels(caretxy);
  apos := ScreenToClient(srcCodeMemo.ClientToScreen(srcCodeMemo.RowColumnToPixels(caretxy)));

  if codeInsight_ComboBox.Items.Count = 0 then Exit;

  codeInsight_ComboBox.Left := apos.X;
  codeInsight_ComboBox.Top  := apos.Y;
  codeInsight_ComboBox.Visible := True;
  codeInsight_ComboBox.ItemIndex := 0;
  codeInsight_ComboBox.Invalidate;
  codeInsight_ComboBox.SetFocus;
end;

procedure TPas6502_Form.Find1Click(Sender: TObject);
var
  srcCodeMemo   : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(false);

  ffindReplace.Editor := srcCodeMemo;
  ffindReplace.setToFind;

  ffindReplace.show;
end;

procedure TPas6502_Form.FindandReplace1Click(Sender: TObject);
var
  srcCodeMemo   : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(false);

  ffindReplace.Editor := srcCodeMemo;
  ffindReplace.setToFindReplace;

  ffindReplace.show;
end;

function RemoveAmpersand(ACaption : AnsiString) : AnsiString;
begin
  Result := StringReplace(ACaption,'&','',[rfReplaceAll]);
end;

procedure TPas6502_Form.tutorialClick(sender : TObject);
var
  tutorialFile : AnsiString;
  fileInfo     : TFileInfo;
begin
  if Application.MessageBox('Are you sure, all open data will be lost...','Open Example project?',MB_OKCANCEL) = IDCANCEL then Exit;

  tutorialFile := ExtractFilePath(ParamStr(0)) + cExamplesFolder+RemoveAmpersand(TMenuItem(sender).Parent.Caption+TMenuItem(sender).Caption);

  if not FileExists(ChangeFileExt(tutorialFile,'.dpr')) then Exit;

  SourceCode_SynMemo.Lines.LoadFromFile(ChangeFileExt(tutorialFile,'.dpr'));

  FProjectFileName := ChangeFileExt(tutorialFile,'.dpr');
  fileInfo := TFileInfo(TTabSheet(PageControl_SourceCode.ActivePage).Tag);
  fileInfo.fileName := FProjectFileName;

  SetCaption(FProjectFileName);

  updateMostRecentlyUsedFilesList(FProjectFileName);

  SourceCode_SynMemo.SetFocus;

  getInsightDataFromSource;
end;

procedure TPas6502_Form.mruFilesClick(sender : TObject);
var
  recentFile : AnsiString;
  fileInfo   : TFileInfo;
begin
  if Application.MessageBox('Are you sure, all open data will be lost...','Open recent file?',MB_OKCANCEL) = IDCANCEL then Exit;

  recentFile := TmruFile(FmruFiles.Items[TMenuItem(sender).Tag]).fileName;

  if not FileExists(ChangeFileExt(recentFile,'.dpr')) then Exit;

  SourceCode_SynMemo.Lines.LoadFromFile(ChangeFileExt(recentFile,'.dpr'));

  FProjectFileName := ChangeFileExt(recentFile,'.dpr');
  fileInfo := TFileInfo(TTabSheet(PageControl_SourceCode.ActivePage).Tag);
  fileInfo.fileName := FProjectFileName;

  SetCaption(FProjectFileName);

  SourceCode_SynMemo.SetFocus;

  getInsightDataFromSource;
end;

type
  TFileList = array of AnsiString;

procedure GetFilesAt(const Root,match: AnsiString; var filesList : TFileList);
var
  SearchRec : TSearchRec;
  Folders   : array of AnsiString;
  Folder    : AnsiString;
  I         : Integer;
  Last      : Integer;
begin
  SetLength(Folders, 1);
  Folders[0] := Root;
  I := 0;
  while (I < Length(Folders)) do
  begin
    Folder := IncludeTrailingBackslash(Folders[I]);
    Inc(I);
    { Collect child folders first. }
    if (FindFirst(Folder + '*.*', faDirectory, SearchRec) = 0) then
    begin
      repeat
        if not ((SearchRec.Name = '.') or (SearchRec.Name = '..')) then
        begin
          Last := Length(Folders);
          SetLength(Folders, Succ(Last));
          Folders[Last] := Folder + SearchRec.Name;
        end;
      until (FindNext(SearchRec) <> 0);
      FindClose(SearchRec);
    end;
    { Collect files next.}
    if (FindFirst(Folder + match, faAnyFile - faDirectory, SearchRec) = 0) then
    begin
      repeat
        if not ((SearchRec.Attr and faDirectory) = faDirectory) then
        begin
          SetLength(filesList,Length(filesList)+1);
          filesList[high(filesList)] := Folder+SearchRec.Name;
//          WriteLn(Folder, SearchRec.Name);
        end;
      until (FindNext(SearchRec) <> 0);
      FindClose(SearchRec);
    end;
  end;
end;
procedure TPas6502_Form.loadTutorialFilesList;
var
  sr        : TSearchRec;
  FileAttrs : Integer;
  subItem   : TMenuItem;

  filesList : TFileList;
  i         : Integer;
  rootPath  : AnsiString;
  // catagory info; c64, bbc, etc...
  catPath   : AnsiString;
  catItem   : TMenuItem;
begin
  FileAttrs := faAnyFile;

  menuItem_tutorials.Clear;

  SetLength(filesList,0);

  rootPath := ExtractFilePath(ParamStr(0))+cExamplesFolder;

  GetFilesAt(rootPath,'*.dpr',filesList);

  for i := 0 to high(filesList) do begin
    catPath := ExtractFilePath(Copy(filesList[i],Length(rootPath)+1,Length(filesList[i])));
    catItem := menuItem_tutorials.Find(catPath);

    if catItem = nil then begin
    // catagory item doesn't exist, so add it...
      catItem := TMenuItem.Create(menuItem_tutorials);
      catItem.Caption := catPath;
      menuItem_tutorials.Add(catItem);
    end;

    // create and add tutorial under correct catagory sub menu
    subItem := TMenuItem.Create(menuItem_tutorials);
    subItem.Caption := ExtractFileName(filesList[i]);
    subItem.OnClick := tutorialClick;
    catItem.Add(subItem);
  end;
end;

procedure TPas6502_Form.loadMostRecentlyUsedFilesList;
var
  sr         : TSearchRec;
  FileAttrs  : Integer;
  subItem    : TMenuItem;
  mruIniFile : TIniFile;
  fileName   : AnsiString;
  count,i,j  : Integer;
  mruFile    : TmruFile;
  found      : Boolean;
begin
  mruIniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mruFiles.ini');
  try
    count := mruIniFile.ReadInteger('data','fileCount',0);

    FmruFiles.Clear;

    if count > 0 then begin
      for i := 0 to count - 1 do begin
        fileName := mruIniFile.ReadString('data','file'+IntToStr(i),'');

        found := False;

        // check if file exists in list already and don't add it
        for j := 0 to FmruFiles.Count - 1 do
          if LowerCase(fileName) = LowerCase(TmruFile(FmruFiles.Items[j]).fileName) then begin
            found := True;
            break;
          end;

        if not found and FileExists(fileName) then begin
          mruFile := TmruFile.Create;
          mruFile.fileName := fileName;

          FmruFiles.Add(mruFile);
        end;
      end;
    end;

    MenuItem_openRecent.Clear;

    for i  := 0 to FmruFiles.Count - 1 do begin
      mruFile := TmruFile(FmruFiles.Items[i]);

      subItem := TMenuItem.Create(MenuItem_openRecent);
      subItem.Caption := '&'+IntToStr(i)+' '+ExtractFileName(mruFile.fileName);
      subItem.Tag     := i;
      subItem.OnClick := mruFilesClick;
      MenuItem_openRecent.Add(subItem);
    end;
  finally
    mruIniFile.Free;
  end;
end;

procedure TPas6502_Form.updateMostRecentlyUsedFilesList(fileName : AnsiString);
var
  mruFile : TmruFile;
  i       : Integer;
begin
  for i := 0 to FmruFiles.Count - 1 do
    if LowerCase(fileName) = LowerCase(TmruFile(FmruFiles.Items[i]).fileName) then
    // don't add to list as it exists already
      Exit;

  mruFile := TmruFile.Create;
  mruFile.fileName := fileName;

  FmruFiles.Add(mruFile);

  if FmruFiles.Count > cMAX_MRU_FILES then FmruFiles.Delete(0);

  saveMostRecentlyUsedFilesList;
  loadMostRecentlyUsedFilesList;
end;

procedure TPas6502_Form.saveMostRecentlyUsedFilesList;
var
  sr         : TSearchRec;
  FileAttrs  : Integer;
  subItem    : TMenuItem;
  mruIniFile : TIniFile;
  count,i    : Integer;
  mruFile    : TmruFile;
begin
  DeleteFile(ExtractFilePath(ParamStr(0)) + 'mruFiles.ini');

  mruIniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'mruFiles.ini');
  try
    mruIniFile.WriteInteger('data','fileCount',FmruFiles.Count);

    if FmruFiles.Count > 0 then begin
      for i := 0 to FmruFiles.Count - 1 do begin
        mruFile := TmruFile(FmruFiles.Items[i]);

        mruIniFile.WriteString('data','file'+IntToStr(i),mruFile.fileName);
      end;
    end;
  finally
    mruIniFile.Free;
  end;
end;

procedure TPas6502_Form.saveTheme(themeName : AnsiString);
var
  IniFile   : TIniFile;
  themeFile : AnsiString;
  themePath : AnsiString;
  srcMemo   : TSynEdit;
  section   : AnsiString;
begin
  themePath := ExtractFilePath(ParamStr(0))+'themes\';

  themeFile := themePath+themeName+'.ini';

  ForceDirectories(themePath);

  IniFile := TIniFile.Create(themeFile);
  try
    section := 'ide theme';

    SynPasSyn.SaveToIniFile(IniFile);

    srcMemo := getSourceCodeMemo(true);
    IniFile.WriteInteger(section,'background',srcMemo.Color);
  finally
    IniFile.Free;
  end;
end;

procedure TPas6502_Form.loadTheme(themeName : AnsiString);
var
  IniFile   : TIniFile;
  themeFile : AnsiString;
  themePath : AnsiString;
  srcMemo   : TSynEdit;
  section   : AnsiString;
begin
  themePath := ExtractFilePath(ParamStr(0))+'themes\';

  themeFile := themePath+themeName+'.ini';

  if not FileExists(themeFile) then Exit;

  IniFile := TIniFile.Create(themeFile);
  try
    section := 'ide theme';

    SynPasSyn.LoadFromIniFile(IniFile);

    srcMemo := getSourceCodeMemo(true);
    srcMemo.Color := IniFile.readInteger(section,'background',-16777211);
//    AsmOutput_Memo.Color := srcMemo.Color;
  finally
    IniFile.Free;
  end;
end;

procedure Tpas6502_Form.loadThemes;
var
  themeFiles : TFileList;
  themePath  : AnsiString;
  i          : Integer;
begin
  themePath := ExtractFilePath(ParamStr(0))+'themes\';

  GetFilesAt(themePath,'*.ini',themeFiles);

  theme_ComboBox.Items.Clear;

  for i := 0 to High(themeFiles) do begin
    theme_ComboBox.Items.Add(ChangeFileExt(ExtractFileName(themeFiles[i]),''));
  end;

  i := theme_ComboBox.Items.IndexOf(FthemeName);

  if i <> -1 then theme_ComboBox.ItemIndex := i;
end;

function hexToColor(v : Integer) : Integer;
begin
  Result := ((v shr 16) and $ff) + (v and $00ff00) + ((v and $ff) shl 16);
end;

procedure TPas6502_Form.setSourceCodeFontSize(pageControl : TPageControl);
var
  srcMemo  : TSynEdit;
  i        : Integer;
  fileInfo : TFileInfo;
begin
  SourceCode_SynMemo.Font.Size := FfontSize;
  SourceCode_SynMemo.Invalidate;

  for i := 1 to pageControl.PageCount - 1 do begin
    fileInfo := TFileInfo(pageControl.Pages[i].Tag);

    fileInfo.srcMemo.Font.Size := FfontSize;
    fileInfo.srcMemo.Invalidate;
  end;

  fontSize_Edit.Text := IntToStr(FfontSize);
end;

procedure TPas6502_Form.FontSizeMinus_MenuItemClick(Sender: TObject);
var
  srcMemo  : TSynEdit;
begin
  srcMemo := getSourceCodeMemo(true);

  FfontSize := FfontSize - 1;

  FfontSize := Max(FONT_MIN,FfontSize);
  FfontSize := Min(FONT_MAX,FfontSize);

  setSourceCodeFontSize(PageControl_SourceCode);
end;

procedure TPas6502_Form.FontSizePlus_MenuItemClick(Sender: TObject);
var
  srcMemo  : TSynEdit;
begin
  srcMemo := getSourceCodeMemo(true);

  FfontSize := FfontSize + 1;

  FfontSize := Max(FONT_MIN,FfontSize);
  FfontSize := Min(FONT_MAX,FfontSize);

  setSourceCodeFontSize(PageControl_SourceCode);
end;

procedure TPas6502_Form.FormActivate(Sender: TObject);
const
  classic = 0;
  modern  = 1;
  borlandStyle = classic;

var
  srcMemo : TSynEdit;

  darkblack    : Integer;
  darkblue     : Integer;
  darkgreen    : Integer;
  darkcyan     : Integer;
  darkred      : Integer;
  darkmagenta  : Integer;
  darkyellow   : Integer;
  darkwhite    : Integer;
  darkscroll   : Integer;
  lightblack   : Integer;
  lightblue    : Integer;
  lightgreen   : Integer;
  lightcyan    : Integer;
  lightred     : Integer;
  lightmagenta : Integer;
  lightyellow  : Integer;
  lightwhite   : Integer;
  lightscroll  : Integer;

begin
if BorlandStyle = classic then begin
  darkblack := $000000;
  darkblue := $0000a8;
  darkgreen := $00a800;
  darkcyan := $00a8a8;
  darkred := $a80000;
  darkmagenta := $a800a8;
  darkyellow := $a85700;
  darkwhite := $a8a8a8;
  darkscroll := $0038a8;
  lightblack := $575757;
  lightblue := $5757ff;
  lightgreen := $57ff57;
  lightcyan := $57ffff;
  lightred := $ff5757;
  lightmagenta := $ff57ff;
  lightyellow := $ffff57;
  lightwhite := $ffffff;
  lightscroll := $0070a8;
end
else begin
  darkblack := $000000;
  darkblue := $003078;
  darkgreen := $308800;
  darkcyan := $00a8a8;
  darkred := $a80000;
  darkmagenta := $a800a8;
  darkyellow := $a85700;
  darkwhite := $a8a8a8;
  darkscroll := $004078;
  lightblack := $575757;
  lightblue := $5757ff;
  lightgreen := $57ff57;
  lightcyan := $57ffff;
  lightred := $ff5757;
  lightmagenta := $ff57ff;
  lightyellow := $ffff57;
  lightwhite := $ffffff;
  lightscroll := $006078;
end;

{
//  delphi
//  -------
  srcMemo := getSourceCodeMemo(true);
  srcMemo.Color := hexToColor($FFFFFF); // background

  SynPasSyn.AsmAttri.Foreground        := clDefault;//hexToColor($3B3B74);
  SynPasSyn.CharAttri.Foreground       := clPurple;//hexToColor($000000);
  SynPasSyn.CommentAttri.Foreground    := clGreen;//hexToColor($008000);
  SynPasSyn.DirectiveAttri.Foreground  := clTeal;//clFuchsia;
  SynPasSyn.FloatAttri.Foreground      := clBlue;//hexToColor($c0bed1);
  SynPasSyn.HexAttri.Foreground        := clBlue;//hexToColor($CD3131);
  SynPasSyn.IdentifierAttri.Foreground := clDefault;//hexToColor($3B3B3B);
  // keywords
  SynPasSyn.KeyAttri.Foreground        := clNavy;//hexToColor($AF00DB);
  // numbers
  SynPasSyn.NumberAttri.Foreground     := clBlue;//hexToColor($09865D);
  SynPasSyn.SpaceAttri.Foreground      := clRed;//srcMemo.Color;
  SynPasSyn.StringAttri.Foreground     := clBlue;//hexToColor($AC1515);
  SynPasSyn.SymbolAttri.Foreground     := clDefault;//hexToColor($000000);
  // built-in types; byte, integer.
  SynPasSyn.TypeAttri.Foreground       := clDefault;//hexToColor($AC1515);

  saveTheme('delphi');
}

{
//  vs code
//  -------
  srcMemo := getSourceCodeMemo(true);
  srcMemo.Color := hexToColor($FFFFFF); // background

  SynPasSyn.AsmAttri.Foreground        := hexToColor($3B3B74);
  SynPasSyn.CharAttri.Foreground       := hexToColor($000000);
  SynPasSyn.CommentAttri.Foreground    := hexToColor($008000);
  SynPasSyn.DirectiveAttri.Foreground  := clFuchsia;
  SynPasSyn.FloatAttri.Foreground      := hexToColor($c0bed1);
  SynPasSyn.HexAttri.Foreground        := hexToColor($CD3131);
  SynPasSyn.IdentifierAttri.Foreground := hexToColor($3B3B3B);
  // keywords
  SynPasSyn.KeyAttri.Foreground        := hexToColor($AF00DB);
  // numbers
  SynPasSyn.NumberAttri.Foreground     := hexToColor($09865D);
  SynPasSyn.SpaceAttri.Foreground      := clRed;//srcMemo.Color;
  SynPasSyn.StringAttri.Foreground     := hexToColor($AC1515);
  SynPasSyn.SymbolAttri.Foreground     := hexToColor($000000);
  // built-in types; byte, integer.
  SynPasSyn.TypeAttri.Foreground       := hexToColor($AC1515);

  saveTheme('vs code');

}
{
//  zenburn
//  -------
  srcMemo := getSourceCodeMemo(true);
  srcMemo.Color := hexToColor($3F3F3F); // background

  SynPasSyn.AsmAttri.Foreground        := hexToColor($8ACCCF);
  SynPasSyn.CharAttri.Foreground       := hexToColor($F0DFAF);
  SynPasSyn.CommentAttri.Foreground    := hexToColor($7f9f7f);
  SynPasSyn.DirectiveAttri.Foreground  := clFuchsia;
  SynPasSyn.FloatAttri.Foreground      := hexToColor($c0bed1);
  SynPasSyn.HexAttri.Foreground        := hexToColor($FFFFFF);
  SynPasSyn.IdentifierAttri.Foreground := hexToColor($F0DFAF);
  // keywords
  SynPasSyn.KeyAttri.Foreground        := hexToColor($D0EAAC);
  // numbers
  SynPasSyn.NumberAttri.Foreground     := hexToColor($8cd0d3);
  SynPasSyn.SpaceAttri.Foreground      := clRed;//srcMemo.Color;
  SynPasSyn.StringAttri.Foreground     := hexToColor($cc9393);
  SynPasSyn.SymbolAttri.Foreground     := hexToColor($F0DFAF);
  // built-in types; byte, integer.
  SynPasSyn.TypeAttri.Foreground       := hexToColor($dfdfbf);

  saveTheme('zenburn');
}

{
//  borland classic
//  ---------------
  srcMemo := getSourceCodeMemo(true);
  srcMemo.Color := hexToColor(darkblue); // background

//  AsmOutput_Memo.Color := srcMemo.Color;

  SynPasSyn.AsmAttri.Foreground        := clLime;
  SynPasSyn.CharAttri.Foreground       := hexToColor($FFFFFF);
  SynPasSyn.CommentAttri.Foreground    := hexToColor($00FFFF);
  SynPasSyn.DirectiveAttri.Foreground  := clFuchsia;
//  SynPasSyn.FloatAttri.Foreground      := clYellow;
  SynPasSyn.HexAttri.Foreground        := hexToColor($FFFFFF);
  SynPasSyn.IdentifierAttri.Foreground := hexToColor($FFFF00);
  // keywords
  SynPasSyn.KeyAttri.Foreground        := hexToColor($FFFFFF);
  // numbers
  SynPasSyn.NumberAttri.Foreground     := hexToColor($FFFF00);
  SynPasSyn.SpaceAttri.Foreground      := clRed;//srcMemo.Color;
  SynPasSyn.StringAttri.Foreground     := hexToColor($FFFF00);
  SynPasSyn.SymbolAttri.Foreground     := hexToColor($FFFF00);
  // built-in types; byte, integer.
  SynPasSyn.TypeAttri.Foreground       := hexToColor($FFFF00);

  saveTheme('borland classic');
  }
end;

procedure TPas6502_Form.FormCreate(Sender: TObject);
var
  IniFile : TIniFile;

  w,h,sph : Integer;
  sw      : Integer;
  p       : TPoint;
begin
  StatusBar_SourceCodeStatus.SimpleText := Format('Row: %.2d, Col: %.2d',[SourceCode_SynMemo.CaretY,SourceCode_SynMemo.CaretX]);
  FKickAssemblerJarFile  := '';
  FViceX64ExeFile        := '';
  FBBCEmulatorFile       := '';
  FExomizerExeFile       := '';
  FAtari8BitEmulatorFile := '';
  FAppleIIEmulatorFile   := '';

  codeInsight_ComboBox.Visible   := False;
  codeInsight_ComboBox.Font.Size := 10;
  codeInsight_ComboBox.Font.Name := 'courier New';
//  codeInsight_ComboBox.Font.Style := codeInsight_ComboBox.Font.Style + [fsBold];
//  codeInsight_ComboBox.Sorted := True;

  if FileExists(ExtractFilePath(ParamStr(0)) + 'Pas6502.ini') then begin
    IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Pas6502.ini');

    FthemeName := IniFile.ReadString('setup','theme','delphi');
    FfontSize  := IniFile.ReadInteger('setup','fontsize',10);

    sph     := IniFile.ReadInteger('setup','windowSplitHeight',0);
    sw      := IniFile.ReadInteger('setup','windowSplitWidth',0);

    w       := IniFile.ReadInteger('setup','windowWidth',0);
    h       := IniFile.ReadInteger('setup','windowHeight',0);

    if sph > 0 then split_Panel.Height := sph;

    if sw > 0 then PageControl_SourceCode.Width := sw;

    if w > 0 then Width  := w;
    if h > 0 then Height := h;

    FKickAssemblerJarFile := ExtractFilePath(ParamStr(0))+'utilities\KickAssembler\KickAss.jar';
//    FKickAssemblerJarFile := IniFile.ReadString('setup','KickAssemblerFileName','C:\utilities\KickAssembler\KickAss.jar');
//    FKickAssemblerJarFile := ReplaceStr(FKickAssemblerJarFile,'"','');

    FExomizerExeFile      := ExtractFilePath(ParamStr(0))+'utilities\Exomiser\win32\exomizer.exe';
//    FExomizerExeFile      := IniFile.ReadString('setup','ExomizerFileName','C:\utilities\Exomiser\win32\exomizer.exe');
//    FExomizerExeFile      := ReplaceStr(FExomizerExeFile,'"','');

    FAtari8BitEmulatorFile := IniFile.ReadString('setup','Atari8BitEmulatorFileName','C:\emulators\Altirra\Altirra.exe');
    FBBCEmulatorFile       := IniFile.ReadString('setup','BBCMicroEmulatorFileName','C:\emulators\BeebEm\BeebEm.exe');
    FViceX64ExeFile        := IniFile.ReadString('setup','VICEEmulatorFileName','C:\Emulators\WinVICE-2.2-x64\x64sc.exe');
    FViceXvicExeFile       := IniFile.ReadString('setup','VICEvic20EmulatorFileName','C:\Emulators\WinVICE-2.2-x64\xvic.exe');
    FViceX128ExeFile       := IniFile.ReadString('setup','VICE128EmulatorFileName','C:\Emulators\WinVICE-2.2-x64\x128.exe');
    FNESExeFile            := IniFile.ReadString('setup','NESemulatorFileName','C:\emulators\nes\fceux\fceux.exe');
    FAppleIIEmulatorFile   := IniFile.ReadString('setup','AppleiiEmulatorFileName','C:\emulators\appleii\AppleWin\AppleWin.exe');
    FViceXPETExeFile       := IniFile.ReadString('setup','VICEPETEmulatorFileName','C:\emulators\WinVICE-2.2-x64\xpet.exe');

    FAtari8BitEmulatorFile := ReplaceStr(FAtari8BitEmulatorFile,'"','');
    FBBCEmulatorFile       := ReplaceStr(FBBCEmulatorFile,'"','');
    FViceX64ExeFile        := ReplaceStr(FViceX64ExeFile,'"','');
    FViceXvicExeFile       := ReplaceStr(FViceXvicExeFile,'"','');
    FViceX128ExeFile       := ReplaceStr(FViceX128ExeFile,'"','');
    FNESExeFile            := ReplaceStr(FNESExeFile,'"','');
    FAppleIIEmulatorFile   := ReplaceStr(FAppleIIEmulatorFile,'"','');
    FViceXPETExeFile       := ReplaceStr(FViceXPETExeFile,'"','');

    IniFile.Free;
//    FprojectInfo
  end;

  FprojectInfo := TFileInfo.Create(FProjectFileName,true,true);

  args := TStringList.Create;

  TTabSheet(PageControl_SourceCode.Pages[0]).Tag := Integer(TFileInfo.Create(FProjectFileName,True,true));
  FProjectFileName := 'Untitled.dpr';
  SetCaption(FProjectFileName);

  FCodeInsightData := TCodeInsightData.Create('');
  FCodeInsightData.clear;

//  FCodeInsightData := TCodeInsightData.Create('c64');
//  for i := 0 to High(cC64Color) do
//    FCodeInsightData.AddDataByName('color').AddDataByName(cC64Color[i]);
//
//  FCodeInsightData.clear;

  FmruFiles := TObjectList.Create(True);

  loadTutorialFilesList;
  loadMostRecentlyUsedFilesList;
  ffindReplace := TFindReplaceDialog.Create(self);

  FsourceInfo := TStringList.Create;

  clearSrcHistory;

{  replace_Edit.Top      := find_Edit.Top + find_Edit.Height + 8;
  findReplace_Panel.ClientHeight := frMode_Button.Top + frMode_Button.Height + 8;

  p := ScreenToClient(SourceCode_SynMemo.ClientToScreen(Point(SourceCode_SynMemo.Left,SourceCode_SynMemo.Top)));
  findReplace_Panel.Left := p.X + (SourceCode_SynMemo.ClientWidth - findReplace_Panel.Width);
  findReplace_Panel.Top  := p.Y;
}
  routines_cleanup;

  prevSrc_MenuItem.ShortCut := ShortCut(VK_LEFT, [ssAlt]); // [
  nextSrc_MenuItem.ShortCut := ShortCut(VK_RIGHT, [ssAlt]); // ]

  fontSizeMinus_MenuItem.ShortCut := ShortCut(189,[ssCtrl]); // [ = OEM_4
  fontSizePlus_MenuItem.ShortCut := ShortCut(187,[ssCtrl]);  // ] = OEM_6

  LineIndentdecrease_MenuItem.ShortCut := ShortCut(VK_OEM_4,[ssCtrl]); // [ = OEM_4
  LineIndentincrease_MenuItem.ShortCut := ShortCut(VK_OEM_6,[ssCtrl]);  // ] = OEM_6

  toggleCommentLines_MenuItem.ShortCut := ShortCut(VK_OEM_2,[ssCtrl]); // /

//  toggleComments_MenuItem.ShortCut := ShortCut(VK_DIVIDE, [ssCtrl]); // [

  setSourceCodeFontSize(PageControl_SourceCode);
  SourceCode_SynMemo.Font.Name := FONT_NAME;
end;

procedure TPas6502_Form.FormDestroy(Sender: TObject);
var
  IniFile : TIniFile;
begin
  IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Pas6502.ini');
  try
    IniFile.WriteInteger('setup','windowSplitHeight',split_Panel.Height);
    IniFile.WriteInteger('setup','windowSplitWidth',PageControl_SourceCode.Width);
    IniFile.WriteInteger('setup','windowWidth',Width);
    IniFile.WriteInteger('setup','windowHeight',Height);
    IniFile.WriteString('setup','theme',FthemeName);
    IniFile.WriteInteger('setup','fontsize',FfontSize);
  finally
    IniFile.Free;
  end;

  FmruFiles.Free;
  FsourceInfo.Clear;
  FsourceInfo.Free;
//  ffindReplace.Free;
end;

procedure TPas6502_Form.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  p : TBufferCoord;
  srcCodeMemo   : TSynEdit;
begin
  srcCodeMemo := getSourceCodeMemo(true);

//  if (Key = VK_F9) then begin
//    PageControl_SourceCode.Pages[0].SetFocus;
//
//    if (ssCTRL in Shift) then
//      CompileProgram(false)
//    else
//      CompileProgram(true);
//  end
//  else
  if (Key = VK_ESCAPE) then begin
    codeInsight_ComboBox.visible := False;
    srcCodeMemo.SetFocus;
    ffindReplace.Hide;
  end
  else
  if (Key = ord('/')) and (ssCtrl in Shift) then begin
    toggleCommenttedLines;
  end
  else
  if (Key = Ord('y')) and (ssCtrl in Shift) then begin
  // delete the current line!
    p := srcCodeMemo.CaretXY;

    srcCodeMemo.Lines.Delete(p.Line - 1);
  end
  else
  if (Key = Ord('g')) and (ssCtrl in Shift) then begin
    if GotoLineDialog.ShowModal = mrOk then begin
    end;
  end
  else
  if Key = vk_F1 then begin
    help_Form.Show;
  end;
end;

procedure TPas6502_Form.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  synMemo : TSynEdit;
begin
  if (Key = VK_F3) and (ssShift in Shift) then
    ffindReplace.previousMatch_ButtonClick(nil)
  else
  if (Key = VK_F3) and not (ssShift in Shift) then
    ffindReplace.nextMatch_ButtonClick(nil)
  else
  if (key = VK_ESCAPE) then
    codeInsight_ComboBox.Visible := False
  else begin
    synMemo := getSourceCodeMemo(true);

    CodeInsight_Timer.Enabled := synMemo.Focused and (Key = VK_OEM_PERIOD);
  end;
end;

procedure TPas6502_Form.FormResize(Sender: TObject);
begin
  GroupBox1.Height := 250;
end;

procedure TPas6502_Form.FormShow(Sender: TObject);
begin
  SourceCode_SynMemo.SetFocus;
  loadThemes;

//  loadTheme('delphi');
  loadTheme(FthemeName);

  OptionsDialog.Atari8BitFile_Edit.Text      :=  FAtari8BitEmulatorFile;
  OptionsDialog.BBC_EmulatorFile_Edit.Text   :=  FBBCEmulatorFile;
  OptionsDialog.X64File_Edit.Text            :=  FViceX64ExeFile;
  OptionsDialog.vic20File_Edit.Text          :=  FViceXvicExeFile;
  OptionsDialog.X128File_Edit.Text           :=  FViceX128ExeFile;
  OptionsDialog.NESfile_Edit.Text            :=  FNESExeFile;
  OptionsDialog.AppleIIFile_Edit.Text        :=  FAppleIIEmulatorFile;
end;

procedure TPas6502_Form.Open1Click(Sender: TObject);
var
  fileExt  : AnsiString;
  fileInfo : TFileInfo;
begin
  OpenDialog.Title       := 'Open Pas6502 Project/Unit File';
  OpenDialog.DefaultExt  := '*.dpr;*.pas';
  OpenDialog.Filter      := 'Pascal Project/Unit Files (*.dpr;*.pas)|*.dpr;*.pas';
  OpenDialog.FileName    := '*.dpr;*.pas';
  OpenDialog.FilterIndex := 1;

  loadTutorialFilesList;

  if OpenDialog.Execute then begin
    fileExt := ExtractFileExt(OpenDialog.FileName);

    if (fileExt = '.dpr') then begin
      SourceCode_SynMemo.Lines.LoadFromFile(ChangeFileExt(OpenDialog.FileName,'.dpr'));

      FProjectFileName := ChangeFileExt(OpenDialog.FileName,'.dpr');
      fileInfo := TFileInfo(TTabSheet(PageControl_SourceCode.ActivePage).Tag);
      fileInfo.fileName := FProjectFileName;

      SetCaption(FProjectFileName);

      updateMostRecentlyUsedFilesList(FProjectFileName);

      SourceCode_SynMemo.SetFocus;

      getInsightDataFromSource;
    end else begin
      loadUnit(OpenDialog.FileName);
{      NewUnit1Click(nil);

      fileInfo := TFileInfo(TTabSheet(PageControl_SourceCode.ActivePage).Tag);
      fileInfo.fileName := ChangeFileExt(OpenDialog.FileName,'.pas');

      getSourceCodeMemo(true).Lines.LoadFromFile(fileInfo.fileName);
      PageControl_SourceCode.ActivePage.Caption := ExtractFileName(fileInfo.fileName);}
    end;
  end;
end;

procedure TPas6502_Form.Save1Click(Sender: TObject);
var
  fileInfo : TFileInfo;
begin
  fileInfo := TFileInfo(TTabSheet(PageControl_SourceCode.ActivePage).Tag);
  if fileInfo.isNew then begin
    SaveAs1Click(Sender);
    Exit;
  end;

  if PageControl_SourceCode.ActivePageIndex = 0 then begin
    // is main project file so save it
    fileInfo.isDirty  := False;
    fileInfo.isNew    := False;

    SourceCode_SynMemo.Lines.SaveToFile(fileInfo.fileName);

    loadTutorialFilesList;
  end else begin
    // is a unit, etc. so save it
    fileInfo.isDirty  := False;
    fileInfo.isNew    := False;

    getSourceCodeMemo(true).Lines.SaveToFile(fileInfo.fileName);
    PageControl_SourceCode.ActivePage.Caption := ExtractFileName(fileInfo.fileName);
  end;
end;

procedure TPas6502_Form.SaveAs1Click(Sender: TObject);
var
  fileInfo : TFileInfo;
begin
  fileInfo := TFileInfo(TTabSheet(PageControl_SourceCode.ActivePage).Tag);

  if PageControl_SourceCode.ActivePageIndex = 0 then begin
    // do save as
    SaveDialog.Title       := 'Save Pas6502 Project File';
    SaveDialog.DefaultExt  := '*.dpr';
    SaveDialog.Filter      := 'Pascal Project Files (*.dpr)|*.dpr';
    SaveDialog.FileName    := FProjectFileName;
    SaveDialog.FilterIndex := 1;

    if SaveDialog.Execute then begin
      fileInfo.fileName := ChangeFileExt(SaveDialog.FileName,'.dpr');
      fileInfo.isDirty  := False;
      fileInfo.isNew    := False;

      SourceCode_SynMemo.Lines.SaveToFile(fileInfo.fileName);

      if Pos(LowerCase(ParamStr(0))+cExamplesFolder,LowerCase(fileInfo.fileName)) <> 0  then
        loadTutorialFilesList;

      FProjectFileName  := fileInfo.fileName;
      SetCaption(FProjectFileName);
      SourceCode_SynMemo.Tag := 1;
    end;
  end else begin
    // do save as
    SaveDialog.Title       := 'Save Pas6502 Unit File';
    SaveDialog.DefaultExt  := '*.pas';
    SaveDialog.Filter      := 'Pascal Unit Files (*.pas)|*.pas';
    SaveDialog.FileName    := fileInfo.fileName;
    SaveDialog.FilterIndex := 1;

    if SaveDialog.Execute then begin
      fileInfo.fileName := ChangeFileExt(SaveDialog.FileName,'.pas');
      fileInfo.isDirty := False;
      fileInfo.isNew    := False;

      getSourceCodeMemo(true).Lines.SaveToFile(fileInfo.fileName);

      PageControl_SourceCode.ActivePage.Caption := ExtractFileName(fileInfo.fileName);
    end;
  end;
end;

procedure TPas6502_Form.OptionsActionExecute(Sender: TObject);
var
  IniFile : TIniFile;
begin
  if (OptionsDialog.ShowModal = mrOK) then begin
    FAtari8BitEmulatorFile := OptionsDialog.Atari8BitFile_Edit.Text;
    FBBCEmulatorFile       := OptionsDialog.BBC_EmulatorFile_Edit.Text;
    FViceX64ExeFile        := OptionsDialog.X64File_Edit.Text;
    FViceXvicExeFile       := OptionsDialog.vic20File_Edit.Text;
    FViceX128ExeFile       := OptionsDialog.X128File_Edit.Text;
    FNESExeFile            := OptionsDialog.NESfile_Edit.Text;
    FAppleIIEmulatorFile   := OptionsDialog.AppleIIFile_Edit.Text;
{
    FAtari8BitEmulatorFile := IniFile.ReadString('setup','Atari8BitEmulatorFileName','C:\emulators\Altirra\Altirra.exe');
    FBBCEmulatorFile       := IniFile.ReadString('setup','BBCMicroEmulatorFileName','C:\emulators\BeebEm\BeebEm.exe');
    FViceX64ExeFile        := IniFile.ReadString('setup','VICEEmulatorFileName','C:\Emulators\WinVICE-2.2-x64\x64sc.exe');
    FViceXvicExeFile       := IniFile.ReadString('setup','VICEvic20EmulatorFileName','C:\Emulators\WinVICE-2.2-x64\xvic.exe');
    FViceX128ExeFile       := IniFile.ReadString('setup','VICE128EmulatorFileName','C:\Emulators\WinVICE-2.2-x64\x128.exe');
    FNESExeFile            := IniFile.ReadString('setup','NESemulatorFileName','C:\emulators\nes\fceux\fceux.exe');
    FAppleIIEmulatorFile   := IniFile.ReadString('setup','AppleiiEmulatorFileName','C:\emulators\appleii\AppleWin\AppleWin.exe');
}
    IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Pas6502.ini');

    IniFile.WriteString('setup','Atari8BitEmulatorFileName' ,'"'+FAtari8BitEmulatorFile+'"');
    IniFile.WriteString('setup','BBCMicroEmulatorFileName'  ,'"'+FBBCEmulatorFile+'"');
    IniFile.WriteString('setup','VICEEmulatorFileName'      ,'"'+FViceX64ExeFile+'"');
    IniFile.WriteString('setup','VICEvic20EmulatorFileName' ,'"'+FViceXvicExeFile+'"');
    IniFile.WriteString('setup','VICE128EmulatorFileName'   ,'"'+FViceX128ExeFile+'"');
    IniFile.WriteString('setup','NESemulatorFileName'       ,'"'+FNESExeFile+'"');
    IniFile.WriteString('setup','AppleiiEmulatorFileName'   ,'"'+FAppleIIEmulatorFile+'"');

    IniFile.WriteString('setup','KickAssemblerFileName'     ,'"'+FKickAssemblerJarFile+'"');
    IniFile.WriteString('setup','ExomizerFileName'          ,'"'+FExomizerExeFile+'"');

    IniFile.Free;
  end;
end;

procedure TPas6502_Form.gotoSource(offset : Integer; fileName : String);
var
  srcCodeMemo : TSynEdit;
  index       : Integer;
  text        : String;
  c           : Char;
  i           : Integer;

  function fileIsOpen(fileName : String) : Boolean;
  var
    i  : Integer;
    fi : TFileInfo;
  begin
    Result := True;

     for i := 0 to PageControl_SourceCode.PageCount - 1 do begin
       fi := TFileInfo(PageControl_SourceCode.Pages[i].Tag);

       if ExtractFileName(fileName) = ExtractFileName(fi.fileName) then begin
         index := i;
         Exit;
       end;
     end;

     Result := False;
  end;
begin
  srcCodeMemo        := getSourceCodeMemo(true);
  srcCodeMemo.Cursor := crIBeam;

  gotoDefinition_Panel.Visible := False;

  if FileExists(fileName) then begin


    if fileIsOpen(fileName) then begin
      PageControl_SourceCode.ActivePage      := PageControl_SourceCode.Pages[index];
      PageControl_SourceCode.ActivePageIndex := index;
    end
    else
      loadUnit(fileName);
  end;

  srcCodeMemo := getSourceCodeMemo(true);


  if offset > 0 then begin
  // place cursor at beginning of the line
    i := offset - 1;

    text := srcCodeMemo.Text;

    while (i > 0) and not(text[i] in [#13,#10]) do
      Dec(i);

    offset := i;
  end;

  srcCodeMemo.SelStart := offset;
//
//  for i := 1 to 10 do begin
//    SendMessage(srcCodeMemo.Handle, WM_VSCROLL, SB_LINEDOWN, 0);
//    srcCodeMemo.invalidate;
//  end;

end;

procedure TPas6502_Form.theme_ComboBoxChange(Sender: TObject);
begin
  FthemeName := theme_ComboBox.Items[theme_ComboBox.ItemIndex];

  loadTheme(FthemeName);
end;

procedure TPas6502_Form.SourceCode_SynMemoClick(Sender: TObject);
var
  fileInfo    : TFileInfo;
  srcCodeMemo : TSynEdit;
begin
  if not FctrlIsDown then begin
    srcCodeMemo := getSourceCodeMemo(true);

    srcCodeMemo.Cursor := crIBeam;
    Exit;
  end;

  codeInsight_ComboBox.Visible := False;

  if identifierLink = nil then Exit;

  fileInfo := TFileInfo(PageControl_SourceCode.ActivePage.Tag);

  // save current source position to history
  pushSrcHistory(0,getSourceCodeMemo(True).SelStart,fileInfo.fileName);

  gotoSource(identifierLink.offset,identifierLink.fileName);
end;

procedure TPas6502_Form.SourceCode_SynMemoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (identifierLink <> nil) and not(FctrlIsDown) then begin
    getSourceCodeMemo(true).Cursor := crIBeam;

    gotoDefinition_Panel.Visible   := False;
  end;

  if (key = VK_ESCAPE) then
    codeInsight_ComboBox.Visible := False
  else
  if (Sender is TSynEdit) then begin
    if (Key = VK_OEM_PERIOD) then
      CodeInsight_Timer.Enabled := True
    else
      CodeInsight_Timer.Enabled := False;
  end;
end;

procedure TPas6502_Form.clearSrcHistory;
begin
  SetLength(FsrcHistory,0);
  FsrcHistoryCount := 0;
  FsrcHistoryIndex := -1;
end;

procedure TPas6502_Form.pushSrcHistory(scrollPos,offset : Integer; fileName : String);
begin
  FsrcHistoryCount := Length(FsrcHistory);

  SetLength(FsrcHistory,FsrcHistoryCount + 1);

  FsrcHistory[FsrcHistoryCount].scrollPos := scrollPos;
  FsrcHistory[FsrcHistoryCount].offset    := offset;
  FsrcHistory[FsrcHistoryCount].fileName  := fileName;

  Inc(FsrcHistoryCount);

  FsrcHistoryIndex := FsrcHistoryCount - 1;
end;

function  TPas6502_Form.popSrcHistory : TSourceInfoRec;
begin
  Dec(FsrcHistoryIndex);
  Dec(FsrcHistoryCount);

  Result.scrollPos := FsrcHistory[FsrcHistoryCount].scrollPos;
  Result.offset    := FsrcHistory[FsrcHistoryCount].offset;
  Result.fileName  := FsrcHistory[FsrcHistoryCount].fileName;
end;

procedure TPas6502_Form.prevSrcHistory;
// goto previous source position
var
  srcPos : TSourceInfoRec;
begin
  if FsrcHistoryCount = 0 then Exit;

  srcPos := popSrcHistory;
  gotoSource(srcPos.offset,srcPos.fileName);

  Exit;
  if FsrcHistoryIndex >= 0 then begin
    srcPos := FsrcHistory[FsrcHistoryIndex];

    gotoSource(srcPos.offset,srcPos.fileName);

    Dec(FsrcHistoryIndex);
  end;
end;

procedure TPas6502_Form.nextScrPosHistory;
var
  srcPos : TSourceInfoRec;
begin
  Exit;
  if FsrcHistoryCount = 0 then Exit;

  if FsrcHistoryIndex < FsrcHistoryCount then Exit;

  Inc(FsrcHistoryIndex);

  srcPos := FsrcHistory[FsrcHistoryIndex];

  gotoSource(srcPos.offset,srcPos.fileName);
end;

procedure TPas6502_Form.prevSrc_MenuItemClick(Sender: TObject);
begin
  prevSrcHistory;
end;

procedure TPas6502_Form.nextSrc_MenuItemClick(Sender: TObject);
begin
  nextScrPosHistory;
end;

procedure TPas6502_Form.loadUnit(fileName : String);
var
  fileInfo : TFileInfo;
begin
  NewUnit1Click(nil);

  fileInfo := TFileInfo(TTabSheet(PageControl_SourceCode.ActivePage).Tag);
  fileInfo.fileName := ChangeFileExt(fileName,'.pas');

  fileInfo.srcMemo.Visible := True;
  fileInfo.srcMemo.Lines.LoadFromFile(fileInfo.fileName);
  fileInfo.srcMemo.Font.Size := FfontSize;
  PageControl_SourceCode.ActivePage.Caption := ExtractFileName(fileInfo.fileName);
end;

function  TPas6502_Form.getSourceInfoByName(name : String) : TSourceInfo;
var
  i : Integer;
begin
  Result := nil;

  i := FsourceInfo.IndexOf(name);

  if i <> -1 then
    Result := TSourceInfo(FsourceInfo.Objects[i]);
end;

procedure TPas6502_Form.clearSourceInfo;
var
  i : Integer;
begin
  for i := 0 to FsourceInfo.Count - 1 do
    TSourceInfo(FsourceInfo.Objects[i]).Free;

  FsourceInfo.Clear;
end;

procedure TPas6502_Form.addSourceInfoByName(name : String; symbol : TSymbol; srcOffset : Integer; srcFile : String);
var
  srcInfo : TSourceInfo;
begin
  srcInfo := TSourceInfo.Create;
  srcInfo.symbol   := symbol;
  srcInfo.offset   := srcOffset;
  srcInfo.fileName := srcFile;

  FsourceInfo.AddObject(name,srcInfo);
end;

function  TPas6502_Form.expandWordAt(col : Integer; line : String) : String;
var
  c : Integer;
begin
  Result := '';

  if not IsAlphaNum(line[col]) then Exit;

  Result := line[col];

  c := col - 1;

  while (c >= 1) and IsAlphaNum(line[c]) do begin
  // get word chars before this position
    Result := line[c] + Result;
    Dec(c);
  end;

  c := col + 1;

  while (c <= Length(line)) and IsAlphaNum(line[c]) do begin
  // get word chars before this position
    Result := Result + line[c];
    Inc(c);
  end;
end;

procedure TPas6502_Form.SourceCode_SynMemoMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  coord       : TBufferCoord;
  srcCodeMemo : TSynEdit;
  word        : String;
  data        : TCodeInsightData;
  p           : TPoint;
  symSize     : Integer;
  textSize    : TSize;
  useHand     : Boolean;
begin
  FctrlIsDown := (ssCtrl in Shift);

  identifierLink := nil;

  srcCodeMemo := getSourceCodeMemo(true);

  useHand := False;
  try

//  if not(ssCtrl in Shift) then begin
//    Exit;
//  end;

  if not Assigned(srcCodeMemo) then Exit;

  if srcCodeMemo.GetPositionOfMouse(coord) then begin
    word := srcCodeMemo.GetWordAtRowCol(coord);

    if word <> '' then begin
      if not (word[1] in ['_','a'..'z','A'..'Z']) then begin
        gotoDefinition_Panel.Visible   := False;
        Exit;
      end;

      identifierLink := getSourceInfoByName(word);

      if identifierLink <> nil then begin
        if (identifierLink.offset <> -1) and FileExists(identifierLink.fileName) then begin
          p := ScreenToClient(srcCodeMemo.ClientToScreen(Point(X,Y)));

          if not (ssCtrl in Shift) then begin
            symSize := identifierLink.symbol.size;

            if (identifierLink.symbol is TConstSymbol) then begin
              if TConstSymbol(identifierLink.symbol).isArray then
                symSize := symSize * TConstSymbol(identifierLink.symbol).len
              else
              if TConstSymbol(identifierLink.symbol).isBinaryFile then
                symSize := TConstSymbol(identifierLink.symbol).len;
            end;

            if identifierLink.symbol.symType <> nil then
              symSize := identifierLink.symbol.symType.size;

            if identifierLink.offset = 0 then
            // is unit
              gotoDefinition_Panel.Caption := Format('(unit)',[symSize])
            else begin
              if symSize = 0 then
                gotoDefinition_Panel.Caption := Format('%s',[getSymbolInfo(identifierLink.symbol)])
              else
                gotoDefinition_Panel.Caption := Format('%d byte(s) : %s',[symSize,getSymbolInfo(identifierLink.symbol)]);
            end;
          end
          else begin
            useHand := True;

            if identifierLink.offset = 0 then
              gotoDefinition_Panel.Caption := 'Open "'#13#10+identifierLink.fileName+'"'
            else
              gotoDefinition_Panel.Caption := 'Go to definition of '+identifierLink.symbol.symName;
          end;

          gotoDefinition_Panel.Left    := p.X;
          gotoDefinition_Panel.Top     := p.Y+50;
          gotoDefinition_Panel.Visible := True;

          textSize := srcCodeMemo.Canvas.TextExtent(gotoDefinition_Panel.Caption);

          gotoDefinition_Panel.ClientWidth  := textSize.cx+10;
          gotoDefinition_Panel.ClientHeight := textSize.cy;
          Exit;
        end;
      end;
    end
    else begin
      gotoDefinition_Panel.Visible   := False;
//      srcCodeMemo.Cursor             := oldCursor;
    end;
  end;
  finally
    if useHand then
      srcCodeMemo.Cursor := crHandPoint
    else
      srcCodeMemo.Cursor := crIBeam;
  end;
end;

procedure TPas6502_Form.SourceCode_SynMemoMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  srcCodeMemo : TSynEdit;
begin
  if not(ssCtrl in Shift) then begin
    srcCodeMemo := self.getSourceCodeMemo(true);
    srcCodeMemo.Cursor := crIBeam;
  end;
end;

procedure TPas6502_Form.Splitter2CanResize(Sender: TObject;
  var NewSize: Integer; var Accept: Boolean);
const
  MIN_SIZE = 300;
begin
  if NewSize < MIN_SIZE then begin
    Accept := False;
    Exit;
  end;
end;

procedure TPas6502_Form.SourceCode_SymMemoStatusChanged(Sender: TObject; Changes: TSynStatusChanges);
var
  synEdit : TSynEdit;
begin
  synEdit := getSourceCodeMemo(false);

  StatusBar_SourceCodeStatus.SimpleText := Format('Row: %.2d, Col: %.2d',[synEdit.CaretY,synEdit.CaretX]);
end;

function TPas6502_Form.StartProcess(ExeName: AnsiString; CmdLineArgs: AnsiString = '';
  ShowWindow: boolean = True; WaitForFinish: boolean = False): integer;
var
  StartInfo : TStartupInfo;
  ProcInfo  : TProcessInformation;
begin
  //Simple wrapper for the CreateProcess command
  //returns the process id of the started process.
  FillChar(StartInfo,SizeOf(TStartupInfo),#0);
  FillChar(ProcInfo,SizeOf(TProcessInformation),#0);
  StartInfo.cb := SizeOf(TStartupInfo);
  if not(ShowWindow) then begin
    StartInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartInfo.wShowWindow := SW_HIDE;
  end;
  CreateProcess(nil,PChar(ExeName + ' ' + CmdLineArgs),nil,nil,False,
    CREATE_NEW_PROCESS_GROUP + NORMAL_PRIORITY_CLASS,nil,nil,StartInfo,
    ProcInfo);
  Result := ProcInfo.dwProcessId;
  if WaitForFinish then begin
    WaitForSingleObject(ProcInfo.hProcess,Infinite);
  end;
  //close process & thread handles
  CloseHandle(ProcInfo.hProcess);
  CloseHandle(ProcInfo.hThread);
end;

procedure TPas6502_Form.getInsightDataFromSource;
// parse source file to get code insight data
var
  stream : TMemoryStream;
  parser : TParserAST;
begin
  if SourceCode_SynMemo.Lines.Text = '' then Exit;

  clearSourceInfo;

  parser := TParserAST.Create(FCodeInsightData,OnScopeUpdate);
  parser.projectPath := ExtractFilePath(FProjectFileName);
  parser.currentFile := FProjectFileName;
  parser.srcLine     := getSourceCodeMemo(true).Lines.Count - 1;
  try
    stream := TMemoryStream.Create;
    try
      SourceCode_SynMemo.Lines.SaveToStream(stream);
      parser.parse(stream,false);
    finally
      stream.Free;
    end;
  finally
    parser.progAST.Free;
    parser.Free;
  end;
end;

end.
