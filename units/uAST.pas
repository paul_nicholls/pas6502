unit uAST;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

{$H+}

interface

uses
  System.Contnrs,System.Classes,uToken,uSymbolTable;

const
  // internal values for true/false
  cTRUE    = -1;
  cFALSE   = 0;

  cASCIIToPETSCIITable: array[0..256 - 1] of Byte = (
    $00,$01,$02,$03,$04,$05,$06,$07,$14,$20,$0d,$11,$93,$0a,$0e,$0f,
    $10,$0b,$12,$13,$08,$15,$16,$17,$18,$19,$1a,$1b,$1c,$1d,$1e,$1f,
    $20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$2a,$2b,$2c,$2d,$2e,$2f,
    $30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$3a,$3b,$3c,$3d,$3e,$3f,
    $40,$c1,$c2,$c3,$c4,$c5,$c6,$c7,$c8,$c9,$ca,$cb,$cc,$cd,$ce,$cf,
    $d0,$d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9,$da,$5b,$5c,$5d,$5e,$5f,
    $c0,$41,$42,$43,$44,$45,$46,$47,$48,$49,$4a,$4b,$4c,$4d,$4e,$4f,
    $50,$51,$52,$53,$54,$55,$56,$57,$58,$59,$5a,$db,$dc,$dd,$de,$df,
    $80,$81,$82,$83,$84,$85,$86,$87,$88,$89,$8a,$8b,$8c,$8d,$8e,$8f,
    $90,$91,$92,$0c,$94,$95,$96,$97,$98,$99,$9a,$9b,$9c,$9d,$9e,$9f,
    $a0,$a1,$a2,$a3,$a4,$a5,$a6,$a7,$a8,$a9,$aa,$ab,$ac,$ad,$ae,$af,
    $b0,$b1,$b2,$b3,$b4,$b5,$b6,$b7,$b8,$b9,$ba,$bb,$bc,$bd,$be,$bf,
    $60,$61,$62,$63,$64,$65,$66,$67,$68,$69,$6a,$6b,$6c,$6d,$6e,$6f,
    $70,$71,$72,$73,$74,$75,$76,$77,$78,$79,$7a,$7b,$7c,$7d,$7e,$7f,
    $e0,$e1,$e2,$e3,$e4,$e5,$e6,$e7,$e8,$e9,$ea,$eb,$ec,$ed,$ee,$ef,
    $f0,$f1,$f2,$f3,$f4,$f5,$f6,$f7,$f8,$f9,$fa,$fb,$fc,$fd,$fe,$ff
  );

  cPETSCIIToCBMScreenCode: array [0..256 - 1] of Byte = (
    $80,$81,$82,$83,$84,$85,$86,$87,$88,$89,$8a,$8b,$8c,$8d,$8e,$8f,
    $90,$91,$92,$93,$94,$95,$96,$97,$98,$99,$9a,$9b,$9c,$9d,$9e,$9f,
    $20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$2a,$2b,$2c,$2d,$2e,$2f,
    $30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$3a,$3b,$3c,$3d,$3e,$3f,
    $00,$01,$02,$03,$04,$05,$06,$07,$08,$09,$0a,$0b,$0c,$0d,$0e,$0f,
    $10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$1a,$1b,$1c,$1d,$1e,$1f,
    $40,$41,$42,$43,$44,$45,$46,$47,$48,$49,$4a,$4b,$4c,$4d,$4e,$4f,
    $50,$51,$52,$53,$54,$55,$56,$57,$58,$59,$5a,$5b,$5c,$5d,$5e,$5f,
    $c0,$c1,$c2,$c3,$c4,$c5,$c6,$c7,$c8,$c9,$ca,$cb,$cc,$cd,$ce,$cf,
    $d0,$d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9,$da,$db,$dc,$dd,$de,$df,
    $60,$61,$62,$63,$64,$65,$66,$67,$68,$69,$6a,$6b,$6c,$6d,$6e,$6f,
    $70,$71,$72,$73,$74,$75,$76,$77,$78,$79,$7a,$7b,$7c,$7d,$7e,$7f,
    $40,$41,$42,$43,$44,$45,$46,$47,$48,$49,$4a,$4b,$4c,$4d,$4e,$4f,
    $50,$51,$52,$53,$54,$55,$56,$57,$58,$59,$5a,$5b,$5c,$5d,$5e,$5f,
    $60,$61,$62,$63,$64,$65,$66,$67,$68,$69,$6a,$6b,$6c,$6d,$6e,$6f,
    $70,$71,$72,$73,$74,$75,$76,$77,$78,$79,$7a,$7b,$7c,$7d,$7e,$5e
  );

type
  TASTList     = TObjectList;

  TProgram          = class;
  TBlock            = class;
  TAsmBlock         = class;
  TUnitBlock        = class;
  TPoke             = class;
  TAssign           = class;
  TCall             = class;
  TMacroCall        = class;
  TFuncCall         = class;
  TIfThen           = class;
  TWhile            = class;
  TRepeat           = class;
  TFor              = class;
  TNumber           = class;
  TVariable         = class;
  TString           = class;
  TBinOp            = class;
  TUnaryOp          = class;
  TCompound         = class;
  TAsmCompound      = class;
  TVarDecl          = class;
  TConstDecl        = class;
  TParam            = class;
  TProcDecl         = class;
  TMacroDecl        = class;
  TFuncDecl         = class;
  TBuiltinProcedure = class;
  TBuiltinFunction  = class;
  TPeek             = class;
  TImportBinary     = class;
  TAbsoluteArrayData = class;
  TImportSource     = class;
  TExit             = class;
  TBreak            = class;
  TSetupRasterIRQ   = class;
  TEndIRQ           = class;
  TDecimalMode      = class;
  TSegmentDecl      = class;

  TParamArray  = array of TParam;
  TNumberArray = array of TNumber;
  TStringArray = array of AnsiString;

  IASTVisitor = interface
  ['{F9E08167-966C-446A-9C85-F73D1E648B08}']
    procedure visitProgram(node : TProgram);
    procedure visitBlock(node : TBlock);
    procedure visitAsmBlock(node : TAsmBlock);
    procedure visitUnitBlock(node : TUnitBlock);
    procedure visitPoke(node: TPoke);
    procedure visitAssign(node : TAssign);
    procedure visitCall(node : TCall);
    procedure visitMacroCall(node : TMacroCall);
    procedure visitFuncCall(node : TFuncCall);
    procedure visitIfThen(node : TIfThen);
    procedure visitWhile(node : TWhile);
    procedure visitRepeat(node : TRepeat);
    procedure visitFor(node : TFor);
    procedure visitNumber(node : TNumber);
    procedure visitVariable(node : TVariable);
    procedure visitString(node : TString);
    procedure visitBinOp(node : TBinOp);
    procedure visitUnaryOp(node : TUnaryOp);
    procedure visitCompound(node : TCompound);
    procedure visitAsmCompound(node : TAsmCompound);
    procedure visitVarDecl  (node : TVarDecl);
    procedure visitConstDecl(node : TConstDecl);
    procedure visitProcDecl (node : TProcDecl);
    procedure visitMacroDecl(node : TMacroDecl);
    procedure visitFuncDecl (node : TFuncDecl);
    procedure visitBuiltinProcedure(node : TBuiltinProcedure);
    procedure visitBuiltinFunction(node : TBuiltinFunction);
    procedure visitImportBinary(node : TImportBinary);
    procedure visitAbsoluteArrayData(node : TAbsoluteArrayData);
    procedure visitImportSource(node : TImportSource);
    procedure visitExit(node : TExit);
    procedure visitBreak(node : TBreak);
    procedure visitSetupRasterIRQ(node : TSetupRasterIRQ);
    procedure visitEndIRQ(node : TEndIRQ);
    procedure visitDecimalMode(decMode : TDecimalMode);
    procedure visitSegmentDecl(node : TSegmentDecl);
  end;

  TAST = class(TInterfacedObject)
  public
    size         : Integer;
    isAddress    : Boolean;
    arrayParam   : TAST;
    //-- pointer stuff ---------
    deref        : Boolean;
    // size in bytes for pointer read/write (default to 1 byte)
    derefRWsize  : Integer;
    // offset from pointer for record read/write (default to 0 bytes)
    derefOffset  : Integer;
    //--------------------------
    isRecordVar  : Boolean;
    isSigned     : Boolean;
    sym          : TSymbol;
    sourceLine   : Integer;
    sourceFile   : AnsiString;
    isLoopVar    : Boolean;
    sizeOverride : Integer;
    isParam      : Boolean;

    isAbsoluteAddress : Boolean;

    constructor Create;
    
    function  isLeaf     : Boolean; virtual;
    function  isInteger  : Boolean; virtual;
    function  isString   : Boolean; virtual;
    function  isVariable : Boolean; virtual;
    function  isOp       : Boolean; virtual;
    function  isUnaryOp  : Boolean; virtual;
    function  isBinOp    : Boolean; virtual;

    procedure acceptVisitor(aVisitor : IASTVisitor); virtual;

    function  clone : TAST; virtual;
    function  toString : AnsiString; virtual;

    function  equals(value : TAST) : Boolean;
  end;

  TDecimalMode = class(TAST)
  public
    isOn : Boolean;

    constructor create(aIsOn : boolean);

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TExit = class(TAST)
  public
    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TBreak = class(TAST)
  public
    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TSetupRasterIRQ = class(TAST)
  public
    routineName : AnsiString;
    rasterLine  : AnsiString;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TEndIRQ = class(TAST)
  public
    routineName   : AnsiString;
    rasterLine    : AnsiString;
    useSameRaster : Boolean;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TEmpty = class(TAST)
  public
    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TOperator = class(TAST)
  public
    op   : TToken;
  end;

  TUnaryOp = class(TOperator)
  public
    expr : TAST;

    constructor Create(aOp : TToken; aExpr : TAST); overload;
    constructor Create(aOp : TTokenType; aOpValue : AnsiString; aExpr : TAST); overload;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;

    function  clone : TAST; override;
    function  isOp  : Boolean; override;
    function  isUnaryOp : Boolean; override;
    function  toString : AnsiString; override;
  end;

  TCastOp = class(TUnaryOp)
  public
    newSize  : Integer;
    isSigned : Boolean;

    constructor Create(aOp : TToken; aExpr : TAST; aNewsize : Integer; aIsSigned : Boolean); overload;
    constructor Create(aExpr : TAST; aNewsize : Integer; aIsSigned : Boolean);               overload;

    function    clone : TAST; override;
  end;

  TBinOp = class(TOperator)
  public
    left  : TAST;
    right : TAST;

    constructor Create(aLeft : TAST; aOp : TToken; aRight : TAST);                          overload;
    constructor Create(aLeft : TAST; aOp : TTokenType; aOpStr : AnsiString; aRight : TAST); overload;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;

    function  clone : TAST; override;
    function  isOp  : Boolean; override;
    function  isBinOp : Boolean; override;
    function  toString : AnsiString; override;
  end;

  TVariable = class(TAST)
  public
    token        : TToken;
    value        : Variant;
    isPointer    : Boolean;
    isVarAddress : Boolean;
    isRegister   : Boolean;
    isFuncResult : Boolean;
    arraySize    : Integer;
    address      : AnsiString;
    loReg        : AnsiString;
    hiReg        : AnsiString;
    use8BitValue : Boolean;
    indexOffset  : AnsiString;
    onStack      : Boolean;
    stackOfs     : Byte;
    recName      : AnsiString;

    constructor Create(aToken : TToken; aSize : Integer; aIsSigned : Boolean = false); reintroduce; overload;
    constructor Create(aValue : AnsiString; aSize : Integer; aIsSigned : Boolean = false); reintroduce; overload;

    function  isLeaf     : Boolean; override;
    function  isVariable : Boolean; override;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;

    function  clone : TAST; override;
    function  toString : AnsiString; override;
  end;

  TNumber = class(TAST)
  public
    token    : TToken;
    value    : Variant;
    isSigned : Boolean;

    constructor Create(aToken : TToken); reintroduce; overload;
    constructor Create(aValue : Integer); reintroduce; overload;
    constructor CreateBool(aValue : Boolean); reintroduce;

    function  isLeaf    : Boolean; override;
    function  isInteger : Boolean; override;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;

    function  clone : TAST; override;
    function  toString : AnsiString; override;
  end;

  TString = class(TAST)
  public
    token : TToken;
    value : Variant;
    sType : TTokenType;

    constructor Create(aToken : TToken); reintroduce; overload;
    constructor Create(aValue : AnsiString); reintroduce; overload;

    function  isLeaf    : Boolean; override;
    function  isString  : Boolean; override;
    function  toString : AnsiString; override;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TImportBinary = class(TAST)
  public
    name     : AnsiString;
    fileName : AnsiString;
    fileSize : Int64;
    isSid    : Boolean;
    address  : AnsiString;
    byteSkip : AnsiString;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TAbsoluteArrayData = class(TAST)
  public
    name     : AnsiString;
    address  : AnsiString;
    data     : array of AnsiString;
    dataSize : Integer;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TCompound = class(TAST)
  public
    children : TASTList;

    constructor Create;
    destructor  Destroy; override;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TAsmCompound = class(TAST)
  public
    asmStatements : TStringList;

    constructor Create;
    destructor  Destroy; override;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TBlock = class(TAST)
  public
    blockName         : AnsiString;
    declarations      : TASTList;
    compoundStatement : TCompound;
    retType           : TTokenType;
    isFunc            : Boolean;
    isMacro           : Boolean;
    onStack           : Boolean;
    stackSize         : Integer;

    constructor Create; virtual;
    destructor  Destroy; override;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TAsmBlock = class(TBlock)
  public
    asmStatements : TStringList;

    constructor Create; override;
    destructor  Destroy; override;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TUnitBlock = class(TBlock)
  public
    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TImportSource = class(TAST)
  public
    fileName : AnsiString;

    constructor create(aFileName : AnsiString);
    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TVarDecl = class(TAST)
  public
    varName       : AnsiString;
    varSym        : TSymbol;
    address       : AnsiString;
    defaultValue  : AnsiString;
    isArray       : Boolean;
    isPointer     : Boolean;
    isPointerType : Boolean;
    isVarAddress  : Boolean;
    arraySize     : Integer;
    onStack       : Boolean;
    stackOfs      : Integer;

    constructor Create(aName : AnsiString; aVarSym : TSymbol; aAddress,aDefaultValue : AnsiString);

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TConstDecl = class(TAST)
  public
    constName  : AnsiString;
    constValue : TToken;
    constType  : TSymbol;
    values     : TNumberArray;
    isArray    : Boolean;

    constructor Create(aName : AnsiString; aValue : TToken; aType : TSymbol);        overload;
    constructor Create(aName : AnsiString; aValues : TNumberArray; aType : TSymbol); overload;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TParam = class(TAST)
  public
    paramName    : AnsiString;
    paramType    : TSymbol;
    isPointer    : Boolean;
    isVarAddress : Boolean;
    address      : AnsiString;

    constructor Create(aName: AnsiString; aType : TSymbol);
  end;

  TMacroDecl = class(TAST)
  private
  public
    macroName   : AnsiString;
    macroParams : TStringArray;
    block       : TBlock;
    scope       : TscopedSymbolTable;

    constructor Create(aName : AnsiString; const aParams : TStringArray; aBlock : TBlock; aScope : TscopedSymbolTable);

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TProcDecl = class(TAST)
  private
  public
    procName   : AnsiString;
    procParams : TParamArray;
    block      : TBlock;
    scope      : TscopedSymbolTable;
    onStack    : Boolean;

    constructor Create(aName : AnsiString; aParams : TParamArray; aBlock : TBlock; aScope : TscopedSymbolTable);

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TFuncDecl = class(TAST)
  private
  public
    funcName   : AnsiString;
    funcParams : TParamArray;
    block      : TBlock;
    scope      : TscopedSymbolTable;
    onStack    : Boolean;

    constructor Create(aName : AnsiString; aParams : TParamArray; aBlock : TBlock; aScope : TscopedSymbolTable);

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TBuiltinProcedure = class(TAST)
  public
    procName : AnsiString;
    procType : TTokenType;
    params   : TASTList;

    constructor Create(aType : TTokenType);
    destructor  Destroy;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TBuiltinFunction = class(TAST)
  public
    funcName : AnsiString;
    funcType : TTokenType;
    params   : TASTList;

    constructor Create(aType : TTokenType);
    destructor  Destroy;

    function  expand(aTacList : TObject) : TAST; virtual; abstract;
    function  toString : AnsiString; override;
    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TPeek = class(TBuiltinFunction)
  public
    function  expand(aTacList : TObject) : TAST; override;
  end;

  TIfThen = class(TAST)
  public
    condition  : TAST;
    trueBranch : TAST;
    elseBranch : TAST;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TWhile = class(TAST)
  public
    condition   : TAST;
    statement   : TAST;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TRepeat = class(TAST)
  public
    condition : TAST;
    children  : TASTList;

    constructor Create;
    destructor  Destroy; override;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TFor = class(TAST)
  public
    initializer  : TAssign;
    condition    : TAST;
    statement    : TAST;
    incrementer  : TAssign;
    is0to255loop : Boolean;
    isNdownto0loop : Boolean;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TPoke = class(TAST)
  public
    address : TAST;
    expr    : TAST;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TAssign = class(TAST)
  private
  public
    variable   : TVariable;
    op         : TToken;
    expr       : TAST;
    decimalModeOn : Boolean;

    constructor Create(aVar : TAST; aExpr : TAST);

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TCall = class(TAST)
  public
    procName  : AnsiString;
    params    : TASTList;
    onStack   : Boolean;
    stackSize : Integer;

    constructor Create;
    destructor  Destroy;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TMacroCall = class(TAST)
  public
    macroName : AnsiString;
    params    : TStringList;

    constructor Create;
    destructor  Destroy;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TFuncCall = class(TAST)
  public
    funcName  : AnsiString;
    funcType  : TTokenType;
    params    : TASTList;
    onStack   : Boolean;
    stackSize : Integer;

    constructor Create;
    destructor  Destroy;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;

    function  clone : TAST; override;

    function  toString : AnsiString; override;
  end;

  TSegmentDecl = class(TAST)
    segmentName : AnsiString;

    function toString : AnsiString;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

  TNESheader = record
    mapper       : Byte;
    system       : Byte;
    country      : Byte;
    prgrom_banks : Byte;
    chrrom_banks : Byte;
  end;

  TSegmentInfo = class
    name   : AnsiString;
    start  : AnsiString;
    min    : AnsiString;
    max    : AnsiString;
    outSeg : Boolean;
    isMain : Boolean;
  end;

  TProgram = class(TAST)
  private
  public
    // target type (ttC64, ttBBC); defaults to ttC64
    target             : TTokenType;
    //
    loadAddress        : AnsiString;
    //
    name               : AnsiString;
    fileName           : AnsiString;
    SegmentsFileName   : AnsiString;
    HeaderFileName     : AnsiString;
    PRGROMsegment      : AnsiString;
    IRQsegment         : AnsiString;

    imports            : TStringList;
    binImports         : TList;
    deferredBinImports : TList;
    absoluteArrayData  : TList;
    unitBlocks         : TASTList;
    block              : TBlock;
    scope              : TscopedSymbolTable;

    segments           : TList;

    nesHeader          : TNESheader;

    constructor Create(aName : AnsiString; aBlock : TBlock; aScope : TscopedSymbolTable);
    destructor  Destroy; override;

    procedure acceptVisitor(aVisitor : IASTVisitor); override;
  end;

function  PowerOf2ToShift(n : Integer) : Integer;

function  StrIntToHex(aNumber : AnsiString) : AnsiString;
function  simplifyUnaryOp(node : TUnaryOp) : TAST;
function  simplifyBinOp(node : TBinOp) : TAST;
function  simplify(e : TAST) : TAST;

function  charToPETSCII(c : AnsiChar) : Byte;
function  charToCBMscreenCode(c : AnsiChar) : Byte;

function newCast(aToken : TToken; expr : TAST) : TCastOp; overload;
function newCast(expr : TAST; aNewSize : Integer; aIsSigned : Boolean) : TCastOp; overload;

implementation

uses
  Math,System.SysUtils,uThreeAddressCode,uCodeGen_6502;

function newCast(expr : TAST; aNewSize : Integer; aIsSigned : Boolean) : TCastOp; overload;
begin
  Result := TCastOp.Create(expr,aNewSize,aIsSigned);
end;

function newCast(aToken : TToken; expr : TAST) : TCastOp;
begin
  case aToken.tType of
    ttWord     : Result := TCastOp.Create(aToken,expr,2,false);
    ttByte     : Result := TCastOp.Create(aToken,expr,1,false);
    ttInteger  : Result := TCastOp.Create(aToken,expr,2,true);
    ttShortInt : Result := TCastOp.Create(aToken,expr,1,true);
    ttInt24    : Result := TCastOp.Create(aToken,expr,3,false);
  end;
end;

function  charToPETSCII(c : AnsiChar) : Byte;
begin
  Result := cASCIIToPETSCIITable[Ord(c)];
end;

function charToCBMscreenCode(c : AnsiChar) : Byte;
begin
  Result := cPETSCIIToCBMScreenCode[cASCIIToPETSCIITable[Ord(c)]];
end;

type
  { TIntNumType }
  TIntNumType = (
    ntSInt8,
    ntUInt8,
    ntSInt16,
    ntUInt16,
    ntUInt24,
    ntSInt32,
    ntUInt32
  );
const
  cIntNumSize : array[TIntNumType] of Integer = (
    8,
    8,
    16,
    16,
    24,
    32,
    32
  );
type
  { TIntNumTypeRange }
  TIntNumTypeRange = record
    MinVal,MaxVal: Int64;
  end;
const
  { cNumTypeRange }
  cNumTypeRange: array [TIntNumType] of TIntNumTypeRange = (
    (MinVal: - 128;        MaxVal: 127),
    (MinVal: 0;            MaxVal: 255),
    (MinVal: - 32768;      MaxVal: 32767),
    (MinVal: 0;            MaxVal: 65535),
    (MinVal: 0;            MaxVal: (1 shl 24) - 1),
    (MinVal: - 2147483648; MaxVal: 2147483647),
    (MinVal: 0;            MaxVal: 4294967295)
  );
function BinToInt64(aBin: String): Int64;
var
  i, iValueSize : Integer;
begin
  Result     := 0;
  iValueSize := Length(aBin);
  for i := iValueSize downto 1 do
    if aBin[i] = '1' then Result := Result + (1 shl (iValueSize - i));
end;
function  IntegerInRange(const aNumber,aMin,aMax: Int64): Boolean;
begin
  Result := (aNumber >= aMin) and
            (aNumber <= aMax);
end;
procedure GetIntegerSize(const aNumber: String; var aSize : Integer);
var
  i: TIntNumType;
  v, c: Integer;
begin
  aSize := 0;
  if aNumber = '' then Exit;
  c := 0;
  if aNumber[1] = '%' then
  begin
    v := BinToInt64(aNumber);
    c := 0;
  end
  else
  if aNumber[1] = '$' then
  begin
    v := StrToInt(aNumber);
    c := 0;
  end
  else
    Val(aNumber, v, c);
  if c <> 0 then Exit;
  for i := Low(TIntNumType) to High(TIntNumType) do
    if IntegerInRange(v, cNumTypeRange[i].MinVal,cNumTypeRange[i].MaxVal) then
    begin
      aSize := cIntNumSize[i] div 8;
      Exit;
    end;
end;
function  StrIntToHex(aNumber : AnsiString) : AnsiString;
var
  c,v  : Integer;
  size : Integer;
begin
  Result := '$00';

  if aNumber[1] = '%' then
  begin
    v := BinToInt64(aNumber);
    c := 0;
  end
  else
  if aNumber[1] = '$' then
  begin
    v := StrToInt(aNumber);
    c := 0;
  end
  else
    Val(aNumber, v, c);
  GetIntegerSize(aNumber,size);
  Result := IntToHex(v,size*2);
end;

function  isPositiveInteger(node : TAST) : Boolean;
begin
  Result := False;
  if not node.isInteger then Exit;

  Result := TNumber(node).value >= 0;
end;
function  isVariable(node : TAST) : Boolean;
begin
  Result := node is TVariable;
end;
function  IsPowerOf2(num : Integer): Boolean;
var
  bit   : Integer;
  count : Integer;
begin
  Result := False;
  count := 0;
  bit   := 1;
  if (num = 0) then Exit;
  repeat
    if (num and bit <> 0) then Inc(count);
    if (count > 1) then Exit;
    bit := bit * 2;
  until bit > num;
  Result := True;
end;
function  largestPowerOf2(n : Integer): Integer;
begin
  Result := Trunc(Power(2, Floor(Log10(n) / Log10(2))));
end;
function  PowerOf2ToShift(n : Integer) : Integer;
var
  count : Integer;
begin
  count := -1;
  while n > 0 do begin
    Inc(count);
    n := n div 2;
  end;
  Result := count;
end;
function  ExpandMultiply(leftValue : TAST; n : Integer): TAST;
var
  p     : Integer;
  bit   : Integer;
  i     : Integer;
  shift : TAST;
begin
  if (n = 0) then begin
  // * 0 = 0
    Result := TNumber.Create(0);
    Exit;
  end;
  Result := leftValue;
  if (n = 1) then Exit;
  // * 1 = leftValue 
  if IsPowerOf2(n) then begin
    Result := TBinOp.Create(leftValue,Token(ttShl,'<<'),TNumber.Create(PowerOf2ToShift(n)));
    Exit;
  end;
  Result := nil;
  
  p   := 0;
  bit := (1 shl p);
  repeat
    if ((bit and n) <> 0) then begin
      if p = 0 then
        shift := leftValue.clone
      else
        shift := TBinOp.Create(leftValue.clone,Token(ttShl,'<<'),TNumber.Create(p));
      if Result = nil then
        Result := shift
      else
        Result := TBinOp.Create(Result,Token(ttPlus,'+'),shift);
    end;
    Inc(p);
    bit := bit shl 1;
  until bit > n;
end;
function  DivToShift(leftArg : TAST; n : Integer) : TAST;
var
  shift : Integer;
begin
  shift := PowerOf2ToShift(n);
  Result := simplify(leftArg);
  if (n = 1) then Exit;
  
  if (Shift > 0) then
    Result := TBinOp.Create(Result,Token(ttShr,'>>'),TNumber.Create(shift));
end;
function  simplifyUnaryOp(node : TUnaryOp) : TAST;
var
  right : TAST;
begin
  Result := node;

  right  := simplify(node.expr);

  if (right.isInteger) then begin
    case node.op.tType of
      ttNot : Result := TNumber.Create(TNumber(right).value xor -1);
      ttNeg : Result := TNumber.Create(TNumber(right).value *   -1);
    end;
  end;
end;

function  simplifyBinOp(node : TBinOp) : TAST;
var
  left,right : TAST;
begin
  Result := node;

  left  := simplify(node.left);
  right := simplify(node.right);

  if (left.isString and right.isString) and (node.op.tType = ttPlus) then begin
    Result := TString.Create(TString(left).value + TString(right).value);
  end
  else
  if (left.isInteger) and (right.isInteger) then begin
    left  := TNumber(left);
    right := TNumber(right);

    case node.op.tType of
      ttPlus  : Result := TNumber.Create(TNumber(left).value +   TNumber(right).value);
      ttMinus : Result := TNumber.Create(TNumber(left).value -   TNumber(right).value);
      ttTimes : Result := TNumber.Create(TNumber(left).value *   TNumber(right).value);
      ttSlash : Result := TNumber.Create(TNumber(left).value div TNumber(right).value);
      ttDiv   : Result := TNumber.Create(TNumber(left).value div TNumber(right).value);

      ttMod   : Result := TNumber.Create(TNumber(left).value mod TNumber(right).value);

      ttShl   : Result := TNumber.Create(TNumber(left).value shl TNumber(right).value);
      ttShr   : Result := TNumber.Create(TNumber(left).value shr TNumber(right).value);

      ttAnd   : Result := TNumber.Create(TNumber(left).value and TNumber(right).value);
      ttOr    : Result := TNumber.Create(TNumber(left).value or  TNumber(right).value);
      ttXor   : Result := TNumber.Create(TNumber(left).value xor TNumber(right).value);

      ttEql   : Result := TNumber.CreateBool(TNumber(left).value =  TNumber(right).value);
      ttNeq   : Result := TNumber.CreateBool(TNumber(left).value <> TNumber(right).value);
      ttLss   : Result := TNumber.CreateBool(TNumber(left).value <  TNumber(right).value);
      ttLeq   : Result := TNumber.CreateBool(TNumber(left).value <= TNumber(right).value);
      ttGtr   : Result := TNumber.CreateBool(TNumber(left).value >  TNumber(right).value);
      ttGeq   : Result := TNumber.CreateBool(TNumber(left).value >= TNumber(right).value);
    end;
  end
  else
  if (left.isVariable) and (node.op.tType = ttTimes) and isPositiveInteger(right) then
    Result := ExpandMultiply(left,TNumber(right).value)
  else
  if (right.isVariable) and (node.op.tType = ttTimes) and isPositiveInteger(left) then begin
    if TNumber(left).value = 0 then
      Result := TNumber.Create(0);
  end
  else
  if (node.op.tType in [ttDiv,ttSlash]) and right.isInteger and isPowerOf2(TNumber(right).value) then begin
    Result := DivToShift(left,TNumber(right).value);
  end;
end;

function  simplify(e : TAST) : TAST;
begin
  Result := e;

  if      (e is TUnaryOp) then Result := simplifyUnaryOp(TUnaryOp(e))
  else if (e is TBinOp)   then Result := simplifyBinOp(TBinOp(e));
end;

//----------------------------------------------------
//   AST nodes
//----------------------------------------------------
constructor TAST.Create;
begin
  size := 0;

  sourceLine := -1;
  sourceFile := '';

  isAddress         := False;
  arrayParam        := nil;
  deref             := False;
  isAbsoluteAddress := False;
  isSigned          := False;
  sym               := nil;
  isLoopVar         := False;
  sizeOverride      := -1;

  isRecordVar       := False;
  derefRWsize       := 1;
  derefOffset       := 0;

  isParam           := False;
end;

function  TAST.isLeaf : Boolean;
begin
  Result := False;
end;

function  TAST.isInteger : Boolean;
begin
  Result := False;
end;

function  TAST.isString : Boolean;
begin
  Result := False;
end;

function TAST.isVariable : Boolean;
begin
  Result := False;
end;

function  TAST.isOp : Boolean;
begin
  Result := False;
end;

function  TAST.isUnaryOp : Boolean;
begin
  Result := False;
end;

function  TAST.isBinOp : Boolean;
begin
  Result := False;
end;

procedure TAST.acceptVisitor(aVisitor : IASTVisitor);
begin
  WriteLn('"',self.ClassName,'.acceptVisitor()" not implemented!!');
end;

function  TAST.clone : TAST;
begin
  Result      := TAST.Create;
  Result.size := size;

  Result.isAddress  := isAddress;
  Result.arrayParam := arrayParam.clone;
  Result.deref      := deref;
  Result.isSigned   := isSigned;
  Result.sym        := sym;
  Result.sourceLine   := sourceLine;
  Result.isLoopVar    := isLoopVar;
  Result.sizeOverride := sizeOverride;
  Result.derefRWsize  := derefRWsize;

  Result.isAbsoluteAddress := isAbsoluteAddress;
end;

function  TAST.toString : AnsiString;
begin
  Result := '';
end;

function  TAST.equals(value : TAST) : Boolean;
begin
  Result := False;

  if value = nil then Exit;

  if not(value is TAST) then Exit;

  if ClassName <> value.ClassName then Exit;

  Result := toString = value.toString;
end;

constructor TDecimalMode.create(aIsOn : boolean);
begin
  inherited create;

  isOn := aIsOn;
end;

procedure TDecimalMode.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitDecimalMode(self);
end;

constructor TUnaryOp.Create(aOp : TToken; aExpr : TAST);
begin
  op   := aOp;
  expr := aExpr;
end;

constructor TUnaryOp.Create(aOp : TTokenType; aOpValue : AnsiString; aExpr : TAST);
begin
  op   := uToken.Token(aOp,aOpValue);
  expr := aExpr;
end;

procedure TUnaryOp.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitUnaryOp(self);
end;

function  TUnaryOp.clone : TAST;
begin
  Result := TUnaryOp.Create(op,expr.clone);
end;

function  TUnaryOp.isOp  : Boolean;
begin
  Result := True;
end;

function  TUnaryOp.isUnaryOp : Boolean;
begin
  Result := True;
end;

function  TUnaryOp.toString : AnsiString;
begin
  Result := op.tValue;
end;

constructor TCastOp.Create(aOp : TToken; aExpr : TAST; aNewsize : Integer; aIsSigned : Boolean);
begin
  inherited Create(aOp,aExpr);

  newSize  := aNewSize;
  isSigned := aIsSigned;
end;

constructor TCastOp.Create(aExpr : TAST; aNewsize : Integer; aIsSigned : Boolean);
var
  Op : TToken;
begin
  if aIsSigned then begin
    case aNewSize of
      1 : Op := Token(ttShortInt,'shortint');
      2 : Op := Token(ttInteger ,'Integer');
      3 : Op := Token(ttInteger ,'Int24');
    end;
  end
  else begin
    case aNewSize of
      1 : Op := Token(ttByte,'byte');
      2 : Op := Token(ttWord,'word');
      3 : Op := Token(ttInteger ,'Int24');
    end;
  end;

  Create(Op,aExpr,aNewSize,aIsSigned);
end;

function  TCastOp.clone : TAST;
begin
  Result := TCastOp.Create(op,expr.clone,newSize,isSigned);
end;

constructor TBinOp.Create(aLeft : TAST; aOp : TToken; aRight : TAST);
begin
  left  := aLeft;
  op    := aOp;
  right := aRight;
end;

constructor TBinOp.Create(aLeft : TAST; aOp : TTokenType; aOpStr : AnsiString; aRight : TAST);
begin
  left      := aLeft;
  op.tType  := aOp;
  op.tValue := aOpStr;
  right     := aRight;
end;

procedure TBinOp.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitBinOp(self);
end;

function  TBinOp.clone : TAST;
begin
  Result := TBinOp.Create(left.clone,op,right.clone);
end;

function  TBinOp.isOp  : Boolean;
begin
  Result := True;
end;

function  TBinOp.isBinOp : Boolean;
begin
  Result := True;
end;

function  TBinOp.toString : AnsiString;
begin
  Result := op.tValue;
end;

constructor TNumber.Create(aToken : TToken);
var
  v : Integer;
begin
  inherited Create;

  if (aToken.tValue = '%') or (aToken.tValue = '$') then
    aToken.tValue := aToken.tValue + '0';

  token := aToken;
  if (token.tValue[1] = '%') then
    value := BinToInt64(Copy(token.tValue,2,Length(token.tValue)))
  else begin
    TryStrToInt(aToken.tValue,v);

    value := v;
  end;

  GetIntegerSize(aToken.tValue,size);

  isSigned := aToken.tValue[1] = '-';
end;

constructor TNumber.Create(aValue : Integer);
begin
  token := uToken.Token(ttIntNum,aValue);
  value := aValue;

  GetIntegerSize(IntToStr(aValue),size);

  isSigned := aValue < 0;
end;

constructor TNumber.CreateBool(aValue : Boolean);
begin
  token := uToken.Token(ttIntNum,-1*ord(aValue));
  value := aValue;
  size  := 1;

  isSigned := False;
end;

function  TNumber.isLeaf : Boolean;
begin
  Result := True;
end;

function  TNumber.isInteger : Boolean;
begin
  Result := True;
end;

function  TNumber.clone : TAST;
begin
  Result := TNumber.Create(token);
end;

procedure TNumber.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitNumber(self);
end;

function  TNumber.toString : AnsiString;
begin
  Result := token.tValue;
end;

constructor TVariable.Create(aToken : TToken; aSize : Integer; aIsSigned : Boolean = false);
var
  v : Integer;
begin
  inherited Create;

  recName      := '';
  token        := aToken;
  value        := aToken.tValue;
  size         := aSize;
  isPointer    := False;
  isVarAddress := False;
  isRegister   := False;
  isFuncResult := False;
  loReg        := '';
  hiReg        := '';
  address      := '';
  arraySize    := -1;
  use8BitValue := False;
  indexOffset  := '';
  isSigned     := aIsSigned;
  onStack      := False;
end;

constructor TVariable.Create(aValue : AnsiString; aSize : Integer; aIsSigned : Boolean = false);
begin
  inherited Create;

  recName      := '';
  token        := uToken.Token(ttIdent,aValue);
  value        := aValue;
  size         := aSize;
  isPointer    := False;
  isVarAddress := False;
  isRegister   := False;
  isFuncResult := False;
  loReg        := '';
  hiReg        := '';
  address      := '';
  arraySize    := -1;
  use8BitValue := False;
  indexOffset  := '';
  isSigned     := aIsSigned;
  onStack      := False;
end;

function  TVariable.isLeaf : Boolean;
begin
  Result := True;
end;

function  TVariable.isVariable : Boolean;
begin
  Result := True;
end;

procedure TVariable.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitVariable(self);
end;

function  TVariable.clone : TAST;
begin
  Result := TVariable.Create(token,size);

  TVariable(Result).sym       := sym;
  TVariable(Result).deref     := deref;
  TVariable(Result).isPointer := isPointer;
  TVariable(Result).isVarAddress := isVarAddress;
  TVariable(Result).isPointer    := isPointer;
  TVariable(Result).sizeOverride := sizeOverride;

  if arrayParam <> nil then
    TVariable(Result).arrayParam := arrayParam.clone;

  TVariable(Result).isRegister := isRegister;
  TVariable(Result).loReg      := loReg;
  TVariable(Result).hiReg      := hiReg;
  TVariable(Result).isAddress  := isAddress;
  TVariable(Result).isAbsoluteAddress := isAbsoluteAddress;
  TVariable(Result).use8BitValue      := use8BitValue;

  TVariable(Result).indexOffset := indexOffset;

  TVariable(Result).recName := recName;
  TVariable(Result).isRecordVar := isRecordVar;
  TVariable(Result).onStack     := onStack;
  TVariable(Result).stackOfs    := stackOfs;
  TVariable(Result).token       := token;
  TVariable(Result).derefRWsize := derefRWsize;
  TVariable(Result).derefOffset := derefOffset;
end;

function  TVariable.toString : AnsiString;
begin
  Result := token.tValue;
  if   isVarAddress then
    Result := '@'+Result
  else
  if deref then
    Result := '^'+Result;
end;

constructor TString.Create(aToken : TToken);
var
  v : Integer;
begin
  inherited Create;

  token := aToken;
  value := aToken.tValue;
  size  := Length(aToken.tValue);
  // default to screen code
  sType := ttString;
end;

constructor TString.Create(aValue : AnsiString);
begin
  inherited Create;

  token := uToken.Token(ttIdent,aValue);
  value := aValue;
  size  := Length(aValue);
  // default to screen code
  sType := ttString;
end;

function  TString.isLeaf : Boolean;
begin
  Result := True;
end;

function  TString.isString : Boolean;
begin
  Result := True;
end;

procedure TString.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitString(self);
end;

function  TString.toString : AnsiString;
begin
  Result := token.tValue;
end;

constructor TAssign.Create(aVar : TAST; aExpr : TAST);
begin
  inherited Create;

  variable   := TVariable(aVar);
  op         := Token(ttBecomes,':=');
  expr       := aExpr;
  decimalModeOn := False;
end;

procedure TAssign.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitAssign(self);
end;

procedure TPoke.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitPoke(self);
end;

constructor TCompound.Create;
begin
  inherited Create;

  children := TASTList.Create;
end;

destructor TCompound.Destroy;
begin
  FreeAndNil(children);
end;

procedure TCompound.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitCompound(self);
end;

constructor TAsmCompound.Create;
begin
  inherited Create;

  asmStatements := TStringList.Create;
end;

destructor TAsmCompound.Destroy;
begin
  FreeAndNil(asmStatements);
end;

procedure TAsmCompound.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitAsmCompound(self);
end;

constructor TVarDecl.Create(aName : AnsiString; aVarSym : TSymbol; aAddress,aDefaultValue : AnsiString);
begin
  inherited Create;

  varName       := aName;
  varSym        := aVarSym;
  address       := aAddress;
  defaultValue  := aDefaultValue;
  isArray       := False;
  isPointer     := False;
  isPointerType := False;
  isVarAddress  := False;
  onStack       := False;
  stackOfs      := 0;
end;

procedure TVarDecl.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitVarDecl(self);
end;

constructor TImportSource.create(aFileName : AnsiString);
begin
  inherited Create;

  fileName := aFileName;
end;

procedure TImportSource.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitImportSource(self);
end;

constructor TConstDecl.Create(aName : AnsiString; aValue : TToken; aType : TSymbol);
begin
  inherited Create;

  constName  := aName;
  constValue := aValue;
  constType  := aType;
  isArray    := False;
end;

constructor TConstDecl.Create(aName : AnsiString; aValues : TNumberArray; aType : TSymbol);
begin
   inherited Create;

 constName  := aName;
  values     := aValues;
  constType  := aType;
  isArray    := True;
//  size       := aType.size;
end;

procedure TConstDecl.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitConstDecl(self);
end;

constructor TBlock.Create;
begin
  inherited Create;

  declarations := TASTList.Create;
  onStack      := False;
end;

destructor  TBlock.Destroy;
begin
  FreeAndNil(declarations);
end;

procedure TBlock.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitBlock(self);
end;

constructor TAsmBlock.Create;
begin
  inherited Create;

  asmStatements := TStringList.Create;
end;

destructor  TAsmBlock.Destroy;
begin
  inherited Destroy;

  FreeAndNil(asmStatements);
end;

procedure TAsmBlock.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitAsmBlock(self);
end;

procedure TUnitBlock.acceptVisitor(aVisitor: IASTVisitor);
begin
  aVisitor.visitUnitBlock(self);
end;

constructor TProgram.Create(aName: AnsiString; aBlock: TBlock; aScope : TscopedSymbolTable);
begin
  inherited Create;

  unitBlocks         := TASTList.Create;
  imports            := TStringList.Create;
  segments              := TList.Create;
  binImports         := TList.Create;
  deferredBinImports := TList.Create;
  absoluteArrayData  := TList.Create;
  name               := aName;
  block              := aBlock;
  scope              := aScope;
end;

destructor  TProgram.Destroy;
begin
   FreeAndNil(imports);
   FreeAndNil(segments);
end;

procedure TProgram.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitProgram(self);
end;

constructor TMacroDecl.Create(aName : AnsiString; const aParams : TStringArray; aBlock : TBlock; aScope : TscopedSymbolTable);
var
  i : Integer;
begin
  inherited Create;

  macroName   := aName;

  SetLength(macroParams,Length(aParams));

  for i := 0 to High(aParams) do
    macroParams[i] := aParams[i];

  block       := aBlock;
  scope       := aScope;
end;

procedure TMacroDecl.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitMacroDecl(self);
end;

constructor TProcDecl.Create(aName : AnsiString; aParams : TParamArray; aBlock : TBlock; aScope : TscopedSymbolTable);
begin
  inherited Create;

  procName   := aName;
  procParams := aParams;
  block      := aBlock;
  scope      := aScope;
  onStack    := False;
end;

procedure TProcDecl.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitProcDecl(self);
end;

constructor TFuncDecl.Create(aName : AnsiString; aParams : TParamArray; aBlock : TBlock; aScope : TscopedSymbolTable);
begin
  inherited Create;

  funcName   := aName;
  funcParams := aParams;
  block      := aBlock;
  scope      := aScope;
  onStack    := False;
end;

procedure TFuncDecl.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitFuncDecl(self);
end;

constructor TCall.Create;
begin
  inherited Create;

  params := TASTList.Create;
  stackSize := 0;
  onStack   := False;
end;

destructor  TCall.Destroy;
begin
  FreeAndNil(params);
end;

procedure TCall.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitCall(self);
end;

constructor TMacroCall.Create;
begin
  inherited Create;

  params := TStringList.Create;
end;

destructor  TMacroCall.Destroy;
begin
  FreeAndNil(params);
end;

procedure TMacroCall.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitMacroCall(self);
end;

constructor TFuncCall.Create;
begin
  inherited Create;

  params    := TASTList.Create;
  funcType  := ttFunc;
  onStack   := False;
  stackSize := 0;
end;

destructor  TFuncCall.Destroy;
begin
  FreeAndNil(params);
end;

procedure TFuncCall.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitFuncCall(self);
end;

function  TFuncCall.clone : TAST;
var
  f : TFuncCall;
  i : Integer;
begin
  inherited;

  f := TFuncCall.Create;
  f.funcName := funcName;
  f.funcType := funcType;
  f.size     := size;

  for i := 0 to f.params.Count - 1 do
    f.params.Add(TAST(params.Items[i]).clone);
end;

function  TFuncCall.toString : AnsiString;
begin
  Result := funcName + '()';
end;

procedure TSegmentDecl.acceptVisitor(aVisitor: IASTVisitor);
begin
  aVisitor.visitSegmentDecl(self);
end;

function TSegmentDecl.toString : AnsiString;
begin
  Result := '.SEGMENT '+segmentName;
end;

constructor TBuiltinProcedure.Create(aType : TTokenType);
begin
  inherited Create;

  procType := aType;
  params   := TASTList.Create;
end;

destructor  TBuiltinProcedure.Destroy;
begin
  FreeAndNil(params);
end;

procedure TBuiltinProcedure.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitBuiltinProcedure(self);
end;

constructor TBuiltinFunction.Create(aType : TTokenType);
begin
  inherited Create;

  funcType := aType;
  params   := TASTList.Create;
end;

destructor  TBuiltinFunction.Destroy;
begin
  FreeAndNil(params);
end;

procedure TBuiltinFunction.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitBuiltinFunction(self);
end;

function  TBuiltinFunction.toString : AnsiString;
begin
  Result := LowerCase(funcName);
end;

function  TPeek.expand(aTacList : TObject) : TAST;
var
  temp     : TAST;
  temp2    : TAST;
  srcAddr  : TAST;
  tacNode  : TTACNode;
  funcVar  : TVariable;
  peekVar  : TVariable;
begin
  temp  := TVariable.Create(uToken.Token(ttIdent,TTACList(aTacList).getTemp),1);
  temp2 := nil;

  Result := temp;

  srcAddr  := TAST(params[0]);

  if srcAddr.isInteger then begin
    srcAddr.size := 1;
//    srcAddr.isAbsoluteAddress := True;
    TNumber(srcAddr).token.tValue := '!'+TNumber(srcAddr).token.tValue;
  end
  else
  if srcAddr.isVariable then begin
    srcAddr.size := 1;
    srcAddr.deref := True;
    srcAddr.arrayParam := TNumber.Create(0);
  end{
  else
  if srcAddr.isBinOp then begin
    temp2   := TVariable.Create(uToken.Token(ttIdent,TTACList(aTacList).getTemp),2);
    tacNode := TTacNode.Create(temp2,nil,srcAddr,nil,nil,nil);
    TTACList(aTacList).addNode(tacNode);
    tacNode := TTacNode.Create(temp,nil,temp2,nil,nil,nil);
    TTACList(aTacList).addNode(tacNode);
    Exit;
  end};

  // add node loading src into temp variable and return the temp
  tacNode := TTacNode.Create(temp,nil,srcAddr,nil,nil,nil,nil);

  TTACList(aTacList).addNode(tacNode);
end;

procedure TIfThen.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitIfThen(self);
end;

procedure TWhile.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitWhile(self);
end;

constructor TRepeat.Create;
begin
  inherited Create;

  children := TASTList.Create;
end;

destructor TRepeat.Destroy;
begin
  FreeAndNil(children);
end;

procedure TRepeat.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitRepeat(self);
end;

procedure TFor.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitFor(self);
end;

constructor TParam.Create(aName: AnsiString; aType: TSymbol);
begin
  inherited Create;

  paramName := aName;
  paramType := aType;
end;

procedure TAbsoluteArrayData.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitAbsoluteArrayData(self);
end;

procedure TImportBinary.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitImportBinary(self);
end;

procedure TExit.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitExit(self);
end;

procedure TBreak.acceptVisitor(aVisitor : IASTVisitor);
begin
  aVisitor.visitBreak(self);
end;

procedure TSetupRasterIRQ.acceptVisitor(aVisitor: IASTVisitor);
begin
  aVisitor.visitSetupRasterIRQ(self);
end;

procedure TEndIRQ.acceptVisitor(aVisitor: IASTVisitor);
begin
  aVisitor.visitEndIRQ(self);
end;

procedure TEmpty.acceptVisitor(aVisitor : IASTVisitor);
begin

end;

end.
