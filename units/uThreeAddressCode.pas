unit uThreeAddressCode;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  System.Classes,uAST, uToken,uASM,uSymbolTable;

const
  TEMP_REG        = '_tr_';
  MAX_TEMP         = 16;

  FUNC_RESULT_NAME = '_fr_';
  MAX_FUNC_RESULT  = 3;
                                             
type
  TCT = (
    ctBinOp,
    ctUnaryOp,
    ctValue,
    ctIndexed
  );
//---------------------------------------------------------
// three address code node
//---------------------------------------------------------
  TTACNode = class
  private
  protected
    Ftype : TCT;

    function  getNodeFragmentStr(node : TAST; symbolTable : TscopedSymbolTable) : AnsiString;
    procedure init;
  public
    left        : TAST;
    op          : TAST;
    right1      : TAST;
    right2      : TAST;
    leftIndex   : TAST;
    right1Index : TAST;
    right2Index : TAST;
    isTemp      : Boolean;
    falseLabel  : AnsiString;

    constructor Create(aLeft,aOp,aRight1,aRight2,aLeftIndex,aRight1Index,aRight2Index : TAST);               overload;

    function  equals(value : TAST) : boolean;
    function  matches(node : TTACNode) : Boolean;

    function  getOp : TTokenType;
    function  getFragmentName(var aFragmentStr : AnsiString; symbolTable : TscopedSymbolTable; doingCondition : Boolean) : Boolean;
    function  toString : AnsiString;

    property nodeType : TCT read Ftype write Ftype;
  end;

//---------------------------------------------------------
// three address code list
//---------------------------------------------------------
  TTACList = class
  private
    FtacList : TStringList;
    FtempNum : Integer;
    FdecimalModeOn: Boolean;

    function  getCount : Integer;
    function  getNode(index : Integer) : TTACNode;

    function  isSimpleIncrement(left : TAST; right1 : TAST; op : TTokenType; right2 : TAST) : Boolean;
    function  isSimpleDecrement(left : TAST; right1 : TAST; op : TTokenType; right2 : TAST) : Boolean;
    function  isSimpleShift(left : TAST; right1 : TAST; op : TTokenType; right2 : TAST) : Boolean;

    function  doCopyPropagation : Boolean;
    function  doAgebricSimplification : Boolean;
    function  removeDeadCode : Boolean;
    procedure loadStoreTransferOptimisation;

    function  getDestVar : TAST;
    function  getLastNode : TTACNode;
  public

    constructor Create;
    destructor  Destroy;

    class function  equals(ast : TAST; aValue : Integer) : Boolean;
    class function  isTemp(ast : TAST) : Boolean;
    class function  getOp(ast : TAST) : TTokenType;

    function getTemp : AnsiString;

    procedure reset;
    procedure addNode(aType : TTACNode);

    function  optimise : Boolean;
    procedure debugPrint;

    property  count : Integer read getCount;
    property  Items[index : Integer] : TTACNode read getNode;
    property  destVar  : TAST     read getDestVar;
    property  lastNode : TTACNode read getLastNode;
    property  decimalModeOn : Boolean read FdecimalModeOn write FdecimalModeOn;
  end;

implementation

uses
  System.SysUtils,System.StrUtils,uCodeGen_6502,uFragments,uParserAST;

//---------------------------------------------------------
// three address code node
//---------------------------------------------------------
procedure TTACNode.init;
begin
  left   := nil;
  op     := nil;
  right1 := nil;
  right2 := nil;
  isTemp := False;
end;

constructor TTACNode.Create(aLeft,aOp,aRight1,aRight2,aLeftIndex,aRight1Index,aRight2Index : TAST);
// aLeft[aLeftIndex] := aRight1[aRight1Index] <aOp> aRight2[aRight2Index]
begin
  init;

  Ftype       := ctValue;
  left        := aLeft;
  op          := aOp;
  right1      := aRight1;
  right2      := aRight2;
  leftIndex   := aLeftIndex;
  right1Index := aRight1Index;
  right2Index := aRight2Index;
  falseLabel  := '';

  if      (aLeftIndex <> nil) or (aRight1Index <> nil) or (aRight2Index <> nil) then Ftype := ctIndexed
  else if (op = nil)     then Ftype := ctValue
  else if (op.isUnaryOp) then Ftype := ctUnaryOp
  else if (op.isBinOp)   then Ftype := ctBinOp
end;

function  isEqual(n1,n2 : TAST) : Boolean;
begin
  Result := False;

  if n1 = nil then Exit;
  if n2 = nil then Exit;

  if (n1.ClassName <> n2.ClassName) then Exit;

  Result := n1.toString = n2.toString;
end;

function  TTACNode.equals(value: TAST) : Boolean;
begin
  Result := False;
end;

function  TTACNode.matches(node : TTACNode) : Boolean;
begin

end;

function  TTACNode.getOp : TTokenType;
begin
  Result := ttEOF;

  if op = nil then Exit;

  if op.isUnaryOp then Result := TUnaryOp(op).op.tType;
  if op.isBinOp   then Result := TBinOp(op).op.tType;
end;

function  TTACNode.getNodeFragmentStr(node : TAST; symbolTable : TscopedSymbolTable) : AnsiString;
var
  val      : AnsiString;
  sym      : TSymbol;
  isSigned : Boolean;
begin
  Result := '';
  val := getNodeMemOrValueStr(node,symbolTable);
  Result := Result + val;
  isSigned := False;

  if node is TVariable then begin
    isSigned := TVariable(node).isSigned;
  end
  else
  if node is TNumber then
    isSigned := TNumber(node).isSigned;

//  Result := Result + PNTR[node.deref];
  if val[1] = '@' then begin
  // address is 16-bit
    Result := Result + Format('%s%d',[SIGNED[isSigned],16]);
    Result := Copy(Result,2,length(Result));
  end
  else
  if (node.isString) or (node.size = 0) then
    Result := Result + Format('%s%d',[SIGNED[isSigned],8])
  else begin
    if node.deref then begin
      Result := Result + Format('%s%d',[SIGNED[isSigned],node.derefRWsize*8]);
    end
    else
      Result := Result + Format('%s%d',[SIGNED[isSigned],node.size*8]);
  end;
//  Result := Result + SIGNED[node.isSigned];
{
  if node.isVariable then begin
    sym := symbolTable.lookup(TVariable(node).token.tValue);

    if sym is TVarSymbol then
      Result := Result + Format('_sp(%d)',[sym.stackOfs]);
  end;
}
end;

function  TTACNode.getFragmentName(var aFragmentStr : AnsiString; symbolTable : TscopedSymbolTable; doingCondition : Boolean) : Boolean;
// returns false if not normal fragment string, ie. func call, etc.
begin
  aCount := 1;
  cCount := 1;
  mCount := 1;
  pCount := 1;
  sCount := 1;

  Result := False;

  if left is TFuncCall then Exit;
  if left is TPeek     then Exit;
  if left is TPoke     then Exit;

//  Result := right1 <> nil;
  Result := True;

  aFragmentStr := '';

  if left <> nil then begin
    aFragmentStr := aFragmentStr + getNodeFragmentStr(left,symbolTable);
    if leftIndex <> nil then begin
      aFragmentStr := aFragmentStr + '[' + getNodeFragmentStr(leftIndex,symbolTable) + ']';

//      if (left is TVariable) and left.deref and (left.derefOffset <> 0) then
//        aFragmentStr := aFragmentStr + '_ofs_' + getNodeFragmentStr(TNumber.Create(Token(ttInteger,TVariable(left).derefOffset)),symbolTable)
//      else
      if (left is TVariable) and (TVariable(left).indexOffset <> '') then
        aFragmentStr := aFragmentStr + '_ofs_' + getNodeFragmentStr(TNumber.Create(Token(ttInteger,TVariable(left).indexOffset)),symbolTable);
    end;
  end;

  if right1 <> nil then begin
    if left <> nil then
      aFragmentStr := aFragmentStr + '=' + getNodeFragmentStr(right1,symbolTable)
    else
      aFragmentStr := getNodeFragmentStr(right1,symbolTable);

    if right1Index <> nil then begin
      aFragmentStr := aFragmentStr + '[' + getNodeFragmentStr(right1Index,symbolTable) + ']';

//      if (right1 is TVariable) and right1.deref and (right1.derefOffset <> 0) then
//        aFragmentStr := aFragmentStr + '_ofs_' + getNodeFragmentStr(TNumber.Create(Token(ttInteger,TVariable(right1).derefOffset)),symbolTable)
//      else
      if (right1 is TVariable) and (TVariable(right1).indexOffset <> '') then
        aFragmentStr := aFragmentStr + '_ofs_' + getNodeFragmentStr(TNumber.Create(Token(ttInteger,TVariable(right1).indexOffset)),symbolTable);
    end;
  end;

  if op <> nil then
    aFragmentStr := aFragmentStr + '_' + getOpStr(TOperator(op).op) + '_';

  if right2 <> nil then begin
    aFragmentStr := aFragmentStr + getNodeFragmentStr(right2,symbolTable);
//    if right2.arrayParam <> nil then aFragmentStr := aFragmentStr + '[' + getNodeFragmentStr(right2.arrayParam,symbolTable) + ']';
  end;

  if falseLabel <> '' then begin
    aFragmentStr := aFragmentStr + '_l1';
  end;

  aFragmentStr := aFragmentStr + '.asm';
end;

function  TTACNode.toString : AnsiString;
var
  node : TTACNode;

  procedure outputTypedPointerInfo(ast : TAST);
  var
    recName : AnsiString;
  begin
    if ast = nil then Exit;

    if not(ast.isVariable) then Exit;

    if TVariable(ast).recName = '' then Exit;

    recName := TVariable(ast).recName;

    Result := Result + ' ('+recName+')';
  end;
begin
  node := self;

  Result := '';
  
  if node.left is TFuncCall then
    Result := node.left.toString+'()'
  else begin
    if node.left <> nil then Result := node.left.toString;
    
    if node.leftIndex <> nil then begin
      Result := Result + '['+node.leftIndex.toString+']';

      outputTypedPointerInfo(node.left);

      if (node.left is TVariable) and (TVariable(node.left).indexOffset <> '') then
        Result := Result + ' + ' + TVariable(node.left).indexOffset;
    end;

    if node.left <> nil then Result := Result + ' := ';

    if (node.op is TUnaryOp) then begin
      Result := Result + node.op.toString+' ';
      if node.right2 <> nil then
        Result := Result + node.right2.toString;
    end
    else
    if (node.op is TBinOp) then begin
      Result := Result + node.right1.toString;
      Result := Result + ' '+node.op.toString+' ';
      Result := Result + node.right2.toString;
    end
    else
    if node.left is TFuncCall then
      Result := Result + node.left.toString
    else
    if node.left is TPeek then
      Result := Result + node.left.toString
    else
    begin
      // value node only, no op
      Result := Result + node.right1.toString;
      if node.right1Index <> nil then begin
        Result := Result + '['+node.right1Index.toString+']';

      outputTypedPointerInfo(node.right1);

      if (node.right1 is TVariable) and (TVariable(node.right1).indexOffset <> '') then
        Result := Result + ' + ' + TVariable(node.right1).indexOffset;
      end;
    end;
  end;
end;

//---------------------------------------------------------
// three address code list
//---------------------------------------------------------
function TTACList.getTemp : AnsiString;
begin
  Result := Format(TEMP_REG+'%d',[FtempNum]);
  Inc(FtempNum);
end;

constructor TTACList.Create;
begin
  FtacList := TStringList.Create;
  reset;
end;

destructor  TTACList.Destroy;
begin
  FtacList.Free;
end;

procedure TTACList.reset;
begin
  FtempNum := 0;
end;

procedure TTACList.addNode(aType : TTACNode);
begin
  FtacList.AddObject(aType.left.toString,aType);
end;

function  TTACList.getCount : Integer;
begin
  Result := FtacList.Count;
end;

function TTACList.getNode(index : Integer) : TTACNode;
begin
  Result := TTACNode(FtacList.Objects[index]);
end;

class function  TTACList.equals(ast : TAST; aValue : Integer) : Boolean;
begin
  Result := False;

  if ast = nil         then Exit;
  if not ast.isInteger then Exit;

  Result := (TNumber(ast).value = aValue);
end;

class function  TTACList.isTemp(ast : TAST) : Boolean;
begin
  Result := False;

  if ast = nil then Exit;

  Result := Pos(TEMP_REG,ast.toString) > 0;
end;

class function  TTACList.getOp(ast : TAST) : TTokenType;
begin
  Result := ttEOF;

  if ast = nil    then Exit;
  if not ast.isOp then Exit;

  if ast.isUnaryOp then Result := TUnaryOp(ast).op.tType;
  if ast.isBinOp   then Result := TBinOp(ast).op.tType;
end;

function  TTACList.isSimpleIncrement(left : TAST; right1 : TAST; op : TTokenType; right2 : TAST) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if not(left is TVariable) then Exit;
  if right1 = nil then Exit;
  if right2 = nil then Exit;
  if op <> ttPlus then Exit;

  if (left.toString = right1.toString) and equals(right2,1) then
    Result := True
  else
  if (left.toString = right2.toString) and equals(right1,1) then
    Result := True;
end;

function  TTACList.isSimpleDecrement(left : TAST; right1 : TAST; op : TTokenType; right2 : TAST) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if not(left is TVariable) then Exit;
  if right1 = nil then Exit;
  if right2 = nil then Exit;
  if op <> ttMinus then Exit;

  if (left.toString = right1.toString) and equals(right2,1) then
    Result := True;
end;

function  TTACList.isSimpleShift(left : TAST; right1 : TAST; op : TTokenType; right2 : TAST) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if not(left is TVariable) then Exit;
  if right1 = nil then Exit;
  if right2 = nil then Exit;
  if not(op in [ttSHL,ttSHR]) then Exit;

  if (left.toString = right1.toString) then
    Result := True;
end;

function  TTACList.doCopyPropagation : Boolean;
var
  i         : Integer;
  node      : TTACNode;
  index     : Integer;
  foundNode : TTACNode;

  function  findMatchAndReplaceWithConst(var aNode : TAST) : Boolean;
  begin
    Result := False;

    if aNode = nil then Exit;

    index := FtacList.IndexOf(aNode.ToString);

    if index = -1 then Exit;

    foundNode := TTACNode(FtacList.Objects[index]);

    if foundNode.nodeType <> ctValue then Exit;

    // replace node with value
    aNode := foundNode.right1;

    Result := True;
  end;

begin
  Result := False;

  for i := FtacList.Count - 1 downto 1 do begin
    // find each node and check to see if there is a matching
    // const value node and replace with that one
    node := TTACNode(FtacList.Objects[i]);

    if isTemp(node.right1)     then Result := findMatchAndReplaceWithConst(node.right1)     or Result;
    if isTemp(node.right2)     then Result := findMatchAndReplaceWithConst(node.right2)     or Result;
    if isTemp(node.leftIndex)  then Result := findMatchAndReplaceWithConst(node.leftIndex)  or Result;
    if isTemp(node.right1Index) then Result := findMatchAndReplaceWithConst(node.right1Index) or Result;
    if isTemp(node.right2Index) then Result := findMatchAndReplaceWithConst(node.right2Index) or Result;

    // if this node refers to another node with no temps, then propagate that one too
    if not isTemp(node.left) and (node.nodeType in [ctValue]) then begin
      if node.right1 <> nil then begin

        // find matching temp node
        index := FtacList.IndexOf(node.right1.toString);

        if index <> -1 then begin
          foundNode := TTACNode(FtacList.Objects[index]);

  //        if not isTemp(foundNode.right1) and not isTemp(foundNode.right2) then begin
            node.right1     := foundNode.right1;
            node.right2     := foundNode.right2;
            node.op         := foundNode.op;
            node.leftIndex  := foundNode.leftIndex;
            node.right1Index := foundNode.right1Index;
            node.right2Index := foundNode.right2Index;
            node.nodeType   := foundNode.nodeType;

            Result := True;
  //        end;
        end;
      end;
    end;
  end;
end;

function  TTACList.doAgebricSimplification : Boolean;
var
  i    : Integer;
  node : TTACNode;
  op   : TTokenType;
  r1   : TAST;
  r2   : TAST;
  left : TAST;
begin
  Result := False;

  for i := 0 to FtacList.Count - 1 do begin
    node := TTACNode(FtacList.Objects[i]);

    op   := getOp(node.op);
    r1   := node.right1;
    r2   := node.right2;
    left := node.left;

    if (op = ttLss) and equals(r2,0) then begin
      node.right2   := nil;
      node.op       := TUnaryOp.Create(Token(ttLssZero,'<'),r1);
      node.nodeType := ctUnaryOp;
      Result        := True;
    end
    else
    if (op = ttTimes) and equals(r2,2) then begin
      node.right2   := nil;
      node.op       := TUnaryOp.Create(Token(ttMul2,'*'),r1);
      node.nodeType := ctUnaryOp;
      Result        := True;
    end
    else
    if (op in [ttDiv,ttSlash]) and equals(r2,2) then begin
      node.right2   := nil;
      node.op       := TUnaryOp.Create(Token(ttDiv2,'*'),r1);
      node.nodeType := ctUnaryOp;
      Result        := True;
    end
    else
    if (left <> nil) and (op = ttShr) and equals(r2,8) then begin
      // word = int24 >> 8 -> word = int24.hiword
      node.right2   := nil;
      node.op       := TUnaryOp.Create(Token(ttDiv256,'/'),r1);
      node.nodeType := ctUnaryOp;
      Result        := True;
    end
    else
    if (left <> nil) and (op = ttShl) and (left.size = 3) and (r1.size = 2) and equals(r2,8) then begin
      // int24 = word << 8 -> int.hiword = word
      node.right2   := nil;
      node.op       := TUnaryOp.Create(Token(ttMul256,'*'),r1);
      node.nodeType := ctUnaryOp;
      Result        := True;
    end
    else
    if (op = ttPlus) and (r1.toString = r2.toString) and (r1.size = 1) then begin
      // x + x -> x << 1
      node.right2   := TNumber.Create(1);
      node.op       := TBinOp.Create(r1,Token(ttShl,'<<'),node.right2);
      node.nodeType := ctBinOp;
      Result        := True;
    end
    else
    if ((op = ttTimes) and (equals(r1,0) or equals(r2,0))) or ((op = ttMinus) and (isEqual(r1,r2))) then begin
      // x * 0, 0 * x -> 0
      node.right1     := TNumber.Create(0);
      node.op         := nil;
      node.right2     := nil;
      node.right1Index := nil;
      node.nodeType   := ctValue;
      Result          := True;
    end
    else
    if (op = ttSlash) and isEqual(r1,r2) then begin
      // x / x => 1
      // replace with '1' value
      node.right1     := TNumber.Create(1);
      node.op         := nil;
      node.right2     := nil;
      node.right1Index := nil;
      node.nodeType   := ctValue;
      Result          := True;
    end
    else
    if (op = ttMinus) and equals(r2,0) then begin
      // x - 0 => x
      // replace with 'x' value
      node.op         := nil;
      node.right2     := nil;
      node.leftIndex  := nil;
      node.right1Index := nil;
      node.nodeType   := ctValue;
      Result          := True;
    end
    else
    if (op = ttPlus) and equals(r2,0) then begin
      // x + 0 => x
      // replace with 'x' value
      node.op         := nil;
      node.right2     := nil;
      node.leftIndex  := nil;
      node.right1Index := nil;
      node.nodeType   := ctValue;
      Result          := True;
    end
    else
    if (op = ttSlash) and equals(r2,1) then begin
      // x / 1 => x
      // replace with 'x' value
      node.op         := nil;
      node.right2     := nil;
      node.leftIndex  := nil;
      node.right1Index := nil;
      node.nodeType   := ctValue;
      Result          := True;
    end
    else
    if (op = ttSlash) and (node.nodeType = ctBinOp) and equals(r1,0) then begin
      // 0 / x => 0
      // replace with 0 value
      node.right1     := TNumber.Create(0);
      node.op         := nil;
      node.right2     := nil;
      node.right1Index := nil;
      node.nodeType   := ctValue;
      Result          := True;
    end
    else
    if (op = ttTimes) and equals(r2,-1) then begin
      // change x * -1 => neg x
      node.right1   := nil;
      node.right2   := r1;
      node.op       := TUnaryOp.Create(ttNeg,'neg',r1);
      node.nodeType := ctUnaryOp;
      Result        := True;
    end
    else
    if ((op = ttTimes) and equals(r1,-1)) or ((op = ttMinus) and equals(r1,0)) then begin
      // change -1 * x => neg x
      // change  0 - x => neg x
      node.right1   := nil;
      node.right2   := r2;
      node.op       := TUnaryOp.Create(ttNeg,'neg',r2);
      node.nodeType := ctUnaryOp;
      Result        := True;
    end
    else
    if isSimpleIncrement(left,r1,op,r2) and (not FdecimalModeOn) then begin
      // x = x + 1 -> inc x
      // x = 1 + x -> inc x
      node.right1   := nil;
      node.right2   := nil;
      node.op       := TUnaryOp.Create(ttInc,'inc',left);
      node.nodeType := ctUnaryOp;
      Result := True;
    end
    else
    if isSimpleDecrement(left,r1,op,r2) and (not FdecimalModeOn) then begin
      // x = x - 1 -> dec x
      node.right1   := nil;
      node.right2   := nil;
      node.op       := TUnaryOp.Create(ttDec,'dec',left);
      node.nodeType := ctUnaryOp;
      Result := True;
    end;

    if isSimpleShift(left,r1,op,r2) then begin
      // x = x << or x = x >> -> x << or >> without assign
      node.left := nil;
    end;

  end;
end;

function  TTACList.removeDeadCode : Boolean;
var
  srcI,checkI : Integer;
  srcNode     : TTACNode;
  checkNode   : TTACNode;
  found       : Boolean;
begin
  Result := False;

  srcI := 0;
  while srcI < FtacList.Count - 1 do begin
    srcNode := TTACNode(FtacList.Objects[srcI]);

    found := True;
    if isTemp(srcNode.left) then begin
      found := False;
      checkI := srcI + 1;
      while checkI < FtacList.Count do begin
        checkNode := TTACNode(FtacList.Objects[checkI]);

        found := (srcNode.left.equals(checkNode.right1))     or found;
        found := (srcNode.left.equals(checkNode.right2))     or found;
        found := (srcNode.left.equals(checkNode.leftIndex))  or found;
        found := (srcNode.left.equals(checkNode.right1Index)) or found;

        if found then break;

        inc(checkI);
      end;
    end;

    if found then
      inc(srcI)
    else begin
      FtacList.Delete(srcI);

      Result := True;
    end;
  end;
end;

function isRelop(const aOp: TAST): Boolean;
begin
  Result := False;
  if aOp = nil then Exit;

  Result := TOperator(aOp).op.tType in [ttEql, ttNeq, ttLss, ttLeq, ttGtr, ttGeq];
end;

procedure TTACList.loadStoreTransferOptimisation;
var
  i        : Integer;
  prevNode : TTACNode;
  node     : TTACNode;
begin
  if FtacList.Count = 1 then Exit;
  
  for i := 0 to FtacList.Count - 2 do begin
    prevNode := TTACNode(FtacList.Objects[i]);
    node     := TTACNode(FtacList.Objects[i + 1]);

    if node.right1 <> nil then begin
      // check to see if can transfer index from a -> x
      if (node.right1Index <> nil) and (node.right1Index.size = 1) then begin
        if not TVariable(node.right1).deref then begin
          if (TTACList.isTemp(node.right1Index)) and (prevNode.left.toString = node.right1Index.toString) then begin
            // transfer to x-register for index
            prevNode.left   := TVariable.Create(cX_REGISTER,1);

            node.right1Index := TVariable.Create(cX_REGISTER,1);
         end;
        end
        else begin
        // is deref so check if can transfer index from a -> y
          if (TTACList.isTemp(node.right1Index)) and (prevNode.left.toString = node.right1Index.toString) then begin
            // transfer to x-register for index
            prevNode.left   := TVariable.Create(cY_REGISTER,1);

            node.right1Index := TVariable.Create(cY_REGISTER,1);
         end;
        end;
      end;

    // check for store followed immediately by load same value
    // and cancel store/lode if applicable
      if (prevNode.left.toString = node.right1.toString) and (prevNode.left.size = 1) and (node.right1.size = 1) then begin
        prevNode.left := TVariable.Create(cA_REGISTER,1);

        node.right1   := TVariable.Create(cA_REGISTER,1);
      end;
    end;

    if node.left <> nil then begin
      if (node.leftIndex <> nil) and (node.leftIndex.size = 1) then begin
        if not TVariable(node.left).deref then begin
          if (TTACList.isTemp(node.leftIndex)) and (prevNode.left.toString = node.leftIndex.toString) then begin
            // transfer to x-register for index
            prevNode.left   := TVariable.Create(cX_REGISTER,1);

            node.leftIndex := TVariable.Create(cX_REGISTER,1);
          end;
        end
        else begin
        // is deref so check if can transfer index from a -> y
          if (TTACList.isTemp(node.leftIndex)) and (prevNode.left.toString = node.leftIndex.toString) then begin
            // transfer to x-register for index
            prevNode.left   := TVariable.Create(cY_REGISTER,1);

            node.leftIndex := TVariable.Create(cY_REGISTER,1);
         end;
        end;
      end;
    end;
  end;

end;

function  TTACList.optimise : Boolean;
var
  optimised : Boolean;
  node      : TTACNode;
  n1,n2     : TTACNode;
begin
  repeat
    optimised := doCopyPropagation;
    optimised := doAgebricSimplification or optimised;
    optimised := removeDeadCode          or optimised;
    optimised := doCopyPropagation       or optimised;
  until not optimised;

  loadStoreTransferOptimisation;

  Result := optimised;
end;

function  TTACList.getDestVar: TAST;
begin
  Result := TTacNode(Items[count - 1]).left;
end;

function  TTACList.getLastNode : TTACNode;
begin
  Result := TTacNode(Items[count - 1]);
end;

procedure TTACList.debugPrint;
var
  i    : Integer;
  node : TTACNode;
begin
//  if FtacList.Count = 1 then Exit;

  writeLn(Format('TTACList debugPrint (%d items):',[FtacList.Count]));
  for i := 0 to FtacList.Count - 1 do begin
    node := TTACNode(FtacList.Objects[i]);

//    writeLn(node.toString);

    if node.left is TFuncCall then
      writeLn(node.left.toString)
    else begin
      if node.left <> nil then write(node.left.toString);
      if node.leftIndex <> nil then write('[',node.leftIndex.toString,']');

      write(' := ');

      if (node.op is TUnaryOp) then begin
        write(node.op.toString,' ');
        if node.right2 <> nil then
          write(node.right2.toString);

        writeLn;
      end
      else
      if (node.op is TBinOp) then begin
        write(node.right1.toString);
        write(' ',node.op.toString,' ');
        write(node.right2.toString);

        writeLn;
      end
      else
      if node.left is TFuncCall then
        writeLn(node.left.toString)
      else
      if node.left is TPeek then
        writeLn(node.left.toString)
      else
      begin
        // value node only, no op
        write(node.right1.toString);
        if node.right1Index <> nil then write('[',node.right1Index.toString,']');

        writeLn;
      end;
    end;
  end;

  writeLn('-------------------');
end;

end.
