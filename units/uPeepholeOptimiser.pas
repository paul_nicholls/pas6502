unit uPeepholeOptimiser;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  System.Classes,usesStringWriter;

type
  TCommandsList = array of AnsiString;

  TPeepholeOptimiser = class
  protected
    Fwriter : TsesStringWriter;
    Fcode   : TStringList;
    Fvars   : TStringList;
    Falways : TStringList;

    procedure stripComments(comment : AnsiString; var aCode : AnsiString);
    function  splitMultipleCommands(separator : AnsiString; var code : AnsiString) : TCommandsList;
    procedure optimiseCode(aCode : AnsiString);      virtual; abstract;

    procedure clearVars;
    procedure writeVar(name,value : AnsiString);
    function  getVar(name : AnsiString; var value : AnsiString) : boolean;

    function  isBranchOp(op : AnsiString) : Boolean; virtual; abstract;
    function  isShiftOp(op : AnsiString) : Boolean; virtual; abstract;
    function  isStoreOp(op : AnsiString) : Boolean; virtual; abstract;
    function  mustLoad(v : AnsiString) : Boolean;
  public
    constructor create(aWriter : TsesStringWriter);
    destructor  destroy; override;

    procedure clearRegisters;                        virtual; abstract;

    procedure emitCode(aCode : AnsiString);                                overload;
    procedure emitCode(aCode : AnsiString; const aArgs: array of const);   overload;

    procedure addAlwaysLoad(v : AnsiString);
  end;

  TPeepholeOptimiser6502 = class(TPeepholeOptimiser)
  private
    // registers
    x : AnsiString;
    y : AnsiString;
    a : AnsiString;
  protected
    function  isBranchOp(op : AnsiString) : Boolean; override;
    function  isShiftOp(op : AnsiString) : Boolean; override;
    function  isStoreOp(op : AnsiString) : Boolean; override;

    function  opChangesA(op,operand : AnsiString) : Boolean;
    function  isIndirectY(operand : AnsiString) : Boolean;
    function  canLoadRegister(op,operand : AnsiString) : Boolean;
    procedure processCommand(op,operand : AnsiString);
    procedure optimiseCode(aCode : AnsiString); override;
  public
    procedure clearRegisters; override;
  end;

implementation

uses
  System.Types,System.SysUtils,System.StrUtils;

constructor TPeepholeOptimiser.create(aWriter : TsesStringWriter);
begin
  Fwriter := aWriter;

  Fcode   := TStringList.Create;
  Fvars   := TStringList.Create;
  Falways := TStringList.Create;
end;

destructor  TPeepholeOptimiser.destroy;
begin
  Falways.Free;
  Fcode.Free;
  Fvars.Free;
end;

procedure TPeepholeOptimiser.clearVars;
begin
  Fvars.Clear;
end;

procedure TPeepholeOptimiser.writeVar(name,value : AnsiString);
begin
  Fvars.Values[name] := value;
end;

function  TPeepholeOptimiser.getVar(name : AnsiString; var value : AnsiString) : boolean;
var
  i : Integer;
begin
  Result := False;

  i := Fvars.IndexOfName(name);

  if i = -1 then Exit;

  value := Fvars.Values[name];
end;

procedure TPeepholeOptimiser.stripComments(comment : AnsiString; var aCode : AnsiString);
const
  cLFset = [#13,#10];

var
  c,nl,i : Integer;
begin
  c := pos(comment,aCode);
  while c > 0 do begin
    i := c;
    while (i <= Length(aCode)) and not(aCode[i] in cLFset) do
      Inc(i);

    if i > Length(aCode) then Exit;

    if (aCode[i] in cLFset) then
      // delete comment string section
      Delete(aCode,c,i - c + 1);

    c := pos(comment,aCode);
  end;
end;

function TPeepholeOptimiser.splitMultipleCommands(separator : AnsiString; var code : AnsiString) : TCommandsList;
var
  i : Integer;
begin
  SetLength(Result,0);

  code := StringReplace(code,separator,#13#10,[rfReplaceAll]);
end;

procedure TPeepholeOptimiser.emitCode(aCode : AnsiString);
begin
  optimiseCode(aCode);
end;

procedure TPeepholeOptimiser.emitCode(aCode : AnsiString; const aArgs: array of const);
begin
  optimiseCode(Format(aCode,aArgs));
end;

procedure TPeepholeOptimiser.addAlwaysLoad(v : AnsiString);
begin
  Falways.Add(v);
end;

function  TPeepholeOptimiser.mustLoad(v : AnsiString) : Boolean;
var
  i : Integer;
begin
  i := Falways.IndexOf(v);

  Result := i <> -1;
end;

procedure TPeepholeOptimiser6502.clearRegisters;
begin
  x := '';
  y := '';
  a := '';
end;

function  TPeepholeOptimiser6502.isBranchOp(op : AnsiString) : Boolean;
begin
  Result := (op = 'jsr') or (op = 'jmp') or
            (op = 'bcc') or (op = 'bcs') or (op = 'bmi') or (op = 'bpl') or (op = 'bvs') or (op = 'bvc')
end;

function  TPeepholeOptimiser6502.isShiftOp(op : AnsiString) : Boolean;
begin
  Result := (op = 'lsr') or (op = 'asl') or
            (op = 'rol') or (op = 'ror');
end;

function  TPeepholeOptimiser6502.isStoreOp(op : AnsiString) : Boolean;
begin
  Result := (op = 'sta') or (op = 'stx') or
            (op = 'sty');
end;

function  TPeepholeOptimiser6502.opChangesA(op,operand : AnsiString) : Boolean;
const
  changeAOp : array[1..12] of AnsiString = (
    'eor',
    'ora',
    'and',
    'adc',
    'sbc',
    'sbcx',
    'sbcy',
    'adcx',
    'adcy',
    'lsr',
    'asl',
    'pla'
  );

var
  i          : Integer;
  notImplied : boolean;
begin
  Result := False;

  notImplied := (pos(',y',operand) > 0) or
                (pos(',x',operand) > 0);

  if isShiftOp(op) and notImplied then Exit;

  for i  := 1 to High(changeAop) do
    if (op = changeAop[i]) then begin
      Result := True;
      Exit;
    end;
end;

function  TPeepholeOptimiser6502.isIndirectY(operand : AnsiString) : Boolean;
begin
  operand := LowerCase(StringReplace(operand,' ','',[rfReplaceAll]));
  Result :=  Pos('),y',operand) > 0;
//  Result := RightStr(operand,2) = '),y';
end;

function  TPeepholeOptimiser6502.canLoadRegister(op,operand : AnsiString) : Boolean;
begin
  Result := True;

  if mustLoad(operand) then begin
    Exit;
  end;

  if isBranchOp(op) then begin
    clearRegisters;
    clearVars;
    Exit;
  end;

  if (op = 'iny') or (op = 'dey') then begin
    y := '';
    Exit;
  end
  else
  if (op = 'inx') or (op = 'dex') then begin
    x := '';
    Exit;
  end
  else
  if opChangesA(op,operand) then begin
    a := '';
    Exit
  end
  else
  if isStoreOp(op) then begin
    // if overwriting var that was loaded in reg, then clear reg
    // as chached value might have changed
    if      (a = operand) then a := ''
    else if (x = operand) then x := ''
    else if (y = operand) then y := '';

    // clear y for indirect store
    if isIndirectY(operand) then y := '';
  end
  else
  if (op = 'lda') then begin
    if isIndirectY(operand) then begin
    // always load indirect y
    // clear y reg too
      y := '';
      Exit;
    end;

    if a = operand then
      Result := False
    else
      a := operand;
  end
  else
  if (op = 'ldx') then begin
    if x = operand then
      Result := False
    else
      x := operand;
  end
  else
  if (op = 'ldy') then begin
    if y = operand then
      Result := False
    else
      y := operand;
  end
  else
  if (op = 'tax') then begin
    if (a <> '') then
      x := a;
  end
  else
  if (op = 'tay') then begin
    if (a <> '') then
      y := a;
  end
  else
  if (op = 'tya') then begin
    if (y <> '') then
      a := y;
  end
  else
  if (op = 'txa') then begin
    if (x <> '') then
      a := x;
  end
end;

procedure TPeepholeOptimiser6502.processCommand(op,operand : AnsiString);
var
  c : Integer;
  i : Integer;
  t : AnsiString;
  comment : AnsiString;
begin
  if (op = 'jsr') or (op = 'jmp') then
    clearRegisters;

  if op = '' then
    Fwriter.writeLn('  '+op + ' ' + operand)
  else
  if LeftStr(op,2) = '//' then
    Fwriter.writeLn('  '+op + ' ' + operand)
  else
  if (op[length(op)] = ':') then
    // is label so output with no space prefix
    Fwriter.writeLn(op + ' ' + operand)
  else
  if (op[1] = ':') or (Pos('.import',op) > 0) or (Pos('.const',op) > 0) or (Pos('.for',op) > 0)or (Pos('.label',op) > 0)or (Pos('.var',op) > 0) then
    // is macro/kick asm command so just emit it!
    Fwriter.writeLn('  '+op + ' ' + operand)
  else begin
    // is normal asm code
    comment := '';
    // get rid of spaces up until // if applicable
    c := Pos('//',operand);

    if c > 0 then begin
      comment := ' '+Copy(operand,c,Length(operand));
      operand := StringReplace(Copy(operand,1,c-1),' ','',[rfReplaceAll])
    end
    else
      operand := StringReplace(operand,' ','',[rfReplaceAll]);;

    if rightStr(operand,2) = '+0' then
    //  change '+0' -> ''
      operand := leftStr(operand,Length(operand)-2);

    if copy(operand,1,2) = '#<' then
      // change '#<' -> '#'
      operand := StringReplace(operand,'#<','#',[rfReplaceAll]);

    if canLoadRegister(op,operand) then
      Fwriter.writeLn('  '+op + ' ' + operand+comment);
  end;
end;

procedure TPeepholeOptimiser6502.optimiseCode(aCode : AnsiString);
var
  i,j,p   : Integer;
  code    : AnsiString;
  line    : AnsiString;
  lines   : TStringDynArray;
  op      : AnsiString;
  operand : AnsiString;
begin
  Fcode.Text := aCode;

  for i := 0 to Fcode.Count - 1 do begin
    line := Trim(Fcode.Strings[i]);

    stripComments('//',line);
    if Pos('.for',line) = 0 then begin
      lines := SplitString(line,';');

      for j := 0 to High(lines) do begin
        line := Trim(lines[j]);

        // separate op from operand
        p       := Pos(' ',line);
        op      := Copy(line,1,p - 1);
        operand := Copy(line,p + 1,Length(line));

        processCommand(Trim(op),Trim(operand));
      end;
    end
    else begin
      // separate op from operand
      p       := Pos(' ',line);
      op      := Copy(line,1,p - 1);
      operand := Copy(line,p + 1,Length(line));

      processCommand(Trim(op),Trim(operand));
    end;
  end;
end;

end.
