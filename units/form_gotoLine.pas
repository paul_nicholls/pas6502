unit form_gotoLine;

interface

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls;

type
  TGotoLineDialog = class(TForm)
    OKBtn: TButton;
    CancelBtn: TButton;
    Bevel1: TBevel;
    Label1: TLabel;
    lineNumber_Edit: TEdit;
    procedure lineNumber_EditKeyPress(Sender: TObject; var Key: Char);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  GotoLineDialog: TGotoLineDialog;

implementation

{$R *.dfm}

uses
  System.UITypes;

procedure TGotoLineDialog.lineNumber_EditKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Ord(Key) in [vk0..vk9,vkBack,vkReturn]) then Key := #0;
end;

procedure TGotoLineDialog.FormActivate(Sender: TObject);
begin
  lineNumber_Edit.SetFocus;
end;

end.
