unit uSymbolTable;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

{$H+}

interface

uses
  System.Classes,uToken;

type
  TSymbol = class;

  TSymbolClass = (
    scBuiltinType,
    scTypedPointer,
    scCustomType,
    scVar,
    scConst,
    scRecordType,
    scProc,
    scFunc,
    scBuiltinFunc,
    scMacro,
    scUnit
  );

  TRoutineInfo = class(TInterfacedObject)
    name      : AnsiString;
    startLine : Integer;
    endLine   : Integer;
    refCount  : Integer;
  end;

  TSourceInfo = class
  // holds offset into source file and the source filename
    symbol   : TSymbol;
    varSize  : Integer;
    offset   : Integer;
    fileName : AnsiString;
  end;

  TSourceInfoRec = record
  // holds offset into source file and the source filename
    scrollPos : Integer;
    offset    : Integer;
    fileName  : AnsiString;
  end;

  TSourceInfoArray = array of TSourceInfoRec;

  TSymbol = class(TInterfacedObject)
  protected
    FrefCount : Integer;
    FsymClass : TSymbolClass;
    FsymSize  : Integer;

  public
    symName       : AnsiString;
    symType       : TSymbol;
    memOfs        : Integer;
    isPointerType : Boolean;
    isVolatile    : Boolean;
    uses8BitIndex : Boolean;
    isKickAsmData : Boolean;
    kickAsmData   : AnsiString;
    isSigned      : Boolean;
    onStack       : Boolean;
    stackOfs      : Byte;
    isRecArray    : Boolean;
    symRecInfo    : TSymbol;
    isFwdDecl     : Boolean;

    varSize       : Integer;
    srcOffset     : Integer;
    srcFile       : String;
    isPrivate     : Boolean;

    constructor Create(aName : AnsiString; aType : TSymbol; aSize : Integer = 1);

    procedure setSourceInfo(offset : Integer; fileName : AnsiString);

    property refCount : Integer      read FrefCount write FrefCount;
    property symClass : TSymbolClass read FsymClass;
    property size     : Integer      read FsymSize;
    property name     : AnsiString   read symName;
  end;

  TBuiltInTypeSymbol = class(TSymbol)
  public
    constructor Create(aName : AnsiString; aSize : Integer; aIsSigned : Boolean); reintroduce; overload;
  end;

  TCustomTypeSymbol = class(TSymbol)
  public
    parentSym     : TSymbol;
    isPointerType : Boolean;

    constructor Create(aName : AnsiString; aParentSym : TSymbol; aSize : Integer); reintroduce; overload;
  end;

  TConstSymbol = class(TSymbol)
  public
    // len used for array constants
    isArray       : Boolean;
    len           : Integer;
    value         : TToken;
    isMsbValues   : Boolean;
    isBinaryFile  : Boolean;
    // true if array can use 8-bit index

    constructor Create(aName: AnsiString; aValue : TToken; aType : TSymbol); reintroduce; overload;
  end;

  TRecordSymbol = class(TCustomTypeSymbol)
  public
    vars : TList;

    constructor Create(aName : AnsiString); reintroduce;
    destructor  Destroy;
  end;

  TVarSymbol = class(TSymbol)
  public
    isArray      : Boolean;
    isFuncResult : Boolean;
    arraySize    : Integer;
    address      : AnsiString;
    // true if array can use 8-bit index

    Constructor Create(aName : AnsiString; aType : TSymbol);
  end;

  TUnitSymbol = class(TSymbol)
  public
    Constructor Create(aName : AnsiString);
  end;

  TMacroSymbol = class(TSymbol)
  public
    macroType   : TTokenType;
    paramCount  : Integer;
    params      : array of AnsiString;

    constructor Create(aName : AnsiString; const aParams : array of AnsiString); reintroduce; overload;
  end;

  TRoutine = class (TSymbol)
  public
    paramCount  : Integer;
    params      : array of TVarSymbol;
    onStack     : Boolean;
    stackSize   : Integer;

    constructor Create(aName : AnsiString; const aParams : array of TVarSymbol); reintroduce; overload;
  end;

  TProcSymbol = class(TRoutine)
  public
    procType    : TTokenType;
    irqType     : TTokenType;
    procAddress : AnsiString;

    constructor Create(aName : AnsiString; const aParams : array of TVarSymbol); reintroduce; overload;
  end;

  TFuncSymbol = class(TRoutine)
  public
    funcType    : TTokenType;
    funcAddress : AnsiString;

    constructor Create(aName : AnsiString; const aParams : array of TVarSymbol); reintroduce; overload;
  end;

  TscopedSymbolTable = class(TInterfacedObject)
  private
    Fsymbols        : TStringList;
    FscopeName      : AnsiString;
    FscopeLevel     : Integer;
    FenclosingScope : TscopedSymbolTable;

    procedure initBuiltIns;
  public
    function  lookup(aName : AnsiString; currentScopeOnly: Boolean = False) : TSymbol;
    function  insert(aSymbol : TSymbol; currentScopeOnly: Boolean = False) : Boolean;
    procedure replace(aSymbol : TSymbol; currentScopeOnly: Boolean = False);

    constructor Create(aScopeName : AnsiString; aScopeLevel : Integer; aEnclosingScope : TscopedSymbolTable);
    destructor  Destroy;

    procedure debugPrint;
    function  count : Integer;
    function  getSymbol(index : Integer) : TSymbol;

    function  getFullyQualifiedScopeName : AnsiString;

    property enclosingScope : TscopedSymbolTable read FenclosingScope;
    property scopeName      : AnsiString         read FscopeName;
    property scopeLevel     : Integer            read FscopeLevel;
    property fullScopeName  : AnsiString         read getFullyQualifiedScopeName;

    property symbols[index : Integer] : TSymbol  read getSymbol;
  end;

function  builtinTypeSymbol(aName : AnsiString; aSize : Integer; aIsSigned : Boolean) : TSymbol;
function  varSymbol(aName : AnsiString; aType : TSymbol) : TSymbol;
function  newRoutineInfo(startLine,endLine : Integer) : TRoutineInfo;

function isParamVar(routine,varName : AnsiString; var param : TSymbol; var stackSize : Integer; symbolTable : TscopedSymbolTable) : Boolean;

procedure routines_init;
procedure routines_cleanup;
procedure routines_addInfo(aRoutineName : AnsiString; startLine,endLine : Integer);
procedure routines_setAsUsed(aRoutineName: AnsiString; aScope : TScopedSymbolTable);
procedure routines_searchFor(aSearchValue,aAsmCode : AnsiString; aScope : TscopedSymbolTable);
procedure routines_clear;
function  routines_getCount : Integer;
procedure routines_deleteInfo(index : Integer);
function  routines_getInfo(index : Integer) : TRoutineInfo;
function  routines_getName(index : Integer) : AnsiString;
function  routines_getList : TStringList;

implementation

uses
  System.SysUtils;

var
  FroutinesList : TStringList;

function isParamVar(routine,varName : AnsiString; var param : TSymbol; var stackSize : Integer; symbolTable : TscopedSymbolTable) : Boolean;
var
  i   : Integer;
  sym : TSymbol;
begin
  Result := False;

  sym := symbolTable.lookup(routine);

  if not (sym is TRoutine) then Exit;

  for i := 0 to TRoutine(sym).paramCount - 1 do
    if TRoutine(sym).params[i].name = varName then begin
      param     := TRoutine(sym).params[i];
      stackSize := TRoutine(sym).stackSize;
      Result    := true;
      Exit;
    end;
end;

function routines_sort(List: TStringList; Index1, Index2: Integer): Integer;
var
  Value1, Value2: Integer;
begin
  Value1 := TRoutineInfo(List.Objects[Index1]).startLine;
  Value2 := TRoutineInfo(List.Objects[Index2]).startLine;
  if Value1 < Value2 then
    Result := -1
  else if Value2 < Value1 then
    Result := 1
  else
    Result := 0;
end;

procedure routines_init;
begin
  FroutinesList := TStringList.Create;
  FroutinesList.OwnsObjects := True;
end;

procedure routines_cleanup;
begin
  FreeAndNil(FroutinesList);
end;

procedure routines_clear;
begin
  FroutinesList.Clear;
end;

function  routines_getCount : Integer;
begin
  Result := FroutinesList.Count;
end;

function  routines_getInfo(index : Integer) : TRoutineInfo;
begin
  Result := TRoutineInfo(FroutinesList.Objects[index]);
end;

procedure routines_deleteInfo(index : Integer);
begin
  FroutinesList.Delete(index);
end;

function  routines_getName(index : Integer) : AnsiString;
begin
  Result := FroutinesList.Strings[index];
end;

function  routines_getList : TStringList;
begin
  Result := FroutinesList;
end;

procedure routines_addInfo(aRoutineName : AnsiString; startLine,endLine : Integer);
var
  i     : Integer;
  info  : TRoutineInfo;
begin
  if not Assigned(FroutinesList) then
    Exit;

  endLine   := endLine - 2;

  for i := 0 to FroutinesList.Count - 1 do
    if aRoutineName = FroutinesList.Strings[i] then begin
      info := TRoutineInfo(FroutinesList.Objects[i]);
      info.name      := aRoutineName;
      info.startLine := startLine;
      info.endLine   := endLine;
      Exit;
    end;

  FroutinesList.AddObject(aRoutineName,newRoutineInfo(startLine,endLine));

  FroutinesList.CustomSort(routines_sort);

//  writeLn(format('added "%s": (start %d, end: %d)',[aRoutineName,startLine+1,endLine+1]));
end;

procedure routines_setAsUsed(aRoutineName: AnsiString; aScope : TScopedSymbolTable);
var
  i     : Integer;
  info  : TRoutineInfo;
  found : Boolean;
  sym   : TSymbol;
begin
  sym := aScope.lookup(aRoutineName);

  if sym = nil then Exit;

  sym.refCount := sym.refCount + 1;

  if not Assigned(FroutinesList) then Exit;

  found := False;
  for i := 0 to FroutinesList.Count - 1 do
    if aRoutineName = FroutinesList.Strings[i] then begin
      found := True;
      info := TRoutineInfo(FroutinesList.Objects[i]);

      break;
    end;

  if not found then Exit;

  // increase refcount of this routine to mark as used
  Inc(info.refCount);

//  writeLn(format('using "%s": (start %d, end: %d)',[aRoutineName,info.startLine+1,info.endLine+1]));
end;

procedure routines_searchFor(aSearchValue,aAsmCode : AnsiString; aScope : TscopedSymbolTable);
var
  ofs  : Integer;
  i    : Integer;
  p    : Integer;
  routineName : AnsiString;
begin
  ofs := 1;
  p   := Pos(aSearchValue,aAsmCode,ofs);

  while p <> 0 do begin
    // found searchValue so get name of routine being called
    ofs := p + Length(aSearchValue);

    i := ofs;
    routineName := '';
    while (i <= Length(aAsmCode)) do begin
      if (aAsmCode[i] in ['a'..'z','A'..'Z','0'..'9','_']) then
        routineName := routineName + aAsmCode[i]
      else
        break;

      Inc(i);
    end;

    if routineName <> '' then
      // check for valid identifier with start '_' or letter
      if routineName[1] in ['a'..'z','A'..'Z','_'] then
        routines_setAsUsed(routineName,aScope);

    // find next searchValue
    p   := Pos(aSearchValue,aAsmCode,ofs);
  end;
end;

function  builtinTypeSymbol(aName : AnsiString; aSize : Integer; aIsSigned : Boolean) : TSymbol;
begin
  Result := TBuiltInTypeSymbol.Create(aName,aSize,aIsSigned);
end;

function  varSymbol(aName : AnsiString; aType : TSymbol) : TSymbol;
begin
  Result := TVarSymbol.Create(aName,aType);
end;

function  newRoutineInfo(startLine,endLine : Integer) : TRoutineInfo;
begin
  Result := TRoutineInfo.Create;
  Result.startLine := startLine;
  Result.endLine   := endLine;
  Result.refCount  := 0;
end;

constructor TSymbol.Create(aName: AnsiString; aType: TSymbol; aSize : Integer = 1);
begin
  symName  := aName;
  symType  := aType;
  memOfs   := 0;
  FsymSize := aSize;

  isSigned := False;
  isVolatile := False;
  uses8BitIndex := False;
  isKickAsmData := False;
  isRecArray    := False;

  srcOffset := -1;
  srcfile   := '';
  isPrivate := False;
  isFwdDecl := False;
end;

procedure TSymbol.setSourceInfo(offset : Integer; fileName : AnsiString);
begin
  srcOffset := offset;
  srcfile   := fileName;
end;

constructor TBuiltInTypeSymbol.Create(aName : AnsiString; aSize : Integer; aIsSigned : Boolean);
begin
  inherited Create(aName,nil,aSize);
  isSigned := aIsSigned;
  FsymClass := scBuiltInType;
end;

constructor TCustomTypeSymbol.Create(aName : AnsiString; aParentSym : TSymbol; aSize : Integer);
begin
  inherited Create(aName,nil,aSize);

  isPointerType := False;
  parentSym     := aParentSym;

  FsymClass     := scCustomType;
end;

constructor TVarSymbol.Create(aName : AnsiString; aType : TSymbol);
begin
  inherited Create(aName,aType);

  isFuncResult := False;

  address      := '';

  FsymClass := scVar;
  FsymSize  := aType.size;
end;

Constructor TUnitSymbol.Create(aName : AnsiString);
begin
  inherited Create(aName,nil);

  FsymClass := scUnit;
end;

constructor TRoutine.Create(aName : AnsiString; const aParams : array of TVarSymbol);
var
  i : Integer;
begin
  inherited Create(aName,nil,0);

  onStack     := False;

  SetLength(params,Length(aParams));
  for i := 0 to High(aParams) do begin
    params[i] := aParams[i];
  end;
end;

constructor TProcSymbol.Create(aName : AnsiString; const aParams : array of TVarSymbol);
begin
  inherited Create(aName,aParams);

  procAddress := '';
  FsymClass   := scProc;
end;

constructor TMacroSymbol.Create(aName: AnsiString; const aParams : array of AnsiString);
var
  i : Integer;
begin
  inherited Create(aName,nil,0);

  setLength(params,Length(aParams));

  for i  := 0 to High(aParams) do
    params[i] := aParams[i];  

  FsymClass := scMacro;
end;

constructor TFuncSymbol.Create(aName : AnsiString; const aParams : array of TVarSymbol);
var
  i : Integer;
begin
  inherited Create(aName,aParams);

  funcAddress := '';
  FsymClass   := scFunc;
end;

constructor TConstSymbol.Create(aName: AnsiString; aValue : TToken; aType: TSymbol);
begin
  if (aType <> nil) and (aType.symType <> nil) then
    inherited Create(aName,aType,aType.symType.size)
  else
    inherited Create(aName,aType,0);

  len   := size;
  value := aValue;

  FsymClass := scConst;
  isBinaryFile := False;
end;

constructor TRecordSymbol.Create(aName: AnsiString);
begin
  inherited Create(aName,nil,0);

  vars := TList.Create;

  FsymClass := scRecordType;
end;

destructor  TRecordSymbol.Destroy;
var
  i : Integer;
begin
  for i := 0 to vars.Count - 1 do
    TSymbol(vars.Items[i]).Free;

  FreeAndNil(vars);
end;

constructor TscopedSymbolTable.Create(aScopeName : AnsiString; aScopeLevel : Integer; aEnclosingScope : TscopedSymbolTable);
begin
  Fsymbols        := TStringList.Create;
  FscopeName      := aScopeName;
  FscopeLevel     := aScopeLevel;
  FenclosingScope := aEnclosingScope;

  initBuiltIns;
end;

destructor  TscopedSymbolTable.Destroy;
var
  i : Integer;
begin
  for i := 0 to Fsymbols.Count - 1 do
    TSymbol(Fsymbols.Objects[i]).Free;

  Fsymbols.Free;
end;

function  TscopedSymbolTable.getFullyQualifiedScopeName : AnsiString;
begin
  Result :=  scopeName;

  if (enclosingScope <> nil) then begin
    Result := enclosingScope.getFullyQualifiedScopeName + '.' + Result;
  end;
end;

procedure TscopedSymbolTable.initBuiltIns;
begin
  insert(builtInTypeSymbol('byte'    ,1,false));
  insert(builtInTypeSymbol('shortint' ,1,true));
  insert(builtInTypeSymbol('integer' ,2,true));
  insert(builtInTypeSymbol('string'  ,255,false));
  insert(builtInTypeSymbol('ascii'   ,255,false));
  insert(builtInTypeSymbol('petscii' ,255,false));
  insert(builtInTypeSymbol('pointer' ,2,false));
  insert(builtInTypeSymbol('word'    ,2,false));
  insert(builtInTypeSymbol('dword'   ,4,false));
  insert(builtInTypeSymbol('int24'   ,3,false));
  insert(builtInTypeSymbol('boolean' ,1,false));
end;

function  TscopedSymbolTable.lookup(aName: AnsiString; currentScopeOnly: Boolean = False) : TSymbol;
var
  i : Integer;
  s : TSymbol;
begin
  Result := nil;

  for i := 0 to Fsymbols.Count - 1 do begin
    s := TSymbol(Fsymbols.Objects[i]);

    if (s.symClass in [scBuiltinType,scBuiltinFunc]) then begin
      if (LowerCase(aName) = LowerCase(s.symName)) then begin
        Result := s;
        Exit;
      end;
    end else begin
      if (aName = s.symName) then begin
        Result := s;
        Exit;
      end;
    end;
  end;

  if currentScopeOnly then Exit;

  if (FenclosingScope <> nil) then
    Result := FenclosingScope.lookup(aName);
end;

function  TscopedSymbolTable.insert(aSymbol : TSymbol; currentScopeOnly: Boolean = False) : Boolean;
var
  sym : TSymbol;
begin
  Result := False;

  sym := lookup(aSymbol.name,currentScopeOnly);

  if (sym <> nil) then Exit;

  Fsymbols.AddObject(aSymbol.name,aSymbol);

  Result := True;
end;

procedure TscopedSymbolTable.replace(aSymbol : TSymbol; currentScopeOnly: Boolean = False);
var
  sym : TSymbol;
  i   : Integer;
begin
  sym := lookup(aSymbol.name,currentScopeOnly);

  if (sym = nil) then
    Fsymbols.AddObject(aSymbol.name,aSymbol)
  else begin
    // replace existing one
    for i := 0 to Fsymbols.Count - 1 do begin
      if sym = Fsymbols.Objects[i] then begin
        Fsymbols.Objects[i].Free;
        Fsymbols.Strings[i] := aSymbol.name;
        Fsymbols.Objects[i] := aSymbol;
        Exit;
      end;
    end;

    if currentScopeOnly then Exit;

    if (FenclosingScope <> nil) then
      FenclosingScope.replace(aSymbol,currentScopeOnly);
  end;
end;

procedure TscopedSymbolTable.debugPrint;
var
  i : Integer;
begin
  writeLn(scopeName);
  for i := 0 to Fsymbols.Count - 1 do begin
    WriteLn(TSymbol(Fsymbols.Objects[i]).name,':',Fsymbols.Objects[i].ClassName);
  end;
  writeLn('-----------------------------------------------------------');
end;

function  TscopedSymbolTable.count : Integer;
begin
  Result := Fsymbols.Count;
end;

function  TscopedSymbolTable.getSymbol(index : Integer) : TSymbol;
begin
  Result := TSymbol(Fsymbols.Objects[index]);
end;

end.
