object codeInsightOptions: TcodeInsightOptions
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Code Insight Options'
  ClientHeight = 114
  ClientWidth = 227
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  FormStyle = fsStayOnTop
  Position = poMainFormCenter
  TextHeight = 15
  object Label1: TLabel
    Left = 16
    Top = 12
    Width = 157
    Height = 15
    Caption = 'Code insight timer delay (mS)'
  end
  object OKBtn: TButton
    Left = 28
    Top = 76
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 132
    Top = 76
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object codeInsightTimer_MaskEdit: TMaskEdit
    Left = 16
    Top = 33
    Width = 120
    Height = 23
    EditMask = '!9999;1;_'
    MaxLength = 4
    TabOrder = 2
    Text = '    '
  end
end
