object findReplaceDialog: TfindReplaceDialog
  Left = 227
  Top = 108
  BorderStyle = bsToolWindow
  Caption = 'Dialog'
  ClientHeight = 82
  ClientWidth = 484
  Color = clBtnFace
  ParentFont = True
  KeyPreview = True
  Position = poDefault
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyUp = FormKeyUp
  OnShow = FormShow
  TextHeight = 15
  object results_Label: TLabel
    Left = 310
    Top = 4
    Width = 87
    Height = 15
    Margins.Left = 6
    Margins.Top = 6
    Margins.Right = 6
    Margins.Bottom = 6
    AutoSize = False
    Caption = 'No results'
  end
  object frMode_Button: TButton
    Left = 0
    Top = 3
    Width = 38
    Height = 21
    Caption = '>'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = frMode_ButtonClick
  end
  object replace_Edit: TEdit
    Left = 44
    Top = 33
    Width = 197
    Height = 23
    Hint = 'Replace'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    Visible = False
  end
  object ToolBar1: TToolBar
    Left = 247
    Top = 4
    Width = 60
    Height = 23
    Align = alNone
    ButtonHeight = 23
    ButtonWidth = 26
    Caption = 'ToolBar1'
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = True
    TabOrder = 3
    StyleElements = []
    object matchCase_ToolButton: TToolButton
      Left = 0
      Top = 0
      Hint = 'Match Case'
      AutoSize = True
      Caption = 'Aa'
      Style = tbsCheck
      OnClick = matchCase_ToolButtonClick
    end
    object matchWholeWord_ToolButton: TToolButton
      Left = 25
      Top = 0
      Hint = 'Match Whole Word'
      AutoSize = True
      Caption = '|ab|'
      Style = tbsCheck
      OnClick = matchWholeWord_ToolButtonClick
    end
  end
  object previousMatch_Button: TButton
    Left = 406
    Top = 2
    Width = 38
    Height = 21
    Hint = 'Previous Match'
    Caption = '<'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    OnClick = previousMatch_ButtonClick
  end
  object nextMatch_Button: TButton
    Left = 450
    Top = 2
    Width = 38
    Height = 21
    Hint = 'Next Match'
    Caption = '>'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    OnClick = nextMatch_ButtonClick
  end
  object replace_Button: TButton
    Left = 258
    Top = 31
    Width = 80
    Height = 23
    Margins.Left = 6
    Margins.Top = 6
    Margins.Right = 6
    Margins.Bottom = 6
    Caption = 'Replace'
    TabOrder = 6
    OnClick = replace_ButtonClick
  end
  object replaceAll_Button: TButton
    Left = 340
    Top = 31
    Width = 80
    Height = 23
    Margins.Left = 6
    Margins.Top = 6
    Margins.Right = 6
    Margins.Bottom = 6
    Caption = 'Replace All'
    TabOrder = 7
    OnClick = replaceAll_ButtonClick
  end
  object find_Edit: TComboBox
    Left = 44
    Top = 4
    Width = 197
    Height = 23
    TabOrder = 1
    OnChange = find_EditChange
    OnKeyPress = find_EditKeyPress
  end
  object findTextDelay_Timer: TTimer
    Enabled = False
    OnTimer = findTextDelay_TimerTimer
    Left = 90
    Top = 8
  end
end
