object help_Form: Thelp_Form
  Left = 0
  Top = 0
  Caption = 'PAS 6502 Help File'
  ClientHeight = 529
  ClientWidth = 653
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OnCreate = FormCreate
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 479
    Width = 653
    Height = 50
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 512
    ExplicitWidth = 667
    object close_Button: TButton
      Left = 6
      Top = 12
      Width = 75
      Height = 25
      Caption = 'Close'
      Default = True
      ModalResult = 1
      TabOrder = 0
      OnClick = close_ButtonClick
    end
  end
  object help_RichEdit: TRichEdit
    Left = 0
    Top = 0
    Width = 653
    Height = 479
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      'RichEdit1')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
end
