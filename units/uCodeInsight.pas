unit uCodeInsight;
//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------
interface

uses
  System.Classes,System.Types,uSymbolTable;

type
  TCodeInsightData = class
  private
  protected
    FName      : AnsiString;
    FExtraData : TObject;
    FData      : TStringList;
    FLevel     : Integer;
    FParent    : TCodeInsightData;
    Foffset    : Integer;
    FfileName  : String;
    Fsymbol    : TSymbol;
  public
    constructor Create(AName : AnsiString; AParent : TCodeInsightData = Nil);
    destructor  Destroy; override;
    function  AddDataByName(AName : AnsiString; AExtraData : TObject) : TCodeInsightData;
    function  GetDataByName(AName : AnsiString) : TCodeInsightData;
    function  GetDataAsArray: TStringDynArray;

    procedure setOffsetAndFile(sym : TSymbol; offset : Integer; fileName : String);

    function  getCount : Integer;

    function  getData(index : Integer) : TCodeInsightData;

    procedure clear;
    property  Name      : AnsiString read FName;
    property  extraData : TObject read FextraData;

    property  offset    : Integer read Foffset;
    property  fileName  : String  read FfileName;

    property  symbol    : TSymbol read Fsymbol;

    property  parent    : TCodeInsightData read Fparent;
  end;

implementation

constructor TCodeInsightData.Create(AName : AnsiString; AParent : TCodeInsightData = Nil);
begin
  FData        := TStringList.Create;
  FData.Sorted := False;
  FName        := AName;
  FParent      := AParent;
  FLevel       := 0;

  Foffset      := -1;
  ffileName    := '';

  if AParent <> Nil then FLevel := AParent.FLevel + 1;
end;

destructor TCodeInsightData.destroy;
begin
  clear;
  FData.Free;
  FextraData := nil;
end;

procedure TCodeInsightData.clear;
var
  i : Integer;
begin
  for i := FData.Count - 1 downto 0 do begin
    TCodeInsightData(FData.Objects[i]).clear;
    TCodeInsightData(FData.Objects[i]).Free;
    FData.Delete(i);
  end;
end;

function  TCodeInsightData.AddDataByName(AName : AnsiString; AExtraData : TObject) : TCodeInsightData;
var
  i : Integer;
begin
  i := FData.IndexOf(AName);
  if i <> -1 then begin
    Result := TCodeInsightData(FData.Objects[i]);
    Result.FExtraData := AExtraData;
    Exit;
  end;
  Result := TCodeInsightData.Create(AName,Self);
  Result.FExtraData := AExtraData;
  FData.AddObject(AName,Result);
end;

function  TCodeInsightData.GetDataByName(AName : AnsiString) : TCodeInsightData;
var
  i : Integer;
begin
  Result := Nil;
  if (FName = AName) then
    Result := Self
  else begin
    i := FData.IndexOf(AName);
    if i = -1 then begin
      for i := 0 to FData.Count - 1 do begin
        Result := TCodeInsightData(FData.Objects[i]).GetDataByName(AName);
        if (Result <> Nil) then Exit;
      end;
    end
    else
      Result := TCodeInsightData(FData.Objects[i]);
  end;
end;

function  TCodeInsightData.GetDataAsArray: TStringDynArray;
var
  i : Integer;
begin
  SetLength(Result,0);
  for i := 0 to FData.Count - 1 do begin
    SetLength(Result,Length(Result) + 1);
    Result[High(Result)] := FData.Strings[i];
  end;
end;

function  TCodeInsightData.getCount : Integer;
begin
  Result := FData.Count;
end;

function  TCodeInsightData.getData(index : Integer) : TCodeInsightData;
begin
  Result := TCodeInsightData(FData.Objects[index]);
end;


procedure TCodeInsightData.setOffsetAndFile(sym : TSymbol; offset : Integer; fileName : String);
begin
  Foffset   := offset;
  FfileName := fileName;
  Fsymbol   := sym;
end;

end.
