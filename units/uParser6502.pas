unit uParser6502;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

{$H+}

interface

uses
  uLexer,uToken,uAST;

type
  TParser6502 = class
  private
    Flexer        : TLexer;
    FcurrentToken : TToken;

    procedure visitUnaryOp(aNode : TUnaryOp);
    procedure visitBinOp(aNode : TBinOp);
    procedure visitVariable(aNode : TVariable);
    procedure visitNumber(aNode : TNumber);

    procedure visitAssign(aNode : TAssign);
    procedure visitCompound(aNode : TCompound);

    procedure visit(aNode : TAST);

    function  simplifyUnaryOp(aNode : TUnaryOp) : TAST;
    function  simplifyBinOp(aNode : TBinOp) : TAST;
    function  simplify(aNode : TAST) : TAST;

    procedure error(msg : AnsiString);

    function  accept(aTokenType : TTokenType) : Boolean;
    procedure match(aTokenType : TTokenType);

    function  factor              : TAST;
    function  term                : TAST;
    function  expr                : TAST;

    function  empty               : TAST;

    function  variable            : TAST;
    function  assignmentStatement : TAST;
    function  statement           : TAST;
    function  compoundStatement   : TCompound;

    function  varDeclarations : TASTList;
    function  declarations : TASTList;
    function  block : TBlock;
    function  doProgram : TAST;
  public
    constructor create(aLexer : TLexer);

    function  parse : Boolean;
  end;

implementation

uses
  System.Classes,System.SysUtils;

//----------------------------------------------------
//   TParser6502
//----------------------------------------------------
constructor TParser6502.create(aLexer : TLexer);
begin
  Flexer        := aLexer;
  FcurrentToken := Flexer.getNextToken;
end;

procedure TParser6502.error(msg : AnsiString);
begin
  raise Exception.Create(msg +  Format('at (col: %d, row: %d)',[Flexer.tokenCol,Flexer.tokenRow]));
end;

function  TParser6502.accept(aTokenType : TTokenType) : Boolean;
begin
  Result := False;

  if (FcurrentToken.tType = aTokenType) then begin
    FcurrentToken := Flexer.getNextToken;
    Result := True;
  end
end;

procedure TParser6502.match(aTokenType : TTokenType);
begin
  if (FcurrentToken.tType = aTokenType) then
    FcurrentToken := Flexer.getNextToken
  else
    error('Match: unexpected token "'+FcurrentToken.tValue+'"');
end;

//----------------------------------------------------
//   AST node visitor
//----------------------------------------------------
procedure TParser6502.visitUnaryOp(aNode : TUnaryOp);
begin
  visit(aNode.expr);

  Write(aNode.op.tValue,' ');
end;

procedure TParser6502.visitBinOp(aNode : TBinOp);
begin
  visit(aNode.left);
  visit(aNode.right);

  Write(aNode.op.tValue,' ');
end;

procedure TParser6502.visitVariable(aNode : TVariable);
begin
  Write(aNode.value,' ');
end;

procedure TParser6502.visitAssign(aNode : TAssign);
begin
  Write(aNode.variable.token.tValue,' := ');
  visit(aNode.expr);
  WriteLn;
end;

procedure TParser6502.visitCompound(aNode : TCompound);
var
  i : Integer;
begin
  for i := 0 to aNode.children.Count - 1 do begin
    visit(TAST(aNode.children.Items[i]));
  end;
end;

procedure TParser6502.visitNumber(aNode : TNumber);
begin
  Write(aNode.value,' ');
end;

procedure TParser6502.visit(aNode : TAST);
begin
  if (aNode is TUnaryOp) then
    visitUnaryOp(TUnaryOp(aNode))
  else if (aNode is TBinOp) then
    visitBinOp(TBinOp(aNode))
  else if (aNode is TNumber) then
    visitNumber(TNumber(aNode))
  else if (aNode is TVariable) then
    visitVariable(TVariable(aNode))
  else if (aNode is TAssign) then
    visitAssign(TAssign(aNode))
  else if (aNode is TCompound) then
    visitCompound(TCompound(aNode))
  else if (aNode is TEmpty) then begin

  end
  else
    raise Exception.Create('Visit: Unknown AST Node "'+aNode.ClassName+'"');
end;

function  TParser6502.simplifyUnaryOp(aNode : TUnaryOp) : TAST;
var
  expr : TAST;
begin
  Result := aNode;

  expr := simplify(aNode.expr);

  if (expr.isInteger) then begin
    Result := expr;

    if (aNode.op.tType = ttMinus) then
      TNumber(Result).value := - TNumber(Result).value;
  end;
end;

function  TParser6502.simplifyBinOp(aNode : TBinOp) : TAST;
var
  nl,nr : TAST;
begin
  Result := aNode;

  nl := simplify(aNode.left);
  nr := simplify(aNode.right);

  if (nl.isInteger and nr.isInteger) then begin
    case aNode.op.tType of
      TTokenType.ttPlus  : Result := TNumber.Create(TNumber(nl).value +   TNumber(nr).value);
      TTokenType.ttMinus : Result := TNumber.Create(TNumber(nl).value -   TNumber(nr).value);
      TTokenType.ttTimes   : Result := TNumber.Create(TNumber(nl).value *   TNumber(nr).value);
      TTokenType.ttDiv   : Result := TNumber.Create(TNumber(nl).value div TNumber(nr).value);
    end;
  end;
end;

function  TParser6502.simplify(aNode : TAST) : TAST;
begin
  Result := aNode;

  if (aNode is TUnaryOp) then
    Result := simplifyUnaryOp(TUnaryOp(aNode))
  else
  if (aNode is TBinOp) then
    Result := simplifyBinOp(TBinOp(aNode));
end;

function  TParser6502.variable : TAST;
begin
  Result := TVariable.Create(FcurrentToken);

  match(ttIdent);
end;

function  TParser6502.factor : TAST;
var
  token : TToken;
begin
  token := FcurrentToken;

  if (token.tType in [ttPlus,ttMinus]) then begin
    match(token.tType);
    Result := TUnaryOp.Create(token,factor);
  end
  else
  if (token.tType = ttLParen) then begin
    match(ttLParen);
    Result := expr;
    match(ttRParen);
  end
  else
  if (token.tType = ttIntNum) then begin
    Result := TNumber.Create(token);
    match(token.tType);
  end
  else
  if (token.tType = ttIdent) then begin
    Result := TVariable.Create(token);
    match(token.tType);
  end
  else
    error('Unknown factor: "'+token.tValue+'"');
end;

function  TParser6502.term : TAST;
var
  token : TToken;
  node  : TAST;
begin
  node := factor;

  while (FcurrentToken.tType in [ttTimes,ttDiv,ttSlash]) do begin
    token := FcurrentToken;
    match(token.tType);

    node := TBinOp.Create(node,token,factor);
  end;

  Result := node;
end;

function  TParser6502.expr : TAST;
var
  token : TToken;
  node  : TAST;
begin
  node := term;

  while (FcurrentToken.tType in [ttPlus,ttMinus]) do begin
    token := FcurrentToken;
    match(token.tType);

    node := TBinOp.Create(node,token,term);
  end;

  Result := node;
end;

function  TParser6502.empty : TAST;
begin
  Result := TEmpty.Create;
end;

function  TParser6502.assignmentStatement : TAST;
var
  v : TAST;
  e : TAST;
begin
  v := variable;

  match(ttBecomes);

  e := expr;

  Result := TAssign.Create(v,e);
end;

function  TParser6502.compoundStatement : TCompound;
var
  node : TCompound;
begin
  node := TCompound.Create;

  match(ttBegin);

  if (FcurrentToken.tType <> ttEnd) then
    node.children.add(statement);

  while accept(ttSemiColon) do begin
    if (FcurrentToken.tType <> ttEnd) then
      node.children.add(statement);
  end;

  match(ttEnd);

  Result := node;
end;

function  TParser6502.statement : TAST;
begin
  case FcurrentToken.tType of
    ttBegin : Result := compoundStatement;
    ttIdent : Result := assignmentStatement;
  else
    Result := empty;
  end;
end;

function  TParser6502.varDeclarations : TASTList;
var
  variables : TList;
  varDecls  : TASTList;

  i        : Integer;
begin
  variables := TASTList.Create;

  // add variables to list
  variables.Add(variable);

  while accept(ttComma) do begin
    variables.Add(variable);
  end;

  match(ttColon);

  if not (FcurrentToken.tType in [ttInteger,ttReal]) then
    error('Unknown var type: "'+FcurrentToken.tValue+'"');

  // fill in var nodes type
  varDecls := TASTList.Create;

//  for i := 0 to variables.Count - 1 do
//    varDecls.Add(TVarDecl.Create(TVariable(variables.Items[i]),FcurrentToken));

  match(FcurrentToken.tType);

  match(ttSemiColon);

  Result := varDecls;
end;

function  TParser6502.declarations : TASTList;
begin
  if accept(ttVar) then begin
    repeat
      varDeclarations;
    until  (FcurrentToken.tType <> ttIdent);
  end;
end;

function  TParser6502.block : TBlock;
var
  node : TBlock;
begin
  node := TBlock.Create;

  node.declarations      := declarations;
  node.compoundStatement := compoundStatement;

  Result := node;
end;

function  TParser6502.doProgram : TAST;
var
  prog     : TProgram;
  progName : AnsiString;
begin
  match(ttProgram);

  progName := FcurrentToken.tValue;

  match(ttIdent);

  match(ttSemiColon);

  prog := TProgram.Create(progName,block);

  match(ttPeriod);

  Result := prog;
end;

function  TParser6502.parse : Boolean;
var
  v : TAST;
begin
  try
    v := doProgram;

    visit(v);
  except
    on E:Exception do
      WriteLn(E.Message);
  end;
end;

end.
