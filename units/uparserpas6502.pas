unit uParserPas6502;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

{$H+}

interface

uses
  Classes, SysUtils,uParser,uExpressions;

const
  cASCIIToPETSCIITable: array[0..256 - 1] of Byte = (
    $00,$01,$02,$03,$04,$05,$06,$07,$14,$20,$0d,$11,$93,$0a,$0e,$0f,
    $10,$0b,$12,$13,$08,$15,$16,$17,$18,$19,$1a,$1b,$1c,$1d,$1e,$1f,
    $20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$2a,$2b,$2c,$2d,$2e,$2f,
    $30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$3a,$3b,$3c,$3d,$3e,$3f,
    $40,$c1,$c2,$c3,$c4,$c5,$c6,$c7,$c8,$c9,$ca,$cb,$cc,$cd,$ce,$cf,
    $d0,$d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9,$da,$5b,$5c,$5d,$5e,$5f,
    $c0,$41,$42,$43,$44,$45,$46,$47,$48,$49,$4a,$4b,$4c,$4d,$4e,$4f,
    $50,$51,$52,$53,$54,$55,$56,$57,$58,$59,$5a,$db,$dc,$dd,$de,$df,
    $80,$81,$82,$83,$84,$85,$86,$87,$88,$89,$8a,$8b,$8c,$8d,$8e,$8f,
    $90,$91,$92,$0c,$94,$95,$96,$97,$98,$99,$9a,$9b,$9c,$9d,$9e,$9f,
    $a0,$a1,$a2,$a3,$a4,$a5,$a6,$a7,$a8,$a9,$aa,$ab,$ac,$ad,$ae,$af,
    $b0,$b1,$b2,$b3,$b4,$b5,$b6,$b7,$b8,$b9,$ba,$bb,$bc,$bd,$be,$bf,
    $60,$61,$62,$63,$64,$65,$66,$67,$68,$69,$6a,$6b,$6c,$6d,$6e,$6f,
    $70,$71,$72,$73,$74,$75,$76,$77,$78,$79,$7a,$7b,$7c,$7d,$7e,$7f,
    $e0,$e1,$e2,$e3,$e4,$e5,$e6,$e7,$e8,$e9,$ea,$eb,$ec,$ed,$ee,$ef,
    $f0,$f1,$f2,$f3,$f4,$f5,$f6,$f7,$f8,$f9,$fa,$fb,$fc,$fd,$fe,$ff
  );

  cPETSCIIToCBMScreenCode: array [0..256 - 1] of Byte = (
    $80,$81,$82,$83,$84,$85,$86,$87,$88,$89,$8a,$8b,$8c,$8d,$8e,$8f,
    $90,$91,$92,$93,$94,$95,$96,$97,$98,$99,$9a,$9b,$9c,$9d,$9e,$9f,
    $20,$21,$22,$23,$24,$25,$26,$27,$28,$29,$2a,$2b,$2c,$2d,$2e,$2f,
    $30,$31,$32,$33,$34,$35,$36,$37,$38,$39,$3a,$3b,$3c,$3d,$3e,$3f,
    $00,$01,$02,$03,$04,$05,$06,$07,$08,$09,$0a,$0b,$0c,$0d,$0e,$0f,
    $10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$1a,$1b,$1c,$1d,$1e,$1f,
    $40,$41,$42,$43,$44,$45,$46,$47,$48,$49,$4a,$4b,$4c,$4d,$4e,$4f,
    $50,$51,$52,$53,$54,$55,$56,$57,$58,$59,$5a,$5b,$5c,$5d,$5e,$5f,
    $c0,$c1,$c2,$c3,$c4,$c5,$c6,$c7,$c8,$c9,$ca,$cb,$cc,$cd,$ce,$cf,
    $d0,$d1,$d2,$d3,$d4,$d5,$d6,$d7,$d8,$d9,$da,$db,$dc,$dd,$de,$df,
    $60,$61,$62,$63,$64,$65,$66,$67,$68,$69,$6a,$6b,$6c,$6d,$6e,$6f,
    $70,$71,$72,$73,$74,$75,$76,$77,$78,$79,$7a,$7b,$7c,$7d,$7e,$7f,
    $40,$41,$42,$43,$44,$45,$46,$47,$48,$49,$4a,$4b,$4c,$4d,$4e,$4f,
    $50,$51,$52,$53,$54,$55,$56,$57,$58,$59,$5a,$5b,$5c,$5d,$5e,$5f,
    $60,$61,$62,$63,$64,$65,$66,$67,$68,$69,$6a,$6b,$6c,$6d,$6e,$6f,
    $70,$71,$72,$73,$74,$75,$76,$77,$78,$79,$7a,$7b,$7c,$7d,$7e,$5e
  );

type
  TRegType         = (eFloatRegister,eIntRegister);
  TAnsiStringArray = array of AnsiString;

  TDeclarationLocation = (
    dlProgram,
    dlInterface,
    dlImplementation,
    dlProcFunc
  );

  TParserPas6502 = class(TBaseParser)
  private
    FFloatRegisterIndex : Integer;
    FIntRegisterIndex   : Integer;

    FIntTypeStack       : array[0..255] of TIntNumType;
    FIntTypeStackIndex  : Integer;
    FSourceCodeLines    : TStrings;
    FDeferredBinFiles   : TStringList;

    FScope              : AnsiString;
    FIsCartridge        : Boolean;

    procedure SetScope(aScope : AnsiString);

    procedure pushIntType(intType: TIntNumType);
    function  popIntType: TIntNumType;

    function  writeToRegister: Integer;
    function  readFromRegister: Integer;

    procedure loadIntegerRegMemArray(variable : String; deref : Boolean; symSize : Integer; symSubType : TSymSubType);
    procedure loadIntegerRegMem(variable : String);
    procedure loadIntegerRegMem8(variable : String);
    procedure loadIntegerRegMemAddr8(address : String);

    procedure loadIntRegMemIndirect8;

    procedure loadIntegerRegImm(num : String);

    function  intNumTypeToSize(i : TIntNumType) : String;

    procedure storeIntegerRegMem(variable : String; deref : Boolean; symSize : Integer; symSubType : TSymSubType);
    procedure storeIntegerRegMemArray(variable : String; deref : Boolean; symSize : Integer; symSubType : TSymSubType);

    procedure set16im(value : Integer; variable : AnsiString; deref : Boolean; symSize : Integer; symSubType : TSymSubType);
    procedure copy16(srcVar,dstVar : AnsiString; deref : Boolean; symSize : Integer; symSubType : TSymSubType);

    procedure emitAddImm;
    procedure emitSubImm;

    procedure emitMulImm;
    procedure emitDivImm;
    procedure emitShl;
    procedure emitShr;

    procedure emitJmpIntRegFalse(falseLabel : AnsiString);
    procedure emitRelOp(AOp : TExpNodeType);
    procedure emitLogicOp(AOp : TExpNodeType);

    function  doAddOp(ConstantsOnly: Boolean; ALeftNode: TExpTreeNode; AToken: Integer; AOp: TExpNodeType): TExpTreeNode;
    function  doMulOp(ConstantsOnly: Boolean; ALeftNode: TExpTreeNode; AToken: Integer; AOp: TExpNodeType): TExpTreeNode;
    function  doRelOp(ConstantsOnly: Boolean; ALeftNode: TExpTreeNode; AToken: Integer; AOp: TExpNodeType): TExpTreeNode;

    function  factor(ConstantsOnly : Boolean)           : TExpTreeNode;
    function  signedFactor(ConstantsOnly : Boolean)     : TExpTreeNode;
    function  term(ConstantsOnly : Boolean)             : TExpTreeNode;
    function  simpleExpression(ConstantsOnly : Boolean) : TExpTreeNode;
    function  expression(ConstantsOnly : Boolean)       : TExpTreeNode;

    procedure LoadBinary;
    procedure doWriteLn;
    procedure clearScreen;
    procedure copyData8;

    procedure doAsmBlock;

    procedure StringToSymSubType(aVarType : AnsiString; aSym : TSymInfo);

    procedure RegisterVar(aVarName: AnsiString; aVarType: AnsiString; aAddress: AnsiString);

    procedure Poke;
    procedure Poke16;
    procedure Condition;
    procedure doIf;
    procedure doWhile;
    procedure doProcFuncCall(procName : AnsiString);
    procedure setupInterrupt;
    procedure setupRasterIRQ;
    procedure irqEnd;
    procedure Assignment(variable : String);
    procedure doDec;
    procedure doInc;
    procedure Statement;
    procedure InitializedConstDecls(sym : TSymInfo; DeclarationLocation : TDeclarationLocation);
    procedure TypeDecls(DeclarationLocation : TDeclarationLocation);
    procedure ConstDecls(DeclarationLocation : TDeclarationLocation);
    procedure GetVarDecls(var vars : TAnsiStringArray; var varType,address : AnsiString; var varLen : Integer; var isArray : Boolean);
    procedure VarDecls(DeclarationLocation : TDeclarationLocation);
    procedure FuncDecls(DeclarationLocation : TDeclarationLocation);
    procedure ProcDecls(DeclarationLocation : TDeclarationLocation);
    procedure Block(DeclarationLocation : TDeclarationLocation; isUnitBlock : Boolean; blockName : AnsiString = '');
    procedure UsesClause(DeclarationLocation : TDeclarationLocation);
    procedure BlockDecls(DeclarationLocation : TDeclarationLocation);
    procedure doUnit;
    procedure EmitBinFile(aFileName : AnsiString; aAddress : Integer);
    procedure doProgram;

    procedure NodeVisitor(ANode : TExpTreeNode);

  protected
    procedure RegisterKeywordTokens; override;
    procedure ParseInput; override;
    procedure ParseUnit(UnitName : String);
  public
    constructor Create(ASourceCodeLines : TStrings; AIsCartridge : Boolean); reintroduce; overload;
  end;

implementation

{uses
  System.Math;}

const
  cConsoleType_c64 = 'c64';

var
  Token_poke           : Integer;
  Token_poke16         : Integer;
  Token_interrupt      : Integer;
  Token_rasterirq      : Integer;
  Token_setupinterrupt : Integer;
  Token_setuprasterirq : Integer;
  Token_irqend         : Integer;
  Token_uses           : Integer;
  Token_unit           : Integer;
  Token_interface      : Integer;
  Token_implementation : Integer;
  Token_loadbinary     : Integer;
  Token_writeln        : Integer;
  Token_clearscreen    : Integer;
  Token_copydata8      : Integer;
  Token_readjoysticks  : Integer;
  Token_exit           : Integer;

constructor TParserPas6502.Create(ASourceCodeLines : TStrings; AIsCartridge : Boolean);
begin
  inherited Create;

  FIsCartridge      := aIsCartridge;

  FSourceCodeLines  := ASourceCodeLines;
  FDeferredBinFiles := TStringList.Create;
  FDeferredBinFiles.Sorted := False;
end;

procedure TParserPas6502.NodeVisitor(ANode : TExpTreeNode);
// convert expression to ASM
var
  varName : AnsiString;
  sym     : TSymInfo;
begin
  if ANode.Data.NodeType = ntInt then
    loadIntegerRegImm(ANode.Data.ToString)
  else
  if ANode.Data.NodeType = ntBool then
    loadIntegerRegImm(IntToStr(ANode.Data.IntValue))
  else
  if ANode.Data.NodeType = ntIdent then begin
    sym := GetSymbol(ANode.Data.ToString);

    if (sym.SymName = 'peek') then begin
      if ANode.SymIndex.IsLeaf and (ANode.SymIndex.Data.NodeType = ntInt) then begin
        loadIntegerRegMemAddr8('$'+IntToHex(ANode.SymIndex.Data.IntValue,4));
      end else begin
        VisitTreeNodes(ANode.SymIndex,NodeVisitor);

        loadIntRegMemIndirect8;
      end;
    end
    else
    if (sym.SymType = eSymType_func) then begin
      doProcFuncCall(sym.SymName);
    end
    else
    if sym.SymIsArray then begin
      // emit array index expression
      VisitTreeNodes(ANode.SymIndex,NodeVisitor);

      loadIntegerRegMemArray(sym.SymName,ANode.deref,sym.SymSize,sym.SymSubType);
    end else begin
      if (sym.SymSubType in[eSymSubType_byte,eSymSubType_boolean]) then
        loadIntegerRegMem8(sym.SymName)
      else
        loadIntegerRegMem(sym.SymName);
    end;
  end
  else begin
    case ANode.Data.NodeType of
      ntOpAdd : emitAddImm;
      ntOpSub : emitSubImm;
      ntOpMul : emitMulImm;
      ntOpDiv : emitDivImm;
      ntOpShl : emitShl;
      ntOpShr : emitShr;
      ntOpEql,
      ntOpNeq,
      ntOpLss,
      ntOpLeq,
      ntOpGtr,
      ntOpGeq : emitRelOp(ANode.Data.NodeType);
      ntOpAnd,
      ntOpOr,
      ntOpXor,
      ntOpNot : emitLogicOp(ANode.Data.NodeType);
    else
      EmitLn('  ' + ANode.Data.ToString);
    end;
  end;
end;

procedure TParserPas6502.SetScope(aScope : AnsiString);
begin
  if aScope = '' then
    FScope := aScope
  else
    FScope := aScope + '_';
end;

procedure TParserPas6502.pushIntType(intType: TIntNumType);
begin
  Inc(FIntTypeStackIndex);
  FIntTypeStack[FIntTypeStackIndex] := intType;
end;

function  TParserPas6502.popIntType: TIntNumType;
begin
  Result := FIntTypeStack[FIntTypeStackIndex];
  Dec(FIntTypeStackIndex);
end;

function  TParserPas6502.writeToRegister: Integer;
begin
  Inc(FIntRegisterIndex);
  Result := FIntRegisterIndex;
end;

function  TParserPas6502.readFromRegister: Integer;
begin
  Result := FIntRegisterIndex;
  Dec(FIntRegisterIndex);
end;

procedure TParserPas6502.loadIntegerRegMemArray(variable : String; deref : Boolean; symSize : Integer; symSubType : TSymSubType);
const
  cLoad8    = ':loadIntRegMemArray8(%s)';
  cLoad16   = ':loadIntRegMemArray(%s)';
  cLoad8At  = ':loadIntRegMemArray8At(%s)';
  cLoad16At = ':loadIntRegMemArrayAt(%s)';
begin
  if (deref) and (symSubType = eSymSubType_pbyte) then
    EmitCode(Format(cLoad8at,[variable]))
  else
  if (deref) and (symSubType = eSymSubType_pinteger) then
    EmitCode(Format(cLoad16at,[variable]))
  else begin
    if symSize = 1 then
      EmitCode(Format(cLoad8,[variable]))
    else
      EmitCode(Format(cLoad16,[variable]));
  end;
end;

procedure TParserPas6502.loadIntegerRegMem(variable : String);
const
  cLoad = '  :loadIntRegMem(%s)';

var
  numType : TIntNumType;
  number  : Int64;
begin
  EmitLn(Format(cLoad,[variable]));
end;

procedure TParserPas6502.loadIntegerRegMem8(variable : String);
const
  cLoad = '  :loadIntRegMem8(%s)';

var
  numType : TIntNumType;
  number  : Int64;
begin
  EmitLn(Format(cLoad,[variable]));
end;

procedure TParserPas6502.loadIntegerRegMemAddr8(address : String);
const
  cLoad = '  :loadIntRegMemAddr8(%s)';

var
  numType : TIntNumType;
  number  : Int64;
begin
  EmitLn(Format(cLoad,[address]));
end;

procedure TParserPas6502.loadIntRegMemIndirect8;
const
  cLoad = '  :loadIntRegMemIndirect8()';

var
  numType : TIntNumType;
  number  : Int64;
begin
  EmitLn(Format(cLoad,[]));
end;

procedure TParserPas6502.loadIntegerRegImm(num : String);
const
  cLoad = '  :loadIntRegIm(%s)';

var
  numType : TIntNumType;
  number  : Int64;
begin
  GetIntegerTypeAndValue(num,numType,number);

  case numType of
    ntSInt8  : begin EmitLn(Format(cLoad,[num])); end;
    ntUInt8  : begin EmitLn(Format(cLoad,[num])); end;
    ntSInt16 : begin EmitLn(Format(cLoad,[num])); end;
    ntUInt16 : begin EmitLn(Format(cLoad,[num])); end;
    ntSInt32 : Error('Number '''+num+''' too large! (16-bits max)');
    ntUInt32 : Error('Number '''+num+''' too large! (16-bits max)');
  end;
end;

function  TParserPas6502.intNumTypeToSize(i : TIntNumType) : String;
begin
  Result := '08';

  case i of
    ntSInt8  : Result := '08';
    ntUInt8  : Result := '08';
    ntSInt16 : Result := '16';
    ntUInt16 : Result := '16';
    ntSInt32 : Error('Number too large! (16-bits max)');
    ntUInt32 : Error('Number too large! (16-bits max)');
  end;
end;

procedure TParserPas6502.storeIntegerRegMem(variable : String; deref : Boolean; symSize : Integer; symSubType : TSymSubType);
const
  cStore8    = ':storeIntRegMem8(%s)';
  cStore16   = ':storeIntRegMem(%s)';
  cStore8at  = ':storeIntRegMem8At(%s)';
  cStore16at = ':storeIntRegMemAt(%s)';
begin
  variable := LowerCase(variable);

  if (deref) and (symSubType = eSymSubType_pbyte) then
    EmitCode(Format(cStore8at,[variable]))
  else
  if (deref) and (symSubType = eSymSubType_pinteger) then
    EmitCode(Format(cStore16at,[variable]))
  else begin
    if symSize = 1 then
      EmitCode(Format(cStore8,[variable]))
    else
      EmitCode(Format(cStore16,[variable]));
  end;
end;

procedure TParserPas6502.storeIntegerRegMemArray(variable : String; deref : Boolean; symSize : Integer; symSubType : TSymSubType);
const
  cStore8    = ':storeIntRegMemArray8(%s)';
  cStore16   = ':storeIntRegMemArray(%s)';
  cStore8At  = ':storeIntRegMemArray8At(%s)';
  cStore16At = ':storeIntRegMemArrayAt(%s)';
var
  Ofs   : Integer;
  Value : Integer;
begin
  variable := LowerCase(variable);

  Value := readFromRegister;
  Ofs   := readFromRegister;

  if (deref) and (symSubType = eSymSubType_pbyte) then
    EmitCode(Format(cStore8at,[variable]))
  else
  if (deref) and (symSubType = eSymSubType_pinteger) then
    EmitCode(Format(cStore16at,[variable]))
  else begin
    if symSize = 1 then
      EmitCode(Format(cStore8,[variable]))
    else
      EmitCode(Format(cStore16,[variable]));
  end;
end;

procedure TParserPas6502.set16im(value : Integer; variable : AnsiString; deref : Boolean; symSize : Integer; symSubType : TSymSubType);
const
  cOp8    = ':set8im(%d,%s)';
  cOp16   = ':set16im(%d,%s)';
  cOpAt8  = ':set8imAt(%d,%s)';
  cOpAt16 = ':set16imAt(%d,%s)';
begin
  variable := LowerCase(variable);

  if (deref) and (symSubType = eSymSubType_pbyte) then
    EmitCode(Format(cOpAt8,[value,variable]))
  else
  if (deref) and (symSubType = eSymSubType_pinteger) then
    EmitCode(Format(cOpAt16,[value,variable]))
  else begin
    if symSize = 1 then
      EmitCode(Format(cOp8,[value,variable]))
    else
      EmitCode(Format(cOp16,[value,variable]));
  end;
end;

procedure TParserPas6502.copy16(srcVar,dstVar : AnsiString; deref : Boolean; symSize : Integer; symSubType : TSymSubType);
const
  cOp8    = ':copy8(%s,%s)';
  cOp16   = ':copy16(%s,%s)';
  cOpAt8  = ':copy8At(%s,%s)';
  cOpAt16 = ':copy16At(%s,%s)';
begin
  srcVar := LowerCase(srcVar);
  dstVar := LowerCase(dstVar);

  if (deref) and (symSubType = eSymSubType_pbyte) then
    EmitCode(Format(cOpAt8,[srcVar,dstVar]))
  else
  if (deref) and (symSubType = eSymSubType_pinteger) then
    EmitCode(Format(cOpAt16,[srcVar,dstVar]))
  else begin
    if symSize = 1 then
      EmitCode(Format(cOp8,[srcVar,dstVar]))
    else
      EmitCode(Format(cOp16,[srcVar,dstVar]));
  end;
end;

procedure TParserPas6502.emitAddImm;
const
  cAdd = ':addIntReg()';
begin
  EmitCode(Format(cAdd,[]));
end;

procedure TParserPas6502.emitSubImm;
const
  cSub = ':subIntReg()';
begin
  EmitCode(Format(cSub,[]));
end;

procedure TParserPas6502.emitMulImm;
const
  cOp = ':mulIntReg(%d,%d,%d)';
var
  op1Reg   : Integer;
  op2Reg   : Integer;
begin
  op2Reg := readFromRegister;
  op1Reg := readFromRegister;

  EmitCode(Format(cOp,[op1Reg,op2Reg,writeToRegister]));
end;

procedure TParserPas6502.emitDivImm;
const
  cOp = ':divIntReg(%d,%d,%d)';
var
  op1Reg   : Integer;
  op2Reg   : Integer;
begin
  op2Reg := readFromRegister;
  op1Reg := readFromRegister;

  EmitCode(Format(cOp,[op1Reg,op2Reg,writeToRegister]));
end;

procedure TParserPas6502.emitShl;
const
  cOp = ':shlIntReg()';
begin
  EmitCode(Format(cOp,[]));
end;

procedure TParserPas6502.emitShr;
const
  cOp = ':shrIntReg()';
begin
  EmitCode(Format(cOp,[]));
end;

procedure TParserPas6502.emitJmpIntRegFalse(falseLabel : AnsiString);
const
  cOp = ':jmpIntRegFalse(%s)';
begin
  EmitCode(Format(cOp,[falseLabel]));
end;

procedure TParserPas6502.emitRelOp(AOp : TExpNodeType);
const
  cOps : array[0..5] of String = (
    ':cmpIntRegEql()',
    ':cmpIntRegNeq()',
    ':cmpIntRegLss()',
    ':cmpIntRegLeq()',
    ':cmpIntRegGtr()',
    ':cmpIntRegGeq()'
  );
begin
  EmitCode(Format(cOps[Ord(AOp) - Ord(ntOpEql)],[]));
end;

procedure TParserPas6502.emitLogicOp(AOp : TExpNodeType);
const
  cOps : array[0..3] of String = (
    ':orIntReg()',
    ':andIntReg()',
    ':xorIntReg()',
    ':notIntReg()'
  );
begin
  if AOp <> ntOpNot then begin
    EmitCode(Format(cOps[Ord(AOp) - Ord(ntOpOr)],[]));
  end
  else begin
    EmitCode(Format(cOps[Ord(AOp) - Ord(ntOpOr)],[]));
  end;
end;

function  TParserPas6502.doMulOp(ConstantsOnly: Boolean; ALeftNode: TExpTreeNode; AToken: Integer; AOp: TExpNodeType): TExpTreeNode;
begin
  Expect(AToken);

  Result := TExpTreeNode.Create(AOp,ALeftNode,signedFactor(ConstantsOnly));
end;

function  TParserPas6502.doAddOp(ConstantsOnly: Boolean; ALeftNode: TExpTreeNode; AToken: Integer; AOp: TExpNodeType): TExpTreeNode;
begin
  Expect(AToken);

  Result := TExpTreeNode.Create(AOp,ALeftNode,term(ConstantsOnly));
end;

function  TParserPas6502.doRelOp(ConstantsOnly: Boolean; ALeftNode: TExpTreeNode; AToken: Integer; AOp: TExpNodeType): TExpTreeNode;
begin
  Expect(AToken);

  Result := TExpTreeNode.Create(AOp,ALeftNode,simpleExpression(ConstantsOnly));
end;

function  TParserPas6502.factor(ConstantsOnly : Boolean) : TExpTreeNode;
var
  t       : TToken;
  n       : Integer;
  c       : Integer;
  IsUnary : Boolean;
  sym     : TSymInfo;
  ident   : String;
  e       : TExpTreeNode;
begin
  t := Token;

  IsUnary := False;

  if (t.TokenType in[Token_plus,Token_minus]) then begin
    IsUnary := (t.TokenType = Token_minus);
    GetToken;
  end;

  t := Token;

  if Accept(Token_intnumber) then begin
    Val(t.TokenValue,n,c);

    if IsUnary then n := n * -1;

    Result := TExpTreeNode.Create(n);
  end
  else
  if Accept(Token_true) or Accept(Token_False) then begin
    if IsUnary then
      Result := TExpTreeNode.Create(not(t.TokenType = Token_true))
    else
      Result := TExpTreeNode.Create(t.TokenType = Token_true);
  end
  else
  if Accept(Token_ident) then begin
    ident := t.TokenValue;

    while Accept(Token_period) do begin
      ident := ident + '.' + Token.TokenValue;
      GetToken;
    end;

    sym := GetSymbol(FScope + ident);

    if sym = Nil then sym := GetSymbol(ident);

    if (sym = Nil) then Error('Undeclared identifier "'+ident+'"');

    if (sym.SymType = eSymType_Proc) then Error('Procedure "'+ident+'" not useable in expressions');

    if ConstantsOnly then begin
      if (sym.SymType = eSymType_Var) then Error('Constants only in constant declarations');
    end;

    e := Nil;

    if sym.SymIsArray then begin
      Expect(Token_lbracket);
      e := SimplifyTree(expression(false));
      Expect(Token_rbracket);
    end
    else
    if sym.SymType = eSymType_func then begin
      if (sym.SymName = 'peek') then begin
        Expect(Token_lparen);
        e := SimplifyTree(expression(false));
        Expect(Token_rparen);
      end
    end;

    if (sym.SymType in [eSymType_Func,eSymType_Var,eSymType_Addr]) then begin
      Result := TExpTreeNode.Create(sym.SymName);

      Result.Deref := Accept(Token_caret);

      if Result.Deref and not (sym.SymSubType in [eSymSubType_pbyte,eSymSubType_pinteger]) then Error('cannot dereference a non-pointer variable!');

      if sym.SymIsArray or (sym.SymName = 'peek') then begin
        Result.SymIndex := e;
      end;

      if IsUnary then begin
        Result := TExpTreeNode.Create(ntOpXor,Result,TExpTreeNode.Create(-1));
        Result := TExpTreeNode.Create(ntOpAdd,Result,TExpTreeNode.Create(1));
      end;
    end
    else
    if ((sym.SymType = eSymType_Const)) then begin
      if LowerCase(sym.SymValue) = 'true' then
        n := cTRUE
      else
      if LowerCase(sym.SymValue) = 'false' then
        n := cFALSE
      else
        Val(sym.SymValue,n,c);

      if IsUnary then n := n * -1;

      Result := TExpTreeNode.Create(n);
    end;


  end
  else
  if Accept(Token_lparen) then begin
    Result := expression(ConstantsOnly);

    Expect(Token_rparen);

    if IsUnary then begin
      Result := TExpTreeNode.Create(ntOpXor,Result,TExpTreeNode.Create(-1));
      Result := TExpTreeNode.Create(ntOpAdd,Result,TExpTreeNode.Create(1));
    end;
  end
  else
    Error('Factor: found "'+t.TokenValue+'"');
end;

function  TParserPas6502.signedFactor(ConstantsOnly : Boolean): TExpTreeNode;
var
  IsNot   : Boolean;
begin
  IsNot   := False;

  if Accept(Token_not) then begin
    IsNot := True;
  end;

  Result := factor(ConstantsOnly);

  if IsNot then begin
    Result := TExpTreeNode.Create(ntOpNot,Result);
  end;
end;

function  TParserPas6502.term(ConstantsOnly : Boolean) : TExpTreeNode;
begin
  Result := signedFactor(ConstantsOnly);

  while (Token.TokenType in [Token_times,Token_slash,Token_div,Token_and,Token_shl,Token_shr]) do begin
    if      (Token.TokenType = Token_times) then Result := doMulOp(ConstantsOnly,Result,Token.TokenType,ntOpMul)
    else if (Token.TokenType = Token_slash) then Result := doMulOp(ConstantsOnly,Result,Token.TokenType,ntOpDiv)
    else if (Token.TokenType = Token_div)   then Result := doMulOp(ConstantsOnly,Result,Token.TokenType,ntOpDiv)
    else if (Token.TokenType = Token_and)   then Result := doMulOp(ConstantsOnly,Result,Token.TokenType,ntOpAnd)
    else if (Token.TokenType = Token_shl)   then Result := doMulOp(ConstantsOnly,Result,Token.TokenType,ntOpShl)
    else if (Token.TokenType = Token_shr)   then Result := doMulOp(ConstantsOnly,Result,Token.TokenType,ntOpShr)
    else Error('Mulop');
  end;
end;

function  TParserPas6502.simpleExpression(ConstantsOnly : Boolean) : TExpTreeNode;
begin
  Result := term(ConstantsOnly);

  while (Token.TokenType in [Token_plus,Token_minus,Token_or,Token_xor]) do begin
    if      (Token.TokenType = Token_plus)  then Result := doAddOp(ConstantsOnly,Result,Token.TokenType,ntOpAdd)
    else if (Token.TokenType = Token_minus) then Result := doAddOp(ConstantsOnly,Result,Token.TokenType,ntOpSub)
    else if (Token.TokenType = Token_or)    then Result := doAddOp(ConstantsOnly,Result,Token.TokenType,ntOpOr)
    else if (Token.TokenType = Token_xor)   then Result := doAddOp(ConstantsOnly,Result,Token.TokenType,ntOpXor)
    else Error('Addop');
  end;
end;

function  TParserPas6502.expression(ConstantsOnly : Boolean): TExpTreeNode;
begin
  Result := simpleExpression(ConstantsOnly);

  if IsRelop(Token.TokenType) then begin
    if      (Token.TokenType = Token_eql) then Result := doRelOp(ConstantsOnly,Result,Token.TokenType,ntOpEql)
    else if (Token.TokenType = Token_neq) then Result := doRelOp(ConstantsOnly,Result,Token.TokenType,ntOpNeq)
    else if (Token.TokenType = Token_lss) then Result := doRelOp(ConstantsOnly,Result,Token.TokenType,ntOpLss)
    else if (Token.TokenType = Token_leq) then Result := doRelOp(ConstantsOnly,Result,Token.TokenType,ntOpLeq)
    else if (Token.TokenType = Token_gtr) then Result := doRelOp(ConstantsOnly,Result,Token.TokenType,ntOpGtr)
    else if (Token.TokenType = Token_geq) then Result := doRelOp(ConstantsOnly,Result,Token.TokenType,ntOpGeq);
  end;
end;

procedure TParserPas6502.Assignment(variable : String);
var
  e        : TExpTreeNode; // assignment expression
  i        : TExpTreeNode; // array index expression
  valueSym : TSymInfo;
  arraySym : TSymInfo;
  sym      : TSymInfo;
  deref    : Boolean;
begin
  sym := GetSymbol(variable);

  if (sym = Nil) then Error('Undeclared identifier "'+variable+'"');

  if (sym.SymType = eSymType_Const) then Error('Left side cannot be assigned to');
  if sym.SymIsReadOnly then Error('Left side cannot be assigned to');

  if not(sym.SymSubType in [eSymSubType_pbyte,eSymSubType_pinteger]) and deref then
    Error('Can''t dereference a non-pointer type!');

  if (sym.SymIsArray) then begin
    Expect(Token_lbracket);

    i := SimplifyTree(TExpTreeNode.Create(ntOpMul,expression(false),TExpTreeNode.Create(sym.SymSize)));
    VisitTreeNodes(i,NodeVisitor);

    Expect(Token_rbracket);
  end;

  deref := Accept(Token_caret);

  if deref and not (sym.SymSubType in[eSymSubType_pbyte,eSymSubType_pinteger]) then
    Error('Cannot dereference a non-pointer variable!');

  Expect(Token_becomes);

  e := SimplifyTree(expression(false));

  if sym.SymIsArray then begin
    VisitTreeNodes(e,NodeVisitor);

    storeIntegerRegMemArray(variable,deref,sym.SymSize,sym.SymSubType);
  end
  else begin
    if (e.IsLeaf) then begin
    // simplify to direct set and copy for simple expressions (number, ident)
      if (e.Data.NodeType in [ntInt,ntBool]) then begin
        set16im(e.Data.IntValue,variable,deref,sym.SymSize,sym.SymSubType);
      end
      else begin
        arraySym := GetSymbol(e.Data.IdentValue);

        if arraySym.SymName = 'peek' then begin
          // visit array index first

          if e.SymIndex.isLeaf and (e.SymIndex.Data.NodeType = ntInt) then
            loadIntegerRegMemAddr8('$'+IntToHex(e.SymIndex.Data.IntValue,4))
          else
            VisitTreeNodes(e.SymIndex,NodeVisitor);

          storeIntegerRegMem(variable,deref,sym.SymSize,sym.SymSubType);
        end
        else
        if arraySym.SymIsArray then begin
          // visit array index first
          VisitTreeNodes(e.SymIndex,NodeVisitor);

          loadIntegerRegMemArray(e.Data.IdentValue,e.Deref,arraySym.SymSize,arraySym.SymSubType);

          storeIntegerRegMem(variable,deref,sym.SymSize,sym.SymSubType);
        end
        else begin
          valueSym := GetSymbol(e.Data.IdentValue);

          if (valueSym.SymName <> 'peek') then begin
            doProcFuncCall(valueSym.SymName);

            storeIntegerRegMem(variable,deref,sym.SymSize,sym.SymSubType);
          end
          else
            copy16(e.Data.IdentValue,variable,deref,sym.SymSize,sym.SymSubType);
        end;
      end;
    end else begin
      VisitTreeNodes(e,NodeVisitor);
      storeIntegerRegMem(variable,deref,sym.SymSize,sym.SymSubType);
    end;
  end;
end;

procedure TParserPas6502.LoadBinary;
begin

end;

function  StrToScreenString(s : AnsiString) : AnsiString;
var
  i : Integer;
begin
  Result := '';
  for i := 1 to Length(s) do
    Result := Result + Char(cPETSCIIToCBMScreenCode[cASCIIToPETSCIITable[Ord(s[i])]]);
end;

procedure TParserPas6502.doWriteLn;
var
  s : AnsiString;
begin
  Expect(Token_lparen);

  s := Token.TokenValue;

  Expect(Token_string);
  Expect(Token_rparen);

	EmitCode('jsr PUTSTRI');
  EmitCode('.encoding "petscii_mixed"');
	EmitCode('.text "'+s+'"');
  EmitCode('.byte 13,10,0');
end;

procedure TParserPas6502.copyData8;
var
  e        : TExpTreeNode;
  src      : TExpTreeNode;
  dst      : TExpTreeNode;
  count    : TExpTreeNode;
  countReg : Integer;
  srcReg   : Integer;
  dstReg   : Integer;
begin
  Expect(Token_lparen);

  src := SimplifyTree(expression(false));

  Expect(Token_comma);

  dst := SimplifyTree(expression(false));

  Expect(Token_comma);

  count := SimplifyTree(expression(false));

  if src.IsLeaf and dst.IsLeaf and count.IsLeaf and (src.Data.NodeType = ntInt) and (dst.Data.NodeType = ntInt) and (count.Data.NodeType = ntInt) then
    EmitCode(':copyData8('+'$'+IntToHex(src.Data.IntValue,4)+','+'$'+IntToHex(dst.Data.IntValue,4)+','+IntToStr(count.Data.IntValue)+')')
  else begin
    VisitTreeNodes(src,NodeVisitor);
    VisitTreeNodes(dst,NodeVisitor);
    VisitTreeNodes(count,NodeVisitor);

    countReg := readFromRegister;
    dstReg   := readFromRegister;
    srcReg   := readFromRegister;

    EmitCode(':copyDataIntReg8('+IntToStr(srcReg)+','+IntToStr(dstReg)+','+IntToStr(countReg)+')');
  end;

  Expect(Token_rparen);
end;

procedure TParserPas6502.clearScreen;
var
  e        : TExpTreeNode;
  addr     : TExpTreeNode;
  value    : TExpTreeNode;
  valueReg : Integer;
  addrReg  : Integer;
begin
  Expect(Token_lparen);

  addr := SimplifyTree(expression(false));

  Expect(Token_comma);

  value := SimplifyTree(expression(false));

  if addr.IsLeaf and value.IsLeaf and (addr.Data.NodeType = ntInt) and (value.Data.NodeType = ntInt) then
    EmitCode(':clearScreen('+'$'+IntToHex(addr.Data.IntValue,4)+','+IntToStr(value.Data.IntValue)+')')
  else begin
    VisitTreeNodes(addr,NodeVisitor);
    VisitTreeNodes(value,NodeVisitor);

    valueReg := readFromRegister;
    addrReg  := readFromRegister;

    EmitCode(':clearScreenIntReg('+IntToStr(addrReg)+','+IntToStr(valueReg)+')');
  end;

  Expect(Token_rparen);
end;

procedure TParserPas6502.Poke;
var
  e        : TExpTreeNode;
  addr     : TExpTreeNode;
  value    : TExpTreeNode;
  valueSym : TSymInfo;
begin
  Expect(Token_lparen);

  addr := SimplifyTree(expression(false));

  Expect(Token_comma);

  value := SimplifyTree(expression(false));

  if addr.IsLeaf and value.IsLeaf and (addr.Data.NodeType = ntInt) and (value.Data.NodeType = ntInt) then
    set16im(value.Data.IntValue,'$'+IntToHex(addr.Data.IntValue,4),False,1,eSymSubType_byte)
  else begin
    VisitTreeNodes(addr,NodeVisitor);
    VisitTreeNodes(value,NodeVisitor);

    EmitCode(':poke()');
  end;

  Expect(Token_rparen);
end;

{
procedure TParserPas6502.Poke;
var
  e        : TExpTreeNode;
  addr     : TExpTreeNode;
  value    : TExpTreeNode;
  valueSym : TSymInfo;
  valueReg : Integer;
  addrReg  : Integer;
begin
  Expect(Token_lparen);

  addr := SimplifyTree(expression(false));

  Expect(Token_comma);

  value := SimplifyTree(expression(false));

  if addr.IsLeaf and value.IsLeaf and (addr.Data.NodeType = ntInt) and (value.Data.NodeType = ntInt) then
    set16im(value.Data.IntValue,'$'+IntToHex(addr.Data.IntValue,4),False,1,eSymSubType_byte)
  else begin
    VisitTreeNodes(addr,NodeVisitor);

    if value.Data.NodeType = ntIdent then begin
      valueSym := GetSymbol(value.Data.IdentValue);

      if (valueSym.SymName <> 'peek') and (valueSym.SymType = eSymType_Func) then begin
        doProcFuncCall(valueSym.SymName);
        addrReg  := readFromRegister;
        valueReg := readFromRegister;
      end
      else
        VisitTreeNodes(value,NodeVisitor);
    end
    else begin
      VisitTreeNodes(value,NodeVisitor);

      valueReg := readFromRegister;
      addrReg  := readFromRegister;
    end;

    EmitCode(':poke('+IntToStr(addrReg)+','+IntToStr(valueReg)+')');
  end;

  Expect(Token_rparen);
end;
}
procedure TParserPas6502.Poke16;
var
  e        : TExpTreeNode;
  addr     : TExpTreeNode;
  value    : TExpTreeNode;
  valueReg : Integer;
  addrReg  : Integer;
begin
  Expect(Token_lparen);

  addr := SimplifyTree(expression(false));

  Expect(Token_comma);

  value := SimplifyTree(expression(false));

  if addr.IsLeaf and value.IsLeaf and (addr.Data.NodeType = ntInt) and (value.Data.NodeType = ntInt) then
    set16im(value.Data.IntValue,'$'+IntToHex(addr.Data.IntValue,4),False,2,eSymSubType_integer)
  else begin
    VisitTreeNodes(addr,NodeVisitor);
    VisitTreeNodes(value,NodeVisitor);
    EmitCode(':poke16()');
  end;

  Expect(Token_rparen);
end;

procedure TParserPas6502.Condition;
var
  c : TExpTreeNode;
begin
  c := SimplifyTree(expression(false));
  VisitTreeNodes(c,NodeVisitor);

  EmitCode(':intRegToBoolean()');
end;

procedure TParserPas6502.doIf;
var
  L1,L2 : AnsiString;
begin
  Condition;

  Expect(Token_then);

  L1 := NewLabel;
  L2 := L1;

  emitJmpIntRegFalse(L1);

  Statement;

  if Accept(Token_else) then begin
    L2 := NewLabel;
    EmitCode('jmp ' + L2);
    EmitLabel(L1);

    Statement;
  end;

  EmitLabel(L2);
end;

procedure TParserPas6502.doWhile;
var
  L1,L2 : AnsiString;
begin
  L1 := NewLabel;
  L2 := NewLabel;

  EmitLabel(L1);

  Condition;

  Expect(Token_do);

  emitJmpIntRegFalse(L2);

  Statement;

  EmitCode('jmp '+L1);

  EmitLabel(L2);
end;

procedure TParserPas6502.doInc;
var
  t        : TToken;
  variable : AnsiString;
  incValue : TExpTreeNode;
  sym      : TSymInfo;
  deref    : Boolean;
begin

  Expect(Token_lparen);
  variable := Token.TokenValue;
  Expect(Token_ident);

  while Accept(Token_period) do begin
    variable := variable + '.' + Token.TokenValue;
    Expect(Token_ident);
  end;

  sym := GetSymbol(FScope + variable);

  if sym = Nil then sym := GetSymbol(variable);

  if (sym = Nil) then Error('Undeclared identifier: "'+variable+'"');

  if not(sym.SymType in [eSymType_var,eSymType_Addr]) then Error('Can only increment variables');

  deref := Accept(Token_caret);

  if Accept(Token_comma) then begin
    incValue := SimplifyTree(expression(false));
  end;

  Expect(Token_rparen);

  if sym.SymSubType = eSymSubType_pbyte then begin
    if deref then
      EmitCode(':incMem8At('+sym.SymName+')')
    else
      EmitCode(':incMem('+sym.SymName+')')
  end
  else
  if sym.SymSubType = eSymSubType_pinteger then begin
    if deref then
      EmitCode(':incMemAt('+sym.SymName+')')
    else begin
      EmitCode(':incMem('+sym.SymName+')');
      EmitCode(':incMem('+sym.SymName+')');
    end;
  end
  else begin
    if (sym.SymSize = 2) then
      EmitCode(':incMem('+sym.SymName+')')
    else
      EmitCode(':incMem8('+sym.SymName+')');
  end;
end;

procedure TParserPas6502.doDec;
var
  t        : TToken;
  variable : AnsiString;
  decValue : TExpTreeNode;
  sym      : TSymInfo;
  deref    : Boolean;
begin
  Expect(Token_lparen);
  variable := Token.TokenValue;
  Expect(Token_ident);

  while Accept(Token_period) do begin
    variable := variable + '.' + Token.TokenValue;
    Expect(Token_ident);
  end;

  sym := GetSymbol(FScope + variable);

  if sym = Nil then sym := GetSymbol(variable);

  if (sym = Nil) then Error('Undeclared identifier: "'+variable+'"');

  if not(sym.SymType in [eSymType_var,eSymType_Addr]) then Error('Can only decrement variables');

  deref := Accept(Token_caret);

  if Accept(Token_comma) then begin
    decValue := SimplifyTree(expression(false));
  end;

  Expect(Token_rparen);

  if sym.SymSubType = eSymSubType_pbyte then begin
    if deref then
      EmitCode(':decMem8At('+sym.SymName+')')
    else
      EmitCode(':decMem8('+sym.SymName+')')
  end
  else
  if sym.SymSubType = eSymSubType_pinteger then begin
    if deref then
      EmitCode(':decMemAt('+sym.SymName+')')
    else begin
      EmitCode(':decMem('+sym.SymName+')');
      EmitCode(':decMem('+sym.SymName+')');
    end;
  end
  else begin
    if (sym.SymSize = 2) then
      EmitCode(':decMem('+sym.SymName+')')
    else
      EmitCode(':decMem8('+sym.SymName+')');
  end;
end;

procedure TParserPas6502.doAsmBlock;
var
  startLine : Integer;
  endLine   : Integer;
  line      : AnsiString;
  i         : Integer;
begin
  startLine := FLineCount - 1;

  while not EOF and (Token.TokenType <> Token_asmend) do begin
    GetToken;
  end;

  endLine := FLineCount;
  Expect(Token_asmend);

  if not(endLine = startLine + 1) then begin
    // asm block not empty, adjust start/end
//    Inc(startLine);
//    Dec(endLine);

    for i := startLine to endLine do begin
      line := Trim(FSourceCodeLines[i - 1]);
      if (Pos('asmbegin',LowerCase(line)) = 0) and
         (Pos('asmend',LowerCase(line))   = 0) then begin
        if Copy(line,Length(line),1) = ':' then
        // is label
          EmitLn(line)
        else
          EmitCode(line);
      end;
    end;
  end;
end;

procedure TParserPas6502.doProcFuncCall(procName : AnsiString);
begin
  if Accept(Token_lparen) then
    Expect(Token_rparen);

  EmitCode('jsr '+procName);
end;

procedure TParserPas6502.setupInterrupt;
var
  sym     : TSymInfo;
  irqName : AnsiString;
begin
  Expect(Token_lparen);

  irqName := Token.TokenValue;

  sym := GetSymbol(irqName);

  if sym = Nil then Error('Undeclared identifier!');

  if (sym.SymType <> eSymType_IRQProc) then Error('Procedure "'+irqName+'" must be an interrupt');

  Expect(Token_ident);

  EmitCode('sei');
  EmitCode('lda #<'+sym.SymName);
  EmitCode('sta irqveclo');
  EmitCode('lda #>'+sym.SymName);
  EmitCode('sta irqvechi');
  EmitCode('cli');

  Expect(Token_rparen);
end;

procedure TParserPas6502.setupRasterIRQ;
var
  sym     : TSymInfo;
  irqName : AnsiString;
  irqLine : TExpTreeNode;
begin
  Expect(Token_lparen);

  irqName := Token.TokenValue;

  sym := GetSymbol(irqName);

  if sym = Nil then Error('Undeclared identifier!');

  if (sym.SymType <> eSymType_RasterProc) then Error('Procedure "'+irqName+'" must be a raster irq');

  Expect(Token_ident);

  Expect(Token_comma);

  irqLine := SimplifyTree(expression(true));

  EmitCode('sei');
  EmitCode('lda #$35  // Bank out kernal and basic, keep I/O');
  EmitCode('sta $01   // $e000-$ffff');
  EmitCode('');
  EmitCode(':SetupIRQ('+sym.SymName+','+IntToStr(irqLine.Data.IntValue)+')');
  EmitCode('cli');

  Expect(Token_rparen);
end;

procedure TParserPas6502.irqEnd;
var
  sym     : TSymInfo;
  irqName : AnsiString;
  irqLine : TExpTreeNode;
begin
  Expect(Token_lparen);

  irqName := Token.TokenValue;

  sym := GetSymbol(irqName);

  if sym = Nil then Error('Undeclared identifier!');

  if (sym.SymType <> eSymType_RasterProc) then Error('"'+irqName+'" must be a raster irq');

  Expect(Token_ident);

  Expect(Token_comma);

  irqLine := SimplifyTree(expression(true));

  EmitCode(':irq_end('+irqName+','+IntToStr(irqLine.Data.IntValue)+')');

  Expect(Token_rparen);
end;

procedure TParserPas6502.Statement;
var
  t       : TToken;
  varName : AnsiString;
  sym     : TSymInfo;
begin
  t       := Token;
  varName := Token.TokenValue;

  if Accept(Token_exit) then begin
    EmitCode('rts');
  end
  else
  if Accept(Token_readjoysticks) then begin
    if Accept(Token_lparen) then begin
      EmitCode('jsr readJoysticksInput');
      Expect(Token_rparen);
    end
    else
      EmitCode('jsr readJoysticksInput');
  end
  else
  if Accept(Token_setupinterrupt) then begin
    setupInterrupt;
  end
  else
  if Accept(Token_setuprasterirq) then begin
    setupRasterIRQ
  end
  else
  if Accept(Token_irqend) then begin
    irqEnd
  end
  else
  if Accept(Token_writeln) then
    doWriteLn
  else
  if Accept(Token_clearscreen) then
    clearScreen
  else
  if Accept(Token_copydata8) then
    copyData8
  else
  if Accept(Token_poke) then
    Poke
  else
  if Accept(Token_poke16) then
    Poke16
  else
  if Accept(Token_while) then
    doWhile
  else
  if Accept(Token_if) then
    doIf
  else
  if Accept(Token_inc) then
    doInc
  else
  if Accept(Token_dec) then
    doDec
  else
  if Accept(Token_ident) then begin
    while Accept(Token_period) do begin
      varName := varName + '.' + Token.TokenValue;
      Expect(Token_ident);
    end;

    sym := GetSymbol(FScope + varName);

    if sym = Nil then sym := GetSymbol(varName);

    if sym = Nil then Error('Undeclared identifier / keyword "'+t.TokenValue+'"');

    if sym.SymType <> eSymType_Proc then
      Assignment(sym.SymName)
    else
      doProcFuncCall(sym.SymName);
  end
  else
  if Accept(Token_asmbegin) then
   doAsmBlock
  else
  if Accept(Token_begin) then begin
    Statement;
    while Accept(Token_semicolon) do begin
      if Token.TokenType <> Token_end then
        Statement;
    end;
    Expect(Token_end);
  end;
end;

procedure TParserPas6502.GetVarDecls(var vars : TAnsiStringArray; var varType,address : AnsiString; var varLen : Integer; var isArray : Boolean);
var
  i          : Integer;
  IsAbsolute : Boolean;
  sym        : TSymInfo;
  e          : TExpTreeNode;
  symType    : TSymInfo;

  procedure parseVarDecl(varDecl : AnsiString);
  begin
    varDecl := FScope + varDecl;

    sym := AddSymbol(varDecl);
    sym.SymType := eSymType_Var;

    i := Length(vars);
    SetLength(vars,i + 1);

    vars[i] := varDecl;
    Expect(Token_ident);
  end;
begin
  repeat
    parseVarDecl(Token.TokenValue);

    while Accept(Token_comma) do begin
      parseVarDecl(Token.TokenValue);
    end;
  until Token.TokenType = Token_colon;

  Expect(Token_colon);

  isArray := False;
  varLen  := 1;

  if Accept(Token_array) then begin
    isArray := True;

    Expect(Token_lbracket);
    // get array start range (must be 0)
    e := SimplifyTree(expression(true));

    if (e.Data.IntValue <> 0) then Error('Can''t have array start range other than "0"');

    Expect(Token_range);

    // get array end range (must be positive);
    e := SimplifyTree(expression(true));

    if (e.Data.IntValue < 0) then Error('Array end range must be positive');

    varLen := e.Data.IntValue + 1;

    Expect(Token_rbracket);

    Expect(Token_of);
  end;
  
  varType := Token.TokenValue;
  if (LowerCase(varType) <> 'integer') and
     (LowerCase(varType) <> 'boolean') and
     (LowerCase(varType) <> 'byte') and
     (LowerCase(varType) <> 'pbyte') and
     (LowerCase(varType) <> 'pinteger') then begin
    symType := GetSymbol(varType);
    if symType = Nil then
      Error('Undeclared type, found "'+varType+'"')
    else
    if (symType.SymSubType <> eSymSubType_typeRecord) then
      Error('Illegal variable delaration type, found "'+varType+'"')
  end;

  varType := LowerCase(varType);

  Expect(Token_ident);

  Address := '';

  if Accept(Token_absolute) then begin
    if ((varType = 'pbyte') or (varType = 'pinteger')) then Error('Cannot use pByte/pInteger with the absolute declaration');

    e := SimplifyTree(expression(true));

    Address := IntToStr(e.Data.IntValue);
  end;

  Expect(Token_semicolon);
end;

procedure TParserPas6502.VarDecls(DeclarationLocation : TDeclarationLocation);
var
  varType : AnsiString;
  address : AnsiString;
  vars    : TAnsiStringArray;
  varLen  : Integer;
  isArray : Boolean;
  i       : Integer;
  s       : Integer;
  sym     : TSymInfo;
  typeSym : TSymInfo;
  subSym  : TSymInfo;
  gSym    : TSymInfo; // global sym including fully qualified name
  n,c     : Integer;
begin
  repeat
    SetLength(vars,0);
    GetVarDecls(vars,varType,address,varLen,isArray);

    for i := 0 to Length(vars) - 1 do begin
      sym            := GetSymbol(vars[i]);
      sym.SymAddr    := address;
      sym.SymLength  := varLen;
      sym.SymIsArray := isArray;
      sym.SymSize    := 1;

      StringToSymSubType(varType,sym);

      if (sym.SymSubType = eSymSubType_typeRecord) then begin
      // output record type var and sub variables
        EmitLn(sym.SymName + ': {');
        typeSym := GetSymbol(varType);

        for s := 0 to typeSym.SymRecordVars.count - 1 do begin
          subSym := typeSym.SymRecordVars.Symbols[s];

          gSym   := AddSymbol(sym.SymName + '.' + subSym.SymName);
          gSym.SymType    := eSymType_Var;
          gSym.SymValue   := subSym.SymValue;
          gSym.SymSubType := subSym.SymSubType;
          gSym.SymSize    := subSym.SymSize;
          gSym.SymLength  := subSym.SymLength;

          EmitCode(subSym.SymName + ': .fill '+IntToStr(subSym.SymSize * subSym.SymLength)+',0');
        end;
        EmitLn('}');
        EmitLn('');
      end
      else begin
        if (address <> '') then begin
          Val(sym.SymAddr,n,c);

          if (DeclarationLocation = dlInterface) then
            EmitLn('.var @'+sym.SymName+' = ' + sym.SymAddr + ' // $'+IntToHex(n,4))
          else
            EmitLn('.var '+sym.SymName+' = ' + sym.SymAddr + ' // $'+IntToHex(n,4));

          sym.SymType := eSymType_Addr;
        end
        else begin
          if (DeclarationLocation = dlInterface) then
            EmitLn('@'+sym.SymName + ': .fill '+IntToStr(sym.SymSize * sym.SymLength)+',0')
          else
            EmitLn(sym.SymName + ': .fill '+IntToStr(sym.SymSize * sym.SymLength)+',0');
        end;
      end;
    end;
  until Token.TokenType in [Token_type,Token_begin,Token_implementation,Token_func,Token_proc,Token_rasterirq,Token_end];
end;

procedure TParserPas6502.InitializedConstDecls(sym : TSymInfo; DeclarationLocation : TDeclarationLocation);
var
  varType  : AnsiString;
  varLen   : Integer;
  e        : TExpTreeNode;
  addrTree : TExpTreeNode;
  valStr   : AnsiString;
  valStrLo : AnsiString;
  valStrHi : AnsiString;
  outStr   : AnsiString;
  i        : Integer;
  binPath  : AnsiString;
  binData  : array of Byte;
  binFile  : TFileStream;

  address  : Integer;
  code     : Integer;

  hasAddress : Boolean;

  symAddr  : TSymInfo;
  symData  : TSymInfo;
  symSize  : TSymInfo;
begin
  Sym.SymIsArray    := False;
  Sym.SymType       := eSymType_Var;
  Sym.SymIsReadOnly := True;
  sym.SymSize       := 1;

  if Accept(Token_loadbinary) then begin
    Expect(Token_lparen);

    binPath := Token.TokenValue;

    Expect(Token_string);
    if not FileExists(binPath) then begin
      binPath := FProjectPath + binPath;

      if not FileExists(binPath) then Error('Binary file "'+ExtractFileName(binPath)+'" not found');
    end;

    hasAddress := False;

    if Accept(Token_comma) then begin
      hasAddress := True;

      addrTree := SimplifyTree(expression(True));
      address  := addrTree.Data.IntValue;
    end;

    // load binary data and emit as code
    binFile := TFileStream.Create(binPath,fmOpenRead);
    try
      SetLength(binData,binFile.Size);
      binFile.Read(binData[0],binFile.Size);

      symAddr := AddSymbol(Sym.SymName + '.address');
      symAddr.SymIsArray    := False;
      symAddr.SymType       := eSymType_Var;
      symAddr.SymSubType    := eSymSubType_pinteger;
      symAddr.SymIsReadOnly := True;
      symAddr.SymSize       := 2;

      symSize  := AddSymbol(Sym.SymName + '.size');
      symSize.SymIsArray    := False;
      symSize.SymType       := eSymType_Var;
      symSize.SymSubType    := eSymSubType_integer;
      symSize.SymIsReadOnly := True;
      symSize.SymSize       := 2;

      if hasAddress then begin
        EmitLn('.eval _saveAddress = *');
        EmitLn('* = $' + IntToHex(address,4) + ' "'+Sym.SymName+'"');
      end;

      EmitLn(Sym.SymName + ': {');
      EmitCode('.var file = LoadBinary("'+binPath+'")');

      if not hasAddress then begin
        EmitCode('size:    .byte <file.getSize(),>file.getSize()');
        EmitCode('address: .byte <data,>data');
      end;
      EmitCode('data:    .fill file.getSize(), file.get(i)');
      EmitLn('}');

      if hasAddress then begin
        EmitLn('* = _saveAddress');
      end;

      EmitLn('');
    finally
      binFile.Free;
    end;
    Expect(Token_rparen);

    Exit;
  end;

  if Accept(Token_array) then begin
    Expect(Token_lbracket);

    // get array start range (must be 0)
    e := SimplifyTree(expression(True));

    if (e.Data.IntValue <> 0) then Error('Can''t have array start range other than "0"');

    Expect(Token_range);

    // get array end range (must be positive)
    e := SimplifyTree(expression(True));

    if (e.Data.IntValue < 0) then Error('Array end range must be positive');

    varLen := e.Data.IntValue + 1;

    sym.SymIsArray := True;
    sym.SymLength  := varLen;

    Expect(Token_rbracket);

    Expect(Token_of);
  end;

  varType := Token.TokenValue;
  if (LowerCase(varType) <> 'integer') and
     (LowerCase(varType) <> 'boolean') and
     (LowerCase(varType) <> 'byte') and
     (LowerCase(varType) <> 'pbyte') and
     (LowerCase(varType) <> 'pinteger') then
    Error('Illegal Initialized constant delaration type "'+varType+'"; found "'+varType+'"');

  varType := LowerCase(varType);

  Expect(Token_ident);

  // set variable sub type
  StringToSymSubType(varType,sym);

  Expect(Token_eql);

  if not sym.SymIsArray then begin
    e := SimplifyTree(expression(True));

    valStr := IntToHex(e.Data.IntValue,4);
    valStrLo := '$'+Copy(valStr,3,2);
    valStrHi := '$'+Copy(valStr,1,2);

    if (DeclarationLocation = dlInterface) then begin
      if sym.SymSize = 1 then
        EmitLn('@'+sym.SymName + ': .byte ' + valStrLo)
      else
        EmitLn('@'+sym.SymName + ': .byte ' + valStrLo+','+valStrHi)
    end
    else begin
      if sym.SymSize = 1 then
        EmitLn(sym.SymName + ': .byte ' + valStrLo)
      else
        EmitLn(sym.SymName + ': .byte ' + valStrLo+','+valStrHi)
    end;
  end else begin
  // parse and emit array values
    Expect(Token_lparen);

    outStr := sym.SymName + ': .byte ';

    if (DeclarationLocation = dlInterface) then
      outStr := '@'+outStr;

    for i := 1 to varLen do begin
      e := SimplifyTree(expression(True));

      valStr := IntToHex(e.Data.IntValue,4);
      valStrLo := '$'+Copy(valStr,3,2);
      valStrHi := '$'+Copy(valStr,1,2);

      if sym.SymSize = 1 then
        outStr := outStr + valStrLo
      else
        outStr := outStr + valStrLo+','+valStrHi;

      if (i < varLen) then outStr := outStr + ',';

      if i < varLen then Expect(Token_comma);
    end;

    Expect(Token_rparen);

    EmitLn(outStr);
  end;
end;

procedure TParserPas6502.StringToSymSubType(aVarType : AnsiString; aSym : TSymInfo);
var
  symType : TSymInfo;
begin
  aVarType := LowerCase(aVarType);

  aSym.SymLength := 1;

  // set variable sub type
  if (aVarType = 'byte') then begin
    aSym.SymSubType := eSymSubType_byte;
    aSym.SymSize    := 1;
  end
  else
  if (aVarType = 'boolean') then begin
    aSym.SymSubType := eSymSubType_boolean;
    aSym.SymSize    := 1;
  end
  else
  if (aVarType = 'pbyte') then begin
    aSym.SymSubType := eSymSubType_pbyte;
    aSym.SymSize    := 2;
  end
  else
  if (aVarType = 'pinteger') then begin
    aSym.SymSubType := eSymSubType_pinteger;
    aSym.SymSize    := 2;
  end
  else
  if (aVarType = 'integer') then begin
    aSym.SymSubType := eSymSubType_integer;
    aSym.SymSize    := 2;
  end
  else begin
    symType := GetSymbol(aVarType);

    if symType = Nil then
      Error('Undeclared type identifier found: "'+aVarType+'"')
    else
    if (symType.symSubType <> eSymSubType_typeRecord) then
      Error('Unknown type found: "'+aVarType+'"');
  end;
end;

procedure TParserPas6502.RegisterVar(aVarName: AnsiString; aVarType: AnsiString; aAddress: AnsiString);
var
  sym : TSymInfo;
begin
  sym  := AddSymbol(aVarName);
  sym.SymIsArray    := False;
  sym.SymType       := eSymType_Var;
  sym.SymIsReadOnly := True;
  sym.SymAddr       := aAddress;

  StringToSymSubType(aVarType,sym);

  if (sym.SymAddr <> '') then
    EmitLn('.var '+sym.SymName+' = '+sym.SymAddr)
  else
    EmitLn(sym.SymName + ': .fill ' + IntToStr(sym.SymSize) + ',0');
end;

procedure TParserPas6502.TypeDecls(DeclarationLocation : TDeclarationLocation);
var
  typeName : AnsiString;
  varType  : AnsiString;
  varName  : AnsiString;
  typeSym  : TSymInfo;
  subSym   : TSymInfo;
begin
  repeat
    typeName            := Token.TokenValue;
    typeName            := FScope + typeName;

    typeSym             := AddSymbol(typeName);
    typeSym.SymSubType  := eSymSubType_typeRecord;
    typeSym.SymTypeName := LowerCase(typeName);

    Expect(Token_ident);

    Expect(Token_eql);

    Expect(Token_record);
      while Token.TokenType <> Token_end do begin
        varName := Token.TokenValue;
        subSym := typeSym.SymRecordVars.AddSymbol(varName);

        Expect(Token_ident);
        Expect(Token_colon);
        varType := Token.TokenValue;

        StringToSymSubType(varType,subSym);

        Expect(Token_ident);
        Expect(Token_semicolon);
      end;

    Expect(Token_end);
    Expect(Token_semicolon);
  until Token.TokenType <> Token_ident;
end;

procedure TParserPas6502.ConstDecls(DeclarationLocation : TDeclarationLocation);
var
  constName : AnsiString;
  i         : Integer;
  sym       : TSymInfo;
  e         : TExpTreeNode;
begin
  repeat
    constName   := Token.TokenValue;

    Expect(Token_ident);

    while Accept(Token_period) do begin
      constName := constName + '.' + Token.TokenValue;
      GetToken;
    end;

    constName := FScope + constName;

    sym               := AddSymbol(constName);
    sym.SymType       := eSymType_Const;
    sym.SymIsReadOnly := True;

    if Accept(Token_colon) then begin
    // constName : type = value
      InitializedConstDecls(sym,DeclarationLocation);

      Expect(Token_semicolon);
    end
    else begin
    // constName = value;
      Expect(Token_eql);

      e := SimplifyTree(expression(true));

      sym.SymValue := e.Data.ToString;

      Expect(Token_semicolon);

      if (DeclarationLocation = dlInterface) then
        EmitLn('.const @'+sym.SymName+' = '+sym.SymValue)
      else
        EmitLn('.const '+sym.SymName+' = '+sym.SymValue);
    end;

  until Token.TokenType in [Token_type,Token_var,Token_begin,Token_implementation,Token_func,Token_proc,Token_rasterirq,Token_end];

  EmitLn('');
end;

procedure TParserPas6502.ProcDecls(DeclarationLocation : TDeclarationLocation);
type
  TProcType = (
    ptNormal,
    ptRasterIRQ,
    ptIRQ
  );
var
  procName : AnsiString;
  sym      : TSymInfo;
  procType : TProcType;
begin
  procName := Token.TokenValue;
  procType := ptNormal;

  sym := AddSymbol(procName);
  sym.SymType := eSymType_proc;

  Expect(Token_ident);

  if Accept(Token_lparen) then
    Expect(Token_rparen);

  Expect(Token_semicolon);

  if Accept(Token_rasterirq) then begin
    procType := ptRasterIRQ;
    Expect(Token_semicolon);
    sym.SymType := eSymType_RasterProc;
  end
  else
  if Accept(Token_interrupt) then begin
    procType := ptIRQ;;
    Expect(Token_semicolon);
    sym.SymType := eSymType_IRQProc;
  end;

  if DeclarationLocation in [dlProgram,dlImplementation] then begin
    SetScope(procName);

    BlockDecls(dlProcFunc);

    Expect(Token_begin);

    EmitLn('');

    EmitLn(LowerCase(procName)+': {');

    if (procType = ptRasterIRQ) then
      EmitCode('irq_start(finished)');

    repeat
      Statement;
      if Accept(Token_semicolon) then begin
        if Token.TokenType <> Token_end then
          Statement;
      end;
    until Token.TokenType = Token_end;

    if (procType = ptRasterIRQ) then
      EmitLabel('finished')
    else
    if (procType = ptIRQ) then begin
      EmitLn('// jmp to default interrupt address');
      EmitCode('jmp stdirq');
    end
    else
      EmitCode('rts');

    SetScope('');
    EmitLn('}');

    Expect(Token_end);
    Expect(Token_semicolon);
  end;
end;

procedure TParserPas6502.FuncDecls(DeclarationLocation : TDeclarationLocation);
var
  funcName   : AnsiString;
  funcResult : AnsiString;
  resultName : AnsiString;
  sym        : TSymInfo;
  resultSym  : TSymInfo;
begin
  funcName   := Token.TokenValue;

  sym := AddSymbol(funcName);
  sym.SymType := eSymType_func;

  Expect(Token_ident);

  if Accept(Token_lparen) then
    Expect(Token_rparen);

  Expect(Token_colon);

  funcResult := Token.TokenValue;

  StringToSymSubType(funcResult,sym);

  if (sym.SymSubType = eSymSubType_typeRecord) then Error('Function can only return primitive types');

  Expect(Token_ident);

  Expect(Token_semicolon);

  if DeclarationLocation in [dlProgram,dlImplementation] then begin
    resultName := sym.SymName + '_result';
    resultSym  := AddSymbol(resultName);
    resultSym.SymSubType := sym.SymSubType;
    resultSym.SymSize    := sym.SymSize;
    resultSym.SymLength  := sym.SymLength;

    EmitLn(resultName+': .fill '+IntToStr(sym.SymSize)+',0');
    EmitLn('');

    SetScope(funcName);

    BlockDecls(dlProcFunc);

    EmitLn(sym.SymName+': {');

    Expect(Token_begin);

    EmitLn('');

    repeat
      Statement;
      if Accept(Token_semicolon) then begin
        if Token.TokenType <> Token_end then
          Statement;
      end;
    until Token.TokenType = Token_end;

    if (sym.SymSize = 1) then
      loadIntegerRegMem8(resultName)
    else
      loadIntegerRegMem(resultName);

    EmitCode('rts');
    EmitLn('}');

    Expect(Token_end);
    Expect(Token_semicolon);

    SetScope('');
  end;
end;

procedure TParserPas6502.Block(DeclarationLocation : TDeclarationLocation; isUnitBlock : Boolean; blockName : AnsiString = '');
begin
  if (blockName <> '') then begin
    if Accept(Token_const) then begin
      ConstDecls(DeclarationLocation);
    end;

    if Accept(Token_var) then begin
      VarDecls(DeclarationLocation);
    end;

    EmitLn('');
  end;

  if (DeclarationLocation = dlInterface) then Exit;

  if (DeclarationLocation = dlProgram) then begin
    EmitLabel(blockName);
    Expect(Token_begin);

    repeat
      Statement;
      if Accept(Token_semicolon) then begin
        if Token.TokenType <> Token_end then
          Statement;
      end;
    until Token.TokenType = Token_end;

    Expect(Token_end);

    EmitCode('rts');
  end;
end;

procedure TParserPas6502.UsesClause(DeclarationLocation : TDeclarationLocation);
var
  unitName : String;
begin
  unitName := LowerCase(Token.TokenValue);
  GetToken;

  ParseUnit(unitName);

  while Accept(Token_comma) do begin
    unitName := LowerCase(Token.TokenValue);
    GetToken;

    ParseUnit(unitName);
  end;

  Expect(Token_semicolon);
end;

procedure TParserPas6502.BlockDecls(DeclarationLocation : TDeclarationLocation);
begin
  repeat
    if Accept(Token_type) then begin
      TypeDecls(DeclarationLocation);
    end;

    if Accept(Token_const) then begin
      ConstDecls(DeclarationLocation);
    end;

    if Accept(Token_var) then begin
      VarDecls(DeclarationLocation);
    end;

    if DeclarationLocation <> dlProcFunc then begin
      if Accept(Token_proc) then begin
        ProcDecls(DeclarationLocation)
      end;

      if Accept(Token_func) then begin
        FuncDecls(DeclarationLocation);
      end;
    end;

  until not (Token.tokenType in [Token_type,Token_const,Token_var,Token_proc,Token_func]);
end;

procedure TParserPas6502.doUnit;
var
  unitName : AnsiString;
begin
  Expect(Token_unit);

  unitName := Token.TokenValue;
  Expect(Token_ident);

  Expect(Token_semicolon);

  Expect(Token_interface);

  if Accept(Token_uses) then
    UsesClause(dlInterface);

  EmitLn(unitName + ': {');

    BlockDecls(dlInterface);

    Expect(Token_implementation);

    BlockDecls(dlImplementation);

    Expect(Token_end);

  EmitLn('}');
  EmitLn('');
  Expect(Token_period);
end;

procedure TParserPas6502.EmitBinFile(aFileName : AnsiString; aAddress : Integer);
var
  binPath  : AnsiString;
  binData  : array of Byte;
  binFile  : TFileStream;
  addr     : AnsiString;

  SymName  : AnsiString;
  sym      : TSymInfo;

  symAddr  : TSymInfo;
  symData  : TSymInfo;
  symSize  : TSymInfo;
begin
  SymName   := Copy(aFileName,1,Pos('|',aFileName) - 1);
  aFileName := Copy(aFileName,Pos('|',aFileName) + 1,Length(aFileName));

  Sym := AddSymbol(SymName);
  Sym.SymIsArray    := False;
  Sym.SymType       := eSymType_Var;
  Sym.SymIsReadOnly := True;
  sym.SymSize       := 1;

  // load binary data and emit as code
  binFile := TFileStream.Create(binPath,fmOpenRead);
  try
    SetLength(binData,binFile.Size);
    binFile.Read(binData[0],binFile.Size);

    symAddr := AddSymbol(Sym.SymName + '.address');
    symAddr.SymIsArray    := False;
    symAddr.SymType       := eSymType_Var;
    symAddr.SymSubType    := eSymSubType_pinteger;
    symAddr.SymIsReadOnly := True;
    symAddr.SymSize       := 2;

    symSize  := AddSymbol(Sym.SymName + '.size');
    symSize.SymIsArray    := False;
    symSize.SymType       := eSymType_Var;
    symSize.SymSubType    := eSymSubType_integer;
    symSize.SymIsReadOnly := True;
    symSize.SymSize       := 2;

    EmitLn(Sym.SymName + ': {');
    EmitCode('.var file = LoadBinary("'+binPath+'")');
    EmitCode('size:    .byte <file.getSize(),>file.getSize()');
    EmitCode('address: .byte <data,>data');
    EmitCode('data:    .fill file.getSize(), file.get(i)');
    EmitLn('}');
    EmitLn('');
  finally
    binFile.Free;
  end;
end;

procedure TParserPas6502.doProgram;
var
  progName : AnsiString;
  sym      : TSymInfo;
  i        : Integer;
begin
  Expect(Token_program);

  progName := Token.TokenValue;

  sym := AddSymbol(progName);
  sym.SymValue := progName;
  sym.SymType  := eSymType_Proc;

  Expect(Token_ident);

  if Accept(Token_lparen) then begin
    Expect(Token_ident);
    while Accept(Token_comma) do begin
      Expect(Token_ident);
    end;
    Expect(Token_rparen);
  end;
  Expect(Token_semicolon);

  {if not FIsCartridge then} begin
    EmitLn(':BasicUpstart2('+progName+')');
    EmitLn('');
    EmitLn('* = $0900 "main code"');
    EmitLn('jmp '+progName);
  end
{  else begin
    EmitLn('* = $80d');
    EmitLn('jmp '+progName+'');
  end};

  EmitLn('');
  EmitLn('.import source "pas6502_rtl.asm"');
  EmitLn('.import source "pas6502_interrupts.asm"');
  EmitLn('.import source "pas6502_joystick.asm"');
  EmitLn('.import source "pas6502_random.asm"');
  EmitLn('');

  RegisterVar('joyUp'    ,'byte','joysticks.up');
  RegisterVar('joyDown'  ,'byte','joysticks.down');
  RegisterVar('joyLeft'  ,'byte','joysticks.left');
  RegisterVar('joyRight' ,'byte','joysticks.right');
  RegisterVar('joyButton','byte','joysticks.button');

  EmitLn('');

  if Accept(Token_uses) then
    UsesClause(dlProgram);

  BlockDecls(dlProgram);

  EmitLn('');
  EmitLn(progName+': {');

  Expect(Token_begin);
    repeat
      Statement;
      if Accept(Token_semicolon) then begin
        if Token.TokenType <> Token_end then
          Statement;
      end;
    until Token.TokenType = Token_end;

  Expect(Token_end);
  EmitCode('rts');
  EmitLn('}');
  EmitLn('');

  for i := 0 to FDeferredBinFiles.Count - 1 do begin
    EmitBinFile(FDeferredBinFiles.Strings[i],Integer(FDeferredBinFiles.Objects[i]));
  end;

  Expect(Token_period);
end;

procedure TParserPas6502.RegisterKeywordTokens;
begin
  inherited RegisterKeywordTokens;

  Token_poke           := RegisterKeywordToken('poke');
  Token_poke16         := RegisterKeywordToken('poke16');
  Token_interrupt      := RegisterKeywordToken('interrupt');
  Token_rasterirq      := RegisterKeywordToken('rasterirq');
  Token_setuprasterirq := RegisterKeywordToken('setuprasterirq');
  Token_setupinterrupt := RegisterKeywordToken('setupinterrupt');
  Token_irqend         := RegisterKeywordToken('irqend');
  Token_uses           := RegisterKeywordToken('uses');
  Token_unit           := RegisterKeywordToken('unit');
  Token_interface      := RegisterKeywordToken('interface');
  Token_implementation := RegisterKeywordToken('implementation');
  Token_loadbinary     := RegisterKeywordToken('loadbinary');
  Token_writeln        := RegisterKeywordToken('writeln');
  Token_clearscreen    := RegisterKeywordToken('clearscreen');
  Token_copydata8      := RegisterKeywordToken('copyData8');
  Token_readjoysticks  := RegisterKeywordToken('readjoysticks');
  Token_exit           := RegisterKeywordToken('exit');
end;

procedure TParserPas6502.ParseInput;
var
  sym : TSymInfo;
begin
  FFloatRegisterIndex := -1;
  FIntRegisterIndex   := -1;
  FIntTypeStackIndex  := -1;
  FScope              := '';

  FDeferredBinFiles.Clear;

  // define functions

  sym := AddSymbol('peek');
  sym.SymType := eSymType_func;

  doProgram;
end;

procedure TParserPas6502.ParseUnit(UnitName : String);
var
  UnitParser   : TBaseParser;
  srcStream    : TFileStream;
  UnitFileName : String;
begin
  UnitFileName := FProjectPath + UnitName + '.pas';

  if not FileExists(UnitFileName) then
    UnitFileName := FParserPath + 'imports\' + UnitName + '.pas';

  if not FileExists(UnitFileName) then Error ('Unit "'+UnitName+'" not found');

  UnitParser := TParserPas6502.Create(FSymbolTable,False);

  UnitParser.ProjectPath := ProjectPath;
  UnitParser.ParserPath  := ParserPath;

  try
    srcStream := TFileStream.Create(UnitFileName,fmOpenRead);

    try
      try
        SaveSrcStreamInfo(FSrcStream,FCharacter,FLineCount,FCharPos,FToken,FEOF);

        FSrcStream := srcStream;

        ResetSrcStreamInfo;

        doUnit;

        RestoreSrcStreamInfo(FSrcStream,FCharacter,FLineCount,FCharPos,FToken,FEOF);
      except
        on E:Exception do
        begin
          Error('Unit "'+UnitFileName+'": '+E.Message,False);
        end;
      end;
    finally
      srcStream.Free;
    end;
  finally
    UnitParser.Free;
  end;
end;

end.

