unit uCodeGen_6502;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  uAST,usesStringWriter;

const
  cA_REGISTER  = '_A_REG_';
  cX_REGISTER  = '_Y_REG_';
  cY_REGISTER  = '_A_REG_';
  cAX_REGISTER = '_AX_REG_';

type
  TCodeGen_6502 = Class
  private
    fPntr : AnsiString;
  protected
    Fwriter      : TsesStringWriter;
    FdoingCondition : Boolean;
    FindentValue : Integer;

    procedure error(aMsg : AnsiString);

    function  indent : AnsiString;

    procedure emitCompare_16(left,right : AnsiString; lower,same,higher : Boolean);

    procedure shiftLeft_0808(src : AnsiString; shift : Byte; dst : AnsiString);
    procedure shiftLeft_0816(src : AnsiString; shift : Byte; dst : AnsiString);
    procedure shiftLeft_1616(src : AnsiString; shift : Byte; dst : AnsiString);
    procedure shiftLeft_08var(src,shift,dst : AnsiString);

    procedure shiftRight_0808(src : AnsiString; shift : Byte; dst : AnsiString);
    procedure shiftRight_0816(src : AnsiString; shift : Byte; dst : AnsiString);
    procedure shiftRight_1616(src : AnsiString; shift : Byte; dst : AnsiString);
    procedure shiftRight_1608(src : AnsiString; shift : Byte; dst : AnsiString);
    procedure shiftRight_08var(src,shift,dst : AnsiString);

    procedure add_080808(src1,src2,dst: AnsiString);
    procedure add_080816(src1,src2,dst: AnsiString);
    procedure add_081616(src1,src2,dst: AnsiString);
    procedure add_161616(src1,src2,dst: AnsiString);

    procedure sub_080808(src1,src2,dst: AnsiString);
    procedure sub_080816(src1,src2,dst: AnsiString);
    procedure sub_081616(src1,src2,dst: AnsiString);
    procedure sub_161616(src1,src2,dst: AnsiString);

    procedure absoluteAddress_read0808(src,dst : AnsiString);
    procedure absoluteAddress_read0816(src,dst : AnsiString);

    procedure absoluteAddress_write0808(src,dst : AnsiString);

    procedure writeIndexed_080808(src,dst,index : AnsiString);
    procedure writeIndexed_081608(src,dst,index : AnsiString);
    procedure writeIndexed_080816(src,dst,index : AnsiString);
    procedure writeIndexed_081616(src,dst,index : AnsiString);
    procedure writeIndexed_161616(src,dst,index : AnsiString);
    procedure writeIndexed_160816(src,dst,index : AnsiString);

    procedure writeIndexedPointer_080808(src,dst,index : AnsiString);
    procedure writeIndexedPointer_081608(src,dst,index : AnsiString);

    procedure readIndexed_080808(src,dst,index : AnsiString);
    procedure readIndexed_080816(src,dst,index : AnsiString);
    procedure readIndexed_081608(src,dst,index : AnsiString);
    procedure readIndexed_081616(src,dst,index : AnsiString);
    procedure readIndexed_161608(src,dst,index : AnsiString);
    procedure readIndexed_161616(src,dst,index : AnsiString);
    procedure readIndexed_160816(src,dst,index : AnsiString);

    procedure readIndexedPointer_080808(src,dst,index : AnsiString);
    procedure readIndexedPointer_081608(src,dst,index : AnsiString);

    procedure readWriteIndexedPointer_080808(src,srcIndex,dst,dstIndex : AnsiString);

    procedure loadReg_08(src,loReg,hiReg : AnsiString);
    procedure loadReg_16(src,loReg,hiReg : AnsiString);

    procedure copy_0808(src,dst : AnsiString);
    procedure copy_0816(src,dst : AnsiString);
    procedure copy_1616(src,dst : AnsiString);
    procedure copy_1608(src,dst : AnsiString);
    procedure copyAddress_1616(src,dst : AnsiString);

    procedure mul_080808(src1,src2,dst: AnsiString);
    procedure mul_080816(src1,src2,dst: AnsiString);

    procedure and_080808(src1,src2,dst: AnsiString);
    procedure and_081616(src1,src2,dst: AnsiString);
    procedure and_161616(src1,src2,dst: AnsiString);
    procedure and_080816(src1,src2,dst: AnsiString);

    procedure or_080808(src1,src2,dst: AnsiString);
    procedure or_081616(src1,src2,dst: AnsiString);
    procedure or_161616(src1,src2,dst: AnsiString);
    procedure or_080816(src1,src2,dst: AnsiString);

    procedure xor_080808(src1,src2,dst: AnsiString);
    procedure xor_081616(src1,src2,dst: AnsiString);
    procedure xor_161616(src1,src2,dst: AnsiString);
    procedure xor_080816(src1,src2,dst: AnsiString);

    procedure neg_0808(right,dst : AnsiString);
    procedure neg_0816(right,dst : AnsiString);
    procedure neg_1616(right,dst : AnsiString);

    procedure not_0808(right,dst : AnsiString);
    procedure not_0816(right,dst : AnsiString);

    procedure eql_080808(src1,src2,dst: AnsiString);
    procedure eql_080816(src1,src2,dst: AnsiString);
    procedure eql_081616(src1,src2,dst: AnsiString);
    procedure eql_161616(src1,src2,dst: AnsiString);
    procedure eql_160816(src1,src2,dst: AnsiString);

    procedure neq_080808(src1,src2,dst: AnsiString);
    procedure neq_080816(src1,src2,dst: AnsiString);
    procedure neq_161616(src1,src2,dst: AnsiString);
    procedure neq_160816(src1,src2,dst: AnsiString);

    procedure lss_080808(src1,src2,dst: AnsiString);
    procedure lss_080816(src1,src2,dst: AnsiString);
    procedure lss_160816(src1,src2,dst: AnsiString);
    procedure lss_161616(src1,src2,dst: AnsiString);

    procedure geq_160808(src1,src2,dst: AnsiString);
    procedure geq_160816(src1,src2,dst: AnsiString);
    procedure geq_161616(src1,src2,dst: AnsiString);
    procedure geq_080816(src1,src2,dst: AnsiString);

    procedure gtr_080808(src1,src2,dst: AnsiString);
    procedure gtr_080816(src1,src2,dst: AnsiString);
    procedure gtr_161616(src1,src2,dst : AnsiString);
    procedure leq_080808(src1,src2,dst: AnsiString);
    procedure leq_080816(src1,src2,dst: AnsiString);
    procedure leq_160816(src1,src2,dst: AnsiString);
    procedure leq_160808(src1,src2,dst: AnsiString);
  public
    constructor Create(aWriter : TsesStringWriter);

    class function getValue_lo(value : AnsiString) : AnsiString;
    class function getValue_hi(value : AnsiString) : AnsiString;

    function  shiftLeft(src,shift,dst : TAST) : Boolean;
    function  shiftRight(src,shift,dst : TAST) : Boolean;
    function  doAdd(left,right,dst : TAST) : Boolean;
    function  doSub(left,right,dst : TAST) : Boolean;
    function  absoluteAddress_write(src,dst : TAST) : Boolean;
    function  absoluteAddress_read(src,dst : TAST) : Boolean;
    function  writeIndexed_value(dst,index,src : TAST) : Boolean;
    function  readIndexed_value(dst,index,src : TAST) : Boolean;
    function  readWriteIndexed_value(dst,dstIndex,src,srcIndex : TAST) : Boolean;
    function  doCopy(src,dst : TAST) : Boolean;
    function  loadRegister(src : TAST; loReg,hiReg : AnsiString) : Boolean;
    function  doMul(left,right,dst : TAST) : Boolean;
    function  doAnd(left,right,dst : TAST) : Boolean;
    function  doOr(left,right,dst : TAST) : Boolean;
    function  doXor(left,right,dst : TAST) : Boolean;
    function  doNeg(right,dst : TAST) : Boolean;
    function  doNot(right,dst : TAST) : Boolean;
    function  doEql(left,right,dst : TAST) : Boolean;
    function  doNeq(left,right,dst : TAST) : Boolean;
    function  doLss(left,right,dst : TAST) : Boolean;
    function  doGtr(left,right,dst : TAST) : Boolean;
    function  doLeq(left,right,dst : TAST) : Boolean;
    function  doGeq(left,right,dst : TAST) : Boolean;

    property indentValue    : Integer                      write FindentValue;
    property doingCondition : Boolean read FdoingCondition write FdoingCondition;
  end;

function  isDerefed(ast : TAST) : Boolean;

implementation

uses
  System.SysUtils,
  uThreeAddressCode;

function  isDerefed(ast : TAST) : Boolean;
begin
  Result := False;

  if not ast.isVariable then Exit;

  Result := TVariable(ast).deref;
end;

constructor TCodeGen_6502.Create(aWriter : TsesStringWriter);
begin
  Fwriter := aWriter;

  fPntr := '$fb';
end;

procedure TCodeGen_6502.error(aMsg: AnsiString);
begin
  raise Exception.Create('CodeGen: '+aMsg);
end;

class function TCodeGen_6502.getValue_lo(value : AnsiString) : AnsiString;
begin
  if value[1] = '!' then
    Result := Copy(value,2,Length(value))
  else
  if value[1] = '^' then
  // is @alue so return the address instead
    Result := value//(Copy(value,2,Length(value))
  else
  if value[1] = '@' then
  // is @alue so return the address instead
    Result := '<'+Copy(value,2,Length(value))
  else
  if (value[1] in ['-','0'..'9','%','$']) then
    Result := '#<'+value
  else
    Result := value + ' + 0';
end;

class function TCodeGen_6502.getValue_hi(value : AnsiString) : AnsiString;
begin
  if value[1] = '!' then
    Result := Copy(value,2,Length(value))
  else
  if value[1] = '@' then
  // is @value so return the address instead
    Result := '>'+Copy(value,2,Length(value))
  else
  if (value[1] in ['0'..'9','%','$']) then
    Result := '#>'+value
  else
    Result := value + ' + 1';
end;

function  TCodeGen_6502.indent : AnsiString;
begin
  Result := StringOfChar(' ',FindentValue);
end;

function  TCodeGen_6502.shiftLeft(src,shift,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s << %s',[dst.toString,src.toString,shift.toString]);

  if (dst.size = 1) and (shift.isInteger) then
    shiftLeft_0808(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
  if (dst.size = 1) and (shift.isVariable) then
    shiftLeft_08var(src.toString,shift.toString,dst.toString)
  else
  if (src.size = 1) and (shift.isInteger) and (dst.size = 2) then
    shiftLeft_0816(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
  if (src.size = 2) and (shift.isInteger) and (dst.size = 2) then
    shiftLeft_1616(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) shl %s(%d-bit)" not supported',[dst.toString,dst.size*8,src.toString,src.size*8,shift.toString,shift.size*8]));

  Result := True;
end;

function  TCodeGen_6502.shiftRight(src,shift,dst : TAST) : Boolean;
begin
  Result := False;

  writeLn(Format('// %s := %s >> %s',[dst.toString,src.toString,shift.toString]));
  Fwriter.writeLn(indent+'// %s := %s >> %s',[dst.toString,src.toString,shift.toString]);

  if (dst.size = 1) and (shift.isInteger) and (src.size = 1) then
    shiftRight_0808(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
  if (dst.size = 1) and (shift.isVariable) then
    shiftRight_08var(src.toString,shift.toString,dst.toString)
  else
  if (src.size = 1) and (shift.isInteger) and (dst.size = 2) then
    shiftRight_0816(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
  if (src.size = 2) and (shift.isInteger) and (dst.size = 2) then
    shiftRight_1616(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
  if (src.size = 2) and (shift.isInteger) and (dst.size = 1) then
    shiftRight_1608(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) shr %s(%d-bit)" not supported',[dst.toString,dst.size*8,src.toString,src.size*8,shift.toString,shift.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doAdd(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s + %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then
    add_080808(left.toString,right.toString,dst.toString)
  else
  if (left.size = 1) and (right.size = 1) and (dst.size = 2) then
    add_080816(left.toString,right.toString,dst.toString)
  else
  if (left.size = 2) and (right.size = 2) and (dst.size = 2) then
    add_161616(left.toString,right.toString,dst.toString)
  else
  if (left.size = 1) and (right.size = 2) and (dst.size = 2) then
    add_081616(left.toString,right.toString,dst.toString)
  else
  if (left.size = 2) and (right.size = 1) and (dst.size = 2) then
    add_081616(right.toString,left.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) + %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doSub(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s - %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then
    sub_080808(left.toString,right.toString,dst.toString)
  else
  if (left.size = 1) and (right.size = 1) and (dst.size = 2) then
    sub_080816(left.toString,right.toString,dst.toString)
  else
  if (left.size = 2) and (right.size = 2) and (dst.size = 2) then
    sub_161616(left.toString,right.toString,dst.toString)
  else
  if (left.size = 1) and (right.size = 2) and (dst.size = 2) then
    sub_081616(left.toString,right.toString,dst.toString)
  else
  if (left.size = 2) and (right.size = 1) and (dst.size = 2) then
    sub_081616(right.toString,left.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) - %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.absoluteAddress_write(src,dst : TAST) : Boolean;
begin
  if (dst.size = 1) then
    absoluteAddress_write0808(src.toString,dst.toString)
   else
    error(Format('"%s(%d-bit) := %s(%d-bit)" not supported',[dst.toString,src.toString,src.size*8]));


  Result := True;
end;

function  TCodeGen_6502.absoluteAddress_read(src,dst : TAST) : Boolean;
begin
  if (dst.size = 1) then
    absoluteAddress_read0808(src.toString,dst.toString)
  else
  if (dst.size = 2) then
    absoluteAddress_read0816(src.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit)" not supported',[dst.toString,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen_6502.writeIndexed_value(dst,index,src : TAST) : Boolean;
var
  c : AnsiString;
  v : Integer;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s[%s] := %s',[dst.toString,index.toString,src.toString]);

  if (src.size = 1) and (src.isString) then begin
    v := charToCBMscreenCode(src.toString[1]);
    src := TNumber.Create(v);
  end;

  if (dst.size = 1) and (index.size = 1) and isDerefed(dst) then begin
//    c := IntToStr(charToCBMscreenCode(src.toString[1]));
    writeIndexedPointer_080808(src.toString,dst.toString,index.toString);
  end
  else
  if (dst.size = 1) and (index.size = 1) and isDerefed(dst) then
    writeIndexedPointer_080808(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 1) and isDerefed(dst) then begin
//    c := IntToStr(charToCBMscreenCode(src.toString[1]));
    writeIndexedPointer_081608(src.toString,dst.toString,index.toString);
  end
  else
  if (dst.size = 2) and (index.size = 1) and isDerefed(dst) then
    writeIndexedPointer_081608(src.toString,dst.toString,index.toString)
  else
{  if (dst.size = 1) and (index.size = 2) and (src.isString) then begin
//    c := IntToStr(charToCBMscreenCode(src.toString[1]));
    writeIndexed_080816(c,dst.toString,index.toString);
  end
  else}
  if (dst.size = 1) and (index.size = 1) and (src.size = 1) then
    writeIndexed_080808(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 2) then
    writeIndexed_160816(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 1) and (src.size = 1) then
    writeIndexed_081608(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 1) and (src.size = 2) then
    writeIndexed_080808(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 1) then
    writeIndexed_080816(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 2) and (src.size = 1) then
    writeIndexed_081616(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 2) and (src.size = 2) then
    writeIndexed_161616(src.toString,dst.toString,index.toString)
  else
    error(Format('"%s(%d-bit)[%s(%d-bit)] := %s(%d-bit)" not supported',[dst.toString,dst.size*8,index.toString,index.size*8,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen_6502.readIndexed_value(dst,index,src : TAST) : Boolean;
var
  c : AnsiString;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s[%s]',[dst.toString,src.toString,index.toString]);

  if (dst.size = 1) and (index.size = 1) and isDerefed(src) then
    readIndexedPointer_080808(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 1) and isDerefed(src) then
    readIndexedPointer_081608(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 1) and (src.size in [0,1]) then
    readIndexed_080808(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 1) and (src.size in [0,1]) then
    readIndexed_081608(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 1) then
    readIndexed_080816(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 2) then
    readIndexed_160816(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 2) and (src.size = 1) then
    readIndexed_081616(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size in [1,2]) and (src.size = 2) then
    readIndexed_161616(src.toString,dst.toString,index.toString)
  else
{  if (dst.size = 2) and (index.size = 1) and (src.size = 2) then
    readIndexed_161608(src.toString,dst.toString,index.toString)
  else}
    error(Format('"%s(%d-bit) := %s(%d-bit)[%s(%d-bit)]" not supported',[dst.toString,dst.size*8,src.toString,src.size*8,index.toString,index.size*8]));

  Result := True;
end;

function  TCodeGen_6502.readWriteIndexed_value(dst,dstIndex,src,srcIndex : TAST) : Boolean;
var
  c : AnsiString;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s[%s] := %s[%s]',[dst.toString,dstIndex.toString,src.toString,srcIndex.toString]);

  if isDerefed(dst) and (dstIndex.size = 1) and isDerefed(src) and (srcIndex.size = 1) then
    readWriteIndexedPointer_080808(src.toString,srcIndex.toString,dst.toString,dstIndex.toString)
  else
    error(Format('"%s(%d-bit)^[%s(%d-bit)] := %s(%d-bit)^[%s(%d-bit)]" not supported',[dst.toString,dst.size*8,dstIndex.toString,dstIndex.size*8,src.toString,src.size*8,srcIndex.toString,srcIndex.size*8]));

  Result := True;
end;

function  isPointer(v : TAST) : boolean;
begin
  Result := False;

  if not v.isVariable then exit;

  Result := TVariable(v).isPointer;
end;

function  TCodeGen_6502.doCopy(src: TAST; dst: TAST) : Boolean;
var
  c      : AnsiString;
  srcStr : AnsiString;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s',[dst.toString,src.toString]);

  if (src.isString) then begin
  // convert value from string to a screencode
    src := TNumber.Create(charToCBMscreenCode(src.toString[1]));
  end;

  if (dst.size = 2) and  isPointer(src) then begin
    copyAddress_1616(src.toString,dst.toString);
  end
  else
  if (dst.size = 1) and (src.size = 1) then begin
    copy_0808(src.toString,dst.toString);
  end
  else
  if (dst.size = 1) and (src.size = 2) then begin
    copy_1608(src.toString,dst.toString);
  end
  else
  if (dst.size = 2) and (src.size = 1) then begin
    copy_0816(src.toString,dst.toString);
  end
  else
  if (dst.size = 2) and (src.size = 2) then begin
    copy_1616(src.toString,dst.toString);
  end
  else
    error(Format('"%s(%d-bit) := %s(%d-bit)" not supported',[dst.toString,dst.size*8,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen_6502.loadRegister(src : TAST; loReg,hiReg : AnsiString) : Boolean;
begin
  Result := False;

  if (src.size = 1) or (hiReg = '') then
    loadReg_08(src.toString,loReg,hiReg)
  else
  if (src.size = 2) and (hiReg <> '') then
    loadReg_16(src.toString,loReg,hiReg)
  else
    error(Format('Register(s) %s,%s := "%s(%d-bit)" not supported',[loReg,hiReg,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doMul(left,right,dst : TAST) : Boolean;
var
  v : Byte;
begin
  if (left.size = 1) and (left.isString) then begin
    v := charToCBMscreenCode(left.toString[1]);
    left := TNumber.Create(v);
  end;

  if (right.size = 1) and (right.isString) then begin
    v := charToCBMscreenCode(right.toString[1]);
    right := TNumber.Create(v);
  end;

  if (left.size = 1) and (right.size = 1) and (dst.size = 1) then
    mul_080808(left.toString,right.toString,dst.toString)
  else
  if (left.size = 1) and (right.size = 1) and (dst.size = 2) then
    mul_080816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) * %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doAnd(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s & %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then
    and_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 2) then
    and_081616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    and_081616(right.toString,left.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    and_161616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    and_080816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) & %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doOr(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s | %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then
    or_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 2) then
    or_081616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    or_081616(right.toString,left.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    or_161616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    or_080816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) | %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doXor(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s ^ %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then //and (left.size = 1) and (right.size = 1) then
    xor_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    xor_161616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    xor_080816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) ^ %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doNeg(right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := neg %s',[dst.toString,right.toString]);

  if (dst.size = 1) then
    neg_0808(right.toString,dst.toString)
  else
  if (dst.size = 2) and (right.size = 1) then
    neg_0816(right.toString,dst.toString)
  else
  if (dst.size = 2) and (right.size = 2) then
    neg_1616(right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := neg %s(%d-bit)" not supported',[dst.toString,dst.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doNot(right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := not %s',[dst.toString,right.toString]);

  if (dst.size = 1) and (right.size = 1) then
    not_0808(right.toString,dst.toString)
  else
  if (dst.size = 2) and (right.size = 1) then
    not_0816(right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := not %s(%d-bit)" not supported',[dst.toString,dst.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doEql(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s = %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and ((left.size = 1) or (right.size = 1)) then
    eql_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    eql_080816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    eql_161616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    eql_160816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 2) then
    eql_081616(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) = %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doNeq(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s <> %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and ((left.size = 1) or (right.size = 1)) then
    neq_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    neq_080816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    neq_161616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    neq_160816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) <> %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doLss(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s < %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and ((left.size = 1) or (right.size = 1)) then
    lss_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    lss_080816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    lss_161616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    lss_160816(left.toString,right.toString,dst.toString)
  else

  error(Format('"%s(%d-bit) := %s(%d-bit) < %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doGtr(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s > %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and (left.size = 1) and (right.size = 1) then
    gtr_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    gtr_080816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    gtr_161616(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) > %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doLeq(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s <= %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    leq_080816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 1) and (left.size = 2) and (right.size = 1) then
    leq_160808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 1) and (left.size = 1) and (right.size = 1) then
    leq_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    leq_160816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) <= %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen_6502.doGeq(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s >= %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and (left.size = 2) and (right.size = 1) then
    geq_160808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    geq_160816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    geq_161616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    geq_080816(left.toString,right.toString,dst.toString)
  else
  error(Format('"%s(%d-bit) := %s(%d-bit) >= %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

procedure TCodeGen_6502.emitCompare_16(left,right : AnsiString; lower,same,higher : Boolean);
// leaves result in A reg (0 = false, 255 = true)
begin
  Fwriter.writeLn(indent + 'lda ' + getValue_hi(left));
  Fwriter.writeLn(indent + 'cmp ' + getValue_hi(right));
  Fwriter.writeLn(indent + 'bne !+');
  Fwriter.writeLn(indent + 'lda ' + getValue_lo(left));
  Fwriter.writeLn(indent + 'cmp ' + getValue_lo(right));
  Fwriter.writeLn('!:');

  if not lower  then Fwriter.writeLn(indent + 'bcc !isFalse+');
  if not higher then Fwriter.writeLn(indent + 'bne !isFalse+');
  if not same   then Fwriter.writeLn(indent + 'beq !isFalse+');

  Fwriter.writeLn('!isTrue:');
  Fwriter.writeLn(indent + 'lda #$ff');
  Fwriter.writeLn(indent + 'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent + 'lda #$00');
  Fwriter.writeLn('!:');
end;

procedure TCodeGen_6502.shiftLeft_0808(src : AnsiString; shift : Byte; dst : AnsiString);
// 8     8      8
// dst := src << shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'asl');
  end;

  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.shiftLeft_0816(src : AnsiString; shift : Byte; dst : AnsiString);
// 16     8      8
// dst := src << shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_lo(src),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_hi(dst)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'asl %s; rol %s',[getValue_lo(dst),getValue_hi(dst)]);
  end;
end;

procedure TCodeGen_6502.shiftLeft_1616(src : AnsiString; shift : Byte; dst : AnsiString);
// 16     16     16
// dst := src << shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_lo(src),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_hi(src),getValue_hi(dst)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'asl %s; rol %s',[getValue_lo(dst),getValue_hi(dst)]);
  end;
end;

procedure TCodeGen_6502.shiftLeft_08var(src,shift,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);
  Fwriter.writeLn(indent+'ldx %s',[getValue_lo(shift)]);
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'asl');
  Fwriter.writeLn(indent+'dex');
  Fwriter.writeLn(indent+'bne !-');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.shiftRight_0808(src : AnsiString; shift : Byte; dst : AnsiString);
// 8      8      8
// dst := src >> shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'lsr');
  end;

  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.shiftRight_0816(src : AnsiString; shift : Byte; dst : AnsiString);
// 16     8      8
// dst := src >> shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_lo(src),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_hi(dst)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'lsr %s; ror %s',[getValue_hi(dst),getValue_lo(dst)]);
  end;
end;

procedure TCodeGen_6502.shiftRight_1616(src : AnsiString; shift : Byte; dst : AnsiString);
// 16     16      8
// dst := src >> shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_lo(src),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_hi(src),getValue_hi(dst)]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'lsr %s; ror %s',[getValue_hi(dst),getValue_lo(dst)]);
  end;
end;

procedure TCodeGen_6502.shiftRight_1608(src : AnsiString; shift : Byte; dst : AnsiString);
const cTEMP = '_TEMP_';
// 8      16      8
// dst := src >> shift
var
  i : Integer;
begin
  Fwriter.writeLn(indent+'lda %s; sta %s+0',[getValue_lo(src),cTEMP]);
  Fwriter.writeLn(indent+'lda %s; sta %s+1',[getValue_hi(src),cTEMP]);

  for i := 1 to shift do begin
    Fwriter.writeLn(indent+'lsr %s+1; ror %s+0',[cTEMP,cTEMP]);
  end;

  Fwriter.writeLn(indent+'lda %s; sta %s',[cTEMP,getValue_lo(dst)]);
end;

procedure TCodeGen_6502.shiftRight_08var(src,shift,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);
  Fwriter.writeLn(indent+'ldx %s',[getValue_lo(shift)]);
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'lsr');
  Fwriter.writeLn(indent+'dex');
  Fwriter.writeLn(indent+'bne !-');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.add_161616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'adc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);

  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'adc %s',[getValue_hi(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.add_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'adc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.add_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'adc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'adc #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.add_081616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'adc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);

  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'adc %s',[getValue_hi(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.sub_161616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);

  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_hi(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.sub_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.sub_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.sub_081616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);

  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_hi(src2)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.absoluteAddress_read0808(src,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda '+getValue_lo(src));
  Fwriter.writeLn(indent+'sta '+getValue_lo(dst));
end;

procedure TCodeGen_6502.absoluteAddress_read0816(src,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda '+getValue_lo(src));
  Fwriter.writeLn(indent+'sta '+getValue_lo(dst));
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta '+getValue_hi(dst));
end;

procedure TCodeGen_6502.absoluteAddress_write0808(src,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda '+getValue_lo(src));
  Fwriter.writeLn(indent+'sta '+getValue_lo(dst));
end;

procedure TCodeGen_6502.writeIndexed_080808(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);
  Fwriter.writeLn(indent+'sta %s,y',[dst]);
end;

procedure TCodeGen_6502.writeIndexed_081608(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);
  Fwriter.writeLn(indent+'sta %s,y',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'iny');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta %s,y',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.writeIndexed_080816(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[dst,getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[dst,getValue_hi(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);
  Fwriter.writeLn(indent+'sta ($fb),y');
end;

procedure TCodeGen_6502.writeIndexed_081616(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[dst,getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[dst,getValue_hi(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);
  Fwriter.writeLn(indent+'sta ($fb),y');
  Fwriter.writeLn(indent+'iny');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta ($fb),y');
end;

procedure TCodeGen_6502.writeIndexed_161616(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[dst,getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[dst,getValue_hi(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);
  Fwriter.writeLn(indent+'sta ($fb),y');
  Fwriter.writeLn(indent+'iny');
  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src)]);
  Fwriter.writeLn(indent+'sta ($fb),y');
end;

procedure TCodeGen_6502.writeIndexed_160816(src: AnsiString; dst: AnsiString; index: AnsiString);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[dst,getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[dst,getValue_hi(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);
  Fwriter.writeLn(indent+'sta ($fb),y');
end;

procedure TCodeGen_6502.writeIndexedPointer_080808(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda %s',[src]);
  Fwriter.writeLn(indent+'sta (%s),y',[dst]);
end;

procedure TCodeGen_6502.writeIndexedPointer_081608(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src)]);
  Fwriter.writeLn(indent+'sta (%s),y',[dst]);
end;

procedure TCodeGen_6502.readWriteIndexedPointer_080808(src,srcIndex,dst,dstIndex : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_lo(srcIndex)]);
  Fwriter.writeLn(indent+'lda (%s),y',[src]);
  Fwriter.writeLn(indent+'ldy %s',[getValue_lo(dstIndex)]);
  Fwriter.writeLn(indent+'sta (%s),y',[dst]);
end;

procedure TCodeGen_6502.readIndexedPointer_080808(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda (%s),y',[src]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.readIndexedPointer_081608(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda (%s),y',[src]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.readIndexed_080808(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda %s,y',[src]);
  Fwriter.writeLn(indent+'sta %s',[dst]);
end;

procedure TCodeGen_6502.readIndexed_081608(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy %s',[getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda %s,y',[src]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;
//CodeGen: "_r_0(16-bit) := screenRow(16-bit)[y(8-bit)]" not supported
procedure TCodeGen_6502.readIndexed_161608(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy %s',[index]);
  Fwriter.writeLn(indent+'lda %s,y; sta %s',[getValue_lo(src),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0;   sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.readIndexed_080816(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[src,getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[src,getValue_hi(index)]);
  Fwriter.writeLn(indent+'lda ($fb),y');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.readIndexed_081616(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[src,getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[src,getValue_hi(index)]);
  Fwriter.writeLn(indent+'lda ($fb),y');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.readIndexed_161616(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[src,getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[src,getValue_hi(index)]);
  Fwriter.writeLn(indent+'lda ($fb),y');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'iny');
  Fwriter.writeLn(indent+'lda ($fb),y');
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.readIndexed_160816(src,dst,index : AnsiString);
begin
  Fwriter.writeLn(indent+'ldy #0');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'lda #<%s; adc %s; sta $fb + 0',[src,getValue_lo(index)]);
  Fwriter.writeLn(indent+'lda #>%s; adc %s; sta $fb + 1',[src,getValue_hi(index)]);
  Fwriter.writeLn(indent+'lda ($fb),y');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.loadReg_08(src,loReg,hiReg : AnsiString);
begin
  Fwriter.writeLn(indent+'ld%s %s',[loReg,getValue_lo(src)]);
end;

procedure TCodeGen_6502.loadReg_16(src,loReg,hiReg : AnsiString);
begin
  Fwriter.writeLn(indent+'ld%s %s',[loReg,getValue_lo(src)]);
  Fwriter.writeLn(indent+'ld%s %s',[hiReg,getValue_hi(src)]);
end;

procedure TCodeGen_6502.copy_0808(src,dst : AnsiString);
begin
  if (src = cA_REGISTER) then
    Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)])
  else
  if (src = cX_REGISTER) then
    Fwriter.writeLn(indent+'stx %s',[getValue_lo(dst)])
  else
  if (src = cY_REGISTER) then
    Fwriter.writeLn(indent+'sty %s',[getValue_lo(dst)])
  else
    Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_lo(src),getValue_lo(dst)]);
end;

procedure TCodeGen_6502.copy_0816(src,dst : AnsiString);
begin
  if (src = cA_REGISTER) then begin
    Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
    Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_hi(dst)]);
  end
  else
  if (src = cX_REGISTER) then begin
    Fwriter.writeLn(indent+'stx %s',[getValue_lo(dst)]);
    Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_hi(dst)]);
  end
  else
  if (src = cY_REGISTER) then begin
    Fwriter.writeLn(indent+'sty %s',[getValue_lo(dst)]);
    Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_hi(dst)]);
  end
  else begin
    Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_lo(src),getValue_lo(dst)]);
    Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_hi(dst)]);
  end;
end;

procedure TCodeGen_6502.copy_1608(src,dst : AnsiString);
begin
  if (src = cAX_REGISTER) then
    Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)])
  else
    Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_lo(src),getValue_lo(dst)]);
end;

procedure TCodeGen_6502.copy_1616(src,dst : AnsiString);
begin
  if (src = cAX_REGISTER) then begin
    Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
    Fwriter.writeLn(indent+'stx %s',[getValue_hi(dst)]);
  end
  else begin
    Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_lo(src),getValue_lo(dst)]);
    Fwriter.writeLn(indent+'lda %s; sta %s',[getValue_hi(src),getValue_hi(dst)]);
  end;
end;

procedure TCodeGen_6502.copyAddress_1616(src,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda #<%s; sta %s',[src,getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #>%s; sta %s',[src,getValue_hi(dst)]);
end;

procedure TCodeGen_6502.mul_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; ldx %s; jsr mul_8x8; lda mul_product_lo; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
end;

procedure TCodeGen_6502.mul_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; ldx %s; jsr mul_8x8; lda mul_product_lo; sta %s; lda mul_product_hi; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst),getValue_hi(dst)]);
end;

procedure TCodeGen_6502.and_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; and %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
end;

procedure TCodeGen_6502.and_081616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; and %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0; and %s; sta %s',[getValue_hi(src2),getValue_hi(dst)]);
end;

procedure TCodeGen_6502.and_161616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; and %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda %s; and %s; sta %s',[getValue_hi(src1),getValue_hi(src2),getValue_hi(dst)]);
end;

procedure TCodeGen_6502.and_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; and %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.or_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; ora %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
end;

procedure TCodeGen_6502.or_081616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; ora %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0; ora %s; sta %s',[getValue_hi(src2),getValue_hi(dst)]);
end;

procedure TCodeGen_6502.or_161616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; ora %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda %s; ora %s; sta %s',[getValue_hi(src1),getValue_hi(src2),getValue_hi(dst)]);
end;

procedure TCodeGen_6502.or_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; ora %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0; ora %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.xor_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; eor %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
end;

procedure TCodeGen_6502.xor_081616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; eor %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0; eor %s; sta %s',[getValue_hi(src2),getValue_hi(dst)]);
end;

procedure TCodeGen_6502.xor_161616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; eor %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda %s; eor %s; sta %s',[getValue_hi(src1),getValue_hi(src2),getValue_hi(dst)]);
end;

procedure TCodeGen_6502.xor_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s; eor %s; sta %s',[getValue_lo(src1),getValue_lo(src2),getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0; sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.neg_0808(right,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(right)]);
  Fwriter.writeLn(indent+'eor $ff');
  Fwriter.writeLn(indent+'clc');
  Fwriter.writeLn(indent+'adc #1');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.neg_0816(right,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(right)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_hi(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.neg_1616(right,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(right)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'sbc %s',[getValue_hi(right)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.not_0808(right,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(right)]);
  Fwriter.writeLn(indent+'eor $ff');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.not_0816(right,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(right)]);
  Fwriter.writeLn(indent+'eor $ff');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.eql_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.eql_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

{
procedure equal; assembler;
asm
  lda #>x
  cmp #>y
  bne !isFalse+
  lda #<x
  cmp #<y
  bne !isFalse+
!isTrue:
  lda #$ff
  rts
!isFalse:
  lda #$00
  rts
!check:
end;

}
procedure TCodeGen_6502.eql_081616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'lda #0');
  Fwriter.writeLn(indent+'cmp %s',[getValue_hi(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.eql_161616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_hi(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.eql_160816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'cmp #0');
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

{
procedure notEqual; assembler;
asm
  lda #>x
  cmp #>y
  bne !isTrue+
  lda #<x
  cmp #<y
  bne !isTrue+
!isFalse:
  lda #$00
  rts
!isTrue:
  lda #$ff
end;
}
procedure TCodeGen_6502.neq_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'beq !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.neq_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'beq !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.neq_161616(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_hi(src2)]);

  Fwriter.writeLn(indent+'beq !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.neq_160816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_hi(src2)]);

  Fwriter.writeLn(indent+'beq !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.lss_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'bcs !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.lss_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'bcs !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.lss_160816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'sbc #0');

  Fwriter.writeLn(indent+'bmi !isTrue+');
  Fwriter.writeLn(indent+'  lda #$00');
  Fwriter.writeLn(indent+'  jmp !+');

  Fwriter.writeLn(indent+'!isTrue:');
  Fwriter.writeLn(indent+'  lda #$ff');
  Fwriter.writeLn(indent+'  jmp !+');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.lss_161616(src1,src2,dst: AnsiString);
begin
  emitCompare_16(src1,src2,true,false,false);
//  Fwriter.writeLn(indent+'sec');
//  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
//  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
//  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
//  Fwriter.writeLn(indent+'sbc %s',[getValue_hi(src2)]);
//
//  Fwriter.writeLn(indent+'bcs !isFalse+');
//  Fwriter.writeLn(indent+'// true');
//  Fwriter.writeLn(indent+'lda #$ff');
//  Fwriter.writeLn(indent+'jmp !+');
//  Fwriter.writeLn('!isFalse:');
//  Fwriter.writeLn(indent+'lda #$00');
//  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.geq_160808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'sbc #0');

  Fwriter.writeLn(indent+'bccLong !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.geq_160816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'sec');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'sbc %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'sbc #0');

  Fwriter.writeLn(indent+'bccLong !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.geq_161616(src1,src2,dst: AnsiString);
begin
  emitCompare_16(src1,src2,false,false,true);
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.geq_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'bcc !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.gtr_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'bcc !isFalse+');
  Fwriter.writeLn(indent+'beq !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.gtr_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'bcc !isFalse+');
  Fwriter.writeLn(indent+'beq !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.gtr_161616(src1,src2,dst : AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_hi(src2)]);
  Fwriter.writeLn(indent+'bne !+');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'bcc !isFalse+ // is lower');
  Fwriter.writeLn(indent+'bne !isTrue+ // is higher');
  Fwriter.writeLn(indent+'beq !isFalse+ // is same');
  Fwriter.writeLn('!isTrue:');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.leq_080808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'gtr !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
end;

procedure TCodeGen_6502.leq_080816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'gtr !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

{
  lda #>x               //MSB of 1st number
  cmp #0                //MSB of 2nd number
  bne check1           //X < Y

  lda #<x               //LSB of 1st number
  cmp #<y               //LSB of 2nd number
  beq !isTrue+  //isEqual            //X = Y

check1:
  bcc !isTrue+  //isLower

  lda #$00
  jmp !+

!isTrue:
  lda #$ff
!:
}
procedure TCodeGen_6502.leq_160816(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'  lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'  cmp #0                //MSB of 2nd number');
  Fwriter.writeLn(indent+'  bne !check1+           //X < Y');

  Fwriter.writeLn(indent+'  lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'  cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'  beq !isTrue+  //isEqual            //X = Y');

  Fwriter.writeLn('!check1:');
  Fwriter.writeLn(indent+'  bcc !isTrue+  //isLower');

  Fwriter.writeLn(indent+'  lda #$00');
  Fwriter.writeLn(indent+'  jmp !+');

  Fwriter.writeLn('!isTrue:');
  Fwriter.writeLn(indent+'  lda #$ff');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

procedure TCodeGen_6502.leq_160808(src1,src2,dst: AnsiString);
begin
  Fwriter.writeLn(indent+'lda %s',[getValue_hi(src1)]);
  Fwriter.writeLn(indent+'bne !isFalse+');
  Fwriter.writeLn(indent+'lda %s',[getValue_lo(src1)]);
  Fwriter.writeLn(indent+'cmp %s',[getValue_lo(src2)]);
  Fwriter.writeLn(indent+'gtr !isFalse+');
  Fwriter.writeLn(indent+'// true');
  Fwriter.writeLn(indent+'lda #$ff');
  Fwriter.writeLn(indent+'jmp !+');
  Fwriter.writeLn('!isFalse:');
  Fwriter.writeLn(indent+'lda #$00');
  Fwriter.writeLn('!:');
  Fwriter.writeLn(indent+'sta %s',[getValue_lo(dst)]);
  Fwriter.writeLn(indent+'sta %s',[getValue_hi(dst)]);
end;

end.
