unit form_options;
//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------
interface
uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Dialogs,
  Vcl.ComCtrls;
type
  TOptionsDialog = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    OKBtn: TButton;
    CancelBtn: TButton;
    OpenDialog: TOpenDialog;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    c64_GroupBox: TGroupBox;
    Label2: TLabel;
    X64File_Edit: TEdit;
    X64FileBrowse_Button: TButton;
    c128_GroupBox: TGroupBox;
    Label5: TLabel;
    X128File_Edit: TEdit;
    c128Browse_Button: TButton;
    vic20_GroupBox: TGroupBox;
    Label7: TLabel;
    vic20File_Edit: TEdit;
    vic20FileBrowse_Button: TButton;
    bbc_GroupBox: TGroupBox;
    Label4: TLabel;
    BBC_EmulatorFile_Edit: TEdit;
    BBCbrowse_Button: TButton;
    atari8Bit_GroupBox: TGroupBox;
    Label6: TLabel;
    Atari8BitFile_Edit: TEdit;
    Atari8BitBrowse_Button: TButton;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    NESfile_Edit: TEdit;
    NESbrowseFile_Button: TButton;
    TabSheet7: TTabSheet;
    GroupBox3: TGroupBox;
    Label3: TLabel;
    AppleIIFile_Edit: TEdit;
    appleBrowseFile_Button: TButton;
    TabSheet8: TTabSheet;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    petFile_Edit: TEdit;
    petBrowseFile_Button: TButton;
    procedure X64FileBrowse_ButtonClick(Sender: TObject);
    procedure BBCbrowse_ButtonClick(Sender: TObject);
    procedure c128Browse_ButtonClick(Sender: TObject);
    procedure Atari8BitBrowse_ButtonClick(Sender: TObject);
    procedure vic20FileBrowse_ButtonClick(Sender: TObject);
    procedure NESbrowseFile_ButtonClick(Sender: TObject);
    procedure appleBrowseFile_ButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
var
  OptionsDialog: TOptionsDialog;
implementation
{$R *.dfm}
procedure TOptionsDialog.vic20FileBrowse_ButtonClick(Sender: TObject);
begin
  OpenDialog.Title    := 'Browse for Vic-20 Emulator file (*.exe)';
  OpenDialog.Filter   := 'Vic-20 Emulator (*.exe)|*.exe';
  OpenDialog.FileName := '*.exe';
  if OpenDialog.Execute then begin
    X64File_Edit.Text := OpenDialog.FileName;
  end;
end;

procedure TOptionsDialog.X64FileBrowse_ButtonClick(Sender: TObject);
begin
  OpenDialog.Title    := 'Browse for C64 Emulator file (*.exe)';
  OpenDialog.Filter   := 'C64 Emulator (*.exe)|*.exe';
  OpenDialog.FileName := '*.exe';
  if OpenDialog.Execute then begin
    X64File_Edit.Text := OpenDialog.FileName;
  end;
end;
procedure TOptionsDialog.BBCbrowse_ButtonClick(Sender: TObject);
begin
  OpenDialog.Title    := 'Browse for BBC Emulator file (*.exe)';
  OpenDialog.Filter   := 'BBC Emulator (*.exe)|*.exe';
  OpenDialog.FileName := '*.exe';
  if OpenDialog.Execute then begin
    BBC_EmulatorFile_Edit.Text := OpenDialog.FileName;
  end;
end;

procedure TOptionsDialog.appleBrowseFile_ButtonClick(Sender: TObject);
begin
  OpenDialog.Title    := 'Browse for Apple II Emulator file (*.exe)';
  OpenDialog.Filter   := 'Apple II Emulator (*.exe)|*.exe';
  OpenDialog.FileName := '*.exe';
  if OpenDialog.Execute then begin
    AppleIIFile_Edit.Text := OpenDialog.FileName;
  end;
end;

procedure TOptionsDialog.Atari8BitBrowse_ButtonClick(Sender: TObject);
begin
  OpenDialog.Title    := 'Browse for Atari 8-Bit Emulator file (*.exe)';
  OpenDialog.Filter   := 'Atari 8-Bit Emulator (*.exe)|*.exe';
  OpenDialog.FileName := '*.exe';
  if OpenDialog.Execute then begin
    Atari8BitFile_Edit.Text := OpenDialog.FileName;
  end;
end;

procedure TOptionsDialog.c128Browse_ButtonClick(Sender: TObject);
begin
  OpenDialog.Title    := 'Browse for C128 Emulator file (*.exe)';
  OpenDialog.Filter   := 'C128 Emulator (*.exe)|*.exe';
  OpenDialog.FileName := '*.exe';
  if OpenDialog.Execute then begin
    X128File_Edit.Text := OpenDialog.FileName;
  end;
end;

procedure TOptionsDialog.NESbrowseFile_ButtonClick(Sender: TObject);
begin
  OpenDialog.Title    := 'Browse for NES Emulator file (*.exe)';
  OpenDialog.Filter   := 'NES Emulator (*.exe)|*.exe';
  OpenDialog.FileName := '*.exe';
  if OpenDialog.Execute then begin
    NESfile_Edit.Text := OpenDialog.FileName;
  end;
end;


end.
