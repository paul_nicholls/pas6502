unit form_findReplace;

interface

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.ComCtrls,
  Vcl.ToolWin,SynEdit,System.Contnrs;

type
  TfindReplaceOption = (froMatchWholeWord,froMatchCase);

  TfindReplaceOptions = set of TfindReplaceOption;

  TFindResult = class
    SelStart  : Integer;
    SelLength : Integer;
  end;

  TfindReplaceDialog = class(TForm)
    results_Label: TLabel;
    frMode_Button: TButton;
    replace_Edit: TEdit;
    ToolBar1: TToolBar;
    matchCase_ToolButton: TToolButton;
    matchWholeWord_ToolButton: TToolButton;
    previousMatch_Button: TButton;
    nextMatch_Button: TButton;
    replace_Button: TButton;
    replaceAll_Button: TButton;
    findTextDelay_Timer: TTimer;
    find_Edit: TComboBox;
    procedure frMode_ButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure matchCase_ToolButtonClick(Sender: TObject);
    procedure matchWholeWord_ToolButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure nextMatch_ButtonClick(Sender: TObject);
    procedure previousMatch_ButtonClick(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure find_EditChange(Sender: TObject);
    procedure replace_ButtonClick(Sender: TObject);
    procedure replaceAll_ButtonClick(Sender: TObject);
    procedure findTextDelay_TimerTimer(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure find_EditKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
    fEditor      : TSynEdit;
    fFindResults : TObjectList;
    fOptions     : TfindReplaceOptions;
    ftextIndex   : Integer;
    fFindHistory : TStringList;

    procedure setEditor(aEditor : TSynEdit);
    function  isWholeWord(text : String; SelStart,SelLength : Integer) : boolean;
    procedure replaceTextAt(SelStart,SelLength : Integer);
    procedure updateLabelCaption;
    procedure addFindResult(findText : TFindResult);
    procedure findAllTextOccurences(isProgressiveSearch : Boolean);
    function  getClosestMatchToCursor(cursorPos : Integer) : Integer;
    function  foundText(var offset : Integer; var findResult : TFindResult) : Boolean;
    procedure clearFindResults;
  protected
    procedure CreateParams(var Params: TCreateParams); override; // ADD THIS LINE!
  public
    { Public declarations }
    procedure setToFind;
    procedure setToFindReplace;

    property Editor : TSynEdit read fEditor write setEditor;
  end;

var
  findReplaceDialog: TfindReplaceDialog;

function isAlphaNum(c : Char) : Boolean;

implementation

{$R *.dfm}

const
  MODE_FIND         = 0;
  MODE_FIND_REPLACE = 1;

  FIND_HISTORY_FILE = 'findHistory.txt';

procedure TfindReplaceDialog.CreateParams(var Params: TCreateParams);
begin
  inherited;
//  Params.Style := Params.Style or WS_BORDER or WS_THICKFRAME;
end;

procedure TfindReplaceDialog.find_EditChange(Sender: TObject);
begin
  findTextDelay_Timer.Enabled := True;
  findAllTextOccurences(true);
end;

procedure TfindReplaceDialog.find_EditKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = Chr(VK_RETURN) then begin
    Key := #0;
    findAllTextOccurences(false);
  end
  else
  if Key = Chr(VK_ESCAPE) then begin
    Key := #0;
    Hide;
    fEditor.SetFocus;
  end;
end;

procedure TfindReplaceDialog.FormActivate(Sender: TObject);
var
  p     : TPoint;
  coord : TBufferCoord;
  word  : String;
begin
  p := fEditor.ClientToScreen(Point(fEditor.Left,fEditor.Top));
  Left := p.X + (fEditor.ClientWidth - Width);
  Top  := 8;//p.Y - Height;//srcCodeMemo.Top - ffindReplace.Height;//Self.Top + 8;

  FocusControl(find_Edit);

  if fEditor.SelText <> '' then
    find_Edit.Text := fEditor.SelText;

  clearFindResults;
  updateLabelCaption;
//  findAllTextOccurences;
end;

procedure TfindReplaceDialog.FormCreate(Sender: TObject);
begin
  fFindResults := TObjectList.Create;
  fFindResults.OwnsObjects := True;

  fOptions := [];

  fFindHistory := TStringList.Create;

  if FileExists(ExtractFilePath(ParamStr(0))+FIND_HISTORY_FILE) then begin
    find_Edit.Items.LoadFromFile(ExtractFilePath(ParamStr(0))+FIND_HISTORY_FILE);
  end;
end;

procedure TfindReplaceDialog.FormDestroy(Sender: TObject);
begin
  clearFindResults;
  fFindResults.Free;
  fFindHistory.Free;
end;

procedure TfindReplaceDialog.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F3) and (ssShift in Shift) then
    previousMatch_ButtonClick(nil)
  else
  if (Key = VK_F3) and not (ssShift in Shift) then
    nextMatch_ButtonClick(nil)
end;

procedure TfindReplaceDialog.FormShow(Sender: TObject);
begin
  if frMode_Button.Tag = MODE_FIND_REPLACE then begin
    setToFindReplace;
  end
  else begin
    setToFind;
  end;

  if matchCase_ToolButton.Down then
    fOptions := fOptions + [froMatchCase]
  else
    fOptions := fOptions - [froMatchCase];

  if matchWholeWord_ToolButton.Down then
    fOptions := fOptions + [froMatchWholeWord]
  else
    fOptions := fOptions - [froMatchWholeWord];

//  if fEditor.SelText <> '' then begin
//    find_Edit.Text := fEditor.SelText;
//    findAllTextOccurences;
//  end;

//  find_Edit.SetFocus;
end;

procedure TfindReplaceDialog.frMode_ButtonClick(Sender: TObject);
begin
  if frMode_Button.Tag = MODE_FIND then begin
    setToFindReplace;
  end
  else begin
    setToFind;
  end;
end;

procedure TfindReplaceDialog.matchCase_ToolButtonClick(Sender: TObject);
begin
  if matchCase_ToolButton.Down then
    fOptions := fOptions + [froMatchCase]
  else
    fOptions := fOptions - [froMatchCase];

  findAllTextOccurences(false);
end;

procedure TfindReplaceDialog.matchWholeWord_ToolButtonClick(Sender: TObject);
begin
  if matchWholeWord_ToolButton.Down then
    fOptions := fOptions + [froMatchWholeWord]
  else
    fOptions := fOptions - [froMatchWholeWord];

  findAllTextOccurences(false);
end;

procedure TfindReplaceDialog.nextMatch_ButtonClick(Sender: TObject);
begin
  if ftextIndex = -1 then Exit;

  if fFindResults.Count = 0 then Exit;

  ftextIndex := (ftextIndex + 1) mod fFindResults.Count;

  fEditor.SelStart  := TfindResult(fFindResults.Items[ftextIndex]).SelStart;
  fEditor.SelLength := TfindResult(fFindResults.Items[ftextIndex]).SelLength;

  updateLabelCaption;
end;

procedure TfindReplaceDialog.previousMatch_ButtonClick(Sender: TObject);
begin
  if ftextIndex = -1 then Exit;

  if fFindResults.Count = 0 then Exit;

  ftextIndex := (ftextIndex - 1);

  if fTextIndex = -1 then
    fTextIndex := fFindResults.Count - 1;

  fEditor.SelStart  := TfindResult(fFindResults.Items[ftextIndex]).SelStart;
  fEditor.SelLength := TfindResult(fFindResults.Items[ftextIndex]).SelLength;

  updateLabelCaption;
end;

function isAlphaNum(c : Char) : Boolean;
begin
  Result := (c in ['_','a'..'z','A'..'Z','0'..'9']);
end;

function  TfindReplaceDialog.isWholeWord(text : String; SelStart,SelLength : Integer) : boolean;
var
  c : Char;
begin
  Result := False;

  if SelStart > 1 then begin
    // check previous character for '_', letters or digits
    c := text[SelStart-1];

    if isAlphaNum(c) then Exit;
  end;

  if SelStart + SelLength < Length(text) then begin
    // check next character for '_', letters or digits
    c := text[SelStart+SelLength];

    if isAlphaNum(c) then Exit;
  end;

  Result := True;
end;

procedure TfindReplaceDialog.updateLabelCaption;
begin
  if fFindResults.Count = 0 then begin
    results_Label.Caption := 'No results';
    results_Label.Font.Color := clRed;
  end
  else begin
    results_Label.Caption := IntToStr(ftextIndex+1) + ' of ' + IntToStr(fFindResults.Count);
    results_Label.Font.Color := clBlack;
  end;
end;

procedure TfindReplaceDialog.replaceTextAt(SelStart,SelLength : Integer);
begin
  fEditor.SelStart  := SelStart;
  fEditor.SelLength := SelLength;

  fEditor.SelText := replace_Edit.Text;
end;

procedure TfindReplaceDialog.replaceAll_ButtonClick(Sender: TObject);
var
  i : Integer;
  f : TFindResult;
  text : String;
begin
  fEditor.BeginUpdate;

  for i := ffindResults.Count - 1 downto 0 do begin
    f := TfindResult(ffindResults.Items[i]);

    replaceTextAt(f.SelStart,f.SelLength);
  end;
  fEditor.EndUpdate;

  findAllTextOccurences(false);
end;

procedure TfindReplaceDialog.replace_ButtonClick(Sender: TObject);
var
  text : String;
begin
  if fEditor.ReadOnly then Exit;

  if fEditor.SelText = '' then Exit;

  if replace_Edit.Text = '' then Exit;

  replaceTextAt(fEditor.SelStart,fEditor.SelLength);

  findAllTextOccurences(false);
end;

procedure TfindReplaceDialog.clearFindResults;
begin
  fFindResults.Clear;
end;

function  TfindReplaceDialog.getClosestMatchToCursor(cursorPos : Integer) : Integer;
var
  i        : Integer;
  d        : Integer;
  minIndex : Integer;
  minD     : Integer;
begin
  minIndex := -1;
  minD     := 10000000;

  if fFindResults.Count = 0 then Exit;

  for i  := 0 to fFindResults.Count - 1 do begin
    d := Abs(TFindResult(fFindResults.Items[i]).SelStart - cursorPos);

    if d <= minD then begin
      minD     := d;
      minIndex := i;
    end;
  end;

  Result := minIndex;
end;

function  TfindReplaceDialog.foundText(var offset : Integer; var findResult : TFindResult) : Boolean;
var
  p    : Integer;
  text : String;
begin
  Result := False;

  text := fEditor.Text;

  if not (froMatchCase in fOptions) then
    p := Pos(Uppercase(find_Edit.Text),Uppercase(text),offset)
  else
    p := Pos(find_Edit.Text,text,offset);

  if p = 0 then Exit;

  Result := True;

  findResult := nil;

  if not(froMatchWholeWord in fOptions) then begin
    findResult := TFindResult.Create;
    findResult.SelStart  := p-1;
    findResult.SelLength := Length(find_Edit.Text);
  end
  else if isWholeWord(text,p,Length(find_Edit.Text)) then begin
    findResult := TFindResult.Create;
    findResult.SelStart  := p-1;
    findResult.SelLength := Length(find_Edit.Text);
  end;

  offset := p + Length(find_Edit.Text);
end;

procedure TfindReplaceDialog.addFindResult(findText : TFindResult);
begin
  fFindResults.add(findText);
end;

procedure TfindReplaceDialog.findAllTextOccurences(isProgressiveSearch : Boolean);
var
  foundResult : TFindResult;
  offset      : Integer;
  cursorPos   : Integer;
  msg         : WideString;
begin
  clearFindResults;

  offset     := 1;
  ftextIndex := -1;

  if find_Edit.Text = '' then Exit;

  if Length(find_Edit.Text) = 1 then begin
    Exit;
    msg := 'Such a short search ("'+find_Edit.Text+'") could take ages, are you sure?';

    if Application.MessageBox(PWideChar(msg), 'Warning!', MB_OKCANCEL) = IDCANCEL then
      Exit;
  end;

  // save cursor
  cursorPos := fEditor.SelStart;

  while foundText(offset,foundResult) do begin
    if foundResult <> nil then
      addFindResult(foundResult);
  end;

  if fFindResults.Count = 0 then begin
    results_Label.Caption := 'No results';
    fEditor.SelStart  := cursorPos;
    fEditor.SelLength := 0;
  end
  else begin
    offset := getClosestMatchToCursor(cursorPos);

    if offset <> -1 then begin
      ftextIndex        := offset;

      fEditor.SelStart  := TfindResult(fFindResults.Items[offset]).SelStart;
      fEditor.SelLength := TfindResult(fFindResults.Items[offset]).SelLength;
    end;

    // only add the search if it was found!
    if not isProgressiveSearch and (find_Edit.items.IndexOf(find_Edit.Text) = -1) then begin
    // add search if not in list
      find_Edit.Items.Insert(0,find_Edit.Text);
    end;

  end;

  updateLabelCaption;
end;

procedure TfindReplaceDialog.findTextDelay_TimerTimer(Sender: TObject);
begin
  findTextDelay_Timer.Enabled := False;

//  findAllTextOccurences;
end;

procedure TfindReplaceDialog.setToFind;
begin
  // goto find mode
  frMode_Button.Caption     := '>';
  frMode_Button.Tag         := MODE_FIND;
  replace_Edit.Visible      := False;
  replace_Button.Visible    := False;
  replaceAll_Button.Visible := False;
  ClientHeight              := frMode_Button.Top + frMode_Button.Height + 8;

  Caption := 'Find Text';
end;

procedure TfindReplaceDialog.setToFindReplace;
begin
  if fEditor.ReadOnly then begin
  // can't do replace, only find
    setToFind;
    Exit;
  end;

  // goto find replace mode
  frMode_Button.Caption     := 'v';
  frMode_Button.Tag         := MODE_FIND_REPLACE;
  replace_Edit.Visible      := True;
  replace_Button.Visible    := True;
  replaceAll_Button.Visible := True;
  ClientHeight              := replace_Edit.Top + replace_Edit.Height + 8;

  Caption := 'Find and replace Text';
end;

procedure TfindReplaceDialog.setEditor(aEditor : TSynEdit);
var
  p : TPoint;
begin
  fEditor := aEditor;

{  if not assigned(fEditor) then Exit;

  p := ScreenToClient(fEditor.ClientToScreen(Point(fEditor.Left,fEditor.Top)));
  Left := p.X + (fEditor.ClientWidth - fEditor.Width);
  Top  := p.Y;  }
end;

end.
