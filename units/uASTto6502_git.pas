unit uASTto6502;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  System.Classes,uToken,uAST,uSymbolTable,usesStringWriter,uThreeAddressCode,
  uCodeGen_6502;

const
  cMAX_TEMP_REGS_VARIABLE = 3;
  cTEMP_REGS_VARIABLE     = '_TEMP_REG_VAR_';
  cTEMP_IF_VARIABLE       = '_TEMP_IF_VAR';

type
  TExpToken = record
    token : TToken;
    size  : Integer;
  end;

  TTokenList = array of TExpToken;

  TASTto6502 = class(TInterfacedObject,IASTVisitor)
  private
    FlabelIndex        : Integer;
    Fwriter            : TsesStringWriter;
    FasmStream         : TStream;
    FcurrentScope      : TscopedSymbolTable;

    FcodeGen           : TCodeGen_6502;

    FflattenExpression : Boolean;
    Fexpression        : TList;

    FfillExpTokens     : Boolean;
    FexpTokens         : TTokenList;
    FblockIRQType      : TTokenType;
    FfoundEndIrq: Boolean;
    FloopExitLabelCount : Integer;
    FloopExitLabels: array[0..20] of AnsiString;

    procedure saveRegVarInfo(src : TAST; loReg,hiReg : AnsiString);
    procedure emitSaveRegVars();
    procedure resetTokens;
    procedure resetExpression;
    procedure pushToken(aToken : TToken; aSize : Integer);
    procedure pushExpNode(node : TAST);
    function  newLabel : AnsiString;
    procedure emitLabel(aLabel : AnsiString);
    procedure emitCode(aCode : AnsiString);  overload;
    procedure emitCode(aTacList : TTACList); overload;

    function  flattenExpression(node : TAST; aExpression : TList = nil) : TList;
    procedure debugExpression;

    procedure tokenizeExpression(node : TAST; aResetTokens : Boolean);
    procedure emitExpression(node : TAST; falseLabel : AnsiString = '');

    procedure emitCompare_16(left,right : TExpToken; lower,same,higher : Boolean; falseLabel : AnsiString);

    procedure emitVarDecl(varDecl : TVarDecl);
    procedure emitBinOp(left,right,op : TExpToken);
    procedure emitBinOpCondition(falseLabel : AnsiString);
    procedure emitCondition(expr : TAST; falseLabel : AnsiString);
    function  isSimpleAdd2(node : TAssign; symSize : Integer) : Boolean;
    function  isSimpleSub2(node : TAssign; symSize : Integer) : Boolean;
    function  isSimpleIncrement(node : TAssign; symSize : Integer) : Boolean;
    function  isSimpleDecrement(node : TAssign; symSize : Integer) : Boolean;
    function  isSHL(node : TAssign; var shiftCount : Integer) : Boolean;
    function  isSHR(node : TAssign; var shiftCount : Integer) : Boolean;
    procedure doWrite(node : TBuiltinProcedure; carridgeReturn : Boolean);
    function  isSimpleReadWriteIndexedPointer(aSrc,aDst : TAST) : Boolean;
    procedure pushLoopExitLabel(exitLabel : AnsiString);
    procedure popLoopExitLabel;
    function  peekLoopExitLabel : AnsiString;
//----------------------------
// AST Visitor methods
//----------------------------
    procedure visitProgram  (node : TProgram);
    procedure visitBlock    (node : TBlock);
    procedure visitAsmBlock (node : TAsmBlock);
    procedure visitUnitBlock(node : TUnitBlock);
    procedure visitPoke     (node : TPoke);
    procedure visitAssign   (node : TAssign);
    procedure visitCall     (node : TCall);
    procedure visitMacroCall(node : TMacroCall);
    procedure visitFuncCall (node : TFuncCall);
    procedure visitIfThen   (node : TIfThen);
    procedure visitWhile    (node : TWhile);
    procedure visitFor      (node : TFor);
    procedure visitNumber   (node : TNumber);
    procedure visitVariable (node : TVariable);
    procedure visitString   (node : TString);
    procedure visitBinOp    (node : TBinOp);
    procedure visitUnaryOp  (node : TUnaryOp);
    procedure visitCompound (node : TCompound);
    procedure visitAsmCompound(node : TAsmCompound);
    procedure visitVarDecl  (node : TVarDecl);
    procedure visitConstDecl(node : TConstDecl);
    procedure visitProcDecl (node : TProcDecl);
    procedure visitMacroDecl(node : TMacroDecl);
    procedure visitFuncDecl (node : TFuncDecl);
    procedure visitBuiltinProcedure(node : TBuiltinProcedure);
    procedure visitImportBinary(node : TImportBinary);
    procedure visitImportSource(node : TImportSource);
    procedure visitExit(node : TExit);
    procedure visitBreak(node: TBreak);
    procedure visitSetupRasterIRQ(node : TSetupRasterIRQ);
    procedure visitEndIRQ(node : TEndIRQ);

    procedure emitStackInfo;
    procedure generateIndexShiftTAC(aIndex : TAST; aTacList : TTACList);
    procedure generateTAC(node : TAST; aTacList : TTACList);           overload;
    function  generateTAC(node : TAST; dstVar : TVariable) : TTACList; overload;
//-----------------------------
  public
    constructor Create;
    destructor  Destroy; override;

    procedure genCode(aNode : TAST; aAsmStream : TStream);
  end;

  TTempRegVar = class(TInterfacedObject)
  public
    src   : TAST;
    loReg : AnsiString;
    hiReg : AnsiString;

    constructor create(src : TAST; loReg,hiReg : AnsiString);
  end;

implementation

uses
  System.SysUtils,uASM;

const
  MAIN_LABEL  = '_main_';
  START_LABEL = '_start_';

var
  indentValue    : Integer = 0;

  tempRegVarIndex : Byte = 0;

  tempRegVars : array of TTempRegVar;

procedure resetTempRegVar();
var
  i : Integer;
begin
  for i := 0 to tempRegVarIndex - 1 do begin
    tempRegVars[i] := nil;
  end;

  SetLength(tempRegVars,0);
  tempRegVarIndex := 0;
end;

function getTempRegVar() : AnsiString;
begin
  Result := cTEMP_REGS_VARIABLE + IntToStr(tempRegVarIndex);
  Inc(tempRegVarIndex);
end;

procedure decIndent;
begin
  dec(indentValue,2);
end;

procedure incIndent;
begin
  inc(indentValue,2);
end;

procedure setIndent(v : Integer);
begin
  indentValue := v;
end;

function  indent : AnsiString;
begin
  Result := StringOfChar(' ',indentValue);
end;

function  leafNodeToImmOrAddr(node : TExpToken) : AnsiString;
begin
  if (node.token.tType = ttIntNum) then begin
    // is number/immediate
    Result := '#'+node.token.tValue;
  end else begin
    // is variable/absolute
    Result := node.token.tValue;
  end;
end;

function  getLeafNodeLo(node : TExpToken) : AnsiString;
begin
  if (node.token.tType = ttIntNum) then begin
    // is number/immediate
    Result := '#<'+node.token.tValue;
  end else begin
    // is variable/absolute
    Result := node.token.tValue + ' + 0';
  end;
end;

function  getLeafNodeHi(node : TExpToken) : AnsiString;
begin
  if node.size > 1 then begin
    if (node.token.tType = ttIntNum) then begin
      // is number/immediate
      Result := '#>'+node.token.tValue;
    end else begin
      // is variable/absolute
      Result := node.token.tValue + ' + 1';
    end;
  end
  else
  //  return 0 as no high byte
    Result := '#0';
end;

constructor TTempRegVar.create(src: TAST; loReg: AnsiString; hiReg: AnsiString);
begin
  self.src   := src;
  self.loReg := loReg;
  self.hiReg := hiReg;
end;

constructor TASTto6502.Create;
begin
  FcurrentScope       := nil;
  Fexpression         := TList.Create;
  FlabelIndex         := 0;
  FloopExitLabelCount := 0;

  FfillExpTokens      := False;
  FflattenExpression  := False;
end;

destructor  TASTto6502.Destroy;
begin
  Fexpression.Free;
end;

procedure TASTto6502.saveRegVarInfo(src : TAST; loReg,hiReg : AnsiString);
var
  i : Integer;
begin
  i := Length(tempRegVars);
  SetLength(tempRegVars,i + 1);
  tempRegVars[i] := TTempRegVar.create(src,loReg,hiReg);
end;

procedure TASTto6502.emitSaveRegVars();
var
  i : Integer;
begin
  for i := 0 to tempRegVarIndex - 1 do begin
    FcodeGen.loadRegister(tempRegVars[i].src,tempRegVars[i].loReg,tempRegVars[i].hiReg);
    tempRegVars[i] := nil;
  end;
end;

procedure TASTto6502.resetTokens;
begin
  SetLength(FexpTokens,0);
end;

procedure TASTto6502.resetExpression;
begin
  Fexpression.Clear;
end;

procedure TASTto6502.pushToken(aToken : TToken; aSize : Integer);
var
  i : Integer;
begin
  i := Length(FexpTokens);
  SetLength(FexpTokens,i + 1);
  FexpTokens[i].token := aToken;
  FexpTokens[i].size  := aSize;
end;

procedure TASTto6502.pushExpNode(node : TAST);
begin
  Fexpression.Add(node);
end;

function  TASTto6502.newLabel : AnsiString;
begin
  Result := 'L'+IntToStr(FlabelIndex);
  Inc(FlabelIndex);
end;

procedure TASTto6502.emitLabel(aLabel : AnsiString);
begin
  decIndent;
  Fwriter.writeLn(indent+aLabel+':');
  incIndent;
end;

procedure TASTto6502.emitCode(aCode : AnsiString);
begin
  Fwriter.writeLn(indent+aCode);
end;

procedure TASTto6502.emitCode(aTacList : TTACList);
var
  i          : Integer;
  node       : TTACNode;
  done       : Boolean;
  reg        : AnsiString;
  funcResult : TAST;
begin
  FcodeGen.indentValue := indentValue;

  for i := 0 to aTacList.Count - 1 do begin
    done := False;
    node := aTacList.Items[i];

    if TVariable(node.left).isRegister then begin
      // dst is a register so substitute with temp var first
      reg := getTempRegVar();
      TVariable(node.left).value        := reg;
      TVariable(node.left).token.tValue := reg;
    end;

    case node.getOp of
      ttShl   : done := FcodeGen.shiftLeft(node.right1,node.right2,node.left);
      ttShr   : done := FcodeGen.shiftRight(node.right1,node.right2,node.left);
      ttPlus  : done := FcodeGen.doAdd(node.right1,node.right2,node.left);
      ttMinus : done := FcodeGen.doSub(node.right1,node.right2,node.left);
      ttAnd   : done := FcodeGen.doAnd(node.right1,node.right2,node.left);
      ttOr    : done := FcodeGen.doOr(node.right1,node.right2,node.left);
      ttXor   : done := FcodeGen.doXor(node.right1,node.right2,node.left);
      ttNeg   : done := FcodeGen.doNeg(node.right2,node.left);
      ttNot   : done := FcodeGen.doNot(node.right2,node.left);
      ttEql   : done := FcodeGen.doEql(node.right1,node.right2,node.left);
      ttNeq   : done := FcodeGen.doNeq(node.right1,node.right2,node.left);
      ttLss   : done := FcodeGen.doLss(node.right1,node.right2,node.left);
      ttGtr   : done := FcodeGen.doGtr(node.right1,node.right2,node.left);
      ttLeq   : done := FcodeGen.doLeq(node.right1,node.right2,node.left);
      ttGeq   : done := FcodeGen.doGeq(node.right1,node.right2,node.left);
      ttTimes : done := FcodeGen.doMul(node.right1,node.right2,node.left);
    end;

    if done and TVariable(node.left).isRegister then begin
      // load register(s) with source
      saveRegVarInfo(node.left,TVariable(node.left).loReg,TVariable(node.left).hiReg);
//      FcodeGen.loadRegister(node.left,TVariable(node.left).loReg,TVariable(node.left).hiReg);
    end;

    if not done then begin
      if (node.op = nil) then begin
             if (node.leftIndex <> nil) and (node.rightIndex <> nil) then done := FcodeGen.readWriteIndexed_value(node.left,node.leftIndex,node.right1,node.rightIndex)
        else if (node.leftIndex <> nil)                              then done := FcodeGen.writeIndexed_value(node.left,node.leftIndex,node.right1)
        else if (node.rightIndex <> nil)                             then done := FcodeGen.readIndexed_value(node.left,node.rightIndex,node.right1)
        else begin
          if TVariable(node.left).isRegister then begin
            // load register(s) with source
           saveRegVarInfo(node.right1,TVariable(node.left).loReg,TVariable(node.left).hiReg);
//            done := FcodeGen.loadRegister(node.right1,TVariable(node.left).loReg,TVariable(node.left).hiReg);
          end
          else begin
            if node.right1 is TFuncCall then begin
              visitFuncCall(TFuncCall(node.right1));
              if TFuncCall(node.right1).funcType = ttAssembler then begin
                if TFuncCall(node.right1).size = 1 then
                  funcResult := TVariable.Create(uCodeGen_6502.cA_REGISTER,TFuncCall(node.right1).size)
                else
                  funcResult := TVariable.Create(uCodeGen_6502.cAX_REGISTER,TFuncCall(node.right1).size)
              end
              else
                funcResult := TVariable.Create(TFuncCall(node.right1).funcName+'.result',node.right1.size);

              done := FcodeGen.copy(funcResult,node.left);
            end
            else
              done := FcodeGen.copy(node.right1,node.left)
          end;
        end;
      end else begin
        emitCode('Maths op: "'+node.op.toString+'" not supported yet...');
      end;
    end;
  end;
end;

procedure emitAdd(writer : TsesStringWriter; left,right: TExpToken);
begin
  writer.writeLn(indent+'clc');
  writer.writeLn(indent+'lda '+leafNodeToImmOrAddr(left));
  writer.writeLn(indent+'adc '+leafNodeToImmOrAddr(right));
end;

procedure emitSub(writer : TsesStringWriter; left,right: TExpToken);
begin
  writer.writeLn(indent+'sec');
  writer.writeLn(indent+'lda '+leafNodeToImmOrAddr(left));
  writer.writeLn(indent+'sbc '+leafNodeToImmOrAddr(right));
end;

//----------------------------
// AST Visitor methods
//----------------------------
procedure TASTto6502.emitStackInfo;
var
  i : Integer;
begin
  for i := 0 to MAX_TEMP - 1 do begin
    Fwriter.writeLn('%s%d: .byte 0,0',[TEMP_REG,i]);
  end;

  Fwriter.writeLn();
//
//  Fwriter.writeLn('_stack_backup_: .fill %d,0',[MAX_TEMP*2]);
//
//  Fwriter.writeLn();
//
//  Fwriter.writeLn('%sindex: .byte 0',[TEMP_NAME]);
//  Fwriter.write(TEMP_NAME+'lo: .byte ');
//
//  for i := 0 to MAX_TEMP - 1 do begin
//    Fwriter.write('0');
//
//    if i < (MAX_TEMP - 1) then Fwriter.write(',');
//  end;
//  Fwriter.writeLn();
//
//  Fwriter.write(TEMP_NAME+'hi: .byte ');
//
//  for i := 0 to MAX_TEMP - 1 do begin
//    Fwriter.write('0');
//
//    if i < (MAX_TEMP - 1) then Fwriter.write(',');
//  end;
//
//  Fwriter.writeLn();
//  Fwriter.writeLn();
//  Fwriter.writeLn('.macro saveStack() {');
//  Fwriter.writeLn('  .for(var x = 0; x < %d; x++) {',[MAX_TEMP*2]);
//  Fwriter.writeLn('    lda %s0+x',[TEMP_NAME]);
//  Fwriter.writeLn('    sta _stack_backup_+x');
//  Fwriter.writeLn('  }');
//  Fwriter.writeLn('}');
//  Fwriter.writeLn();
//  Fwriter.writeLn('.macro restoreStack() {');
//  Fwriter.writeLn('  .for(var x = 0; x < %d; x++) {',[MAX_TEMP*2]);
//  Fwriter.writeLn('    lda _stack_backup_+x');
//  Fwriter.writeLn('    sta %s0+x',[TEMP_NAME]);
//  Fwriter.writeLn('  }');
//  Fwriter.writeLn('}');
  Fwriter.writeLn(FUNC_RESULT_NAME+'index: .byte 0');
  Fwriter.write(FUNC_RESULT_NAME+': .byte ');

  for i := 0 to MAX_FUNC_RESULT - 2 do begin
    Fwriter.write('0,');
  end;
  Fwriter.writeLn('0');
end;

procedure TASTto6502.visitProgram(node : TProgram);
var
  i       : Integer;
  binFile : AnsiString;
begin
  FcurrentScope := node.scope;

  if (node.loadAddress <> '') then begin
    if (node.target in [ttBBC]) then begin
      binFile := StringReplace(UpperCase(node.fileName),' ','',[rfReplaceAll]);

//      Fwriter.writeLn(Format('.segment %s [start=$'+node.loadAddress+',outPrg="%s", allowOverlap]',[node.name,binFile]));
//      Fwriter.writeLn(Format('.segment %s [start=$'+node.loadAddress+',outBin="%s\myfiles\%s", allowOverlap]',[node.name,binFile,binFile]));
      Fwriter.writeLn();

      Fwriter.writeLn('* = $'+node.loadAddress+' "main code"');
      Fwriter.writeLn;

      // no basic upstart, only simple jump
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
    end else begin
      Fwriter.writeLn(Format('.segment %s [start=$'+node.loadAddress+',outPrg="%s.prg", allowOverlap]',[node.name,node.fileName]));
      Fwriter.writeLn();

      Fwriter.writeLn('* = $'+node.loadAddress+' "main code"');
      Fwriter.writeLn;

      // no basic upstart, only simple jump
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
    end;
  end
  else begin
    if (node.target = ttC64) then begin
      Fwriter.writeLn(Format('.segment %s [start=$0801,outPrg="%s.prg", allowOverlap]',[node.name,node.fileName]));
      Fwriter.writeLn();

//      Fwriter.writeLn(':BasicUpstart2(%s_%s)',[node.name,MAIN_LABEL]);
      Fwriter.writeLn(':BasicUpstart2(%s)',[START_LABEL]);
      Fwriter.writeLn;
      Fwriter.writeLn('* = $080d "main code"');
      Fwriter.writeLn(START_LABEL+':');
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
      Fwriter.writeLn;
    end
    else
    if (node.target in [ttBBC]) then begin
      binFile := StringReplace(UpperCase(node.fileName),' ','',[rfReplaceAll]);

//      Fwriter.writeLn(Format('.segment %s [start=$'+node.loadAddress+',outPrg="%s", allowOverlap]',[node.name,binFile]));
//      Fwriter.writeLn();

      Fwriter.writeLn('* = $'+node.loadAddress+' "main code"');
      Fwriter.writeLn;

      // no basic upstart, only simple jump
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
    end;
  end;

  setIndent(0);

  // do source file imports "units"
  for i := 0 to node.imports.Count - 1 do
    Fwriter.writeLn('.import source "'+node.imports.Strings[i]+'"');

  Fwriter.writeLn();

  // do all binary imports
  for i := 0 to node.binImports.Count - 1 do
    TAST(node.binImports.Items[i]).acceptVisitor(self);

  // visit all unit included declarations
  for i := 0 to node.unitBlocks.Count - 1 do
    TAST(node.unitBlocks.Items[i]).acceptVisitor(self);

  Fwriter.writeLn(indent+node.name+':');
  incIndent;

  Fwriter.writeLn();

  // visit program block
  node.block.acceptVisitor(self);

  emitCode('rts');
  decIndent;
//  Fwriter.writeLn(indent+'}');

  decIndent;

  FcurrentScope := node.scope.enclosingScope;

  Fwriter.writeLn();
  Fwriter.writeLn('// temp variables, etc.');

  for i := 0 to cMAX_TEMP_REGS_VARIABLE - 1 do begin
  // temp register variables for register params
    Fwriter.writeLn(cTEMP_REGS_VARIABLE+IntToStr(i)+': .byte 0,0');
  end;

  Fwriter.writeLn(cTEMP_IF_VARIABLE  +': .byte 0,0');

  emitStackInfo;

  // do all deferred binary imports
  for i := 0 to node.deferredBinImports.Count - 1 do
    TAST(node.deferredBinImports.Items[i]).acceptVisitor(self);
end;

procedure TASTto6502.visitBlock(node : TBlock);
var
  i   : Integer;
begin
  if not node.isMacro then begin
    for i := 0 to node.declarations.Count - 1 do begin
      TAST(node.declarations.Items[i]).acceptVisitor(self);
    end;

    if (node.blockName <> '') then begin
      if FcurrentScope.scopeLevel > 1 then
        Fwriter.writeLn(indent+MAIN_LABEL+':')
      else
        Fwriter.writeLn(indent+node.blockName+'_'+MAIN_LABEL+':');
    end;
  end;

  if FblockIRQType = ttRasterIRQ then begin
//    emitCode(':saveStack()');
    emitCode('  :irq_start(end)');
  end;

  node.compoundStatement.acceptVisitor(self);
end;

procedure TASTto6502.visitAsmBlock(node : TAsmBlock);
var
  i   : Integer;
begin
  if not node.isMacro then begin
    for i := 0 to node.declarations.Count - 1 do begin
      TAST(node.declarations.Items[i]).acceptVisitor(self);
    end;

    if (node.blockName <> '') then
      Fwriter.writeLn(indent+MAIN_LABEL+':');

    if FblockIRQType = ttRasterIRQ then
      emitCode('  :irq_start(end)');
  end;

  for i := 0 to node.asmStatements.Count - 1 do begin
  // output asm statements
    emitCode(Trim(node.asmStatements.Strings[i]));
  end;
end;

procedure TASTto6502.visitUnitBlock(node: TUnitBlock);
var
  i : Integer;
begin
  for i := 0 to node.declarations.Count - 1 do begin
    TAST(node.declarations.Items[i]).acceptVisitor(self);
  end;
end;

procedure TASTto6502.emitBinOp(left,right,op : TExpToken);
begin
  if (left.size = 1) and (right.size = 1) then begin
    case op.token.tType of
      ttPlus  : begin
        emitCode('clc');
        emitCode('lda '+leafNodeToImmOrAddr(left));
        emitCode('adc '+leafNodeToImmOrAddr(right));
      end;
      ttMinus :  begin
        emitCode('sec');
        emitCode('lda '+leafNodeToImmOrAddr(left));
        emitCode('sbc '+leafNodeToImmOrAddr(right));
      end;
    end;
  end;
end;

function  TASTto6502.isSimpleAdd2(node : TAssign; symSize : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttPlus) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value = 2);
    end
    else
    if binOp.right.isVariable and (binOp.left.isInteger) then begin
      Result := (TVariable(binOp.right).value = node.variable.value) and (TNumber(binOp.left).value = 2);
    end;
  end;

  if not Result then Exit;

  if symSize = 1 then begin
    emitCode('inc '+node.variable.value);
    emitCode('inc '+node.variable.value);
  end
  else begin
    emitCode('inc '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('inc '+node.variable.value+' + 1');
    emitLabel('!');
    emitCode('inc '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('inc '+node.variable.value+' + 1');
    emitLabel('!');
  end;
end;

function  TASTto6502.isSimpleSub2(node : TAssign; symSize : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttMinus) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value = 2);
    end
    else
    if binOp.right.isVariable and (binOp.left.isInteger) then begin
      Result := (TVariable(binOp.right).value = node.variable.value) and (TNumber(binOp.left).value = 2);
    end;
  end;

  if not Result then Exit;

  if symSize = 1 then begin
    emitCode('dec '+node.variable.value);
    emitCode('dec '+node.variable.value);
  end
  else begin
    emitCode('lda '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('dec '+node.variable.value+' + 1');
    emitLabel('!');
    emitCode('lda '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('dec '+node.variable.value+' + 1');
    emitLabel('!');
  end;
end;

function  TASTto6502.isSimpleIncrement(node : TAssign; symSize : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttPlus) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value = 1);
    end
    else
    if binOp.right.isVariable and (binOp.left.isInteger) then begin
      Result := (TVariable(binOp.right).value = node.variable.value) and (TNumber(binOp.left).value = 1);
    end;
  end;

  if not Result then Exit;

  emitCode('// inc '+node.variable.value);
  if symSize = 1 then
    emitCode('inc '+node.variable.value)
  else begin
    emitCode('inc '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('inc '+node.variable.value+' + 1');
    emitLabel('!');
  end;
end;

function  TASTto6502.isSimpleDecrement(node : TAssign; symSize : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttMinus) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value = 1);
    end
    else
    if binOp.right.isVariable and (binOp.left.isInteger) then begin
      Result := (TVariable(binOp.right).value = node.variable.value) and (TNumber(binOp.left).value = 1);
    end;
  end;

  if not Result then Exit;

  emitCode('// dec '+node.variable.value);
  if symSize = 1 then
    emitCode('dec '+node.variable.value)
  else begin
    emitCode('dec '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('dec '+node.variable.value+' + 1');
    emitLabel('!');
  end;
end;

function  TASTto6502.isSHL(node : TAssign; var shiftCount : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttShl) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value >= 0);
      shiftCount := TNumber(binOp.right).value;
    end;
  end;
end;

function  TASTto6502.isSHR(node : TAssign; var shiftCount : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttShr) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value >= 0);
      shiftCount := TNumber(binOp.right).value;
    end;
  end;
end;

function  isSimpleArrayIndex(node : TAST; var aVarName,aNumber : AnsiString) : Boolean;
var
  leftType  : AnsiString;
  rightType : AnsiString;
begin
  Result := False;

  aVarName := '';
  aNumber  := '0';

  if (node = nil)     then Exit;
  if (node.size <> 1) then Exit;

  if (node.isInteger) then begin
    aNumber := TNumber(node).token.tValue;
    Result := True;
    Exit;
  end
  else if (node.isVariable) then begin
    aVarName := TVariable(node).token.tValue;
    Result := True;
    Exit;
  end else if (node is TBinOp) then begin
    leftType  := TBinOp(node).left.ClassName;
    rightType := TBinOp(node).right.ClassName;

    if ((TBinOp(node).left is TVariable) and (TBinOp(node).right is TNumber)) then begin
      aVarName := TVariable(TBinOp(node).left).token.tValue;
      aNumber  := TNumber(TBinOp(node).right).token.tValue;
      Result := True;
      Exit;
    end;

    if ((TBinOp(node).right is TVariable) and (TBinOp(node).left is TNumber)) then begin
      aVarName := TVariable(TBinOp(node).right).token.tValue;
      aNumber  := TNumber(TBinOp(node).left).token.tValue;
      Result := True;
      Exit;
    end;
  end;
end;

procedure TASTto6502.visitPoke(node : TPoke);
var
  c : Byte;
begin
  if node.expr.isLeaf then
    if (node.address.isInteger and node.expr.isInteger) then begin
      emitCode('lda #' + TNumber(node.expr).token.tValue);
      emitCode('sta ' + TNumber(node.address).token.tValue);
    end
    else
    if (node.address.isInteger and node.expr.isVariable) then begin
      emitCode('lda ' + TVariable(node.expr).token.tValue);
      emitCode('sta ' + TNumber(node.address).token.tValue);
    end
    else
    if (node.address.isInteger and node.expr.isString) then begin
      if TString(node.expr).token.tValue <> '' then begin
        c := charToCBMscreenCode(TString(node.expr).token.tValue[1]);
        emitCode('lda #' + IntToStr(c));
      end
      else
        emitCode('Poke: value is an empty string!');
      emitCode('sta ' + TNumber(node.address).token.tValue);
    end
    else
      emitCode('Poke: unsupported address/value');
end;

var
  stack      : array[0..MAX_TEMP - 1] of TAST;
  sIndex     : Integer;
  reg        : Integer;

function newTemp : AnsiString;
begin
  Result := TEMP_REG + IntToStr(reg);
  Inc(reg);
end;

function popValue : TAST;
begin
  Result := stack[sIndex];
  Dec(sIndex);
end;

procedure pushValue(aValue : TAST);
begin
  Inc(sIndex);
  stack[sIndex] := aValue;
end;

function isIndexedVariable(v : TAST) : Boolean;
begin
  Result := False;

  if v = nil then Exit;
  if not v.isVariable then Exit;

  Result := TVariable(v).arrayParam <> nil;
end;

procedure TASTto6502.generateIndexShiftTAC(aIndex : TAST; aTacList : TTACList);
var
  shift      : Integer;
  right1     : TAST;
  right2     : TAST;
  op         : TAST;
  temp       : TAST;
  tacNode    : TTACNode;
begin
  shift  := PowerOf2ToShift(aIndex.size);
  right2 := TNumber.Create(shift);
  right1 := popValue;

  op     := TBinOp.Create(right1,uToken.Token(ttShl,'<<'),right2);

  temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),2);
  pushValue(temp);
  writeLn(right1.toString,' ',op.toString,' ',right2.toString);

  tacNode := TTACNode.Create(temp,op,right1,right2,nil,nil);
  aTacList.addNode(tacNode);
end;

procedure TASTto6502.generateTAC(node : TAST; aTacList : TTACList);
var
  temp       : TAST;
  ast        : TAST;
  right1     : TAST;
  right2     : TAST;
  index      : TAST;
  exp        : TList;
  i          : Integer;
  tacNode    : TTACNode;
begin
  exp := flattenExpression(node);

//  writeLn;

  for i := 0 to exp.Count - 1 do begin
    ast := exp.Items[i];

    if not ast.isOp then begin
      if isIndexedVariable(ast) then begin
        generateTAC(TVariable(ast).arrayParam,aTacList);

        if (ast.size > 1) and not isDerefed(ast) then begin
          generateIndexShiftTAC(ast,aTacList);
        end;

        right1 := popValue;
        index := right1;//TVariable.Create(uToken.Token(ttIdent,right1.toString),0);
        temp  := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),2);
        pushValue(temp);
//        writeLn(temp.toString,' := ',ast.toString,'[',right1.toString,']');

        tacNode := TTACNode.Create(temp,nil,ast,nil,nil,index);
        aTacList.addNode(tacNode);
      end
      else
        pushValue(ast);
    end
    else
    if (ast.isBinOp) then begin
      right2 := popValue;
      right1 := popValue;

      temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),2);
      pushValue(temp);
//      writeLn(temp.toString,' := ',right1.toString,' ',ast.toString,' ',right2.toString);

      tacNode := TTACNode.Create(temp,ast,right1,right2,nil,nil);
      aTacList.addNode(tacNode);
    end
    else
    if (ast.isUnaryOp) then begin
      right2 := popValue;

      temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),2);
      pushValue(temp);
//      writeLn(temp.toString,' := ',ast.toString,' ',right2.toString);

      tacNode := TTACNode.Create(temp,ast,nil,right2,nil,nil);
      aTacList.addNode(tacNode);
    end;
  end;
end;

function  TASTto6502.generateTAC(node : TAST; dstVar : TVariable) : TTACList;
var
  temp       : TAST;
  right      : TAST;
  value      : TAST;
  index      : TAST;
  tacNode    : TTACNode;
begin
  reg    := 0;
  sIndex := -1;

  Result := TTACList.Create;

  generateTAC(node,Result);

  if dstVar <> nil then begin
    if (sIndex <> -1) then begin
      if isIndexedVariable(dstVar) then begin
        value := popValue;
        generateTAC(dstVar.arrayParam,Result);

        if (dstVar.size > 1) and not isDerefed(dstVar) then begin
          generateIndexShiftTAC(dstVar,Result);
        end;

        right := popValue;

        index := right;//TVariable.Create(uToken.Token(ttIdent,right.toString),0);
        temp  := TVariable.Create(uToken.Token(ttIdent,Result.getTemp),2);
        pushValue(temp);

        tacNode := TTACNode.Create(dstVar,nil,value,nil,index,nil);
        Result.addNode(tacNode);
      end
      else begin
        value := popValue;
//        writeLn(dstVar.toString,' := ',value.toString);

        tacNode := TTACNode.Create(dstVar,nil,value,nil,nil,nil);
        Result.addNode(tacNode);
      end;
    end;
  end;
end;

procedure TASTto6502.pushLoopExitLabel(exitLabel : AnsiString);
var
  i : Integer;
begin
  Inc(FloopExitLabelCount);
  FloopExitLabels[FloopExitLabelCount] := exitLabel;
end;

procedure TASTto6502.popLoopExitLabel;
begin
  Dec(FloopExitLabelCount);
end;

function  TASTto6502.peekLoopExitLabel : AnsiString;
begin
  Result := FloopExitLabels[FloopExitLabelCount];
  Dec(FloopExitLabelCount);
end;

function  TASTto6502.isSimpleReadWriteIndexedPointer(aSrc,aDst : TAST) : Boolean;
var
  src,dst,srcIndex,dstIndex : AnsiString;
begin
  Result := False;

  if not aSrc.isVariable then Exit;
  if not aDst.isVariable then Exit;

  if not TVariable(aSrc).deref then Exit;
  if not TVariable(aDst).deref then Exit;

  if TVariable(aSrc).arrayParam = nil then Exit;
  if TVariable(aDst).arrayParam = nil then Exit;

  if TVariable(aSrc).arrayParam.size <> 1 then Exit;
  if TVariable(aDst).arrayParam.size <> 1 then Exit;

  if not TVariable(aSrc).arrayParam.isLeaf then Exit;
  if not TVariable(aDst).arrayParam.isLeaf then Exit;
  if not (TVariable(aSrc).arrayParam.isInteger or TVariable(aSrc).arrayParam.isVariable) then Exit;
  if not (TVariable(aDst).arrayParam.isInteger or TVariable(aDst).arrayParam.isVariable) then Exit;

  src := aSrc.toString;
  dst := aDst.toString;
  srcIndex := TCodeGen_6502.getValue_lo(TVariable(aSrc).arrayParam.toString);
  dstIndex := TCodeGen_6502.getValue_lo(TVariable(aDst).arrayParam.toString);

  emitCode('ldy '+srcIndex);
  emitCode('lda ('+src+'),y');
  emitCode('ldy '+dstIndex);
  emitCode('sta ('+dst+'),y');

  Result := True;
end;

procedure TASTto6502.visitAssign(node : TAssign);

var
  i          : Integer;
  sym        : TSymbol;
  shiftCount : Integer;
  varName1   : AnsiString;
  number1    : AnsiString;
  varName2   : AnsiString;
  number2    : AnsiString;
  doEmitExp  : Boolean;
  tacList    : TTACList;
begin
  sym := FcurrentScope.lookup(node.variable.toString);

  if (node.variable.arrayParam = nil) and (node.expr.isVariable) then begin
    if TVariable(node.expr).isPointer then begin
      // simple address -> variable
      emitCode('// '+node.variable.token.tValue+' := @'+TVariable(node.expr).token.tValue);
      emitCode('lda #<'+TVariable(node.expr).token.tValue);
      emitCode('sta '+node.variable.token.tValue+' + 0');

      if node.variable.size = 2 then begin
        emitCode('lda #>'+TVariable(node.expr).token.tValue);
        emitCode('sta '+node.variable.token.tValue+' + 1');
      end;
      Exit;
    end;
  end;

  // test for a^[index] := b^[index]
  if isSimpleReadWriteIndexedPointer(node.expr,node.variable) then Exit;

  if sym <> nil then begin
    // test for simple "a := a + 1" type assignment...

    if isSimpleIncrement(node,sym.size) then Exit;

    if isSimpleDecrement(node,sym.size) then Exit;

    if isSimpleAdd2(node,sym.size) then Exit;

    if isSimpleSub2(node,sym.size) then Exit;

    if isSHL(node,shiftCount) then begin
      for i := 1 to shiftCount do begin
        if sym.size = 1 then
          emitCode('asl '+node.variable.value)
        else begin

          emitCode('asl '+node.variable.value+' + 0');
          emitCode('rol '+node.variable.value+' + 1');
        end;
      end;
      Exit;
    end;

    if isSHR(node,shiftCount) then begin
      for i := 1 to shiftCount do begin
        if sym.size = 1 then
          emitCode('lsr '+node.variable.value)
        else begin

          emitCode('lsr '+node.variable.value+' + 0');
          emitCode('ror '+node.variable.value+' + 1');
        end;
      end;
      Exit;
    end;

    if node.expr.isVariable then begin
      if isSimpleArrayIndex(TVariable(node.expr).arrayParam,varName1,number1) and
         isSimpleArrayIndex(node.variable.arrayParam,varName2,number2) then begin
      // check expression to see if is array index load
        if (varName1 <> '') then begin
          emitCode('ldy '+varName1);
          emitCode('lda '+TVariable(node.expr).token.tValue+' + ' + number1+',y');
        end else begin
          emitCode('lda '+TVariable(node.expr).token.tValue+' + ' + number1);
        end;
        // load part
        if (varName2 <> '') then begin
          emitCode('ldy '+varName2);
          emitCode('sta '+sym.symName+' + ' + number2+',y');
        end else begin
          emitCode('sta '+sym.symName+' + '+number2);
        end;

        Exit;
      end;
    end;
  end;


  if (node.variable.arrayParam = nil) and node.variable.deref then
  // add default [0] index to the deref'd variable
  node.variable.arrayParam := TNumber.Create(0);

  tacList := generateTAC(node.expr,node.variable);

  tacList.optimise;

//  tacList.debugPrint;
  emitCode(tacList);

//  writeLn('-----------------------------');

  Exit;

  doEmitExp := True;

  if node.expr.isVariable then begin
    if isSimpleArrayIndex(TVariable(node.expr).arrayParam,varName1,number1) then begin
    // check expression to see if is array index load
      if (varName1 <> '') then begin
        emitCode('ldy '+varName1);
        emitCode('lda '+TVariable(node.expr).token.tValue+' + ' + number1+',y');
      end else begin
        emitCode('lda '+TVariable(node.expr).token.tValue+' + ' + number1);
      end;

      doEmitExp := False;
    end
    else begin
      if TVariable(node.expr).deref then begin
        emitCode('ldy #0');
        emitCode('lda ('+TVariable(node.expr).token.tValue+'),y');

        doEmitExp := False;
      end;
    end;
  end;

  if doEmitExp then emitExpression(node.expr);

  // emit variable part of assignment (the store)
  if isSimpleArrayIndex(node.variable.arrayParam,varName2,number2) then begin
    if (varName2 <> '') then begin
      emitCode('ldy '+varName2);
      emitCode('sta '+sym.symName+' + ' + number2+',y');
    end else begin
      emitCode('sta '+sym.symName+' + '+number2);
    end;
  end
  else if node.variable.deref then begin
    // is a derefed pointer
    emitCode('ldy #0');
    emitCode('sta ('+sym.symName+'),y');
  end
  else begin
    if (sym.size = 1) then begin
      emitCode('sta '+node.variable.token.tValue);
    end
    else begin
      if node.expr.isLeaf then begin
        if node.expr.isInteger then begin
          if (TNumber(node.expr).size > 1) then begin
            if not (node.variable.deref) then begin
              emitCode('lda #<'+TNumber(node.expr).token.tValue);
              emitCode('sta '+sym.symName + ' + 0');
              emitCode('lda #>'+TNumber(node.expr).token.tValue);
              emitCode('sta '+sym.symName + ' + 1');
            end else begin
              emitCode('ldy #0');
              emitCode('lda #<'+TNumber(node.expr).token.tValue);
              emitCode('sta ('+sym.symName + '),y');
              emitCode('iny');
              emitCode('lda #>'+TNumber(node.expr).token.tValue);
              emitCode('sta ('+sym.symName + '),y');
            end;
          end
          else begin
            if not (node.variable.deref) then begin
              emitCode('lda #'+TNumber(node.expr).token.tValue);
              emitCode('sta '+sym.symName);
            end else begin
              emitCode('ldy #0');
              emitCode('lda #'+TNumber(node.expr).token.tValue);
              emitCode('sta ('+sym.symName+'),y');
            end;
          end;
        end;
      end;
    end;
  end;

  //  node.expr.acceptVisitor(self);
end;

procedure TASTto6502.visitCall(node : TCall);
var
  p         : Integer;
  param     : TVarSymbol;
  callParam : TAST;
  procSym   : TProcSymbol;
  loReg     : AnsiString;
  hiReg     : AnsiString;
  value     : AnsiString;
  dstVar    : TVariable;
  assign    : TAssign;
begin
  procSym := TProcSymbol(FcurrentScope.lookup(node.procName));

  if procSym.procType = ttImportBinary then begin
  // for this type, just call the address directly with no main label
    emitCode('jsr '+node.procName);
    Exit;
  end;

  resetTempRegVar();

  for p := 0 to Length(procSym.params) - 1 do begin
    // load params
    param := procSym.params[p];

    callParam := TAST(node.params.Items[p]);
{    FfillExpTokens := True;
    resetTokens;
    TAST(node.params.Items[p]).acceptVisitor(self);
    FfillExpTokens := False;}

    dstVar := TVariable.Create(node.procName + '.' + param.symName,param.size);

    if (procSym.procType = ttAssembler) and isAsmParam(param.symName,param.symType,loReg,hiReg) then begin
    // is a asm param so use correct register(s) to load into
      dstVar.isRegister := True;
      dstVar.loReg      := loReg;
      dstVar.hiReg      := hiReg;

{      value := leafNodeToImmOrAddr(FExpTokens[0]);

      if (value[1] = '#') then
        emitCode('ld'+loReg+' #<'+Copy(value,2,Length(value)))
      else
        emitCode('ld'+loReg+' '+value+' + 0');

      if hiReg <> '' then
        if (value[1] = '#') then
          emitCode('ld'+hiReg+' #>'+Copy(value,2,Length(value)))
        else
          emitCode('ld'+hiReg+' '+value+' + 1');}
    end else begin
//      emitCode('lda '+leafNodeToImmOrAddr(FExpTokens[0]));

//      emitCode('sta '+node.procName + '.' + param.symName);
    end;

    assign := TAssign.Create(dstVar,callParam);
    assign.acceptVisitor(Self);
  end;

  emitSaveRegVars();

  if procSym.procAddress <> '' then
  // is absolute asm proc so use that address
    emitCode('jsr '+procSym.procAddress)
  else
    emitCode('jsr '+node.procName+'.'+MAIN_LABEL);
end;

procedure TASTto6502.visitMacroCall(node : TMacroCall);
var
  i : Integer;
begin
  Fwriter.write(indent+':'+node.macroName);

  Fwriter.write('(');

  for i := 0 to node.params.Count - 1 do begin
    Fwriter.write(node.params.Strings[i]);
    if (i < node.params.Count - 1) then Fwriter.write(',');
  end;

  Fwriter.writeLn(')');
end;

//procedure TASTto6502.visitFuncCall(node : TFuncCall);
//var
//  p         : Integer;
//  param     : TVarSymbol;
//  callParam : TAST;
//  procSym   : TFuncSymbol;
//  loReg     : AnsiString;
//  hiReg     : AnsiString;
//  value     : AnsiString;
//  dstVar    : TVariable;
//  assign    : TAssign;
//begin
//  procSym := TFuncSymbol(FcurrentScope.lookup(node.funcName));
//
//  if procSym.funcType = ttImportBinary then begin
//  // for this type, just call the address directly with no main label
//    emitCode('jsr '+node.funcName);
//    Exit;
//  end;
//
//  resetTempRegVar();
//
//  for p := 0 to Length(procSym.params) - 1 do begin
//    // load params
//    param := procSym.params[p];
//
//    callParam := TAST(node.params.Items[p]);
//{    FfillExpTokens := True;
//    resetTokens;
//    TAST(node.params.Items[p]).acceptVisitor(self);
//    FfillExpTokens := False;}
//
//    dstVar := TVariable.Create(node.funcName + '.' + param.symName,param.size);
//
//    if (procSym.funcType = ttAssembler) and isAsmParam(param.symName,param.symType,loReg,hiReg) then begin
//    // is a asm param so use correct register(s) to load into
//      dstVar.isRegister := True;
//      dstVar.loReg      := loReg;
//      dstVar.hiReg      := hiReg;
//
//{      value := leafNodeToImmOrAddr(FExpTokens[0]);
//
//      if (value[1] = '#') then
//        emitCode('ld'+loReg+' #<'+Copy(value,2,Length(value)))
//      else
//        emitCode('ld'+loReg+' '+value+' + 0');
//
//      if hiReg <> '' then
//        if (value[1] = '#') then
//          emitCode('ld'+hiReg+' #>'+Copy(value,2,Length(value)))
//        else
//          emitCode('ld'+hiReg+' '+value+' + 1');}
//    end else begin
////      emitCode('lda '+leafNodeToImmOrAddr(FExpTokens[0]));
//
////      emitCode('sta '+node.procName + '.' + param.symName);
//    end;
//
//    assign := TAssign.Create(dstVar,callParam);
//    assign.acceptVisitor(Self);
//  end;
//
//  emitSaveRegVars();
//
//  if procSym.funcAddress <> '' then
//  // is absolute asm proc so use that address
//    emitCode('jsr '+procSym.funcAddress)
//  else
//    emitCode('jsr '+node.funcName+'.'+MAIN_LABEL);
//end;


procedure TASTto6502.visitFuncCall(node : TFuncCall);
var
  p       : Integer;
  param   : TVarSymbol;
  funcSym : TFuncSymbol;
  callParam : TAST;
  loReg   : AnsiString;
  hiReg   : AnsiString;
  value   : AnsiString;
  dstVar  : TVariable;
  assign  : TAssign;
begin
  if FflattenExpression then begin
    pushExpNode(node);
    Exit;
  end;

  funcSym := TFuncSymbol(FcurrentScope.lookup(node.funcName));

  for p := 0 to Length(funcSym.params) - 1 do begin
    // load params
    param     := funcSym.params[p];
    callParam := TAST(node.params.Items[p]);

    tokenizeExpression(TAST(node.params.Items[p]),False);

    dstVar := TVariable.Create(node.funcName + '.' + param.symName,param.size);

    if (funcSym.funcType = ttAssembler) and isAsmParam(param.symName,param.symType,loReg,hiReg) then begin
    // is a asm param so use correct register(s) to load into
      value := leafNodeToImmOrAddr(FExpTokens[0]);

      if (value[1] = '#') then
        emitCode('ld'+loReg+' #<'+Copy(value,2,Length(value)))
      else
        emitCode('ld'+loReg+' '+value+' + 0');

      if hiReg <> '' then
        if (value[1] = '#') then
          emitCode('ld'+hiReg+' #>'+Copy(value,2,Length(value)))
        else
          emitCode('ld'+hiReg+' '+value+' + 1');
    end else begin
//      emitCode('lda '+leafNodeToImmOrAddr(FExpTokens[0]));
//
//      emitCode('sta '+node.funcName + '.' + param.symName);

      assign := TAssign.Create(dstVar,callParam);
      assign.acceptVisitor(Self);
    end;

//    assign := TAssign.Create(dstVar,param);
//    assign.acceptVisitor(Self);
  end;

  if funcSym.funcAddress <> '' then
  // is absolute asm func so use that address
    emitCode('jsr '+funcSym.funcAddress)
  else begin
    emitCode('jsr '+node.funcName+'.'+MAIN_LABEL);

//    if funcSym.funcType = ttFunc then
//      emitCode('lda '+node.funcName+'.result');
  end;
end;

var
  tokenStack : array[0..100] of TExpToken;
  tokenIndex : Integer;

procedure pushExpToken(aToken : TExpToken);
begin
  Inc(tokenIndex);
  tokenStack[tokenIndex] := aToken;
end;

function  popExpToken: TExpToken;
begin
  Result := tokenStack[tokenIndex];
  Dec(tokenIndex);
end;

procedure TASTto6502.tokenizeExpression(node : TAST; aResetTokens : Boolean);
var
  i : Integer;
begin
  FfillExpTokens := True;

  if aResetTokens then resetTokens;

  node.acceptVisitor(self);

  FfillExpTokens := False;
end;

function  TASTto6502.flattenExpression(node : TAST; aExpression : TList = nil) : TList;
begin
  FflattenExpression := True;

  if (aExpression = nil) then
    Fexpression := TList.Create
  else
    Fexpression := aExpression;

  node.acceptVisitor(self);

  FflattenExpression := False;

  Result := Fexpression;
end;

procedure TASTto6502.debugExpression;
var
  i : Integer;
  e : TAST;
begin
  for i := 0 to Fexpression.Count - 1 do begin
    e := TAST(Fexpression.Items[i]);

    if      e.isInteger      then write(TNumber(e).value,' ')
    else if e.isVariable     then write(TVariable(e).value,' ')
    else if (e is TBinOp)    then write(TBinOp(e).op.tValue,' ')
    else if (e is TUnaryOp)  then write(TUnaryOp(e).op.tValue,' ')
    else if (e is TFuncCall) then write(TFuncCall(e).funcName,'() ')
  end;

  writeLn;
end;

procedure TASTto6502.emitCompare_16(left,right : TExpToken; lower,same,higher : Boolean; falseLabel : AnsiString);
begin
  emitCode('lda ' + getLeafNodeHi(left));
  emitCode('cmp ' + getLeafNodeHi(right));
  emitCode('bne !+');
  emitCode('lda ' + getLeafNodeLo(left));
  emitCode('cmp ' + getLeafNodeLo(right));
  emitLabel('!');

  if lower then
    emitCode('bcc !+ // is <')
  else
    emitCode('bcc '+falseLabel + ' // is < (false)');

  if higher then
    emitCode('bne !+ // is >')
  else
    emitCode('bne '+falseLabel + ' // is > (false)');

  if same then
    emitCode('beq !+ // is =')
  else
    emitCode('beq '+falseLabel + ' // is = (false)');

  emitCode('// is true');
  emitLabel('!');
end;

procedure TASTto6502.emitExpression(node : TAST; falseLabel : AnsiString = '');
var
  i,c        : Integer;
  code       : Integer;
  expStarted : Boolean;
  left,right : TExpToken;
  expResult  : TExpToken;
begin
  if (node = nil) then Exit;

  expResult.token := Token(ttEOF,'Result');
  expResult.size  := 0;

  expStarted := False;
  tokenIndex := -1;
  tokenizeExpression(node,true);

//  Fwriter.write(indent+'// expression: ');

//  for i := 0 to High(FExpTokens) do
//    Fwriter.write(indent+FExpTokens[i].token.tValue+' ');

//  Fwriter.writeLn();

  if (Length(FExpTokens) = 1) then begin
    // a single literal num/variable
    emitCode('lda '+leafNodeToImmOrAddr(FExpTokens[0]));
    Exit;
  end;

  for i  := 0 to High(FExpTokens) do begin
    if (FExpTokens[i].token.tType in [ttIdent,ttIntNum]) then begin
      // is operand
      pushExpToken(FExpTokens[i]);
    end
    else begin
      if (FExpTokens[i].token.tType in [ttNot,ttNeg]) then begin
        // is a unary op
        right := popExpToken;

        // push pretend expression result onto stack
        pushExpToken(expResult);
      end else begin
      // is a binary op
        right := popExpToken;
        left  := popExpToken;

        if not (expStarted) then begin
          // emit left hand side of expression
          emitCode('lda '+leafNodeToImmOrAddr(left));
          expStarted := True;
        end;

        // push pretend expression result onto stack
        pushExpToken(expResult);

        // do right hand of expression
        case FExpTokens[i].token.tType of
          ttPlus  : begin
            emitCode('clc');
            emitCode('adc '+leafNodeToImmOrAddr(right));
          end;
          ttMinus :  begin
            emitCode('sec');
            emitCode('sbc '+leafNodeToImmOrAddr(right));
          end;
          ttTimes :       emitCode('// do multiply!');
          ttSlash,ttDiv : emitCode('// do division!');
          ttShr   : begin
            Val(right.token.tValue,c,code);
            for c := 1 to c do emitCode('lsr');
          end;
          ttShl   : begin
            Val(right.token.tValue,c,code);
            for c := 1 to c do emitCode('asl');
          end;
          ttAnd   :       emitCode('and '+leafNodeToImmOrAddr(right));
          ttOr    :       emitCode('ora '+leafNodeToImmOrAddr(right));
          ttXor   :       emitCode('eor '+leafNodeToImmOrAddr(right));
          ttEql   :       begin
            emitCode('cmp '+leafNodeToImmOrAddr(right));
            if (falseLabel <> '') then
//              emitCode('bne !+');
              emitCode('beq !+');
              emitCode('jmp '+falseLabel+' // jump to false label');
              emitLabel('!');
          end;
        end;
      end;
    end;
  end;
end;

procedure TASTto6502.emitBinOpCondition(falseLabel : AnsiString);
var
  left  : TExpToken;
  right : TExpToken;
  op    : TExpToken;
begin
  // is a single binary op condition
  left  := FexpTokens[0];
  right := FexpTokens[1];
  op    := FexpTokens[2];


  if (left.size = 2) or (right.size = 2) then begin
    if op.token.tType in [ttGtr,ttGeq,ttLss,ttLeq,ttNeq,ttEql] then begin
      emitCode('// '+ leafNodeToImmOrAddr(left) + ' '+op.token.tValue+' ' + leafNodeToImmOrAddr(right));
//      emitCode('sbc ' + getLeafNodeLo(right));
//      emitCode('lda ' + getLeafNodeHi(left));
//      emitCode('sbc ' + getLeafNodeHi(right));

      case op.token.tType of
        ttGtr : begin
          emitCompare_16(left,right,false,false,true,falseLabel);
//          emitCode('ldy ' + getLeafNodeLo(left));
//          emitCode('lda ' + getLeafNodeHi(left));

  //        emitCode('beq '+falseLabel+' // = branch to false label');
  //        emitCode('bcc '+falseLabel+' // < branch to false label');
//          emitCode('beqLong '+falseLabel+' // = branch to false label');
//          emitCode('bccLong '+falseLabel+' // < branch to false label');
        end;
        ttGeq : begin
        // >=
          emitCompare_16(left,right,false,true,true,falseLabel);
//          emitCode('ldy ' + getLeafNodeLo(left));
//          emitCode('lda ' + getLeafNodeHi(left));

  //        emitCode('bcc '+falseLabel+' // < branch to false label');
//          emitCode('bccLong '+falseLabel+' // < branch to false label');
  //        emitCode('bcs !+ // >= branch to true label');
  //        emitCode('jmp '+falseLabel+' // jump to false label');
  //        emitLabel('!');
        end;
        ttLss : begin
        // <
          emitCompare_16(left,right,true,false,false,falseLabel);
//          emitCode('ldy ' + getLeafNodeLo(left));
//          emitCode('lda ' + getLeafNodeHi(left));

  //        emitCode('bcs '+falseLabel+' // >= branch to false label');
//          emitCode('bcsLong '+falseLabel+' // >= branch to false label');
  //        emitCode('bcc !+ // < branch to true label');
  //        emitCode('jmp '+falseLabel+' // jump to false label');
  //        emitLabel('!');
        end;
        ttLeq : begin
        // <=
          emitCompare_16(left,right,true,true,false,falseLabel);
//          emitCode('ldy ' + getLeafNodeLo(left));
//          emitCode('lda ' + getLeafNodeHi(left));
//
//          emitCode('bcc !+ // < branch to true label');
//          emitCode('beq !+ // = branch to true label');
//          emitCode('jmp '+falseLabel);
//          emitLabel('!');
        end;
        ttNeq : begin
        // <>
          emitCompare_16(left,right,true,false,true,falseLabel);
//          emitCode('ldy '     + getLeafNodeLo(left));
//          emitCode('lda '     + getLeafNodeHi(left));
//
//          emitCode('cpy '     + getLeafNodeLo(right));//   ; compare low bytes
//          emitCode('beq ' + falseLabel+' // to false label');
//          emitCode('cmp '     + getLeafNodeHi(right));//   ; compare high bytes
//          emitCode('beq ' + falseLabel+' // to false label');
        end;
        ttEql : begin
        // =
          emitCompare_16(left,right,false,true,false,falseLabel);
//          emitCode('ldy '     + getLeafNodeLo(left));
//          emitCode('lda '     + getLeafNodeHi(left));
//
//          emitCode('cpy '     + getLeafNodeLo(right));//   ; compare low bytes
//          emitCode('bne ' + falseLabel+' // to false label');
//          emitCode('cmp '     + getLeafNodeHi(right));//   ; compare high bytes
//          emitCode('bne ' + falseLabel+' // to false label');
        end;
      end;
    end
    else
      Fwriter.writeLn('16-bit BinOp '+op.token.tValue + 'not supported yet');
  end
  else
  if (left.size = 1) and (right.size = 1) then begin
    emitCode('// '+ leafNodeToImmOrAddr(left) + ' '+op.token.tValue+' ' + leafNodeToImmOrAddr(right));

    case op.token.tType of
      ttGtr : begin
      // >
        emitCode('lda '+leafNodeToImmOrAddr(left));
        emitCode('cmp '+leafNodeToImmOrAddr(right));
//        emitCode('beq '+falseLabel+' // = branch to false label');
//        emitCode('bcc '+falseLabel+' // < branch to false label');
        emitCode('beqLong '+falseLabel+' // = branch to false label');
        emitCode('bccLong '+falseLabel+' // < branch to false label');
      end;
      ttGeq : begin
      // >=
        emitCode('lda '+leafNodeToImmOrAddr(left));
        emitCode('cmp '+leafNodeToImmOrAddr(right));
//        emitCode('bcc '+falseLabel+' // < branch to false label');
        emitCode('bccLong '+falseLabel+' // < branch to false label');
//        emitCode('bcs !+ // >= branch to true label');
//        emitCode('jmp '+falseLabel+' // jump to false label');
//        emitLabel('!');
      end;
      ttLss : begin
      // <
        emitCode('lda '+leafNodeToImmOrAddr(left));
        emitCode('cmp '+leafNodeToImmOrAddr(right));
//        emitCode('bcs '+falseLabel+' // >= branch to false label');
        emitCode('bcsLong '+falseLabel+' // >= branch to false label');
//        emitCode('bcc !+ // < branch to true label');
//        emitCode('jmp '+falseLabel+' // jump to false label');
//        emitLabel('!');
      end;
      ttLeq : begin
      // <=
        emitCode('lda '+leafNodeToImmOrAddr(left));
        emitCode('cmp '+leafNodeToImmOrAddr(right));
        emitCode('bcc !+ // < branch to true label');
        emitCode('beq !+ // = branch to true label');
        emitCode('jmp '+falseLabel);
        emitLabel('!');
      end;
      ttNeq : begin
      // <>
        emitCode('lda '+leafNodeToImmOrAddr(left));
        emitCode('cmp '+leafNodeToImmOrAddr(right));
//        emitCode('bne '+falseLabel+' // branch to false label');
        emitCode('beqLong '+falseLabel+' // branch to false label');
//        emitCode('bne !+ // >= branch to true label');
//        emitCode('jmp '+falseLabel+' // jump to false label');
//        emitLabel('!');
      end;
      ttEql : begin
      // =
        emitCode('lda '+leafNodeToImmOrAddr(left));
        emitCode('cmp '+leafNodeToImmOrAddr(right));
//        emitCode('bne '+falseLabel+' //branch to false label');
        emitCode('bneLong '+falseLabel+' // branch to false label');
//        emitCode('beq !+');
//        emitCode('jmp '+falseLabel+' //branch to false label');
//        emitLabel('!');
      end;
      else
        Fwriter.writeLn('8-bit BinOp '+op.token.tValue + 'not supported yet');
    end;
  end;
end;

procedure TASTto6502.emitCondition(expr : TAST; falseLabel : AnsiString);
var
  doEmitExp : Boolean;
  tacList   : TTACList;
  dstVar    : TVariable;
begin
  FcodeGen.doingCondition := True;

  FfillExpTokens := True;
  resetTokens;
  expr.acceptVisitor(self);
  FfillExpTokens := False;

  doEmitExp := True;
  if (expr is TFuncCall) then doEmitExp := False;

  if (Length(FexpTokens) = 1) then begin
    // condition = a single literal num/variable
    emitCode('// '+ FexpTokens[0].token.tValue + ' = false');
    if (FexpTokens[0].size = 1) then begin
      if (FexpTokens[0].token.tType = ttInteger) then
        emitCode('lda #'+FexpTokens[0].token.tValue)
      else
        emitCode('lda '+FexpTokens[0].token.tValue);
    end else begin
      // is larger than 1 byte so or values together to check for false
      emitCode('lda '+getLeafNodeLo(TExpToken(FexpTokens[0])));
      emitCode('ora '+getLeafNodeHi(TExpToken(FexpTokens[0])));
    end
  end
  else
  begin
    dstVar := TVariable.Create('a',1);
    dstVar.isRegister := True;
    dstVar.loReg      := 'a';
    dstVar.hiReg      := '';

    tacList := TTACList.Create;

    generateTAC(expr,tacList);

    tacList.optimise;

    tacList.debugPrint;
    emitCode(tacList);
//    FcodeGen.loadRegister(dstVar,dstVar.loReg,dstVar.hiReg);
  end;

//    emitCode('beq '+falseLabel+' //0 = false, other = true');
    emitCode('beqLong '+falseLabel+' //0 = false, other = true');
//    emitCode('bne !+ // branch to true label');
//    emitCode('jmp '+falseLabel+' // jump to false label');
//    emitLabel('!');

//  else if (Length(FexpTokens) = 2) then begin
//    // is unary op expression
//  end
//  else if (Length(FexpTokens) = 3) then begin
//    emitBinOpCondition(falseLabel);
//  end else begin
//    if doEmitExp then
//      emitExpression(expr,falseLabel);
//  end;

  FcodeGen.doingCondition := false;
end;

procedure TASTto6502.visitIfThen(node : TIfThen);
var
  ifTrueLabel : AnsiString;
  ifElseLabel : AnsiString;
  ifEndLabel  : AnsiString;
begin
  ifTrueLabel := newLabel;
  ifElseLabel := newLabel;
  ifEndLabel  := newLabel;

//  emitCode('//if-then');

  resetTempRegVar();

  emitCondition(node.condition,ifElseLabel);

//  node.condition.acceptVisitor(self);

  node.trueBranch.acceptVisitor(self);

  emitCode('jmp '+ifEndLabel);

  emitCode('//else');

  emitLabel(ifElseLabel);

  if (node.elseBranch <> nil) then begin

    node.elseBranch.acceptVisitor(self);
  end;

  emitLabel(ifEndLabel);
end;

procedure TASTto6502.visitWhile(node : TWhile);
var
  whileLabel : AnsiString;
  falseLabel : AnsiString;
  isInfinite : Boolean;
begin
  whileLabel := newLabel;
  falseLabel := newLabel;

//  emitCode('// while loop');

  // check for infinite loop and don't emit condition!!
  isInfinite := False;

  if (node.condition.isInteger) then
    isInfinite := (TNumber(node.condition).value <> 0);

  emitLabel(whileLabel);

  if not isInfinite then emitCondition(node.condition,falseLabel);

//  node.condition.acceptVisitor(self);

  pushLoopExitLabel(falseLabel);
  node.statement.acceptVisitor(self);

  Fwriter.writeLn();
  emitCode('jmp '+whileLabel);

  emitLabel(falseLabel);

  popLoopExitLabel;
end;

procedure TASTto6502.visitFor(node : TFor);
var
  forLabel   : AnsiString;
  falseLabel : AnsiString;
begin
  forLabel   := newLabel;
  falseLabel := newLabel;

//  emitCode('// while loop');

  node.initializer.acceptVisitor(self);

  emitLabel(forLabel);

  emitCondition(node.condition,falseLabel);

//  node.condition.acceptVisitor(self);

  pushLoopExitLabel(falseLabel);
  node.statement.acceptVisitor(self);

  Fwriter.writeLn();
  node.incrementer.acceptVisitor(self);

  emitCode('jmp '+forLabel);

  emitLabel(falseLabel);
  popLoopExitLabel;
end;

procedure TASTto6502.visitNumber(node : TNumber);
begin
  if (FfillExpTokens) then
    pushToken(node.token,node.size)
  else
  if FflattenExpression then begin
    pushExpNode(node);
  end;
//  write(node.token.tValue,' ');
end;

procedure TASTto6502.visitVariable(node : TVariable);
begin
  if (FfillExpTokens) then begin
    pushToken(node.token,node.size);

    if node.arrayParam <> nil then begin
      pushToken(Token(ttLBracket,'['),0);
      tokenizeExpression(node.arrayParam,false);
      pushToken(Token(ttRBracket,']'),0);
    end;

  end
  else
  if FflattenExpression then begin
    pushExpNode(node);
  end;
//  write(node.token.tValue,' ');
end;

procedure TASTto6502.visitString(node : TString);
begin
  if (FfillExpTokens) then
    pushToken(node.token,node.size)
  else
  if FflattenExpression then begin
    pushExpNode(node);
  end;
//  write(node.token.tValue,' ');
end;

procedure TASTto6502.visitBinOp(node : TBinOp);
begin
//  if (node.left.size = 1) and (node.right.size = 1) then begin
//    emitBinOp_1_1(node);
//  end;
  node.left.acceptVisitor(self);

  node.right.acceptVisitor(self);

  if (FfillExpTokens) then
    pushToken(node.op,node.size)
  else
  if FflattenExpression then begin
    pushExpNode(node);
  end;
//  write(node.op.tValue,' ');
end;

procedure TASTto6502.visitUnaryOp(node : TUnaryOp);
begin
{  Write('(');
  write(node.op.tValue,' ');
  node.expr.acceptVisitor(self);
  Write(')');}

  node.expr.acceptVisitor(self);

  if (FfillExpTokens) then
    pushToken(node.op,node.size)
  else
  if FflattenExpression then
    pushExpNode(node);
end;

procedure TASTto6502.visitCompound (node : TCompound);
var
  i : Integer;
begin
//  incIndent;
  for i := 0 to node.children.Count - 1 do
    TAST(node.children.Items[i]).acceptVisitor(self);
//  decIndent;
end;

procedure TASTto6502.visitAsmCompound(node : TAsmCompound);
var
  i : Integer;
begin
  for i := 0 to node.asmStatements.Count - 1 do begin
  // output asm statements
    emitCode(Trim(node.asmStatements.Strings[i]));
  end;
end;

procedure TASTto6502.emitVarDecl(varDecl : TVarDecl);
var
  varSize    : Integer;
  i          : Integer;
  shift      : Byte;
  code       : Integer;
  v          : Integer;
  hex        : AnsiString;
  defaultHex : AnsiString;
  sym        : TSymbol;
begin
  if (varDecl.address <> '') then begin
    // is absolute address variable
    if FcurrentScope.scopeLevel = 1 then begin
      emitCode('.var '+varDecl.varName+' ='+varDecl.address);

      for i := 0 to varDecl.size - 1 do
        emitCode('.var '+varDecl.varName+'.b'+IntToStr(i)+' = '+varDecl.address);
    end else begin
      // is proc scope
      emitCode('.label '+varDecl.varName+' ='+varDecl.address);

      for i := 0 to varDecl.size - 1 do
        emitCode('.label '+varDecl.varName+'.b'+IntToStr(i)+' = '+varDecl.address);
    end;
    Exit;
  end;

  if (varDecl.varSym <> nil) and (varDecl.varSym.symType is TRecordSymbol) then begin
    // is record so emit record header and children var decls
    Fwriter.write(indent+'%s: { ',[varDecl.varName]);
    incIndent;

    Fwriter.writeLn;

    for i := 0 to TRecordSymbol(varDecl.varSym.symType).vars.Count - 1 do
      emitVarDecl(TVarDecl(TRecordSymbol(varDecl.varSym.symType).vars.Items[i]));

    decIndent;
    Fwriter.writeLn(' }');
    Exit;
  end;

  if (varDecl.varSym = nil) then
    varSize := varDecl.size
  else
    varSize := varDecl.varSym.size;

  Fwriter.write(indent+'%s: { ',[varDecl.varName]);

  if varDecl.defaultValue = '' then begin
    // fill with default 0 value
    if varDecl.isArray then begin
      if not varDecl.isPointer then begin
        // fill array bytes
        Fwriter.write('.byte ');
        for i := 0 to (varDecl.arraySize * varSize) - 1 do begin
          Fwriter.write('0');
          if i < (varDecl.arraySize * varSize) - 1 then Fwriter.write(',');
        end;
      end;
    end
    else begin
      for i := 0 to varSize - 1 do begin
        Fwriter.write('b%d: .byte 0',[i]);
        if (i < varSize - 1) then Fwriter.write(';');
      end;
    end;
  end else begin
    // fill with defaultValue

    defaultHex := StrIntToHex(varDecl.defaultValue);
    while Length(defaultHex) < (varSize * 2) do
      defaultHex := '0' + defaultHex;

    for i := 0 to varSize - 1 do begin
      shift := i * 8;
      hex := Copy(defaultHex,Length(defaultHex) - 1-i*2,2);

      Fwriter.write('b%d: .byte $%s',[i,hex]);
      if (i < varSize - 1) then Fwriter.write(';');
    end;
  end;

  Fwriter.writeLn(' }');
end;

procedure TASTto6502.visitVarDecl  (node : TVarDecl);
begin
  emitVarDecl(node);
end;

procedure TASTto6502.visitConstDecl(node : TConstDecl);
var
  varSize  : Integer;
  i        : Integer;
  num      : TNumber;
  c        : Byte;
  constSym : TConstSymbol;
begin
  if (node.constType <> nil) then begin
    // typed constant
    varSize := node.constType.size;

    Fwriter.write(indent+'%s: { ',[node.constName]);

    if not node.isArray then begin
      if (node.constValue.tType = ttString) then begin
        Fwriter.write('.byte %d',[Length(node.constValue.tValue)]);

        for i := 1 to Length(node.constValue.tValue) do begin

          // 'strings' default to CBM screen code
          c := cPETSCIIToCBMScreenCode[cASCIIToPETSCIITable[Ord(node.constValue.tValue[i])]];

          constSym := TConstSymbol(FcurrentScope.lookup(node.constName));

          if (constSym.symType.symName = 'ascii') then
            // replace char with ascii code
            c := cPETSCIIToCBMScreenCode[cASCIIToPETSCIITable[Ord(node.constValue.tValue[i])]]
          else
          if (constSym.symType.symName = 'petscii') then
            // replace char with petscii code
            c := cASCIIToPETSCIITable[Ord(node.constValue.tValue[i])];

          Fwriter.write(',$%.2x',[c]);
        end;
      end else begin
        // non-string typed const
        if (varSize = 1) then
          Fwriter.writeLn('.byte <%s',[node.constValue.tValue])
        else
          Fwriter.writeLn('.byte <%s, >%s',[node.constValue.tValue,node.constValue.tValue]);
      end;
    end else begin
      // array typed constant
      Fwriter.write('.byte ');
      for i := 0 to high(node.values) do begin
        if (varSize = 1) then
          Fwriter.write('<%s',[node.values[i].value])
        else
          Fwriter.write('<%s, >%s',[node.values[i].value,node.values[i].value]);

        if i < high(node.values) then Fwriter.write(',');
      end;
      Fwriter.writeLn();
    end;

    Fwriter.writeLn(' }');
  end else begin
    // normal constant
    if (node.constValue.tType = ttString) then
      emitCode(Format('.const %s = "%s"',[node.constName,node.constValue.tValue]))
    else
      emitCode(Format('.const %s = %s',[node.constName,node.constValue.tValue]));
  end;
end;

procedure TASTto6502.visitProcDecl (node : TProcDecl);
var
  procSym : TProcSymbol;
begin
  procSym := TProcSymbol(FcurrentScope.lookup(node.procName));

  if (procSym.refCount = 0) and (procSym.procType <> ttAssembler) then
  // if procedure not used then don't generate code!!
    Exit;

  if (procSym.procType = ttAssembler) and (procSym.procAddress <> '') then
  // don't emit any procedure decl stuff for absolute asm procs
    Exit;

  FcurrentScope := node.scope;

  Fwriter.writeLn();
  emitCode(node.procName+': {');
  incIndent;

  FblockIRQType := procSym.irqType;

  FfoundEndIrq := False;

  node.block.acceptVisitor(self);

  if (procSym.irqType = ttRasterIRQ) then begin
//    emitCode(':restoreStack()');
{    if not FfoundEndIrq then begin
      emitCode('lda #$00');
      emitCode('ldx #$00');
      emitCode('ldy #$00');
      emitCode('rti');
    end
      }

    emitCode('end:');
  end
  else begin
    emitCode('rts');
  end;

  decIndent;
  Fwriter.writeLn(indent+'}');

  FblockIRQType := ttNone;

  FcurrentScope := node.scope.enclosingScope;
end;

procedure TASTto6502.visitMacroDecl (node : TMacroDecl);
var
  macroSym : TMacroSymbol;
  i        : Integer;
begin
  macroSym := TMacroSymbol(FcurrentScope.lookup(node.macroName));

  Fwriter.writeLn();
  Fwriter.write('.macro ' + node.macroName + '(');

  for i := 0 to macroSym.paramCount - 1 do begin
    Fwriter.write(macroSym.params[i]);
    if (i < macroSym.paramCount - 1) then Fwriter.write(',');
  end;

  Fwriter.writeLn(') {');

  if macroSym.macroType = ttMacro then incIndent;

  node.block.acceptVisitor(self);

  if macroSym.macroType = ttMacro then decIndent;

  Fwriter.writeLn('}');
end;

procedure TASTto6502.visitFuncDecl (node : TFuncDecl);
var
  funcSym : TFuncSymbol;
begin
  funcSym := TFuncSymbol(FcurrentScope.lookup(node.funcName));

{  if (funcSym.refCount = 0) and (funcSym.funcType <> ttAssembler) then
  // if function not used then don't generate code!!
    Exit;}

  if (funcSym.funcType = ttAssembler) and (funcSym.funcAddress <> '') then
  // don't emit any procedure decl stuff for absolute asm procs
    Exit;

  FcurrentScope := node.scope;

  Fwriter.writeLn();
  Fwriter.writeLn(indent+node.funcName+': {');
  incIndent;

  node.block.acceptVisitor(self);

  emitCode('rts');
  decIndent;
  Fwriter.writeLn(indent+'}');

  FcurrentScope := node.scope.enclosingScope;
end;

procedure writeInteger(aWriter : TsesStringWriter; aInt : Integer);
begin
end;

procedure writeString(aWriter : TsesStringWriter; aString : AnsiString);
begin
  if (Length(aString) = 0) then Exit;

  aWriter.writeLn(indent+'{');
  aWriter.writeLn(indent+'lda #<aString');
  aWriter.writeLn(indent+'ldy #>aString');
  aWriter.writeLn(indent+'jsr $AB1E');
  aWriter.writeLn(indent+'jmp !+');
  aWriter.writeLn(indent+'');
  decIndent;
  aWriter.writeLn(indent+'.encoding "petscii_mixed"');
  aWriter.writeLn(indent+'aString: .text "'+aString+'"');
  aWriter.writeLn(indent+'         .byte 0');
  aWriter.writeLn(indent+'.encoding "screencode_mixed"');
  incIndent;
  aWriter.writeLn(indent+'');
  decIndent;
  aWriter.writeLn(indent+'!:');
  incIndent;
  aWriter.writeLn(indent+'}');
end;

procedure TASTto6502.doWrite(node : TBuiltinProcedure; carridgeReturn : Boolean);
var
  i        : Integer;
  param    : TAST;
  sym      : TSymbol;
  lLabel   : AnsiString;
begin
  for i := 0 to node.params.Count - 1 do begin
    param := TAST(node.params.Items[i]);

    if (param.isLeaf) then begin
      // is a single variable/number/string to output

      if (param.isInteger) then begin
        writeInteger(Fwriter,TNumber(param).value);
      end
      else
      if (param.isString) then begin
        writeString(Fwriter,TString(param).value);
      end
      else begin
        // is variable
        sym := FcurrentScope.lookup(TVariable(param).token.tValue);

        if (sym.symType.symName = 'string') then begin
          lLabel   := newLabel;

          emitCode('ldx #1');
          emitLabel(lLabel);
          emitCode('txa // save x');
          emitCode('pha');
          emitCode('lda '+TVariable(param).token.tValue+',x');
          emitCode('jsr $ffd2');
          emitCode('pla // restore x');
          emitCode('tax');
          emitCode('inx');
          emitCode('cpx '+TVariable(param).token.tValue);
          emitCode('bne '+lLabel);
        end
        else
        if (param.size = 1) then begin
          emitCode('ldx '+TVariable(param).token.tValue);
          emitCode('lda #0');
          emitCode('jsr $bdcd // PRINTWORD');
        end
        else begin
          emitCode('ldx '+TVariable(param).token.tValue + ' + 0');
          emitCode('lda '+TVariable(param).token.tValue + ' + 1');
          emitCode('jsr $bdcd // PRINTWORD');
        end;
      end;
    end
  end;
  if carridgeReturn then begin
    emitCode('lda #13');
    emitCode('jsr $ffd2// CHROUT');
  end;
end;

procedure TASTto6502.visitBuiltinProcedure(node : TBuiltinProcedure);
begin
  if (node.procType = ttWriteLn)    then doWrite(node,True)
  else if (node.procType = ttWrite) then doWrite(node,False)
  else begin
{    case node.procType of
      ttDisableIrq   : emitCode('sei');
      ttEnableIrq    : emitCode('cli');
      ttVic38Columns : begin
        emitCode('// enable 38-column mode');
        emitCode('lda $d016');
        emitCode('and #%11110111');
        emitCode('sta $d016');
        emitCode('//');
      end;
      ttVic40Columns : begin
        emitCode('// enable 40-column mode');
        emitCode('lda $d016');
        emitCode('ora #%00001000');
        emitCode('sta $d016');
        emitCode('//');
      end;
    end;}
  end;
end;

procedure TASTto6502.visitImportBinary(node : TImportBinary);
begin
(*
 *=$a000 "Titlescreen Music"
 titleMusic: {
   .import binary "audio\ALBINO_4.sid", $7e // get rid of sid header (skip $7e bytes)
 }
*)

  if (node.address <> '') then
    Fwriter.writeLn(Format('* = %s "%s"',[node.address,node.name]));

  Fwriter.writeLn(node.name+':{');
  Fwriter.writeLn(Format('  .import binary "%s",%s',[node.fileName,node.byteSkip]));
  Fwriter.writeLn('  .label size = %d',[node.fileSize]);

  if node.isSid then begin
    // is sid so add init/play addresses
    Fwriter.writeLn('// labels used if binary is a sid file');
    Fwriter.writeLn('  .label init = '+node.name);
    Fwriter.writeLn('  .label play = '+node.name+' + 3');
  end;
  Fwriter.writeLn('}');
end;

procedure TASTto6502.visitImportSource(node : TImportSource);
begin
  Fwriter.writeLn('.import source "'+node.fileName+'"');
end;

procedure TASTto6502.visitExit(node: TExit);
begin
  emitCode('rts');
end;

procedure TASTto6502.visitBreak(node: TBreak);
begin
  emitCode('jmp '+peekLoopExitLabel);
end;

procedure TASTto6502.visitSetupRasterIRQ(node: TSetupRasterIRQ);
begin
  emitCode(Format(':SetupRasterIRQ(%s,%s)',[node.routineName+'.'+MAIN_LABEL,node.rasterLine]));
end;

procedure TASTto6502.visitEndIRQ(node: TEndIRQ);
begin
  FfoundEndIrq := True;
//  emitCode(':restoreStack()');
  if node.useSameRaster then
    emitCode(Format(':irq_end2(%s)',[node.routineName+'.'+MAIN_LABEL]))
  else
    emitCode(Format(':irq_end(%s,%s)',[node.routineName+'.'+MAIN_LABEL,node.rasterLine]));
//  emitCode(Format(':EndIRQ(%s,%s)',[node.routineName+'.'+MAIN_LABEL,node.rasterLine]));
end;

//----------------------------------------
procedure TASTto6502.genCode(aNode : TAST; aAsmStream : TStream);
begin
  FasmStream := aAsmStream;
  Fwriter    := TsesStringWriter.create(FasmStream);
  FcodeGen   := TCodeGen_6502.Create(Fwriter);

  if (aNode <> nil) then
    aNode.acceptVisitor(self);
end;

end.
