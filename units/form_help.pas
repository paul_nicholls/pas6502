unit form_help;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls, RichEdit;

type
  Thelp_Form = class(TForm)
    Panel2: TPanel;
    close_Button: TButton;
    help_RichEdit: TRichEdit;
    procedure FormCreate(Sender: TObject);
    procedure close_ButtonClick(Sender: TObject);
  private
    { Private declarations }
  protected
    procedure WndProc(var Message: TMessage); override;
  public
    { Public declarations }
  end;

var
  help_Form: Thelp_Form;

implementation

{$R *.dfm}

uses
  ShellAPI;

procedure Thelp_Form.close_ButtonClick(Sender: TObject);
begin
  hide;
end;

procedure Thelp_Form.FormCreate(Sender: TObject);
var
  mask: Word;
begin
  mask := SendMessage(help_RichEdit.Handle, EM_GETEVENTMASK, 0, 0);
  SendMessage(help_RichEdit.Handle, EM_SETEVENTMASK, 0, mask or ENM_LINK);
  SendMessage(help_RichEdit.Handle, EM_AUTOURLDETECT, Integer(True), 0);

  help_RichEdit.lines.LoadFromFile(ExtractFilePath(ParamStr(0))+'readme_help.rtf');
end;

procedure Thelp_Form.WndProc(var Message: TMessage);
var
 p      : TENLink;
 strURL : string;
begin
 if (Message.Msg = WM_NOTIFY) then
 begin
   if (PNMHDR(Message.LParam).code = EN_LINK) then
   begin
     p := TENLink(Pointer(TWMNotify(Message).NMHdr)^);
     if (p.msg = WM_LBUTTONDOWN) then
     begin
       SendMessage(help_RichEdit.Handle, EM_EXSETSEL, 0, LongInt(@(p.chrg)));
       strURL := help_RichEdit.SelText;
       ShellExecute(Handle, 'open', PChar(strURL), 0, 0, SW_SHOWNORMAL);
     end;
   end;
 end;

 inherited;
end;

end.
