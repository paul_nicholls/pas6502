unit form_options;

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses Winapi.Windows, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Forms,
  Vcl.Controls, Vcl.StdCtrls, Vcl.Buttons, Vcl.ExtCtrls, Vcl.Dialogs;

type
  TOptionsDialog = class(TForm)
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    OKBtn: TButton;
    CancelBtn: TButton;
    GroupBox2: TGroupBox;
    KickAssemblerJarFile_Edit: TEdit;
    KickAssemblerFileBrowse_Button: TButton;
    Label1: TLabel;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    X64File_Edit: TEdit;
    X64FileBrowse_Button: TButton;
    OpenDialog: TOpenDialog;
    GroupBox4: TGroupBox;
    Label3: TLabel;
    ExomizerFile_Edit: TEdit;
    ExomizerBrowse_Button: TButton;
    procedure KickAssemblerFileBrowse_ButtonClick(Sender: TObject);
    procedure X64FileBrowse_ButtonClick(Sender: TObject);
    procedure ExomizerBrowse_ButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  OptionsDialog: TOptionsDialog;

implementation

{$R *.dfm}

procedure TOptionsDialog.KickAssemblerFileBrowse_ButtonClick(Sender: TObject);
begin
  OpenDialog.Title    := 'Browse for Kick Assembler Jar file (KickAss.jar)';
  OpenDialog.Filter   := 'Kick Assembler Jar (KickAss.jar)|*.jar';
  OpenDialog.FileName := 'KickAss.jar';

  if OpenDialog.Execute then begin
    KickAssemblerJarFile_Edit.Text := OpenDialog.FileName;
  end;
end;

procedure TOptionsDialog.X64FileBrowse_ButtonClick(Sender: TObject);
begin
  OpenDialog.Title    := 'Browse for VICE C64 Emulator file (x64.exe)';
  OpenDialog.Filter   := 'Vice C64 Emulator (x64.exe)|*.exe';
  OpenDialog.FileName := 'x64.exe';

  if OpenDialog.Execute then begin
    X64File_Edit.Text := OpenDialog.FileName;
  end;
end;

procedure TOptionsDialog.ExomizerBrowse_ButtonClick(Sender: TObject);
begin
  OpenDialog.Title    := 'Browse for Exomizer.exe file (Exomizer.exe)';
  OpenDialog.Filter   := 'Exomizer (Exomizer.exe)|*.exe';
  OpenDialog.FileName := 'Exomizer.exe';

  if OpenDialog.Execute then begin
    ExomizerFile_Edit.Text := OpenDialog.FileName;
  end;
end;

end.
