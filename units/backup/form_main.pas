unit form_main;

{$MODE Delphi}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface
//-----------------------------------------------------------------------------
// uses SynEdit components found here:
// https://github.com/SynEdit/SynEdit
//-----------------------------------------------------------------------------

uses
  {Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,}uParser,uParserPas6502, {Vcl.ExtCtrls,
  Vcl.StdCtrls,} SynEdit, SynMemo, {SynHighlighterAsm,} SynEditHighlighter,
  {SynEditCodeFolding,} SynHighlighterPas, LCLIntf, ExtCtrls, StdCtrls, Menus,
  ActnList, StdActns, Controls, uExpressions, {Vcl.StdActns,
  System.Actions, Vcl.ActnList, Vcl.Menus, System.ImageList, Vcl.ImgList,System.Types,System.StrUtils,}form_options,
  uCodeInsight, SysUtils;

type
  TPas6502_Form = class(TForm)
    Panel1: TPanel;
    Compile_Button: TButton;
    CompileAndRun_Button: TButton;
    Panel2: TPanel;
    GroupBox2: TGroupBox;
    Splitter1: TSplitter;
    GroupBox3: TGroupBox;
    SourceCode_SynMemo: TSynMemo;
    SynPasSyn1: TSynPasSyn;
    AsmOutput_Memo: TMemo;
    Splitter2: TSplitter;
    GroupBox1: TGroupBox;
    Error_Memo: TMemo;
    MainMenu: TMainMenu;
    ActionList1: TActionList;
    FileOpen1: TFileOpen;
    FileSaveAs1: TFileSaveAs;
    FileExit1: TFileExit;
    ImageList1: TImageList;
    File1: TMenuItem;
    Open1: TMenuItem;
    SaveAs1: TMenuItem;
    Exit1: TMenuItem;
    Options1: TMenuItem;
    OptionsAction: TAction;
    CodeInsight_PopupMenu: TPopupMenu;
    CodeInsight_Timer: TTimer;
    New_Action: TAction;
    NewProject1: TMenuItem;
    PackFile_CheckBox: TCheckBox;
    GenerateCRTFile_CheckBox: TCheckBox;
    procedure Compile_ButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SourceCode_SynMemoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CompileAndRun_ButtonClick(Sender: TObject);
    procedure OptionsActionExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FileSaveAs1Accept(Sender: TObject);
    procedure FileOpen1Accept(Sender: TObject);
    procedure FileSaveAs1BeforeExecute(Sender: TObject);
    procedure CodeInsight_TimerTimer(Sender: TObject);
    procedure New_ActionExecute(Sender: TObject);
  private
    { Private declarations }
    FKickAssemblerJarFile : String;
    FViceX64ExeFile       : String;
    FExomizerExeFile      : String;
    FProjectFileName      : String;
    FCodeInsightData: TCodeInsightData;

    procedure CodeInsight_OnClick(Sender: TObject);
    procedure GetCodeInsightDataFromLine(line : String; var insightData : TStringDynArray);
    procedure SetCaption(ACaption : String);
    function StartProcess(ExeName: string; CmdLineArgs: string = '';
      ShowWindow: boolean = True; WaitForFinish: boolean = False): integer;
    procedure CopyAsmImportsToProjectFolder(ProjectFolder : String);
    function  CreateAndCompileAsmFile : Boolean;
    function  PackCompiledProgram : Boolean;
    function  CreateAndCompileCRTFile : Boolean;
    procedure ExecuteCompiledAsmFile;
    procedure CompileProgram(ExecuteProgram : Boolean);
  public
    { Public declarations }
  end;

var
  Pas6502_Form: TPas6502_Form;

implementation

{$R *.lfm}

uses IniFiles,
     dprocess;  // this is the TProcess unit from FPC, now ported to delphi

const
  cImportFolder = 'imports\';
  cImportFiles : array[1..5] of String = (
    'pas6502_rtl.asm',
    'pas6502_cartridge_boot.asm',
    'pas6502_interrupts.asm',
    'pas6502_joystick.asm',
    'pas6502_random.asm'
  );

procedure OutLn(s: string); overload;
begin
  Pas6502_Form.Error_Memo.lines.add(s);
end;

procedure OutLn(s: string; i: integer); overload;
begin
  outln(s + inttostr(i));
end;

procedure DoLog(s: string);
begin
  OutLn('Log: '+s);
end;

function RunProcess(const Binary: string; args: TStrings): boolean;
const
  BufSize = 1024;
var
  p: TProcess;
  // Buf: string;  // L505 note: must use ansistring
  Buf: ansistring; //
  Count: integer;
  i: integer;
  LineStart: integer;
  // OutputLine: string;  //L505 note: must use ansistring
  OutputLine: ansistring; //

begin
  p := TProcess.Create(nil);
  try
    p.Executable := Binary;

    p.Options := [poUsePipes,
                  poStdErrToOutPut];
//    p.CurrentDirectory := ExtractFilePath(p.Executable);
    p.ShowWindow := swoHIDE {ShowNormal};

    p.Parameters.Assign(args);
//    DoLog('Running command '+ p.Executable +' with arguments: '+ p.Parameters.Text);
    p.Execute;

    { Now process the output }
    OutputLine:='';
    SetLength(Buf,BufSize);
    repeat
      if (p.Output<>nil) then
      begin
        // Count:=p.Output.Read(Buf[1],Length(Buf));
        Count:=p.Output.Read(pchar(Buf)^, BufSize);  //L505 changed to pchar because of unicodestring
        // outln('DEBUG: len buf: ', length(buf));
      end
      else
        Count:=0;
      LineStart:=1;
      i:=1;
      while i<=Count do
      begin
        // L505
        //if Buf[i] in [#10,#13] then
        if CharInSet(Buf[i], [#10,#13]) then
        begin
          OutputLine:=OutputLine+Copy(Buf,LineStart,i-LineStart);
          outln(OutputLine);
          OutputLine:='';
          // L505
          //if (i<Count) and (Buf[i+1] in [#10,#13]) and (Buf[i]<>Buf[i+1]) then
          if (i<Count) and (CharInset(Buf[i], [#10,#13])) and (Buf[i]<>Buf[i+1]) then
            inc(i);
          LineStart:=i+1;
        end;
        inc(i);
      end;
      OutputLine:=Copy(Buf,LineStart,Count-LineStart+1);
    until Count=0;
    if OutputLine <> '' then
      outln(OutputLine);
//  else
//    outln('DEBUG: empty line');
    p.WaitOnExit;
    Result := p.ExitStatus = 0;
    if not Result then
      outln('Command '+ p.Executable +' failed with exit code: ', p.ExitStatus);
  finally
    FreeAndNil(p);
  end;
end;

const
{$ifdef MSWINDOWS}prog = 'cmd';{$endif}

{$ifdef MACOS}prog = 'ls';{$endif}

var args: TStringList;

{$ifdef MSWINDOWS}
procedure AddArg(arg : String);
begin
  args.add(arg);
end;
{$endif}

procedure ClearArgs;
begin
  args.Clear;
end;

procedure TPas6502_Form.CodeInsight_OnClick(Sender: TObject);
var
  InsertionPos  : TBufferCoord;
  InsertionText : String;
begin
  InsertionText := TMenuItem(Sender).Caption;
  InsertionPos  := SourceCode_SynMemo.CaretXY;

  SourceCode_SynMemo.InsertBlock(InsertionPos,InsertionPos,PWideChar(InsertionText),True);
end;

procedure TPas6502_Form.GetCodeInsightDataFromLine(line : String; var insightData : TStringDynArray);
var
  i    : Integer;
  data : String;
begin
  SetLength(insightData,0);

  if line = '' then Exit;

  i := Length(line);
  while (i > 0) do begin
    if line[i] = ' ' then begin
      line := Copy(line,i,Length(line));
      Break;
    end;
    Dec(i);
  end;

  insightData := SplitString(line,'.');

  for i := 0 to High(insightData) do
    insightData[i] := Trim(insightData[i]);

  if insightData[High(insightData)] = '' then
    SetLength(insightData,High(insightData));
end;

procedure TPas6502_Form.New_ActionExecute(Sender: TObject);
begin
  FProjectFileName := 'Untitled.dpr';
  SetCaption(FProjectFileName);
  SourceCode_SynMemo.Clear;
end;

procedure TPas6502_Form.CodeInsight_TimerTimer(Sender: TObject);
var
  apos               : TPoint;
  caretxy            : TDisplayCoord;
  line               : String;
  codeInsightStrings : TStringDynArray;
  codeInsightData    : TCodeInsightData;
  insightData        : TCodeInsightData;
  insightStrings     : TStringDynArray;
  i                  : Integer;
  NewItem            : TMenuItem;
begin
  CodeInsight_Timer.Enabled := False;

  caretxy.Column := SourceCode_SynMemo.CaretX;
  caretxy.Row    := SourceCode_SynMemo.CaretY;

  line := SourceCode_SynMemo.LineText;

  if (line[Length(line)] <> '.') then
  // not at a period right now so exit
    Exit;

  GetCodeInsightDataFromLine(line,codeInsightStrings);

  if Length(codeInsightStrings) = 0 then Exit;

  SetLength(insightStrings,0);

  insightStrings := FCodeInsightData.GetDataAsArray;

  i := 0;
  repeat
    insightData := FCodeInsightData.GetDataByName(codeInsightStrings[i]);

    if (insightData <> Nil) then begin
      insightStrings := insightData.GetDataAsArray;
    end
    else begin
      SetLength(insightStrings,0);
      Break;
    end;
    Inc(i);
  until i > High(codeInsightStrings);

  apos := SourceCode_SynMemo.ClientToScreen(SourceCode_SynMemo.RowColumnToPixels(caretxy));

  CodeInsight_PopupMenu.Items.Clear;

  for i := 0 to High(insightStrings) do begin
    NewItem := TMenuItem.Create(Nil);
    NewItem.Caption := insightStrings[i];
    NewItem.OnClick := CodeInsight_OnClick;

    CodeInsight_PopupMenu.Items.Add(NewItem);
  end;

  CodeInsight_PopupMenu.Popup(apos.X,apos.y);
end;

procedure TPas6502_Form.CompileAndRun_ButtonClick(Sender: TObject);
begin
  CompileProgram(true);
  // put focus back onto source code memo
  SourceCode_SynMemo.SetFocus;
end;

procedure TPas6502_Form.Compile_ButtonClick(Sender: TObject);
begin
  CompileProgram(false);
  // put focus back onto source code memo
  SourceCode_SynMemo.SetFocus;
end;

procedure TPas6502_Form.CopyAsmImportsToProjectFolder(ProjectFolder : String);
var
  i : Integer;
  srcFile : String;
  dstFile : String;
begin
  for i := 1 to High(cImportFiles) do begin
    srcFile := ExtractFilePath(ParamStr(0))+cImportFolder+cImportFiles[i];
    dstFile := ProjectFolder + cImportFiles[i];
    CopyFile(PWideChar(srcFile),
             PWideChar(dstFile),False);
  end;
end;

function  TPas6502_Form.CreateAndCompileAsmFile : Boolean;
var
  asmFileName   : String;
  ProjectFolder : String;
begin
  asmFileName   := ChangeFileExt(FProjectFileName,'.asm');
  ProjectFolder := ExtractFilePath(FProjectFileName);

  CopyAsmImportsToProjectFolder(ProjectFolder);

  AsmOutput_Memo.Lines.SaveToFile(asmFileName);

  ClearArgs;
  AddArg('"'+FKickAssemblerJarFile+'"');
  AddArg('"'+asmFileName+'"');

  Result := RunProcess('compile.bat',args)
end;

function  TPas6502_Form.PackCompiledProgram : Boolean;
var
  prgFileName    : String;
  packedFileName : String;
  ProjectFolder  : String;
begin
  prgFileName    := ChangeFileExt(FProjectFileName,'.prg');

  packedFileName := ChangeFileExt(FProjectFileName,'.exo');

  ClearArgs;
  AddArg('"'+FExomizerExeFile+'"');
  AddArg('"'+prgFileName+'"');
  AddArg('"'+packedFileName+'"');

  Result := RunProcess('pack.bat',args);
end;

function  TPas6502_Form.CreateAndCompileCRTFile : Boolean;
var
  prgFileName   : String;
  ProjectFolder : String;
  crtBootFile   : TStringList;
  asmFileName   : String;
  binFileName   : String;
  crtFileName   : String;
  cartconvName  : String;
begin
  ProjectFolder := ExtractFilePath(FProjectFileName);

  if PackFile_CheckBox.Checked then
    prgFileName  := ChangeFileExt(FProjectFileName,'.exo')
  else
    prgFileName  := ChangeFileExt(FProjectFileName,'.prg');

  cartconvName := ExtractFilePath(FViceX64ExeFile) + 'cartconv.exe';
  asmFileName := ProjectFolder + 'pas6502_cartridge_boot.asm';
  binFileName := ProjectFolder + 'pas6502_cartridge_boot.bin';
  crtFileName := ChangeFileExt(FProjectFileName,'.crt');

  crtBootFile := TStringList.Create;
  try
    crtBootFile.LoadFromFile(asmFileName);
    crtBootFile.Text := ReplaceStr(crtBootFile.Text,'<EXOMIZED DATA>',prgFileName);
    crtBootFile.SaveToFile(asmFileName);
  finally
    crtBootFile.Free;
  end;

  ClearArgs;
  AddArg('"'+FKickAssemblerJarFile+'"');
  AddArg('"'+asmFileName+'"');
  AddArg('"'+cartconvName+'"');
  AddArg('"'+binFileName+'"');
  AddArg('"'+crtFileName+'"');

  Result := RunProcess('compileAndCreateCRTFile.bat',args);
end;

procedure TPas6502_Form.ExecuteCompiledAsmFile;
var
  prgFileName   : String;
begin
  if GenerateCRTFile_CheckBox.Checked then begin
    prgFileName  := ChangeFileExt(FProjectFileName,'.crt');

     OpenDocument(PChar('"'+FViceX64ExeFile+'"')); { *Converted from ShellExecute* }
  end
  else begin
    if PackFile_CheckBox.Checked then
      prgFileName  := ChangeFileExt(FProjectFileName,'.exo')
    else
      prgFileName  := ChangeFileExt(FProjectFileName,'.prg');

      ClearArgs;
      AddArg('"'+FViceX64ExeFile+'"');
      AddArg('"'+prgFileName+'"');

      RunProcess('executeProgram.bat',args);
  end;
end;

procedure TPas6502_Form.CompileProgram(ExecuteProgram : Boolean);
var
  srcStream : TMemoryStream;
  asmStream : TMemoryStream;
  parser    : TBaseParser;
  cursor    : TCursor;
begin
  if not FileExists(FKickAssemblerJarFile) or not FileExists(FViceX64ExeFile) or not FileExists(FExomizerExeFile) then begin
    ShowMessage('Use "Options" to configure where the Kick Assembler, VICE emulator, and Exomizer files are found!');
    Exit;
  end;

  AsmOutput_Memo.Clear;
  Error_Memo.Text := '';

  cursor := Screen.Cursor;
  try
    Screen.Cursor := crHourGlass;

    parser := TParserPas6502.Create(SourceCode_SynMemo.Lines,GenerateCRTFile_CheckBox.Checked);

    try
      srcStream := TMemoryStream.Create;
      asmStream := TMemoryStream.Create;

      try
        SourceCode_SynMemo.Lines.SaveToStream(srcStream);

        parser.ProjectPath := ExtractFilePath(FProjectFileName);
        parser.ParserPath  := ExtractFilePath(ParamStr(0));

        if not(parser.ParseProgram(srcStream,asmStream)) then
          Error_Memo.Text := parser.ErrorMsg
        else begin
          Error_Memo.Text := 'Compile successful!';
          Error_Memo.Lines.Add('');

          AsmOutput_Memo.Clear;
          AsmOutput_Memo.Lines.LoadFromStream(asmStream);

          if (CreateAndCompileAsmFile) then begin
            if PackFile_CheckBox.Checked        then PackCompiledProgram;
            if GenerateCRTFile_CheckBox.Checked then CreateAndCompileCRTFile;
            if ExecuteProgram                   then ExecuteCompiledAsmFile;
          end;
        end;
      finally
        srcStream.Free;
        asmStream.Free;
      end;
    finally
      parser.Free;
    end;
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TPas6502_Form.SetCaption(ACaption : String);
begin
  Caption := 'Pas 6502 Compiler - "' + ExtractFileName(ACaption) + '"';
end;

procedure TPas6502_Form.FileOpen1Accept(Sender: TObject);
begin
  SourceCode_SynMemo.Lines.LoadFromFile(FileOpen1.Dialog.FileName);
  FProjectFileName := FileOpen1.Dialog.FileName;
  SetCaption(FProjectFileName);
end;

procedure TPas6502_Form.FileSaveAs1Accept(Sender: TObject);
begin
  SourceCode_SynMemo.Lines.SaveToFile(FileSaveAs1.Dialog.FileName);
  FProjectFileName := FileSaveAs1.Dialog.FileName;
  SetCaption(FProjectFileName);
end;

procedure TPas6502_Form.FileSaveAs1BeforeExecute(Sender: TObject);
begin
  FileSaveAs1.Dialog.FileName := ExtractFileName(FProjectFileName);
end;

procedure TPas6502_Form.FormCreate(Sender: TObject);
const
  cC64Color : array[0..15] of String = (
    'BLACK',
    'WHITE',
    'RED',
    'CYAN',
    'PURPLE',
    'GREEN',
    'BLUE',
    'YELLOW',
    'ORANGE',
    'BROWN',
    'LIGHT_RED',
    'DARK_GREY',
    'GREY',
    'LIGHT_GREEN',
    'LIGHT_BLUE',
    'LIGHT_GREY'
  );

var
  i       : Integer;
  IniFile : TIniFile;
begin
  FKickAssemblerJarFile := '';
  FViceX64ExeFile       := '';
  FExomizerExeFile      := '';

  if FileExists(ExtractFilePath(ParamStr(0)) + 'Pas6502.ini') then begin
    IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Pas6502.ini');

    FKickAssemblerJarFile := IniFile.ReadString('setup','KickAssemblerFileName','C:\utilities\KickAssembler\KickAss.jar');
    FViceX64ExeFile       := IniFile.ReadString('setup','VICEEmulatorFileName','C:\Emulators\WinVICE-2.2-x64\x64.exe');
    FExomizerExeFile      := IniFile.ReadString('setup','ExomizerFileName','C:\utilities\Exomiser\win32\exomizer.exe');

    FKickAssemblerJarFile := ReplaceStr(FKickAssemblerJarFile,'"','');
    FViceX64ExeFile       := ReplaceStr(FViceX64ExeFile,'"','');
    FExomizerExeFile       := ReplaceStr(FExomizerExeFile,'"','');

    IniFile.Free;
  end;

  args := TStringList.Create;

  FProjectFileName      := ExtractFilePath(ParamStr(0)) + 'Untitled.dpr';

  SetCaption(FProjectFileName);

  FCodeInsightData := TCodeInsightData.Create('c64');

  for i := 0 to High(cC64Color) do
    FCodeInsightData.AddDataByName('color').AddDataByName(cC64Color[i]);
end;

procedure TPas6502_Form.FormShow(Sender: TObject);
begin
  SourceCode_SynMemo.SetFocus;
end;

procedure TPas6502_Form.OptionsActionExecute(Sender: TObject);
var
  IniFile : TIniFile;
begin
  if (OptionsDialog.ShowModal = mrOK) then begin
    FKickAssemblerJarFile := OptionsDialog.KickAssemblerJarFile_Edit.Text;
    FViceX64ExeFile       := OptionsDialog.X64File_Edit.Text;
    FExomizerExeFile      := OptionsDialog.ExomizerFile_Edit.Text;

    IniFile := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Pas6502.ini');

    IniFile.WriteString('setup','KickAssemblerFileName','"'+FKickAssemblerJarFile+'"');
    IniFile.WriteString('setup','VICEEmulatorFileName' ,'"'+FViceX64ExeFile+'"');
    IniFile.WriteString('setup','ExomizerFileName'     ,'"'+FExomizerExeFile+'"');

    IniFile.Free;
  end;
end;

procedure TPas6502_Form.SourceCode_SynMemoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F9) then begin
    if (ssCTRL in Shift) then
      CompileProgram(false)
    else
      CompileProgram(true);
  end
  else
  if (Sender is TSynMemo) then begin
    if (Key = VK_OEM_PERIOD) then
      CodeInsight_Timer.Enabled := True
    else
      CodeInsight_Timer.Enabled := False;
  end;
end;

function TPas6502_Form.StartProcess(ExeName: string; CmdLineArgs: string = '';
  ShowWindow: boolean = True; WaitForFinish: boolean = False): integer;
var
  StartInfo : TStartupInfo;
  ProcInfo  : TProcessInformation;
begin
  //Simple wrapper for the CreateProcess command
  //returns the process id of the started process.
  FillChar(StartInfo,SizeOf(TStartupInfo),#0);
  FillChar(ProcInfo,SizeOf(TProcessInformation),#0);
  StartInfo.cb := SizeOf(TStartupInfo);

  if not(ShowWindow) then begin
    StartInfo.dwFlags := STARTF_USESHOWWINDOW;
    StartInfo.wShowWindow := SW_HIDE;
  end;

  CreateProcess(nil,PChar(ExeName + ' ' + CmdLineArgs),nil,nil,False,
    CREATE_NEW_PROCESS_GROUP + NORMAL_PRIORITY_CLASS,nil,nil,StartInfo,
    ProcInfo);

  Result := ProcInfo.dwProcessId;

  if WaitForFinish then begin
    WaitForSingleObject(ProcInfo.hProcess,Infinite);
  end;

  //close process & thread handles
  FileClose(ProcInfo.hProcess); { *Converted from CloseHandle* }
  FileClose(ProcInfo.hThread); { *Converted from CloseHandle* }
end;

end.
