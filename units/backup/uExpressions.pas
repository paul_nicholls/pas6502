unit uExpressions;

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  System.Classes;

const
  cFalseValue = 0;
  cTrueValue  = -1;

type
  TExpNodeType = (
    ntInt,
    ntBool,
    ntIdent,
    ntOpAdd,
    ntOpSub,
    ntOpMul,
    ntOpDiv,
    ntOpShl,
    ntOpShr,
    ntOpOr,
    ntOpAnd,
    ntOpXor,
    ntOpNot,
    ntOpEql,
    ntOpNeq,
    ntOpLss,
    ntOpLeq,
    ntOpGtr,
    ntOpGeq
  );

  TExpNode = record
    function ToString: String;

    case NodeType: TExpNodeType of
      ntBool,ntInt : (IntValue   : Integer);
      ntIdent      : (IdentValue : ShortString);

      ntOpAdd,
      ntOpSub,
      ntOpMul,
      ntOpDiv,
      ntOpShl,
      ntOpShr,
      ntOpOr,
      ntOpAnd,
      ntOpXor,
      ntOpNot,
      ntOpEql,
      ntOpNeq,
      ntOpLss,
      ntOpLeq,
      ntOpGtr,
      ntOpGeq : (OpValue : ShortString);
  end;

  TExpNodeStack = class(TObject)
  private
  const
    cMAX_SIZE = 100;
  var
    FNodes : array[0..cMAX_SIZE - 1] of TExpNode;
    FIndex : Integer;

    function  GetNode(AIndex : Integer): TExpNode;
  public
    constructor Create;

    function  IsFull : Boolean;
    function  IsEmpty : Boolean;
    function  PopNode : TExpNode;
    procedure PushNode(ANode : TExpNode);     overload;
    procedure PushNode(Value : Integer);      overload;
    procedure PushNode(Value : Boolean);      overload;
    procedure PushNode(Value : String);       overload;
    procedure PushNode(Value : TExpNodeType); overload;

    procedure DebugPrint;

    function  Count : Integer;
    property  Nodes[AIndex : Integer] : TExpNode read GetNode;
  end;

  TExpTreeNode = class(TInterfacedObject)
  private
    FData       : TExpNode;
    FLeftNode   : TExpTreeNode;
    FRightNode  : TExpTreeNode;
    FSymIndex   : TExpTreeNode;
    FDeref      : Boolean;

    procedure DisplayTreeNodePostFix(ANode : TExpTreeNode);
    function  cloneNode(node : TExpTreeNode) : TExpTreeNode;
  public
    // leaf constructors
    constructor Create(Value : TExpNode);                                                                      overload;
    constructor Create(Value : Integer);                                                                       overload;
    constructor Create(Value : Boolean);                                                                       overload;
    constructor Create(Value : String);                                                                        overload;
    // operator constructor
    constructor Create(Value : TExpNodeType; ALeftNode : TExpTreeNode = Nil; ARightNode : TExpTreeNode = Nil); overload;

    function  clone: TExpTreeNode;

    procedure DisplayTree;

    function  IsLeaf: Boolean;

    property Data       : TExpNode     read FData;
    property LeftNode   : TExpTreeNode read FLeftNode;
    property RightNode  : TExpTreeNode read FRightNode;

    property SymIndex : TExpTreeNode read FSymIndex write FSymIndex;
    property Deref    : Boolean      read FDeref    write FDeref;
  end;

  TNodeVisitor = procedure(ANode : TExpTreeNode) of object;

  TExpression = class(TObject)
  private
    FNodes : TExpNodeStack;

    procedure ExpandMultiply(aStack : TExpNodeStack; ident : String; n : Integer);
    procedure DivToShift(aStack : TExpNodeStack; arg1 : TExpNode; n : Integer);
    procedure MulToShift(aStack : TExpNodeStack; arg1 : TExpNode; n : Integer);
    procedure SimplifyUnaryOp(aStack : TExpNodeStack; aOp : TExpNodeType);
    procedure SimplifyBinaryOp(aStack : TExpNodeStack; aOp : TExpNodeType);
  public
    constructor Create;
    destructor  Destroy; override;

    function  PopNode: TExpNode;

    procedure PushNode(Value : Integer);      overload;
    procedure PushNode(Value : Boolean);      overload;
    procedure PushNode(Value : String);       overload;
    procedure PushNode(Value : TExpNodeType); overload;

    procedure Simplify;
    procedure DebugPrint;
  end;

function  ExpandMultiply(node : TExpTreeNode; n : Integer): TExpTreeNode;
function  NewExpNode(Value : Integer): TExpNode;      overload;
function  NewExpNode(Value : Boolean): TExpNode;      overload;
function  NewExpNode(Value : String): TExpNode;       overload;
function  NewExpNode(Value : TExpNodeType): TExpNode; overload;

function  SimplifyUnaryOp(root : TExpTreeNode) : TExpTreeNode;
function  SimplifyBinaryOp(root : TExpTreeNode) : TExpTreeNode;
function  SimplifyTree(const ANode : TExpTreeNode) : TExpTreeNode;
procedure VisitTreeNodes(ANode : TExpTreeNode; AVisitor : TNodeVisitor);

//function  IsRelOp(aOp : TExpNodeType) : Boolean;

implementation

uses
  System.SysUtils,System.Math;

function  IsOp(aOp : TExpNodeType) : Boolean;
begin
  Result := aOp in [
    ntOpAdd,
    ntOpSub,
    ntOpMul,
    ntOpDiv,
    ntOpShl,
    ntOpShr,
    ntOpOr,
    ntOpAnd,
    ntOpXor,
    ntOpEql,
    ntOpNeq,
    ntOpLss,
    ntOpLeq,
    ntOpGtr,
    ntOpGeq
  ];
end;

function  IsRelOp(aOp : TExpNodeType) : Boolean;
begin
  Result := aOp in [
  ntOpEql,
  ntOpNeq,
  ntOpLss,
  ntOpLeq,
  ntOpGtr,
  ntOpGeq
  ];
end;

function  IsBinaryOp(aOp : TExpNodeType) : Boolean;
begin
  Result := aOp in [ntOpAdd,ntOpSub,ntOpMul,ntOpDiv,ntOpShl,ntOpShr,ntOpOr,ntOpAnd,ntOpXor,
  ntOpEql,
  ntOpNeq,
  ntOpLss,
  ntOpLeq,
  ntOpGtr,
  ntOpGeq];
end;

function  IsUnaryOp(aOp : TExpNodeType) : Boolean;
begin
  Result := aOp in [ntOpNot];
end;

function  TExpNode.ToString : String;
begin
  if NodeType = TExpNodeType.ntInt then
    Result := IntToStr(IntValue)
  else
  if NodeType = TExpNodeType.ntBool then begin
    Result := 'False';
    if (IntValue <> 0) then Result := 'True'
  end
  else
  if IsOp(NodeType) then begin
    case NodeType of
      ntOpAdd : Result := '+';
      ntOpSub : Result := '-';
      ntOpMul : Result := '*';
      ntOpDiv : Result := '/';
      ntOpShl : Result := '<<';
      ntOpShr : Result := '>>';
      ntOpOr  : Result := '|';
      ntOpAnd : Result := '&';
      ntOpXor : Result := '^';
      ntOpNot : Result := '!';
      ntOpEql : Result := '=';
      ntOpNeq : Result := '<>';
      ntOpLss : Result := '<';
      ntOpLeq : Result := '<=';
      ntOpGtr : Result := '>';
      ntOpGeq : Result := '>=';
    end;
  end
  else
    Result := IdentValue;
end;

function  NewExpNode(Value : Integer): TExpNode; overload;
begin
  Result.IntValue := Value;
  Result.NodeType := TExpNodeType.ntInt;
end;

function  NewExpNode(Value : Boolean): TExpNode; overload;
begin
  Result.IntValue := cFalseValue;
  Result.NodeType := TExpNodeType.ntBool;

  if Value then  Result.IntValue := cTrueValue;
end;

function  NewExpNode(Value : String): TExpNode; overload;
begin
  Result.IdentValue := Value;
  Result.NodeType   := TExpNodeType.ntIdent;
end;

function  NewExpNode(Value : TExpNodeType): TExpNode; overload;
begin
  Result.NodeType := Value;
  case Value of
    ntOpAdd : Result.OpValue := '+';
    ntOpSub : Result.OpValue := '-';
    ntOpMul : Result.OpValue := '*';
    ntOpDiv : Result.OpValue := '/';
    ntOpShl : Result.OpValue := '<<';
    ntOpShr : Result.OpValue := '>>';
    ntOpOr  : Result.OpValue := '|';
    ntOpAnd : Result.OpValue := '&';
    ntOpXor : Result.OpValue := '^';
    ntOpNot : Result.OpValue := '!';
    ntOpEql : Result.OpValue := '=';
    ntOpNeq : Result.OpValue := '<>';
    ntOpLss : Result.OpValue := '<';
    ntOpLeq : Result.OpValue := '<=';
    ntOpGtr : Result.OpValue := '>';
    ntOpGeq : Result.OpValue := '>=';
  end;
end;

constructor TExpNodeStack.Create;
begin
  FIndex := -1;
end;

function  TExpNodeStack.GetNode(AIndex: Integer) : TExpNode;
begin
  Result := FNodes[AIndex];
end;

function  TExpNodeStack.IsFull : Boolean;
begin
  Result := FIndex >= cMAX_SIZE;
end;

function  TExpNodeStack.IsEmpty : Boolean;
begin
  Result := FIndex = -1;
end;

function  TExpNodeStack.PopNode : TExpNode;
begin
  if IsEmpty then raise Exception.Create('TExpNodeStack: cannot pop an empty stack!');

  Result := FNodes[FIndex];
  Dec(FIndex);
end;

procedure TExpNodeStack.PushNode(ANode: TExpNode);
begin
  Inc(FIndex);
  FNodes[FIndex] := ANode;
end;

procedure TExpNodeStack.PushNode(Value : Integer);
begin
  PushNode(NewExpNode(Value));
end;

procedure TExpNodeStack.PushNode(Value : Boolean);
begin
  PushNode(NewExpNode(Value));
end;

procedure TExpNodeStack.PushNode(Value : String);
begin
  PushNode(NewExpNode(Value));
end;

procedure TExpNodeStack.PushNode(Value : TExpNodeType);
begin
  PushNode(NewExpNode(Value));
end;

function  TExpNodeStack.Count : Integer;
begin
  Result := FIndex + 1;
end;

procedure TExpNodeStack.DebugPrint;
var
  i : Integer;
begin
  Write('(');
  for i := 0 to FIndex do
    Write(Nodes[i].ToString,' ');
  WriteLn(')');
end;

function  IsBoolean(arg : TExpNode): Boolean;
begin
  Result := arg.NodeType = ntBool;
end;

function  IsZero(arg : TExpNode): Boolean;
begin
  Result := (arg.NodeType = ntInt) and (arg.IntValue = 0);
end;

function  IsPositiveNumber(arg : TExpNode): Boolean;
begin
  Result := (arg.NodeType = ntInt) and (arg.IntValue >= 0);
end;

function  IsInteger(arg : TExpNode): Boolean;
begin
  Result := (arg.NodeType = ntInt);
end;

function  IsIdent(arg : TExpNode): Boolean;
begin
  Result := (arg.NodeType = ntIdent);
end;

function  IsPowerOf2(num : Integer): Boolean;
var
  bit   : Integer;
  count : Integer;
begin
  Result := False;

  count := 0;
  bit   := 1;

  if (num = 0) then Exit;

  repeat
    if (num and bit <> 0) then Inc(count);

    if (count > 1) then Exit;

    bit := bit * 2;
  until bit > num;

  Result := True;
end;

function  largestPowerOf2(n : Integer): Integer;
begin
  Result := Trunc(Power(2, Floor(Log10(n) / Log10(2))));
end;

function  PowerOf2ToShift(n : Integer) : Integer;
var
  count : Integer;
begin
  count := -1;

  while n > 0 do begin
    Inc(count);
    n := n div 2;
  end;

  Result := count;
end;

// leaf constructors
constructor TExpTreeNode.Create(Value : TExpNode);
begin
  FData       := Value;
  FLeftNode   := Nil;
  FRightNode  := Nil;
  FSymIndex := Nil;
  FDeref      := False;
end;

constructor TExpTreeNode.Create(Value : Integer);
begin
  FData      := NewExpNode(Value);
  FLeftNode  := Nil;
  FRightNode := Nil;
end;

constructor TExpTreeNode.Create(Value : Boolean);
begin
  FData      := NewExpNode(Value);
  FLeftNode  := Nil;
  FRightNode := Nil;
end;

constructor TExpTreeNode.Create(Value : String);
begin
  FData      := NewExpNode(Value);
  FLeftNode  := Nil;
  FRightNode := Nil;
end;

// operator constructor
constructor TExpTreeNode.Create(Value : TExpNodeType; ALeftNode : TExpTreeNode = Nil; ARightNode : TExpTreeNode = Nil);
begin
  FData      := NewExpNode(Value);
  FLeftNode  := ALeftNode;
  FRightNode := ARightNode;
end;

function  TExpTreeNode.IsLeaf: Boolean;
begin
  Result := (FLeftNode = Nil) and (FRightNode = Nil);
end;

function  ExpandMultiply(node : TExpTreeNode; n : Integer): TExpTreeNode;
var
  p      : Integer;
  bit    : Integer;
  i      : Integer;
  shifts : array of TExpTreeNode;
begin
  if (n = 0) then begin
    Result := TExpTreeNode.Create(0);
    Exit;
  end;

  Result := node.clone;

  if (n = 1) then Exit;

  if IsPowerOf2(n) then begin
    Result := TExpTreeNode.Create(ntOpShl,node.clone,TExpTreeNode.Create(PowerOf2ToShift(n)));
    Exit;
  end;

  SetLength(shifts,0);

  p   := 0;
  bit := (1 shl p);

  while (bit <= n) do begin
    if ((bit and n) <> 0) then begin
      if (p > 0) then begin
        i := Length(shifts);
        SetLength(shifts,i + 1);
        shifts[i]  := TExpTreeNode.Create(ntOpShl,node.clone,TExpTreeNode.Create(p));

//        Result := TExpTreeNode.Create(ntOpAdd,Result,shift);
      end;
    end;

    p   := p + 1;
    bit := (1 shl p);
  end;

  Result := shifts[0];

  if (Length(shifts) = 1) then Exit;

  for i := 1 to High(shifts) do begin
    Result := TExpTreeNode.Create(ntOpAdd,Result,shifts[i]);
  end;
end;

function  DivToShift(arg : TExpTreeNode; n : Integer) : TExpTreeNode;
var
  shift : Integer;
begin
  shift := PowerOf2ToShift(n);

  Result := SimplifyTree(arg);
  
  if (Shift > 0) then
    Result := TExpTreeNode.Create(ntOpShr,Result,TExpTreeNode.Create(shift));
end;

function  SimplifyUnaryOp(root : TExpTreeNode) : TExpTreeNode;
var
  arg : TExpTreeNode;
  op  : TExpNodeType;
begin
  Result := root;

  op  := root.Data.NodeType;
  arg := SimplifyTree(root.LeftNode);

  if (op = ntOpNot) and IsBoolean(arg.Data) then begin
    Result := TExpTreeNode.Create(not(arg.Data.IntValue <> 0));
    Exit;
  end;

  if (op = ntOpNot) and IsInteger(arg.Data) then begin
    Result := TExpTreeNode.Create(not arg.Data.IntValue);
    Exit;
  end;

  // can't simplify any further
  Result := TExpTreeNode.Create(op,arg,nil);
end;

function  SimplifyBinaryOp(root : TExpTreeNode) : TExpTreeNode;
var
  arg1,arg2 : TExpTreeNode;
  op        : TExpNodeType;
begin
  Result := root;

  op   := root.Data.NodeType;
  arg1 := SimplifyTree(root.LeftNode);
  arg2 := SimplifyTree(root.RightNode);

  if (IsInteger(arg1.Data) or IsBoolean(arg1.Data)) and (IsInteger(arg2.Data) or IsBoolean(arg2.Data)) then begin
    // both are numbers/boolean so let's do the math!
    case op of
      ntOpAdd : Result := TExpTreeNode.Create(arg1.Data.IntValue +   arg2.Data.IntValue);
      ntOpSub : Result := TExpTreeNode.Create(arg1.Data.IntValue -   arg2.Data.IntValue);
      ntOpMul : Result := TExpTreeNode.Create(arg1.Data.IntValue *   arg2.Data.IntValue);
      ntOpDiv : Result := TExpTreeNode.Create(arg1.Data.IntValue div arg2.Data.IntValue);
      ntOpShl : Result := TExpTreeNode.Create(arg1.Data.IntValue shl arg2.Data.IntValue);
      ntOpShr : Result := TExpTreeNode.Create(arg1.Data.IntValue shr arg2.Data.IntValue);
      ntOpOr  : Result := TExpTreeNode.Create(arg1.Data.IntValue or  arg2.Data.IntValue);
      ntOpAnd : Result := TExpTreeNode.Create(arg1.Data.IntValue and arg2.Data.IntValue);
      ntOpXor : Result := TExpTreeNode.Create(arg1.Data.IntValue xor arg2.Data.IntValue);
      ntOpEql : Result := TExpTreeNode.Create(arg1.Data.IntValue =   arg2.Data.IntValue);
      ntOpNeq : Result := TExpTreeNode.Create(arg1.Data.IntValue <>  arg2.Data.IntValue);
      ntOpLss : Result := TExpTreeNode.Create(arg1.Data.IntValue <   arg2.Data.IntValue);
      ntOpLeq : Result := TExpTreeNode.Create(arg1.Data.IntValue <=  arg2.Data.IntValue);
      ntOpGtr : Result := TExpTreeNode.Create(arg1.Data.IntValue >   arg2.Data.IntValue);
      ntOpGeq : Result := TExpTreeNode.Create(arg1.Data.IntValue >=  arg2.Data.IntValue);
    else
      Result := TExpTreeNode.Create(op,arg1,arg2);
      Exit;
    end;
    Exit;
  end;

  if (op = ntOpDiv) and IsPositiveNumber(arg2.Data) then begin
    if (IsPowerOf2(arg2.Data.IntValue)) then begin
      Result := DivToShift(arg1,arg2.Data.IntValue);
      Exit;
    end;
  end;

  if (op = ntOpMul) and IsPositiveNumber(arg2.Data) then begin
      Result := ExpandMultiply(arg1,arg2.Data.IntValue);
      Exit;
  end;

  if (op = ntOpMul) and IsPositiveNumber(arg1.Data) then begin
      Result := ExpandMultiply(arg2,arg1.Data.IntValue);
      Exit;
  end;

  // can't simplify any further
  Result := TExpTreeNode.Create(op,arg1,arg2);
end;

function  SimplifyTree(const ANode : TExpTreeNode) : TExpTreeNode;
begin
  Result := ANode;

  if Result = Nil then Exit;

  if Result.IsLeaf then begin
    Result := TExpTreeNode.Create(Result.Data);
    Result.SymIndex := ANode.SymIndex;
    Result.Deref      := ANode.Deref;
  end
  else
  if IsUnaryOp(Result.Data.NodeType) then
    Result := SimplifyUnaryOp(Result)
  else
  if IsBinaryOp(Result.Data.NodeType) then
    Result := SimplifyBinaryOp(Result);
end;

procedure VisitTreeNode(ANode : TExpTreeNode; AVisitor : TNodeVisitor);
begin
  if (ANode = Nil) then Exit;

  VisitTreeNode(ANode.LeftNode ,AVisitor);
  VisitTreeNode(ANode.RightNode,AVisitor);
  AVisitor(ANode);
end;

procedure VisitTreeNodes(ANode : TExpTreeNode; AVisitor : TNodeVisitor);
begin
  VisitTreeNode(ANode,AVisitor);
end;


function  TExpTreeNode.cloneNode(node : TExpTreeNode) : TExpTreeNode;
begin
  Result := node;

  if node = Nil then Exit;

  if node.IsLeaf then
    Result :=  TExpTreeNode.Create(node.Data)
  else
    Result := TExpTreeNode.Create(node.Data.NodeType,cloneNode(node.LeftNode),cloneNode(node.RightNode));

  Result.SymIndex := node.SymIndex;
  Result.Deref    := node.Deref;
end;

function  TExpTreeNode.clone: TExpTreeNode;
begin
  Result := cloneNode(Self);
end;

procedure TExpTreeNode.DisplayTreeNodePostFix(ANode : TExpTreeNode);
begin
  if (ANode = Nil) then Exit;

  DisplayTreeNodePostFix(ANode.LeftNode);
  DisplayTreeNodePostFix(ANode.RightNode);
  Write(ANode.Data.ToString,' ');
end;

procedure TExpTreeNode.DisplayTree;
begin
  Write('Tree = ');
  DisplayTreeNodePostFix(Self);
  WriteLn;
end;

constructor TExpression.Create;
begin
  FNodes := TExpNodeStack.Create;
end;

destructor  TExpression.Destroy;
begin
  FNodes.Free;
end;

procedure TExpression.ExpandMultiply(aStack : TExpNodeStack; ident : String; n : Integer);
var
  p   : Integer;
  bit : Integer;
  c,i : Integer;
begin
  p   := 0;
  bit := (1 shl p);
  c   := 0;

  while (bit <= n) do begin
    if ((bit and n) <> 0) then begin
      aStack.PushNode(ident);
      if (p > 0) then begin
        aStack.PushNode(p);
        aStack.PushNode(ntOpShl);
      end;

      Inc(c);
    end;

    p   := p + 1;
    bit := (1 shl p);
  end;

  for i := 2 to c do
    aStack.PushNode(ntOpAdd);
end;

procedure TExpression.DivToShift(aStack : TExpNodeStack; arg1 : TExpNode; n : Integer);
var
  shift : Integer;
begin
  aStack.PushNode(arg1);

  shift := PowerOf2ToShift(n);

  if (shift > 0) then begin
    aStack.PushNode(shift);
    aStack.PushNode(ntOpShr);
  end;
end;

procedure TExpression.MulToShift(aStack : TExpNodeStack; arg1 : TExpNode; n : Integer);
var
  shift : Integer;
begin
  aStack.PushNode(arg1);

  shift := PowerOf2ToShift(n);

  if (shift > 0) then begin
    aStack.PushNode(shift);
    aStack.PushNode(ntOpShl);
  end;
end;

procedure TExpression.PushNode(Value : Integer);
begin
  FNodes.PushNode(NewExpNode(Value));
end;

procedure TExpression.PushNode(Value : Boolean);
begin
  FNodes.PushNode(NewExpNode(Value));
end;

procedure TExpression.PushNode(Value : String);
begin
  FNodes.PushNode(NewExpNode(Value));
end;

procedure TExpression.PushNode(Value : TExpNodeType);
begin
  FNodes.PushNode(NewExpNode(Value));
end;

function  TExpression.PopNode: TExpNode;
begin
  Result := FNodes.PopNode;
end;

procedure TExpression.SimplifyUnaryOp(aStack : TExpNodeStack; aOp : TExpNodeType);
var
  arg : TExpNode;
begin
  arg := aStack.PopNode;

  if (aOp = ntOpNot) and (IsBoolean(arg) or IsInteger(arg)) then begin
    // is a integer/bool so let's do this!
    aStack.PushNode((not arg.IntValue) <> 0);
    Exit;
  end;

  // can't simplify
  aStack.PushNode(arg);
  aStack.PushNode(aOp);
end;

procedure TExpression.SimplifyBinaryOp(aStack : TExpNodeStack; aOp : TExpNodeType);
var
  arg1,arg2 : TExpNode;
begin
  arg2 := aStack.PopNode;
  arg1 := aStack.PopNode;

  if IsInteger(arg1) and IsInteger(arg2) then begin
    // both are numbers so let's do the math!
    case aOp of
      ntOpAdd : aStack.PushNode(arg1.IntValue +   arg2.IntValue);
      ntOpSub : aStack.PushNode(arg1.IntValue -   arg2.IntValue);
      ntOpMul : aStack.PushNode(arg1.IntValue *   arg2.IntValue);
      ntOpDiv : aStack.PushNode(arg1.IntValue div arg2.IntValue);
      ntOpShl : aStack.PushNode(arg1.IntValue shl arg2.IntValue);
      ntOpShr : aStack.PushNode(arg1.IntValue shr arg2.IntValue);
      ntOpOr  : aStack.PushNode(arg1.IntValue or  arg2.IntValue);
      ntOpAnd : aStack.PushNode(arg1.IntValue and arg2.IntValue);
      ntOpXor : aStack.PushNode(arg1.IntValue xor arg2.IntValue);
      ntOpEql : aStack.PushNode(arg1.IntValue =   arg2.IntValue);
      ntOpNeq : aStack.PushNode(arg1.IntValue <>  arg2.IntValue);
      ntOpLss : aStack.PushNode(arg1.IntValue <   arg2.IntValue);
      ntOpLeq : aStack.PushNode(arg1.IntValue <=  arg2.IntValue);
      ntOpGtr : aStack.PushNode(arg1.IntValue >   arg2.IntValue);
      ntOpGeq : aStack.PushNode(arg1.IntValue >=  arg2.IntValue);
    end;
    Exit;
  end;

  if IsBoolean(arg1) and IsBoolean(arg2) then begin
    // both are boolean so let's do the logic!
    case aOp of
      ntOpOr  : aStack.PushNode(arg1.IntValue or  arg2.IntValue);
      ntOpAnd : aStack.PushNode(arg1.IntValue and arg2.IntValue);
      ntOpXor : aStack.PushNode(arg1.IntValue xor arg2.IntValue);
      ntOpEql : aStack.PushNode(arg1.IntValue =   arg2.IntValue);
      ntOpNeq : aStack.PushNode(arg1.IntValue <>  arg2.IntValue);
      ntOpLss : aStack.PushNode(arg1.IntValue <   arg2.IntValue);
      ntOpLeq : aStack.PushNode(arg1.IntValue <=  arg2.IntValue);
      ntOpGtr : aStack.PushNode(arg1.IntValue >   arg2.IntValue);
      ntOpGeq : aStack.PushNode(arg1.IntValue >=  arg2.IntValue);
    end;
    Exit;
  end;

  if (aOp = ntOpDiv) and IsPositiveNumber(arg2) then begin
    if (IsPowerOf2(arg2.IntValue)) then begin
      DivToShift(aStack,arg1,arg2.IntValue);
      Exit;
    end;
  end;

  if (aOp = ntOpMul) and IsPositiveNumber(arg2) then begin
    if IsIdent(arg1) then begin
      ExpandMultiply(aStack,arg1.IdentValue,arg2.IntValue);
      Exit;
    end;

    if IsPowerOf2(arg2.IntValue) then begin
      MulToShift(aStack,arg1,arg2.IntValue);
      Exit;
    end;
  end;

  if (aOp = ntOpMul) and IsPositiveNumber(arg1) then begin
    if IsIdent(arg2) then begin
      ExpandMultiply(aStack,arg2.IdentValue,arg1.IntValue);
      Exit;
    end;

    if IsPowerOf2(arg1.IntValue) then begin
      MulToShift(aStack,arg2,arg1.IntValue);
      Exit;
    end;
  end;

  // can't simplify
  aStack.PushNode(arg1);
  aStack.PushNode(arg2);
  aStack.PushNode(aOp);
end;

procedure TExpression.Simplify;
var
  Simplified   : TExpNodeStack;
  SrcNodeIndex : Integer;
  SrcNode      : TExpNode;
begin
  WriteLn('simplifying...');

  Simplified := TExpNodeStack.Create;

  for SrcNodeIndex := 0 to FNodes.Count - 1 do begin
    SrcNode := FNodes.Nodes[SrcNodeIndex];

    if IsUnaryOp(SrcNode.NodeType) then
      SimplifyUnaryOp(Simplified,SrcNode.NodeType)
    else
    if IsBinaryOp(SrcNode.NodeType) then
      SimplifyBinaryOp(Simplified,SrcNode.NodeType)
    else
    // is operand so push onto stack
      Simplified.PushNode(SrcNode);
  end;

  FreeAndNil(FNodes);
  FNodes := Simplified;
end;

procedure TExpression.DebugPrint;
begin
  FNodes.DebugPrint;
end;

end.
