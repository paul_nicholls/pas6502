unit uCodeInsight;

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  System.Classes,System.Types;

type
  TCodeInsightData = class
  private
  protected
    FName   : String;
    FData   : TStringList;
    FLevel  : Integer;
    FParent : TCodeInsightData;
  public
    constructor Create(AName : String; AParent : TCodeInsightData = Nil);

    function  AddDataByName(AName : String) : TCodeInsightData;
    function  GetDataByName(AName : String) : TCodeInsightData;
    function  GetDataAsArray: TStringDynArray;

    property  Name : String read FName;
  end;

implementation

constructor TCodeInsightData.Create(AName : String;AParent : TCodeInsightData = Nil);
begin
  FData        := TStringList.Create;
  FData.Sorted := False;
  FName        := AName;
  FParent      := AParent;
  FLevel       := 0;
  if AParent <> Nil then FLevel := AParent.FLevel + 1;
end;

function  TCodeInsightData.AddDataByName(AName : String) : TCodeInsightData;
var
  i : Integer;
begin
  i := FData.IndexOf(AName);

  if i <> -1 then begin
    Result := TCodeInsightData(FData.Objects[i]);
    Exit;
  end;

  Result := TCodeInsightData.Create(AName,Self);
  FData.AddObject(AName,Result);
end;

function  TCodeInsightData.GetDataByName(AName : String) : TCodeInsightData;
var
  i : Integer;
begin
  Result := Nil;

  if (FName = AName) then
    Result := Self
  else begin
    i := FData.IndexOf(AName);

    if i = -1 then begin
      for i := 0 to FData.Count - 1 do begin
        Result := TCodeInsightData(FData.Objects[i]).GetDataByName(AName);

        if (Result <> Nil) then Exit;
      end;
    end
    else
      Result := TCodeInsightData(FData.Objects[i]);
  end;
end;

function  TCodeInsightData.GetDataAsArray: TStringDynArray;
var
  i : Integer;
begin
  SetLength(Result,0);
  for i := 0 to FData.Count - 1 do begin
    SetLength(Result,Length(Result) + 1);
    Result[High(Result)] := FData.Strings[i];
  end;
end;

end.
