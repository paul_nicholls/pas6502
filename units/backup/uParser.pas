unit uParser;

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  SysUtils,
  Classes,
  System.Contnrs;

const
  cCRLF  = #13#10;
  cCR    = #13;
  cLF    = #10;
  cFALSE = 0;
  cTRUE  = -1;

// tokens
var
  // Generic tokens
  Token_unknown      : Integer;
  Token_eof          : Integer;
  Token_ident        : Integer;
  Token_intnumber    : Integer;
  Token_fracnumber   : Integer;
  Token_string       : Integer;
  Token_boolean      : Integer;
  Token_colon        : Integer;
  Token_caret        : Integer;
  Token_lparen       : Integer;
  Token_rparen       : Integer;
  Token_lbracket     : Integer;
  Token_rbracket     : Integer;
  Token_times        : Integer;
  Token_slash        : Integer;
  Token_plus         : Integer;
  Token_minus        : Integer;
  Token_eql          : Integer;
  Token_neq          : Integer;
  Token_lss          : Integer;
  Token_leq          : Integer;
  Token_gtr          : Integer;
  Token_geq          : Integer;
  Token_semicolon    : Integer;
  Token_becomes      : Integer;
  Token_comma        : Integer;
  Token_period       : Integer;
  Token_dollar       : Integer;
  Token_at           : Integer;
  Token_hash         : Integer;
  Token_comment1     : Integer;
  Token_comment2s    : Integer;
  Token_comment2e    : Integer;
  Token_comment3s    : Integer;
  Token_comment3e    : Integer;
  Token_range        : Integer;

  // Keyword tokens
  Token_type         : Integer;
  Token_record       : Integer;
  Token_var          : Integer;
  Token_proc         : Integer;
  Token_func         : Integer;
  Token_begin        : Integer;
  Token_end          : Integer;
  Token_asmbegin     : Integer;
  Token_asmend       : Integer;
  Token_if           : Integer;
  Token_then         : Integer;
  Token_else         : Integer;
  Token_while        : Integer;
  Token_do           : Integer;
  Token_const        : Integer;
  Token_program      : Integer;
  Token_and          : Integer;
  Token_or           : Integer;
  Token_xor          : Integer;
  Token_div          : Integer;
  Token_mod          : Integer;
  Token_shl          : Integer;
  Token_shr          : Integer;
  Token_not          : Integer;
  Token_false        : Integer;
  Token_true         : Integer;
  Token_absolute     : Integer;
  Token_array        : Integer;
  Token_of           : Integer;
  Token_inc          : Integer;
  Token_dec          : Integer;

type
  TSymType = (
    eSymType_Nil,
    eSymType_Const,
    eSymType_Var,
    eSymType_Addr,
    eSymType_Proc,
    eSymType_RasterProc,
    eSymType_IRQProc,
    eSymType_Func
  );

  TSymSubType = (
    eSymSubType_typeRecord,
    eSymSubType_byte,
    eSymSubType_pbyte,
    eSymSubType_pinteger,
    eSymSubType_integer,
    eSymSubType_boolean
  );

{..............................................................................}

{..............................................................................}
  TSymInfo = class;

  TSymInfoList = class
  private
    FSymbols : TStringList;
  public
    constructor create;
    destructor  Destroy;

    function AddSymbol(aSymName : AnsiString) : TSymInfo;
    function GetCount : Integer;
    function GetSymbol(aIndex : Integer) : TSymInfo;

    property count : Integer read GetCount;
    property Symbols[aIndex : Integer] : TSymInfo read GetSymbol;
  end;

  TSymInfo = class
    SymName       : AnsiString;
    SymValue      : AnsiString;
    SymTypeName   : AnsiString;
    SymAddr       : AnsiString;
    SymScope      : AnsiString;
    SymType       : TSymType;
    SymSubType    : TSymSubType;
    SymLength     : Integer; // # of SubType units; usually 1, but larger for arrays
    SymSize       : Integer; // size of primitive; 1 or 2 usually
    SymIsArray    : Boolean;
    SymIsReadOnly : Boolean;
    SymRecordVars : TSymInfoList;

    constructor Create;
    destructor  Destroy;
  end;

  { TIntNumType }
  TIntNumType = (
    ntSInt8,
    ntUInt8,
    ntSInt16,
    ntUInt16,
    ntSInt32,
    ntUInt32
  );

  { TIntNumTypeRange }
  TIntNumTypeRange = record
    MinVal,MaxVal: Int64;
  end;

const
  { cNumTypeRange }
  cNumTypeRange: array [TIntNumType] of TIntNumTypeRange = (
    (MinVal: - 128;        MaxVal: 127),
    (MinVal: 0;            MaxVal: 255),
    (MinVal: - 32768;      MaxVal: 32767),
    (MinVal: 0;            MaxVal: 65535),
    (MinVal: - 2147483648; MaxVal: 2147483647),
    (MinVal: 0;            MaxVal: 4294967295)
  );

type
  { TToken }
  TToken = record
    TokenValue  : String;
    TokenType   : Integer;
    TokenRow    : Integer;
  end;

  { TCharSet }
  TCharSet = set of AnsiChar;

  { TParseException }
  TParseException = class(Exception);

  TStreamInfo = class
    stream    : TStream;
    character : AnsiChar;
    LineCount : Integer;
    CharPos   : Integer;
    Token     : TToken;
    EOF       : Boolean;
  end;

{ --- TBaseParser ----------------------------------------------------------- }
  TBaseParser = class(TInterfacedObject)
  private
  protected
    FStringChar      : AnsiChar;
    FCharacter       : AnsiChar;
    FEOF             : Boolean;
    FToken           : TToken;
    FAllowBinNumbers : Boolean;
    FLineCount       : Integer;
    FCharPos         : Integer;
    FIntNumbersOnly  : Boolean;
    FClearSymbolTable: Boolean;

    FSavedStreamInfo  : TStack;
    FLabelIndex       : Integer;
    FCurrentTokenType : Integer;
    FTokenList        : TStringList;
    FKeywordList      : TStringList;

    FSrcStream        : TStream;
    FDstStream        : TStream;
    FErrorMsg         : AnsiString;
    FSymbolTable      : TStringList;
    FParserPath       : AnsiString;
    FProjectPath      : AnsiString;

    function  NewTokenType : Integer;
    function  NewLabel     : AnsiString;
    function  RegisterKeywordToken(const aTokenName: AnsiString): Integer;
    function  RegisterGenericToken(const aTokenName: AnsiString): Integer;
    procedure CheckTokenForKeyword(var aToken: TToken);
    procedure ClearAllTokens;

    Function  AddSymbol(n : AnsiString) : TSymInfo;
    Function  GetSymbol(n : AnsiString) : TSymInfo;
    Function  InTable(n : AnsiString) : Boolean;

    procedure ClearSymTable;
    procedure InitTokens;
    // override these if you want to alter the tokens being registered
    procedure RegisterGenericTokens; virtual;
    procedure RegisterKeywordTokens; virtual;
    //

    function  TokenToStr(const aTokenType: Integer): AnsiString;
    procedure Error(const aErrorMsg: String; AddLocationInformation : Boolean = True);
    procedure Emit(const aMsg: AnsiString);
    procedure EmitLn(aMsg: AnsiString);
    procedure EmitLabel(aLabel : AnsiString);
    procedure EmitCode(aCode : AnsiString);

    function  IsDecDigit  (const c: AnsiChar): Boolean;
    function  IsHexDigit  (const c: AnsiChar): Boolean;
    function  IsBinDigit  (const c: AnsiChar): Boolean;
    function  IsAlpha     (const c: AnsiChar): Boolean;
    function  IsIdentChar (const c: AnsiChar): Boolean;
    function  IsIdentStart(const c: AnsiChar): Boolean;
    function  IsWhiteSpace(const c: AnsiChar): Boolean;
    function  IsOperator  (const c: AnsiChar): Boolean;  virtual;
    function  IsRelop     (const ATokenType : Integer): Boolean;

    function  Accept(const aTokenType: Integer): Boolean;
    procedure Expect(const aTokenType: Integer);

    function  ReadUntil(const aCharSet: TCharSet): String;
    function  GetLine: String;
    function  ReadLn: String;

    procedure GetCharacter;
    procedure GetDecNumber;
    procedure GetHexNumber;
    procedure GetBinNumber;
    procedure GetIdent;
    procedure GetString;
    procedure GetOperator; virtual;

    // returns the current token and then retrieves the next one
    function  GetToken: TToken;
    procedure SkipToEndOfLine;
    procedure SkipWhiteSpaces;

    procedure ResetSrcStreamInfo;
    procedure SaveSrcStreamInfo(astream    : TStream;
                                aCharacter : AnsiChar;
                                aLineCount : Integer;
                                aCharPos   : Integer;
                                aToken     : TToken;
                                aEOF       : Boolean);

    procedure RestoreSrcStreamInfo(var astream    : TStream;
                                   var aCharacter : AnsiChar;
                                   var aLineCount : Integer;
                                   var aCharPos   : Integer;
                                   var aToken     : TToken;
                                   var aEOF       : Boolean);

    procedure ParseInput; virtual; abstract;
  public
    constructor Create;
    destructor  Destroy; override;

    function  ParseProgram(const aSrcStream,aDstStream: TStream): Boolean;

    property Character       : AnsiChar    read FCharacter;
    property SrcStream       : TStream read FSrcStream;
    property DstStream       : TStream read FDstStream;
    property StringChar      : AnsiChar    read FStringChar      write FStringChar;
    property Token           : TToken  read FToken;
    property ErrorMsg        : AnsiString  read FErrorMsg;
    property AllowBinNumbers : Boolean read FAllowBinNumbers write FAllowBinNumbers;
    property LineCount       : Integer read FLineCount;
    property CharPos         : Integer read FCharPos;
    property IntNumbersOnly  : Boolean read FIntNumbersOnly  write FIntNumbersOnly;

    property ParserPath      : AnsiString  read FParserPath      write FParserPath;
    property ProjectPath     : AnsiString  read FProjectPath     write FProjectPath;

    property EOF             : Boolean read FEOF;
  end;

{ --- Routines -------------------------------------------------------------- }

function  BinToInt64(aBin: String): Int64;
function  IntegerInRange(const aNumber,aMin,aMax: Int64): Boolean;
procedure GetIntegerTypeAndValue(const aNumber: String; var aIntNumType: TIntNumType; var aValue: Int64);

implementation

{ --- Routines -------------------------------------------------------------- }
function  NewToken(const aTokenValue: String; const aTokenType,aTokenRow: Integer): TToken;
begin
  Result.TokenValue  := aTokenValue;
  Result.TokenType   := aTokenType;
  Result.TokenRow    := aTokenRow;
end;

{$ifdef fpc}
function  CharInSet(const c: AnsiChar; const aSet: TCharSet): Boolean;
begin
  Result := c in aSet;
end;

{$endif}

constructor TSymInfo.Create;
begin
  SymRecordVars := TSymInfoList.create;
end;

destructor  TSymInfo.Destroy;
begin
  SymRecordVars.Free;
end;

constructor TSymInfoList.create;
begin
  FSymbols := TStringList.Create;
  FSymbols.Sorted := False;
end;

destructor  TSymInfoList.Destroy;
var
  i : Integer;
begin
  for i := 0 to FSymbols.Count - 1 do begin
    TSymInfo(FSymbols.Objects[i]).Free;
  end;
  FreeAndNil(FSymbols);
end;

function TSymInfoList.AddSymbol(aSymName : AnsiString) : TSymInfo;
begin
  Result := TSymInfo.Create;
  Result.SymName       := LowerCase(aSymName);
  Result.SymAddr       := '';
  Result.SymType       := eSymType_Var;
  Result.SymIsArray    := False;
  Result.SymIsReadOnly := False;

  FSymbols.AddObject(LowerCase(aSymName),Result);
end;

function TSymInfoList.GetCount : Integer;
begin
  Result := FSymbols.Count;
end;

function TSymInfoList.GetSymbol(aIndex : Integer) : TSymInfo;
begin
  Result := TSymInfo(FSymbols.Objects[aIndex]);
end;

function  TBaseParser.TokenToStr(const aTokenType: Integer): AnsiString;
var
  Index: Integer;
begin
  Result := 'Unknown';

  Index := FTokenList.IndexOfObject(Pointer(aTokenType));

  if Index <> -1 then
    Result := FTokenList.Strings[Index]
  else
  begin
    Index := FKeywordList.IndexOfObject(Pointer(aTokenType));

    if Index <> -1 then
      Result := FKeywordList.Strings[Index]
  end;
end;

function BinToInt64(aBin: String): Int64;
var
  i, iValueSize : Integer;
begin
  Result     := 0;
  iValueSize := Length(aBin);

  for i := iValueSize downto 1 do
    if aBin[i] = '1' then Result := Result + (1 shl (iValueSize - i));
end;

function  IntegerInRange(const aNumber,aMin,aMax: Int64): Boolean;
begin
  Result := (aNumber >= aMin) and
            (aNumber <= aMax);
end;

procedure GetIntegerTypeAndValue(const aNumber: String; var aIntNumType: TIntNumType; var aValue: Int64);
var
  i: TIntNumType;
  v, c: Integer;
begin
  if aNumber = '' then
  begin
    aIntNumType := ntUInt8;
    aValue   := 0;
    Exit;
  end;

  c := 0;

  if aNumber[1] = '%' then
  begin
    v := BinToInt64(aNumber);
    c := 0;
  end
  else
  if aNumber[1] = '$' then
  begin
    v := StrToInt(aNumber);
    c := 0;
  end
  else
    Val(aNumber, v, c);

  if c <> 0 then
    raise TParseException.Create('Invalid number "'+aNumber+'"');

  for i := Low(TIntNumType) to High(TIntNumType) do
    if IntegerInRange(v, cNumTypeRange[i].MinVal,cNumTypeRange[i].MaxVal) then
    begin
      aIntNumType := i;
      AValue      := v;
      Exit;
    end;

  raise TParseException.Create('Invalid number "'+aNumber+'"');
end;

{ --- TBaseParser ----------------------------------------------------------- }
Function  TBaseParser.AddSymbol(n : AnsiString) : TSymInfo;
var
  Index : Integer;
Begin
  for Index := 0 to FSymbolTable.Count - 1 do begin
    if FSymbolTable.Strings[Index] = LowerCase(n) then Error('Identifier redeclared: "'+n+'"');
  end;

  n := LowerCase(n);

  Result := TSymInfo.Create;
  Result.SymName       := n;
  Result.SymValue      := '';
  Result.SymType       := eSymType_Nil;
  Result.SymScope      := '';
  Result.SymIsReadOnly := False;

  FSymbolTable.AddObject(Result.SymName,Result);
End;
{..............................................................................}

{..............................................................................}
Function  TBaseParser.GetSymbol(n : AnsiString) : TSymInfo;
Var
  Index : Integer;
Begin
  Result := Nil;

  n := LowerCase(n);

  for Index := 0 to FSymbolTable.Count - 1 do begin
    if FSymbolTable.Strings[Index] = n then begin
      Result := TSymInfo(FSymbolTable.Objects[Index]);
      Exit;
    end;
  end;
End;
{..............................................................................}

{..............................................................................}
Function  TBaseParser.InTable(n : AnsiString) : Boolean;
Var
  Index : Integer;
Begin
  Result := False;

  n := LowerCase(n);

  for Index := 0 to FSymbolTable.Count - 1 do begin
    if FSymbolTable.Strings[Index] = n then begin
      Result := True;
      Exit;
    end;
  end;
End;
{..............................................................................}

{..............................................................................}
procedure  TBaseParser.ClearSymTable;
var
  i : Integer;
begin
  for i := 0 to FSymbolTable.Count - 1 do
    TSymInfo(FSymbolTable.Objects[i]).Free;

  FSymbolTable.Clear;
end;

constructor TBaseParser.Create;
begin
  inherited Create;

  FAllowBinNumbers   := True;
  FIntNumbersOnly    := True;

  FSavedStreamInfo := TStack.Create;

  FStringChar      := '''';
  FLineCount       := 1;
  FCharPos         := 1;

  FLabelIndex      := 0;

  FTokenList       := TStringList.Create;
  FTokenList.Duplicates := dupIgnore;

  FKeywordList       := TStringList.Create;
  FKeywordList.Duplicates := dupIgnore;

  FSymbolTable := TStringList.Create;
  FSymbolTable.Sorted := False;
  FClearSymbolTable   := True;

  InitTokens;

  Token_unknown      := RegisterGenericToken('Unknown');
  Token_eof          := RegisterGenericToken('EOF');
  Token_ident        := RegisterGenericToken('Identifier');
  Token_intnumber    := RegisterGenericToken('Integer');
  Token_fracnumber   := RegisterGenericToken('Single');
  Token_string       := RegisterGenericToken('String');
  Token_boolean      := RegisterGenericToken('Boolean');

  FToken.TokenValue  := 'Unknown';
  FToken.TokenType   := Token_unknown;
  FToken.TokenRow    := FLineCount;
end;

destructor  TBaseParser.Destroy;
begin
  if FClearSymbolTable then ClearSymTable;

  FTokenList.Free;
  FKeywordList.Free;

  inherited Destroy;
end;

function  TBaseParser.NewLabel : AnsiString;
begin
  Result := 'L' + IntToStr(FLabelIndex);
  Inc(FLabelIndex);
end;

function  TBaseParser.NewTokenType: Integer;
begin
  Result := FCurrentTokenType;
  Inc(FCurrentTokenType);
end;

function  TBaseParser.RegisterKeywordToken(const aTokenName: AnsiString): Integer;
var
  TokenType: Integer;
begin
  TokenType := NewTokenType;
  FKeywordList.AddObject(LowerCase(aTokenName),Pointer(TokenType));

  Result := TokenType;
end;

function  TBaseParser.RegisterGenericToken(const aTokenName: AnsiString): Integer;
var
  TokenType: Integer;
begin
  TokenType := NewTokenType;
  FTokenList.AddObject(LowerCase(aTokenName),Pointer(TokenType));

  Result := TokenType;
end;

procedure TBaseParser.CheckTokenForKeyword(var aToken: TToken);
var
  Index: Integer;
begin
  for Index := 0 to FKeywordList.Count - 1 do begin
    if (LowerCase(aToken.TokenValue) = FKeywordList.Strings[Index]) then begin
      aToken.TokenType := Integer(FKeywordList.Objects[Index]);
      Exit;
    end;
  end;
end;

procedure TBaseParser.InitTokens;
begin
  FCurrentTokenType := 0;
  ClearAllTokens;

  RegisterGenericTokens;
  RegisterKeywordTokens;
end;

procedure TBaseParser.Error(const aErrorMsg: String; AddLocationInformation : Boolean = True);
begin
  if AddLocationInformation then
    raise TParseException.Create(Format('Error at Line %d: %s',[FLineCount,aErrorMsg]))
  else
    raise TParseException.Create(aErrorMsg);
end;

procedure TBaseParser.Emit(const aMsg: AnsiString);
begin
  FDstStream.Write(PAnsiChar(aMsg)^,Length(aMsg));
end;

procedure TBaseParser.EmitLn(aMsg: AnsiString);
begin
  aMsg := aMsg + cCRLF;
  FDstStream.Write(PAnsiChar(aMsg)^,Length(aMsg));
end;

procedure TBaseParser.EmitLabel(aLabel : AnsiString);
begin
  EmitLn(aLabel + ':');
end;

procedure TBaseParser.EmitCode(aCode : AnsiString);
begin
  EmitLn('  ' + aCode);
end;

function  TBaseParser.IsDecDigit(const c: AnsiChar): Boolean;
begin
  Result := CharInSet(c,['0'..'9']);
end;

function  TBaseParser.IsHexDigit(const c: AnsiChar): Boolean;
begin
  Result := CharInSet(c,['0'..'9','a'..'z','A'..'Z']);
end;

function  TBaseParser.IsBinDigit(const c: AnsiChar): Boolean;
begin
  Result := CharInSet(c,['0'..'1']);
end;

function  TBaseParser.IsAlpha(const c: AnsiChar): Boolean;
begin
  Result := CharInSet(c,['a'..'z','A'..'Z']);
end;

function  TBaseParser.IsIdentChar(const c: AnsiChar): Boolean;
begin
  Result := CharInSet(c,['0'..'9','_','a'..'z','A'..'Z']);
end;

function  TBaseParser.IsIdentStart(const c: AnsiChar): Boolean;
begin
  Result := CharInSet(c,['_','a'..'z','A'..'Z']);
end;

function  TBaseParser.IsWhiteSpace(const c: AnsiChar): Boolean;
begin
  Result := CharInSet(c,[' ',#9,#13,#10]);
end;

function  TBaseParser.IsOperator(const c: AnsiChar): Boolean;
begin
  Result := CharInSet(c,['=','<','>','-','+','*','/',':','^']);
end;

function  TBaseParser.IsRelop(const ATokenType: Integer): Boolean;
begin
  Result := ATokenType in[
  Token_eql,
  Token_neq,
  Token_lss,
  Token_leq,
  Token_gtr,
  Token_geq
  ];
end;

procedure TBaseParser.GetCharacter;
begin
  FCharacter := #0;
  FEOF       := true;
  if FSrcStream.Position < FSrcStream.Size then
  begin
    FSrcStream.Read(FCharacter,1);//SizeOf(FCharacter));
    if (FCharacter in [cLF]) then begin
      FCharPos := 1;
      Inc(FLineCount);
    end
    else
      Inc(FCharPos);
    FEOF := false;
  end;
end;

procedure TBaseParser.GetDecNumber;
begin
  if not IsDecDigit(FCharacter) then Error('Number expected');

  FToken.TokenType  := Token_intnumber;
  FToken.TokenValue := '';

  while (not FEOF) and IsDecDigit(FCharacter) do
  begin
    FToken.TokenValue := FToken.TokenValue + FCharacter;
    GetCharacter;
  end;

  if not FIntNumbersOnly then begin
    if FCharacter = '.' then
    begin
      FToken.TokenType  := Token_fracnumber;
    // get fractional part of number
      FToken.TokenValue := FToken.TokenValue + FCharacter;
      GetCharacter;

      while (not FEOF) and IsDecDigit(FCharacter) do
      begin
        FToken.TokenValue := FToken.TokenValue + FCharacter;
        GetCharacter;
      end;
    end;

    // get any scientific part of number
    if LowerCase(FCharacter) = 'e' then
    begin
      // parse 'e' part
      FToken.TokenValue := FToken.TokenValue + FCharacter;
      GetCharacter;

      // parse +/- number part
      FToken.TokenValue := FToken.TokenValue + FCharacter;
      GetCharacter;

      while (not FEOF) and IsDecDigit(FCharacter) do
      begin
        FToken.TokenValue := FToken.TokenValue + FCharacter;
        GetCharacter;
      end;
    end;
  end;

  SkipWhiteSpaces;
end;

procedure TBaseParser.GetHexNumber;
begin
  if FCharacter <> '$' then Error('Number expected');

  FToken.TokenType  := Token_intnumber;
  FToken.TokenValue := FCharacter;

  GetCharacter;

  while (not FEOF) and IsHexDigit(FCharacter) do
  begin
    FToken.TokenValue := FToken.TokenValue + FCharacter;
    GetCharacter;
  end;
  SkipWhiteSpaces;
end;

procedure TBaseParser.GetBinNumber;
begin
  if FCharacter <> '%' then Error('Number expected');

  FToken.TokenType  := Token_intnumber;
  FToken.TokenValue := FCharacter;

  GetCharacter;

  while (not FEOF) and IsBinDigit(FCharacter) do
  begin
    FToken.TokenValue := FToken.TokenValue + FCharacter;
    GetCharacter;
  end;
  SkipWhiteSpaces;

  FToken.TokenValue := IntToStr(BinToInt64(FToken.TokenValue));
end;

procedure TBaseParser.GetIdent;
begin
  if not IsIdentStart(FCharacter) then Error('Identifier expected');

  FToken.TokenType  := Token_ident;
  FToken.TokenValue := '';

  while (not FEOF) and IsIdentChar(FCharacter) do
  begin
    FToken.TokenValue := FToken.TokenValue + FCharacter;
    GetCharacter;
  end;
  SkipWhiteSpaces;
end;

procedure TBaseParser.GetString;
var
  EOFString: Boolean;
begin
  if FCharacter <> FStringChar then Error('String expected');

  FToken.TokenType  := Token_string;
  FToken.TokenValue := '';

  GetCharacter;

  EOFString := False;

  while not EOFString do
  begin
    while (not FEOF) and (FCharacter <> FStringChar) do
    begin
      FToken.TokenValue := FToken.TokenValue + FCharacter;
      GetCharacter;
    end;
    GetCharacter;

    if FCharacter = FStringChar then
    begin
      FToken.TokenValue := FToken.TokenValue + FCharacter;
      GetCharacter;
    end
    else
      EOFString := True;
  end;
  SkipWhiteSpaces;
end;

procedure TBaseParser.GetOperator;
begin
  FToken.TokenValue := FCharacter;
  if CharInSet(FCharacter,  ['=','-','+','*','/','^']) then
  begin
    Case FCharacter Of
      '=' : FToken.TokenType := Token_eql;
      '-' : FToken.TokenType := Token_minus;
      '+' : FToken.TokenType := Token_plus;
      '*' : FToken.TokenType := Token_times;
      '/' : FToken.TokenType := Token_slash;
      '^' : FToken.TokenType := Token_caret;
    end;
    GetCharacter;
    if (FToken.TokenType = Token_slash) and (FCharacter = '/') then
    begin
      FToken.TokenValue := '//';
      FToken.TokenType  := Token_comment1;
      SkipToEndOfLine;
      GetToken;
    end;
  end
  else
  if FCharacter = ':' then
  // could be ':', or ':='
  begin
    FToken.TokenType := Token_colon;
    GetCharacter;
    if FCharacter = '=' then
    begin
      FToken.TokenValue := ':=';
      FToken.TokenType  := Token_becomes;
      GetCharacter;
    end;
  end
  else
  if FCharacter = '<' then
  // could be '<', or '<=', or '<>'
  begin
    FToken.TokenType := Token_lss;
    GetCharacter;
    if FCharacter = '=' then
    begin
      FToken.TokenValue := '<=';
      FToken.TokenType  := Token_leq;
      GetCharacter;
    end
    else
    if FCharacter = '>' then
    // is '<>'
    begin
      FToken.TokenValue := '<>';
      FToken.TokenType  := Token_neq;
      GetCharacter;
    end;;
  end
  else
  if FCharacter = '>' then
  // could be '>', or '>='
  begin
    FToken.TokenType := Token_gtr;
    GetCharacter;
    if FCharacter = '=' then
    begin
      FToken.TokenValue := '>=';
      FToken.TokenType  := Token_geq;
      GetCharacter;
    end;
  end;
  SkipWhiteSpaces;
end;

function  TBaseParser.GetToken: TToken;
begin
  Result := FToken;
  Result.TokenRow    := FLineCount;

  if FEOF then
  begin
    FToken.TokenValue := 'EOF';
    FToken.TokenType  := Token_eof;
  end
  else
  if IsIdentStart(FCharacter) then
  begin
    GetIdent;
    CheckTokenForKeyword(FToken);
  end
  else
  if FCharacter = '$' then
    GetHexNumber
  else
  If FAllowBinNumbers and (FCharacter = '%') then
    GetBinNumber
  else
  if IsDecDigit(FCharacter) then
    GetDecNumber
  else
  if FCharacter = FStringChar then
    GetString
  else
  If IsOperator(FCharacter) then
    GetOperator
  else
  begin
    FToken.TokenValue := FCharacter;
    case FCharacter of
      ',' : FToken.TokenType := Token_comma;
      ';' : FToken.TokenType := Token_semicolon;
      '(' : FToken.TokenType := Token_lparen;
      ')' : FToken.TokenType := Token_rparen;
      '.' : FToken.TokenType := Token_period;
      '@' : FToken.TokenType := Token_at;
      '#' : FToken.TokenType := Token_hash;
      '{' : FToken.TokenType := Token_comment2s;
      '}' : FToken.TokenType := Token_comment2e;
      '[' : FToken.TokenType := Token_lbracket;
      ']' : FToken.TokenType := Token_rbracket;
    end;
    GetCharacter;
    if FToken.TokenType = Token_comment2s then begin
      while (not EOF) and (FCharacter <> '}') do GetCharacter;
      if FCharacter = '}' then GetCharacter;
      SkipWhiteSpaces;
      GetToken;
    end;
    if (FToken.TokenType = Token_period) and (FCharacter = '.') then
    begin
      FToken.TokenValue := '..';
      FToken.TokenType  := Token_range;
      GetCharacter;
    end;
    SkipWhiteSpaces;
  end;
end;

procedure TBaseParser.SkipToEndOfLine;
begin
  while (not FEOF) and not(FCharacter in [cCR,cLF]) do
    GetCharacter;
  SkipWhiteSpaces;
end;

procedure TBaseParser.SkipWhiteSpaces;
begin
  while not FEOF and IsWhiteSpace(FCharacter) do begin
    GetCharacter;
  end;

  Exit;
(*  while not FEOF and (FCharacter = cCR)do begin
  // new line
    Inc(FLineCount);
    GetCharacter;
    SkipWhiteSpaces;
  end
  else
  if FCharacter = '{' then
  // multi-line comment
  begin
    while FCharacter <> '}'do
    begin
      GetCharacter;
      if FEOF then Break;
    end;
    if FCharacter = '{' then
      GetCharacter;
    SkipWhiteSpaces;
  end;*)
end;

function  TBaseParser.Accept(const aTokenType: Integer): Boolean;
begin
  if FToken.TokenType =  aTokenType then
  begin
    GetToken;
    Result :=  True;
  end
  else
    Result :=  False;
end;

procedure TBaseParser.Expect(const aTokenType: Integer);
begin
  if FToken.TokenType =  aTokenType then
    GetToken
  else
  begin
    if FToken.TokenType <> aTokenType then begin
      Error('Expected token "'+TokenToStr(aTokenType)+'", found token "'+TokenToStr(FToken.TokenType)+'" instead');
    end;
  end;
end;

function  TBaseParser.ReadUntil(const aCharSet: TCharSet): String;
begin
  Result := '';

  while not CharInSet(FCharacter,aCharSet) and (not FEOF) do
  begin
    Result := Result + FCharacter;
    GetCharacter;
  end;
  SkipWhiteSpaces;
  GetToken;
end;

function  TBaseParser.GetLine: String;
begin
  Result := '';

  while (FCharacter <> cCR) and (not FEOF) do
  begin
    Result := Result + FCharacter;
    GetCharacter;
  end;

  while (FCharacter = cCR) and (not FEOF) do
    GetCharacter;
end;

function  TBaseParser.ReadLn: String;
begin
  Result := '';

  while (FCharacter <> cCR) and (not FEOF) do
  begin
    Result := Result + FCharacter;
    GetCharacter;
  end;
  SkipWhiteSpaces;
  GetToken;
end;

procedure TBaseParser.ResetSrcStreamInfo;
begin
  FSrcStream.Seek(0,soFromBeginning);

  FLineCount := 1;
  FCharPos   := 1;

  // initialize parser
  GetCharacter;

  SkipWhiteSpaces;
  GetToken;
end;

procedure TBaseParser.SaveSrcStreamInfo(astream    : TStream;
                                        aCharacter : AnsiChar;
                                        aLineCount : Integer;
                                        aCharPos   : Integer;
                                        aToken     : TToken;
                                        aEOF       : Boolean);
var
  info : TStreamInfo;
begin
  info := TStreamInfo.Create;

  info.stream    := astream;
  info.character := aCharacter;
  info.LineCount := alineCount;
  info.CharPos   := aCharPos;
  info.Token     := aToken;
  info.EOF       := aEOF;

  FSavedStreamInfo.Push(info);
end;

procedure TBaseParser.RestoreSrcStreamInfo(var astream    : TStream;
                                           var aCharacter : AnsiChar;
                                           var aLineCount : Integer;
                                           var aCharPos   : Integer;
                                           var aToken     : TToken;
                                           var aEOF       : Boolean);
var
  info : TStreamInfo;
begin
  info := FSavedStreamInfo.Pop;

  astream    := info.stream;
  aCharacter := info.character;
  aLineCount := info.LineCount;
  aCharPos   := info.CharPos;
  aToken     := info.Token;
  aEOF       := info.EOF;
end;

function  TBaseParser.ParseProgram(const aSrcStream,aDstStream: TStream): Boolean;
begin
  FSrcStream := aSrcStream;
  FDstStream := aDstStream;

  FSrcStream.Seek(0,soFromBeginning);

  FErrorMsg := '';
  Result    := True;

  FLineCount := 1;
  FCharPos   := 1;

  // initialize parser
  GetCharacter;

  SkipWhiteSpaces;
  GetToken;

  try
    ParseInput; // override this to make a proper parser

    FDstStream.Seek(0,soFromBeginning);

  except
    on E:Exception do
    begin
      FErrorMsg := E.Message;
      Result    := False;
    end;
  end;
end;

procedure TBaseParser.RegisterGenericTokens;
begin
  Token_colon        := RegisterGenericToken(':');
  Token_caret        := RegisterGenericToken('^');
  Token_lparen       := RegisterGenericToken('(');
  Token_rparen       := RegisterGenericToken(')');
  Token_lbracket     := RegisterGenericToken('[');
  Token_rbracket     := RegisterGenericToken(']');
  Token_times        := RegisterGenericToken('*');
  Token_slash        := RegisterGenericToken('/');
  Token_plus         := RegisterGenericToken('+');
  Token_minus        := RegisterGenericToken('-');
  Token_eql          := RegisterGenericToken('=');
  Token_neq          := RegisterGenericToken('<>');
  Token_lss          := RegisterGenericToken('<');
  Token_leq          := RegisterGenericToken('<=');
  Token_gtr          := RegisterGenericToken('>');
  Token_geq          := RegisterGenericToken('>=');
  Token_semicolon    := RegisterGenericToken(';');
  Token_becomes      := RegisterGenericToken(':=');
  Token_comma        := RegisterGenericToken(',');
  Token_period       := RegisterGenericToken('.');
  Token_dollar       := RegisterGenericToken('$');
  Token_at           := RegisterGenericToken('@');
  Token_hash         := RegisterGenericToken('#');
  Token_comment1     := RegisterGenericToken('//');
  Token_comment2s    := RegisterGenericToken('{');
  Token_comment2e    := RegisterGenericToken('}');
  Token_comment3s    := RegisterGenericToken('(*');
  Token_comment3e    := RegisterGenericToken('*)');
  Token_range        := RegisterGenericToken('..');
end;

procedure TBaseParser.RegisterKeywordTokens;
begin
  Token_var          := RegisterKeywordToken('var');
  Token_type         := RegisterKeywordToken('type');
  Token_record       := RegisterKeywordToken('record');
  Token_proc         := RegisterKeywordToken('procedure');
  Token_func         := RegisterKeywordToken('function');
  Token_begin        := RegisterKeywordToken('begin');
  Token_end          := RegisterKeywordToken('end');
  Token_asmbegin     := RegisterKeywordToken('asmbegin');
  Token_asmend       := RegisterKeywordToken('asmend');
  Token_if           := RegisterKeywordToken('if');
  Token_then         := RegisterKeywordToken('then');
  Token_else         := RegisterKeywordToken('else');
  Token_while        := RegisterKeywordToken('while');
  Token_do           := RegisterKeywordToken('do');
  Token_const        := RegisterKeywordToken('const');
  Token_program      := RegisterKeywordToken('program');
  Token_and          := RegisterKeywordToken('and');
  Token_or           := RegisterKeywordToken('or');
  Token_xor          := RegisterKeywordToken('xor');
  Token_div          := RegisterKeywordToken('div');
  Token_mod          := RegisterKeywordToken('mod');
  Token_shl          := RegisterKeywordToken('shl');
  Token_shr          := RegisterKeywordToken('shr');
  Token_not          := RegisterKeywordToken('not');
  Token_false        := RegisterKeywordToken('false');
  Token_true         := RegisterKeywordToken('true');
  Token_absolute     := RegisterKeywordToken('absolute');
  Token_array        := RegisterKeywordToken('array');
  Token_of           := RegisterKeywordToken('of');
  Token_inc          := RegisterKeywordToken('inc');
  Token_dec          := RegisterKeywordToken('dec');
end;

procedure TBaseParser.ClearAllTokens;
begin
  FTokenList.Clear;
  FKeywordList.Clear;
end;

begin
end.
