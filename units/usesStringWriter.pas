unit usesStringWriter;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  System.Classes;

type
  TOnWriteString = procedure(aMsg : AnsiString) of Object;

  TsesStringWriter = class
  private
    FdstStream     : TStream;
    FcurrentLine   : Integer;
    FonWriteString : TOnWriteString;
  public
    constructor create(aDstStream : TStream);

    procedure write(aMsg : AnsiString);                                overload;
    procedure write(aMsg : AnsiString; const aArgs: array of const);   overload;
    procedure writeLn(aMsg : AnsiString);                              overload;
    procedure writeLn(aMsg : AnsiString; const aArgs: array of const); overload;
    procedure writeLn();                                               overload;

    property currentLine   : Integer read FcurrentLine;
    property onWriteString : TOnWriteString write FonWriteString;
  end;

implementation

uses
  System.SysUtils;

constructor TsesStringWriter.create(aDstStream : TStream);
begin
  FdstStream := aDstStream;

  FcurrentLine := 0;
end;

procedure TsesStringWriter.write(aMsg : AnsiString);
var
  i : Integer;
begin
  if aMsg = '' then Exit;

  if Assigned(FonWriteString) then FonWriteString(aMsg);

  for i := 1 to Length(aMsg) do
    if aMsg[i] = #13 then
      Inc(FcurrentLine);

  FdstStream.WriteBuffer(PAnsiChar(aMsg)^,Length(aMsg));
end;

procedure TsesStringWriter.writeLn(aMsg : AnsiString);
begin
  write(aMsg + #13#10);
end;

procedure TsesStringWriter.writeLn;
begin
  write(#13#10);
end;

procedure TsesStringWriter.write(aMsg : AnsiString; const aArgs: array of const);
var
  i : Integer;
begin
  write(Format(aMsg,aArgs));
end;

procedure TsesStringWriter.writeLn(aMsg : AnsiString; const aArgs: array of const);
var
  i : Integer;
begin
  writeLn(Format(aMsg,aArgs));
end;

end.
