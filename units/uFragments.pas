unit uFragments;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

{$H+}

interface

uses
  System.Classes,SysUtils,uThreeAddressCode,uToken,uAST,uSymbolTable;

(*
Integer types are described by the following:
1. v value / p pointer / q pointer to pointer
2. b byte / w word / d dword
3. u unsigned / s signed


The storage of the value is described by 2 letters
- aa A-register
- xx X-register
- yy Y-register
- ax a,x-register (a = msb,x = lsb)
- xa x,a-register (x = msb,a = lsb)
- z? zeropage-memory {z1, etc}
- m? main-memory {m1, etc}
- c? constant {c1, etc}
*)

type
  TReadWriteType = (rwAddr,rwConst,rwMem,rwStack);

const
  PNTR   : Array[boolean] of Char              = ('v','p'); // value/pointer
  SIZE   : Array[0..1]    of Char              = ('b','w'); // byte/word
  SIGNED : Array[boolean] of Char              = ('u','s'); //unsigned/signed
  MEMORY : array[TReadWriteType] of AnsiString = ('@a','c','m','s'); //is address, constant, memory, or stack
  FSTART = AnsiChar('{');
  FEND   = AnsiChar('}');

procedure createFragmentFile(fragmentName : AnsiString);
function  fragmentFileExists(fragmentDir,fragmentName : AnsiString) : Boolean;
function  loadFragment(fragmentDir,fragmentName : AnsiString; var fragment : AnsiString) : Boolean;
function  getReadWriteType(v : AnsiString) : TReadWriteType;
function  getNodeMemOrValueStr(node : TAST; symbolTable : TscopedSymbolTable) : AnsiString;
function  getOpStr(t : TToken) : AnsiString;
procedure fillFragment(var fragment : AnsiString; node : TTACNode; symbolTable : TscopedSymbolTable);
function isStackVar(node : TAST; symbolTable : TscopedSymbolTable; var stackOfs : Integer) : Boolean;

var
  aCount : Byte;
  cCount : Byte;
  mCount : Byte;
  pCount : Byte;
  sCount : Byte;

implementation

uses
  uCodeGen_6502,uASM;

function  getOpStr(t : TToken) : AnsiString;
begin
  case t.tType of
    ttPlus    : Result := 'add';
    ttMinus   : Result := 'sub';
    ttTimes   : Result := 'mul';
    ttSlash   : Result := 'div';
    ttBecomes : Result := '=';
    ttNot     : Result := 'not';
    ttNeg     : Result := 'neg';
    ttEql     : Result := '==';
    ttNeq     : Result := '!=';
    ttLss     : Result := 'lss';
    ttLeq     : Result := 'leq';
    ttGtr     : Result := 'gtr';
    ttGeq     : Result := 'geq';
    ttDiv     : Result := 'div';
    ttMod     : Result := 'mod';
    ttAnd     : Result := 'and';
    ttOr      : Result := 'or';
    ttXor     : Result := 'xor';
    ttShl     : Result := 'shl';
    ttShr     : Result := 'shr';
    ttInc     : Result := 'inc';
    ttDec     : Result := 'dec';
    ttDiv256  : Result := 'div256';
    ttMul256  : Result := 'mul256';
    ttDiv2    : Result := 'div2';
    ttMul2    : Result := 'mul2';
    ttLssZero : Result := 'lssZero';
  else
    Result := t.tvalue;
  end;

end;

function  getReadWriteType(v : AnsiString) : TReadWriteType;
begin
  if v[1] = '!' then
  // read/write from number address
    Result := rwMem
  else
  if v[1] = '@' then
  // is @value so return the address instead
    Result := rwAddr
  else
  if (v[1] in ['-','0'..'9','%','$']) then
  // immediate value constant
    Result := rwConst
  else
  // read/write from variable address
    Result := rwMem;
end;

function  isRegisterName(node : TAST; symbolTable : TscopedSymbolTable; var newName : AnsiString) : Boolean;
var
  hiReg,loReg : AnsiString;
  sym         : TSymbol;
  paramType   : TSymbol;
  name        : AnsiString;
  paramName   : AnsiString;
  v           : AnsiString;
  p,i         : Integer;
begin
  Result := False;
  
  v := node.toString;
  newName := v;

  p := pos('.',v);

  if p > 0 then begin
    name      := Copy(v,1,p-1);
    paramName := Copy(v,p+1,Length(v));

    sym       := symbolTable.lookup(name);

    if sym is TProcSymbol then begin
      for i := 0 to TProcSymbol(sym).paramCount - 1 do begin
        if TVarSymbol(TProcSymbol(sym).params[i]).name = paramName then begin
          paramType := TVarSymbol(TProcSymbol(sym).params[i]).symType;

          if (TProcSymbol(sym).procType = ttAssembler) and isAsmParam(paramName,paramType,loReg,hiReg) then begin
            newName := hiReg+loReg; // name minus the '.' part
            Result := True;
            Exit;
          end;
        end;        
      end;
    end
    else
    if sym is TFuncSymbol then begin
      for i := 0 to TFuncSymbol(sym).paramCount - 1 do begin
        if TVarSymbol(TFuncSymbol(sym).params[i]).name = paramName then begin
          paramType := TVarSymbol(TFuncSymbol(sym).params[i]).symType;

          if (TFuncSymbol(sym).funcType = ttAssembler) and isAsmParam(paramName,paramType,loReg,hiReg) then begin
            newName := hiReg+loReg; // name minus the '.' part
            Result := True;
            Exit;
          end;
        end;
      end;
    end;
  end;
end;

function isStackVar(node : TAST; symbolTable : TscopedSymbolTable; var stackOfs : Integer) : Boolean;
var
  sym       : TSymbol;
  name      : AnsiString;
  proc      : AnsiString;
  i,p       : Integer;
  param     : TSymbol;
  stackSize : Integer;
begin
  Result := False;

  name := TVariable(node).token.tValue;

  if not(name[1] in ['_','a'..'z','A'..'Z']) then
    name := Copy(name,2,Length(name));

  sym := symbolTable.lookup(name);

  if sym = nil then begin
    // not found so check for routine.param instead
    p := Pos('.',name);

    if p > 0 then begin
      proc := Copy(name,1,p-1);
      name := Copy(name,p+1,Length(name));

      sym := symbolTable.lookup(proc);

      if isParamVar(proc,name,param,stackSize,symbolTable) then begin
        Result   := param.onStack;
        stackOfs := param.stackOfs;
//        if Result then
//        // is stack routine so expand stack index prior to entering
//          stackOfs := stackOfs + stackSize;
        Exit;
      end;
    end;
  end
  else begin

    if (symbolTable.scopeLevel > 1) and isParamVar(symbolTable.scopeName,sym.symName,param,stackSize,symbolTable) then begin
      Result   := param.onStack;
      stackOfs := param.stackOfs;
      Exit;
    end
    else begin
      Result   := sym.onStack;
      stackOfs := sym.stackOfs;
    end;
  end;
end;

function  getNodeMemOrValueStr(node : TAST; symbolTable : TscopedSymbolTable) : AnsiString;
var
  v        : AnsiString;
  c        : AnsiString;
  stackOfs : Integer;
begin
  if isRegisterName(node,symbolTable,v) then begin
    Result := v;
    
    if (v = 'a') then
      Result := 'aa'
    else
    if (v = 'x') then
      Result := 'xx'
    else
    if (v = 'y') then
      Result := 'yy'
    else
    if (v = 'ax') then
      Result := 'ax'
    else
    if (v = 'xa') then
      Result := 'xa'
  end
  else
  if (v = cA_REGISTER) then
    Result := 'aa'
  else
  if (v = cX_REGISTER) then
    Result := 'xx'
  else
  if (v = cY_REGISTER) then
    Result := 'yy'
  else
  if (v = cAX_REGISTER) then
    Result := 'ax'
  else
  if (v = cXA_REGISTER) then
    Result := 'xa'
  else begin
    if node.deref then
      c := 'p'
    else
      c := MEMORY[getReadWriteType(v)];

    if (c = 'm') and isStackVar(node,symbolTable,stackOfs) then
      c := MEMORY[rwStack];

    Result := c;

    if c = '@a' then begin
      Result := Result + IntToStr(aCount);
      Inc(aCount);
    end
    else
    if c = 'c' then begin
      Result := Result + IntToStr(cCount);
      Inc(cCount);
    end
    else
    if c = 'm' then begin
      Result := Result + IntToStr(mCount);
      Inc(mCount);
    end
    else
    if c = 'p' then begin
      Result := Result + IntToStr(pCount);
      Inc(pCount);
    end
    else
    if c = 's' then begin
      Result := Result + IntToStr(sCount);
      Inc(sCount);
    end;
  end;
end;

procedure createFragmentFile(fragmentName : AnsiString);
var
  sl       : TStringList;
  fileName : AnsiString;
begin
  fileName := ExtractFilePath(ParamStr(0))+'fragments\'+fragmentName;

  sl := TStringList.Create;
  try
    sl.Text := '// '+ChangeFileExt(ExtractFileName(fragmentName),'');
    sl.saveToFile(fileName);
  finally
    sl.Free;
  end;
end;

function  fragmentFileExists(fragmentDir,fragmentName : AnsiString) : Boolean;
var
  fileName : AnsiString;
begin
  fileName := ExtractFilePath(ParamStr(0))+'fragments\'+fragmentDir+fragmentName;

  Result := fileExists(fileName);
end;

function  loadFragment(fragmentDir,fragmentName : AnsiString; var fragment : AnsiString) : Boolean;
var
  sl       : TStringList;
  fileName : AnsiString;
begin
  fileName := ExtractFilePath(ParamStr(0))+'fragments\'+fragmentDir+fragmentName;

  Result := False;

  sl := TStringList.Create;
  try
    if FileExists(fileName) then begin
      sl.loadFromFile(fileName);
      fragment := sl.Text;
      Result   := True;
    end;
  finally
    sl.Free;
  end;
end;

procedure replaceFragrentPartWithValue(var fragment : AnsiString; node : TAST; symbolTable : TscopedSymbolTable);
var
  value    : AnsiString;
  mem      : AnsiString;
  stackOfs : Integer;
begin
  if node = nil then Exit;

  mem   := getNodeMemOrValueStr(node,symbolTable);
  if mem[1] = '@' then
    mem := Copy(mem,2,length(mem));
  
  value := node.toString;
  if      value[1] = '!' then value := Copy(value,2,length(value))
  else if value[1] = '@' then value := Copy(value,2,length(value))
  else if value[1] = '^' then value := Copy(value,2,length(value));

  if isStackVar(node,symbolTable,stackOfs) then
    value := Format(cSTACK_VAR,[stackOfs]);

  fragment := StringReplace(fragment,FSTART+mem+FEND,value,[rfReplaceAll]);
end;

procedure fillFragment(var fragment : AnsiString; node : TTACNode; symbolTable : TscopedSymbolTable);
begin
  aCount := 1;
  cCount := 1;
  mCount := 1;
  pCount := 1;
  sCount := 1;

  if node.left <> nil then begin
    replaceFragrentPartWithValue(fragment,node.left,symbolTable);
    replaceFragrentPartWithValue(fragment,node.leftIndex,symbolTable);

    if (node.left is TVariable) and (TVariable(node.left).indexOffset <> '') then
      replaceFragrentPartWithValue(fragment,TNumber.Create(Token(ttInteger,TVariable(node.left).indexOffset)),symbolTable);
  end;

  if node.right1 <> nil then begin
    replaceFragrentPartWithValue(fragment,node.right1,symbolTable);
    replaceFragrentPartWithValue(fragment,node.right1Index,symbolTable);

    if (node.right1 is TVariable) and (TVariable(node.right1).indexOffset <> '') then
      replaceFragrentPartWithValue(fragment,TNumber.Create(Token(ttInteger,TVariable(node.right1).indexOffset)),symbolTable);
  end;

  if node.right2 <> nil then begin
    replaceFragrentPartWithValue(fragment,node.right2,symbolTable);
//    replaceFragrentPartWithValue(fragment,node.right2.arrayParam,symbolTable);
  end;

  if node.falseLabel <> '' then
    fragment := StringReplace(fragment,FSTART+'l1'+FEND,node.falseLabel,[rfReplaceAll]);

end;

end.
