unit uCodeGen;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  uAST,usesStringWriter;

type
  TCodeGen = class
  protected
    Fwriter      : TsesStringWriter;
    FindentValue : Integer;

    procedure error(aMsg : AnsiString);

    function  indent : AnsiString;

    procedure emitCompare_16(left,right : AnsiString; lower,same,higher : Boolean); virtual; abstract;

    // routine for shifting 8-bit src by shift bits into 16-bit dst
    procedure shiftLeft_0808(src : AnsiString; shift : Byte; dst : AnsiString); virtual; abstract;
    procedure shiftLeft_0816(src : AnsiString; shift : Byte; dst : AnsiString); virtual; abstract;
    procedure shiftLeft_1616(src : AnsiString; shift : Byte; dst : AnsiString); virtual; abstract;
    procedure shiftLeft_08var(src,shift,dst : AnsiString);                      virtual; abstract;

    // routine for shifting 8-bit src by shift bits into 16-bit dst
    procedure shiftRight_0808(src : AnsiString; shift : Byte; dst : AnsiString); virtual; abstract;
    procedure shiftRight_0816(src : AnsiString; shift : Byte; dst : AnsiString); virtual; abstract;
    procedure shiftRight_1616(src : AnsiString; shift : Byte; dst : AnsiString); virtual; abstract;
    procedure shiftRight_08var(src,shift,dst : AnsiString);                      virtual; abstract;

    // dst := src1 + src2
    procedure add_080808(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure add_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure add_081616(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure add_161616(src1,src2,dst: AnsiString);                            virtual; abstract;

    procedure sub_080808(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure sub_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure sub_081616(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure sub_161616(src1,src2,dst: AnsiString);                            virtual; abstract;

    procedure writeIndexed_080808(src,dst,index : AnsiString);                  virtual; abstract;
    procedure writeIndexed_081608(src,dst,index : AnsiString);                  virtual; abstract;
    procedure writeIndexed_080816(src,dst,index : AnsiString);                  virtual; abstract;
    procedure writeIndexed_081616(src,dst,index : AnsiString);                  virtual; abstract;
    procedure writeIndexed_161616(src,dst,index : AnsiString);                  virtual; abstract;
    procedure writeIndexed_160816(src,dst,index : AnsiString);                  virtual; abstract;

    procedure writeIndexedPointer_080808(src,dst,index : AnsiString);           virtual; abstract;
    procedure writeIndexedPointer_081608(src,dst,index : AnsiString);           virtual; abstract;

    procedure readIndexed_080808(src,dst,index : AnsiString);                   virtual; abstract;
    procedure readIndexed_080816(src,dst,index : AnsiString);                   virtual; abstract;
    procedure readIndexed_081608(src,dst,index : AnsiString);                   virtual; abstract;
    procedure readIndexed_081616(src,dst,index : AnsiString);                   virtual; abstract;
    procedure readIndexed_161608(src,dst,index : AnsiString);                   virtual; abstract;
    procedure readIndexed_161616(src,dst,index : AnsiString);                   virtual; abstract;

    procedure readIndexedPointer_080808(src,dst,index : AnsiString);            virtual; abstract;
    procedure readIndexedPointer_081608(src,dst,index : AnsiString);            virtual; abstract;

    procedure readWriteIndexedPointer_080808(src,srcIndex,dst,dstIndex : AnsiString); virtual; abstract;

    // load single register
    procedure loadReg_08(src,loReg,hiReg : AnsiString);                         virtual; abstract;
    // load two registers
    procedure loadReg_16(src,loReg,hiReg : AnsiString);                         virtual; abstract;

    procedure copy_0808(src,dst : AnsiString);                                  virtual; abstract;
    procedure copy_0816(src,dst : AnsiString);                                  virtual; abstract;
    procedure copy_1616(src,dst : AnsiString);                                  virtual; abstract;
    procedure copyAddress_1616(src,dst : AnsiString);                           virtual; abstract;

    procedure mul_080808(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure mul_080816(src1,src2,dst: AnsiString);                            virtual; abstract;

    procedure and_080808(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure and_081616(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure and_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure and_161616(src1,src2,dst: AnsiString);                            virtual; abstract;

    procedure or_080808(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure or_081616(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure or_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure or_161616(src1,src2,dst: AnsiString);                            virtual; abstract;

    procedure xor_080808(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure xor_081616(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure xor_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure xor_161616(src1,src2,dst: AnsiString);                            virtual; abstract;

    procedure neg_0808(right,dst : AnsiString);                                 virtual; abstract;
    procedure neg_0816(right,dst : AnsiString);                                 virtual; abstract;

    procedure not_0808(right,dst : AnsiString);                                 virtual; abstract;
    procedure not_0816(right,dst : AnsiString);                                 virtual; abstract;
    procedure eql_080808(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure eql_161616(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure eql_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure neq_080808(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure neq_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure neq_161616(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure lss_080808(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure lss_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure lss_161616(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure geq_160816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure geq_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure gtr_080808(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure gtr_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
    procedure leq_080816(src1,src2,dst: AnsiString);                            virtual; abstract;
  public
    constructor Create(aWriter : TsesStringWriter); virtual;

    class function getValue_lo(value : AnsiString) : AnsiString;
    class function getValue_hi(value : AnsiString) : AnsiString;

    function  shiftLeft(src,shift,dst : TAST) : Boolean;
    function  shiftRight(src,shift,dst : TAST) : Boolean;
    function  doAdd(left,right,dst : TAST) : Boolean;
    function  doSub(left,right,dst : TAST) : Boolean;
    function  writeIndexed_value(dst,index,src : TAST) : Boolean;
    function  readIndexed_value(dst,index,src : TAST) : Boolean;
    function  readWriteIndexed_value(dst,dstIndex,src,srcIndex : TAST) : Boolean;
    function  copy(src,dst : TAST) : Boolean;
    function  loadRegister(src : TAST; loReg,hiReg : AnsiString) : Boolean;
    function  doMul(left,right,dst : TAST) : Boolean;
    function  doAnd(left,right,dst : TAST) : Boolean;
    function  doOr(left,right,dst : TAST) : Boolean;
    function  doXor(left,right,dst : TAST) : Boolean;
    function  doNeg(right,dst : TAST) : Boolean;
    function  doNot(right,dst : TAST) : Boolean;
    function  doEql(left,right,dst : TAST) : Boolean;
    function  doNeq(left,right,dst : TAST) : Boolean;
    function  doLss(left,right,dst : TAST) : Boolean;
    function  doGtr(left,right,dst : TAST) : Boolean;
    function  doLeq(left,right,dst : TAST) : Boolean;
    function  doGeq(left,right,dst : TAST) : Boolean;

    property indentValue : Integer write FindentValue;
  end;

function  isDerefed(ast : TAST) : Boolean;

implementation

uses
  System.SysUtils;

function  isDerefed(ast : TAST) : Boolean;
begin
  Result := False;

  if not ast.isVariable then Exit;

  Result := TVariable(ast).deref;
end;

constructor TCodeGen.Create(aWriter : TsesStringWriter);
begin
  Fwriter := aWriter;
end;

procedure TCodeGen.error(aMsg: AnsiString);
begin
  raise Exception.Create('CodeGen: '+aMsg);
end;

class function TCodeGen.getValue_lo(value : AnsiString) : AnsiString;
begin
  if (value[1] in ['-','0'..'9','%','$']) then
    Result := '#<'+value
  else
    Result := value + ' + 0';
end;

class function TCodeGen.getValue_hi(value : AnsiString) : AnsiString;
begin
  if (value[1] in ['0'..'9','%','$']) then
    Result := '#>'+value
  else
    Result := value + ' + 1';
end;

function  TCodeGen.indent : AnsiString;
begin
  Result := StringOfChar(' ',FindentValue);
end;

function  TCodeGen.shiftLeft(src,shift,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s << %s',[dst.toString,src.toString,shift.toString]);

  if (dst.size = 1) and (shift.isInteger) then
    shiftLeft_0808(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
  if (dst.size = 1) and (shift.isVariable) then
    shiftLeft_08var(src.toString,shift.toString,dst.toString)
  else
  if (src.size = 1) and (shift.isInteger) and (dst.size = 2) then
    shiftLeft_0816(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
  if (src.size = 2) and (shift.isInteger) and (dst.size = 2) then
    shiftLeft_1616(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) shl %s(%d-bit)" not supported',[dst.toString,dst.size*8,src.toString,src.size*8,shift.toString,shift.size*8]));

  Result := True;
end;

function  TCodeGen.shiftRight(src,shift,dst : TAST) : Boolean;
begin
  Result := False;

  writeLn(Format('// %s := %s >> %s',[dst.toString,src.toString,shift.toString]));
  Fwriter.writeLn(indent+'// %s := %s >> %s',[dst.toString,src.toString,shift.toString]);

  if (dst.size = 1) and (shift.isInteger) then
    shiftRight_0808(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
  if (dst.size = 1) and (shift.isVariable) then
    shiftRight_08var(src.toString,shift.toString,dst.toString)
  else
  if (src.size = 1) and (shift.isInteger) and (dst.size = 2) then
    shiftRight_0816(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
  if (src.size = 2) and (shift.isInteger) and (dst.size = 2) then
    shiftRight_1616(src.toString,TNumber(shift).value and $ff,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) shr %s(%d-bit)" not supported',[dst.toString,dst.size*8,src.toString,src.size*8,shift.toString,shift.size*8]));

  Result := True;
end;

function  TCodeGen.doAdd(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s + %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then
    add_080808(left.toString,right.toString,dst.toString)
  else
  if (left.size = 1) and (right.size = 1) and (dst.size = 2) then
    add_080816(left.toString,right.toString,dst.toString)
  else
  if (left.size = 2) and (right.size = 2) and (dst.size = 2) then
    add_161616(left.toString,right.toString,dst.toString)
  else
  if (left.size = 1) and (right.size = 2) and (dst.size = 2) then
    add_081616(left.toString,right.toString,dst.toString)
  else
  if (left.size = 2) and (right.size = 1) and (dst.size = 2) then
    add_081616(right.toString,left.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) + %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doSub(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s - %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then
    sub_080808(left.toString,right.toString,dst.toString)
  else
  if (left.size = 1) and (right.size = 1) and (dst.size = 2) then
    sub_080816(left.toString,right.toString,dst.toString)
  else
  if (left.size = 2) and (right.size = 2) and (dst.size = 2) then
    sub_161616(left.toString,right.toString,dst.toString)
  else
  if (left.size = 1) and (right.size = 2) and (dst.size = 2) then
    sub_081616(left.toString,right.toString,dst.toString)
  else
  if (left.size = 2) and (right.size = 1) and (dst.size = 2) then
    sub_081616(right.toString,left.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) - %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.writeIndexed_value(dst,index,src : TAST) : Boolean;
var
  c : AnsiString;
  v : Integer;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s[%s] := %s',[dst.toString,index.toString,src.toString]);

  if (src.size = 1) and (src.isString) then begin
    v := charToCBMscreenCode(src.toString[1]);
    src := TNumber.Create(v);
  end;

  if (dst.size = 1) and (index.size = 1) and isDerefed(dst) then begin
//    c := IntToStr(charToCBMscreenCode(src.toString[1]));
    writeIndexedPointer_080808(src.toString,dst.toString,index.toString);
  end
  else
  if (dst.size = 1) and (index.size = 1) and isDerefed(dst) then
    writeIndexedPointer_080808(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 1) and isDerefed(dst) then begin
//    c := IntToStr(charToCBMscreenCode(src.toString[1]));
    writeIndexedPointer_081608(src.toString,dst.toString,index.toString);
  end
  else
  if (dst.size = 2) and (index.size = 1) and isDerefed(dst) then
    writeIndexedPointer_081608(src.toString,dst.toString,index.toString)
  else
{  if (dst.size = 1) and (index.size = 2) and (src.isString) then begin
//    c := IntToStr(charToCBMscreenCode(src.toString[1]));
    writeIndexed_080816(c,dst.toString,index.toString);
  end
  else}
  if (dst.size = 1) and (index.size = 1) and (src.size = 1) then
    writeIndexed_080808(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 2) then
    writeIndexed_160816(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 1) and (src.size = 1) then
    writeIndexed_081608(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 1) and (src.size = 2) then
    writeIndexed_080808(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 1) then
    writeIndexed_080816(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 2) and (src.size = 1) then
    writeIndexed_081616(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 2) and (src.size = 2) then
    writeIndexed_161616(src.toString,dst.toString,index.toString)
  else
    error(Format('"%s(%d-bit)[%s(%d-bit)] := %s(%d-bit)" not supported',[dst.toString,dst.size*8,index.toString,index.size*8,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen.readIndexed_value(dst,index,src : TAST) : Boolean;
var
  c : AnsiString;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s[%s]',[dst.toString,src.toString,index.toString]);

  if (dst.size = 1) and (index.size = 1) and isDerefed(src) then
    readIndexedPointer_080808(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 1) and isDerefed(src) then
    readIndexedPointer_081608(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 1) and (src.size in [0,1]) then
    readIndexed_080808(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 1) and (src.size in [0,1]) then
    readIndexed_081608(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 1) and (index.size = 2) and (src.size = 1) then
    readIndexed_080816(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size = 2) and (src.size = 1) then
    readIndexed_081616(src.toString,dst.toString,index.toString)
  else
  if (dst.size = 2) and (index.size in [1,2]) and (src.size = 2) then
    readIndexed_161616(src.toString,dst.toString,index.toString)
  else
{  if (dst.size = 2) and (index.size = 1) and (src.size = 2) then
    readIndexed_161608(src.toString,dst.toString,index.toString)
  else}
    error(Format('"%s(%d-bit) := %s(%d-bit)[%s(%d-bit)]" not supported',[dst.toString,dst.size*8,src.toString,src.size*8,index.toString,index.size*8]));

  Result := True;
end;

function  TCodeGen.readWriteIndexed_value(dst,dstIndex,src,srcIndex : TAST) : Boolean;
var
  c : AnsiString;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s[%s] := %s[%s]',[dst.toString,dstIndex.toString,src.toString,srcIndex.toString]);

  if isDerefed(dst) and (dstIndex.size = 1) and isDerefed(src) and (srcIndex.size = 1) then
    readWriteIndexedPointer_080808(src.toString,srcIndex.toString,dst.toString,dstIndex.toString)
  else
    error(Format('"%s(%d-bit)^[%s(%d-bit)] := %s(%d-bit)^[%s(%d-bit)]" not supported',[dst.toString,dst.size*8,dstIndex.toString,dstIndex.size*8,src.toString,src.size*8,srcIndex.toString,srcIndex.size*8]));

  Result := True;
end;

function  isPointer(v : TAST) : boolean;
begin
  Result := False;

  if not v.isVariable then exit;

  Result := TVariable(v).isPointer;
end;

function  TCodeGen.copy(src: TAST; dst: TAST) : Boolean;
var
  c : AnsiString;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s',[dst.toString,src.toString]);

  if (dst.size = 2) and  isPointer(src) then begin
    copyAddress_1616(src.toString,dst.toString);
  end
  else
  if (dst.size = 1) and (src.isString) then begin
    c := IntToStr(charToCBMscreenCode(src.toString[1]));
    copy_0808(c,dst.toString);
  end
  else
  if (dst.size = 1) and (src.size = 1) then begin
    copy_0808(src.toString,dst.toString);
  end
  else
  if (dst.size = 2) and (src.size = 1) then begin
    copy_0816(src.toString,dst.toString);
  end
  else
  if (dst.size = 2) and (src.size = 2) then begin
    copy_1616(src.toString,dst.toString);
  end
  else
    error(Format('"%s(%d-bit) := %s(%d-bit)" not supported',[dst.toString,dst.size*8,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen.loadRegister(src : TAST; loReg,hiReg : AnsiString) : Boolean;
begin
  Result := False;

  if (src.size = 1) or (hiReg = '') then
    loadReg_08(src.toString,loReg,hiReg)
  else
  if (src.size = 2) and (hiReg <> '') then
    loadReg_16(src.toString,loReg,hiReg)
  else
    error(Format('Register(s) %s,%s := "%s(%d-bit)" not supported',[loReg,hiReg,src.toString,src.size*8]));

  Result := True;
end;

function  TCodeGen.doMul(left,right,dst : TAST) : Boolean;
var
  v : Byte;
begin
  if (left.size = 1) and (left.isString) then begin
    v := charToCBMscreenCode(left.toString[1]);
    left := TNumber.Create(v);
  end;

  if (right.size = 1) and (right.isString) then begin
    v := charToCBMscreenCode(right.toString[1]);
    right := TNumber.Create(v);
  end;

  if (left.size = 1) and (right.size = 1) and (dst.size = 1) then
    mul_080808(left.toString,right.toString,dst.toString)
  else
  if (left.size = 1) and (right.size = 1) and (dst.size = 2) then
    mul_080816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) & %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doAnd(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s & %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then
    and_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 2) then
    and_081616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    and_081616(right.toString,left.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    and_161616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    and_080816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) & %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doOr(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s | %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then
    or_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 2) then
    or_081616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    or_081616(right.toString,left.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    or_161616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    or_080816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) | %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doXor(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s ^ %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) then //and (left.size = 1) and (right.size = 1) then
    xor_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    xor_161616(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    xor_080816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) ^ %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doNeg(right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := neg %s',[dst.toString,right.toString]);

  if (dst.size = 1) then
    neg_0808(right.toString,dst.toString)
  else
  if (dst.size = 2) and (right.size = 1) then
    neg_0816(right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := neg %s(%d-bit)" not supported',[dst.toString,dst.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doNot(right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := not %s',[dst.toString,right.toString]);

  if (dst.size = 1) and (right.size = 1) then
    not_0808(right.toString,dst.toString)
  else
  if (dst.size = 2) and (right.size = 1) then
    not_0816(right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := not %s(%d-bit)" not supported',[dst.toString,dst.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doEql(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s = %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and ((left.size = 1) or (right.size = 1)) then
    eql_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    eql_080816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    eql_161616(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) = %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doNeq(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s <> %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and ((left.size = 1) or (right.size = 1)) then
    neq_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    neq_080816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    neq_161616(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) <> %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doLss(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s < %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and ((left.size = 1) or (right.size = 1)) then
    lss_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    lss_080816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 2) and (right.size = 2) then
    lss_161616(left.toString,right.toString,dst.toString)
  else
  error(Format('"%s(%d-bit) := %s(%d-bit) < %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doGtr(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s > %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 1) and (left.size = 1) and (right.size = 1) then
    gtr_080808(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    gtr_080816(left.toString,right.toString,dst.toString)
  else

  error(Format('"%s(%d-bit) := %s(%d-bit) > %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doLeq(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s <= %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    leq_080816(left.toString,right.toString,dst.toString)
  else
    error(Format('"%s(%d-bit) := %s(%d-bit) <= %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

function  TCodeGen.doGeq(left,right,dst : TAST) : Boolean;
begin
  Result := False;

  Fwriter.writeLn(indent+'// %s := %s >= %s',[dst.toString,left.toString,right.toString]);

  if (dst.size = 2) and (left.size = 2) and (right.size = 1) then
    geq_160816(left.toString,right.toString,dst.toString)
  else
  if (dst.size = 2) and (left.size = 1) and (right.size = 1) then
    geq_080816(left.toString,right.toString,dst.toString)
  else
  error(Format('"%s(%d-bit) := %s(%d-bit) >= %s(%d-bit)" not supported',[dst.toString,dst.size*8,left.toString,left.size*8,right.toString,right.size*8]));

  Result := True;
end;

end.
