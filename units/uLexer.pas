unit uLexer;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

{$H+}

interface

uses
  System.SysUtils,System.Classes,uToken;

const
  NONE        = AnsiChar(#0);
  STRING_CHAR = AnsiChar('''');
  HASH_CHAR   = AnsiChar('#');


type
  TLexerException = class(Exception);

  TLexerPos = record
    index       : Integer;
    row         : Integer;
    col         : Integer;
    tokenRow    : Integer;
    tokenCol    : Integer;
    currentChar : AnsiChar;
  end;

  TOnLineUpdate = procedure(aCurrentLexerLine : Integer) of Object;

  TLexer = class
  private
    Findex         : Integer;
    Ftext          : AnsiString;

    Frow           : Integer;
    Fcol           : Integer;

    FtokenRow      : Integer;
    FtokenCol      : Integer;

    FcurrentChar   : AnsiChar;
    FreservedWords : array of TToken;

    FonLineUpdate  : TOnLineUpdate;

    FsavedPos      : TLexerPos;

    FstrType       : TTokenType;

    procedure initKeywords;
    procedure registerReservedWord(aTokenType : TTokenType; aIdent : AnsiString);
    function  checkForKeyword(aIdent : AnsiString) : TToken;

    procedure error;

    function  peek : AnsiChar;
    procedure advance;
    procedure advanceUntil(c : AnsiChar);
    procedure skipWhitespace;
    procedure skipMultiLineComment;
    function  ident : AnsiString;
    function  hexNumber : AnsiString;
    function  binNumber : AnsiString;
    function  intNumber : AnsiString;
    function  getHashValue : AnsiChar;
    function  getString : AnsiString;

  protected
  public
    constructor Create;

    procedure init(aStream : TStream);    overload;
    procedure init(aFileName : AnsiString); overload;

    procedure savePosition;
    procedure restorePosition;

    procedure advanceUntil2(c1,c2 : AnsiChar);
    function  readLine : AnsiString;
    function  getQuotedString : AnsiString;
    function  readUntil(match : AnsiString) : AnsiString;
    function  getNextToken : TToken;

    property text      : AnsiString read Ftext;

    property currentChar : AnsiChar   read FcurrentChar;
    property tokenRow    : Integer    read FtokenRow;
    property tokenCol    : Integer    read FtokenCol;
    property row         : Integer    read Frow;

    property offset : Integer    read Findex;

    property onLineUpdate : TonLineUpdate read FonLineUpdate write FonLineUpdate;
  end;

implementation

uses
  Math,uAst;

function  isWhitespace(c : AnsiChar) : Boolean;
begin
  Result := (CharInSet(c,[#9,' ',#10,#13]));
end;

function  isDigit(c : AnsiChar) : Boolean;
begin
  Result := (c >= '0') and (c <= '9');
end;

function  isBinDigit(c : AnsiChar) : Boolean;
begin
  Result := CharInSet(c,['0','1']);
end;

function  isIdentStart(c : AnsiChar) : Boolean;
begin
  c := AnsiChar(LowerCase(c)[1]);

  Result := (CharInSet(c,['_','a'..'z','A'..'Z']));
end;

function  isIdentChar(c : AnsiChar) : Boolean;
begin
  c := AnsiChar(LowerCase(c)[1]);

  Result := (CharInSet(c,['_','a'..'z','0'..'9']));
end;

function  isHexDigit(c : AnsiChar) : Boolean;
begin
  c := AnsiChar(LowerCase(c)[1]);

  Result := (CharInSet(c,['a'..'f','A'..'F','0'..'9']));
end;

constructor TLexer.Create;
begin
  initKeywords;

  FonLineUpdate := nil;
end;

procedure TLexer.init(aFileName : AnsiString);
var
  fs : TFileStream;
begin
  fs := TFileStream.Create(aFileName,fmOpenRead);

  try
    init(fs);
  finally
    fs.Free;
  end;
end;

procedure TLexer.init(aStream : TStream);
begin
  SetLength(Ftext,aStream.Size);

  aStream.Seek(0,soFromBeginning);
  aStream.Read(Ftext[1],aStream.Size);
  Findex  := 1;
  Frow    := 1;
  Fcol    := 0;

  FtokenRow := Frow;
  FTokenCol := Fcol;

  if Ftext <> '' then
    FcurrentChar := Ftext[Findex];
end;

procedure TLexer.initKeywords;
begin
  SetLength(FreservedWords,0);

  registerReservedWord(ttUnit           ,'unit');
  registerReservedWord(ttInterface      ,'interface');
  registerReservedWord(ttImplementation ,'implementation');
  registerReservedWord(ttProgram        ,'program');
  registerReservedWord(ttUses           ,'uses');
  registerReservedWord(ttTarget         ,'target');
  registerReservedWord(ttcdecl          ,'cdecl');

  registerReservedWord(ttImportSegments   ,'importSegments');
  registerReservedWord(ttImportHeader     ,'importHeader');
  registerReservedWord(ttSetIRQsegment    ,'SetIRQsegment');
  registerReservedWord(ttSetPRGROMsegment ,'SetPRGROMsegment');

  // unit 'system' types
  registerReservedWord(ttC64            ,'c64');
  registerReservedWord(ttC128           ,'c128');
  registerReservedWord(ttBBC            ,'bbc');
  registerReservedWord(ttNES            ,'nes');
  registerReservedWord(ttAtari2600      ,'atari2600');
  registerReservedWord(ttAtari8Bit      ,'atari8Bit');
  registerReservedWord(ttVic20          ,'vic20');
  registerReservedWord(ttAppleII        ,'appleii');
  registerReservedWord(ttPET            ,'pet');
//  registerReservedWord(ttAtari2600      ,'atari2600');
//  registerReservedWord(ttAtari400       ,'atari400');
//  registerReservedWord(ttAtari800       ,'atari800');
//  registerReservedWord(ttAtari1200XL    ,'atari1200xl');
//  registerReservedWord(ttAtari5200      ,'atari5200');
//  registerReservedWord(ttAtariXEGS      ,'atarixegs');
//  registerReservedWord(ttAtariXL        ,'atarixl');
//  registerReservedWord(ttAtariXE        ,'atarixe');
// -------------------------------------------
  registerReservedWord(ttNESheader      ,'nesHeader');
  registerReservedWord(ttDec            ,'dec');
  registerReservedWord(ttInc            ,'inc');
  registerReservedWord(ttBegin          ,'begin');
  registerReservedWord(ttEnd            ,'end');
  registerReservedWord(ttVar            ,'var');
  registerReservedWord(ttProc           ,'procedure');
  registerReservedWord(ttFunc           ,'function');
  registerReservedWord(ttForward        ,'forward');
  registerReservedWord(ttConst          ,'const');
  registerReservedWord(ttReal           ,'real');
  registerReservedWord(ttInteger        ,'integer');
  registerReservedWord(ttShortInt       ,'shortint');
  registerReservedWord(ttPointer        ,'pointer');
  registerReservedWord(ttByte           ,'byte');
  registerReservedWord(ttWord           ,'word');
  registerReservedWord(ttDWord          ,'dword');
  registerReservedWord(ttInt24          ,'int24');
  registerReservedWord(ttBoolean        ,'boolean');
  registerReservedWord(ttType           ,'type');
  registerReservedWord(ttRecord         ,'record');
  registerReservedWord(ttString         ,'string');
  registerReservedWord(ttAscii          ,'ascii');
  registerReservedWord(ttPetscii        ,'petscii');
  registerReservedWord(ttTrue           ,'true');
  registerReservedWord(ttFalse          ,'false');
  registerReservedWord(ttDiv            ,'div');
  registerReservedWord(ttMod            ,'mod');
  registerReservedWord(ttIf             ,'if');
  registerReservedWord(ttThen           ,'then');
  registerReservedWord(ttElse           ,'else');
  registerReservedWord(ttWhile          ,'while');
  registerReservedWord(ttDo             ,'do');
  registerReservedWord(ttRepeat         ,'repeat');
  registerReservedWord(ttUntil          ,'until');
  registerReservedWord(ttFor            ,'for');
  registerReservedWord(ttTo             ,'to');
  registerReservedWord(ttDownTo         ,'downto');
  registerReservedWord(ttOf             ,'of');
  registerReservedWord(ttArray          ,'array');
  registerReservedWord(ttNot            ,'not');
  registerReservedWord(ttAnd            ,'and');
  registerReservedWord(ttOr             ,'or');
  registerReservedWord(ttXor            ,'xor');
  registerReservedWord(ttShl            ,'shl');
  registerReservedWord(ttShr            ,'shr');
  registerReservedWord(ttAbsolute       ,'absolute');
  registerReservedWord(ttExternal       ,'external');
//  registerReservedWord(ttRasterIRQ      ,'rasterIRQ');
  registerReservedWord(ttKernalInterrupt,'KernalInterrupt');
  registerReservedWord(ttInterrupt      ,'Interrupt');
  registerReservedWord(ttSetupIRQ       ,'setupIRQ');
//  registerReservedWord(ttSetupRasterIRQ ,'SetupRasterIRQ');
  registerReservedWord(ttEndIRQ         ,'endIRQ');
  registerReservedWord(ttAssembler      ,'assembler');
  registerReservedWord(ttMacro          ,'macro');
  registerReservedWord(ttImport         ,'import');
  registerReservedWord(ttImportBinary   ,'importBinary');
  registerReservedWord(ttSource         ,'source');
  registerReservedWord(ttAsm            ,'asm');
  registerReservedWord(ttPoke           ,'poke');
  registerReservedWord(ttPeek           ,'peek');
  registerReservedWord(ttWriteLn        ,'writeln');
  registerReservedWord(ttWrite          ,'write');
  registerReservedWord(ttLength         ,'length');
  registerReservedWord(ttExit           ,'exit');
  registerReservedWord(ttBreak          ,'break');
  registerReservedWord(ttAsc            ,'asc');
  registerReservedWord(ttChr            ,'chr');
  registerReservedWord(ttLo             ,'lo');
  registerReservedWord(ttHi             ,'hi');
  registerReservedWord(ttSin            ,'sin');
  registerReservedWord(ttCos            ,'cos');
  registerReservedWord(ttTan            ,'tan');
  registerReservedWord(ttKickAsm        ,'KickAsm');

  registerReservedWord(ttVolatile       ,'volatile');
  registerReservedWord(ttdecimalmodeon  ,'decimalmodeon');
  registerReservedWord(ttdecimalmodeoff ,'decimalmodeoff');
end;

procedure TLexer.error;
begin
  raise TLexerException.Create(Format('Invalid character: found "'+FcurrentChar+'" at (line: %d, col: %d)',[Frow,FCol]));
end;

procedure TLexer.savePosition;
begin
  FsavedPos.index       := Findex;
  FsavedPos.row         := Frow;
  FsavedPos.col         := Fcol;
  FsavedPos.tokenRow    := FtokenRow;
  FsavedPos.tokenCol    := FtokenCol;
  FsavedPos.currentChar := FcurrentChar;
end;

procedure TLexer.restorePosition;
begin
  Findex       := FsavedPos.index;
  Frow         := FsavedPos.row;
  Fcol         := FsavedPos.col;
  FtokenRow    := FsavedPos.tokenRow;
  FtokenCol    := FsavedPos.tokenCol;
  FcurrentChar := FsavedPos.currentChar;
end;

function  TLexer.peek : AnsiChar;
var
  peekPos : Integer;
begin
  peekPos := Findex + 1;

  if (peekPos > Length(Ftext)) then
    Result := NONE
  else
    Result := Ftext[peekPos];
end;

procedure TLexer.advance;
begin
  if Assigned(FonlineUpdate) then
    FonLineUpdate(Frow);

  Inc(Findex);

  if (Findex > Length(Ftext)) then
    FcurrentChar := NONE
  else begin
    FcurrentChar := Ftext[Findex];

    if (FcurrentChar = #13) then begin
      Inc(Frow);
      Fcol := 1;
    end
    else if not(CharInSet(FcurrentChar,[#13,#10,#9])) then begin
      Inc(Fcol);
    end;
  end;
end;

procedure TLexer.advanceUntil(c : AnsiChar);
begin
  while (FcurrentChar <> NONE) and (FcurrentChar <> c) do
    advance;
end;

procedure TLexer.advanceUntil2(c1,c2 : AnsiChar);
begin
  while (FcurrentChar <> NONE) and (FcurrentChar <> c1) and (peek <> c2) do
    advance;
  advance;
end;

procedure TLexer.skipWhitespace;
begin
  while (FcurrentChar <> NONE) and isWhitespace(FcurrentChar) do
    advance;
end;

function  TLexer.ident : AnsiString;
begin
  Result := '';
  while (FcurrentChar <> NONE) and isIdentChar(FcurrentChar) do begin
    Result := Result + FcurrentChar;
    advance;
  end;
end;

function  TLexer.intNumber : AnsiString;
begin
  Result := '';
  while (FcurrentChar <> NONE) and isDigit(FcurrentChar) do begin
    Result := Result + FcurrentChar;
    advance;
  end;
end;

function  TLexer.hexNumber : AnsiString;
begin
  Result := FcurrentChar;

  advance; // consume '$'

  while (FcurrentChar <> NONE) and isHexDigit(FcurrentChar) do begin
    Result := Result + FcurrentChar;
    advance;
  end;
end;

function  TLexer.binNumber : AnsiString;
begin
  Result := FcurrentChar;

  advance; // consume '%'

  while (FcurrentChar <> NONE) and isBinDigit(FcurrentChar) do begin
    Result := Result + FcurrentChar;
    advance;
  end;
end;

function convertString(var s : AnsiString; stringType : AnsiChar) : TTokenType;
var
  i : Integer;
  c : AnsiChar;
begin
    case stringType of
      // screen code string
      's' : Result := ttString;
      // ascii string
      'a' : Result := ttAscii;
      // petscii string
      'p' : Result := ttPetscii;
    end;
//  for i := 1 to Length(s) do begin
//    case stringType of
//      // screen code string
//      's' : begin
//        c := AnsiChar(cPETSCIIToCBMScreenCode[cASCIIToPETSCIITable[Ord(s[i])]]);
//        Result := ttString;
//      end;
//      // ascii string
//      'a' : begin
//        c := s[i];
//        Result := ttAscii;
//      end;
//      // petscii string
//      'p' : begin
//        c := AnsiChar(cASCIIToPETSCIITable[Ord(s[i])]);
//        Result := ttPetscii;
//      end;
//    end;
//
////    s[i] := c;
//  end;
end;

function  TLexer.getHashValue : AnsiChar;
// returns #<value> as an AnsiChar
var
  codeStr   : AnsiString;
  codeValue : Integer;
begin
  // eat the #
  advance;

  codeStr := '';
  while isDigit(FcurrentChar) do begin
    codeStr := codeStr + FcurrentChar;
    advance;
  end;

  codeValue := StrToInt('0'+codeStr);
  codeValue := min(255,codeValue);

  Result := AnsiChar(Chr(codeValue));
end;

function  TLexer.getString : AnsiString;
var
  stringType : AnsiChar;
begin
  Result := '';

  FstrType := ttString;

  advance; // consume STRING_CHAR

  while (FcurrentChar <> NONE) and (FcurrentChar <> STRING_CHAR) do begin
    Result := Result + FcurrentChar;
    advance;
  end;

  advance; // consume STRING_CHAR

  if (FcurrentChar = STRING_CHAR) then
    // 'escaped' double '' = single '
    Result := Result + STRING_CHAR + getString;

  if (FcurrentChar in ['s','p','a']) then begin
    // must be string type postfix; screen code, petscii, ascii
    // respectively so read it
    stringType := FcurrentChar;
    advance;
    FstrType := convertString(Result,stringType);
  end;
end;

procedure TLexer.registerReservedWord(aTokenType : TTokenType; aIdent : AnsiString);
var
  i : Integer;
begin
  i := Length(FreservedWords);

  SetLength(FreservedWords,i + 1);

  FreservedWords[i] := Token(aTokenType,LowerCase(aIdent));
end;

function  TLexer.checkForKeyword(aIdent : AnsiString) : TToken;
var
  i : Integer;
begin
  Result := Token(ttIdent,aIdent);

  for i := 0 to High(FreservedWords) do begin
    if (LowerCase(aIdent) = FreservedWords[i].tValue) then begin
      Result := Token(FreservedWords[i].tType,FreservedWords[i].tValue);
      Break;
    end;
  end;
end;

function  TLexer.readLine : AnsiString;
begin
  Result := '';

  while (FcurrentChar <> NONE) and (FcurrentChar <> #13) do begin
    Result := Result + FcurrentChar;
    advance;
  end;

  // skip end of line chars
  while (FcurrentChar in [#13,#10]) do
    advance;
end;

function  TLexer.getQuotedString : AnsiString;
begin
  Result := '';

  while (FcurrentChar <> NONE) and (FcurrentChar <> '"') do begin
    Result := Result + FcurrentChar;
    advance;
  end;
end;

function  TLexer.readUntil(match : AnsiString) : AnsiString;
begin
  Result := '';

  if match = '' then Exit;

  if Length(match) > 2 then match := Copy(match,1,2);

  if Length(match) = 1 then begin
    repeat
      Result := Result + FcurrentChar;
      advance;

    until (FcurrentChar = NONE) or (FcurrentChar = match[1]);

    if FcurrentCHar = NONE then error;

    advance;
    skipWhiteSpace;
  end
  else begin
  // assume match is 2 characters
    repeat
      Result := Result + FcurrentChar;
      advance;

    until (FcurrentChar = NONE) or ((FcurrentChar = match[1]) and (peek = match[2]));

    if FcurrentCHar = NONE then error;

    advance;
    advance;
    skipWhiteSpace;
  end;
end;

function  TLexer.getNextToken : TToken;
begin
  skipWhitespace;

  while (FcurrentChar <> NONE) do begin
    if (isWhitespace(FcurrentChar)) then begin
      skipWhitespace;

      continue;
    end
    else
    if (FcurrentChar = '(') and (peek = '*') then begin
      repeat
        advance;
      until (FcurrentChar = NONE) or ((FcurrentChar = '*') and (peek = ')'));
      advance;
      advance;

      continue;
    end
    else
    if (FcurrentChar = '{') and (peek = '{') then begin
      Result := Token(ttLKickAsm,'{{');
      advance;
      advance;
      Exit;
    end
    else
    if (FcurrentChar = '}') and (peek = '}') then begin
      Result := Token(ttRKickAsm,'}}');
      advance;
      advance;
      Exit;
    end
    else
    if FcurrentChar = '{' then begin
      advance; // the '{'
      skipMultiLineComment;

      continue;
    end
    else
    if (FcurrentChar = '/') and (peek = '/') then begin
    // skip '//' comments
      advanceUntil(#13);
      skipWhitespace;

      continue;
    end;

    FtokenRow := Frow;
    FtokenCol := Fcol;

    if (FcurrentChar in [STRING_CHAR]) then begin
      Result.tValue := getString;
      Result.tType  := FstrType;
      Exit;
    end
    else
    if (isIdentStart(FcurrentChar)) then begin
      Result := checkForKeyword(ident);
      Exit;
    end
    else
    if (FcurrentChar = '$') then begin
      Result := Token(ttIntNum,hexNumber);
      Exit;
    end
    else
    if (FcurrentChar = '%') then begin
      Result := Token(ttIntNum,binNumber);
      Exit;
    end
    else
    if (FcurrentChar = '@') then begin
      Result := Token(ttAt,FcurrentChar);
      advance;
      Exit;
    end
    else
    if (FcurrentChar = '"') then begin
      Result := Token(ttQuote,binNumber);
      Exit;
    end
    else
    if (FcurrentChar = '?') then begin
      Result := Token(ttQuestionMark,binNumber);
      Exit;
    end
    else
    if (isDigit(FcurrentChar)) then begin
      Result := Token(ttIntNum,intNumber);
      Exit;
    end
    else
    if (FcurrentChar = '*') then begin
      Result := Token(ttTimes,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '/') then begin
      Result := Token(ttSlash,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '+') then begin
      Result := Token(ttPlus,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '-') then begin
      Result := Token(ttMinus,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '!') then begin
      Result := Token(ttExclamation,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '|') then begin
      Result := Token(ttPipe,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '&') then begin
      Result := Token(ttAmpersand,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '#') then begin
      Result := Token(ttHash     ,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '[') then begin
      Result := Token(ttLBracket,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = ']') then begin
      Result := Token(ttRBracket,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '^') then begin
      Result := Token(ttCaret,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '(') then begin
      Result := Token(ttLParen,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = ')') then begin
      Result := Token(ttRParen,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '.') then begin
      Result := Token(ttPeriod,FcurrentChar);

      if (peek = '.') then begin
        Result := Token(ttRange,'..');
        advance;
      end;
      advance;
      Exit;
    end
    else if (FcurrentChar = ';') then begin
      Result := Token(ttSemiColon,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = ',') then begin
      Result := Token(ttComma,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = ':') then begin
      Result := Token(ttColon,FcurrentChar);

      if (peek = '=') then begin
        Result := Token(ttBecomes,':=');
        advance;
      end;
      advance;
      Exit;
    end
    else if (FcurrentChar = '=') then begin
      Result := Token(ttEql,FcurrentChar);
      advance;
      Exit;
    end
    else if (FcurrentChar = '<') then begin
      Result := Token(ttlss,FcurrentChar);

      if (peek = '=') then begin
        Result := Token(ttLeq,'<=');
        advance;
      end
      else
      if (peek = '>') then begin
        Result := Token(ttNeq,'<>');
        advance;
      end;
      advance;
      Exit;
    end
    else if (FcurrentChar = '>') then begin
      Result := Token(ttGtr,FcurrentChar);

      if (peek = '=') then begin
        Result := Token(ttGeq,'>=');
        advance;
      end;
      advance;
      Exit;
    end
    else if (FcurrentChar = ':') and (peek = '=') then begin
      Result := Token(ttBecomes,':=');
      advance;
      advance;
      Exit;
    end
    else
      error;
  end;

  Result := Token(ttEOF,'EOF');
end;

procedure TLexer.skipMultiLineComment;
begin
  while (FcurrentChar <> NONE) and (FcurrentChar <> '}') do begin
    advance;
  end;
  advance; // the closing '}'
end;

end.
