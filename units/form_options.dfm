object OptionsDialog: TOptionsDialog
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Set Emulator Paths'
  ClientHeight = 347
  ClientWidth = 687
  Color = clBtnFace
  ParentFont = True
  Position = poScreenCenter
  TextHeight = 15
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 687
    Height = 302
    Align = alClient
    Caption = 'Path Options'
    TabOrder = 0
    ExplicitWidth = 677
    ExplicitHeight = 270
    object PageControl1: TPageControl
      Left = 2
      Top = 17
      Width = 683
      Height = 283
      ActivePage = TabSheet8
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'C64'
        object c64_GroupBox: TGroupBox
          Left = 3
          Top = 3
          Width = 537
          Height = 49
          Caption = 'C64 Emulator Options'
          TabOrder = 0
          object Label2: TLabel
            Left = 8
            Top = 20
            Width = 93
            Height = 15
            Caption = 'Emulator .exe File'
          end
          object X64File_Edit: TEdit
            Left = 131
            Top = 17
            Width = 361
            Height = 23
            ImeName = 'X64File_Edit'
            TabOrder = 0
            Text = 'C:\Emulators\WinVICE-2.2-x64\x64.exe'
          end
          object X64FileBrowse_Button: TButton
            Left = 498
            Top = 17
            Width = 25
            Height = 21
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = X64FileBrowse_ButtonClick
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'C128'
        ImageIndex = 1
        object c128_GroupBox: TGroupBox
          Left = 3
          Top = 3
          Width = 537
          Height = 49
          Caption = 'C128 Emulator Options'
          TabOrder = 0
          object Label5: TLabel
            Left = 8
            Top = 20
            Width = 93
            Height = 15
            Caption = 'Emulator .exe File'
          end
          object X128File_Edit: TEdit
            Left = 131
            Top = 17
            Width = 361
            Height = 23
            TabOrder = 0
            Text = 'C:\Emulators\WinVICE-2.2-x64\x128.exe'
          end
          object c128Browse_Button: TButton
            Left = 498
            Top = 17
            Width = 25
            Height = 21
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = c128Browse_ButtonClick
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Vic-20'
        ImageIndex = 2
        object vic20_GroupBox: TGroupBox
          Left = 3
          Top = 3
          Width = 537
          Height = 49
          Caption = 'VIC-20 Emulator Options'
          TabOrder = 0
          object Label7: TLabel
            Left = 8
            Top = 24
            Width = 93
            Height = 15
            Caption = 'Emulator .exe File'
          end
          object vic20File_Edit: TEdit
            Left = 131
            Top = 17
            Width = 361
            Height = 23
            ImeName = 'Edit'
            TabOrder = 0
            Text = 'C:\Emulators\WinVICE-2.2-x64\xvic.exe'
          end
          object vic20FileBrowse_Button: TButton
            Left = 498
            Top = 17
            Width = 25
            Height = 21
            Caption = '...'
            DisabledImageName = 'vic20FileBrowse_Button'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = vic20FileBrowse_ButtonClick
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'BBC micro'
        ImageIndex = 3
        object bbc_GroupBox: TGroupBox
          Left = 3
          Top = 3
          Width = 537
          Height = 49
          Caption = 'BBC Emulator Options'
          TabOrder = 0
          object Label4: TLabel
            Left = 8
            Top = 20
            Width = 93
            Height = 15
            Caption = 'Emulator .exe File'
          end
          object BBC_EmulatorFile_Edit: TEdit
            Left = 131
            Top = 17
            Width = 361
            Height = 23
            TabOrder = 0
            Text = 'C:\emulators\BeebEm\BeebEm.exe'
          end
          object BBCbrowse_Button: TButton
            Left = 498
            Top = 17
            Width = 25
            Height = 21
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = BBCbrowse_ButtonClick
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Atari 8-bit'
        ImageIndex = 4
        object atari8Bit_GroupBox: TGroupBox
          Left = 3
          Top = 3
          Width = 537
          Height = 49
          Caption = 'Atari 8-Bit Emulator Options'
          TabOrder = 0
          object Label6: TLabel
            Left = 8
            Top = 20
            Width = 93
            Height = 15
            Caption = 'Emulator .exe File'
          end
          object Atari8BitFile_Edit: TEdit
            Left = 131
            Top = 17
            Width = 361
            Height = 23
            ImeName = 'US'
            TabOrder = 0
            Text = 'C:\emulators\Altirra\Altirra.exe'
          end
          object Atari8BitBrowse_Button: TButton
            Left = 498
            Top = 17
            Width = 25
            Height = 21
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = Atari8BitBrowse_ButtonClick
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'NES'
        ImageIndex = 5
        object GroupBox2: TGroupBox
          Left = 7
          Top = 3
          Width = 537
          Height = 49
          Caption = 'NES Emulator Options'
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 20
            Width = 93
            Height = 15
            Caption = 'Emulator .exe File'
          end
          object NESfile_Edit: TEdit
            Left = 131
            Top = 17
            Width = 361
            Height = 23
            ImeName = 'US'
            TabOrder = 0
            Text = 'C:\emulators\nes\fceux\fceux.exe'
          end
          object NESbrowseFile_Button: TButton
            Left = 498
            Top = 17
            Width = 25
            Height = 21
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = NESbrowseFile_ButtonClick
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = 'Apple II'
        ImageIndex = 6
        object GroupBox3: TGroupBox
          Left = 7
          Top = 3
          Width = 537
          Height = 49
          Caption = 'Apple II Emulator Options'
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 20
            Width = 93
            Height = 15
            Caption = 'Emulator .exe File'
          end
          object AppleIIFile_Edit: TEdit
            Left = 131
            Top = 19
            Width = 361
            Height = 23
            ImeName = 'US'
            TabOrder = 0
            Text = 'C:\emulators\nes\fceux\fceux.exe'
          end
          object appleBrowseFile_Button: TButton
            Left = 498
            Top = 17
            Width = 25
            Height = 21
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = appleBrowseFile_ButtonClick
          end
        end
      end
      object TabSheet8: TTabSheet
        Caption = 'PET'
        ImageIndex = 7
        object GroupBox4: TGroupBox
          Left = 5
          Top = 5
          Width = 537
          Height = 49
          Caption = 'PET Emulator Options'
          TabOrder = 0
          object Label8: TLabel
            Left = 8
            Top = 24
            Width = 93
            Height = 15
            Caption = 'Emulator .exe File'
          end
          object petFile_Edit: TEdit
            Left = 131
            Top = 17
            Width = 361
            Height = 23
            ImeName = 'US'
            TabOrder = 0
            Text = 'C:\Emulators\WinVICE-2.2-x64\xpet.exe'
          end
          object petBrowseFile_Button: TButton
            Left = 498
            Top = 17
            Width = 25
            Height = 21
            Caption = '...'
            DisabledImageName = 'vic20FileBrowse_Button'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            OnClick = vic20FileBrowse_ButtonClick
          end
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 302
    Width = 687
    Height = 45
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 270
    ExplicitWidth = 677
    object OKBtn: TButton
      Left = 212
      Top = 8
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object CancelBtn: TButton
      Left = 368
      Top = 6
      Width = 75
      Height = 27
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
  object OpenDialog: TOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 352
    Top = 64
  end
end
