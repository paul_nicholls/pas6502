﻿unit uParserAST;

{$ifdef fpc}
{$MODE Delphi}
{$endif}
// ------------------------------------------------------------------------
// This file is part of Pas6502.
//
// You can find Pas6502 here:
// https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
// Pas6502 is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Pas6502 is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
// ------------------------------------------------------------------------

interface

uses
  System.Contnrs,System.SysUtils, System.Classes, uLexer, uToken, uAST, uSymbolTable,uCodeInsight;

const
  MIN_POINTER_ADDRESS = $04;
  MAX_POINTER_ADDRESS = $7F;

  STR_TYPE = ['s','a','p'];

  cNES   = 0;
  cNESVS = 1;

  cNES_HEADER    = 'NES_HEADER';
  cSEGMENT       = 'SEGMENT';
  cCLEARSEGMENTS = 'CLEARSEGMENTS';
  cADDSEGMENT    = 'ADDSEGMENT';
  cMEMORY        = 'MEMORY';
  cSEGMENTS      = 'SEGMENTS';

type
  TParseException = class(Exception);

  TOnScopeUpdate = procedure(aScope : TScopedSymbolTable; aCurrentFile : AnsiString) of Object;

  TCommandsList = array of AnsiString;

  TPointerType = class
  public
    pointerType  : AnsiString;
    typeIdenfier : AnsiString;
    typeSym      : TSymbol;
  end;

  TRecInfo = class
    fullName  : AnsiString;
    varName   : AnsiString;
    varType   : TToken;
    varSize   : Integer;
    varMemOfs : Integer;
    isSigned  : Boolean;
  end;

  TRecInfoArray = array of TRecInfo;

  TParserAST = class
  private
    Flines   : TStringList;
    FsrcLine : Integer;
    Flexer: TLexer;
    Ftoken: TToken;
    FerrorMsg: AnsiString;
    FprogAST: TProgram;
    FpointerAddress: Integer;

    FonScopeUpdate : TOnScopeUpdate;

    FCodeInsightData : TCodeInsightData;

    FcurrentFile     : AnsiString;
    FcurrentFileFull : AnsiString;

    FcurrentScope: TscopedSymbolTable;
    FparsingUnit: Boolean;
    FprojectPath: AnsiString;
    FloopLevel: Integer;
    FglobalBlock : TBlock;

    FreplaceConstVar : Boolean;
    FconstVarName    : AnsiString;
    FconstVarValue   : Integer;
    FsavedToken      : TToken;
    FsavedPntrTypes  : TList;
    FconstStringNumber : Integer;
    FdecimalModeOn     : Boolean;

    FrecordVarInfo     : TStringList;

    procedure stripComments(comment : AnsiString; var aCode : AnsiString);
    procedure processAsmCommand(op,operand : AnsiString);
    procedure checkForVarUsageInAsmCode(line : AnsiString);
    function  recVarExists(recName,varName : AnsiString; var info : TRecInfo) : Boolean;
    function  getRecVarInfo(recName : AnsiString) : TRecInfoArray;
    procedure addRecVarInfo(recName,varName : AnsiString; varType : TToken; size,memOfs : Integer; isSigned : Boolean);
    procedure clearRecVarInfo;

    procedure error(aMsg: AnsiString);
    function accept(S: TTokenType): Boolean;                      overload;
    function accept(S: TTokenType; aValue : AnsiString): Boolean; overload;
    function expect(S: TTokenType): Boolean;                      overload;
    function expect(S: TTokenType; aValue : AnsiString): Boolean; overload;
    procedure nextToken;

    procedure savePosition;
    procedure restorePosition;

    procedure onLineUpdate(aCurrentLexerLine : Integer);

    function  isRecordArrayVar(arrayVar : AnsiString; var sym : TSymbol; var recArrayVar : AnsiString) : Boolean;
    function  isCustomTypePointerVar(sym : TSymbol; var isSigned : Boolean; var  derefRWsize, derefOffset : Integer; var recName : AnsiString) : Boolean;
    procedure checkforIndexPlusOffset(var v : TVariable);
    function  factor: TAST;
    function  signedFactor: TAST;
    function  term: TAST;
    function  simpleExpression: TAST;
    function  expression: TAST;

    function  asmCompound: TAsmCompound;
    function  compound: TCompound;

    procedure addRecordVarToCodeInsight(aFullyQualifiedName : AnsiString; aVarDecl: TVarDecl);
    procedure expandRecordVarToSymbolTable(aVarDecl: TVarDecl);

    procedure addVarRecordInfoToSymbolTable(varName : AnsiString; recordInfo : TRecInfoArray);
    procedure generateStackInfo(routine : TRoutine; block : TBlock);
    function formalParams(var paramCount: Integer): TParamArray;
    procedure procDecl(aBlock: TBlock);
    procedure funcDecl(aBlock: TBlock);
    procedure macroDecl(aBlock: TBlock);
    function  getPointerAddress: Byte;
    function  parseKickAsmData : AnsiString;

    procedure segmentDecl(aBlock: TBlock);
    procedure clearSegments;
    procedure addSegment;

    procedure expandRecordTypeToSymbolTable(aVarDecl: TVarDecl);
    procedure doTypedConstDecl_array(aConstName: AnsiString; aBlock: TBlock; offset : Integer; fileName : String);
    procedure constDecls(aBlock: TBlock);
    procedure typeDecls(aBlock: TBlock);
    procedure varDecls(aBlock: TBlock);

    procedure unitDecls(aProg: TProgram; aUnitName: AnsiString);
    function block(aBlockName: AnsiString; aRetType: TTokenType;
      aIsFunc: Boolean; aIsMacro: Boolean): TBlock;
    function asmBlock(aBlockName: AnsiString; aIsAbsoluteAsmProc: Boolean;
      aRetType: TTokenType; aIsFunc: Boolean; aIsMacro: Boolean): TAsmBlock;
    function statement: TAST;
    function doPoke: TPoke;
    function doIf: TIfThen;
    function doWhile: TWhile;
    function doRepeat: TRepeat;
    function doFor: TFor;
    function doBuiltInProc(aType: TTokenType; aName: AnsiString)
      : TBuiltinProcedure;
    function doBuiltInFunc(aType: TTokenType; aName: AnsiString)
      : TBuiltinFunction;
    function doAssign(aVariable: TVariable): TAST;
    function doDecInc(decIncToken : TToken) : TAST;
    function  getConstStringName : AnsiString;
    procedure processStringParam(var param : TAST);
    procedure getCallParams(params : TASTList; var paramCount : Integer);
    function  doCall(aProcName: AnsiString): TAST;
    function  doFuncCall(aFuncName: AnsiString): TAST;
    function  doMacroCall(aMacroName: AnsiString): TAST;

    procedure doUnit;
    procedure loadUnit(aProg: TProgram; aUnitName: AnsiString);
    procedure parseUsesClause(aProg: TProgram);
    procedure doSegments;
    procedure doCfgFile;
    procedure loadCfgFile(aProg: TProgram; aCfgName: AnsiString);
    procedure extractMacroNamesFromSource(aAsmFileName: AnsiString);
    procedure parseImport(aProg: TProgram; aBlock: TBlock);
    procedure registerImportFile(aProg: TProgram; aFileName: AnsiString);
    procedure parseImportBinary(aConstName: AnsiString; aBlock : TBlock);
    procedure addConst(name,value : AnsiString);
    procedure addVar(name,aType : AnsiString);
    procedure copyImportsToProjectFolder;
    procedure importTargetIniData;
    function doProgram: TProgram;

    procedure getNES_segmentsInfo;
    procedure getNES_headerInfo;
    procedure getNES_IRQinfo;
    procedure getNES_PRGROMinfo;
  public
    constructor Create(aCodeInsightData : TCodeInsightData; aOnScopeUpdate : TOnScopeUpdate);
    destructor Destroy;

    function parse(aStream: TStream; aParsingUnit: Boolean): Boolean; overload;
    function parse(aFileName: AnsiString; aParsingUnit: Boolean) : Boolean; overload;
    function parseCfg(aProgram: TProgram; aScope: TscopedSymbolTable;
      aFileName: AnsiString): Boolean;
    function parseUnit(aProgram: TProgram; aScope: TscopedSymbolTable;
      aFileName: AnsiString): Boolean;
    function parseAsmFile(aProgram: TProgram; aScope: TscopedSymbolTable;
      aFileName: AnsiString): Boolean;

    property errorMsg       : AnsiString read FerrorMsg;

    property progAST        : TProgram   read FprogAST;
    property projectPath    : AnsiString read FprojectPath    write FprojectPath;
    property pointerAddress : Integer    read FpointerAddress write FpointerAddress;
    property srcLine        : Integer    read FsrcLine        write FsrcLine;
    property scope          : TScopedSymbolTable read FcurrentScope;
    property currentFile    : AnsiString         read FcurrentFile write FcurrentFile;
  end;

implementation

uses
  System.IniFiles,System.Math, System.IOUtils,System.Types,System.StrUtils, uASM;

const
  BUILT_IN_PROCS = [ttWriteLn, ttWrite,ttPoke];

  BUILT_IN_FUNCS = [ttPeek];

  TARGET_TYPE = [
    ttC64,
    ttC128,
    ttBBC,
    ttAtari8Bit,
    ttVic20,
    ttNES,
    ttAppleii,
    ttPET
//    ttAtari400,
//    ttAtari800,
//    ttAtari1200XL,
//    ttAtari5200,
//    ttAtariXEGS,
//    ttAtariXL,
//    ttAtariXE
  ];

  MACHINE_ACORN_TYPES = [ttBBC];
  MACHINE_APPLEII_TYPES = [ttAppleII];
  MACHINE_ATART_8BIT  = [
    ttAtari8Bit
//    ttAtari400,
//    ttAtari800,
//    ttAtari1200XL,
//    ttAtari5200,
//    ttAtariXEGS,
//    ttAtariXL,
//    ttAtariXE
  ];

  PROCS_C64 = [ttWriteLn, ttWrite];

  PROCS_C128 = [ttWriteLn, ttWrite];

  FUNCS_C64 = [];

  PROCS_BBC = [ttWriteLn, ttWrite];

function isPointerType(sym : TSymbol) : Boolean;
begin
  Result := False;

  if sym.symType = nil then Exit;

  if (sym.symType is TCustomTypeSymbol) then
    if TCustomTypeSymbol(sym.symType).isPointerType then begin
      Result := True;
      Exit;
    end;

  Result := (LowerCase(sym.symType.symName) = 'pointer');
end;

function  isSimpleIncrement(node : TAssign; symSize : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttPlus) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value = 1);
    end
    else
    if binOp.right.isVariable and (binOp.left.isInteger) then begin
      Result := (TVariable(binOp.right).value = node.variable.value) and (TNumber(binOp.left).value = 1);
    end;
  end;
end;

function  isSimpleDecrement(node : TAssign; symSize : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttMinus) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value = 1);
    end
    else
    if binOp.right.isVariable and (binOp.left.isInteger) then begin
      Result := (TVariable(binOp.right).value = node.variable.value) and (TNumber(binOp.left).value = 1);
    end;
  end;
end;

constructor TParserAST.Create(aCodeInsightData : TCodeInsightData; aOnScopeUpdate : TOnScopeUpdate);
begin
  FCodeInsightData := aCodeInsightData;

  FonScopeUpdate := aOnScopeUpdate;

  Flines := TStringList.Create;

  Flexer := TLexer.Create;
  Flexer.onLineUpdate := onLineUpdate;

  FcurrentScope := nil;
  FprogAST := nil;

  FloopLevel := 0;
  FconstStringNumber := 0;

  FreplaceConstVar := False;

  FsavedPntrTypes := TList.Create;

  FpointerAddress := MIN_POINTER_ADDRESS;

  FrecordVarInfo := TStringList.Create;
end;

destructor TParserAST.Destroy;
begin
  clearRecVarInfo;

  FrecordVarInfo.Free;
  FsavedPntrTypes.Free;
  Flines.Free;
  Flexer.Free;

  if assigned(FcurrentScope) then
    FreeAndNil(FcurrentScope);
end;

procedure TParserAST.error(aMsg: AnsiString);
begin
  raise TParseException.Create(aMsg + Format(' at line: %d/ col: %d',
    [Flexer.tokenRow, Flexer.tokenCol - 1]));
end;

function TParserAST.accept(S: TTokenType): Boolean;
begin
  Result := False;

  if (Ftoken.tType = S) then
  begin
    Ftoken := Flexer.getNextToken;

    Result := True;
  end
end;

function TParserAST.accept(S: TTokenType; aValue : AnsiString): Boolean;
begin
  Result := False;

  if (Ftoken.tValue = aValue) and (Ftoken.tType = S) then
  begin
    Ftoken := Flexer.getNextToken;

    Result := True;
  end
end;

function TParserAST.expect(S: TTokenType): Boolean;
begin
  Result := accept(S);

  if not Result then
    error('Unexpected symbol: found "' + Ftoken.tValue + '"');
end;

function TParserAST.expect(S: TTokenType; aValue : AnsiString): Boolean;
begin
  Result := accept(S,aValue);

  if not Result then
    error('Unexpected symbol: found "' + Ftoken.tValue + '"');
end;

procedure TParserAST.nextToken;
begin
  Ftoken := Flexer.getNextToken;
end;

procedure TParserAST.savePosition;
begin
  FsavedToken := Ftoken;
  Flexer.savePosition;
end;

procedure TParserAST.restorePosition;
begin
  Flexer.restorePosition;
  Ftoken := FsavedToken;
end;

procedure TParserAST.onLineUpdate(aCurrentLexerLine : Integer);
begin
  if aCurrentLexerLine = FsrcLine then begin
  // get scope info above the current source line
    if Assigned(FcurrentScope) and Assigned(FonScopeUpdate) then
      FonScopeUpdate(FcurrentScope,FcurrentFile);
  end;
end;

// looks for used variable symbols and increases refCount if used in ASM code
procedure TParserAST.stripComments(comment : AnsiString; var aCode : AnsiString);
const
  cLFset = [#13,#10];

var
  c,nl,i : Integer;
begin
  c := pos(comment,aCode);
  while c > 0 do begin
    i := c;
    while (i <= Length(aCode)) and not(aCode[i] in cLFset) do
      Inc(i);

    if i > Length(aCode) then Exit;

    if (aCode[i] in cLFset) then
      // delete comment string section
      Delete(aCode,c,i - c + 1);

    c := pos(comment,aCode);
  end;
end;

procedure TParserAST.processAsmCommand(op,operand : AnsiString);
var
  sym     : TSymbol;
  c       : Integer;
  i       : Integer;
  t       : AnsiString;
  comment : AnsiString;
begin
  if (op = '') or (operand = '') then
    exit
  else
  if LeftStr(op,2) = '//' then
  // is comment
    exit
  else
  if (op[length(op)] = ':') then
    // is label
    exit
  else
  if (op[1] = ':') or (op[1] = '.') then
    // is macro/kick asm command so just emit it!
    exit//Fwriter.writeLn('  '+op + ' ' + operand)
  else begin
    // is normal asm code
    comment := '';
    // get rid of spaces up until // if applicable
    c := Pos('//',operand);

    if c > 0 then begin
      comment := ' '+Copy(operand,c,Length(operand));
      operand := StringReplace(Copy(operand,1,c-1),' ','',[rfReplaceAll])
    end
    else
      operand := StringReplace(operand,' ','',[rfReplaceAll]);;

    operand := StringReplace(operand,'#<','',[rfReplaceAll]);
    if rightStr(operand,2) = '+0' then
    //  change '+0' -> ''
      operand := leftStr(operand,Length(operand)-2);

    operand := StringReplace(operand,'#<','',[rfReplaceAll]);
    operand := StringReplace(operand,'#>','',[rfReplaceAll]);

    operand := StringReplace(operand,'(','',[rfReplaceAll]);
    operand := StringReplace(operand,')','',[rfReplaceAll]);

    if pos(',',operand) > 0 then operand := Copy(operand,1,pos(',',operand)-1);

    if pos(' ',operand) > 0 then operand := Copy(operand,1,pos(' ',operand)-1);

    if leftStr(operand,1) = '$' then Exit;   // is immediate result
    if leftStr(operand,1) = '#' then Exit;   // is immediate result
    if leftStr(operand,1) = '!' then Exit;   // is label
    if leftStr(op,1)      = '!' then Exit;   // is label
    if leftStr(op,1)      = '.' then Exit;   // is kick asm command

    // get rid of + part
    if pos('+',operand) > 0 then operand := Copy(operand,1,pos('+',operand)-1);

    // get rid of - part
    if pos('-',operand) > 0 then operand := Copy(operand,1,pos('-',operand)-1);

    operand := Trim(operand);

//    writeLn(op,': "',operand,'"');

    sym := FcurrentScope.lookup(operand);

    if sym <> nil then begin
      sym.refCount := sym.refCount + 1;
    end;

    // look for starting part of record variable without sub parts "."....
    if pos('.',operand) > 0 then begin
      operand := Copy(operand,1,pos('.',operand)-1);

      sym := FcurrentScope.lookup(operand);

      if sym <> nil then sym.refCount := sym.refCount + 1;
    end;
  end;
end;

procedure TParserAST.checkForVarUsageInAsmCode(line : AnsiString);
var
  i,j,p   : Integer;
  code    : AnsiString;
  lines   : TStringDynArray;
  op      : AnsiString;
  operand : AnsiString;
begin
  line := Trim(line);

  stripComments('//',line);
  if Pos('.for',line) = 0 then begin
    lines := SplitString(line,';');

    for j := 0 to High(lines) do begin
      line := Trim(lines[j]);

      // separate op from operand
      p       := Pos(' ',line);
      op      := Copy(line,1,p - 1);
      operand := Copy(line,p + 1,Length(line));

      processAsmCommand(Trim(op),Trim(operand));
    end;
  end
  else begin
    // separate op from operand
    p       := Pos(' ',line);
    op      := Copy(line,1,p - 1);
    operand := Copy(line,p + 1,Length(line));

    processAsmCommand(Trim(op),Trim(operand));
  end;
end;

function TParserAST.recVarExists(recName,varName : AnsiString; var info : TRecInfo) : Boolean;
var
  i    : Integer;
begin
  Result := False;

  for i := 0 to FrecordVarInfo.Count - 1 do begin
    info := TRecInfo(FrecordVarInfo.Objects[i]);

    if info.fullName = recName+'.'+varName then begin
      Result := True;
      Exit;
    end;
  end;
end;

function  TParserAST.getRecVarInfo(recName : AnsiString) : TRecInfoArray;
// get all var info pertaining to this record name
var
  i    : Integer;
  info : TRecInfo;
begin
  SetLength(Result,0);

  for i := 0 to FrecordVarInfo.Count - 1 do begin
    info := TRecInfo(FrecordVarInfo.Objects[i]);

    if Pos(recName+'.',info.fullName) = 1 then begin
      SetLength(Result,Length(Result)+1);
      Result[High(Result)] := info;
    end;
  end;
end;

procedure TParserAST.addRecVarInfo(recName,varName : AnsiString; varType : TToken; size,memOfs : Integer; isSigned : Boolean);
var
  i        : Integer;
  info     : TRecInfo;
begin
  i := FrecordVarInfo.IndexOf(recName+'.'+varName);

  if i = -1 then begin
    info := TRecInfo.Create;
    info.fullName  := recName+'.'+varName;
    info.varName   := varName;
    info.varType   := varType;
    info.varSize   := size;
    info.varMemOfs := memOfs;
    info.isSigned  := isSigned;

    FrecordVarInfo.AddObject(recName+'.'+varName,info);
  end;
end;

procedure TParserAST.clearRecVarInfo;
var
  i        : Integer;
  infoList : TRecInfo;
begin
  for i := 0 to FrecordVarInfo.Count - 1 do begin
    infoList := TRecInfo(FrecordVarInfo.Objects[i]);
    infoList.Free;
  end;

  FrecordVarInfo.Clear;
end;

function TParserAST.isRecordArrayVar(arrayVar : AnsiString; var sym : TSymbol; var recArrayVar : AnsiString) : Boolean;
//  check for array[].<var> and replace with array_<var>[]
//
var
  token    : TToken;
  varSym   : TSymbol;
  varName  : AnsiString;
  name     : AnsiString;
  variable : TVarSymbol;
begin
  Result := False;

  savePosition;
  try
    if Ftoken.tType <> ttLBracket then Exit;
    // ignore [expression]
    expect(ttLBracket);
    simpleExpression;
    expect(ttRBracket);

    if Ftoken.tType <> ttPeriod then Exit;
    varName := '';

    while accept(ttPeriod) do
    begin
      varName := varName + '.' + Ftoken.tValue;
      expect(ttIdent);
    end;

    name    := arrayVar + '[]' + varName;
    varName := StringReplace(arrayVar + varName,'.','_',[rfReplaceAll]);

    varSym := FcurrentScope.lookup(varName);

    if varSym = nil then
      error('Factor: Unidentified record array var: "'+name+'"');

    sym           := varSym;
    sym.isPrivate := True;
    recArrayVar   := varName;

    name := StringReplace(name,'[]','',[rfReplaceAll]);

    variable := TVarSymbol(VarSymbol(name,FcurrentScope.lookup('byte')));

    Result := True;
  finally
    restorePosition;
  end;

end;

function TParserAST.isCustomTypePointerVar(sym : TSymbol; var isSigned : Boolean; var derefRWsize, derefOffset : Integer; var recName : AnsiString) : Boolean;
var
  token    : TToken;
  varSym   : TSymbol;
  varName  : AnsiString;
  name     : AnsiString;
  variable : TVarSymbol;
  recInfo  : TRecInfo;
begin
  Result := False;

  recName := '';

  if not(sym.symType is TCustomTypeSymbol) then Exit;

  if Ftoken.tType = ttPeriod then begin
  // is record pointer
    varName := '';

    while accept(ttPeriod) do
    begin
      varName := varName + '.' + Ftoken.tValue;
      expect(ttIdent);
    end;

    name := sym.symName + '^' + varName;

    if recVarExists(TCustomTypeSymbol(sym.symType).parentSym.symName,RightStr(varName,Length(varName)-1),recInfo) then begin
      isSigned    := recInfo.isSigned;//varType.tType in [ttShortInt,ttInteger];
      derefRWsize := recInfo.varSize;
      derefOffset := recInfo.varMemOfs;

      recName := TCustomTypeSymbol(sym.symType).parentSym.symName+varName;
    end
    else
      error('Factor: Unidentified record pointer var: "'+name+'"');

  end
  else begin
    isSigned    := TCustomTypeSymbol(sym.symType).parentSym.isSigned;
    derefRWsize := TCustomTypeSymbol(sym.symType).parentSym.size;
    derefOffset := 0;
    recName     := TCustomTypeSymbol(sym.symType).parentSym.symName
  end;

  Result := True;
end;

procedure TParserAST.checkforIndexPlusOffset(var v : TVariable);
// looks for variable[index(8) +-offset]
// if found, convert to variable[index(8)].indexOffset = +-offset
var
  index  : TAST;
  offset : TAST;
  op     : TBinOp;
begin
  if True then

  if not(v.arrayParam is TBinOp) then Exit;

  op := TBinOp(v.arrayParam);
  // only do +- op
  if not(op.op.tType in[ttPlus,ttMinus]) then Exit;

  if op.left.isVariable and op.right.isInteger then begin
    index  := op.left;
    offset := op.right;
  end
  else
  if op.right.isVariable and op.left.isInteger then begin
    index  := op.right;
    offset := op.left;
  end
  else
    Exit;

  if index.size > 1 then Exit;

  v.indexOffset := offset.toString;
  v.arrayParam  := index.clone;
end;

function  TParserAST.factor: TAST;
var
  unaryToken: TToken;
  token: TToken;
  strType : TTokenType;
  node: TAST;
  isUnary: Boolean;
  isVarAddress: Boolean;
  sym: TSymbol;
  funcSym: TFuncSymbol;
  e,x,n: TAST;
  lengthEnabled: Boolean;
  size : Integer;
  isSigned : Boolean;
  varType  : TSymbol;
  isRecArrayVar : Boolean;
  recArrayVar   : AnsiString;

  derefRWsize : Integer;
  derefOffset : Integer;
  isCustomTypePointer : Boolean;

  recName             : AnsiString;
begin
  isUnary := False;
  isVarAddress := False;
  isRecArrayVar := False;

  if (Ftoken.tType = ttAt) then
  begin
    isVarAddress := True;
    nextToken;
  end;

  unaryToken := Ftoken;

  if (unaryToken.tType in [ttPlus, ttMinus]) then
  begin
    if isVarAddress then
      error('Factor: Incompatible with @ operation');

    isUnary := (unaryToken.tType = ttMinus);

    nextToken;
  end;

  token := Ftoken;

  if token.tType in [ttSin,ttCos,ttTan] then begin
    nextToken;

    expect(ttLParen);

// sin(x, n) � returns n*sin(x*PI/128)
// cos(x, n) � returns n*cos(x*PI/128)
// tan(x, n) � returns n*tan(x*PI/128)

    x := simplify(simpleExpression);
    expect(ttComma);
    n := simplify(simpleExpression);

    if not (x.isInteger and n.isInteger) then error('function "'+token.tValue+'()" parameters must resolve to a number');

    expect(ttRParen);

    case token.tType of
      ttSin : node := TNumber.Create(floor(TNumber(n).value * sin((TNumber(x).value * PI) / 128)));
      ttCos : node := TNumber.Create(floor(TNumber(n).value * cos((TNumber(x).value * PI) / 128)));
      ttTan : node := TNumber.Create(floor(TNumber(n).value * tan((TNumber(x).value * PI) / 128)));
    else
      node := TNumber.Create(0);
    end;
  end
  else
  if accept(ttLength) then
  begin
    if isVarAddress then
      error('Factor: Incompatible with @ operation');

    expect(ttLParen);

    lengthEnabled := False;

    token := Ftoken;

    e := simplify(simpleExpression);

    expect(ttRParen);

    if not e.isVariable then
      error('length() only works on a variable/string');

    sym := FcurrentScope.lookup(e.toString);// TVariable(e).token.tValue);

    if (sym is TConstSymbol) then
    begin
      if sym.symType <> nil then
      begin
        lengthEnabled := (sym.symType.symName = 'string') or
          (sym.symType.symName = 'ascii') or (sym.symType.symName = 'petscii');
      end;
      lengthEnabled := lengthEnabled or (TConstSymbol(sym).isArray);
    end
    else if (sym is TVarSymbol) then
    begin
      if sym.symType <> nil then
      begin
        lengthEnabled := (sym.symType.symName = 'string') or
          (sym.symType.symName = 'ascii') or (sym.symType.symName = 'petscii');
      end;
      lengthEnabled := lengthEnabled or (TVarSymbol(sym).isArray);
    end;

    if not lengthEnabled then
      error('length(): incompatable type "' + sym.symType.symName + '" found');

    if TConstSymbol(sym).isArray then begin
      // is array so return actual array length
      if TConstSymbol(sym).symType.size = 1 then
        node := TNumber.Create(TConstSymbol(sym).len)
      else
      // compensate for word array types
        node := TNumber.Create(TConstSymbol(sym).len*2);
    end
    else
      // return the base string variable so location e[0] will be read
      node := TVariable.Create(token, 1);
  end
  else
  if accept(ttAsc) then begin
    expect(ttLParen);
    e := simplify(simpleExpression);
    if not e.isString then error('Asc() only works on a char const right now.');

    expect(ttRParen);
    node := TNumber.Create(cASCIIToPETSCIITable[ord(String(TNumber(e).value)[1])]);
  end
  else
  if accept(ttChr) then begin
    expect(ttLParen);
    e := simplify(simpleExpression);
    if not e.isInteger then error('Chr() only works on a number const right now.');

    expect(ttRParen);
    node := TString.Create(chr(cASCIIToPETSCIITable[Integer(TNumber(e).value)]));
  end
  else if accept(ttTrue) then
  begin
    if isVarAddress then
      error('Factor: Immediate value incompatible with @ operation');

    node := TNumber.CreateBool(True);
  end
  else if accept(ttFalse) then
  begin
    if isVarAddress then
      error('Factor: Immediate value incompatible with @ operation');

    node := TNumber.CreateBool(False);
  end
  else if (Ftoken.tType in [ttString,ttPetscii,ttAscii]) then begin
    strType := Ftoken.tType;

    expect(strType);

    if isVarAddress then
      error('Factor: String incompatible with @ operation');

    node := TString.Create(token);

    TString(node).token.tType := strType;
  end
  else
  if Ftoken.tType in BUILT_IN_FUNCS then
    node := doBuiltInFunc(Ftoken.tType, Ftoken.tValue)
  else if accept(ttIdent) then
  begin
    while accept(ttPeriod) do
    begin
      token.tValue := token.tValue + '.' + Ftoken.tValue;
      expect(ttIdent);
    end;

    if LowerCase(token.tValue) = 'result' then
      token.tValue := LowerCase(token.tValue);

    sym := FcurrentScope.lookup(Ftoken.tValue);

    if FreplaceConstVar and (token.tValue = FconstVarName) then
      // replace this variable with the value (const for-loop variables)
      node := TNumber.Create(FconstVarValue)
    else begin
      sym := FcurrentScope.lookup(token.tValue);

      if (sym = nil) then begin
        isRecArrayVar := isRecordArrayVar(token.tValue,sym,recArrayVar);

        if not isRecArrayVar then
          error('Factor: Undeclared identifier "' + token.tValue + '"');

        token.tValue := recArrayVar;
      end;

  //    if TVarSymbol(sym).isFuncResult then
  //    begin
  //      // is a function result so process accordingly
  //      // return a
  //
  //      node := TVariable.Create('result', TVarSymbol(sym).size);
  //      TVariable(node).isFuncResult := True;
  //
  //      Result := node;
  //      Exit;
  //    end;

      if (sym.symClass = scFunc) then begin
        if isVarAddress then
          error('Factor: Function incompatible with @ operation');

        // is a function call
        node := doFuncCall(sym.symName);
      end
      else
      if (sym.symClass = scConst) then begin
        if (TConstSymbol(sym).symType = nil) then
        begin
          // is normal const, so substitute const value
          case TConstSymbol(sym).value.tType of
            ttIntNum:
              node := TNumber.Create(TConstSymbol(sym).value);
            ttString,ttPetscii,ttascii:
              node := TString.Create(TConstSymbol(sym).value);
          end;
        end
        else
        begin
          // is typed const, treat as normal variable or pointer/array
          node := TVariable.Create(token, sym.size,sym.isSigned);

          if accept(ttLBracket) then begin
            if isVarAddress then
              error('Factor: Array incompatible with @ operation');

            TVariable(node).arrayParam := simplify(simpleExpression);
            expect(ttRBracket);

            checkforIndexPlusOffset(TVariable(node));

            TVariable(node).use8BitValue := (TVariable(node).arraySize * TVariable(node).size) <= 256;

//            if sym.uses8BitIndex then TVariable(node).arrayParam.sizeOverride := 1;
          end;
        end;
      end
      else
      begin
        // normal variable or pointer/array
        sym := FcurrentScope.lookup(token.tValue);

        if sym.symType <> nil then
          node := TVariable.Create(token, sym.size,sym.symType.isSigned)
        else
          node := TVariable.Create(token, sym.size,sym.isSigned);

        TVariable(node).onStack  := sym.onStack;
        TVariable(node).stackOfs := sym.stackOfs;

        if accept(ttLBracket) then begin
          if isVarAddress then
            error('Factor: Array incompatible with @ operation');

          if LowerCase(sym.symType.symName) = 'pointer' then
            error('factor: Pointer var "'+token.tValue+'" has to be deref''d to use an array index with it');

          TVariable(node).arrayParam := simplify(simpleExpression);
//          if sym.uses8BitIndex then TVariable(node).arrayParam.sizeOverride := 1;

          expect(ttRBracket);

          if isRecArrayVar then begin
          // get rid of .var... stuff after ']'
            while accept(ttPeriod) do
            begin
              expect(ttIdent);
            end;

            sym.isPrivate := True;
          end;

          checkforIndexPlusOffset(TVariable(node));

          if TVariable(node).address = '' then begin
            TVariable(node).use8BitValue := (TVariable(node).arraySize * TVariable(node).size) <= 256;

            if (sym.symType is TRecordSymbol) then begin
              sym.isRecArray := True;
//              sym
            end;
          end;
        end
        else if accept(ttCaret) then begin
          if isVarAddress then
            error('Factor: Pointer incompatible with @ operation');

          if not isPointerType(sym) then
            error('Can only deref a pointer type variable');

          isCustomTypePointer := isCustomTypePointerVar(sym,isSigned,derefRWsize,derefOffset,recName);

          if isCustomTypePointer then begin
//            sym.size                    := derefRWsize;
            sym.varSize                 := derefRWsize;
            sym.isSigned                := isSigned;
            TVariable(node).isSigned    := isSigned;
            TVariable(node).arrayParam  := TNumber.Create(derefOffset);
            TVariable(node).derefRWsize := derefRWsize;
            TVariable(node).size        := derefRWsize;
            TVariable(node).recName     := recName;
            node.isRecordVar            := True;
//            TVariable(node).derefOffset := derefOffset;
          end;

          if isCustomTypePointer and (Ftoken.tType = ttLBracket) then
            error('Can only use ^[] with a plain pointer type');

          if accept(ttLBracket) then begin
            if LowerCase(sym.symType.symName) <> 'pointer' then
              error('Can only use deref''d pointer type variable as an array');

            TVariable(node).arrayParam := simplify(simpleExpression);
            TVariable(node).use8BitValue := True;
//            if (TVariable(node).arrayParam.size > 1) or
//              not(TVariable(node).arrayParam.isLeaf) or
//              not(TVariable(node).arrayParam.isInteger or TVariable(node)
//              .arrayParam.isVariable) then
//              error('Deref pointer: array index must be size of 1 and be a number/variable');

            expect(ttRBracket);
          end;

          TVariable(node).deref := True;
          TVariable(node).sym   := sym;
          TVariable(node).use8BitValue := (TVariable(node).arraySize * TVariable(node).size) <= 256;

          if sym.symType is TCustomTypeSymbol  then begin
//            WriteLn(sym.symName+'('+sym.symType.symName+':'+IntToStr(sym.symType.size)+')');
          end;
        end;
      end;
    end;
  end
  else
  if accept(ttWord) or accept(ttByte) or accept(ttInteger) or accept(ttShortInt) or accept(ttInt24) then begin
    // do typecast

    expect(ttLParen);
    e := Simplify(expression);

    if not (e.isInteger or e.isVariable or (e is TUnaryOp) or (e is TBinOp)) then
      error('Factor: can only typecast a number/variable/expression, "'+e.toString+'" found');

    expect(ttRParen);

    if isVarAddress then
      error('Factor: "' + e.toString + '" incompatible with @ operation');

    node := newCast(Token,e);

    if e.isInteger then begin
      size     := TCastOp(node).newSize;
      isSigned := TCastOp(node).isSigned;
      node.Free;

      node := e;
      node.sizeOverride := size;
      node.size         := size;
      node.isSigned     := isSigned;
    end;

  end
  else if (accept(ttIntNum)) then begin
    if isVarAddress then
      error('Factor: "' + token.tValue + '" incompatible with @ operation');

    node := TNumber.Create(token);
  end
  else if (accept(ttLParen)) then begin
    if isVarAddress then
      error('Factor: Immediate value incompatible with @ operation');

    node := simplify(expression());
    expect(ttRParen);
  end
  else begin
    error('factor: syntax error; found "' + Ftoken.tValue + '" ');
    nextToken();
  end;

  if isUnary then
    Result := simplify(TUnaryOp.Create(uToken.token(ttNeg, 'neg'), node))
  else
    Result := node;

  if isVarAddress then
  begin
    // writeLn('Pointer to "'+TVariable(node).token.tValue+'"');
    TVariable(node).isVarAddress := True;
  end;

  if isVarAddress then begin
    if (sym.symClass = scProc) then begin
      if TProcSymbol(sym).procType <> ttAssembler then begin
        TVariable(node).token.tValue := TVariable(node).token.tValue + '._main_';
        TVariable(node).value := token.tValue;
        routines_setAsUsed(TVariable(node).token.tValue,FcurrentScope);

      end
    else
    if (sym.symClass in [scVar,scConst]) then
      TVariable(node).value := token.tValue;
    end;
  end;

  if node.isVariable then begin
    sym := FcurrentScope.lookup(TVariable(node).token.tValue);

    if sym <> nil then begin
      TVariable(node).sym := sym;

      sym.refCount := sym.refCount + 1;

      if TConstSymbol(sym).isBinaryFile then
        TVariable(node).use8BitValue := TConstSymbol(sym).uses8BitIndex;
    end;
  end;
end;

function TParserAST.signedFactor: TAST;
var
  token: TToken;
begin
  token := Ftoken;

  if accept(ttNot) then
    Result := simplify(TUnaryOp.Create(token, factor))
  else
    Result := factor;

//  writeLn(Result.ClassName,': ',Result.toString);
end;

function TParserAST.term: TAST;
var
  token : TToken;
  node  : TAST;
  right : TAST;
  left  : TAST;
  peek  : TAST;
begin
  node := signedFactor;
  token := Ftoken;

  while (Ftoken.tType in [ttTimes, ttSlash, ttDiv, ttMod, ttAnd, ttShl,
    ttShr]) do
  begin
    token := Ftoken;
    nextToken();

    right := signedFactor;
    left  := node;

    node := simplify(TBinOp.Create(node, token, right));

    if (left is TPeek) and (TBinOp(node).op.tType = ttTimes) and (right.toString = '256') then begin
//      node := TUnaryOp.Create(uToken.Token(ttMul256,'*'),node);

      TBinOp(node).op    := uToken.Token(ttShl,'<<');
      TBinOp(node).right := TNumber.Create(8);
    end;
  end;

  Result := node;
end;

function TParserAST.simpleExpression: TAST;
var
  token: TToken;
  node: TAST;
  right: TAST;
begin
  node := term;
  token := Ftoken;

  while (Ftoken.tType in [ttPlus, ttMinus, ttOr, ttXor]) do
  begin
    token := Ftoken;
    nextToken();

    right := term;

    node := simplify(TBinOp.Create(node, token, right));
  end;

  Result := node;

//  writeLn(node.ClassName,': ',node.toString);
end;

function isRelop(const aTokenType: TTokenType): Boolean;
begin
  Result := aTokenType in [ttEql, ttNeq, ttLss, ttLeq, ttGtr, ttGeq];
end;

function TParserAST.expression: TAST;
var
  token: TToken;
  node: TAST;
begin
  node := simplify(simpleExpression);

  if isRelop(Ftoken.tType) then
  begin
    token := Ftoken;
    nextToken;

    node := simplify(TBinOp.Create(node, token, simplify(simpleExpression)));
  end;

  Result := simplify(node);
end;

function TParserAST.doPoke: TPoke;
begin
  Result := TPoke.Create;

  expect(ttLParen);
  Result.address := simplify(simpleExpression);
  expect(ttComma);
  Result.expr := simplify(simpleExpression);
  expect(ttRParen);
end;

function TParserAST.doIf: TIfThen;
var
  sourceLine : Integer;
begin
  sourceLine := Flexer.tokenRow;

  Result := TIfThen.Create;

  Result.condition := simplify(expression);
  Result.condition.sourceLine := sourceLine;
  Result.condition.sourceFile := FcurrentFileFull;

  expect(ttThen);

  Result.trueBranch := statement;
  Result.elseBranch := nil;

  if accept(ttElse) then
  begin
    Result.elseBranch := statement;
  end;

  Result.sourceLine := sourceLine;
  Result.sourceFile := FcurrentFileFull;
end;

function TParserAST.doWhile: TWhile;
var
  sourceLine : Integer;
begin
  sourceLine := Flexer.tokenRow;

  Result := TWhile.Create;

  Result.condition := simplify(expression);
  Result.condition.sourceLine := sourceLine;
  Result.condition.sourceFile := FcurrentFileFull;

  expect(ttDo);

  Inc(FloopLevel);
  Result.statement := statement;
  Dec(FloopLevel);

  Result.sourceLine := sourceLine;
  Result.sourceFile := FcurrentFileFull;
end;

function TParserAST.doRepeat: TRepeat;
var
  sourceLine : Integer;
begin
  sourceLine := Flexer.tokenRow;

  Result := TRepeat.Create;

  Inc(FloopLevel);

  repeat
    if (Ftoken.tType = ttUntil) then
      break;

    Result.children.Add(statement);

  until not(accept(ttSemiColon));

  expect(ttUntil);

  Result.condition := simplify(expression);
  Result.condition.sourceLine := sourceLine;
  Result.condition.sourceFile := FcurrentFileFull;

  Dec(FloopLevel);

  Result.sourceLine := sourceLine;
  Result.sourceFile := FcurrentFileFull;
end;

function TParserAST.doFor: TFor;
var
  variable: TAST;
  startValue: TAST;
  endValue: TAST;
  step: TNumber;
  incrementor: TBinOp;
  sourceLine : Integer;
  cast       : TCastOp;
begin
  sourceLine := Flexer.tokenRow;

  // for variable := startValue to/downto endValue do
  Result := TFor.Create;
  Result.is0to255loop := False;
  Result.isNdownto0loop := False;

  variable := simplify(simpleExpression);

  if (not variable.isVariable) then
    error('For loop: initializer must assign to a variable');

  variable.isLoopVar := True;

  expect(ttBecomes);

  startValue := simplify(simpleExpression);
  if startValue.isInteger then
    TNumber(startValue).isSigned := TVariable(variable).isSigned;

  Result.initializer := TAssign.Create(variable, startValue);
  Result.initializer.sourceLine := sourceLine;
  Result.initializer.sourceFile := FcurrentFileFull;
  Result.initializer.decimalModeOn := FdecimalModeOn;

  if accept(ttTo) then
    step := TNumber.Create(1)
  else
  begin
    expect(ttDownTo);
    step := TNumber.Create(-1);
  end;

  // to/downto value
  endValue := simplify(simpleExpression);
  if endValue.isInteger then
    TNumber(endValue).isSigned := TVariable(variable).isSigned;

  expect(ttDo);

  Inc(FloopLevel);
  Result.statement := statement;
  Result.statement.sourceLine := sourceLine;
  Result.statement.sourceFile := FcurrentFileFull;
  Dec(FloopLevel);

  if (step.value = 1) then
    Result.condition := TBinOp.Create(variable, token(ttLeq, '<='), endValue)
  else
    Result.condition := TBinOp.Create(variable, token(ttGeq, '>='), endValue);

  incrementor := TBinOp.Create(variable, token(ttPlus, '+'), step);
  incrementor.sourceLine := sourceLine;
  incrementor.sourceFile := FcurrentFileFull;

  Result.incrementer := TAssign.Create(variable, incrementor);

  Result.sourceLine := sourceLine;
  Result.incrementer.sourceLine := sourceLine;
  Result.incrementer.sourceFile := FcurrentFileFull;
  Result.incrementer.decimalModeOn := FdecimalModeOn;
  Result.condition.sourceLine := sourceLine;
  Result.condition.sourceFile := FcurrentFileFull;

  if (startValue is TNumber) and (endValue is TNumber) then
    if (TNumber(startValue).value = 0) and (TNumber(endValue).value = 255) then
      if (startValue.size = 1) and (endValue.size = 1) and (variable.size = 1) then
        Result.is0to255loop := True;

  if (step.value = -1) and (startValue.size = 1) and (endValue.size = 1) then begin
    if (startValue is TNumber) and (endValue is TNumber) and (variable.size = 1) then
      if TNumber(endValue).value = 0 then
        Result.isNdownto0loop := True;
  end;

  if (TBinOp(Result.condition).right.size <> variable.size) or
     (TBinOp(Result.condition).right.isSigned <> variable.isSigned) then begin

    cast := newCast(TBinOp(Result.condition).right,variable.size,variable.isSigned);

    TBinOp(Result.condition).right := cast;
  end;
end;

function TParserAST.doBuiltInProc(aType: TTokenType; aName: AnsiString)
  : TBuiltinProcedure;
var
  paramCount: Integer;
  count: Integer;
begin
  expect(aType);

  if FprogAST.target = ttC64 then begin
    if (aType in PROCS_C64) and (FprogAST.target <> ttC64) then
      error('To use procedure "' + aName + '" include "c64" in uses clause');
  end
  else
  if FprogAST.target = ttC128 then begin
    if (aType in PROCS_C128) and (FprogAST.target <> ttC128) then
      error('To use procedure "' + aName + '" include "c128" in uses clause');
  end;

  case aType of
    ttWriteLn:
      paramCount := -1;
    ttWrite:
      paramCount := -1;
  else
    paramCount := 0;
  end;

  Result := TBuiltinProcedure.Create(aType);
  Result.procName := aName;

  count := 0;

  if accept(ttLParen) then
  begin
    if (Ftoken.tType <> ttRParen) then
    begin
      Result.params.Add(simplify(expression));
      Inc(count);

      while accept(ttComma) do
      begin
        Result.params.Add(simplify(expression));
        Inc(count);
      end;
    end;
    expect(ttRParen);
  end;

  if (paramCount <> -1) then
    if (count <> paramCount) then
      error(Format('Procedure "%s" expected %d params, found %d params',
        [aName, paramCount, count]));
end;

function TParserAST.doBuiltInFunc(aType: TTokenType; aName: AnsiString)
  : TBuiltinFunction;
var
  paramCount : Integer;
  count      : Integer;
  dstVar     : TVariable;
begin
  expect(aType);

  if (aType in FUNCS_C64) and (FprogAST.target <> ttC64) then
    error('To use function "' + aName + '" include "c64" in uses clause');

  case aType of
    ttPeek: begin
      paramCount := 1;
      Result := TPeek.Create(aType);
    end
  else
    error('internal error: unknown built in function;'+aName);
  end;

  Result.funcName := aName;

  count := 0;

  if accept(ttLParen) then
  begin
    if (Ftoken.tType <> ttRParen) then
    begin
      Result.params.Add(simplify(expression));
      Inc(count);

      while accept(ttComma) do
      begin
        Result.params.Add(simplify(expression));
        Inc(count);
      end;
    end;
    expect(ttRParen);
  end;

  if (paramCount <> -1) then
    if (count <> paramCount) then
      error(Format('Function "%s" expected %d params, found %d params',
        [aName, paramCount, count]));
end;

function TParserAST.doAssign(aVariable: TVariable): TAST;
var
  sym        : TSymbol;
  arrayParam : TAST;
  sourceLine : Integer;
  c          : Integer;
begin
  sourceLine := Flexer.tokenRow;

  sym := FcurrentScope.lookup(aVariable.token.tValue);

  if (sym.symClass = scConst) then
    error('Cannot assign to left-hand-side!');

  if sym is TConstSymbol then
    error('Cannot assign to left-hand-side!');

  if accept(ttCaret) then
  begin
    if not isPointerType(sym.symType) then //( LowerCase(sym.symType.symName) <> 'pointer' then
      error('Can only deref pointer types');

    aVariable.deref := True;
  end;

  expect(ttBecomes);

  Result := TAssign.Create(aVariable, simplify(expression));
  Result.sourceLine := sourceLine;
  Result.sourceFile := FcurrentFileFull;
  TAssign(Result).decimalModeOn := FdecimalModeOn;

  if not isPointerType(sym) and TAssign(Result).expr.isString then
    if TString(TAssign(Result).expr).size = 1 then begin
      // change the string to a number equal to the character
      // default to screen code
      c := cPETSCIIToCBMScreenCode[cASCIIToPETSCIITable[ord(TString(TAssign(Result).expr).token.tValue[1])]];

      if TString(TAssign(Result).expr).token.tType = ttAscii then begin
        c := Ord(TString(TAssign(Result).expr).token.tValue[1]);
      end
      else
      if TString(TAssign(Result).expr).token.tType = ttPetscii then begin
        c := cASCIIToPETSCIITable[ord(TString(TAssign(Result).expr).token.tValue[1])];
      end;

      TAssign(Result).expr := TNumber.Create(c);
    end;
end;

function TParserAST.doDecInc(decIncToken : TToken) : TAST;
var
  assignVar  : TAST;
  expr       : TAST;
  sourceLine : Integer;
begin
  sourceLine := Flexer.tokenRow;

  expect(ttLParen);

  assignVar := simplify(simpleExpression);

  if not assignVar.isVariable then
    error('Illegal left-hand assignment expression in dec/inc call');

  if accept(ttComma) then begin
  // has optional, overriding +- value
    expr := simplify(simpleExpression);
  end
  else
  // default to 1 as altering value
    expr := TNumber.Create(Token(ttInteger,1));

  case decIncToken.tType of
    ttDec : Result := TAssign.Create(assignVar,TBinOp.Create(assignVar,Token(ttMinus,'-'),expr));
    ttInc : Result := TAssign.Create(assignVar,TBinOp.Create(assignVar,Token(ttPlus,'+'),expr));
  end;

  expect(ttRParen);

  Result.sourceLine := sourceLine;
  Result.sourceFile := FcurrentFileFull;
  TAssign(Result).decimalModeOn := FdecimalModeOn;
end;

function  TParserAST.getConstStringName : AnsiString;
begin
  Result := '__CONST_STRING__' + IntToStr(FconstStringNumber);

  inc(FconstStringNumber);
end;

procedure TParserAST.processStringParam(var param : TAST);
var
  s             : TString;
  constName     : AnsiString;
  constType     : TSymbol;
  constTypeName : TToken;
  constValue    : TToken;
  sym           : TSymbol;
begin
  if not (param.isString) then Exit;

  s := TString(param);

  constName     := getConstStringName;

  constValue    := Token(ttString,s.value);
  constTypeName := Token(ttString,'string');

  constType := FcurrentScope.lookup(constTypeName.tValue);

  // insert this new const string into the symbol table
  sym := TConstSymbol.Create(constName, constValue,constType);
  FcurrentScope.insert(sym);

  // add this const decl to the block's list
  FglobalBlock.declarations.Add(TConstDecl.Create(constName, constValue,
    constType));

  param := TVariable.Create(constName,s.size,false);
  TVariable(param).isVarAddress := True;
end;

procedure TParserAST.getCallParams(params : TASTList; var paramCount : Integer);
var
  param : TAST;
begin
  // at least one param passed
  param := simplify(expression);

//      writeLn(param.toString);

  processStringParam(param);

  Inc(paramCount);

  params.Add(param);

  // do others while a comma is found
  while accept(ttComma) do
  begin
    param := simplify(expression);

    processStringParam(param);

    Inc(paramCount);

    params.Add(param);
  end;
end;

function TParserAST.doCall(aProcName: AnsiString): TAST;
var
  sym: TProcSymbol;
  param: TAST;
  paramCount: Integer;
begin
  Result := TCall.Create;

  routines_setAsUsed(aProcName,FcurrentScope);

  TCall(Result).procName := aProcName;

  sym := TProcSymbol(FcurrentScope.lookup(aProcName));

  if (sym = nil) then
    error('call: Undeclared procedure name "' + aProcName + '" found');

  sym.refCount := sym.refCount + 1;

  paramCount := 0;

  if accept(ttLParen) then
  begin
    if (Ftoken.tType <> ttRParen) then
      getCallParams(TCall(Result).params,paramCount);
    expect(ttRParen);
  end;

  if (paramCount <> sym.paramCount) then
    error('Procedure call: Parameter count mismatch;  found ' + IntToStr(paramCount) +
      ', expected ' + IntToStr(sym.paramCount));
end;

function TParserAST.doMacroCall(aMacroName: AnsiString): TAST;
var
  sym: TMacroSymbol;
  param: TAST;
  paramCount: Integer;
begin
  Result := TMacroCall.Create;

  TMacroCall(Result).macroName := aMacroName;

  sym := TMacroSymbol(FcurrentScope.lookup(aMacroName));

  if (sym = nil) then
    error('Macro call: Undeclared macro name "' + aMacroName + '" found');

  if sym.symClass <> scMacro then
    error('Macro call: "' + aMacroName + '" is not a macro');

  paramCount := 0;

  if accept(ttLParen) then
  begin
    if (Ftoken.tType <> ttRParen) then
    begin
      // at least one param passed
      param := simplify(expression);

      if not(param.isVariable or param.isInteger or param.isString) then
        error('Macro parameters can only be a variable, integer or string');

      Inc(paramCount);

      if param.isInteger then
        TMacroCall(Result).params.Add(TNumber(param).token.tValue)
      else if param.isVariable then
        TMacroCall(Result).params.Add(TVariable(param).token.tValue)
      else
        TMacroCall(Result).params.Add('"' + TString(param).token.tValue + '"');

      // do others while a comma is found
      while accept(ttComma) do
      begin
        param := simplify(expression);

        if not(param.isVariable or param.isInteger or param.isString) then
          error('Macro parameters can only be a variable, integer or string');

        Inc(paramCount);

        if param.isInteger then
          TMacroCall(Result).params.Add(TNumber(param).token.tValue)
        else if param.isVariable then
          TMacroCall(Result).params.Add(TVariable(param).token.tValue)
        else
          TMacroCall(Result).params.Add('"' + TString(param)
            .token.tValue + '"');
      end;
    end;
    expect(ttRParen);
  end;

  if sym.paramCount <> paramCount then
    error(Format('Macro call: found %d parameters, expected %d',
      [paramCount, sym.paramCount]));
end;

function TParserAST.doFuncCall(aFuncName: AnsiString): TAST;
var
  sym: TFuncSymbol;
  param: TAST;
  paramCount: Integer;
begin
  Result := TFuncCall.Create;

  routines_setAsUsed(aFuncName,FcurrentScope);

  uASM.enterFunction;

  TFuncCall(Result).funcName := aFuncName;

  sym := TFuncSymbol(FcurrentScope.lookup(aFuncName));

  if (sym = nil) then
    error('call: Undeclared function name "' + aFuncName + '" found');

  TFuncCall(Result).funcType  := sym.funcType;
  TFuncCall(Result).size      := sym.symType.size;
  TFuncCall(Result).onStack   := sym.onStack;
  TFuncCall(Result).stackSize := sym.stackSize;

  sym.refCount := sym.refCount + 1;

  paramCount := 0;

  if accept(ttLParen) then
  begin
    if (Ftoken.tType <> ttRParen) then
      getCallParams(TFuncCall(Result).params,paramCount);
    expect(ttRParen);
  end;

  if (paramCount <> sym.paramCount) then
    error('function call: Parameter count mismatch;  found ' + IntToStr(paramCount) +
      ', expected ' + IntToStr(sym.paramCount));

  uASM.exitFunction;
end;

function TParserAST.statement: TAST;
var
  token      : TToken;
  assignVar  : TAST;
  expr       : TAST;
  incDecExpr : TAST;
  sym        : TSymbol;
begin
  token := Ftoken;

  if accept (ttdecimalModeOn) then begin
    Result := TDecimalMode.Create(true);
    FdecimalModeOn := True;
  end
  else
  if accept(ttDecimalModeOff) then begin
    Result := TDecimalMode.Create(false);
    FdecimalModeOn := false;
  end
  else
  if (token.tType = ttIdent) then
  begin
    assignVar := simplify(simpleExpression);

    if not (assignVar is TVariable) then error('statement: expected an assignment variable, procedure call, or macro call.');

    if (Ftoken.tType in [ttSemiColon, ttLParen]) then
    begin
      // found a ';' or '(' so must be a "call" or macro "call" statement...
      sym := FcurrentScope.lookup(TVariable(assignVar).token.tValue);

      if sym = nil then
        error('Undeclared procedure/macro name "' + TVariable(assignVar)
          .token.tValue + '"');

      if sym.symClass = scFunc then
        error('Function "' + TVariable(assignVar).token.tValue +
          '" must be part of an assignment statement');

      if sym.symClass = scProc then
        Result := doCall(TVariable(assignVar).token.tValue)
      else
        Result := doMacroCall(TVariable(assignVar).token.tValue);
    end
    else
    begin
      if not assignVar.isVariable then
        error('Illegal left-hand assignment expression or procedure call name');

      // not a call, so must be an assignment
      Result := doAssign(TVariable(assignVar));
    end;
  end
  else if accept(ttExit) then
  begin
    Result := TExit.Create;
  end
  else if accept(ttBreak) then
  begin
    if FloopLevel = 0 then error('Must be inside a loop to break out of!');

    Result := TBreak.Create;
  end
  else if (Ftoken.tType = ttBegin) then
  begin
    Result := compound;
  end
  else if (Ftoken.tType = ttAsm) then
  begin
    Result := asmCompound;
  end
  else if accept(ttDec) or accept(ttInc) then begin
  // is a inc/dec command so process that
    Result := doDecInc(token);
  end
  else if accept(ttPoke) then
    Result := doPoke
  else if (accept(ttIf)) then
    Result := doIf
  else if (accept(ttWhile)) then
    Result := doWhile
  else if (accept(ttRepeat)) then
    Result := doRepeat
  else if (accept(ttFor)) then
    Result := doFor
    // else if accept(ttWriteLn) then
    // Result := doWriteLn
  else if (Ftoken.tType in BUILT_IN_PROCS) then
    Result := doBuiltInProc(Ftoken.tType, Ftoken.tValue)
//  else if accept(ttSetupRasterIRQ) then
//  begin
//    Result := doSetupRasterIRQ;
//  end
//  else if accept(ttEndIRQ) then
//  begin
//    Result := doEndIRQ;
//  end
  else if (Ftoken.tType = ttSemiColon) then
    // no statement here so return empty one!
    Result := TEmpty.Create
  else
  begin
    error('statement: syntax error; found "' + Ftoken.tValue + '"');
    nextToken();
  end;
end;

function TParserAST.compound: TCompound;
begin
  expect(ttBegin);

  Result := TCompound.Create;

  repeat
    if (Ftoken.tType = ttEnd) then
      break;

    Result.children.Add(statement);

  until not(accept(ttSemiColon));

  expect(ttEnd);
end;

function TParserAST.asmCompound: TAsmCompound;
var
  i: Integer;
  asmStart: Integer;
  asmEnd: Integer;
begin
  Result := TAsmCompound.Create;
  expect(ttAsm);
  asmStart := Flexer.tokenRow;

  while (Ftoken.tType <> ttEOF) and (Ftoken.tType <> ttEnd) do
  begin
    nextToken;
  end;

  if (Ftoken.tType = ttEnd) then
    asmEnd := Flexer.tokenRow - 1;

  nextToken;

  for i := asmStart to asmEnd do
    Result.asmStatements.Add(Flines.Strings[i - 1]);
end;

function TParserAST.getPointerAddress: Byte;
begin
  if (FpointerAddress > MAX_POINTER_ADDRESS) then
    error('Exceeded max pointer address $"' + IntToHex(MIN_POINTER_ADDRESS,
      2) + '"');

  Result := FpointerAddress;
  // inc twice due to 2 byte pointers
  Inc(FpointerAddress,2);
end;

procedure appendStr(var aList: TStringArray; aVal: AnsiString);
var
  i: Integer;
begin
  i := Length(aList);

  SetLength(aList, i + 1);
  aList[i] := aVal;
end;

procedure appendSrcInfo(var aList: TSourceInfoArray; offset : Integer; fileName : String);
var
  i: Integer;
begin
  i := Length(aList);

  SetLength(aList, i + 1);
  aList[i].offset   := offset;
  aList[i].fileName := fileName;
end;

procedure appendNumber(var aList: TNumberArray; aVal: TNumber);
var
  i: Integer;
begin
  i := Length(aList);

  SetLength(aList, i + 1);
  aList[i] := aVal;
end;

function  TParserAST.parseKickAsmData : AnsiString;
var
  i: Integer;
  asmStart: Integer;
  asmEnd: Integer;
begin
  Result := '';

  expect(ttLKickAsm);
  asmStart := Flexer.tokenRow;

  while (Ftoken.tType <> ttEOF) and (Ftoken.tType <> ttRKickAsm) do
  begin
    nextToken;
  end;

  if Ftoken.tType <> ttRKickAsm then
    error('KickAsm data: expected "}}", found end of file!');

  if (Ftoken.tType = ttRKickAsm) then
    asmEnd := Flexer.tokenRow;

  expect(ttRKickAsm);

//  Flexer.advanceUntil2('}','}');

//  while (Ftoken.tType <> ttEOF) and (Ftoken.tType <> ttRKickAsm) do
//  begin
//    nextToken;
//  end;

//  if (Ftoken.tType <> ttEOF) then
//    asmEnd := Flexer.tokenRow;// - 1;

//  asmEnd := Flexer.row - 1;
//  expect(ttRKickAsm);
//
  for i := asmStart to asmEnd do
    Result := Result + Flines.Strings[i - 1];
end;

procedure TParserAST.segmentDecl(aBlock: TBlock);
var
  segment : TSegmentDecl;
  e       : TAST;
begin
  if (FcurrentScope.scopeLevel > 1) then
    error('#'+cSEGMENT+' can only be used at the global scope level');

  expect(ttLParen);
  e := Simplify(simpleExpression);
  expect(ttRParen);
  expect(ttSemiColon);

  if not e.isString then error('#'+cSEGMENT+' param must be a string!');

  segment := TSegmentDecl.Create;
  segment.segmentName := e.toString;

//  writeLn(aBlock.blockName,': ',segment.segmentName);
  aBlock.declarations.Add(segment);
end;

procedure TParserAST.clearSegments;
var
  i : Integer;
begin
  if (FcurrentScope.scopeLevel > 1) then
    error('#'+cCLEARSEGMENTS+' can only be used at the global scope level');

  expect(ttSemiColon);

  for i := 0 to FprogAST.segments.Count - 1 do
    TSegmentInfo(FprogAST.segments.Items[i]).Free;

  FprogAST.segments.Clear;
end;

procedure TParserAST.addSegment;
var
  seg   : TSegmentInfo;
  name  : AnsiString;
  start : AnsiString;
  min   : AnsiString;
  max   : AnsiString;
  e     : TAST;

  function getInteger(name : AnsiString) : AnsiString;
  begin
    e := Simplify(simpleExpression);
    if not e.isInteger then
      error(cADDSEGMENT+': "'+name+'" param must be an integer');
    Result := TNumber(e).token.tValue;
  end;
begin
  if (FcurrentScope.scopeLevel > 1) then
    error('#'+cADDSEGMENT+' can only be used at the global scope level');

  expect(ttLParen);
  e := Simplify(simpleExpression);

  if not e.isString then error('#'+cADDSEGMENT+' name param must be a string!');

  name := e.toString;

  expect(ttComma);
  start := getInteger('min');
  min   := start;
  expect(ttComma);
  max   := getInteger('max');

  expect(ttRParen);
  expect(ttSemiColon);

  seg := TSegmentInfo.Create;
  seg.name := name;
  seg.start := start;
  seg.min   := min;
  seg.max   := max;

  FprogAST.segments.Add(seg);
end;

procedure TParserAST.doTypedConstDecl_array(aConstName: AnsiString;
  aBlock: TBlock; offset : Integer; fileName : String);
var
  constType         : TSymbol;
  constTypeName     : TToken;
  e                 : TAST;
  count             : Integer;
  loRange           : Integer;
  hiRange           : Integer;
  sym               : TSymbol;
  values            : TNumberArray;
  constSym          : TConstSymbol;
  forVar            : AnsiString;
  i                 : Integer;
  isMsb             : Boolean;
  absoluteArrayData : TAbsoluteArrayData;
  isKickAsmData     : Boolean;

  function getValue: TAST;
  begin
    Result := simplify(simpleExpression);

    sym := FcurrentScope.lookup(Result.toString);

    if not (Result.isInteger or (Result.isVariable))  then
      error('Const decl; array values must be numeric/an address');

    if Result.size > constType.size then
      error('Const decl; array value too large');
  end;

begin
  if accept(ttOf) then begin
  // is array defined by its data values only ; values or for loop
  // a : array of Byte = (1,2,3);
  // a : array of Byte = for i := 0 to 30 do (i);

    loRange := 0;

    constTypeName := Ftoken;

    if not(constTypeName.tType in [ttByte,ttInteger,ttShortInt,ttWord,ttInt24]) then
      error('Const decl; array types must be numeric, found type "' +
        constTypeName.tValue + '"');

    constType := FcurrentScope.lookup(constTypeName.tValue);

    expect(constTypeName.tType);

    expect(ttEql);

    isKickAsmData := False;

    // default to lsb values
    isMsb := False;

    if accept(ttKickAsm) then isKickAsmData := True;

    if not isKickAsmData then begin
      if accept(ttGtr) then begin
        // is MSB value list
        isMsb := True;
      end
      else
      if accept(ttLss) then begin
        // is LSB value list
        isMsb := False;
      end;
    end;

    if isKickAsmData then begin
      constSym := TConstSymbol.Create(aConstName, constType,constType.size);
      constSym.isArray := True;
      constSym.len  := 1;
      constSym.isKickAsmData := True;
      constSym.setSourceInfo(offset,fileName);

      FcurrentScope.insert(constSym,true);

      // add this const decl to the block's list
      aBlock.declarations.Add(TConstDecl.Create(aConstName, values, constType));

      constSym.kickAsmData := parseKickAsmData;

      expect(ttSemiColon);
      Exit;
    end
    else
    if accept(ttLParen) then begin
    // defined by plain data values only
      count := 1;
      SetLength(values, 0);

      appendNumber(values, TNumber(getValue));

      while accept(ttComma) do
      begin
        appendNumber(values, TNumber(getValue));
        Inc(count);
      end;

      expect(ttRParen);

      constSym := TConstSymbol.Create(aConstName, constType,constType.size);
      constSym.isArray := True;
      constSym.isMsbValues := isMsb;
      constSym.len := count div constType.size;
      constSym.uses8BitIndex := (count * constType.size) <= 256;
      constSym.setSourceInfo(offset,fileName);

      FcurrentScope.insert(constSym,true);

      if accept(ttAbsolute) then begin
      // get address of array in memory
        e := simplify(simpleExpression);
        if not(e.isInteger or e.isVariable) then
          error('Absolute: Only supports number/variable absolute addresses');

        absoluteArrayData := TAbsoluteArrayData.Create;
        absoluteArrayData.dataSize := constType.size;
        absoluteArrayData.name     := aConstName;
        absoluteArrayData.address  := e.toString;

        SetLength(absoluteArrayData.data,Length(values));

        for i := 0 to High(values) do absoluteArrayData.data[i] := values[i].toString;

        FprogAST.absoluteArrayData.Add(absoluteArrayData);
      end
      else
      // add this const decl to the block's list
        aBlock.declarations.Add(TConstDecl.Create(aConstName, values, constType));

      expect(ttSemiColon);

      Exit;
    end
    else begin
    // defined by for loop values
    // a : array of Byte = for i := 0 to 30 do (i);
      expect(ttFor);
      forVar := Ftoken.tValue;

      if Ftoken.tType <> ttIdent then error('Const decl; array: expected variable for loop; found "'+e.toString+'"');

      expect(ttIdent);
      expect(ttBecomes);
      e := simplify(simpleExpression);

      if not e.isInteger then error('Const decl; array: for loop start must be a number constant');

      loRange := TNumber(e).value;
      expect(ttTo);
      e := simplify(simpleExpression);

      if not e.isInteger then error('Const decl; array: for loop end must be a number constant');

      hiRange := TNumber(e).value;

      if hiRange < loRange then error('Const decl; array : for loop end must be >= loop start');

      expect(ttDo);
      SetLength(values, 0);

      count := 0;

      // default to lsb values
      isMsb := False;

      if accept(ttGtr) then begin
        // is MSB value list
        isMsb := True;
      end
      else
      if accept(ttLss) then begin
        // is LSB value list
        isMsb := False;
      end;

      expect(ttLParen);

      FconstVarName := forVar;

      for i := loRange to hiRange do begin
        FconstVarValue := i;
        savePosition;
        FreplaceConstVar := True;
        e := simplify(simpleExpression);
        if not e.isInteger then error('Const decl; array: for loop item must resolve to a number');

        FreplaceConstVar := False;
//        writeLn('i: ',i,',',e.toString);
        appendNumber(values, TNumber(e));
        if i < hiRange  then restorePosition;
        Inc(count);
      end;

      expect(ttRParen);

      constSym := TConstSymbol.Create(aConstName, constType,constType.size);
      constSym.isArray := True;
      constSym.len  := count div constType.size;
      constSym.isMsbValues := isMsb;
      constSym.uses8BitIndex := (count * constType.size) <= 256;
      constSym.setSourceInfo(offset,fileName);

      FcurrentScope.insert(constSym,true);

      if accept(ttAbsolute) then begin
      // get address of array in memory
        e := simplify(simpleExpression);
        if not(e.isInteger or e.isVariable) then
          error('Absolute: Only supports number/variable absolute addresses');

        absoluteArrayData := TAbsoluteArrayData.Create;
        absoluteArrayData.dataSize := constType.size;
        absoluteArrayData.name     := aConstName;
        absoluteArrayData.address  := e.toString;

        SetLength(absoluteArrayData.data,Length(values));

        for i := 0 to High(values) do absoluteArrayData.data[i] := values[i].toString;

        FprogAST.absoluteArrayData.Add(absoluteArrayData);
      end
      else
      // add this const decl to the block's list
        aBlock.declarations.Add(TConstDecl.Create(aConstName, values, constType));
      expect(ttSemiColon);

      Exit;
    end;
  end;

  expect(ttLBracket);
  // get array lo range
  e := simplify(simpleExpression);

  if not e.isInteger then
    error('Const decl; array type low index must be a number');

  loRange := TNumber(e).value;

  if loRange <> 0 then
    error('Const decl; array type low index must = 0');

  expect(ttRange);

  // get array hi range
  e := simplify(simpleExpression);

  if not e.isInteger then
    error('Const decl; array type high index must be a number');

  hiRange := TNumber(e).value;

  if hiRange < loRange then
    error('Const decl; array type hi index must  >= lo index');

  expect(ttRBracket);

  expect(ttOf);

  constTypeName := Ftoken;

  if not(constTypeName.tType in [ttByte,ttInteger,ttShortInt, ttWord,ttInt24]) then
    error('Const decl; array types must be numeric, found type "' +
      constTypeName.tValue + '"');

  constType := FcurrentScope.lookup(constTypeName.tValue);

  expect(constTypeName.tType);

  expect(ttEql);

  expect(ttLParen);

  count := 1;
  SetLength(values, 0);

  appendNumber(values, TNumber(getValue));

  while accept(ttComma) do
  begin
    appendNumber(values, TNumber(getValue));
    Inc(count);
  end;

  if (count <> hiRange + 1) then
    error('Typed const: array; value count <> range: expected "'+IntToStr(hiRange+1)+'" values, found "'+IntToStr(count)+'" values instead...');

  expect(ttRParen);

  constSym := TConstSymbol.Create(aConstName, constType,constType.size);
  constSym.isArray := True;
  constSym.len := count div constType.size;
  constSym.isMsbValues := isMsb;
  constSym.uses8BitIndex := (count * constType.size) <= 256;
  constSym.setSourceInfo(offset,fileName);

  FcurrentScope.insert(constSym,true);

  if accept(ttAbsolute) then begin
  // get address of array in memory
    e := simplify(simpleExpression);
    if not(e.isInteger or e.isVariable) then
      error('Absolute: Only supports number/variable absolute addresses');

    absoluteArrayData := TAbsoluteArrayData.Create;
    absoluteArrayData.dataSize := constType.size;
    absoluteArrayData.name     := aConstName;
    absoluteArrayData.address  := e.toString;

    SetLength(absoluteArrayData.data,Length(values));

    for i := 0 to High(values) do absoluteArrayData.data[i] := values[i].toString;

    FprogAST.absoluteArrayData.Add(absoluteArrayData);
  end
  else
  // add this const decl to the block's list
    aBlock.declarations.Add(TConstDecl.Create(aConstName, values, constType));
  expect(ttSemiColon);
end;

procedure TParserAST.constDecls(aBlock: TBlock);
var
  constName     : AnsiString;
  constValue    : TToken;
  constType     : TSymbol;
  constSize     : Integer;
  sym           : TSymbol;
  constTypeName : TToken;
  value         : TToken;
  e             : TAST;
  srcOffset     : Integer;
  srcFile       : String;
begin
  repeat
    constName := Ftoken.tValue;

    srcOffset := Flexer.offset;
    srcFile   := FcurrentFileFull;

    sym := FcurrentScope.lookup(constName, True);

    if (sym <> nil) then
      error('Const decl; identifier "' + constName +
        '" already exists in scope "' + FcurrentScope.scopeName + '"');

//    if FcurrentScope.scopeLevel = 1 then
//      FcodeInsightData.AddDataByName(constName);

    expect(ttIdent);

    if accept(ttColon) then
    begin
      // is a typed constant
      // name : type = value
      if accept(ttArray) then
        // is array constant
        doTypedConstDecl_array(constName, aBlock,srcOffset,srcFile)
      else
      begin
        // other typed constant; number, string
        constTypeName := Ftoken;

        if Ftoken.tType = ttString then begin
          if LowerCase(Ftoken.tValue) = 'petscii' then
            Ftoken.tType := ttPetscii
          else
          if LowerCase(Ftoken.tValue) = 'ascii' then
            Ftoken.tType := ttAscii
        end;

        if not(constTypeName.tType in [ttByte,ttInteger,ttShortInt, ttWord,ttInt24, ttString, ttAscii,
          ttPetscii]) then
          error('Const decl; array types must be numeric, string, petscii, or ascii, found type "'
            + constTypeName.tValue + '"');

        constType := FcurrentScope.lookup(constTypeName.tValue);

        if constType = nil then
          error('Const decl; Unknown const type "' + constTypeName.tValue +
            '" found');

        expect(constTypeName.tType);

        expect(ttEql);

        // get expression and simplify; if not a number, throw error

        e := simplify(expression);

        if constTypeName.tType in [ttString, ttAscii, ttPetscii] then
        begin
          if not(e.isString) then
            error('Const declaration: expected a string value!');

          constValue := TString(e).token;
          constSize  := TString(e).size;
        end
        else
        begin
          if not(e.isInteger) then
            error('Const declaration: expected a number value!');

          constValue := TNumber(e).token;
          constSize  := TNumber(e).size;
        end;

        expect(ttSemiColon);

        sym := TConstSymbol.Create(constName, constValue,constType);
        sym.isSigned := constType.isSigned;
        sym.setSourceInfo(srcOffset,srcFile);

        FcurrentScope.insert(sym,true);

        // add this const decl to the block's list
        aBlock.declarations.Add(TConstDecl.Create(constName, constValue,
          constType));
      end;
    end
    else
    begin
      // normal constant; number, string
      expect(ttEql);

      if accept(ttImportBinary) then
        parseImportBinary(constName,aBlock)
      else
      begin
        // get expression and simplify; if not a number, throw error

        e := simplify(expression);

        if not(e.isInteger or e.isString) then
          error('Const declaration must equate to a number or string!');

        constValue := TNumber(e).token;
        constSize  := TNumber(e).size;

        expect(ttSemiColon);

        sym := TConstSymbol.Create(constName, constValue, nil);
        sym.setSourceInfo(srcOffset,srcFile);

        FcurrentScope.insert(sym,true);

        // add this const decl to the block's list
        aBlock.declarations.Add(TConstDecl.Create(constName, constValue, nil));
      end;
    end;
  until (Ftoken.tType <> ttIdent);
end;

type
  TrecVarInfo = record
    name : AnsiString;
  end;

var
  recVarInfo   : array of TrecVarInfo;
  recVarMemOfs : Integer;
  parentRec    : TVarDecl;

procedure resetRecVarInfo;
begin
  SetLength(recVarInfo, 0);
  recVarMemOfs := 0;
end;

procedure pushRecVarInfo(aName: AnsiString);
var
  i: Integer;
begin
  i := Length(recVarInfo);
  SetLength(recVarInfo, i + 1);
  recVarInfo[i].name   := aName;
end;

procedure popRecVarName;
begin
  SetLength(recVarInfo, High(recVarInfo));
end;

procedure TParserAST.addRecordVarToCodeInsight(aFullyQualifiedName : AnsiString; aVarDecl: TVarDecl);
// add the record var a.b.r.dfd... to the code insight for lookup
var
  i     : Integer;
  data  : TCodeInsightData;
  names : TStringDynArray;
  name  : AnsiString;
begin
  Exit;
  if FcurrentScope.scopeLevel > 1 then
  // only do global scope for now
    Exit;

  names := SplitString(aFullyQualifiedName,'.');

  for i := 0 to High(names) do  begin
    name := names[i];

    if i = 0 then
      data := FcodeInsightData.AddDataByName(name,aVarDecl.varSym)
    else
      data := data.AddDataByName(name,aVarDecl.varSym);
  end;

  if aVarDecl.varSym.size > 1 then begin
    data := data.GetDataByName(name);

    for i  := 0 to aVarDecl.varSym.size - 1 do
      data.AddDataByName('b'+IntToStr(i),FcurrentScope.lookup('byte'));

    if aVarDecl.varSym.size in [3,4] then begin
      data.AddDataByName('loword',FcurrentScope.lookup('word'));
      data.AddDataByName('hiword',FcurrentScope.lookup('word'));
    end;
  end;

end;

function varDeclToToken(varDecl : TVarDecl; aCurrentScope : TScopedSymbolTable) : TToken;
var
  sym : TSymbol;
begin
  sym := aCurrentScope.lookup(varDecl.varSym.symName);

  if varDecl.varSym.size = 1 then begin
    if sym.isSigned then
      Result := Token(ttShortInt,'ShortInt')
    else
      Result := Token(ttByte,'byte');
  end
  else
  if varDecl.varSym.size = 2 then begin
    if sym.isSigned then
      Result := Token(ttInteger,'Integer')
    else
      Result := Token(ttWord,'word');
  end
  else
  if varDecl.varSym.size = 3 then begin
    Result := Token(ttInt24,'int24');
  end;
end;

procedure TParserAST.expandRecordTypeToSymbolTable(aVarDecl: TVarDecl);
var
  i        : Integer;
  typeName : AnsiString;
  varName  : AnsiString;
  sym      : TSymbol;
  subSym   : TSymbol;
begin
  if (aVarDecl.varSym <> nil) and (aVarDecl.varSym.symType is TRecordSymbol) then begin
    // is record so emit record header and children var decls
    pushRecVarInfo(aVarDecl.varName);

    for i := 0 to TRecordSymbol(aVarDecl.varSym.symType).vars.count - 1 do
      expandRecordTypeToSymbolTable
        (TVarDecl(TRecordSymbol(aVarDecl.varSym.symType).vars.Items[i]));

    popRecVarName;
  end
  else
  begin
    // add this fully qualified var name into the symbol table
    // so it can be found by factor
    varName := '';
    for i := 0 to Length(recVarInfo) - 1 do begin
      varName := varName + recVarInfo[i].name;
      if i < Length(recVarInfo) - 1 then
        varName := varName + '.';
    end;
    varName := varName + '.' + aVarDecl.varName;

    typeName := Copy(varName,1,Pos('.',varName)-1);
    varName := Copy(varName,Pos('.',varName)+1,Length(varName));

    sym := FcurrentScope.lookup(aVarDecl.varSym.symName);
    sym.memOfs := recVarMemOfs;

//    writeLn(typeName+'.'+varName,' :',sym.memOfs);

    addRecVarInfo(typeName,varName,varDeclToToken(aVarDecl,FcurrentScope),sym.size,sym.memOfs,sym.isSigned);
//    addRecordVarToCodeInsight(varName,aVarDecl);

    FcurrentScope.insert(varSymbol(typeName+'.'+varName,sym),true);

    if aVarDecl.varSym.size > 1 then begin
      for i := 0 to aVarDecl.varSym.size - 1 do
      begin
        subSym := varSymbol(typeName+'.'+varName + '.b' + IntToStr(i),FcurrentScope.lookup('byte'));
        subSym.memOfs := sym.memOfs + i;

//        writeLn(subSym.symName,' :',subSym.memOfs);

        addRecVarInfo(typeName,varName + '.b' + IntToStr(i),varDeclToToken(aVarDecl,FcurrentScope),1,sym.memOfs+i,sym.isSigned);
        FcurrentScope.insert(subSym,true);
      end;

      if aVarDecl.varSym.size in [3,4] then begin
        subSym := varSymbol(typeName+'.'+varName + '.loword',FcurrentScope.lookup('word'));
        subSym.memOfs := sym.memOfs + 0;

//        writeLn(subSym.symName,' :',subSym.memOfs);

        addRecVarInfo(typeName,varName + '.loword',varDeclToToken(aVarDecl,FcurrentScope),2,sym.memOfs+0,sym.isSigned);
        FcurrentScope.insert(subSym,true);

        subSym := varSymbol(typeName+'.'+varName + '.hiword',FcurrentScope.lookup('word'));
        subSym.memOfs := sym.memOfs + 1+Ord(aVarDecl.varSym.size = 4);

//        writeLn(subSym.symName,' :',subSym.memOfs);

        addRecVarInfo(typeName,varName + '.hiword',varDeclToToken(aVarDecl,FcurrentScope),2,sym.memOfs+1,sym.isSigned);
        FcurrentScope.insert(subSym,true);
      end;
    end;

    Inc(recVarMemOfs,sym.size);
  end;
end;

procedure TParserAST.typeDecls(aBlock: TBlock);
var
  typeIdentifier : AnsiString;
  typeName       : AnsiString;
  varName        : AnsiString;
  varType        : TToken;
  sym            : TSymbol;
  varSym         : TSymbol;
  typeSymbol     : TSymbol;
  varDecl        : TVarDecl;
  variableType   : TSymbol;
  variable       : TVarDecl;
  e              : TAST;
  address        : AnsiString;
  vars           : array of AnsiString;
  i              : Integer;
  isTypedPointer : Boolean;
  srcOffset      : Integer;
  srcFile        : String;

  procedure appendVar(aName: AnsiString);
  begin
    i := Length(vars);
    SetLength(vars, i + 1);
    vars[i] := aName;
  end;

begin
  isTypedPointer := False;

  typeIdentifier := Ftoken.tValue;

  srcOffset := Flexer.offset;
  srcFile   := FcurrentFileFull;

  if FcurrentScope.lookup(typeIdentifier,True) <> nil then
    error('type decl: "' + typeIdentifier + '" already exists');

//  if not FcurrentScope.insert(typeSymbol, True) then

  expect(ttIdent);
  expect(ttEql);

  if accept(ttCaret) then isTypedPointer := True;

  if accept(ttRecord) then begin
  // do record type
    typeSymbol := TRecordSymbol.Create(typeIdentifier);
    typeSymbol.isPointerType := isTypedPointer;
    typeSymbol.setSourceInfo(srcOffset,srcFile);

    FcurrentScope.insert(typeSymbol, True);

    repeat
      // get var name for this section of record
      SetLength(vars, 0);
      varName := Ftoken.tValue;
      appendVar(varName);
      expect(ttIdent);

      while accept(ttComma) do
      begin
        varName := Ftoken.tValue;
        appendVar(varName);
        expect(ttIdent);
      end;

      expect(ttColon);

      // get var type for this section of record " : <type>"
      varType := Ftoken;

      sym := FcurrentScope.lookup(varType.tValue);

      if (sym = nil) then
        error('Record; Var decl: Unknown var type "' + varType.tValue + '"');

      if not(varType.tType in [ttByte,ttInteger,ttShortInt, ttBoolean, ttPointer, ttWord, ttInt24]) then
      begin
        if not(sym.symClass in [scCustomType,scRecordType]) then
          error('Record; Var decl: Unsupported var type "' + varType.tValue + '"');
      end;

      expect(varType.tType);

      address := '';

      if accept(ttAbsolute) then begin
      // this is an absolute memory var in the record
        e := Simplify(simpleExpression);
        if not e.isInteger then
          error('Record; Var decl: absolute variables must be a number!');

        address := e.toString;
      end;

      // add children var decls
      for i := 0 to Length(vars) - 1 do
      begin
        varSym := varSymbol(varType.tValue, sym);
        TVarSymbol(varSym).address := address;

        FcurrentScope.insert(varSym,true);

        varDecl := TVarDecl.Create(vars[i], varSym, '', '');
        varDecl.address := address;

        TRecordSymbol(typeSymbol).vars.Add(varDecl);
      end;

      expect(ttSemiColon);

    until Ftoken.tType = ttEnd;

    expect(ttEnd);

    if (address = '') then
    begin
      // expand this record type to the symbol table
      resetRecVarInfo;

      //vic(TVarDecl): varSym: (TVarSymbol)TVIC
      varSym   := varSymbol('', typeSymbol);
      variable := TVarDecl.Create(typeSymbol.symName, varSym, '', '');

      expandRecordTypeToSymbolTable(variable);
    end

  end
  else begin
    // is a non-record type (pointer type or not)
    typeName := Ftoken.tValue;
    expect(Ftoken.tType);

    sym := FcurrentScope.lookup(typeName,True);
    if sym = nil then error('type decl: Unknown type "' + typeName + '"');

    if isTypedPointer then begin
      typeSymbol := TCustomTypeSymbol.Create(typeIdentifier,sym,2);
      TCustomTypeSymbol(typeSymbol).isPointerType := True;
    end
    else
      typeSymbol := TCustomTypeSymbol.Create(typeIdentifier,sym,sym.size);

    FcurrentScope.insert(typeSymbol, True);
  end;

  expect(ttSemiColon);
end;

procedure TParserAST.expandRecordVarToSymbolTable(aVarDecl: TVarDecl);
var
  i       : Integer;
  varName : AnsiString;
  sym     : TSymbol;
  subSym  : TSymbol;
begin
  if (aVarDecl.varSym <> nil) and (aVarDecl.varSym.symType is TRecordSymbol) then begin
    // is record so emit record header and children var decls
    pushRecVarInfo(aVarDecl.varName);

//    writeLn(Format('.label %s = $0400 + %d {',[aVarDecl.varName,recVarMemOfs]));

    for i := 0 to TRecordSymbol(aVarDecl.varSym.symType).vars.count - 1 do
      expandRecordVarToSymbolTable
        (TVarDecl(TRecordSymbol(aVarDecl.varSym.symType).vars.Items[i]));

//    writeLn('}');

    popRecVarName;
  end
  else
  begin
    // add this fully qualified var name into the symbol table
    // so it can be found by factor
    varName := '';
    for i := 0 to Length(recVarInfo) - 1 do
    begin
      varName := varName + recVarInfo[i].name;
      if i < Length(recVarInfo) - 1 then
        varName := varName + '.';
    end;
    varName := varName + '.' + aVarDecl.varName;

    addRecordVarToCodeInsight(varName,aVarDecl);

    sym := FcurrentScope.lookup(aVarDecl.varSym.symName);
    sym.memOfs := recVarMemOfs;

    FcurrentScope.insert(varSymbol(varName,sym),true);

//    writeLn(Format('.label %s = $0400 + %d {',[aVarDecl.varName,sym.memOfs]));

    if aVarDecl.varSym.size > 1 then begin
      for i := 0 to aVarDecl.varSym.size - 1 do
      begin
        subSym := varSymbol(varName + '.b' + IntToStr(i),FcurrentScope.lookup('byte'));
        subSym.memOfs := sym.memOfs + i;

//        writeLn(Format('.label b%d = $0400 + %d',[i,sym.memOfs + i]));

        FcurrentScope.insert(subSym,true);
      end;

      if aVarDecl.varSym.size in [3,4] then begin
        subSym := varSymbol(varName + '.loword',FcurrentScope.lookup('word'));
        subSym.memOfs := sym.memOfs + 0;

        FcurrentScope.insert(subSym,true);

        subSym := varSymbol(varName + '.hiword',FcurrentScope.lookup('word'));
        subSym.memOfs := sym.memOfs + 1+Ord(aVarDecl.varSym.size = 4);

        FcurrentScope.insert(subSym,true);
      end;
    end;

//    writeLn('}');

    Inc(recVarMemOfs,sym.size);
  end;
end;

procedure TParserAST.varDecls(aBlock: TBlock);
var
  i, j: Integer;
  varList: TStringArray;
  srcInfoList : TSourceInfoArray;
  address: AnsiString;
  varName : AnsiString;
  varSym: TSymbol;
  varType: TSymbol;
  typeToken : TToken;
  e: TAST;
  defaultValue: AnsiString;
  variable: TVarDecl;
  isArray: Boolean;
  isPointer : Boolean;
  arrayType: TTokenType;
  arraySize  : Integer;
  strLength  : Integer;
  data       : TCodeInsightData;
  recordInfo : TRecInfoArray;
  isVolatile : Boolean;

  procedure generateVarInfo(varName : AnsiString; varType : TSymbol; generateSubParts : Boolean; isVolatile : Boolean);
  var
    i, j     : Integer;
    varSym   : TSymbol;
    variable : TVarDecl;
    tempVar  : TVarDecl;
    tempSym  : TSymbol;
  begin
    varSym := varSymbol(varName, varType);
    varSym.srcOffset  := srcInfoList[i].offset;
    varSym.srcFile    := srcInfoList[i].fileName;
    varSym.isVolatile := isVolatile;

    TVarSymbol(varSym).isArray := isArray;
    TVarSymbol(varSym).arraySize := arraySize;

    if isPointerType(varSym) then
      TVarSymbol(varSym).address := '$' + IntToHex(getPointerAddress, 2)
    else
      TVarSymbol(varSym).address := address;

    TVarSymbol(varSym).uses8BitIndex := (arraySize * varType.size) <= 256;

    FcurrentScope.insert(varSym,true);

    variable := TVarDecl.Create(varName, varSym, TVarSymbol(varSym).address, defaultValue);
    variable.isArray := isArray;
    variable.arraySize := arraySize;
    variable.isPointer := isPointer;//arrayType = ttPointer;

  //  if FcurrentScope.scopeLevel = 1 then begin
  //      data := FcodeInsightData.AddDataByName(varList[i]);
  //
  //      if varSym.size > 1 then begin
  //        for j := 0 to varSym.size - 1 do
  //            data.AddDataByName('b' + IntToStr(j));
  //
  //        if varSym.size >= 3 then begin
  //          data.AddDataByName('loword');
  //          data.AddDataByName('hiword');
  //        end;
  //      end;
  //  end;

    if generateSubParts then begin
      if varSym.size in [3,4] then begin
        // add loword/hiword regarding int24/dword variables
        varSym := varSymbol(varName + '.loword',FcurrentScope.lookup('word'));
        TVarSymbol(varSym).address := varName + ' + 0';
        FcurrentScope.insert(varSym,true);

        varSym := varSymbol(varName + '.hiword',FcurrentScope.lookup('word'));
        TVarSymbol(varSym).address := varName + ' + '+IntToStr(1+Ord(varSym.size = 4));
        FcurrentScope.insert(varSym,true);
      end;
    end;

    aBlock.declarations.Add(variable);

    // add all declarations for this variable, including sub-parts
    // for multi-byte variables
    // a: byte = a, b: integer = b.b0, b.b1

    if varType.symName <> 'string' then
    begin
      if (varType.symClass = scRecordType) then
      begin
        // expand this record var to the symbol table
        resetRecVarInfo;

        expandRecordVarToSymbolTable(variable);
      end
      else
      if (varType is TCustomTypeSymbol) and (TCustomTypeSymbol(varType).parentSym is TRecordSymbol) then begin
      // is a custom record type (possibly a pointer)
        tempSym := varSymbol('', TCustomTypeSymbol(varType).parentSym);
        tempVar := TVarDecl.Create(varName, tempSym, '', '');

        // expand this record var to the symbol table
        resetRecVarInfo;

        expandRecordVarToSymbolTable(tempVar);

        tempVar.Free;
        tempSym := nil;
      end
      else
  //      if (varType.symClass = scCustomType) and (TCustomTypeSymbol(varType).isPointerType) then begin
  //        variable.varName := variable.varName + '^';
  //      end
  //      else
      if generateSubParts then begin
        if varType.size > 1 then begin
          for j := 0 to varType.size - 1 do
          begin
            varSym := varSymbol(varName + '.b' + IntToStr(j),
              FcurrentScope.lookup('byte'));

            FcurrentScope.insert(varSym,true);
          end;
        end;
      end;
    end;
  end;

begin
  SetLength(varList, 0);
  defaultValue := '';

  SetLength(srcInfoList,0);

  repeat
    if (FcurrentScope.lookup(Ftoken.tValue, True) <> nil) then
      error('Var decl: identifier "' + Ftoken.tValue +
        '" already exists in scope "' + FcurrentScope.scopeName + '"');

    appendStr(varList, Ftoken.tValue);
    appendSrcInfo(srcInfoList,Flexer.offset,FcurrentFileFull);

    expect(ttIdent);
  until not(accept(ttComma));

  expect(ttColon);

  isArray := False;

  if accept(ttArray) then
  begin
    // parse array Var decl
    isArray := True;
    expect(ttLBracket);

    e := simplify(simpleExpression);

    if not e.isInteger then
      error('Var decl: array low bound must be a number');

    if (e.toString <> '0') then
      error('Var decl: array low bound must = "0"');

    expect(ttRange);

    e := simplify(simpleExpression);

    if not e.isInteger then
      error('Var decl: array high bound must be a number');

    if (TNumber(e).value < 0) then
      error('Var decl: array high bound must >= 0');

    arraySize := TNumber(e).value + 1;

    expect(ttRBracket);

    expect(ttOf);
  end;

  // var type
  varType := FcurrentScope.lookup(Ftoken.tValue);

  //
  if (varType = nil) then
    error('Var decl: Unknown var type "' + Ftoken.tValue + '"');

  if isArray and (Ftoken.tType in [ttString,ttAscii,ttPetscii]) then
    error('Var decl: array type cannot be a "string"');

  if not(Ftoken.tType in [ttBoolean, ttByte,ttInteger,ttShortInt, ttPointer, ttWord, ttInt24,
    ttString,ttAscii,ttPetscii]) then
  begin
    if not(varType.symClass in [scRecordType,scCustomType]) then
//    if not(varType is TRecordSymbol) then
      error('Var decl: Unsupported var type "' + Ftoken.tValue + '"');
  end;

  arrayType := Ftoken.tType;

  isPointer := False;

  if (varType.symClass = scCustomType) then begin
    if TCustomTypeSymbol(varType).isPointerType then begin
      address := '$';// + IntToHex(getPointerAddress, 2);
      isPointer := True;
    end;
  end
  else
  if (Ftoken.tType = ttPointer) then
  begin
    address := '$';// + IntToHex(getPointerAddress, 2);
    isPointer := True;

    if isArray then
      // skip other array pointer values for other pointers
      for i := 1 to arraySize - 1 do
        getPointerAddress;
  end
  else
    address := '';

  // check for truncated string
  strLength := -1;

  if Ftoken.tType in [ttString,ttAscii,ttPetscii] then begin
    typeToken := Ftoken;

    // skip vartype token
    expect(Ftoken.tType);

    if accept(ttLBracket) then begin
      e := Simplify(simpleExpression);
      if not e.isInteger then
        error('Var decl: truncated string length must be a number');

      if TNumber(e).value > 255 then
        error('Var decl: truncated string length must be <= 255');

      strLength := TNumber(e).value + 1;// add in length byte
      expect(ttRBracket);

      // replace string type with new length
      varType := builtinTypeSymbol(typeToken.tValue,strLength,false);
    end;
  end
  else
    // skip vartype token
    expect(Ftoken.tType);

  if accept(ttAbsolute) then
  begin
    if isArray or (address <> '') then
      error('Cannot use absolute on pointer variables/array types');

    e := simplify(simpleExpression);
    if not(e.isInteger or e.isVariable) then
      error('Absolute: Only supports number/variable absolute addresses');

    if e.isLeaf then
      address := e.toString;
  end;

  if accept(ttEql) then
  begin
    if isArray then
      error('Var decl: arrays cannot have a default value');

    // has a default value
    if address <> '' then
      error('Var decl: default values cannot be used on absolute or pointer variables');

    if Length(varList) > 1 then
      error('Var decl: only single var decls can have default values');

    e := simplify(simpleExpression);

    if not e.isInteger then
      error('Var decl: default value must be a number');

    defaultValue := TNumber(e).token.tValue;
  end;

  expect(ttSemiColon);

  isVolatile := False;
  if accept(ttVolatile) then begin
    isVolatile := True;
    expect(ttSemiColon);
  end;

  // generate var decls and add to the block's list
  for i := 0 to High(varList) do
  begin
    // insert base var decl
    varName := varList[i];

    if (isArray) and (varType is TRecordSymbol) then begin
      // change <name>[] -> <name>_recparts[]
      recordInfo := getRecVarInfo(varType.symName);

      for j := 0 to High(recordInfo) do begin
        varName := varList[i]+'_'+StringReplace(recordInfo[j].varName,'.','_',[rfReplaceAll]);
        // use new combined name and new type
        generateVarInfo(varName,FcurrentScope.lookup(recordInfo[j].varType.tValue),false,isVolatile);

        // store variable + record parts into symbol table for code insight
        FcurrentScope.insert(varSymbol(varList[i] + '.' + recordInfo[j].varName, varType));
      end;
    end
    else
    // normal non-record var or array
      generateVarInfo(varName,varType,true,isVolatile);
  end;
end;

function TParserAST.formalParams(var paramCount: Integer): TParamArray;
const
  PARAM_VAR_PREFIX = [ttConst, ttVar];

var
  token: TToken;
  paramVars: array of AnsiString;
  paramSym: TSymbol;
  paramType: TSymbol;
  i, j: Integer;
begin
  // exit early if empty () and no parameters
  if (Ftoken.tType = ttRParen) then
    Exit;

  repeat
    SetLength(paramVars, 0);
    repeat
      // consume and ignore 'const' and 'var' parameter prefixes
      if (Ftoken.tType in PARAM_VAR_PREFIX) then
        nextToken;

      Inc(paramCount);

      paramSym := FcurrentScope.lookup(Ftoken.tValue, True);

      if (paramSym <> nil) then
        error('Procedure/function "' + FcurrentScope.scopeName +
          '": Duplicate param identifier "' + Ftoken.tValue + '" found');

      i := Length(paramVars);
      SetLength(paramVars, i + 1);
      paramVars[i] := Ftoken.tValue;
      expect(ttIdent);
    until not accept(ttComma);

    expect(ttColon);

    paramType := FcurrentScope.lookup(Ftoken.tValue);

    if (paramType = nil) then
      error('Procedure/function "' + FcurrentScope.scopeName +
        '": Undefined param var type "' + Ftoken.tValue + '" found');

    for i := 0 to Length(paramVars) - 1 do
    begin
      j := Length(Result);
      SetLength(Result, j + 1);
      Result[j] := TParam.Create(paramVars[i], paramType);
    end;

    // skip type
    nextToken;
  until not accept(ttSemiColon);
end;

procedure TParserAST.macroDecl(aBlock: TBlock);
var
  i, j: Integer;
  macroName: AnsiString;
  params: TStringArray;
  macroScope: TscopedSymbolTable;
  macroSym: TMacroSymbol;
  sym: TSymbol;
  paramCount: Integer;
  thisMacro: TMacroDecl;
  loReg: AnsiString;
  hiReg: AnsiString;
begin
  if (FcurrentScope.scopeLevel > 1) then
    error('Macro decl: Macros can only be defined in the global scope');

  macroName := Ftoken.tValue;

  sym := FcurrentScope.lookup(macroName, True);

  if sym <> nil then
    error('Macro decl: redeclared identifier "' + macroName + '"');

  expect(ttIdent);

  paramCount := 0;

  SetLength(params, 0);

  if accept(ttLParen) then
  begin
    if Ftoken.tType <> ttRParen then
    begin
      appendStr(params, Ftoken.tValue);
      if (Ftoken.tType <> ttIdent) then
        error('Macro decl: parameters must be identifiers only');
      nextToken;
      Inc(paramCount);

      while accept(ttComma) do
      begin
        appendStr(params, Ftoken.tValue);
        if (Ftoken.tType <> ttIdent) then
          error('Macro decl: parameters must be identifiers only');
        nextToken;
        Inc(paramCount);
      end;
    end;

    expect(ttRParen);
  end;

  macroSym := TMacroSymbol.Create(macroName, params);

  FcurrentScope.insert(macroSym);

  macroSym.macroType := ttMacro;
  macroSym.paramCount := paramCount;

  expect(ttSemiColon);

  if accept(ttAssembler) then
  begin
    macroSym.macroType := ttAssembler;
    expect(ttSemiColon);
  end;

  if (macroSym.macroType = ttMacro) then
    thisMacro := TMacroDecl.Create(macroName, params,
      block(macroName, ttEOF, False, True), macroScope)
  else if (macroSym.macroType = ttAssembler) then
    thisMacro := TMacroDecl.Create(macroName, params,
      asmBlock(macroName, False, ttEOF, False, True), macroScope);

  aBlock.declarations.Add(thisMacro);
end;

function isParamVar(routine : TRoutine; varName : AnsiString; var index : Integer) : Boolean;
var
  i : Integer;
begin
  Result := False;

  for i := 0 to routine.paramCount - 1 do begin
    if routine.params[i].name = varName then begin
      Result := True;
      index  := i;
      Exit;
    end;
  end;
end;


procedure TParserAST.generateStackInfo(routine : TRoutine; block : TBlock);
var
  sym       : TSymbol;
  param     : TVarSymbol;
  i,j       : Integer;
  stackSize : Integer;
  stackOfs  : Integer;
  varDecl   : TVarDecl;
  varSize   : Integer;
  name      : AnsiString;
  index     : Integer;
begin
  // iterate through parameters and set stack offset
  stackOfs  := 1;
  stackSize := 0;

//  writeLn('------------------------');
  for i := 0 to block.declarations.Count - 1 do begin
  // iterate through declarations and set stack offset
    if (block.declarations.Items[i] is TVarDecl) then begin
      varDecl := TVarDecl(block.declarations.Items[i]);

      sym := FcurrentScope.lookup(varDecl.varName,true);

      if not isPointerType(sym) then begin
        sym.stackOfs := stackOfs;
        sym.onStack  := True;

        varDecl.stackOfs := stackOfs;
        varDecl.onStack  := True;

        if isParamVar(routine,varDecl.varName,index) then begin
          routine.params[index].onStack  := true;
          routine.params[index].stackOfs := stackOfs;
        end;

        varSize := varDecl.size;
        if varDecl.varSym <> nil then
          varSize := varDecl.varSym.size;

        // include sub-variables like .b0, .loword, etc.
        if not varDecl.isArray and (varSize > 1) then begin
          for j := 0 to varSize - 1 do begin
            sym := FcurrentScope.lookup(varDecl.varName+'.b'+IntToStr(j),true);
            sym.stackOfs := stackOfs+j;
            sym.onStack  := True;
          end;

          if varSize >= 3 then begin
            sym := FcurrentScope.lookup(varDecl.varName+'.loword',true);
            sym.onStack  := True;
            sym.stackOfs := stackOfs+0;

            sym := FcurrentScope.lookup(varDecl.varName+'.hiword',true);
            sym.onStack  := True;
            sym.stackOfs := stackOfs+1+Ord(varSize = 4);
          end;
        end;

        if varDecl.isArray then
        // use array size instead
          varSize := varDecl.arraySize * varSize;

//        writeLn(Format('%s.'+varDecl.varName+'(%d..%d)',[FcurrentScope.scopeName,stackOfs,stackOfs+varSize-1]));

        Inc(stackOfs,varSize);
        Inc(stackSize,varSize);
      end;
    end;
  end;

  routine.stackSize := stackSize;
  routine.onStack   := True;
end;

function isPointer(sym : TSymbol) : Boolean;
begin
  Result := True;

  if Lowercase(sym.symName) = 'pointer' then Exit;

  if (sym is TCustomTypeSymbol) and TCustomTypeSymbol(sym).isPointerType then Exit;

  Result := False;
end;

procedure TParserAST.addVarRecordInfoToSymbolTable(varName : AnsiString; recordInfo : TRecInfoArray);
var
  sym     : TSymbol;
  subSym  : TSymbol;
  i       : Integer;
  name    : AnsiString;
  varType : TSymbol;
begin
  for i := 0 to High(recordInfo) do begin
    name := varName+'.'+recordInfo[i].varName;
    varType := FcurrentScope.lookup(recordInfo[i].varType.tValue);

    sym := varSymbol(name,varType);
    sym.memOfs   := recordInfo[i].varMemOfs;

//    writeLn(sym.symName,':',sym.symType.symName,' (',sym.symType.isSigned,')');
    FcurrentScope.insert(sym,true);
  end;

    if varType.size > 1 then begin
      for i := 0 to varType.size - 1 do
      begin
        subSym := varSymbol(name + '.b' + IntToStr(i),FcurrentScope.lookup('byte'));
        subSym.memOfs := sym.memOfs + i;

//        writeLn(Format('.label b%d = $0400 + %d',[i,sym.memOfs + i]));

        FcurrentScope.insert(subSym,true);
      end;

      if varType.size in [3,4] then begin
        subSym := varSymbol(name + '.loword',FcurrentScope.lookup('word'));
        subSym.memOfs := sym.memOfs + 0;

        FcurrentScope.insert(subSym,true);

        subSym := varSymbol(name + '.hiword',FcurrentScope.lookup('word'));
        subSym.memOfs := sym.memOfs + 1+Ord(varType.size = 4);

        FcurrentScope.insert(subSym,true);
      end;
    end;
end;

procedure TParserAST.procDecl(aBlock: TBlock);
var
  i, j: Integer;
  procName: AnsiString;
  params: TParamArray;
  procScope: TscopedSymbolTable;
  procSym: TProcSymbol;
  sym: TProcSymbol;
  paramCount: Integer;
  thisProc: TProcDecl;
  loReg: AnsiString;
  hiReg: AnsiString;
  paramVar : TVarDecl;
  paramType : TSymbol;
  e        : TAST;
  srcOffset  : Integer;
  srcFile    : String;

  tempSym    : TSymbol;
  tempVar    : TVarDecl;

  recordInfo : TRecInfoArray;

begin
  procName := Ftoken.tValue;

//  if FcurrentScope.scopeLevel = 1 then
//    if procName[1] <> '_' then
//      FcodeInsightData.AddDataByName(procName);

  procSym := TProcSymbol.Create(procName, []);

  sym := TProcSymbol(FcurrentScope.lookup(procName, True));

  if (sym <> nil) and (sym.isFwdDecl) then
  begin
    // FcurrentScope.replace(procSym,True);
    FreeAndNil(procSym);
    procSym := sym;
    procSym.isFwdDecl := False;
  end
  else if not FcurrentScope.insert(procSym, True) then
    error('procDecl: Redeclared identifier "' + procName + '"');

  srcOffset := FLexer.offset;
  srcFile   := FcurrentFileFull;
  procSym.setSourceInfo(srcOffset,srcFile);

  procScope := TscopedSymbolTable.Create(procName, FcurrentScope.scopeLevel + 1,
    FcurrentScope);
  FcurrentScope := procScope;

  expect(ttIdent);

  paramCount := 0;

  if accept(ttLParen) then
  begin
    params := formalParams(paramCount);

    expect(ttRParen);
  end;

  procSym.procType := ttProc;

  if accept(ttExternal) then
  begin
    e := simplify(simpleExpression);

    if not (e.isInteger) then error('External proc address: Only supports number addresses');

    procSym.procType    := ttAssembler;
    procSym.procAddress := e.toString;

//    expect(Ftoken.tType);
  end
  else
  begin
    expect(ttSemiColon);

    procSym.irqType := ttNone;

//    if accept(ttRasterIRQ) then
//    begin
//      if paramCount > 0 then
//        error('Proc Decl: raster irq routines cannot have parameters');
//      procSym.irqType := ttRasterIRQ;
//      procSym.isVolatile := True;
//
//      expect(ttSemiColon);
//    end
//    else
    if accept(ttInterrupt) then
    begin
      if paramCount > 0 then
        error('Proc Decl: interrupt/raster routines cannot have parameters');
      procSym.irqType := ttInterrupt;
      procSym.isVolatile := True;

      expect(ttSemiColon);
    end
    else
    if accept(ttKernalInterrupt) then
    begin
      if paramCount > 0 then
        error('Proc Decl: kernal interrupt routines cannot have parameters');
      procSym.irqType := ttKernalInterrupt;
      procSym.isVolatile := True;

      expect(ttSemiColon);
    end;


    if accept(ttAssembler) then
    begin
      procSym.procType := ttAssembler;

      expect(ttSemiColon);
    end;
  end;

  if accept(ttCdecl) then begin
    if procSym.procType = ttAssembler then
      Error('Procedure: cannot be both assembler and cdecl');

    procSym.onStack := True;
    expect(ttSemiColon);
  end;

  if accept(ttVolatile) then begin
    procSym.isVolatile := True;
    expect(ttSemiColon);
  end;

  procSym.paramCount := paramCount;

  if accept(ttForward) then
  begin
    routines_addInfo(procSym.name,-1,-1);
    procSym.isFwdDecl := True;
  end
  else
  begin
    // not a forward declaration
    // insert these paramVar : paramType pairs into the current scope
    // including multi-byte parts
    for i := 0 to Length(params) - 1 do
    begin
      // only write non-assembler procedure params as variables in scope

      if (procSym.procType = ttProc) or not isAsmParam(params[i].paramName,
        params[i].paramType, loReg, hiReg) then begin
        FcurrentScope.insert(varSymbol(params[i].paramName,
          params[i].paramType),true);

        if params[i].paramType.size > 1 then begin
          for j := 0 to params[i].paramType.size - 1 do
            FcurrentScope.insert(varSymbol(params[i].paramName + '.b' +
              IntToStr(j), FcurrentScope.lookup('byte')),true);

          if params[i].paramType.size >= 3 then begin
            FcurrentScope.insert(varSymbol(params[i].paramName + '.loword',
              FcurrentScope.lookup('word')),true);

            FcurrentScope.insert(varSymbol(params[i].paramName + '.hiword',
              FcurrentScope.lookup('word')),true);
          end;
        end;
      end;
    end;

    if (procSym.procType = ttProc) then
      thisProc := TProcDecl.Create(procName, params,
        block(procName, ttByte, False, False), procScope)
    else if (procSym.procType = ttAssembler) then
      thisProc := TProcDecl.Create(procName, params,
        asmBlock(procName, procSym.procAddress <> '', ttByte, False, False),
        procScope);

    SetLength(procSym.params, Length(params));

    for i := Length(params) - 1 downto 0 do begin
      procSym.params[i] := TVarSymbol(varSymbol(params[i].paramName,
        params[i].paramType));

 //     procSym.params[i].onStack := procSym.onStack;

      if (procSym.procType = ttProc) or not isAsmParam(params[i].paramName,
        params[i].paramType, loReg, hiReg) then begin

        paramVar := TVarDecl.Create(params[i].paramName,params[i].paramType, '', '');

//        paramVar.onStack := procSym.onStack;

        if isPointer(paramVar.varSym) then begin
//        if (LowerCase(params[i].paramType.symName) = 'pointer') then begin
          paramVar.isPointer := True;
          paramVar.address := '$' + IntToHex(getPointerAddress, 2);

          if (paramVar.varSym is TCustomTypeSymbol) and (TCustomTypeSymbol(paramVar.varSym).parentSym is TRecordSymbol) then begin
          // is a custom record type (possibly a pointer)
            recordInfo := getRecVarInfo(TCustomTypeSymbol(paramVar.varSym).parentSym.symName);

            addVarRecordInfoToSymbolTable(paramVar.varName,recordInfo);
          end;
        end;

        paramVar.isParam := True;

        thisProc.block.declarations.insert(0, paramVar);
      end;
    end;

    aBlock.declarations.Add(thisProc);
    thisProc.onStack := procSym.onStack;

    if procSym.onStack and (procSym.procType <> ttAssembler) then
      generateStackInfo(procSym,thisProc.block);
  end;

  // restore previous scope
  FcurrentScope := procScope.enclosingScope;
end;

procedure TParserAST.funcDecl(aBlock: TBlock);
var
  i, j: Integer;
  funcName: AnsiString;
  params: TParamArray;
  funcScope: TscopedSymbolTable;
  funcSym: TFuncSymbol;
  sym : TFuncSymbol;
  funcResult: TVarSymbol;
  paramCount: Integer;
  thisFunc: TFuncDecl;
  loReg: AnsiString;
  hiReg: AnsiString;
  retType: TTokenType;
  retValue : AnsiString;
  varSym   : TVarSymbol;
  paramVar : TVarDecl;
  e        : TAST;
  srcOffset  : Integer;
  srcFile    : String;
  resultVar  : TVarDecl;
begin
  funcName := Ftoken.tValue;
//  if FcurrentScope.scopeLevel = 1 then
//    if funcName[1] <> '_' then
//      FcodeInsightData.AddDataByName(funcName);

  funcSym := TFuncSymbol.Create(funcName, []);

  sym := TFuncSymbol(FcurrentScope.lookup(funcName, True));

  if (sym <> nil) and (sym.isFwdDecl) then
  begin
    // FcurrentScope.replace(procSym,True);
    FreeAndNil(funcSym);
    funcSym := sym;
    funcSym.isFwdDecl := False;
  end
  else if not FcurrentScope.insert(funcSym, True) then
    error('funcDecl: Redeclared identifier "' + funcName + '"');

  srcOffset := FLexer.offset;
  srcFile   := FcurrentFileFull;
  funcSym.setSourceInfo(srcOffset,srcFile);

  FcurrentScope.insert(funcSym,true);

  funcScope := TscopedSymbolTable.Create(funcName, FcurrentScope.scopeLevel + 1,
    FcurrentScope);
  FcurrentScope := funcScope;

  expect(ttIdent);

  paramCount := 0;

  if accept(ttLParen) then
  begin
    params := formalParams(paramCount);

    expect(ttRParen);
  end;

  funcSym.funcType := ttFunc;

  expect(ttColon);

  retType  := Ftoken.tType;
  retValue := LowerCase(Ftoken.tValue);

  if (not (retType in [ttByte,ttInteger,ttShortInt,ttWord,ttBoolean,ttInt24])) then
    error('Func decl: only supports "byte/word/boolean/int24" return type, found "' +
      Ftoken.tValue + '"');

  funcSym.symType := FcurrentScope.lookup(Ftoken.tValue);

  nextToken;

  if accept(ttExternal) then
  begin
    e := simplify(simpleExpression);

    if not (e.isInteger) then error('External func address: Only supports number addresses');

    funcSym.funcType := ttAssembler;
    funcSym.funcAddress := e.toString;

    //expect(ttIntNum);
  end
  else
  begin
    expect(ttSemiColon);

    if accept(ttAssembler) then
    begin
      funcSym.funcType := ttAssembler;
      expect(ttSemiColon);
    end;
  end;

  if accept(ttCdecl) then begin
    if funcSym.funcType = ttAssembler then
      Error('Function: cannot be both assembler and cdecl');

    funcSym.onStack := True;
    expect(ttSemiColon);
  end;

  if accept(ttVolatile) then begin
    funcSym.isVolatile := True;
    expect(ttSemiColon);
  end;

  if accept(ttForward) then
  begin
    routines_addInfo(funcSym.name,-1,-1);
    funcSym.isFwdDecl := True;
  end
  else
  begin
    // not a forward declaration
    // insert these paramVar : paramType pairs into the current scope
    // including multi-byte parts
    for i := 0 to Length(params) - 1 do
    begin
      // only write non-assembler funcedure params as variables in scope

      if (funcSym.funcType = ttFunc) or not isAsmParam(params[i].paramName,
        params[i].paramType, loReg, hiReg) then
      begin
        FcurrentScope.insert(varSymbol(params[i].paramName, params[i].paramType),true);

        if params[i].paramType.size > 1 then begin
          for j := 0 to params[i].paramType.size - 1 do
            FcurrentScope.insert(varSymbol(params[i].paramName + '.b' + IntToStr(j),
              FcurrentScope.lookup('byte')),true);
        end;
      end;
    end;

    funcSym.paramCount := paramCount;

    if funcSym.funcAddress = '' then
    begin
      // add result type to this function body
      funcResult := TVarSymbol(varSymbol('result', funcSym.symType));
      funcResult.isFuncResult := True;
      FcurrentScope.insert(funcResult,true);

      if FcurrentScope.lookup(retValue).size > 1 then begin
        for j := 0 to FcurrentScope.lookup(retValue).size - 1 do
        begin
          FcurrentScope.insert(varSymbol('result.b' + IntToStr(j),
              FcurrentScope.lookup('byte')),true);
        end;

        if FcurrentScope.lookup(retValue).size = 3 then begin
          FcurrentScope.insert(varSymbol('result.loword',
              FcurrentScope.lookup('word')),true);
          FcurrentScope.insert(varSymbol('result.hiword',
              FcurrentScope.lookup('word')),true);
        end;
      end;
    end;

    if (funcSym.funcType = ttFunc) then
      thisFunc := TFuncDecl.Create(funcName, params,
        block(funcName, retType, funcSym.funcAddress <> '', False), funcScope)
    else if (funcSym.funcType = ttAssembler) then
      thisFunc := TFuncDecl.Create(funcName, params,
        asmBlock(funcName, funcSym.funcAddress <> '', retType,
        funcSym.funcAddress <> '', False), funcScope);

    SetLength(funcSym.params, Length(params));

    for i := Length(params) - 1 downto 0 do
    begin
      funcSym.params[i] := TVarSymbol(varSymbol(params[i].paramName,
        params[i].paramType));

      if (funcSym.funcType = ttFunc) or not isAsmParam(params[i].paramName,
        params[i].paramType, loReg, hiReg) then begin

          paramVar := TVarDecl.Create(params[i].paramName,params[i].paramType, '', '');
          if (LowerCase(params[i].paramType.symName) = 'pointer') then
          begin
            paramVar.isPointer := True;
            paramVar.address := '$' + IntToHex(getPointerAddress, 2);
          end;

          paramVar.isParam := True;

          thisFunc.block.declarations.insert(0, paramVar);
        end;
    end;

    if funcSym.funcType <> ttAssembler then begin
      resultVar := TVarDecl.Create('result',FcurrentScope.lookup(retValue),'','');
      resultVar.isParam := True;

      thisFunc.block.declarations.insert(0, resultVar);
    end;

    aBlock.declarations.Add(thisFunc);

    if funcSym.onStack and (funcSym.funcType <> ttAssembler) then
      generateStackInfo(funcSym,thisFunc.block);
  end;

  // restore previous scope
  FcurrentScope := funcScope.enclosingScope;
end;

function TParserAST.block(aBlockName: AnsiString; aRetType: TTokenType;
  aIsFunc: Boolean; aIsMacro: Boolean): TBlock;
var
  token : TToken;
  sym   : TSymbol;
begin
  Result := TBlock.Create;
  Result.blockName := aBlockName;
  Result.retType   := aRetType;
  Result.isFunc    := aIsFunc;
  Result.isMacro   := aIsMacro;

  if FcurrentScope.scopeLevel = 1 then
    FglobalBlock := Result;


  FsavedPntrTypes.Clear;

//  if aIsFunc then
//  begin
//    case aRetType of
//      ttByte:
//        sym := FcurrentScope.lookup('byte');
//      // ttWord  : FcurrentScope.insert(varSymbol());
//    end;
//
//    FcurrentScope.insert(varSymbol('result', sym));
//    // add result variable decl
//    Result.declarations.Add(TVarDecl.Create('result', sym, '', ''));
//  end;

  if not aIsMacro then
  begin
    repeat
      if accept(ttHash) then begin
        if accept(ttIdent,cSEGMENT) then
        segmentDecl(Result);
      end;

      if (accept(ttConst)) then begin
        constDecls(Result)
      end;

      if accept(ttImport) then begin
        parseImport(FprogAST, Result);
      end;

      if accept(ttType) then begin
        repeat
          typeDecls(Result);
        until Ftoken.tType <> ttIdent;
      end;

      if (accept(ttVar)) then begin
        repeat
          if (Ftoken.tType = ttVar) then
            nextToken;

          varDecls(Result);
        until (Ftoken.tType <> ttIdent) and (Ftoken.tType <> ttVar);
      end;

      while (Ftoken.tType in [ttProc, ttFunc, ttMacro]) do begin
        token := Ftoken;
        nextToken;
        case token.tType of
          ttProc:
            procDecl(Result);
          ttFunc:
            funcDecl(Result);
          ttMacro:
            macroDecl(Result);
        end;

        expect(ttSemiColon);
      end;
    until not(Ftoken.tType in [ttHash,ttConst, ttType, ttVar, ttProc, ttFunc, ttMacro,
      ttImport]);
  end;

  Result.compoundStatement := compound;
end;

function TParserAST.asmBlock(aBlockName: AnsiString;
  aIsAbsoluteAsmProc: Boolean; aRetType: TTokenType; aIsFunc: Boolean;
  aIsMacro: Boolean): TAsmBlock;
var
  i        : Integer;
  asmStart : Integer;
  asmEnd   : Integer;
  sym      : TSymbol;
  line     : AnsiString;
begin
  Result := TAsmBlock.Create;
  Result.blockName := aBlockName;
  Result.retType := aRetType;
  Result.isFunc := aIsFunc;
  Result.isMacro := aIsMacro;

//  if aIsFunc then
//  begin
//    case aRetType of
//      ttByte:
//        sym := FcurrentScope.lookup('byte');
//      // ttWord  : FcurrentScope.insert(varSymbol());
//    end;
//
//    FcurrentScope.insert(varSymbol('result', sym));
//    // add result variable decl
//    Result.declarations.Add(TVarDecl.Create('result', sym, '', ''));
//  end;
//
  if aIsAbsoluteAsmProc then
    Exit;

  if not aIsMacro then
  begin
    repeat
      if accept(ttHash) then begin
        if accept(ttIdent,cSEGMENT) then
        segmentDecl(Result);
      end;

      if (accept(ttConst)) then
      begin
        constDecls(Result)
      end;

      if accept(ttType) then
      begin
        repeat
          typeDecls(Result);
        until Ftoken.tType <> ttIdent;
      end;

      if (accept(ttVar)) then
      begin
        repeat
          if (Ftoken.tType = ttVar) then
            nextToken;

          varDecls(Result);
        until (Ftoken.tType <> ttIdent) and (Ftoken.tType <> ttVar);
      end;
    until not(Ftoken.tType in [ttHash,ttConst, ttType, ttVar]);
  end;

  expect(ttAsm);
  asmStart := Flexer.tokenRow;

  while (Ftoken.tType <> ttEOF) and (Ftoken.tType <> ttEnd) do
  begin
    nextToken;
  end;

  if (Ftoken.tType = ttEnd) then
    asmEnd := Flexer.tokenRow - 1;

  nextToken;

  for i := asmStart to asmEnd do begin
    line := Flines.Strings[i - 1];

    checkForVarUsageInAsmCode(line);

    Result.asmStatements.Add(line);
  end;
end;

procedure TParserAST.unitDecls(aProg: TProgram; aUnitName: AnsiString);
var
  unitBlock: TUnitBlock;
  token: TToken;
begin
  unitBlock := TUnitBlock.Create;
  unitBlock.blockName := aUnitName;

  aProg.unitBlocks.Add(unitBlock);

//  while (Ftoken.tType <> ttEOF) do
  begin
    repeat
      if accept(ttType) then
      begin
        repeat
          typeDecls(unitBlock);
        until Ftoken.tType <> ttIdent;
      end;

      if accept(ttImport) then
      begin
        parseImport(FprogAST, unitBlock);
      end;

      if (accept(ttConst)) then
      begin
        constDecls(unitBlock)
      end;

      if (accept(ttVar)) then
      begin
        repeat
          if (Ftoken.tType = ttVar) then
            nextToken;

          varDecls(unitBlock);
        until (Ftoken.tType <> ttIdent) and (Ftoken.tType <> ttVar);
      end;

      if (Ftoken.tType in [ttProc, ttFunc, ttMacro]) then
      begin
        token := Ftoken;
        nextToken;
        case token.tType of
          ttProc:
            procDecl(unitBlock);
          ttFunc:
            funcDecl(unitBlock);
          ttMacro:
            macroDecl(unitBlock);
        end;

        expect(ttSemiColon);
      end;
      // else
      // nextToken;
    until not(Ftoken.tType in [ttType, ttConst, ttVar, ttProc, ttFunc, ttMacro,
      ttImport]);

//    if Ftoken.tType = ttSemiColon then
//      break;
  end;
end;

procedure TParserAST.doUnit;
var
  unitName  : AnsiString;
begin
  expect(ttUnit);

  unitName := Ftoken.tValue;

  FcurrentFile := unitName+'.pas';

  expect(Ftoken.tType);
  expect(ttSemiColon);

  if accept(ttUses) then
    parseUsesClause(FprogAST);

  unitDecls(FprogAST, unitName);
end;

procedure TParserAST.loadUnit(aProg: TProgram; aUnitName: AnsiString);
var
  unitFileName : AnsiString;
  parserPath   : AnsiString;
  parser       : TParserAST;
  unitSym      : TUnitSymbol;
  srcOffset    : Integer;
  srcFile      : String;
begin
  unitSym := TUnitSymbol.Create(aUnitName);

  FcurrentScope.insert(unitSym);

  parserPath := ExtractFilePath(ParamStr(0));

  unitFileName := FprojectPath + aUnitName + '.pas';

  if not FileExists(unitFileName) then
    unitFileName := parserPath + 'include\' + aUnitName + '.pas';

  if not FileExists(unitFileName) then
    error('Unit "' + aUnitName + '" not found');

  unitSym.setSourceInfo(0,unitFileName);

  parser := TParserAST.Create(FCodeInsightData,FonScopeUpdate);
  try
    parser.pointerAddress := FpointerAddress;

    parser.projectPath := projectPath;
    if not parser.parseUnit(aProg, FcurrentScope, unitFileName) then
      raise TParseException.Create(parser.errorMsg);

    FpointerAddress := parser.pointerAddress;
  finally
    parser.Free;
  end;
end;

procedure TParserAST.parseUsesClause(aProg: TProgram);
var
  unitName: AnsiString;
  unitSym: TSymbol;
begin
  if (Ftoken.tType in TARGET_TYPE) then
  begin
    if aProg.target <> Ftoken.tType then
      error('Unit "' + Ftoken.tValue + '" does not match target type');
  end;

  unitName := Ftoken.tValue;

  expect(Ftoken.tType);

  unitSym := FcurrentScope.lookup(unitName);

  if (unitSym = nil) then
    // unit not loaded yet so do so
    loadUnit(aProg, unitName);

  while accept(ttComma) do
  begin
    if (Ftoken.tType in TARGET_TYPE) then
    begin
      if aProg.target <> Ftoken.tType then
        error('Machine type unit missmatch');
    end;

    unitName := Ftoken.tValue;

    expect(ttIdent);

    unitSym := FcurrentScope.lookup(unitName);

    if (unitSym = nil) then
      // unit not loaded yet so do so
      loadUnit(aProg, unitName);
  end;

  expect(ttSemiColon);
end;

procedure TParserAST.doSegments;
var
  segInfo : TSegmentInfo;
  name    : AnsiString;
  value   : AnsiString;

  procedure parseValue;
  begin
    name := Ftoken.tValue
  end;
begin
  expect(ttLParen);
    while Ftoken.tType = ttIdent do begin
      segInfo := TSegmentInfo.Create;

      segInfo.name := Ftoken.tValue; nextToken;
      expect(ttColon);

      while Ftoken.tType = ttIdent do begin
        if Ftoken.tValue = 'start' then begin

        end;

      end;

      FprogAST.segments.add(segInfo);
    end;
  expect(ttRParen);
end;

procedure TParserAST.doCfgFile;
var
  cfgName : AnsiString;
begin
  clearSegments;

  while true do begin
    if accept(ttIdent,'segments') then begin

    end
    else
      Break;
  end;
end;

function TParserAST.parseCfg(aProgram: TProgram; aScope: TscopedSymbolTable;
  aFileName: AnsiString): Boolean;
var
  fs: TFileStream;
  buffer: AnsiString;
  OldFile : AnsiString;
begin
  Result := False;

  FparsingUnit := True;
  FprogAST := aProgram;
  FcurrentScope := aScope;

  fs := TFileStream.Create(aFileName, fmOpenRead or fmShareDenyNone);
  try
    try
      SetLength(buffer, fs.size);

      fs.Seek(0, soFromBeginning);
      fs.Read(buffer[1], fs.size);

      Flines.Text := buffer;

      Flexer.init(fs);

      Ftoken := Flexer.getNextToken;

      oldFile := FcurrentFile;

      if fs.Size > 0 then
        doCfgFile;

      FcurrentFile := oldFile;

      Result := True;
    except
      on e: Exception do
      begin
        FerrorMsg := aFileName + ': ' + e.Message;
      end;
    end;
  finally
    fs.Free;
  end;
end;

procedure TParserAST.loadCfgFile(aProg: TProgram; aCfgName: AnsiString);
var
  cfgFileName : AnsiString;
  parserPath  : AnsiString;
  parser      : TParserAST;
  unitSym     : TUnitSymbol;
begin
  unitSym := TUnitSymbol.Create(cfgFileName);
  FcurrentScope.insert(unitSym);

  parserPath := ExtractFilePath(ParamStr(0));

  cfgFileName := FprojectPath + aCfgName;

  if not FileExists(cfgFileName) then
    error('Config file "' + cfgFileName + '" not found');

  parser := TParserAST.Create(FCodeInsightData,FonScopeUpdate);
  try
    parser.pointerAddress := FpointerAddress;

    parser.projectPath := projectPath;
    if not parser.parseCfg(aProg, FcurrentScope, cfgFileName) then
      raise TParseException.Create(parser.errorMsg);

    FpointerAddress := parser.pointerAddress;
  finally
    parser.Free;
  end;
end;

function foundMacroInfo(aLine: String; var aMacroName: AnsiString;
  var aParamCount: Integer): Boolean;
const
  cHeader = '.macro';

var
  p: Integer;
  lPos, rPos: Integer;
  splitted: TArray<String>;
begin
  Result := False;

  p := Pos(cHeader, aLine);

  if p <= 0 then
    Exit;

  Result := True;

  aLine := Trim(Copy(aLine, p + Length(cHeader), Length(aLine)));

  lPos := Pos('(', aLine);
  rPos := Pos(')', aLine);

  aMacroName := Trim(Copy(aLine, 1, Pos('(', aLine) - 1));

  aParamCount := 0;

  if (rPos > lPos + 1) then
  begin
    aLine := StringReplace(Trim(Copy(aLine, lPos + 1, rPos - lPos - 1)), ' ',
      '', [rfReplaceAll]);

    splitted := aLine.Split([',']);

    aParamCount := Length(splitted);
  end;
end;

procedure TParserAST.extractMacroNamesFromSource(aAsmFileName: AnsiString);
var
  macroName: AnsiString;
  asmFileName: AnsiString;
  parser: TParserAST;
  paramCount: Integer;
  macroSym: TMacroSymbol;
  line: AnsiString;
  i: Integer;
begin
  for i := 0 to Flines.count - 1 do
  begin
    line := Flines.Strings[i];

    if foundMacroInfo(line, macroName, paramCount) then
    begin
      macroSym := TMacroSymbol.Create(macroName, []);
      macroSym.paramCount := paramCount;

      FcurrentScope.insert(macroSym);
    end;
  end;
  { while Ftoken.tType <> ttEOF do begin
    if accept(ttPeriod) then begin
    if accept(ttMacro) then begin
    macroName := Ftoken.tValue;
    writeLn('Found macro: "'+macroName+'"');
    expect(ttIdent);

    paramCount := 0;
    expect(ttLParen);

    if Ftoken.tType <> ttRParen then begin
    Inc(paramCount);
    nextToken;
    while accept(ttComma) do begin
    Inc(paramCount);
    nextToken;
    end;
    end;

    expect(ttRParen);

    macroSym := TMacroSymbol.Create(macroName,[]);
    macroSym.paramCount := paramCount;

    FcurrentScope.insert(macroSym);
    end
    end;

    nextToken;
    end; }
end;

procedure TParserAST.parseImport(aProg: TProgram; aBlock: TBlock);
var
  asmFileName: AnsiString;
  parserPath: AnsiString;
  parser: TParserAST;
begin
  asmFileName := Ftoken.tValue;

  expect(ttString);
  expect(ttSemiColon);

  parserPath := ExtractFilePath(ParamStr(0));

  if not FileExists(asmFileName) then
  begin
    asmFileName := FprojectPath + ExtractFileName(asmFileName);

    if not FileExists(asmFileName) then
    begin
      asmFileName := parserPath + 'include\' + ExtractFileName(asmFileName);

      if not FileExists(asmFileName) then
        error('Import: Source "' + ExtractFileName(asmFileName) +
          '" not found');
    end;
  end;

  parser := TParserAST.Create(FcodeInsightData,FonScopeUpdate);
  try
    parser.projectPath := projectPath;
    if not parser.parseAsmFile(aProg, FcurrentScope, asmFileName) then
      raise TParseException.Create(parser.errorMsg);
  finally
    parser.Free;
  end;

  aBlock.declarations.Add(TImportSource.Create(asmFileName));
end;

procedure TParserAST.registerImportFile(aProg: TProgram; aFileName: AnsiString);
var
  parser: TParserAST;
  asmFileName: AnsiString;
begin
  asmFileName := ExtractFilePath(ParamStr(0)) + 'include\' +
    ExtractFileName(aFileName);

  parser := TParserAST.Create(FcodeInsightData,FonScopeUpdate);
  try
    parser.projectPath := projectPath;
    if not parser.parseAsmFile(aProg, FcurrentScope, asmFileName) then
      raise TParseException.Create(parser.errorMsg);
  finally
    parser.Free;
  end;

  aProg.imports.Add(asmFileName);
end;

procedure TParserAST.parseImportBinary(aConstName: AnsiString; aBlock : TBlock);
var
  importName: AnsiString;
  constType: TSymbol;
  constSym: TConstSymbol;
  procSym: TProcSymbol;
  count: Integer;
  binary: TImportBinary;
  e: TAST;
  fs: TFileStream;
  path: AnsiString;
begin
  {
    *=$a000 "Titlescreen Music"
    titleMusic:
    .import binary "audio\ALBINO_4.sid", $7e // get rid of sid header (skip $7e bytes)
  }

  binary := TImportBinary.Create;
  binary.name := aConstName;
  binary.address := '';
  binary.byteSkip := '0';

  expect(ttLParen);

  binary.fileName := Ftoken.tValue;

  binary.isSid := Pos('.sid', LowerCase(binary.fileName)) > 0;

  path := binary.fileName;

  if not TPath.IsPathRooted(path) then
    path := FprojectPath + path;

  path := StringReplace(path,#13,'',[rfReplaceAll]);
  path := StringReplace(path,#10,'',[rfReplaceAll]);

  if not FileExists(path) then
    binary.fileSize := 0
  else begin
    fs := TFileStream.Create(path, fmOpenRead);
    try
      binary.fileSize := fs.size;
    finally
      fs.Free;
    end;
  end;

  expect(ttString);

  if accept(ttComma) then
  begin
    e := simplify(simpleExpression);

    if not e.isInteger then
      error('Import binary: byte skip value must be a number');

    binary.byteSkip := TNumber(e).token.tValue;
  end;

  expect(ttRParen);

  if accept(ttAbsolute) then
  begin
    e := simplify(simpleExpression);

    if not e.isInteger then
      error('Import binary: absolute address value must be a number');
    binary.address := TNumber(e).token.tValue;
  end;

  constType := FcurrentScope.lookup('byte');

  constSym := TConstSymbol.Create(aConstName, constType);
  constSym.isArray := True;
  constSym.isBinaryFile := True;
  constSym.len := binary.fileSize;
  constSym.uses8BitIndex := constSym.len <= 256;

  FcurrentScope.insert(constSym);

//  // add .size parameter for this bin
//  constSym := TConstSymbol.Create(aConstName + '.size',
//    FcurrentScope.lookup('word'),2);
//
//  constSym.value := Token(ttInteger,Word(binary.fileSize));
//
//  FcurrentScope.insert(constSym);

  if binary.isSid then
  begin
    // do sid stuff for this bin
    procSym := TProcSymbol.Create(aConstName + '.init', []);
    procSym.procType := ttImportBinary;
    procSym.procAddress := '';

    FcurrentScope.insert(procSym);

    procSym := TProcSymbol.Create(aConstName + '.play', []);
    procSym.procType := ttImportBinary;
    procSym.procAddress := '';

    FcurrentScope.insert(procSym);
  end;

  if binary.address = '' then
    aBlock.declarations.Add(binary)
//    FprogAST.binImports.Add(binary)
  else
    FprogAST.deferredBinImports.Add(binary);

  expect(ttSemiColon);
end;

procedure TParserAST.addConst(name,value : AnsiString);
var
  constValue : TToken;
  sym        : TConstSymbol;
begin
  constValue := uToken.Token(ttIntNum,value);

  sym := TConstSymbol.Create(name, constValue,nil);
  sym.isSigned := false;

  FcurrentScope.insert(sym,true);
end;

procedure TParserAST.addVar(name,aType : AnsiString);
var
  sym     : TVarSymbol;
  varType : TSymbol;
begin
  varType := FcurrentScope.lookup(aType);

  sym := TVarSymbol.Create(name, varType);
  sym.isSigned := varType.isSigned;

  FcurrentScope.insert(sym,true);
end;

procedure TParserAST.copyImportsToProjectFolder;
const
  cImportNames : array[0..1] of AnsiString = ('.asm','.ini');

var
  i : Integer;
  srcFile : AnsiString;
  dstFile : AnsiString;
  a       : DWord;
  name    : AnsiString;
  f       : TStringList;
begin
  name := ExtractFilePath(ParamStr(0))+'include\platform\'+getTargetName(FprogAST.target)+'\'+getTargetName(FprogAST.target);

  for i := 0 to High(cImportNames) do begin
    srcFile := name + cImportNames[i];

    if FileExists(srcFile) then begin
      dstFile := ExtractFilePath(projectPath)+ExtractFileName(srcFile);

      f := TStringList.Create;
      try
        f.LoadFromFile(srcFile);
        f.SaveToFile(dstFile);
      finally
        f.Free;
      end;
    end;
  end;
end;
procedure TParserAST.importTargetIniData;
var
  fileName : AnsiString;
  sections : TStringList;
  section  : TStringList;
  iniFile  : TIniFile;
  s,i,p    : Integer;
  name     : AnsiString;
  value    : AnsiString;
  recName  : AnsiString;
begin
  fileName := ExtractFilePath(projectPath)+getTargetName(FprogAST.target)+'.ini';

  if not FileExists(fileName) then Exit;

  iniFile := TIniFile.Create(fileName);
  try
    sections := TStringList.Create;
    section  := TStringList.Create;
    try
      iniFile.ReadSections(sections);

      for s := 0 to sections.Count - 1 do begin
      // read section and parse contents
        if sections.Strings[s] = 'constants' then begin
          iniFile.ReadSection(sections.Strings[s],section);

          for i := 0 to section.Count - 1 do begin
            value := iniFile.ReadString(sections.Strings[s],section.Strings[i],'');

            AddConst(section.Strings[i],value);
          end;
        end
        else begin
          // must be record data
          recName := sections.Strings[s];

          iniFile.ReadSection(sections.Strings[s],section);

          for i := 0 to section.Count - 1 do begin
            name  := section.Strings[i];
            value := iniFile.ReadString(sections.Strings[s],section.Strings[i],'');

            AddVar(recName+'.'+name,value);
          end;
        end;
      end;
    finally
      sections.Free;
      section.Free;
    end;
  finally
    iniFile.Free;
  end;
end;

function  appendPlatformPath(aProg : TProgram; afileName : AnsiString) : AnsiString;
var
  fullPath : AnsiString;
begin
  Result := aFileName;

  if not FileExists(ExtractFilePath(aProg.fileName)+aFileName) then begin
    fullPath := ExtractFilePath(ParamStr(0))+'include\platform\'+getTargetName(aProg.target)+'\'+aFileName;

    if FileExists(fullPath) then
      Result := fullPath;
  end;
end;

procedure TParserAST.getNES_segmentsInfo;
begin
  expect(ttImportSegments);
  expect(ttLParen);
  FprogAST.SegmentsFileName := appendPlatformPath(FprogAST,Ftoken.tValue);
  expect(ttString);
  expect(ttRParen);
  expect(ttSemiColon);
end;

procedure TParserAST.getNES_headerInfo;
begin
  expect(ttImportHeader);
  expect(ttLParen);
  FprogAST.HeaderFileName := appendPlatformPath(FprogAST,Ftoken.tValue);
  expect(ttString);
  expect(ttRParen);
  expect(ttSemiColon);
end;

procedure TParserAST.getNES_IRQinfo;
begin
  expect(ttSetIRQsegment);
  expect(ttLParen);
  FprogAST.IRQsegment := Ftoken.tValue;
  expect(ttString);
  expect(ttRParen);
  expect(ttSemiColon);
end;

procedure TParserAST.getNES_PRGROMinfo;
begin
  expect(ttSetPRGROMsegment);
  expect(ttLParen);
  FprogAST.PRGROMsegment := Ftoken.tValue;
  expect(ttString);
  expect(ttRParen);
  expect(ttSemiColon);
end;

function TParserAST.doProgram: TProgram;
var
  progName    : AnsiString;
  globalScope : TscopedSymbolTable;

  function getInteger(name : AnsiString) : Integer;
  var
    e : TAST;
  begin
    e := Simplify(simpleExpression);
    if not e.isInteger then
      error('NES Header: "'+name+'" value must be an integer');
    Result := TNumber(e).value;
  end;
begin
  expect(ttProgram);

  uASM.resetFuncResultReg;

  progName := Ftoken.tValue;

  expect(ttIdent);

  expect(ttSemiColon);

  globalScope := TscopedSymbolTable.Create(progName, 1, nil);
  try
    FcurrentScope := globalScope;

    Result := TProgram.Create(progName, nil, globalScope);

    FprogAST := Result;

    // check for 'target = <target type>;'
    // where <target type> = c64,c128,bbc
    // default to c64

    FprogAST.target := ttC64;

    if accept(ttTarget) then
    begin
      expect(ttEql);
      if not(Ftoken.tType in TARGET_TYPE) then
        error('Unsupported machine type "' + Ftoken.tValue + '"');

      FprogAST.target := Ftoken.tType;
      expect(Ftoken.tType);
      expect(ttSemiColon);
    end;

    copyImportsToProjectFolder;
    importTargetIniData;

    if FprogAST.target = ttNES then begin
    // NES target needs segment, header, IRQ and PRGROM info
    // so get those
      while Ftoken.tType in [
        ttImportSegments,
        ttImportHeader,
        ttSetIRQsegment,
        ttSetPRGROMsegment
      ] do
      begin
        case Ftoken.tType of
          ttImportSegments   : getNES_segmentsInfo;
          ttImportHeader     : getNES_headerInfo;
          ttSetIRQsegment    : getNES_IRQinfo;
          ttSetPRGROMsegment : getNES_PRGROMinfo;
        end;
      end;
    end;


    FprogAST.loadAddress := '';

    if FprogAst.target <> ttNES then begin
      // check for '* = <load addressaddress>;'
      if accept(ttTimes) then
      begin
        // parse load address value for project
        expect(ttEql);
        FprogAST.loadAddress := StrIntToHex(Ftoken.tValue);
        expect(ttIntNum);

        expect(ttSemiColon);
      end;
    end;

//    if accept(ttHash) then begin
//      if accept(ttIdent,cSEGMENT) then
//      segmentDecl(Result);
//    end;
//
    registerImportFile(Result, '6502_rtl.asm');
    registerImportFile(Result, 'stack_rtl.asm');

    if (FprogAST.target in MACHINE_ACORN_TYPES) and (FprogAST.loadAddress = '') or
       (FprogAST.target in MACHINE_APPLEII_TYPES) and (FprogAST.loadAddress = '')
    then
      error('This machine type must include a load address "* = <load address>;"');

    loadUnit(Result,'rtl_stack');
    loadUnit(Result,'rtl_builtInRoutines');

    if accept(ttUses) then
      parseUsesClause(Result);

    Result.block := block(progName, ttByte, False, False);

    expect(ttPeriod);
  finally
  end;
end;

function TParserAST.parse(aStream: TStream; aParsingUnit: Boolean): Boolean;
var
  buffer: AnsiString;
begin
  Result := False;

  FcurrentFileFull := FcurrentFile;

  SetLength(buffer, aStream.size);

  aStream.Seek(0, soFromBeginning);
  aStream.Read(buffer[1], aStream.size);

  Flines.Text := buffer;

  Flexer.init(aStream);

  Ftoken := Flexer.getNextToken;

  try
    FerrorMsg := '';

    FprogAST := doProgram;

    Result := True;
  except
    on e: Exception do
    begin
      FerrorMsg := e.Message;
    end;
  end;
end;

function TParserAST.parse(aFileName: AnsiString; aParsingUnit: Boolean)
  : Boolean;
var
  fs: TFileStream;
begin

  fs := TFileStream.Create(aFileName, fmOpenRead);
  try
    Result := parse(fs, aParsingUnit);
  finally
    fs.Free;
  end;
end;

function TParserAST.parseUnit(aProgram: TProgram; aScope: TscopedSymbolTable;
  aFileName: AnsiString): Boolean;
var
  fs: TFileStream;
  buffer: AnsiString;
  OldFile : AnsiString;
begin
  Result := False;

  FparsingUnit := True;
  FprogAST := aProgram;
  FcurrentScope := aScope;
  FcurrentFileFull := aFileName;

  fs := TFileStream.Create(aFileName, fmOpenRead or fmShareDenyNone);
  try
    try
      SetLength(buffer, fs.size);

      fs.Seek(0, soFromBeginning);
      fs.Read(buffer[1], fs.size);

      Flines.Text := buffer;

      Flexer.init(fs);

      Ftoken := Flexer.getNextToken;

      oldFile := FcurrentFile;

      if fs.Size > 0 then
        doUnit;

      FcurrentFile := oldFile;

      Result := True;
    except
      on e: Exception do
      begin
        FerrorMsg := aFileName + ': ' + e.Message;
      end;
    end;
  finally
    fs.Free;
  end;
end;

function TParserAST.parseAsmFile(aProgram: TProgram; aScope: TscopedSymbolTable;
  aFileName: AnsiString): Boolean;
var
  fs: TFileStream;
  buffer: AnsiString;
begin
  Result := False;

  FprogAST := aProgram;
  FcurrentScope := aScope;

  fs := TFileStream.Create(aFileName, fmOpenRead);
  try
    try
      SetLength(buffer, fs.size);

      fs.Seek(0, soFromBeginning);
      fs.Read(buffer[1], fs.size);

      Flines.Text := buffer;

      Flexer.init(fs);

      Ftoken := Flexer.getNextToken;

      extractMacroNamesFromSource(aFileName);

      Result := True;
    except
      on e: Exception do
      begin
        FerrorMsg := aFileName + ': ' + e.Message;
      end;
    end;
  finally
    fs.Free;
  end;
end;

end.

