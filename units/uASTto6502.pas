unit uASTto6502;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  Winapi.Windows,Vcl.StdCtrls,System.Classes,uToken,uAST,uSymbolTable,usesStringWriter,uThreeAddressCode,
  uCodeGen_6502,uPeepholeOptimiser;

const
  cMAX_TEMP_REGS_VARIABLE = 3;
  cTEMP_REGS_VARIABLE     = '_TEMP_REG_VAR_';
  cTEMP_IF_VARIABLE       = '_TEMP_IF_VAR';

  cTEMP                   = '_TEMP_';

  cTEMP_POINTER              = '$fb';

  APPLE = [
    ttAppleII
  ];

  ATARI_8BIT = [
    ttAtari8Bit
//    ttAtari400,
//    ttAtari800,
//    ttAtari1200XL,
//    ttAtari5200,
//    ttAtariXEGS,
//    ttAtariXL,
//    ttAtariXE
  ];

type
  TExpToken = record
    token : TToken;
    size  : Integer;
  end;

  TTokenList = array of TExpToken;

  TASTto6502 = class(TInterfacedObject,IASTVisitor)
  private
    FlabelIndex         : Integer;
    Fwriter             : TsesStringWriter;
    Fwarnings           : TStringList;
    FasmStream          : TStream;
    FroutinesList       : TStringList;
    FcurrentScope       : TscopedSymbolTable;

    FcodeGen            : TCodeGen_6502;

    FflattenExpression  : Boolean;
    Fexpression         : TList;

    FfillExpTokens      : Boolean;
    FexpTokens          : TTokenList;
    FblockIRQType       : TTokenType;
    FfoundEndIrq        : Boolean;
    FloopExitLabelCount : Integer;
    FloopExitLabels     : array[0..100] of AnsiString;
    FtempPointer        : AnsiString;
    // is true if is an if/then or loop condition
    FdoingCondition     : Boolean;

    // use temps of size 2 when generating TAC list,
    // othewise use temps of size 1
    FuseLargeTempVars : Boolean;
    FfragmentDir      : AnsiString;

    FcodeOptimiser : TPeepholeOptimiser;
    Fprog          : TProgram;

    procedure emitWarning(msg : AnsiString; const aArgs: array of const);

    procedure saveRegVarInfo(src : TAST; loReg,hiReg : AnsiString);
    procedure emitSaveRegVars();
    procedure resetTokens;
    procedure resetExpression;
    procedure pushToken(aToken : TToken; aSize : Integer);
    procedure pushExpNode(node : TAST);
    function  newLabel : AnsiString;
    procedure expandStack(size : Integer);
    procedure shrinkStack(size : Integer);
    procedure emitLabel(aLabel : AnsiString);
    procedure emitPeek(peekNode : TTACNode);
    procedure emitCode(aCode : AnsiString);
    procedure emitTACcode(aTacList : TTACList);

    function  flattenExpression(node : TAST; aExpression : TList = nil) : TList;
    procedure debugExpression;

    procedure tokenizeExpression(node : TAST; aResetTokens : Boolean);
    procedure emitExpression(node : TAST; falseLabel : AnsiString = '');

    procedure emitCompare_16(left,right : TExpToken; lower,same,higher : Boolean; falseLabel : AnsiString);

//    procedure emitRecordVarDecl(varDecl : TVarDecl; varAddress : AnsiString; varLevel : Integer; isRecordVar : Boolean);
    procedure emitRecordVarDecl2(aVarDecl: TVarDecl; aVarAddress : AnsiString);
    procedure emitVarDecl(varDecl : TVarDecl);
    procedure emitBinOp(left,right,op : TExpToken);
    procedure emitCondition(expr : TAST; falseLabel : AnsiString);
    function  isSimpleAdd2(node : TAssign; symSize : Integer) : Boolean;
    function  isSimpleSub2(node : TAssign; symSize : Integer) : Boolean;
    function  isSimpleIncrement(node : TAssign; symSize : Integer) : Boolean;
    function  isSimpleDecrement(node : TAssign; symSize : Integer) : Boolean;
    function  isSHL(node : TAssign; var shiftCount : Integer) : Boolean;
    function  isSHR(node : TAssign; var shiftCount : Integer) : Boolean;
    procedure write16BitNumber;
    procedure writeInteger(aWriter : TsesStringWriter; aInt : Integer; aSize : Integer);
    procedure writeVariable(aWriter : TsesStringWriter; v : TAST);
    procedure writeString(aWriter : TsesStringWriter; aString : AnsiString);
    procedure doWrite(node : TBuiltinProcedure; carridgeReturn : Boolean);
//    function  isSimpleReadWriteIndexedPointer(aSrc,aDst : TAST) : Boolean;
    procedure pushLoopExitLabel(exitLabel : AnsiString);
    procedure popLoopExitLabel;
    function  peekLoopExitLabel : AnsiString;
    procedure outputTempVars;
    procedure outputMainCode;
    procedure outputNESsegments;
    procedure outputNESIRQs;
    procedure doNES;
//----------------------------
// AST Visitor methods
//----------------------------
    procedure visitProgram  (node : TProgram);
    procedure visitBlock    (node : TBlock);
    procedure visitAsmBlock (node : TAsmBlock);
    procedure visitUnitBlock(node : TUnitBlock);
    procedure visitPoke     (node : TPoke);
    procedure visitAssign   (node : TAssign);
    procedure visitCall     (node : TCall);
    procedure visitMacroCall(node : TMacroCall);
    procedure visitFuncCall (node : TFuncCall);
    procedure visitIfThen   (node : TIfThen);
    procedure visitWhile    (node : TWhile);
    procedure visitRepeat   (node : TRepeat);
    procedure visitFor      (node : TFor);
    procedure visitNumber   (node : TNumber);
    procedure visitVariable (node : TVariable);
    procedure visitString   (node : TString);
    procedure visitBinOp    (node : TBinOp);
    procedure visitUnaryOp  (node : TUnaryOp);
    procedure visitCompound (node : TCompound);
    procedure visitAsmCompound(node : TAsmCompound);
    procedure visitVarDecl  (node : TVarDecl);
    procedure visitConstDecl(node : TConstDecl);
    procedure visitProcDecl (node : TProcDecl);
    procedure visitMacroDecl(node : TMacroDecl);
    procedure visitFuncDecl (node : TFuncDecl);
    procedure visitBuiltinProcedure(node : TBuiltinProcedure);
    procedure visitBuiltinFunction(node : TBuiltinFunction);
    procedure visitImportBinary(node : TImportBinary);
    procedure visitAbsoluteArrayData(node : TAbsoluteArrayData);
    procedure visitImportSource(node : TImportSource);
    procedure visitExit(node : TExit);
    procedure visitBreak(node: TBreak);
    procedure visitSetupRasterIRQ(node : TSetupRasterIRQ);
    procedure visitEndIRQ(node : TEndIRQ);
    procedure visitCallParam(param : TParam);
    procedure visitDecimalMode(decMode : TDecimalMode);
    procedure visitSegmentDecl(node : TSegmentDecl);

    procedure onAsmWrite(aAsmCode : AnsiString);

    procedure copyResultToRegs(funcSym : TFuncSymbol);
    procedure emitStackInfo;
    function  expandFuncCall(aFuncCall : TFuncCall; aTacList : TTACList) : TAST;
    function  arrayIndexCanBe8Bit(valueAST : TAST) : Boolean;
    function  generateIndexShiftTAC(aIndex : TAST; aTacList : TTACList; use8BitValue : Boolean = false) : TAST;
    procedure generateTAC(node : TAST; aTacList : TTACList; use8BitValue : Boolean = False);           overload;
    function  generateTAC(node : TAST; dstVar : TVariable) : TTACList; overload;
//-----------------------------
  public
    constructor Create;
    destructor  Destroy; override;

    procedure genCode(aNode : TAST; aAsmStream : TStream; aRoutinesList : TStringList; var aWarnings : AnsiString);
  end;

  TTempRegVar = class(TInterfacedObject)
  public
    src   : TAST;
    loReg : AnsiString;
    hiReg : AnsiString;

    constructor create(src : TAST; loReg,hiReg : AnsiString);
  end;

implementation

uses
  System.Math,System.SysUtils,uASM,uFragments;

const
  MAIN_LABEL  = '_main_';
  START_LABEL = '_start_';

var
  indentValue    : Integer = 0;

  tempRegVarIndex : Byte = 0;

  tempRegVars : array of TTempRegVar;

function isRelop(const aTokenType: TTokenType): Boolean;
begin
  Result := aTokenType in [ttEql, ttNeq, ttLss, ttLeq, ttGtr, ttGeq];
end;

procedure resetTempRegVar();
var
  i : Integer;
begin
  for i := 0 to Length(tempRegVars) - 1 do begin
    tempRegVars[i] := nil;
  end;

  SetLength(tempRegVars,0);
  tempRegVarIndex := 0;
end;

function getTempRegVar() : AnsiString;
begin
  Result := cTEMP_REGS_VARIABLE + IntToStr(tempRegVarIndex);
  Inc(tempRegVarIndex);
end;

procedure decIndent;
begin
  dec(indentValue,2);
end;

procedure incIndent;
begin
  inc(indentValue,2);
end;

procedure setIndent(v : Integer);
begin
  indentValue := v;
end;

function  indent : AnsiString;
begin
  Result := StringOfChar(' ',indentValue);
end;

function  leafNodeToImmOrAddr(node : TExpToken) : AnsiString;
begin
  if (node.token.tType = ttIntNum) then begin
    // is number/immediate
    Result := '#'+node.token.tValue;
  end else begin
    // is variable/absolute
    Result := node.token.tValue;
  end;
end;

function  getLeafNodeLo(node : TExpToken) : AnsiString;
begin
  if (node.token.tType = ttIntNum) then begin
    // is number/immediate
    Result := '#<'+node.token.tValue;
  end else begin
    // is variable/absolute
    Result := node.token.tValue + ' + 0';
  end;
end;

function  getLeafNodeHi(node : TExpToken) : AnsiString;
begin
  if node.size > 1 then begin
    if (node.token.tType = ttIntNum) then begin
      // is number/immediate
      Result := '#>'+node.token.tValue;
    end else begin
      // is variable/absolute
      Result := node.token.tValue + ' + 1';
    end;
  end
  else
  //  return 0 as no high byte
    Result := '#0';
end;

constructor TTempRegVar.create(src: TAST; loReg: AnsiString; hiReg: AnsiString);
begin
  self.src   := src;
  self.loReg := loReg;
  self.hiReg := hiReg;
end;

constructor TASTto6502.Create;
begin
  FcurrentScope       := nil;
  Fexpression         := TList.Create;
  FlabelIndex         := 0;
  FloopExitLabelCount := 0;

  FfillExpTokens      := False;
  FflattenExpression  := False;

  Fwarnings := TStringList.Create;

  FtempPointer := cTEMP_POINTER;

  FdoingCondition := False;

  FfragmentDir := '6502\';
end;

destructor  TASTto6502.Destroy;
begin
  Fexpression.Free;
  Fwarnings.Free;
end;

procedure TASTto6502.emitWarning(msg : AnsiString; const aArgs: array of const);
begin
  Fwarnings.add(Format(msg,aArgs));
//  writeLn(Format(msg,aArgs));
end;

procedure TASTto6502.saveRegVarInfo(src : TAST; loReg,hiReg : AnsiString);
var
  i : Integer;
begin
  i := Length(tempRegVars);
  SetLength(tempRegVars,i + 1);
  tempRegVars[i] := TTempRegVar.create(src,loReg,hiReg);
end;

procedure TASTto6502.emitSaveRegVars();
var
  i : Integer;
begin
  for i := 0 to length(tempRegVars) - 1 do begin
    FcodeGen.loadRegister(tempRegVars[i].src,tempRegVars[i].loReg,tempRegVars[i].hiReg);
    tempRegVars[i] := nil;
  end;

  // reset again
  SetLength(tempRegVars,0);
end;

procedure TASTto6502.resetTokens;
begin
  SetLength(FexpTokens,0);
end;

procedure TASTto6502.resetExpression;
begin
  Fexpression.Clear;
end;

procedure TASTto6502.pushToken(aToken : TToken; aSize : Integer);
var
  i : Integer;
begin
  i := Length(FexpTokens);
  SetLength(FexpTokens,i + 1);
  FexpTokens[i].token := aToken;
  FexpTokens[i].size  := aSize;
end;

procedure TASTto6502.pushExpNode(node : TAST);
begin
  Fexpression.Add(node);
end;

procedure TASTto6502.emitPeek(peekNode : TTACNode);
begin
  writeLn(peekNode.left.toString,' <- ',TAST(TPeek(peekNode.right1).params[0]).toString);
end;

function  TASTto6502.newLabel : AnsiString;
begin
  Result := 'L'+IntToStr(FlabelIndex);
  Inc(FlabelIndex);
end;

procedure TASTto6502.emitLabel(aLabel : AnsiString);
begin
  decIndent;
  Fwriter.writeLn(aLabel+':');
  incIndent;
end;

procedure TASTto6502.emitCode(aCode : AnsiString);
begin
  FcodeOptimiser.emitCode(indent+aCode);
//  Fwriter.writeLn(indent+aCode);
end;

procedure TASTto6502.expandStack(size : Integer);
begin
  emitCode(':expandStack('+IntToStr(size)+')');
end;

procedure TASTto6502.shrinkStack(size : Integer);
begin
  emitCode(':shrinkStack('+IntToStr(size)+')');
end;

procedure TASTto6502.emitTACcode(aTacList : TTACList);
var
  i,j          : Integer;
  node         : TTACNode;
  done         : Boolean;
  reg          : AnsiString;
  regVar       : TAST;
  funcResult   : TAST;
  fragmentName : AnsiString;
  fragment     : AnsiString;
  fragmentFile : AnsiString;
  fList        : TStringList;
  sym          : TSymbol;
begin
  FcodeGen.indentValue := indentValue;

  for i := 0 to aTacList.Count - 1 do begin
    done := False;
    node := aTacList.Items[i];

//    if node.right1 = nil then node.right1 := TVariable.Create(cA_REGISTER,1);

    if node.getFragmentName(fragmentName,FcurrentScope,FdoingCondition) then begin
      fragmentFile := ExtractFilePath(ParamStr(0))+'fragments\'+FfragmentDir+fragmentName;

      if not fileExists(fragmentFile) then begin
        Fwriter.WriteLn('//');
        Fwriter.WriteLn(format('// fragment "%s" doesn''t exist, you need to define it...',[fragmentFile]));
        Fwriter.WriteLn('// '+node.toString);
        Fwriter.WriteLn('//');
      end
      else begin
        loadFragment(FfragmentDir,fragmentName,fragment);
        fillFragment(fragment,node,FcurrentScope);

        Fwriter.WriteLn('// '+node.ToString+' // ('+ExtractFileName(fragmentName)+')');

        fList := TStringList.Create;
        try
          fList.Text := fragment;
          for j  := 0 to Flist.Count - 1 do EmitCode(fList.Strings[j]);
        finally
          fList.Free;
        end;
      end;
    end
    else
    if node.left is TFuncCall then begin
      emitSaveRegVars();

      if TFuncCall(node.left).funcType = ttAssembler then begin
        sym := FcurrentScope.lookup(TFuncCall(node.left).funcName);

        if TFuncSymbol(sym).funcAddress <> '' then
        // no _main_ label for external functions
          emitCode('jsr '+TFuncCall(node.left).funcName)
        else
          emitCode('jsr '+TFuncCall(node.left).funcName+'.'+MAIN_LABEL);
      end
      else
        emitCode('jsr '+TFuncCall(node.left).funcName+'.'+MAIN_LABEL);

      if TFuncCall(node.left).onStack then begin
        shrinkStack(TFuncCall(node.left).stackSize);
      end;
    end
    else
    if node.right1 is TPeek then begin
      emitPeek(node)
    end
    else begin
      if (node.left <> nil) and TVariable(node.left).isRegister and ((node.op <> nil) or (not node.right1.isLeaf) or (node.right2 <> nil)) then begin
        // dst is a register so substitute with temp var first
        reg := getTempRegVar();
        TVariable(node.left).value        := reg;
        TVariable(node.left).token.tValue := reg;

        node.left := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),node.left.size);
      end;

      if (node.right1 is TVariable) then begin
        if (node.right1.ToString = cA_REGISTER) or (node.right1.ToString = cX_REGISTER) or (node.right1.ToString = cY_REGISTER) then begin
          regVar := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),1);//TVariable.Create(getTempRegVar(),1);
          FcodeGen.doCopy(node.right1,regVar);

          node.right1 := regVar;
        end
        else
        if (node.right1.ToString = cAX_REGISTER) then begin
          regVar := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),2);//TVariable.Create(getTempRegVar(),2);
          FcodeGen.doCopy(node.right1,regVar);

          node.right1 := regVar;
        end;
      end;

//      if (node.left is TVariable) and (node.left.size = 0) then
//        if TVariable(node.left).sym <> nil then
//          if TVariable(node.left).sym.symType <> nil then
//            TVariable(node.left).size := TVariable(node.left).sym.symType.size;

      if (node.right2 is TVariable) then begin
        if (node.right2.ToString = cA_REGISTER) or (node.right2.ToString = cX_REGISTER) or (node.right2.ToString = cY_REGISTER) then begin
          regVar := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),1);//TVariable.Create(getTempRegVar(),1);
          FcodeGen.doCopy(node.right2,regVar);

          node.right1 := regVar;
        end
        else
        if (node.right2.ToString = cAX_REGISTER) then begin
          regVar := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),2);//TVariable.Create(getTempRegVar(),2);
          FcodeGen.doCopy(node.right2,regVar);

          node.right1 := regVar;
        end;
      end;

      if node.left = nil then
        node.left := TVariable.Create(cA_REGISTER,1);

      case node.getOp of
        ttShl   : done := FcodeGen.shiftLeft(node.right1,node.right2,node.left);
        ttShr   : done := FcodeGen.shiftRight(node.right1,node.right2,node.left);
        ttPlus  : done := FcodeGen.doAdd(node.right1,node.right2,node.left);
        ttMinus : done := FcodeGen.doSub(node.right1,node.right2,node.left);
        ttAnd   : done := FcodeGen.doAnd(node.right1,node.right2,node.left);
        ttOr    : done := FcodeGen.doOr(node.right1,node.right2,node.left);
        ttXor   : done := FcodeGen.doXor(node.right1,node.right2,node.left);
        ttNeg   : done := FcodeGen.doNeg(node.right2,node.left);
        ttNot   : done := FcodeGen.doNot(node.right2,node.left);
        ttEql   : done := FcodeGen.doEql(node.right1,node.right2,node.left);
        ttNeq   : done := FcodeGen.doNeq(node.right1,node.right2,node.left);
        ttLss   : done := FcodeGen.doLss(node.right1,node.right2,node.left);
        ttGtr   : done := FcodeGen.doGtr(node.right1,node.right2,node.left);
        ttLeq   : done := FcodeGen.doLeq(node.right1,node.right2,node.left);
        ttGeq   : done := FcodeGen.doGeq(node.right1,node.right2,node.left);
        ttTimes : done := FcodeGen.doMul(node.right1,node.right2,node.left);
        ttInc   : done := FcodeGen.doInc(node.left);
        ttDec   : done := FcodeGen.doDec(node.left);
      end;

      if done and TVariable(node.left).isRegister then begin
        // load register(s) with source
        saveRegVarInfo(node.left,TVariable(node.left).loReg,TVariable(node.left).hiReg);
  //      FcodeGen.loadRegister(node.left,TVariable(node.left).loReg,TVariable(node.left).hiReg);
      end;

      if not done then begin
        if (node.op = nil) then begin
               if (node.leftIndex <> nil) and (node.right1Index <> nil) then done := FcodeGen.readWriteIndexed_value(node.left,node.leftIndex,node.right1,node.right1Index)
          else if (node.leftIndex <> nil) and (node.right1Index = nil)  then done := FcodeGen.writeIndexed_value(node.left,node.leftIndex,node.right1)
          else if (node.leftIndex = nil) and (node.right1Index <> nil)  then done := FcodeGen.readIndexed_value(node.left,node.right1Index,node.right1)
          else if (node.left.isAddress)                                then done := FcodeGen.writeIndexed_value(node.left,TNumber.Create(0),node.right1)
          else if (node.right1.isAddress)                              then done := FcodeGen.readIndexed_value(node.left,TNumber.Create(0),node.right1)

          else begin
            if TVariable(node.left).isRegister then begin
              // load register(s) with source
             saveRegVarInfo(node.right1,TVariable(node.left).loReg,TVariable(node.left).hiReg);
  //            done := FcodeGen.loadRegister(node.right1,TVariable(node.left).loReg,TVariable(node.left).hiReg);
            end
            else begin
//              if node.right1 is TNumber then
//                done := FcodeGen.absoluteAddress_read(node.right1,node.left)
//              else
//              if (node.left is TNumber) and (node.right1 <> nil) then
//                done := FcodeGen.absoluteAddress_write(node.right1,node.left)
////              if (node.right1.isInteger) and (TNumber(node.right1).isAbsoluteAddress) then
////              // is a peek command
////                done := FcodeGen.absoluteAddress_read(node.right1,node.left)
////              else
////              if (node.left.isInteger) then
////              // is a poke command
////                done := FcodeGen.absoluteAddress_write(node.right1,node.left)
//              else
                done := FcodeGen.doCopy(node.right1,node.left)
            end;
          end;
        end else begin
          emitCode('Maths op: "'+node.op.toString+'" not supported yet...');
        end;
      end;
    end;
  end;
end;

procedure emitAdd(writer : TsesStringWriter; left,right: TExpToken);
begin
  writer.writeLn(indent+'clc');
  writer.writeLn(indent+'lda '+leafNodeToImmOrAddr(left));
  writer.writeLn(indent+'adc '+leafNodeToImmOrAddr(right));
end;

procedure emitSub(writer : TsesStringWriter; left,right: TExpToken);
begin
  writer.writeLn(indent+'sec');
  writer.writeLn(indent+'lda '+leafNodeToImmOrAddr(left));
  writer.writeLn(indent+'sbc '+leafNodeToImmOrAddr(right));
end;

procedure TASTto6502.onAsmWrite(aAsmCode : AnsiString);
// search for the useage of routines, get the routine name, and increase the ref count of it

begin
  routines_searchFor('jsr ',aAsmCode,FcurrentScope);
  routines_searchFor('lda #',aAsmCode,FcurrentScope);
end;

//----------------------------
// AST Visitor methods
//----------------------------
procedure TASTto6502.emitStackInfo;
var
  i : Integer;
begin
  for i := 0 to MAX_TEMP - 1 do begin
    Fwriter.writeLn('%s%d: .byte 0,0,0',[TEMP_REG,i]);
  end;

  Fwriter.writeLn();
//
//  Fwriter.writeLn('_stack_backup_: .fill %d,0',[MAX_TEMP*2]);
//
//  Fwriter.writeLn();
//
//  Fwriter.writeLn('%sindex: .byte 0',[TEMP_NAME]);
//  Fwriter.write(TEMP_NAME+'lo: .byte ');
//
//  for i := 0 to MAX_TEMP - 1 do begin
//    Fwriter.write('0');
//
//    if i < (MAX_TEMP - 1) then Fwriter.write(',');
//  end;
//  Fwriter.writeLn();
//
//  Fwriter.write(TEMP_NAME+'hi: .byte ');
//
//  for i := 0 to MAX_TEMP - 1 do begin
//    Fwriter.write('0');
//
//    if i < (MAX_TEMP - 1) then Fwriter.write(',');
//  end;
//
//  Fwriter.writeLn();
//  Fwriter.writeLn();
//  Fwriter.writeLn('.macro saveStack() {');
//  Fwriter.writeLn('  .for(var x = 0; x < %d; x++) {',[MAX_TEMP*2]);
//  Fwriter.writeLn('    lda %s0+x',[TEMP_NAME]);
//  Fwriter.writeLn('    sta _stack_backup_+x');
//  Fwriter.writeLn('  }');
//  Fwriter.writeLn('}');
//  Fwriter.writeLn();
//  Fwriter.writeLn('.macro restoreStack() {');
//  Fwriter.writeLn('  .for(var x = 0; x < %d; x++) {',[MAX_TEMP*2]);
//  Fwriter.writeLn('    lda _stack_backup_+x');
//  Fwriter.writeLn('    sta %s0+x',[TEMP_NAME]);
//  Fwriter.writeLn('  }');
//  Fwriter.writeLn('}');
  Fwriter.writeLn(FUNC_RESULT_NAME+'index: .byte 0');
  Fwriter.write(FUNC_RESULT_NAME+': .byte ');

  for i := 0 to MAX_FUNC_RESULT - 2 do begin
    Fwriter.write('0,');
  end;
  Fwriter.writeLn('0');
end;

procedure TASTto6502.outputMainCode;
var
  i : Integer;
begin
  setIndent(0);

  // do source file imports "units"
  for i := 0 to Fprog.imports.Count - 1 do
    Fwriter.writeLn('#import "'+Fprog.imports.Strings[i]+'"');

  Fwriter.writeLn();

  // do all binary imports
  for i := 0 to Fprog.binImports.Count - 1 do
    TAST(Fprog.binImports.Items[i]).acceptVisitor(self);

  // visit all unit included declarations
  for i := 0 to Fprog.unitBlocks.Count - 1 do
    TAST(Fprog.unitBlocks.Items[i]).acceptVisitor(self);

  Fwriter.writeLn(indent+Fprog.name+':');
  incIndent;

  Fwriter.writeLn();

  // visit program block
  Fprog.block.acceptVisitor(self);

  emitCode('rts');
  decIndent;
//  Fwriter.writeLn(indent+'}');

  decIndent;

  FcurrentScope := Fprog.scope.enclosingScope;
end;

procedure TASTto6502.outputTempVars;
var
  i : Integer;
begin
  Fwriter.writeLn();
  Fwriter.writeLn('// temp variables, etc.');

  for i := 0 to cMAX_TEMP_REGS_VARIABLE - 1 do begin
  // temp register variables for register params
    Fwriter.writeLn(cTEMP_REGS_VARIABLE+IntToStr(i)+': .byte 0,0,0');
  end;

  Fwriter.writeLn(cTEMP_IF_VARIABLE  +': .byte 0,0');
  Fwriter.writeLn(cTEMP+': .byte 0,0,0');

  emitStackInfo;
end;

procedure TASTto6502.outputNESsegments;
var
  i   : Integer;
  seg : TSegmentInfo;
begin
  Fwriter.writeLn('.segment CARTRIDGE_FILE [outBin="%s.nes", allowOverlap]',[Fprog.fileName]);

  Fwriter.writeLn('#import "'+Fprog.SegmentsFileName+'"');
{
  Fwriter.writeLn('.segmentout [segments ="HEADER"]');

  for i := 0 to Fprog.segments.Count - 1 do begin
    seg := TSegmentInfo(Fprog.segments.Items[i]);

    Fwriter.writeLn('.segmentout [segments ="%s"]',[seg.name]);
  end;

  Fwriter.writeLn();
  Fwriter.writeLn('.segment HEADER [start=$00,max=$10]');
  Fwriter.writeLn('.segment ZP [start=$00,max=$ff]');
  Fwriter.writeLn('//	free ram');
  Fwriter.writeLn('.segment RAM [start=$140,max=$6ff]');
  Fwriter.writeLn('//	sprite ram');
  Fwriter.writeLn('.segment OAM [start=$700,max=$7ff]');

  for i := 0 to Fprog.segments.Count - 1 do begin
    seg := TSegmentInfo(Fprog.segments.Items[i]);
    Fwriter.writeLn('.segment %s [start=%s,min=%s,max=%s,fill]',
      [
        seg.name,
        seg.start,
        seg.min,
        seg.max
      ]);
  end;
}
  Fwriter.writeLn();
//  Fwriter.writeLn('#import "nes.asm"');
//  Fwriter.writeLn();
  Fwriter.writeLn('#import "'+Fprog.HeaderFileName+'"');
{
  Fwriter.writeLn('.segment HEADER');
  Fwriter.writeLn('_NESHEADER(%d,%d,%d,%d,%d)',
    [
    Fprog.nesHeader.mapper,
    Fprog.nesHeader.system,
    Fprog.nesHeader.country,
    Fprog.nesHeader.prgrom_banks,
    Fprog.nesHeader.chrrom_banks
    ]);
}
  Fwriter.writeLn;
  Fwriter.writeLn('//	ZP');
  Fwriter.writeLn('.segment ZP "ZP"');
  Fwriter.writeLn('// define software stack and set index to top of stack (empty)');
  Fwriter.writeLn('TEMP_PNTR: .word 0');
  Fwriter.writeLn;
  Fwriter.writeLn('//	as much RAM as we can get');
  Fwriter.writeLn('//	small stack');
  Fwriter.writeLn('.segment RAM "RAM"');
  Fwriter.writeLn('_sp:	.byte $ff');
  Fwriter.writeLn(cSTACK_NAME+':	.fill 256,0');

  outputTempVars;

  Fwriter.writeLn;
  Fwriter.writeLn('.segment '+Fprog.PRGROMsegment+' "PRGROM"');
  Fwriter.writeLn('//	Our code');
end;

procedure TASTto6502.outputNESIRQs;
begin
  Fwriter.writeLn('');
  Fwriter.writeLn('.segment '+Fprog.IRQsegment+'');
  Fwriter.writeLn('');
  Fwriter.writeLn('*=$fffa "INTERUPT VECTORS"');
  Fwriter.writeLn('.word nmi.%s',[MAIN_LABEL]);
  Fwriter.writeLn('.word %s',[Fprog.name+'_'+MAIN_LABEL]);
  Fwriter.writeLn('.word irq.%s',[MAIN_LABEL]);


end;

procedure TASTto6502.doNES;
begin
  outputNESsegments;

  outputMainCode;

  outputNESIRQs;
end;

procedure TASTto6502.visitProgram(node : TProgram);
  procedure outputStack;
  begin
    Fwriter.writeLn('_sp: .byte $ff');
    Fwriter.writeLn(cSTACK_NAME+':	.fill 256,0');
    Fwriter.writeLn;
  end;

var
  i       : Integer;
  binFile : AnsiString;
  origin  : AnsiString;
  srcFile : AnsiString;
  dstFile : AnsiString;
begin
  Fprog := node;

  FcurrentScope := node.scope;

  if node.target = ttNES then begin
    doNES;
    Exit;
  end;

  Fwriter.writeLn('// temp pointer for routines');
  Fwriter.writeLn('.label TEMP_PNTR = '+FtempPointer);
  Fwriter.writeLn();

  if node.loadAddress <> '' then
    origin := node.loadAddress
  else begin
    if node.target = ttC64 then
      origin := '080d'
    else
    if node.target = ttC128 then
      origin := '1C01'
    else
    if node.target = ttVic20 then
      origin := '1001'
    else
    if node.target = ttPET then
      origin := '0401';
  end;

  Fwriter.writeLn('.label _origin = $%s',[origin]);
  Fwriter.writeLn();

  if (node.loadAddress <> '') then begin
    if (node.target in [ttBBC]) then begin
      binFile := StringReplace(UpperCase(node.fileName),' ','',[rfReplaceAll]);

      Fwriter.writeLn();

      Fwriter.writeLn('* = $'+origin+' "main code"');
      Fwriter.writeLn;

      // no basic upstart, only simple jump
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
      outputStack;
    end
    else
    if (node.target in APPLE) or (node.target in ATARI_8BIT) then begin
      Fwriter.writeLn();

      Fwriter.writeLn('* = $'+origin+' "main code"');
      // no basic upstart, only simple jump
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
      outputStack;
    end
//    else
//    if (node.target in ATARI_8BIT) then begin
//      Fwriter.writeLn();
//
//      Fwriter.writeLn('* = $'+origin+' "main code"');
//      Fwriter.writeLn;
//
//      // no basic upstart, only simple jump
//      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
//      outputStack;
//    end
    else begin
      Fwriter.writeLn(Format('.segment %s [start=$'+origin+',outPrg="%s.prg", allowOverlap]',[node.name,node.fileName]));
      Fwriter.writeLn();

      Fwriter.writeLn('* = $'+origin+' "main code"');
      Fwriter.writeLn;

      // no basic upstart, only simple jump
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
      outputStack;
    end;
  end
  else begin
    if (node.target = ttPET) then begin
      Fwriter.writeLn(Format('.segment %s [start=$'+origin+',outPrg="%s.prg", allowOverlap]',[node.name,node.fileName]));
      Fwriter.writeLn();

//      Fwriter.writeLn(':BasicUpstart2(%s_%s)',[node.name,MAIN_LABEL]);
      Fwriter.writeLn(':BasicUpstart(%s)',[START_LABEL]);
      Fwriter.writeLn;
      Fwriter.writeLn('* = $040d "main code"');
      Fwriter.writeLn(START_LABEL+':');
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
      outputStack;
      Fwriter.writeLn;
    end
    else
    if (node.target = ttVic20) then begin
      Fwriter.writeLn(Format('.segment %s [start=$'+origin+',outPrg="%s.prg", allowOverlap]',[node.name,node.fileName]));
      Fwriter.writeLn();

//      Fwriter.writeLn(':BasicUpstart2(%s_%s)',[node.name,MAIN_LABEL]);
      Fwriter.writeLn(':BasicUpstart(%s)',[START_LABEL]);
      Fwriter.writeLn;
      Fwriter.writeLn('* = $100e "main code"');
      Fwriter.writeLn(START_LABEL+':');
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
      outputStack;
      Fwriter.writeLn;
    end
    else
    if (node.target = ttC64) then begin
      Fwriter.writeLn(Format('.segment %s [start=$'+origin+',outPrg="%s.prg", allowOverlap]',[node.name,node.fileName]));
      Fwriter.writeLn();

//      Fwriter.writeLn(':BasicUpstart2(%s_%s)',[node.name,MAIN_LABEL]);
      Fwriter.writeLn(':BasicUpstart2(%s)',[START_LABEL]);
      Fwriter.writeLn;
      Fwriter.writeLn('* = $080d "main code"');
      Fwriter.writeLn(START_LABEL+':');
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
      outputStack;
      Fwriter.writeLn;
    end
    else
    if (node.target = ttC128) then begin
      Fwriter.writeLn(Format('.segment %s [start=$'+origin+',outPrg="%s.prg", allowOverlap]',[node.name,node.fileName]));
      Fwriter.writeLn();

//      Fwriter.writeLn(':BasicUpstart2(%s_%s)',[node.name,MAIN_LABEL]);
      Fwriter.writeLn(':BasicUpstart(%s)',[START_LABEL]);
      Fwriter.writeLn;
      Fwriter.writeLn('* = $1C0D "main code"');
      Fwriter.writeLn(START_LABEL+':');
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
      Fwriter.writeLn;
      outputStack;
    end
    else
    if (node.target in [ttBBC]) then begin
      binFile := StringReplace(UpperCase(node.fileName),' ','',[rfReplaceAll]);

//      Fwriter.writeLn(Format('.segment %s [start=$'+node.loadAddress+',outPrg="%s", allowOverlap]',[node.name,binFile]));
//      Fwriter.writeLn();

      Fwriter.writeLn('* = $'+origin+' "main code"');
      Fwriter.writeLn;

      // no basic upstart, only simple jump
      Fwriter.writeLn('jmp '+node.name+'_'+MAIN_LABEL);
      outputStack;
    end;
  end;

  outputMainCode;

  outputTempVars;

  // do all deferred binary imports
  for i := 0 to node.deferredBinImports.Count - 1 do
    TAST(node.deferredBinImports.Items[i]).acceptVisitor(self);

  // do all absolute array data
  for i := 0 to node.absoluteArrayData.Count - 1 do
    TAST(node.absoluteArrayData.Items[i]).acceptVisitor(self);
end;

procedure TASTto6502.visitBlock(node : TBlock);
var
  i   : Integer;
begin
  if not node.isMacro then begin
    for i := 0 to node.declarations.Count - 1 do begin
      TAST(node.declarations.Items[i]).acceptVisitor(self);
    end;

    if (node.blockName <> '') then begin
      if FcurrentScope.scopeLevel > 1 then
        emitCode(indent+MAIN_LABEL+':')
      else
        emitCode(indent+node.blockName+'_'+MAIN_LABEL+':');
    end;
  end;

  if FblockIRQType in [ttInterrupt] then begin
//    emitCode(':saveStack()');
    emitCode('  :irq_start()');
  end
  else
  if FblockIRQType = ttKernalInterrupt then
    emitCode('  :kernal_irq_start()');

  node.compoundStatement.acceptVisitor(self);
end;

procedure TASTto6502.visitAsmBlock(node : TAsmBlock);
var
  i   : Integer;
begin
  if not node.isMacro then begin
    for i := 0 to node.declarations.Count - 1 do begin
      TAST(node.declarations.Items[i]).acceptVisitor(self);
    end;

    if (node.blockName <> '') then
      Fwriter.writeLn(indent+MAIN_LABEL+':');

    if FblockIRQType in [ttInterrupt] then begin
  //    emitCode(':saveStack()');
      emitCode('  :irq_start()');
    end
    else
    if FblockIRQType = ttKernalInterrupt then
      emitCode('  :kernal_irq_start()');
  end;

  for i := 0 to node.asmStatements.Count - 1 do begin
  // output asm statements
    Fwriter.writeLn(Trim(node.asmStatements.Strings[i]));
  end;
end;

procedure TASTto6502.visitUnitBlock(node: TUnitBlock);
var
  i : Integer;
begin
  for i := 0 to node.declarations.Count - 1 do begin
    TAST(node.declarations.Items[i]).acceptVisitor(self);
  end;
end;

procedure TASTto6502.emitBinOp(left,right,op : TExpToken);
begin
  if (left.size = 1) and (right.size = 1) then begin
    case op.token.tType of
      ttPlus  : begin
        emitCode('clc');
        emitCode('lda '+leafNodeToImmOrAddr(left));
        emitCode('adc '+leafNodeToImmOrAddr(right));
      end;
      ttMinus :  begin
        emitCode('sec');
        emitCode('lda '+leafNodeToImmOrAddr(left));
        emitCode('sbc '+leafNodeToImmOrAddr(right));
      end;
    end;
  end;
end;

function  TASTto6502.isSimpleAdd2(node : TAssign; symSize : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttPlus) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value = 2);
    end
    else
    if binOp.right.isVariable and (binOp.left.isInteger) then begin
      Result := (TVariable(binOp.right).value = node.variable.value) and (TNumber(binOp.left).value = 2);
    end;
  end;

  if not Result then Exit;

  if symSize = 1 then begin
    emitCode('inc '+node.variable.value);
    emitCode('inc '+node.variable.value);
  end
  else begin
    emitCode('inc '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('inc '+node.variable.value+' + 1');
    emitLabel('!');
    emitCode('inc '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('inc '+node.variable.value+' + 1');
    emitLabel('!');
  end;
end;

function  TASTto6502.isSimpleSub2(node : TAssign; symSize : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttMinus) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value = 2);
    end
    else
    if binOp.right.isVariable and (binOp.left.isInteger) then begin
      Result := (TVariable(binOp.right).value = node.variable.value) and (TNumber(binOp.left).value = 2);
    end;
  end;

  if not Result then Exit;

  if symSize = 1 then begin
    emitCode('dec '+node.variable.value);
    emitCode('dec '+node.variable.value);
  end
  else begin
    emitCode('lda '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('dec '+node.variable.value+' + 1');
    emitLabel('!');
    emitCode('lda '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('dec '+node.variable.value+' + 1');
    emitLabel('!');
  end;
end;

function  TASTto6502.isSimpleIncrement(node : TAssign; symSize : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttPlus) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value = 1);
    end
    else
    if binOp.right.isVariable and (binOp.left.isInteger) then begin
      Result := (TVariable(binOp.right).value = node.variable.value) and (TNumber(binOp.left).value = 1);
    end;
  end;

  if not Result then Exit;

  emitCode('// inc '+node.variable.value);
  if symSize = 1 then
    emitCode('inc '+node.variable.value)
  else begin
    emitCode('inc '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('inc '+node.variable.value+' + 1');
    emitLabel('!');
  end;
end;

function  TASTto6502.isSimpleDecrement(node : TAssign; symSize : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttMinus) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value = 1);
    end
    else
    if binOp.right.isVariable and (binOp.left.isInteger) then begin
      Result := (TVariable(binOp.right).value = node.variable.value) and (TNumber(binOp.left).value = 1);
    end;
  end;

  if not Result then Exit;

  emitCode('// dec '+node.variable.value);
  if symSize = 1 then
    emitCode('dec '+node.variable.value)
  else begin
    emitCode('dec '+node.variable.value+' + 0');
    emitCode('bne !+');
    emitCode('dec '+node.variable.value+' + 1');
    emitLabel('!');
  end;
end;

function  TASTto6502.isSHL(node : TAssign; var shiftCount : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttShl) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value >= 0);
      shiftCount := TNumber(binOp.right).value;
    end;
  end;
end;

function  TASTto6502.isSHR(node : TAssign; var shiftCount : Integer) : Boolean;
var
  binOp : TBinOp;
begin
  Result := False;

  if (node.expr is TBinOp) then begin
    binOp := TBinOp(node.expr);

    if (binOp.op.tType <> ttShr) then Exit;

    if binOp.left.isVariable and (binOp.right.isInteger) then begin
      Result := (TVariable(binOp.left).value = node.variable.value) and (TNumber(binOp.right).value >= 0);
      shiftCount := TNumber(binOp.right).value;
    end;
  end;
end;

function  isSimpleArrayIndex(node : TAST; var aVarName,aNumber : AnsiString) : Boolean;
var
  leftType  : AnsiString;
  rightType : AnsiString;
begin
  Result := False;

  aVarName := '';
  aNumber  := '0';

  if (node = nil)     then Exit;
  if (node.size <> 1) then Exit;

  if (node.isInteger) then begin
    aNumber := TNumber(node).token.tValue;
    Result := True;
    Exit;
  end
  else if (node.isVariable) then begin
    aVarName := TVariable(node).token.tValue;
    Result := True;
    Exit;
  end else if (node is TBinOp) then begin
    leftType  := TBinOp(node).left.ClassName;
    rightType := TBinOp(node).right.ClassName;

    if ((TBinOp(node).left is TVariable) and (TBinOp(node).right is TNumber)) then begin
      aVarName := TVariable(TBinOp(node).left).token.tValue;
      aNumber  := TNumber(TBinOp(node).right).token.tValue;
      Result := True;
      Exit;
    end;

    if ((TBinOp(node).right is TVariable) and (TBinOp(node).left is TNumber)) then begin
      aVarName := TVariable(TBinOp(node).right).token.tValue;
      aNumber  := TNumber(TBinOp(node).left).token.tValue;
      Result := True;
      Exit;
    end;
  end;
end;

function pokeDstIsNumAddressPlusIndex(address : TAST; var addr : TAST) : boolean;
// test for number + 8-bit index
// return addr + index
var
  left    : TAST;
  right   : TAST;
  op      : TTokenType;
begin
  Result := False;

  if not address.isBinOp then Exit;

  left  := TBinOp(address).left;
  right := TBinOp(address).right;
  op    := TBinOp(address).op.tType;

  if (op = ttPlus) and left.isInteger and (right.size = 1) then begin
    Result := True;
    addr   := TVariable.Create(left.toString,1);
    addr.arrayParam := right;
    TVariable(addr).token.tValue := '!'+TVariable(addr).token.tValue;
  end
  else
  if (op = ttPlus) and right.isInteger and (left.size = 1) then begin
    Result := True;
    addr   := TVariable.Create(right.toString,1);
    addr.arrayParam := left;
    TVariable(addr).token.tValue := '!'+TVariable(addr).token.tValue;
  end;
end;

function pokeIsCompatibleWithTAC(node : TPoke) : Boolean;
var
  left    : TAST;
  right   : TAST;
  addr    : TAST;
  binOp   : TBinOp;
  op      : TTokenType;
begin
  Result := True;

  if node.address.isInteger then begin
    // poke is plain number address & number or variable value
    if node.expr.isInteger or node.expr.isVariable then Exit;

    if node.expr is TPeek then begin
      if TPeek(node.expr).params.Count = 1 then begin
        //
        if TAST(TPeek(node.expr).params[0]).isInteger then Exit;

        if TAST(TPeek(node.expr).params[0]).isBinOp then begin
          binOp := TBinOp(TPeek(node.expr).params[0]);

          if binOp.left.isVariable or binOp.right.isVariable then begin
            Result := False;
            Exit
          end;

          if binOp.left is TPeek and binOp.right.isInteger then Exit;
          if binOp.right is TPeek and binOp.left.isInteger  then Exit;
        end;
      end;
    end
    else
    if node.expr is TBinOp then begin
      // look for peek and value, peek or value
      if TBinOp(node.expr).left is TPeek and TBinOp(node.expr).right.isInteger then Exit;
      if TBinOp(node.expr).right is TPeek and TBinOp(node.expr).left.isInteger then Exit;

    end;
  end;

  Result := False;
end;

procedure TASTto6502.visitPoke(node : TPoke);
var
  c       : Byte;
  tacList : TTACList;
  tacNode : TTACNode;
  addr    : TAST;
  index   : TAST;
  assign  : TAssign;
begin
  if pokeIsCompatibleWithTAC(node) then begin
    if node.address.isInteger then begin
      node.address.size := 1;
      TNumber(node.address).token.tValue := '!'+TNumber(node.address).token.tValue;
    end;

    tacList := generateTAC(node.expr,TVariable(node.address));
    tacList.optimise;
//    tacList.debugPrint;

    emitTACcode(tacList);
  end
  else
  if pokeDstIsNumAddressPlusIndex(node.address,addr) then begin
    tacList := generateTAC(node.expr,TVariable(addr));
    tacList.optimise;
//    tacList.debugPrint;

    emitTACcode(tacList);
    Exit;
  end
  else begin
    // use builtin readb/writeb routines instead

    routines_setAsUsed('writeb',FcurrentScope);

    assign := TAssign.Create(TVariable.Create('writeb.addr',2),node.address);
    assign.acceptVisitor(self);

    assign := TAssign.Create(TVariable.Create('writeb.value',1),node.expr);
    assign.acceptVisitor(self);

    Fwriter.writeLn(indent+'jsr writeb.'+MAIN_LABEL);
  end;

  Exit;
{
  if node.address.isVariable then begin
    node.address.size := 1;
//    TVariable(node.address).deref := True;
    node.address.arrayParam := TNumber.Create(0);
  end
  else
  if pokeDstIsNumAddressPlusIndex(node.address,addr,index) then begin
  // it is a number + simple index for the poke dest
    node.address    := addr;//TVariable.Create(addr.toString,1);
//    node.arrayParam := index;
//    node.address.arrayParam := index;
  end;

  tacList := generateTAC(node.expr,TVariable(node.address));
  tacList.optimise;
  tacList.debugPrint;

  emitTACcode(tacList);   }
{**

  if node.address.isVariable then begin
    tacList := generateTAC(node.expr,TVariable(node.address));
    tacList.optimise;
    emitTACcode(tacList);
  end
  else
  if node.expr.isLeaf then begin
    if (node.address.isInteger and node.expr.isInteger) then begin
      emitCode('lda #' + TNumber(node.expr).token.tValue);
      emitCode('sta ' + node.address.toString);
    end
    else
    if (node.address.isInteger and node.expr.isVariable) then begin
      emitCode('lda ' + TVariable(node.expr).token.tValue);
      emitCode('sta ' + node.address.toString);
    end
    else
    if (node.address.isInteger and node.expr.isString) then begin
      if TString(node.expr).token.tValue <> '' then begin
        c := charToCBMscreenCode(TString(node.expr).token.tValue[1]);
        emitCode('lda #' + IntToStr(c));
      end
      else
        emitCode('Poke: value is an empty string!');
    end
    else
      emitCode('sta ' + node.address.toString);
  end else begin
    tacList := generateTAC(node.expr,TVariable(node.address));
    tacList.optimise;
    emitTACcode(tacList);
  end;
  **}
end;

var
  stack      : array[0..MAX_TEMP - 1] of TAST;
  sIndex     : Integer;
  reg        : Integer;

function newTemp : AnsiString;
begin
  Result := TEMP_REG + IntToStr(reg);
  Inc(reg);
end;

function popValue : TAST;
begin
  Result := stack[sIndex];
  Dec(sIndex);
end;

procedure pushValue(aValue : TAST);
begin
  Inc(sIndex);
  stack[sIndex] := aValue;
end;

function isIndexedVariable(v : TAST) : Boolean;
begin
  Result := False;

  if v = nil then Exit;
  if not v.isVariable then Exit;

  Result := TVariable(v).arrayParam <> nil;
end;

function  TASTto6502.expandFuncCall(aFuncCall : TFuncCall; aTacList : TTACList) : TAST;
var
  p         : Integer;
  param     : TVarSymbol;
  funcSym   : TFuncSymbol;
  callParam : TAST;
  temp      : TAST;
  index     : TAST;
  lIndex    : TAST;
  rIndex    : TAST;
  loReg     : AnsiString;
  hiReg     : AnsiString;
  value     : AnsiString;
  funcVar   : TVariable;
  dstVar    : TVariable;
  assign    : TAssign;
  tacNode   : TTACNode;
  tempWordSize : Integer;
begin
  temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),2);

  Result := temp;

  if (aFuncCall.funcType = ttAssembler) or (aFuncCall.onStack) then begin
    if aFuncCall.size = 1 then
      funcVar := TVariable.Create(uCodeGen_6502.cA_REGISTER,aFuncCall.size)
    else
      funcVar := TVariable.Create(uCodeGen_6502.cAX_REGISTER,aFuncCall.size);

    funcVar.isRegister := True;
  end
  else
    funcVar := TVariable.Create(aFuncCall.funcName+'.result',aFuncCall.size);

  funcSym := TFuncSymbol(FcurrentScope.lookup(aFuncCall.funcName));

  if funcSym.onStack then
    expandStack(funcSym.stackSize);

  for p := 0 to Length(funcSym.params) - 1 do begin
    // load params
    param     := funcSym.params[p];
    callParam := TAST(aFuncCall.params.Items[p]);

    if (callParam is TFuncCall) then
      callParam := TAST(expandFuncCall(TFuncCall(callParam),aTacList))
    else
    if callParam is TBuiltInFunction then
      callParam := TBuiltInFunction(callParam).expand(aTacList);

    dstVar := TVariable.Create(aFuncCall.funcName + '.' + param.symName,param.size);

    if (aFuncCall.funcType = ttAssembler) and isAsmParam(param.symName,param.symType,loReg,hiReg) then begin
    // is a asm param so use correct register(s) to load into
      dstVar.isRegister := True;
      dstVar.loReg      := loReg;
      dstVar.hiReg      := hiReg;
    end;

    // add param assignment to list
    if callParam is TBinOp then begin
    //(aLeft,aOp,aRight1,aRight2,aLeftIndex,aRightIndex : TAST)
      lIndex := TBinOp(callParam).left.arrayParam;
      rIndex := TBinOp(callParam).right.arrayParam;

      tacNode := TTacNode.Create(dstVar,callParam,TBinOp(callParam).left,TBinOp(callParam).right,nil,lIndex,rIndex);
    end
    else
    if callParam is TUnaryOp then begin
    //(aLeft,aOp,aRight1,aRight2,aLeftIndex,aRightIndex : TAST)
      tacNode := TTacNode.Create(dstVar,callParam,TUnaryOp(callParam).expr,nil,nil,nil,nil);
    end
    else begin
    // new code!!
      index := nil;

      if isIndexedVariable(callParam) then begin
        index := TVariable(callParam).arrayParam;
        tempWordSize := 1;
        generateTAC(TVariable(callParam).arrayParam,aTacList,TVariable(callParam).use8BitValue);

        if (callParam.size > 1) and not isDerefed(callParam) then begin
          tempWordSize := callParam.size;
          index := generateIndexShiftTAC(callParam,aTacList,TVariable(callParam).use8BitValue);

//          index := popValue;
        end;
      end;
      // --------------------------

      tacNode := TTacNode.Create(dstVar,nil,callParam,nil,nil,index,nil);
    end;

    aTacList.addNode(tacNode);
  end;

  // add node calling the function
  tacNode := TTacNode.Create(aFuncCall,nil,nil,nil,nil,nil,nil);
  aTacList.addNode(tacNode);

  // add node loading func var into temp variable and return the temp
  tacNode := TTacNode.Create(temp,nil,funcVar,nil,nil,nil,nil);

//  tacNode := TTacNode.Create(aFuncCall,nil,nil,nil,nil,nil);
  aTacList.addNode(tacNode);
//  aTacList.debugPrint;
end;

function  TASTto6502.arrayIndexCanBe8Bit(valueAST : TAST) : Boolean;
var
  sym   : TSymbol;
  value : AnsiString;
begin
  Result := False;
  value := valueAST.toString;

  sym := FcurrentScope.lookup(value);

  if (sym is TConstSymbol) then begin
    if (TConstSymbol(sym).isArray)   then begin
      Result := (TConstSymbol(sym).len * TConstSymbol(sym).symType.size) <= 256;
      Exit;
    end;
  end;

  if (sym is TVarSymbol) then begin
    if TVarSymbol(sym).address <> '' then begin
      Result := False;
      Exit;
    end;

    if TVarSymbol(sym).isArray then begin
      Result := TVarSymbol(sym).uses8BitIndex;
      Exit;
    end;
  end;
end;

function  TASTto6502.generateIndexShiftTAC(aIndex : TAST; aTacList : TTACList; use8BitValue : Boolean = false) : TAST;
var
  shift      : Integer;
  right1     : TAST;
  right2     : TAST;
  op         : TAST;
  temp       : TAST;
  tacNode    : TTACNode;
  tempWordSize : Integer;
begin
  if use8BitValue then
    tempWordSize := 1
  else
    tempWordSize := 2;

  shift  := PowerOf2ToShift(aIndex.size);
  right2 := TNumber.Create(shift);
  right1 := popValue;

  op     := TBinOp.Create(right1,uToken.Token(ttShl,'<<'),right2);

//  if right1.sizeOverride > 0 then
//    temp  := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),right1.sizeOverride)
//  else
    temp  := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),tempWordSize);

//  temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),2);
  pushValue(temp);
//  writeLn(right1.toString,' ',op.toString,' ',right2.toString);

  tacNode := TTACNode.Create(temp,op,right1,right2,nil,nil,nil);
  aTacList.addNode(tacNode);

  Result := temp;
end;

procedure TASTto6502.generateTAC(node : TAST; aTacList : TTACList; use8BitValue : Boolean = False);
var
  temp         : TAST;
  ast          : TAST;
  right1       : TAST;
  right2       : TAST;
  index        : TAST;
  exp          : TList;
  i            : Integer;
  tacNode      : TTACNode;
  tempWordSize : Integer;
  sym          : TSymbol;
begin
  tempWordSize := 2;
  if use8BitValue then
    tempWordSize := 1;

  exp := flattenExpression(node);

//  if FuseLargeTempVars then
//    tempWordSize := 2
//  else
//    tempWordSize := 1;
//
//  tempWordSize := 2;

  for i := 0 to exp.Count - 1 do begin
    ast := exp.Items[i];

    if not ast.isOp then begin
      if isIndexedVariable(ast) then begin
        generateTAC(TVariable(ast).arrayParam,aTacList,TVariable(ast).use8BitValue);

        if (ast.size > 1) and not isDerefed(ast) then begin
          generateIndexShiftTAC(ast,aTacList,TVariable(ast).use8BitValue);
        end;

        right1 := popValue;
        index := right1;//TVariable.Create(uToken.Token(ttIdent,right1.toString),0);
//        if ast.derefRWsize = 3 then
          temp  := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),max(ast.size,ast.derefRWsize));
//        else
//          temp  := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),tempWordSize);
        pushValue(temp);
//        writeLn(temp.toString,' := ',ast.toString,'[',right1.toString,']');

        tacNode := TTACNode.Create(temp,nil,ast,nil,nil,index,nil);
        aTacList.addNode(tacNode);
      end
      else begin
//        if ast is TPeek then begin
//          temp  := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),1);
//          pushValue(temp);
//          tacNode := TTACNode.Create(temp,nil,ast,nil,nil,nil);
//          aTacList.addNode(tacNode);
//        end
//        else
        if ast is TFuncCall then
          pushValue(expandFuncCall(TFuncCall(ast),aTacList))
        else
        if ast is TBuiltInFunction then
          pushValue(TBuiltInFunction(ast).expand(aTacList))
        else
          pushValue(ast);
      end;
    end
    else
    if (ast.isBinOp) then begin
      right2 := popValue;
      right1 := popValue;

//      // condition = temp size of 1
//      if right1.isLeaf and right2.isLeaf and isRelOp(TBinOp(ast).op.tType) then
//        temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),1)
//      else
      if (TBinOp(ast).op.tType = ttTimes) and (((right1.size = 1)and(right2.size = 2))or((right1.size = 2)and(right2.size = 1))) then
        // result = int24 size
        temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),3)
      else
      if (right1.size = 3) or (right2.size = 3) or (right1.derefRWsize = 3) or (right2.derefRWsize = 3) then
        temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),3)
      else
//      if (TBinOp(ast).op.tType in [ttEql, ttNeq, ttLss, ttLeq, ttGtr, ttGeq]) then
//      // all rel op = 1 byte result
//        temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),1)
//      else
      if (right1.size = 1) and (right2.size = 1) and (TBinOp(ast).op.tType in [ttAnd,ttXor,ttOr])  then
        temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),1)
      else begin
        temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),tempWordSize);

      end;

        if TBinOp(ast).op.tType = ttMinus then
          // subtraction makes signed result
          temp.isSigned := True
        else
        if TBinOp(ast).op.tType in [ttTimes,ttDiv,ttDiv256] then
          // mul/div of signed = signed result
          temp.isSigned := TBinOp(ast).right.isSigned or TBinOp(ast).left.isSigned
        else
        if TBinOp(ast).left.isSigned or TBinOp(ast).right.isSigned then
          temp.isSigned := True;

      pushValue(temp);
//      writeLn(temp.toString,' := ',right1.toString,' ',ast.toString,' ',right2.toString);

      tacNode := TTACNode.Create(temp,ast,right1,right2,nil,nil,nil);
      tacNode.isTemp := True;
      aTacList.addNode(tacNode);
    end
    else
    if (ast is TCastOp) or (ast.isUnaryOp) then begin
      right2 := popValue;

      if ast is TCastOp then begin
        temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),TCastOp(ast).newSize);
        temp.isSigned := TCastOp(ast).isSigned;
      end
      else begin
        if (right2.size = 1) then
          temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),1)
        else
        if (right2.size = 3) or (right2.derefRWsize = 3)  then
          temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),3)
        else
          temp := TVariable.Create(uToken.Token(ttIdent,aTacList.getTemp),tempWordSize);
      end;

      pushValue(temp);
//      writeLn(temp.toString,' := ',ast.toString,' ',right2.toString);

      tacNode := TTACNode.Create(temp,ast,nil,right2,nil,nil,nil);
      aTacList.addNode(tacNode);
    end;
  end;
end;

function  TASTto6502.generateTAC(node : TAST; dstVar : TVariable) : TTACList;
var
  temp         : TAST;
  right        : TAST;
  value        : TAST;
  index        : TAST;
  tacNode      : TTACNode;
  tempWordSize : Integer;
begin
  tempWordSize := 2;

  reg    := 0;
  sIndex := -1;

  Result := TTACList.Create;

  if dstVar = nil then begin
    if node.size = 0 then
      dstVar  := TVariable.Create(uToken.Token(ttIdent,Result.getTemp),2)// tempWordSize);
    else begin
      dstVar  := TVariable.Create(uToken.Token(ttIdent,Result.getTemp),Max(node.size,node.derefRWsize));// tempWordSize);
    end;
  end;

  generateTAC(node,Result);

  if dstVar <> nil then begin
    if (sIndex <> -1) then begin
      if isIndexedVariable(dstVar) then begin
        value := popValue;
        generateTAC(dstVar.arrayParam,Result,TVariable(dstVar).use8BitValue);

        if (dstVar.size > 1) and not isDerefed(dstVar) then begin
          generateIndexShiftTAC(dstVar,Result,TVariable(dstVar).use8BitValue);
        end;

        right := popValue;

        index := right;//TVariable.Create(uToken.Token(ttIdent,right.toString),0);
//        if index.sizeOverride > 0 then
//          temp  := TVariable.Create(uToken.Token(ttIdent,Result.getTemp),index.sizeOverride)
//        else
        if (right.size = 3) or (right.derefRWsize = 3) then
          temp := TVariable.Create(uToken.Token(ttIdent,Result.getTemp),3)
        else
          temp := TVariable.Create(uToken.Token(ttIdent,Result.getTemp),tempWordSize);

        pushValue(temp);

        tacNode := TTACNode.Create(dstVar,nil,value,nil,index,nil,nil);
        Result.addNode(tacNode);
      end
      else begin
        value := popValue;
//        writeLn(dstVar.toString,' := ',value.toString);

        tacNode := TTACNode.Create(dstVar,nil,value,nil,nil,nil,nil);
        Result.addNode(tacNode);
      end;
    end;
  end;
end;

procedure TASTto6502.pushLoopExitLabel(exitLabel : AnsiString);
var
  i : Integer;
begin
  Inc(FloopExitLabelCount);
  FloopExitLabels[FloopExitLabelCount] := exitLabel;
end;

procedure TASTto6502.popLoopExitLabel;
begin
  Dec(FloopExitLabelCount);
  if FloopExitLabelCount < 0 then FloopExitLabelCount := 0;

end;

function  TASTto6502.peekLoopExitLabel : AnsiString;
begin
  Result := FloopExitLabels[FloopExitLabelCount];
  Dec(FloopExitLabelCount);
end;

{**
function  TASTto6502.isSimpleReadWriteIndexedPointer(aSrc,aDst : TAST) : Boolean;
var
  src,dst,srcIndex,dstIndex : AnsiString;
begin
  Result := False;

  if not aSrc.isVariable then Exit;
  if not aDst.isVariable then Exit;

  if not TVariable(aSrc).deref then Exit;
  if not TVariable(aDst).deref then Exit;

  if TVariable(aSrc).arrayParam = nil then Exit;
  if TVariable(aDst).arrayParam = nil then Exit;

  if TVariable(aSrc).arrayParam.size <> 1 then Exit;
  if TVariable(aDst).arrayParam.size <> 1 then Exit;

  if not TVariable(aSrc).arrayParam.isLeaf then Exit;
  if not TVariable(aDst).arrayParam.isLeaf then Exit;
  if not (TVariable(aSrc).arrayParam.isInteger or TVariable(aSrc).arrayParam.isVariable) then Exit;
  if not (TVariable(aDst).arrayParam.isInteger or TVariable(aDst).arrayParam.isVariable) then Exit;

  src := aSrc.toString;
  dst := aDst.toString;
  srcIndex := TCodeGen_6502.getValue_lo(TVariable(aSrc).arrayParam.toString);
  dstIndex := TCodeGen_6502.getValue_lo(TVariable(aDst).arrayParam.toString);

  emitCode('ldy '+srcIndex);
  emitCode('lda ('+src+'),y');
  emitCode('ldy '+dstIndex);
  emitCode('sta ('+dst+'),y');

  Result := True;
end;
**}
procedure TASTto6502.visitCallParam(param: TParam);
begin

end;

procedure TASTto6502.visitDecimalMode(decMode : TDecimalMode);
begin
  if decMode.isOn then
    emitCode('sed')
  else
    emitCode('cld');
end;

procedure TASTto6502.visitSegmentDecl(node : TSegmentDecl);
begin
  Fwriter.writeLn('.segment %s',[node.segmentName]);
  Fwriter.writeLn;
end;

procedure TASTto6502.visitAssign(node : TAssign);

var
  i          : Integer;
  sym        : TSymbol;
  shiftCount : Integer;
  varName1   : AnsiString;
  number1    : AnsiString;
  varName2   : AnsiString;
  number2    : AnsiString;
  doEmitExp  : Boolean;
  tacList    : TTACList;
begin
//  if (node.variable.arrayParam = nil) and (node.expr.isVariable) then begin
//    if TVariable(node.expr).isPointer then begin
//      // simple address -> variable
//      emitCode('// '+node.variable.token.tValue+' := @'+TVariable(node.expr).token.tValue);
//      emitCode('lda #<'+TVariable(node.expr).token.tValue);
//      emitCode('sta '+node.variable.token.tValue+' + 0');
//
//      if node.variable.size = 2 then begin
//        emitCode('lda #>'+TVariable(node.expr).token.tValue);
//        emitCode('sta '+node.variable.token.tValue+' + 1');
//      end;
//      Exit;
//    end;
//  end;
//
  resetTempRegVar;

  FcodeGen.sourceLine := node.sourceLine;

  if (node.variable.arrayParam = nil) and node.variable.deref then
    // add default [0] index to the deref'd variable
    node.variable.arrayParam := TNumber.Create(0);

  if (node.expr.arrayParam = nil) and node.expr.deref then
    // add default [0] index to the deref'd variable
    node.expr.arrayParam := TNumber.Create(0);

  tacList := generateTAC(node.expr,node.variable);
  tacList.decimalModeOn := node.decimalModeOn;

  tacList.optimise;

//  tacList.debugPrint;

  emitTACcode(tacList);
{**

  sym := FcurrentScope.lookup(node.variable.toString);

  // test for a^[index] := b^[index]
  if isSimpleReadWriteIndexedPointer(node.expr,node.variable) then Exit;

  if sym <> nil then begin
    // test for simple "a := a + 1" type assignment...

    if isSimpleIncrement(node,sym.size) then Exit;

    if isSimpleDecrement(node,sym.size) then Exit;

    if isSimpleAdd2(node,sym.size) then Exit;

    if isSimpleSub2(node,sym.size) then Exit;

    if isSHL(node,shiftCount) then begin
      for i := 1 to shiftCount do begin
        if sym.size = 1 then
          emitCode('asl '+node.variable.value)
        else begin

          emitCode('asl '+node.variable.value+' + 0');
          emitCode('rol '+node.variable.value+' + 1');
        end;
      end;
      Exit;
    end;

    if isSHR(node,shiftCount) then begin
      for i := 1 to shiftCount do begin
        if sym.size = 1 then
          emitCode('lsr '+node.variable.value)
        else begin

          emitCode('lsr '+node.variable.value+' + 0');
          emitCode('ror '+node.variable.value+' + 1');
        end;
      end;
      Exit;
    end;

    if node.expr.isVariable then begin
      if isSimpleArrayIndex(TVariable(node.expr).arrayParam,varName1,number1) and
         isSimpleArrayIndex(node.variable.arrayParam,varName2,number2) then begin
      // check expression to see if is array index load
        if (varName1 <> '') then begin
          emitCode('ldy '+varName1);
          emitCode('lda '+TVariable(node.expr).token.tValue+' + ' + number1+',y');
        end else begin
          emitCode('lda '+TVariable(node.expr).token.tValue+' + ' + number1);
        end;
        // load part
        if (varName2 <> '') then begin
          emitCode('ldy '+varName2);
          emitCode('sta '+sym.symName+' + ' + number2+',y');
        end else begin
          emitCode('sta '+sym.symName+' + '+number2);
        end;

        Exit;
      end;
    end;
  end;

//  writeLn('-----------------------------');

  Exit;

  doEmitExp := True;

  if node.expr.isVariable then begin
    if isSimpleArrayIndex(TVariable(node.expr).arrayParam,varName1,number1) then begin
    // check expression to see if is array index load
      if (varName1 <> '') then begin
        emitCode('ldy '+varName1);
        emitCode('lda '+TVariable(node.expr).token.tValue+' + ' + number1+',y');
      end else begin
        emitCode('lda '+TVariable(node.expr).token.tValue+' + ' + number1);
      end;

      doEmitExp := False;
    end
    else begin
      if TVariable(node.expr).deref then begin
        emitCode('ldy #0');
        emitCode('lda ('+TVariable(node.expr).token.tValue+'),y');

        doEmitExp := False;
      end;
    end;
  end;

  if doEmitExp then emitExpression(node.expr);

  // emit variable part of assignment (the store)
  if isSimpleArrayIndex(node.variable.arrayParam,varName2,number2) then begin
    if (varName2 <> '') then begin
      emitCode('ldy '+varName2);
      emitCode('sta '+sym.symName+' + ' + number2+',y');
    end else begin
      emitCode('sta '+sym.symName+' + '+number2);
    end;
  end
  else if node.variable.deref then begin
    // is a derefed pointer
    emitCode('ldy #0');
    emitCode('sta ('+sym.symName+'),y');
  end
  else begin
    if (sym.size = 1) then begin
      emitCode('sta '+node.variable.token.tValue);
    end
    else begin
      if node.expr.isLeaf then begin
        if node.expr.isInteger then begin
          if (TNumber(node.expr).size > 1) then begin
            if not (node.variable.deref) then begin
              emitCode('lda #<'+TNumber(node.expr).token.tValue);
              emitCode('sta '+sym.symName + ' + 0');
              emitCode('lda #>'+TNumber(node.expr).token.tValue);
              emitCode('sta '+sym.symName + ' + 1');
            end else begin
              emitCode('ldy #0');
              emitCode('lda #<'+TNumber(node.expr).token.tValue);
              emitCode('sta ('+sym.symName + '),y');
              emitCode('iny');
              emitCode('lda #>'+TNumber(node.expr).token.tValue);
              emitCode('sta ('+sym.symName + '),y');
            end;
          end
          else begin
            if not (node.variable.deref) then begin
              emitCode('lda #'+TNumber(node.expr).token.tValue);
              emitCode('sta '+sym.symName);
            end else begin
              emitCode('ldy #0');
              emitCode('lda #'+TNumber(node.expr).token.tValue);
              emitCode('sta ('+sym.symName+'),y');
            end;
          end;
        end;
      end;
    end;
  end;

  //  node.expr.acceptVisitor(self);
**}
end;

procedure TASTto6502.visitCall(node : TCall);
var
  p         : Integer;
  param     : TVarSymbol;
  callParam : TAST;
  procSym   : TProcSymbol;
  loReg     : AnsiString;
  hiReg     : AnsiString;
  value     : AnsiString;
  dstVar    : TVariable;
  assign    : TAssign;
begin
  procSym := TProcSymbol(FcurrentScope.lookup(node.procName));

  if procSym.procType = ttImportBinary then begin
  // for this type, just call the address directly with no main label
    emitCode('jsr '+node.procName);
    Exit;
  end;

  if procSym.onStack then expandStack(procSym.stackSize);

  for p := 0 to Length(procSym.params) - 1 do begin
    // load params
    param     := procSym.params[p];
    callParam := TAST(node.params.Items[p]);

    dstVar := TVariable.Create(node.procName + '.' + param.symName,param.size);

    if (procSym.procType = ttAssembler) and isAsmParam(param.symName,param.symType,loReg,hiReg) then begin
    // is a asm param so use correct register(s) to load into

      dstVar.isRegister := True;
      dstVar.loReg      := loReg;
      dstVar.hiReg      := hiReg;
    end;

    assign := TAssign.Create(dstVar,callParam);
    assign.acceptVisitor(Self);

    emitSaveRegVars();
  end;

  if procSym.procAddress <> '' then
  // is absolute asm proc so use that address
    emitCode('jsr '+procSym.procAddress)
  else begin
    emitCode('jsr '+node.procName+'.'+MAIN_LABEL);
  end;

  if procSym.onStack then shrinkStack(procSym.stackSize);
end;

procedure TASTto6502.visitMacroCall(node : TMacroCall);
var
  i : Integer;
begin
  Fwriter.write(indent+':'+node.macroName);

  Fwriter.write('(');

  for i := 0 to node.params.Count - 1 do begin
    Fwriter.write(node.params.Strings[i]);
    if (i < node.params.Count - 1) then Fwriter.write(',');
  end;

  Fwriter.writeLn(')');
end;

//procedure TASTto6502.visitFuncCall(node : TFuncCall);
//var
//  p         : Integer;
//  param     : TVarSymbol;
//  callParam : TAST;
//  procSym   : TFuncSymbol;
//  loReg     : AnsiString;
//  hiReg     : AnsiString;
//  value     : AnsiString;
//  dstVar    : TVariable;
//  assign    : TAssign;
//begin
//  procSym := TFuncSymbol(FcurrentScope.lookup(node.funcName));
//
//  if procSym.funcType = ttImportBinary then begin
//  // for this type, just call the address directly with no main label
//    emitCode('jsr '+node.funcName);
//    Exit;
//  end;
//
//  resetTempRegVar();
//
//  for p := 0 to Length(procSym.params) - 1 do begin
//    // load params
//    param := procSym.params[p];
//
//    callParam := TAST(node.params.Items[p]);
//{    FfillExpTokens := True;
//    resetTokens;
//    TAST(node.params.Items[p]).acceptVisitor(self);
//    FfillExpTokens := False;}
//
//    dstVar := TVariable.Create(node.funcName + '.' + param.symName,param.size);
//
//    if (procSym.funcType = ttAssembler) and isAsmParam(param.symName,param.symType,loReg,hiReg) then begin
//    // is a asm param so use correct register(s) to load into
//      dstVar.isRegister := True;
//      dstVar.loReg      := loReg;
//      dstVar.hiReg      := hiReg;
//
//{      value := leafNodeToImmOrAddr(FExpTokens[0]);
//
//      if (value[1] = '#') then
//        emitCode('ld'+loReg+' #<'+Copy(value,2,Length(value)))
//      else
//        emitCode('ld'+loReg+' '+value+' + 0');
//
//      if hiReg <> '' then
//        if (value[1] = '#') then
//          emitCode('ld'+hiReg+' #>'+Copy(value,2,Length(value)))
//        else
//          emitCode('ld'+hiReg+' '+value+' + 1');}
//    end else begin
////      emitCode('lda '+leafNodeToImmOrAddr(FExpTokens[0]));
//
////      emitCode('sta '+node.procName + '.' + param.symName);
//    end;
//
//    assign := TAssign.Create(dstVar,callParam);
//    assign.acceptVisitor(Self);
//  end;
//
//  emitSaveRegVars();
//
//  if procSym.funcAddress <> '' then
//  // is absolute asm proc so use that address
//    emitCode('jsr '+procSym.funcAddress)
//  else
//    emitCode('jsr '+node.funcName+'.'+MAIN_LABEL);
//end;


procedure TASTto6502.visitFuncCall(node : TFuncCall);
var
  p       : Integer;
  param   : TVarSymbol;
  funcSym : TFuncSymbol;
  callParam : TAST;
  loReg   : AnsiString;
  hiReg   : AnsiString;
  value   : AnsiString;
  dstVar  : TVariable;
  assign  : TAssign;
begin
  if FflattenExpression then begin
    pushExpNode(node);
    Exit;
  end;

  funcSym := TFuncSymbol(FcurrentScope.lookup(node.funcName));

  if funcSym.onStack then expandStack(funcSym.stackSize);

  for p := 0 to Length(funcSym.params) - 1 do begin
    // load params
    param     := funcSym.params[p];
    callParam := TAST(node.params.Items[p]);

    dstVar := TVariable.Create(node.funcName + '.' + param.symName,param.size);

    if (funcSym.funcType = ttAssembler) and isAsmParam(param.symName,param.symType,loReg,hiReg) then begin
    // is a asm param so use correct register(s) to load into
      dstVar.isRegister := True;
      dstVar.loReg      := loReg;
      dstVar.hiReg      := hiReg;
    end;

    assign := TAssign.Create(dstVar,callParam);
    assign.acceptVisitor(Self);

    emitSaveRegVars();
  end;
end;

var
  tokenStack : array[0..100] of TExpToken;
  tokenIndex : Integer;

procedure pushExpToken(aToken : TExpToken);
begin
  Inc(tokenIndex);
  tokenStack[tokenIndex] := aToken;
end;

function  popExpToken: TExpToken;
begin
  Result := tokenStack[tokenIndex];
  Dec(tokenIndex);
end;

procedure TASTto6502.tokenizeExpression(node : TAST; aResetTokens : Boolean);
var
  i : Integer;
begin
  FfillExpTokens := True;
  FuseLargeTempVars := False;

  if aResetTokens then resetTokens;

  node.acceptVisitor(self);

  FfillExpTokens := False;
end;

function  TASTto6502.flattenExpression(node : TAST; aExpression : TList = nil) : TList;
begin
  FflattenExpression := True;
  FuseLargeTempVars    := False;

  if (aExpression = nil) then
    Fexpression := TList.Create
  else
    Fexpression := aExpression;

  node.acceptVisitor(self);

  FflattenExpression := False;

  Result := Fexpression;
end;

procedure TASTto6502.debugExpression;
var
  i : Integer;
  e : TAST;
begin
  for i := 0 to Fexpression.Count - 1 do begin
    e := TAST(Fexpression.Items[i]);

    if      e.isInteger      then write(TNumber(e).value,' ')
    else if e.isVariable     then write(TVariable(e).value,' ')
    else if (e is TBinOp)    then write(TBinOp(e).op.tValue,' ')
    else if (e is TUnaryOp)  then write(TUnaryOp(e).op.tValue,' ')
    else if (e is TFuncCall) then write(TFuncCall(e).funcName,'() ')
  end;

  writeLn;
end;

procedure TASTto6502.emitCompare_16(left,right : TExpToken; lower,same,higher : Boolean; falseLabel : AnsiString);
begin
  emitCode('lda ' + getLeafNodeHi(left));
  emitCode('cmp ' + getLeafNodeHi(right));
  emitCode('bne !+');
  emitCode('lda ' + getLeafNodeLo(left));
  emitCode('cmp ' + getLeafNodeLo(right));
  emitLabel('!');

  if lower then
    emitCode('bcc !+ // is <')
  else
    emitCode('bcc '+falseLabel + ' // is < (false)');

  if higher then
    emitCode('bne !+ // is >')
  else
    emitCode('bne '+falseLabel + ' // is > (false)');

  if same then
    emitCode('beq !+ // is =')
  else
    emitCode('beq '+falseLabel + ' // is = (false)');

  emitCode('// is true');
  emitLabel('!');
end;

procedure TASTto6502.emitExpression(node : TAST; falseLabel : AnsiString = '');
var
  i,c        : Integer;
  code       : Integer;
  expStarted : Boolean;
  left,right : TExpToken;
  expResult  : TExpToken;
begin
  if (node = nil) then Exit;

  expResult.token := Token(ttEOF,'Result');
  expResult.size  := 0;

  expStarted := False;
  tokenIndex := -1;
  tokenizeExpression(node,true);

//  Fwriter.write(indent+'// expression: ');

//  for i := 0 to High(FExpTokens) do
//    Fwriter.write(indent+FExpTokens[i].token.tValue+' ');

//  Fwriter.writeLn();

  if (Length(FExpTokens) = 1) then begin
    // a single literal num/variable
    emitCode('lda '+leafNodeToImmOrAddr(FExpTokens[0]));
    Exit;
  end;

  for i  := 0 to High(FExpTokens) do begin
    if (FExpTokens[i].token.tType in [ttIdent,ttIntNum]) then begin
      // is operand
      pushExpToken(FExpTokens[i]);
    end
    else begin
      if (FExpTokens[i].token.tType in [ttNot,ttNeg]) then begin
        // is a unary op
        right := popExpToken;

        // push pretend expression result onto stack
        pushExpToken(expResult);
      end else begin
      // is a binary op
        right := popExpToken;
        left  := popExpToken;

        if not (expStarted) then begin
          // emit left hand side of expression
          emitCode('lda '+leafNodeToImmOrAddr(left));
          expStarted := True;
        end;

        // push pretend expression result onto stack
        pushExpToken(expResult);

        // do right hand of expression
        case FExpTokens[i].token.tType of
          ttPlus  : begin
            emitCode('clc');
            emitCode('adc '+leafNodeToImmOrAddr(right));
          end;
          ttMinus :  begin
            emitCode('sec');
            emitCode('sbc '+leafNodeToImmOrAddr(right));
          end;
          ttTimes :       emitCode('// do multiply!');
          ttSlash,ttDiv : emitCode('// do division!');
          ttShr   : begin
            Val(right.token.tValue,c,code);
            for c := 1 to c do emitCode('lsr');
          end;
          ttShl   : begin
            Val(right.token.tValue,c,code);
            for c := 1 to c do emitCode('asl');
          end;
          ttAnd   :       emitCode('and '+leafNodeToImmOrAddr(right));
          ttOr    :       emitCode('ora '+leafNodeToImmOrAddr(right));
          ttXor   :       emitCode('eor '+leafNodeToImmOrAddr(right));
          ttEql   :       begin
            emitCode('cmp '+leafNodeToImmOrAddr(right));
            if (falseLabel <> '') then
//              emitCode('bne !+');
              emitCode('beq !+');
              emitCode('jmp '+falseLabel+' // jump to false label');
              emitLabel('!');
          end;
        end;
      end;
    end;
  end;
end;

procedure TASTto6502.emitCondition(expr : TAST; falseLabel : AnsiString);
var
  doEmitExp : Boolean;
  tacList   : TTACList;
  dstVar    : TVariable;
  lastNode  : TTACNode;
  tacNode   : TTACNode;
  temp      : TVariable;
begin
  FcodeGen.doingCondition := True;
  FdoingCondition         := True;

  tacList := TTACList.Create;

  if expr.isLeaf then begin
  // just load this variable into the A register to check for false
    dstVar := TVariable.Create(cA_REGISTER,1);

    tacList := generateTAC(expr,dstVar);
  end
  else
    generateTAC(expr,tacList);

  tacList.optimise;

  // delete last dest node for condition (doesn't need to store it)
  if (tacList.count > 0) then begin
    lastNode := tacList.Items[tacList.count - 1];

    if tacList.isTemp(lastNode.left) then
    lastNode.left := nil;
  end;

  if tacList.count = 1 then begin
    tacNode := tacList.Items[0];
    tacNode.falseLabel := falseLabel;

    emitTACcode(tacList);
  end
  else begin
    emitTACcode(tacList);
    emitCode('beqLong '+falseLabel+' //0 = false, other = true');
  end;

  FcodeGen.doingCondition := false;
  FdoingCondition         := False;
end;

procedure TASTto6502.visitIfThen(node : TIfThen);
var
  ifTrueLabel : AnsiString;
  ifElseLabel : AnsiString;
  ifEndLabel  : AnsiString;
begin
  ifTrueLabel := newLabel;
  ifElseLabel := newLabel;
  ifEndLabel  := newLabel;

//  emitCode('//if-then');

  resetTempRegVar();

  FcodeGen.sourceLine := node.sourceLine;

  emitCondition(node.condition,ifElseLabel);

//  node.condition.acceptVisitor(self);

  node.trueBranch.acceptVisitor(self);

  emitCode('jmp '+ifEndLabel);

  emitCode('//else');

  emitLabel(ifElseLabel);

  if (node.elseBranch <> nil) then begin

    node.elseBranch.acceptVisitor(self);
  end;

  emitLabel(ifEndLabel);
end;

procedure TASTto6502.visitWhile(node : TWhile);
var
  whileLabel : AnsiString;
  falseLabel : AnsiString;
  isInfinite : Boolean;
begin
  whileLabel := newLabel;
  falseLabel := newLabel;

//  emitCode('// while loop');

  // check for infinite loop and don't emit condition!!
  FcodeGen.sourceLine := node.sourceLine;

  isInfinite := False;

  if (node.condition.isInteger) then
    isInfinite := (TNumber(node.condition).value <> 0);

  emitLabel(whileLabel);

  if not isInfinite then emitCondition(node.condition,falseLabel);

//  node.condition.acceptVisitor(self);

  pushLoopExitLabel(falseLabel);
  node.statement.acceptVisitor(self);

  Fwriter.writeLn();
  emitCode('jmp '+whileLabel);

  emitLabel(falseLabel);

  popLoopExitLabel;
end;

procedure TASTto6502.visitRepeat(node : TRepeat);
var
  repeatLabel : AnsiString;
  falseLabel  : AnsiString;
  i           : Integer;
begin
  repeatLabel := newLabel;
  falseLabel := newLabel;

  FcodeGen.sourceLine := node.sourceLine;

  emitLabel(repeatLabel);

  pushLoopExitLabel(falseLabel);

  for i := 0 to node.children.Count - 1 do
    TAST(node.children.Items[i]).acceptVisitor(self);

  emitCondition(node.condition,repeatLabel);

  Fwriter.writeLn();

  emitLabel(falseLabel);

  popLoopExitLabel;
end;

procedure TASTto6502.visitFor(node : TFor);
var
  forLabel   : AnsiString;
  falseLabel : AnsiString;
begin
  forLabel   := newLabel;
  falseLabel := newLabel;

//  emitCode('// while loop');

  FcodeGen.sourceLine := node.sourceLine;

//  writeLn(node.statement.sourceLine);

  node.initializer.acceptVisitor(self);

  emitLabel(forLabel);

  if node.is0to255loop then begin
    try
      pushLoopExitLabel(falseLabel);
      node.statement.acceptVisitor(self);

      Fwriter.writeLn();

      emitCode('inc '+node.initializer.variable.toString);
//      emitCode('lda '+node.initializer.variable.toString);
      emitCode('beq '+falseLabel);
      emitCode('jmp '+forLabel);

      emitLabel(falseLabel);
    finally
      popLoopExitLabel;
    end;
  end
  else
  if node.isNdownto0loop then begin
    try
      pushLoopExitLabel(falseLabel);
      node.statement.acceptVisitor(self);

      Fwriter.writeLn();

      emitCode('dec '+node.initializer.variable.toString);
//      emitCode('lda '+node.initializer.variable.toString);
      emitCode('lda '+node.initializer.variable.toString);
      emitCode('cmp #255');
      emitCode('beq '+falseLabel);
      emitCode('jmp '+forLabel);

      emitLabel(falseLabel);
    finally
      popLoopExitLabel;
    end;
  end
  else begin
    emitCondition(node.condition,falseLabel);

  //  node.condition.acceptVisitor(self);

    try
      pushLoopExitLabel(falseLabel);
      node.statement.acceptVisitor(self);

      Fwriter.writeLn();
      node.incrementer.acceptVisitor(self);

      emitCode('jmp '+forLabel);

      emitLabel(falseLabel);
    finally
      popLoopExitLabel;
    end;
  end;
end;

procedure TASTto6502.visitNumber(node : TNumber);
begin
  if (FfillExpTokens) then begin
    pushToken(node.token,node.size);
    if node.size > 1 then FuseLargeTempVars := True;
  end
  else
  if FflattenExpression then begin
    pushExpNode(node);
    if node.size > 1 then FuseLargeTempVars := True;
  end;
//  write(node.token.tValue,' ');
end;

procedure TASTto6502.visitVariable(node : TVariable);
begin
  if (FfillExpTokens) then begin
    pushToken(node.token,node.size);
    if node.size > 1 then FuseLargeTempVars := True;

    if node.arrayParam <> nil then begin
      pushToken(Token(ttLBracket,'['),0);
      tokenizeExpression(node.arrayParam,false);
      pushToken(Token(ttRBracket,']'),0);
    end;

  end
  else
  if FflattenExpression then begin
    pushExpNode(node);
    if node.size > 1 then FuseLargeTempVars := True;
  end;
//  write(node.token.tValue,' ');
end;

procedure TASTto6502.visitString(node : TString);
begin
  if (FfillExpTokens) then
    pushToken(node.token,node.size)
  else
  if FflattenExpression then begin
    pushExpNode(node);
  end;
//  write(node.token.tValue,' ');
end;

procedure TASTto6502.visitBinOp(node : TBinOp);
begin
//  if (node.left.size = 1) and (node.right.size = 1) then begin
//    emitBinOp_1_1(node);
//  end;
  node.left.acceptVisitor(self);

  node.right.acceptVisitor(self);

  if (FfillExpTokens) then
    pushToken(node.op,node.size)
  else
  if FflattenExpression then begin
    if node.op.tType in [ttPlus,ttMinus] then
      FuseLargeTempVars := True;

    pushExpNode(node);
  end;
//  write(node.op.tValue,' ');
end;

procedure TASTto6502.visitUnaryOp(node : TUnaryOp);
begin
{  Write('(');
  write(node.op.tValue,' ');
  node.expr.acceptVisitor(self);
  Write(')');}

  node.expr.acceptVisitor(self);

  if (FfillExpTokens) then
    pushToken(node.op,node.size)
  else
  if FflattenExpression then
    pushExpNode(node);
end;

procedure TASTto6502.visitCompound (node : TCompound);
var
  i : Integer;
begin
//  incIndent;
  for i := 0 to node.children.Count - 1 do
    TAST(node.children.Items[i]).acceptVisitor(self);
//  decIndent;
end;

procedure TASTto6502.visitAsmCompound(node : TAsmCompound);
var
  i : Integer;
begin
  for i := 0 to node.asmStatements.Count - 1 do begin
  // output asm statements
    emitCode(Trim(node.asmStatements.Strings[i]));
  end;
end;

procedure TASTto6502.emitVarDecl(varDecl : TVarDecl);
var
  varSize    : Integer;
  i          : Integer;
  shift      : Byte;
  code       : Integer;
  v          : Integer;
  hex        : AnsiString;
  defaultHex : AnsiString;
  sym        : TSymbol;
begin
  if varDecl.onStack then Exit;

  if (varDecl.address <> '') then begin
    // is absolute address variable
    if FcurrentScope.scopeLevel = 1 then begin
      Fwriter.writeLn('.var '+varDecl.varName+' ='+varDecl.address);

      for i := 0 to varDecl.size - 1 do
        Fwriter.writeLn('.var '+varDecl.varName+'.b'+IntToStr(i)+' = '+varDecl.address);
    end else begin
      // is proc scope
      Fwriter.writeLn('.label '+varDecl.varName+' ='+varDecl.address);

      for i := 0 to varDecl.size - 1 do
        Fwriter.writeLn('.label '+varDecl.varName+'.b'+IntToStr(i)+' = '+varDecl.address);
    end;
    Exit;
  end;

  if (varDecl.varSym = nil) then
    varSize := varDecl.size
  else
    varSize := varDecl.varSym.size;

  Fwriter.write(indent+'%s: { ',[varDecl.varName]);

  if varDecl.defaultValue = '' then begin
    // fill with default 0 value
    if varDecl.isArray then begin
      if not varDecl.isPointer then begin
        // fill array bytes
        Fwriter.write('.byte ');
        for i := 0 to (varDecl.arraySize * varSize) - 1 do begin
          Fwriter.write('0');
          if i < (varDecl.arraySize * varSize) - 1 then Fwriter.write(',');
        end;
      end;
    end
    else begin
      if varDecl.isString then begin
        // write string length
        Fwriter.write('.byte %d,',[varSize]);

        for i  := 1 to VarSize - 1 do begin
          // pad with spaces
          Fwriter.write('%d',[32]);
          if (i < varSize - 1) then Fwriter.write(',');
        end;
      end
      else begin
        for i := 0 to varSize - 1 do begin
          Fwriter.write('b%d: .byte 0',[i]);
          if (i < varSize - 1) then Fwriter.write(';');
        end;

        if varSize = 3 then begin
          Fwriter.writeLn();
          Fwriter.writeLn('.label loword = b0');
          Fwriter.writeLn('.label hiword = b1');
        end
        else
        if varSize = 4 then begin
          Fwriter.writeLn();
          Fwriter.writeLn('.label loword = b0');
          Fwriter.writeLn('.label hiword = b2');
        end;
      end;
    end;
  end else begin
    // fill with defaultValue

    defaultHex := StrIntToHex(varDecl.defaultValue);
    while Length(defaultHex) < (varSize * 2) do
      defaultHex := '0' + defaultHex;

    for i := 0 to varSize - 1 do begin
      shift := i * 8;
      hex := Copy(defaultHex,Length(defaultHex) - 1-i*2,2);

      Fwriter.write('b%d: .byte $%s',[i,hex]);
      if (i < varSize - 1) then Fwriter.write(';');
    end;
  end;

  Fwriter.writeLn(' }');
end;

type
  TrecVarInfo = record
    name : AnsiString;
  end;

var
  recVarInfo   : array of TrecVarInfo;
  recVarMemOfs : Integer;

procedure resetRecVarInfo;
begin
  SetLength(recVarInfo, 0);
  recVarMemOfs := 0;
end;

procedure pushRecVarInfo(aName: AnsiString);
var
  i: Integer;
begin
  i := Length(recVarInfo);
  SetLength(recVarInfo, i + 1);
  recVarInfo[i].name   := aName;
end;

procedure popRecVarName;
begin
  SetLength(recVarInfo, High(recVarInfo));
end;

function getRecVarName(aVarDecl : TVarDecl) : AnsiString;
var
  i : Integer;
begin
  Result := '';
  for i := 0 to Length(recVarInfo) - 1 do
  begin
    Result := Result + recVarInfo[i].name;
    if i < Length(recVarInfo) - 1 then
      Result := Result + '.';
  end;
  Result := Result + '.' + aVarDecl.varName;
end;

procedure TASTto6502.emitRecordVarDecl2(aVarDecl: TVarDecl; aVarAddress : AnsiString);
var
  i       : Integer;
  varName : AnsiString;
  varSize : Integer;
  sym     : TSymbol;
  varAddr : AnsiString;
begin
  if (aVarDecl.varSym = nil) then
    varSize := aVarDecl.size
  else
    varSize := aVarDecl.varSym.size;

  if (aVarDecl.varSym <> nil) and (aVarDecl.varSym.symType is TRecordSymbol) then begin
    // is record so emit record header and children var decls
    pushRecVarInfo(aVarDecl.varName);

    sym := FcurrentScope.lookup(aVarDecl.varSym.symName);

    if aVarAddress = '' then
      Fwriter.writeLn(indent+'%s: {',[aVarDecl.varName])
    else begin
      if aVarDecl.address = '' then
        varAddr := Format('%s + $%s /* %d */',[aVarAddress,IntToHex(sym.memOfs,2),sym.memOfs])
      else
        varAddr := Format('%s',[aVarDecl.address]);

      Fwriter.writeLn(indent+'.label %s = %s {',[aVarDecl.varName,varAddr])
    end;

    incIndent;
    for i := 0 to TRecordSymbol(aVarDecl.varSym.symType).vars.count - 1 do
      emitRecordVarDecl2
        (TVarDecl(TRecordSymbol(aVarDecl.varSym.symType).vars.Items[i]),aVarAddress);

    decIndent;
    Fwriter.writeLn(indent+'}');

    popRecVarName;
  end
  else
  begin
    varName := '';
    for i := 0 to Length(recVarInfo) - 1 do
    begin
      varName := varName + recVarInfo[i].name;
      if i < Length(recVarInfo) - 1 then
        varName := varName + '.';
    end;
    varName := varName + '.' + aVarDecl.varName;

    sym := FcurrentScope.lookup(aVarDecl.varSym.symName);
    sym.memOfs := recVarMemOfs;

    if aVarAddress = '' then begin
    // normal record var in ram
//      Fwriter.writeLn(indent+'// %s (%s)',[varName,aVarDecl.varSym.symName]);

      if aVarDecl.address <> '' then begin
        if varSize = 1 then
          Fwriter.writeLn(indent+'.label %s = %s',[aVarDecl.varName,aVarDecl.address])
        else
          Fwriter.writeLn(indent+'.label %s = %s {',[aVarDecl.varName,aVarDecl.address]);

        if varSize > 1 then begin
          for i := 0 to varSize - 1 do begin
            Fwriter.writeLn(indent+'  .label b%d = %s + %d',[i,aVarDecl.varName,i]);
          end;
          Fwriter.writeLn(indent+'}');
        end;

      end
      else begin
        // this var is not at absolute address
        if varSize = 1 then
          Fwriter.writeLn(indent+'%s: .byte 0',[aVarDecl.varName])
        else begin
          Fwriter.write(indent+'%s: {',[aVarDecl.varName]);
          incIndent;
          // writer separate b? parts
          for i := 0 to varSize - 1 do begin
            Fwriter.write('b%d: .byte 0',[i]);
            if i < varSize - 1 then Fwriter.write(';');
          end;
          Fwriter.writeLn();

          if varSize = 3 then begin
            Fwriter.writeLn();
            Fwriter.writeLn(indent+'  .label loword = b0');
            Fwriter.writeLn(indent+'.label hiword = b1');
          end
          else
          if varSize = 4 then begin
            Fwriter.writeLn();
            Fwriter.writeLn(indent+'  .label loword = b0');
            Fwriter.writeLn(indent+'  .label hiword = b2');
          end;

          decIndent;
          Fwriter.writeLn(indent+'}');
        end;
      end;
    end
    else begin
    // record at absolute position in ram
      if aVarDecl.address = '' then
        varAddr := Format('%s + $%s /* %d */',[aVarAddress,IntToHex(sym.memOfs,2),sym.memOfs])
      else
        varAddr := Format('%s',[aVarDecl.address]);

//      Fwriter.writeLn(indent+'// %s (%s)',[varName,aVarDecl.varSym.symName]);
      if varSize = 1 then
        Fwriter.writeLn(indent+'.label %s = %s',[aVarDecl.varName,varAddr])
      else begin
        Fwriter.writeLn(indent+'.label %s = %s {',[aVarDecl.varName,varAddr]);
        incIndent;
        // writer separate b? parts
        for i := 0 to varSize - 1 do begin
          Fwriter.writeLn(indent+'  .label b%d = %s + $%s',[i,aVarDecl.varName,IntToHex(i,2),i]);
        end;

        if varSize = 3 then begin
          Fwriter.writeLn();
          Fwriter.writeLn(indent+'  .label loword = b0');
          Fwriter.writeLn(indent+'  .label hiword = b1');
        end
        else
        if varSize = 4 then begin
          Fwriter.writeLn();
          Fwriter.writeLn(indent+'  .label loword = b0');
          Fwriter.writeLn(indent+'  .label hiword = b2');
        end;

        decIndent;
        Fwriter.writeLn(indent+'}');
      end;

//      if varSize > 1 then
//        Fwriter.writeLn(indent+'}');
    end;

    Inc(recVarMemOfs,sym.size);
  end;
end;
(*
procedure TASTto6502.emitRecordVarDecl(varDecl : TVarDecl; varAddress : AnsiString; varLevel : Integer; isRecordVar : Boolean);
var
  varSize    : Integer;
  i          : Integer;
  shift      : Byte;
  code       : Integer;
  v          : Integer;
  hex        : AnsiString;
  defaultHex : AnsiString;
  sym        : TSymbol;
  memOfs     : Integer;
  name       : AnsiString;
begin
//  sym := FcurrentScope.lookup(fullyQualifiedName);

  memOfs := 0;//sym.memOfs;

{
.label myRec = 1024 {
.label b0 = myRec + 0
.label b1 = myRec + 1
}
    // is record so emit record header and children var decls
  if isRecordVar then begin
    if varAddress = '' then
      Fwriter.write('.var %s : { ',[varDecl.varName])
    else begin
      if varLevel = 0 then
        Fwriter.write('.label %s = %s { ',[varDecl.varName,varAddress])
      else
        Fwriter.write('.label %s = %s + %d { ',[varDecl.varName,varAddress,memOfs]);
    end;

    incIndent;

    Fwriter.writeLn;

    pushRecVarInfo(varDecl.varName);

    for i := 0 to TRecordSymbol(varDecl.varSym.symType).vars.Count - 1 do begin
      isRecordVar := (varDecl.varSym <> nil) and (varDecl.varSym is TRecordSymbol);

      name := TVarDecl(TRecordSymbol(varDecl.varSym.symType).vars.Items[i]).varName;
      writeLn(name);

      emitRecordVarDecl(TVarDecl(TRecordSymbol(varDecl.varSym.symType).vars.Items[i]),varAddress,varLevel+1,isRecordVar);
    end;

    popRecVarName;

    decIndent;
    Fwriter.writeLn(' }');

    Exit;
  end;

  if (varDecl.varSym = nil) then
    varSize := varDecl.size
  else
    varSize := varDecl.varSym.size;

  Fwriter.write(indent+'%s: { ',[varDecl.varName]);

  if varDecl.defaultValue = '' then begin
    if varAddress = '' then begin
      // fill with default 0 value
      for i := 0 to varSize - 1 do begin
        Fwriter.write('b%d: .byte 0',[i]);
        if (i < varSize - 1) then Fwriter.write(';');
      end;
    end
    else
    begin
      // is absolute variable so write labels for different sections
      for i := 0 to varSize - 1 do
        Fwriter.writeLn('.label b%d = %s + %d',[i,varAddress,i]);
    end;

    if varSize = 3 then begin
      Fwriter.writeLn();
      Fwriter.writeLn('.label loword = b0');
      Fwriter.writeLn('.label hiword = b1');
    end
    else
    if varSize = 4 then begin
      Fwriter.writeLn();
      Fwriter.writeLn('.label loword = b0');
      Fwriter.writeLn('.label hiword = b2');
    end;
  end;

  Fwriter.writeLn(' }');
end;
*)
procedure TASTto6502.visitVarDecl  (node : TVarDecl);
var
  sym : TSymbol;
begin
  sym := FcurrentScope.lookup(node.varName);

  if not sym.isVolatile and not node.isParam and (sym.refCount = 0) then begin
    emitWarning('Removing unused variable "%s"...',[sym.symName]);
    Exit;
  end;

  resetRecVarInfo;

  Fwriter.writeLn;
  if (node.varSym <> nil) and (node.varSym.symType is TRecordSymbol) then
    emitRecordVarDecl2(node,node.address)
  else
    emitVarDecl(node);
end;

procedure TASTto6502.visitConstDecl(node : TConstDecl);
var
  varSize  : Integer;
  i        : Integer;
  num      : TNumber;
  c        : Byte;
  constSym : TConstSymbol;
begin
  if (node.constType <> nil) then begin
    // typed constant
    varSize := node.constType.size;

    Fwriter.writeLn();

    Fwriter.write(indent+'%s: { ',[node.constName]);

    if not node.isArray then begin
      if (node.constValue.tType in [ttString,ttPetscii,ttAscii]) then begin
        Fwriter.write('.byte %d',[Length(node.constValue.tValue)]);

        for i := 1 to Length(node.constValue.tValue) do begin

          // 'strings' default to CBM screen code
          c := cPETSCIIToCBMScreenCode[cASCIIToPETSCIITable[Ord(node.constValue.tValue[i])]];

          constSym := TConstSymbol(FcurrentScope.lookup(node.constName));

          if assigned(constSym) then begin
            if constSym.symType <> nil then begin
              if constSym.symType.symName  = 'ascii' then
                c := Ord(node.constValue.tValue[i])//cPETSCIIToCBMScreenCode[cASCIIToPETSCIITable[Ord(node.constValue.tValue[i])]]
              else
              if constSym.symType.symName = 'petscii' then
                c := cASCIIToPETSCIITable[Ord(node.constValue.tValue[i])];
            end;
          end;

//          if (node.constValue.tType = ttAscii) then
//            // replace char with ascii code
//            c := Ord(node.constValue.tValue[i])//cPETSCIIToCBMScreenCode[cASCIIToPETSCIITable[Ord(node.constValue.tValue[i])]]
//          else
//          if (node.constValue.tType = ttPetscii) then
//            // replace char with petscii code
//            c := cASCIIToPETSCIITable[Ord(node.constValue.tValue[i])];

          Fwriter.write(',$%.2x',[c]);
        end;
      end
      else begin
        // non-string typed const
        if (varSize = 1) then
          Fwriter.writeLn('.byte <%s',[node.constValue.tValue])
        else
        if (varSize = 2) then
          Fwriter.writeLn('.byte <%s, >%s',[node.constValue.tValue,node.constValue.tValue])
        else begin
          Fwriter.write('.byte ');

          for i := 0 to varSize - 1 do begin
            Fwriter.write('(%s >> %d)',[node.constValue.tValue,i*8]);

            if i < varSize - 1 then Fwriter.write(',');
          end;

          Fwriter.writeLn();
        end;
      end;
    end
    else begin
      // array typed constant
      constSym := TConstSymbol(FcurrentScope.lookup(node.constName));

      if constSym.isKickAsmData then begin
        Fwriter.writeLn();
        Fwriter.writeLn(constSym.kickAsmData);
      end
      else begin
        Fwriter.write('.byte ');
        for i := 0 to high(node.values) do begin
          if (varSize = 1) then begin
            if constSym.isMsbValues then
              Fwriter.write('>%s',[node.values[i].value])
            else
              Fwriter.write('<%s',[node.values[i].value]);
          end
          else
            Fwriter.write('<%s, >%s',[node.values[i].value,node.values[i].value]);

          if i < high(node.values) then Fwriter.write(',');
        end;
        Fwriter.writeLn();
      end;
    end;

    Fwriter.writeLn(' }');
  end
  else begin
    // normal constant
    if (node.constValue.tType in [ttString,ttPetscii,ttAscii]) then
      emitCode(Format('.const %s = "%s"',[node.constName,node.constValue.tValue]))
    else
      emitCode(Format('.const %s = %s',[node.constName,node.constValue.tValue]));
  end;
end;

procedure TASTto6502.visitProcDecl (node : TProcDecl);
var
  procSym   : TProcSymbol;
  startLine : Integer;
  endLine   : Integer;
begin
  FcodeOptimiser.clearRegisters;

  procSym := TProcSymbol(FcurrentScope.lookup(node.procName));
{
  if (not procSym.isVolatile) and (procSym.refCount = 0) and (procSym.procType <> ttAssembler) then
  // if procedure not used then don't generate code!!
    Exit;
}
  if (procSym.procType = ttAssembler) and (procSym.procAddress <> '') then begin
  // don't emit any procedure decl stuff for absolute asm procs
    emitCode(Format('.label %s = %s',[node.procName,procSym.procAddress]));
    Exit;
  end;

  FcurrentScope := node.scope;

  Fwriter.writeLn();

  startLine := Fwriter.currentLine;

  emitCode(node.procName+': {');
  incIndent;

  FblockIRQType := procSym.irqType;

  FfoundEndIrq := False;

  node.block.acceptVisitor(self);

  if (procSym.irqType = ttInterrupt) then
    emitCode(':irq_end()')
  else
  if (procSym.irqType = ttKernalInterrupt) then
    emitCode(':kernal_irq_end()')
  else
    emitCode('rts');

  decIndent;
  Fwriter.writeLn(indent+'}');

  Fwriter.writeLn();

  endLine := Fwriter.currentLine;

  routines_addInfo(node.procName,startLine,endLine);

  FblockIRQType := ttNone;

  FcurrentScope := node.scope.enclosingScope;
end;

procedure TASTto6502.visitMacroDecl (node : TMacroDecl);
var
  macroSym  : TMacroSymbol;
  i         : Integer;
begin
  macroSym := TMacroSymbol(FcurrentScope.lookup(node.macroName));

  Fwriter.writeLn();

  Fwriter.write('.macro ' + node.macroName + '(');

  for i := 0 to macroSym.paramCount - 1 do begin
    Fwriter.write(macroSym.params[i]);
    if (i < macroSym.paramCount - 1) then Fwriter.write(',');
  end;

  Fwriter.writeLn(') {');

  if macroSym.macroType = ttMacro then incIndent;

  node.block.acceptVisitor(self);

  if macroSym.macroType = ttMacro then decIndent;

  Fwriter.writeLn('}');

  Fwriter.writeLn();
end;

procedure TASTto6502.copyResultToRegs(funcSym : TFuncSymbol);
var
  funcSize  : Integer;
  sym       : TSymbol;
begin
  funcSize := funcSym.size;
  if funcSym.symType <> nil then
    funcSize := funcSym.symType.size;

  sym := FcurrentScope.lookup('result');

  // load result into register(s)
  emitCode('// copy "result" into reg(s)');
  emitCode('ldy _sp');

  if funcSize = 1 then begin
    emitCode(Format('lda '+cSTACK_VAR+',y',[sym.stackOfs]));
  end
  else begin
    emitCode(Format('ldx '+cSTACK_VAR+',y'  ,[sym.stackOfs]));
    emitCode(Format('lda '+cSTACK_VAR+'+1,y',[sym.stackOfs]));
  end;
end;

procedure TASTto6502.visitFuncDecl (node : TFuncDecl);
var
  funcSym   : TFuncSymbol;
  startLine : Integer;
  endLine   : Integer;
begin
  FcodeOptimiser.clearRegisters;

  funcSym := TFuncSymbol(FcurrentScope.lookup(node.funcName));

{
  if (not funcSym.isVolatile) and (funcSym.refCount = 0) and (funcSym.funcType <> ttAssembler) then
  // if function not used then don't generate code!!
    Exit;
}
  if (funcSym.funcType = ttAssembler) and (funcSym.funcAddress <> '') then begin
  // don't emit any procedure decl stuff for absolute asm procs
    emitCode(Format('.label %s = %s',[node.funcName,funcSym.funcAddress]));
    Exit;
  end;

  FcurrentScope := node.scope;

  Fwriter.writeLn();

  startLine := Fwriter.currentLine;

  Fwriter.writeLn(indent+node.funcName+': {');
  incIndent;

  node.block.acceptVisitor(self);

  if funcSym.onStack and (funcSym.funcType <> ttAssembler) then
    copyResultToRegs(funcSym);

  emitCode('rts');
  decIndent;
  Fwriter.writeLn(indent+'}');

  Fwriter.writeLn();

  endLine := Fwriter.currentLine;

  routines_addInfo(node.funcName,startLine,endLine);

  FcurrentScope := node.scope.enclosingScope;
end;

procedure TASTto6502.write16BitNumber;
begin
  if Fprog.target = ttVic20 then
    emitCode('jsr $DDCD // PRINTWORD')
  else
  if Fprog.target = ttC64 then
    emitCode('jsr $bdcd // PRINTWORD')
  else
  if Fprog.target = ttC128 then
    emitCode('jsr $8E32 // PRINTWORD')
  else
    emitCode('TARGET NOT SUPPORTED BY WRITE/WRITELN');
end;

procedure TASTto6502.writeInteger(aWriter : TsesStringWriter; aInt : Integer; aSize : Integer);
var
  isNegative : Boolean;
begin
  isNegative := aInt < 0;

  if aSize = 1 then begin
    if isNegative then begin
      emitCode('lda #'+IntToStr(aInt));
      emitCode('jsr _printSignedA');
    end
    else begin
      emitCode('ldx #<'+IntToStr(aInt) + ' + 0');
      emitCode('lda #0');
      write16BitNumber;
//      emitCode('jsr $bdcd // PRINTWORD');
    end;
  end
  else begin
    if isNegative then begin
      emitCode('ldx #'+IntToStr(aInt) + ' + 0');
      emitCode('lda #'+IntToStr(aInt) + ' + 1');
      emitCode('jsr _printSignedAX');
    end
    else begin
      emitCode('ldx #<'+IntToStr(aInt) + ' + 0');
      emitCode('lda #>'+IntToStr(aInt) + ' + 1');
      write16BitNumber;
//      emitCode('jsr $bdcd // PRINTWORD');
    end;
  end;
end;

procedure TASTto6502.writeVariable(aWriter : TsesStringWriter; v : TAST);
var
  stackOfs : Integer;
  value0   : AnsiString;
  value1   : AnsiString;
begin
  stackOfs := 0;

  if isStackVar(v,FcurrentScope,stackOfs) then begin
    value0 := Format(cSTACK_VAR+',y'  ,[stackOfs]);
    value1 := Format(cSTACK_VAR+'+1,y',[stackOfs]);
  end
  else begin
    value0 := TVariable(v).token.tValue + ' + 0';
    value1 := TVariable(v).token.tValue + ' + 1';
  end;

  if stackOfs > 0 then
    emitCode('ldy _sp');

  if (v.size = 1) then begin
    if v.isSigned then begin
      emitCode('lda '+value0);
      emitCode('jsr _printSignedA');
    end
    else begin
      emitCode('ldx '+value0);
      emitCode('lda #0');
      write16BitNumber;
    end;
  end
  else begin
    if v.isSigned then begin
      emitCode('ldx '+value0);
      emitCode('lda '+value1);
      emitCode('jsr _printSignedAX');
    end
    else begin
      emitCode('ldx '+value0);
      emitCode('lda '+value1);
      write16BitNumber;
    end;
  end;
end;

procedure TASTto6502.writeString(aWriter : TsesStringWriter; aString : AnsiString);
begin
  if (Length(aString) = 0) then Exit;

  emitCode('{');
  if Fprog.target = ttvic20 then begin
    emitCode('lda #<aString');
    emitCode('ldy #>aString');
    emitCode('jsr $CB1E');
  end
  else
  if Fprog.target = ttC64 then begin
    emitCode('lda #<aString');
    emitCode('ldy #>aString');
    emitCode('jsr $AB1E');
  end
  else
  if Fprog.target = ttC128 then begin
    emitCode('ldx #0');
    emitLabel('!');
    emitCode('lda aString,x');
    emitCode('jsr $ffd2');
    emitCode('inx');
    emitCode('cpx #'+IntToStr(Length(aString)));
    emitCode('bne !-');
  end;

  emitCode('jmp !+');
  emitCode('');
  decIndent;
  emitCode('.encoding "petscii_mixed"');
  emitCode('aString: .text "'+aString+'"');
  emitCode('         .byte 0');
  emitCode('.encoding "screencode_mixed"');
  incIndent;
  emitCode('');
  decIndent;
  emitCode('!:');
  incIndent;
  emitCode('}');
end;

procedure TASTto6502.doWrite(node : TBuiltinProcedure; carridgeReturn : Boolean);
var
  i       : Integer;
  param   : TAST;
  tacList : TTACList;
  tacVar  : TAST;
  sym     : TSymbol;
  lLabel  : AnsiString;
begin
  for i := 0 to node.params.Count - 1 do begin
    param := TAST(node.params.Items[i]);

    if (param.isLeaf) then begin
      // is a single variable/number/string to output

      if (param.isInteger) then begin
        writeInteger(Fwriter,TNumber(param).value,TNumber(param).size);
      end
      else
      if (param.isString) then begin
        writeString(Fwriter,TString(param).value);
      end
      else begin
        // is variable
        sym := FcurrentScope.lookup(TVariable(param).token.tValue);

        if (sym.symType.symName = 'string') or (sym.symType.symName = 'petscii') or (sym.symType.symName = 'ascii') then begin
          lLabel   := newLabel;

          emitCode('lda '+TVariable(param).token.tValue);
          emitCode('beq !+ // skip write if 0 length string');
          emitCode('ldx #0');
          emitLabel(lLabel);
          emitCode('txa // save x');
          emitCode('pha');
          emitCode('lda '+TVariable(param).token.tValue+' + 1,x');
          emitCode('jsr $ffd2');
          emitCode('pla // restore x');
          emitCode('tax');
          emitCode('inx');
          emitCode('cpx '+TVariable(param).token.tValue);
          emitCode('bcc '+lLabel);
          emitCode('!:');
        end
        else
          writeVariable(Fwriter,param);
      end;
    end else begin
      // is an equation so create TAC and output the result
      tacList := generateTAC(param,TVariable(nil));
      tacList.optimise;
      emitTACcode(tacList);

      tacVar := tacList.destVar;
      writeVariable(Fwriter,tacVar);
    end;
  end;
  if carridgeReturn then begin
    emitCode('lda #13');
    emitCode('jsr $ffd2 // CHROUT');
  end;
end;

procedure TASTto6502.visitBuiltinProcedure(node : TBuiltinProcedure);
begin
  if (node.procType in [ttWriteLn,ttWrite]) then
    doWrite(node,node.procType = ttWriteLn)
  else begin
{    case node.procType of
      ttDisableIrq   : emitCode('sei');
      ttEnableIrq    : emitCode('cli');
      ttVic38Columns : begin
        emitCode('// enable 38-column mode');
        emitCode('lda $d016');
        emitCode('and #%11110111');
        emitCode('sta $d016');
        emitCode('//');
      end;
      ttVic40Columns : begin
        emitCode('// enable 40-column mode');
        emitCode('lda $d016');
        emitCode('ora #%00001000');
        emitCode('sta $d016');
        emitCode('//');
      end;
    end;}
  end;
end;

procedure TASTto6502.visitBuiltinFunction(node : TBuiltinFunction);
begin
  if FflattenExpression then begin
    pushExpNode(node);
  end;
end;

procedure TASTto6502.visitImportBinary(node : TImportBinary);
var
  constSym : TConstSymbol;
begin
(*
 *=$a000 "Titlescreen Music"
 titleMusic: {
   .import binary "audio\ALBINO_4.sid", $7e // get rid of sid header (skip $7e bytes)
 }
*)

  if (node.address <> '') then
    Fwriter.writeLn(Format('* = %s "%s"',[node.address,node.name]));

  Fwriter.writeLn(node.name+':{');
  Fwriter.writeLn(Format('  .import binary "%s",%s',[node.fileName,node.byteSkip]));
//  Fwriter.writeLn('  .label size = %d',[node.fileSize]);

  if node.isSid then begin
    // is sid so add init/play addresses
    Fwriter.writeLn('// labels used if binary is a sid file');
    Fwriter.writeLn('  .label init = '+node.name);
    Fwriter.writeLn('  .label play = '+node.name+' + 3');
  end;
  Fwriter.writeLn('}');
end;

procedure TASTto6502.visitImportSource(node : TImportSource);
begin
  Fwriter.writeLn('#import "'+node.fileName+'"');
end;

procedure TASTto6502.visitAbsoluteArrayData(node : TAbsoluteArrayData);
var
  i : Integer;
begin
(*
 *=$a000 "array name"
 array name: {
  // data here
 }
*)

//  Fwriter.writeLn('.segment %s [start=%s]',[node.name,node.address]);

  if (node.address <> '') then
    Fwriter.writeLn(Format('* = %s "%s"',[node.address,node.name]));

  Fwriter.writeLn(node.name+':{');
  // output array data here
  Fwriter.write('.byte ');
  for i := 0 to high(node.data) do begin
    if (node.dataSize = 1) then
      Fwriter.write('<%s',[node.data[i]])
    else
      Fwriter.write('<%s, >%s',[node.data[i],node.data[i]]);

    if i < high(node.data) then Fwriter.write(',');
  end;
  Fwriter.writeLn();
  Fwriter.writeLn('}');
end;

procedure TASTto6502.visitExit(node: TExit);
var
  scope : TSymbol;
begin
  scope := FcurrentScope.lookup(FcurrentScope.scopeName);

  if scope is TFuncSymbol then
    if TFuncSymbol(scope).onStack and (TFuncSymbol(scope).funcType <> ttAssembler) then
      copyResultToRegs(TFuncSymbol(scope));

  emitCode('rts');
end;

procedure TASTto6502.visitBreak(node: TBreak);
begin
  emitCode('jmp '+peekLoopExitLabel);
end;

procedure TASTto6502.visitSetupRasterIRQ(node: TSetupRasterIRQ);
begin
  emitCode(Format(':SetupRasterIRQ(%s,%s)',[node.routineName+'.'+MAIN_LABEL,node.rasterLine]));
end;

procedure TASTto6502.visitEndIRQ(node: TEndIRQ);
begin
  FfoundEndIrq := True;
//  emitCode(':restoreStack()');
  if node.useSameRaster then
    emitCode(Format(':irq_end2(%s)',[node.routineName+'.'+MAIN_LABEL]))
  else
    emitCode(Format(':irq_end(%s,%s)',[node.routineName+'.'+MAIN_LABEL,node.rasterLine]));
//  emitCode(Format(':EndIRQ(%s,%s)',[node.routineName+'.'+MAIN_LABEL,node.rasterLine]));
end;

//----------------------------------------
procedure TASTto6502.genCode(aNode : TAST; aAsmStream : TStream; aRoutinesList : TStringList; var aWarnings : AnsiString);
begin
  FroutinesList := aRoutinesList;

  FasmStream := aAsmStream;
  Fwriter    := TsesStringWriter.create(FasmStream);
  Fwriter.onWriteString := self.onAsmWrite;

  FcodeOptimiser := TPeepholeOptimiser6502.create(Fwriter);
  try
    FcodeOptimiser.addAlwaysLoad(cSTACK_PNTR);
    FcodeGen   := TCodeGen_6502.Create(Fwriter,FtempPointer);

    Fwarnings.Clear;

    if (aNode <> nil) then aNode.acceptVisitor(self);

    aWarnings := Fwarnings.Text;
  finally
    FcodeOptimiser.Free;
    Fwriter.Free;
  end;
end;

end.


