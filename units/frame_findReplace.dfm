object frm_findReplace: Tfrm_findReplace
  Left = 0
  Top = 0
  Width = 454
  Height = 73
  TabOrder = 0
  object find_Edit: TEdit
    Left = 44
    Top = 0
    Width = 197
    Height = 28
    Hint = 'Find'
    TabOrder = 1
  end
  object frMode_Button: TButton
    Left = 0
    Top = 0
    Width = 38
    Height = 28
    Caption = '>'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
  end
  object replace_Edit: TEdit
    Left = 44
    Top = 34
    Width = 197
    Height = 28
    Hint = 'Replace'
    TabOrder = 3
  end
  object ToolBar1: TToolBar
    Left = 263
    Top = -1
    Width = 73
    Height = 29
    Align = alNone
    ButtonHeight = 27
    ButtonWidth = 27
    Caption = 'ToolBar1'
    TabOrder = 2
    object matchCase_ToolButton: TToolButton
      Left = 0
      Top = 0
      Caption = 'A'
      ImageIndex = 0
    end
    object wholeWord_ToolButton: TToolButton
      Left = 27
      Top = 0
      Caption = '|ab|'
      ImageIndex = 1
    end
  end
end
