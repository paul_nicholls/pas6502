object ideThemes_Dialog: TideThemes_Dialog
  Left = 227
  Top = 108
  BorderStyle = bsDialog
  Caption = 'IDE Themes'
  ClientHeight = 108
  ClientWidth = 184
  Color = clBtnFace
  ParentFont = True
  Position = poScreenCenter
  TextHeight = 15
  object OKBtn: TButton
    Left = 12
    Top = 70
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object CancelBtn: TButton
    Left = 106
    Top = 70
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 184
    Height = 58
    Align = alTop
    TabOrder = 2
    ExplicitWidth = 174
    object Label3: TLabel
      Left = 12
      Top = 4
      Width = 41
      Height = 15
      Caption = 'Themes'
    end
    object theme_ComboBox: TComboBox
      Left = 12
      Top = 25
      Width = 145
      Height = 23
      Style = csDropDownList
      TabOrder = 0
    end
  end
end
