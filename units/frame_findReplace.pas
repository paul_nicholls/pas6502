unit frame_findReplace;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes,
  Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  Vcl.ButtonGroup, Vcl.ComCtrls, Vcl.ToolWin;

type
  Tfrm_findReplace = class(TFrame)
    find_Edit: TEdit;
    frMode_Button: TButton;
    replace_Edit: TEdit;
    ToolBar1: TToolBar;
    matchCase_ToolButton: TToolButton;
    wholeWord_ToolButton: TToolButton;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

end.
