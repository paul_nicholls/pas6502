unit uToken;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

{$H+}

interface

type
  TTokenType = (
    ttNone,
    ttIdent,
    ttIntNum,
    ttString,
    ttAscii,
    ttPetscii,

    ttPlus,
    ttMinus,
    ttTimes,
    ttSlash,

    ttBecomes,

    ttLParen,
    ttRParen,

    ttLBracket,
    ttRBracket,

    ttSemiColon,
    ttColon,
    ttPeriod,
    ttComma,
    ttExclamation,
    ttAt,

    ttHash,
    ttAmpersand,
    ttPipe,
    ttRange,
    ttQuote,
    ttQuestionMark,
    ttCaret,

    ttNot,
    ttNeg,

    ttEql,ttNeq,ttLss,ttLeq,ttGtr,ttGeq,

    ttUnit,
    ttInterface,
    ttImplementation,
    ttProgram,
    ttUses,
    ttLoadAddress,

    ttImportSegments,
    ttImportHeader,
    ttSetIRQsegment,
    ttSetPRGROMsegment,

    ttTarget,
    ttcdecl,

    // compatable systems
    ttC64,
    ttC128,
    ttBBC,
    ttNES,
    ttAtari2600,
    ttAtari8Bit,
    ttVic20,
    ttAppleII,
    ttPET,
//    ttAtari400,
//    ttAtari800,
//    ttAtari1200XL,
//    ttAtari5200,
//    ttAtariXEGS,
//    ttAtariXL,
//    ttAtariXE,

    ttBegin,
    ttEnd,
    ttVar,
    ttConst,
    ttProc,
    ttForward,
    ttFunc,
    ttReal,
    ttInteger,
    ttShortInt,
    ttPointer,
    ttByte,
    ttWord,
    ttDWord,
    ttInt24,
    ttBoolean,
    ttType,
    ttRecord,
    ttTrue,
    ttFalse,
    ttDiv,
    ttMod,
    ttIf,
    ttThen,
    ttElse,
    ttRepeat,
    ttUntil,
    ttWhile,
    ttDo,
    ttFor,
    ttTo,
    ttDownTo,
    ttOf,
    ttArray,
    ttAbsolute,
    ttExternal,
    ttAssembler,
    ttAsm,
    ttInterrupt,
    ttKernalInterrupt,
    ttSetupIRQ,
    ttSetupRasterIRQ,
    ttEndIRQ,
    ttMacro,
    ttImport,
    ttImportBinary,
    ttSource,
    ttExit,
    ttBreak,
    ttVolatile,
    ttKickAsm,
    ttLKickAsm,
    ttRKickAsm,

    ttNESheader,

    ttdecimalmodeon,
    ttdecimalmodeoff,

    ttLo,
    ttHi,

    ttSin,
    ttCos,
    ttTan,

    ttAsc,
    ttChr,

    ttPoke,
    ttPeek,
    ttLength,
    ttWriteLn,
    ttWrite,
    ttDisableIrq,
    ttEnableIrq,
    ttVic38Columns,
    ttVic40Columns,

    ttAnd,
    ttOr,
    ttXor,
    ttShl,
    ttShr,

    ttInc,
    ttDec,

    ttDiv256,
    ttMul256,
    ttMul2,
    ttDiv2,
    ttLssZero,

    ttEOF
  );

  TToken = record
    tType  : TTokenType;
    tValue : AnsiString;
  end;

function  Token(aType : TTokenType; aValue : Variant) : TToken;
function  getTargetName(target : TTokenType) : AnsiString;

implementation

function  Token(aType : TTokenType; aValue : Variant) : TToken;
begin
  Result.tType  := aType;
  Result.tValue := aValue;
end;

function  getTargetName(target : TTokenType) : AnsiString;
begin
  case target of
    ttC64       : Result := 'c64';
    ttC128      : Result := 'c128';
    ttVic20     : Result := 'vic20';
    ttBBC       : Result := 'bbc';
    ttAtari8Bit : Result := 'atari8bit';
    ttNES       : Result := 'nes';
    ttAppleII   : Result := 'appleii';
  end;
end;

end.
