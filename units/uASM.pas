unit uASM;

{$ifdef fpc}
{$MODE Delphi}
{$endif}

//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

interface

uses
  uSymbolTable;

const
  cSTACK_NAME         = '_STACK_';
  cSTACK_VAR          = cSTACK_NAME + '+%d';
  cSTACK_PNTR         = '_sp';
  cSTACK_PNTR_DEFAULT = '$FF';

{
byte a : Byte, x : Byte, y : Byte:
  a single byte passed via the given CPU register;
  any 1-byte type can be used.

xa : Word, ax : Word, ay : Word, ya : Word, xy : Word, yx : Word:
  a 2-byte word byte passed via given two CPU registers,
  with the high byte passed through the first register and
  the low byte passed through the second register; any 2-byte type can be used.
}
function  isAsmParam(aName : AnsiString; aType : TSymbol; var loReg,hiReg : AnsiString) : Boolean;

function  getFuncResultReg : AnsiString;
procedure resetFuncResultReg;
procedure enterFunction;
procedure exitFunction;
function  getFunctionLevel : Byte;

implementation

uses
  System.SysUtils;

var
  funcResultRegIndex : Byte;
  functionLevel      : Byte;



function  getFuncResultReg : AnsiString;
begin
  Result := '_fr_';//  Format('_FR_%d',[funcResultRegIndex]);
end;

function  getFunctionLevel : Byte;
begin
  Result := functionLevel;
end;

procedure resetFuncResultReg;
begin
  funcResultRegIndex := 0;
  functionLevel      := 0;
end;

procedure enterFunction;
begin
  inc(funcResultRegIndex);
  inc(functionLevel);
end;

procedure exitFunction;
begin
  dec(funcResultRegIndex);
  dec(functionLevel);
end;

function isAsmByteParam(aName : AnsiString; aType : TSymbol; var loReg,hiReg : AnsiString) : Boolean;
const
  cRegName_Byte : array[1..3] of AnsiString = (
    'a',
    'x',
    'y'
  );

var
  i : Integer;
begin
  Result := False;
  loReg  := '';
  hiReg  := '';

  if (aType.size <> 1) then Exit;

  for i := 1 to high(cRegName_Byte) do
    if (cRegName_Byte[i] = aName) then begin
      Result := True;
      loReg  := cRegName_Byte[i];
      Exit;
    end;
end;

function isAsmWordParam(aName : AnsiString; aType : TSymbol; var loReg,hiReg : AnsiString) : Boolean;
const
  cRegName_Word : array[1..6] of AnsiString = (
    'xa',
    'ax',
    'ay',
    'ya',
    'xy',
    'yx'
  );
var
  i : Integer;
begin
  Result := False;
  loReg  := '';
  hiReg  := '';

  if (aType.size <> 2) then Exit;

  for i := 1 to high(cRegName_Word) do
    if (cRegName_Word[i] = aName) then begin
      Result := True;
      loReg  := cRegName_Word[i][2];
      hiReg  := cRegName_Word[i][1];
      Exit;
    end;
end;

function  isAsmParam(aName : AnsiString; aType : TSymbol; var loReg,hiReg : AnsiString) : Boolean;
begin
  Result := isAsmByteParam(aName,aType,loReg,hiReg);

  if not Result then
    Result := isAsmWordParam(aName,aType,loReg,hiReg);
end;

end.
