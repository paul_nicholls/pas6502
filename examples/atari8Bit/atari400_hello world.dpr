program Atari8BitHelloWorld;

target = atari8Bit;
*      = $a000;

const 
  SCREEN_ADDRESS = $2114;
  DL_ADDRESS     = $0230; // SET DISPLAY LIST ADRESS
  EOL            = $9B;
  
type 
  TIOCB = record
    ICHID : Byte;  //DEVICE HANDLER
    ICDNO : Byte;  //DEVICE NUMBER
    ICCOM : Byte;  //I/O COMMAND
    ICSTA : Byte;  //I/O STATUS
    ICBAL : Byte;  //LSB BUFFER ADDR
    ICBAH : Byte;  //MSB BUFFER ADDR
    ICPTL : Byte;  //LSB PUT ROUTINE
    ICPTH : Byte;  //MSB PUT ROUTINE
    ICBLL : Byte;  //LSB BUFFER LEN
    ICBLH : Byte;  //MSB BUFFER LEN
    ICAX1 : Byte;  //AUX BYTE 1
    ICAX2 : Byte;  //AUX BYTE 1
  end;

const  
  dlist: array of Byte = (
   112, // Blank 8 lines
   112, // Blank 8 lines
   112, // Blank 8 lines
   64+2,
   $14,$21,
   2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
   65, // Jump and wait for vertical blank
   $20,$94
  );
  
  hello : array of Byte = (
   $68, $65, $6c, $6c, $6f, $00, $77, $6f, $72, $6c, $64, $01
  );
 

var
  screen : Byte absolute SCREEN_ADDRESS;
  sdlstl : Word absolute DL_ADDRESS;
  i      : Byte;
begin
  // set display list to point to $2000 ram

  sdlstl := $2000;
  
  // store display list in ram at $2000
  for i := 0 to Length(dlist) - 1 do
    poke($2000+i,dlist[i]);
  
  for i := 0 to Length(hello) - 1 do
    screen[i] := hello[i];
  
  while true do;
end.