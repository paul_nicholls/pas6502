program Atari8BitHelloWorld2;

// HELLO WORLD EXAMPLE WITH ANTIC MODE 7 AND 2 (BASIC : GRAPHICS 2 AND 0)

target = atari8Bit;
*      = $a000;

const
  DLIST : array of Byte = (
    112,112,112,64+7,$00,$21,
    64+2,$14,$21,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
    65,$20,$94
  );  
  
  hello : array of Byte = (
   $68, $65, $6c, $6c, $6f, $00, $77, $6f, $72, $6c, $64, $01
  );

var
  i      : Byte;
  
  SDLSTL  : Word absolute $0230; //SET DISPLAY LIST ADRESS
  SDMCTL  : Byte absolute $022F; //DMA ENABLE/CONTROL
  COLOR0  : Byte absolute $02C4;
  COLOR1  : Byte absolute $02C5;
  COLOR2  : Byte absolute $02C6;
  COLOR3  : Byte absolute $02C7;
  COLOR4  : Byte absolute $02C8; //BACKGROUND
  PMBASE  : Byte absolute $D407; //STARTING ADRESS PLAYER MISSILE GRAPHICS
  OURBAS  : Byte absolute $600;
  DMACTL  : Byte absolute $D400; 
  GRACTL  : Byte absolute $D01D;
  SIZEP0  : Byte absolute $D008; //SIZE OF P0/M0 0: Byte absoluteSINGLE 1: Byte absoluteDOUBLE 3: Byte absoluteQUADRUPLE
  HPOSP0  : Byte absolute $D000; //HORIZONTAL POSITION P0/M0
  PCOLR0  : Byte absolute $02C0; //COLOR P0/M0
  VDSLSTL : Byte absolute $0200; //LOW BYTE DISPLAY LIST INTERRUPT ROUTINE
  VDSLSTH : Byte absolute $0201; //HIGH BYTE DISPLAY LIST INTERRUPT ROUTINE
  NMIEN   : Byte absolute $D40E; //ENABLE NONMASKABLE INTERRUPT D7 SET: Byte absoluteDLI D6 SET: Byte absoluteVBI D7+D6: Byte absoluteC0
  CHBAS   : Byte absolute $02F4; //CHARACTER ADRESS BASE
begin
//***DMA CONTROL***
  SDMCTL := 62;

//***SET THE CHARACTERSET BASE ADRESS***
  CHBAS := $E0;

//***STARTING ADRESS OF THE DISPLAY LIST IN RAM***
  SDLSTL := $2000;

//***LOAD THE DISPLAY LIST IN RAM***
 
 for i := 0 to Length(DLIST) - 1 do
   Poke($2000+i,DLIST[i]);

//***SET HELLO WORLD CHARACTERS IN DISPLAY LIST AREA RAM***

  for i := 0 to Length(hello) - 1 do begin
    Poke($2100+i,hello[i]);
    Poke($2114+i,hello[i]);
  end;

  while true do Inc(COLOR0);
end.