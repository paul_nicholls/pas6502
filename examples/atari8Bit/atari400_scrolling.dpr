program Atari8BitScrolling;

// HELLO WORLD EXAMPLE WITH ANTIC MODE 7 AND 2 (BASIC : GRAPHICS 2 AND 0)

target = atari8Bit;
*      = $a000;

var
  i,j   : Byte;
  delay : Word;
  
begin
  // setup display list
  poke(1536,112); //8 blank lines
  poke(1537,112); //8 blank lines
  poke(1538,112); //8 blank lines
  
  // Loop to put in display list
  for i := 1 to 12 do begin
    poke(1536+3*i,71);  // BASIC mode 2 with LMS set
    poke(1536+3*i+1,0); // Low byte of LMS operand
    poke(1536+3*i+2,i); // High byte of LMS operand
  end;
  
  Poke(1575,65);// ANTIC JVB instruction
  Poke(1576,0); // Display List starts at $0600
  Poke(1577,6); 
  
  // tell ANTIC where display list is
  Poke(560,0);
  Poke(561,6);
  
  while true do begin
    //now scroll horizontally
    //Loop through LMS low bytes
    //we use 235---not 255---because screen width is 20 characters
    for i := 0 to 235 do begin
      for j := 1 to 12 do begin // for each mode line
        Poke(1536+3*j+1,i); // Put in new LMS low byte
        // delay
        for delay := 0 to 300 do;
      end;
    end;
  end;
end.