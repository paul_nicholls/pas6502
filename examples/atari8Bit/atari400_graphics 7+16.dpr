program Atari8_graphicsTest7plus16;

target = atari8Bit;
*      = $a000;

uses
  atari8Bit_graphics;

const  
  // screen size in pixels (direct access)
  XRES            = 160;
  YRES            = 96;
  BYTESPERLINE    = 40;
  
  // screen character address information
  // to speed up access
  
  // screen y address
  screenRow  : array of Word = for y := 0 to YRES - 1 do (BYTESPERLINE * y);
  // screen x offset to add to the y address
  screenXOfs : array of Word = for x := 0 to XRES*2 - 1  do (x/8);

  MIN_SIN   = 0;
  MAX_SIN   = YRES div 2;
  
  SIN_AMPL  = MAX_SIN - MIN_SIN - 1;
  SIN_WIDTH = XRES;
  
  // sine table to match width and height of hi-res screen
  // sin(x, n) � returns n*sin(x*PI/128)
  sinTable : array of Byte = for i := 0 to SIN_WIDTH - 1 do 
             (sin((256*i)/SIN_WIDTH,SIN_AMPL));
             
procedure plotPixel_multicolor(screenRam,x : Word; y : Byte; c : Byte);
const
  cMask : array[0..3] of Byte = (
    %00111111,
    %11001111,
    %11110011,
    %11111100
  );

  cColorSet : array[0..15] of Byte = (
  // shift pos
    %00000000,%00000000,%00000000,%00000000, // color
    %01000000,%00010000,%00000100,%00000001,
    %10000000,%00100000,%00001000,%00000010,
    %11000000,%00110000,%00001100,%00000011
  );

  cColorMask : array[0..3] of Byte = (
    %11000000,%00110000,%00001100,%00000011
  );

var
  cx,cy : Byte;
  ox,oy : Byte;
  addr  : Pointer;
  data  : Byte;
begin
  if x >= XRES then Exit;
  if y >= YRES then Exit;
  
  x := x * 2;
  ox := (x and 7) / 2; // offs x; 0..3
  cx := x div 8;
  
  addr := screenRam;  
  addr := addr + screenRow[y];
  addr := addr + cx;
  
  data := addr^;
  
  data := data and cMask[ox];
  
  if c = 0 then Exit;
  
  c := (c * 4) + ox;
  c := cColorSet[c];
  
  addr^ := data or c
end;

var
  y,i : Byte;
  x   : Word;
begin
  graphics7_16;
  
  x := 0;
  while x <> XRES do begin
    // get y location of the pixel to plot on screen
    y := (YRES div 2) - sinTable[x];

    plotPixel_multicolor(screenInfo.screenRam,x+0,y-1,1);
    plotPixel_multicolor(screenInfo.screenRam,x+2,y,2);
    plotPixel_multicolor(screenInfo.screenRam,x+4,y+1,3);
    
    x := x + 1;
  end;
  
  while true do;
end.