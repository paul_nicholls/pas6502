program Atari8_PMverticalExample;
//P/M EXAMPLE - MOVING A SHIP VERTICALLY UPWARDS FROM BASIC

target = atari8Bit;
*      = $a000;

uses
  atari8Bit_graphics;
  
const
  // PLAYER #0 DATA - SHIP
  playerSprite : array of Byte = (
    0,0,153,153,189,189,255,255,189,189,153,153,0,0
  );

procedure doDelay;
var
  w : Word;
begin
  // DELAY LOOP
  for w := 1 to 2000 do;
end;

var
  i,j    : Byte;
  PMBASE : Word;
  SRC    : pointer;
  SPOT   : Word;
  w      : Word;
  PM     : Byte;
begin
  // P/M EXAMPLE - MOVING A SHIP VERTICALLY UPWARDS
  poke(106,peek(106)-8);
  PM     := peek(106);
  PMBASE := PM*256;

  graphics1_16;
  
  poke(559,62);    //SET DMACTL - SINGLE LINE & STANDARD PLAYFIELD
  poke(53277,3);   //SET GRACTL - PLAYERS & MISSILES
  poke(54279,PM);  //TELL ANTIC PMBASE
  poke(53256,1);   //PLAYER #0 DOUBLE WIDTH
  poke(53248,100); //PLAYER #0 HORIZONTAL POSITION
  poke(704,88);    //PLAYER #0 COLOR PINK
  
  // CLEAR OUT P/M AREA for PLAYER #0 ONLY
  for w := PMBASE+1024 to PMBASE+1280 do begin
    poke(w,0);
  end;
  
  // READ PLAYER #0 DATA INTO PLAYER AREA BUT STARTING 100 BYTES INTO
  SPOT := PMBASE+1024+100;
  for i := 0 to 13 do begin
    poke(SPOT+i,playerSprite[i]);
  end;
  
  doDelay;

  // MOVE PLAYER DATA UP TWO SCAN LINES AT A TIME - TEN TIMES
  for j := 1 to 10 do begin
    for i := 0 to 13 do begin
      SRC := SPOT+i;
      poke((SPOT-2)+i,SRC^);
    end;
    SPOT := SPOT-2; //CURRENT PLAYER MEMORY LOCATION
    doDelay;
  end;
  
  for j := 1 to 20 do begin
    for i := 13 downto 0 do begin
      SRC := SPOT+i;
      poke(SPOT+2+i,SRC^);
    end;
    
    // MOVE PLAYER DATA DOWN TWO SCAN LINES AT A TIME - TWENTY TIMES
    SPOT := SPOT+2; //CURRENT PLAYER MEMORY LOCATION
    doDelay;
  end;
  
  // MOVE PLAYER HORIZONTALLY RIGHT
  for i := 100 to 180 do begin
    poke(53248,i);
    doDelay;
  end;
  
  while true do;
end.