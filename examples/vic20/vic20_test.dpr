program vic20_test;

target = vic20;

uses
  vic20_vic;
  
begin
  // increment border infinite loop
  disable_irq;
  while true do
    // border color is the first 3 bits
    vic.bg_border := (vic.bg_border and %11111000) or ((vic.bg_border + 1) and 3);
end.