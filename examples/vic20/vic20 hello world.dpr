program vic20_test;

target = vic20;

const
  name : Petscii = 'paul';
  
begin
  writeLn('hello ',name,',');
  writeLn('welcome to pas6502!');
end.