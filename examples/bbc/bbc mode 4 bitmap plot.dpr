program bbc_test;

target = bbc;

* = $3000;

uses 
  bbc,bbc_vdu;
  
const
  // mode 4 screen address
  SCREEN0_ADDRESS = $5800; 
  // mode 4 screen size in characters
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 32;
  
  // mode 4 screen size in pixels (direct access)
  XRES            = SCREEN_WIDTH  * 8;
  YRES            = SCREEN_HEIGHT * 8;
  
  // mode 4 screen character address information
  // to speed up access
  
  // screen y address
  screenRow  : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (SCREEN0_ADDRESS+XRES*y);//((SCREEN_WIDTH * y)*8);
  // screen x offset to add to the y address
  screenXOfs : array of Word = for x := 0 to SCREEN_WIDTH - 1  do (x*8);

  MIN_SIN   = 0;
  MAX_SIN   = YRES div 2;
  
  SIN_AMPL  = MAX_SIN - MIN_SIN - 1;
  SIN_WIDTH = XRES;
  
  // sine table to match width and height of hi-res screen
  sinTable : array of Byte = for i := 0 to SIN_WIDTH - 1 do 
             (sin((256*i)/SIN_WIDTH,SIN_AMPL));
             
  // bit of pixel to light up (single pixel only)
  bits : array of Byte = (128,64,32,16,8,4,2,1);
  
var
  x     : Word;
  y     : Byte;
  i     : Byte;
  cx,cy : Byte;
  ox,oy : Byte;
  addr  : pointer;
begin
  vdu_mode(4);

  // actually redefines cursor shape as blank, must find better way! :D
  vdu_cursorOff();
  
  x := 0;
  while x <> XRES do begin
    // get y location of the pixel to plot on screen
    y := (YRES div 2) - sinTable[x];

    cx := x shr 3;
    cy := y shr 3;
    ox := x and 7;
    oy := y and 7;
    
    addr := screenRow[cy];
    addr := addr + screenXOfs[cx];

    addr^[oy] := addr^[oy] or bits[ox];

    x := x + 1;
  end;
    
  while true do;
end.