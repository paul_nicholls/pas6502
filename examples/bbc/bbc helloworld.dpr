program bbc_helloworld;

target = bbc;

* = $3000;

uses 
  bbc;
  
const
  cHELLO : ascii = 'hello world';
  
procedure OSASCII(a : Byte) external $FFE3;
procedure OSNEWL()          external $FFE7;

var
  i : Byte; 
begin
  saveRegisters;

  for i := 1 to length(cHELLO) do OSASCII(cHELLO[i]);
  OSNEWL();
  
  restoreRegisters;
end.