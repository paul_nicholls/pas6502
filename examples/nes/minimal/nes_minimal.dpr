program nes_minimal;

target = nes;

ImportSegments('segments_mapper_v.asm');
ImportHeader('header_mapper_v_ntsc.asm');

SetIRQsegment('BANK2');
SetPRGROMsegment('BANK1');

uses
  nes_hardware; 

#SEGMENT('CHARS');

const
  graphics = importBinary('tiles.chr');

#SEGMENT('RAM');

var
  i : Word;
  p : Byte;

#SEGMENT('BANK1');

const
  NameTableData = importBinary('gfx.nam');
  PaletteData   = importBinary('gfx.pal');
  
procedure nmi; assembler; volatile;
asm
  pha
  txa
  pha
  tya
  pha

  // do stuff

  pla
  tay
  pla
  tax
  pla
  rti
end;

procedure irq; assembler; volatile;
asm
  rti
end;

procedure init; assembler;
  asm
  sei                // ignore IRQs
  cld                // disable decimal mode
  ldx #$40
  stx IO.input_1    // disable APU frame IRQ

  //  set stack
  ldx #$ff
  txs

  ldx #$00
  stx PPU.control    // disable NMI
  stx PPU.mask      // disable rendering
  stx IO.dmc_freq   // disable DMC IRQs

  bit PPU.status

  :VWAIT()
    // We now have about 30,000 cycles to burn before the PPU stabilizes.
    // One thing we can do with this time is put RAM in a known state.
  ldx #$00
@clrmem:
  lda #$00
  sta $000,x
  sta $100,x
  sta $300,x
  sta $400,x
  sta $500,x
  sta $600,x
  // move sprites off secreen
  lda #$ff
  sta $200,x
  txa
  sta $700,x
  inx
  bne @clrmem

  lda #$00
  sta PPU.control
  sta PPU.mask
  lda #$7
  sta OAM.address

  //  copy NAMETABLE
  lda #<NameTableData
  sta TEMP_PNTR
  lda #>NameTableData
  sta TEMP_PNTR+1
  PPU_SETADDR(NAMETABLE_0)
  //  4 pages
  ldx #$04
  ldy #$00
lo:
  lda (TEMP_PNTR),y
  sta PPU.data
  iny
  bne lo
  inc TEMP_PNTR+1
  dex
  bne lo

  PPU_SETADDR($3f00)
  ldx #$00
pal_copy:
  lda PaletteData,x
  sta PPU.data
  inx
  cpx #$10
  bne pal_copy
  //  copy same palette to sprites ( and BG color )
  ldx #$00
pal_copy2:
  lda PaletteData,x
  sta PPU.data
  inx
  cpx #$10
  bne pal_copy2

  VWAIT()

  lda #CTRL_NMI_ON
  sta PPU.control
  lda #MASK_BG_ON | MASK_SP_ON
  sta PPU.mask
  
  cli
  
  jmp *
end;

begin
  init();
end.