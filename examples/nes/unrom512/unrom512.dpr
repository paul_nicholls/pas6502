program nes_unrom512;

target = nes;

ImportSegments('segments_unrom512.asm');
ImportHeader('header_unrom512.asm');

SetPRGROMsegment('PRG_FIXED');
SetIRQsegment('VECTORS');

uses
  nes_hardware; 

#SEGMENT('BANKTABLE');

const
  MAPPER_REGISTER : array of Byte = for i := 0 to 255 do (i);

#SEGMENT('RAM');

var
  // PPU writer helper method variables
  ppu_data_pntr : Pointer; // pointer to data to copy to PPU
  ppu_data_size : Byte;    // # of bytes to copy to PPU
  // -----------------------------------------------------
  i : Byte;
  
#SEGMENT('CODE_MAIN');

const
  helloSpriteTiles : array of Byte = (
   %11000011,	// H (00)
   %11000011,
   %11000011,
   %11111111,
   %11111111,
   %11000011,
   %11000011,
   %11000011,
   $00, $00, $00, $00, $00, $00, $00, $00,

   %11111111,	// E (01)
   %11111111,
   %11000000,
   %11111100,
   %11111100,
   %11000000,
   %11111111,
   %11111111,
   $00, $00, $00, $00, $00, $00, $00, $00,

   %11000000,	// L (02)
   %11000000,
   %11000000,
   %11000000,
   %11000000,
   %11000000,
   %11111111,
   %11111111,
   $00, $00, $00, $00, $00, $00, $00, $00,

   %01111110,	// O (03)
   %11100111,
   %11000011,
   %11000011,
   %11000011,
   %11000011,
   %11100111,
   %01111110,
   $00, $00, $00, $00, $00, $00, $00, $00  
  );

  sprites : array of Byte = (
// y    tile attr x
   $6c, $00, $00, $6c, // H tile
   $6c, $01, $00, $76, // E tile
   $6c, $02, $00, $80, // L tile
   $6c, $02, $00, $8A, // L tile
   $6c, $03, $00, $94  // O tile
  );

palettes : array of Byte = (
  // Background Palette
   $0f, $00, $00, $00,
   $0f, $00, $00, $00,
   $0f, $00, $00, $00,
   $0f, $00, $00, $00,

  // Sprite Palette
   $0f, $20, $00, $00,
   $0f, $00, $00, $00,
   $0f, $00, $00, $00,
   $0f, $00, $00, $00
  );

procedure writePPU(ax : Word); assembler;
// ax   = address of PPU write; A = MSB, X = LSB
asm
  sta PPU.address
  stx PPU.address
  
  ldy #0
!writeLoop:
  lda (ppu_data_pntr),y
  sta PPU.data
  iny
  cpy ppu_data_size
  bcc !writeLoop-  
end;

procedure nmi; assembler; volatile;
asm
  pha
  txa
  pha
  tya
  pha

  // do stuff


  pla
  tay
  pla
  tax
  pla
  rti
end;

procedure irq; assembler; volatile;
asm
  rti
end;

procedure copyCHRtoRam;
begin
  PPU.mask      := 0;
  ppu_data_pntr := @helloSpriteTiles;
  ppu_data_size := Length(helloSpriteTiles);
  
  writePPU($0000);
end;

procedure loadPalettes;
begin
  // copy palettes to address $3f00
  ppu_data_pntr := @palettes;
  ppu_data_size := Length(palettes);
  
  writePPU($3f00);
end;

procedure loadSpritesToRAM;
begin
  OAM.dma := $02;    
   
  // Load the hello message into SPR-RAM
  for i := 0 to Length(sprites) - 1 do
    OAM.data := sprites[i];
end;

procedure reset;
begin
  // ignore IRQs
  disable_irq;
  // disable decimal mode
  decimalModeOff;
  
  // disable APU frame IRQ
  IO.input_1 := $40;
  
  PPU.control := 0; // disable NMI
  PPU.mask    := 0; // disable rendering
  IO.dmc_freq := 0; // disable DMC IRQs
  
  asm
  //  set stack
  ldx #$ff
  txs

  bit PPU.status

  :VWAIT()
  end;
  
    // We now have about 30,000 cycles to burn before the PPU stabilizes.
    // One thing we can do with this time is put RAM in a known state.
    
  for i := 0 to 255 do begin
    // clear ram
    poke($0000+i,$00);
    poke($0100+i,$00);
    poke($0300+i,$00);
    poke($0500+i,$00);
    poke($0600+i,$00);
    poke($0700+i,$00);
    // set sprites off screen
    poke($0200+i,$ff);
  end;
end;

procedure init;
begin
  initRAM;
{    
  ldx #$00
  lda #$00
!clrmem:
  lda #$00
  sta $0000, x
  sta $0100, x
  sta $0300, x
  sta $0400, x
  sta $0500, x
  sta $0600, x
  sta $0700, x
  // set sprites to off screen
  lda #$ff
  sta $0200, x
  inx
  bne !clrmem-
  end;
}
  VWAIT;
  
  copyCHRtoRam;
  loadPalettes;
  loadSpritesToRAM;

  // enable NMI
  PPU.control := CTRL_NMI_ON;
  // enable Sprites
  PPU.mask := MASK_SP_ON;
end;

begin
  init();
  
  while true do;
end.