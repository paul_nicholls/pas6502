program nes_hello;

target = nes;

ImportSegments('segments_mapper_v.asm');
ImportHeader('header_mapper_v_ntsc.asm');

SetPRGROMsegment('BANK1');
SetIRQsegment('BANK2');

uses
  nes_hardware; 

#SEGMENT('CHARS');

const
  helloSpriteTiles : array of Byte = (
   %11000011,	// H (00)
   %11000011,
   %11000011,
   %11111111,
   %11111111,
   %11000011,
   %11000011,
   %11000011,
   $00, $00, $00, $00, $00, $00, $00, $00,

   %11111111,	// E (01)
   %11111111,
   %11000000,
   %11111100,
   %11111100,
   %11000000,
   %11111111,
   %11111111,
   $00, $00, $00, $00, $00, $00, $00, $00,

   %11000000,	// L (02)
   %11000000,
   %11000000,
   %11000000,
   %11000000,
   %11000000,
   %11111111,
   %11111111,
   $00, $00, $00, $00, $00, $00, $00, $00,

   %01111110,	// O (03)
   %11100111,
   %11000011,
   %11000011,
   %11000011,
   %11000011,
   %11100111,
   %01111110,
   $00, $00, $00, $00, $00, $00, $00, $00  
  );

#SEGMENT('RAM');

var
  i    : Byte;
  sync : Byte;

#SEGMENT('BANK1');

const
  SCREEN_ORIGIN_X = 8;
  SCREEN_ORIGIN_Y = 8;
  
  SPRITE_COUNT    = 5;
  
  sprites : array of Byte = (
// y    tile attr x
   $6c, $00, $00, $6c, // H tile
   $6c, $01, $00, $76, // E tile
   $6c, $02, $00, $80, // L tile
   $6c, $02, $00, $8A, // L tile
   $6c, $03, $00, $94  // O tile
  );

palettes : array of Byte = (
  // Background Palette
   $0f, $00, $00, $00,
   $0f, $00, $00, $00,
   $0f, $00, $00, $00,
   $0f, $00, $00, $00,

  // Sprite Palette
   $0f, $20, $00, $00,
   $0f, $00, $00, $00,
   $0f, $00, $00, $00,
   $0f, $00, $00, $00
  );

procedure writePPU(ppuAddr : Word; srcData : Pointer; len : Byte); cdecl;
begin
  PPU.address := ppuAddr.b1;
  PPU.address := ppuAddr.b0;
  
  for i := 0 to len - 1 do
    PPU.data := srcData^[i];
end;

procedure waitForSync; assembler;
asm
!loop:
  lda sync
  beq !loop- // loop while sync = 0
  // reset sync
  lda #0
  sta sync    
end;

  
procedure nmi; assembler; volatile;
asm
  inc sync
  rti
end;

procedure irq; assembler; volatile;
asm
  rti
end;

procedure loadPalettes;
begin
  // copy palettes to address $3f00
  writePPU($3f00,@palettes,Length(palettes));
end;

procedure loadSpritesToRAM;
begin
  OAM.dma := $02;    
   
  // Load the hello message into SPR-RAM
  for i := 0 to Length(sprites) - 1 do
    OAM.data := sprites[i];
end;

procedure clear_vram; assembler;
asm
  lda #$00
  sta PPU.address
  sta PPU.address
  tay
  ldx #6
!clear_loop:
  sta PPU.data
  sta PPU.data
  sta PPU.data
  sta PPU.data
  sta PPU.data
  sta PPU.data
  sta PPU.data
  sta PPU.data
  dey
  bne !clear_loop-
  dex
  bne !clear_loop-
end;

procedure init;
begin
  // ignore IRQs
  disable_irq;
  // disable decimal mode
  decimalModeOff;
  
  sync := 0;
  
  // disable APU frame IRQ
  IO.input_1 := $40;
  
  PPU.control := 0; // disable NMI
  PPU.mask    := 0; // disable rendering
  IO.dmc_freq := 0; // disable DMC IRQs
  
  asm
  //  set stack
  ldx #$ff
  txs

  bit PPU.status

  :VWAIT()
    // We now have about 30,000 cycles to burn before the PPU stabilizes.
    // One thing we can do with this time is put RAM in a known state.
  ldx #$00
  lda #$00
!clrmem:
  lda #$00
  sta $0000, x
  sta $0100, x
  sta $0300, x
  sta $0400, x
  sta $0500, x
  sta $0600, x
  sta $0700, x
  // set sprites to off screen
  lda #$ff
  sta $0200, x
  inx
  bne !clrmem-
  end;

  VWAIT;
  clear_vram;
  
  loadPalettes;
  loadSpritesToRAM;
  
  // enable NMI
  PPU.control := CTRL_NMI_ON;
  // enable Sprites
  PPU.mask := MASK_SP_ON;
end;

begin
  init();
  
  while true do begin
    waitForSync;
  end;
end.