program circleCharacterMode;

target = pet;

const
  SCREEN0_ADDRESS = $8000;

  // screen size in characters
  SCREEN_WIDTH    = 80;
  SCREEN_HEIGHT   = 25;
  
  // screen size in characters
  XRES            = SCREEN_WIDTH;
  YRES            = SCREEN_HEIGHT;
  
  // screen character address information
  // to speed up access
  
  // screen y row address
  screenRow  : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (SCREEN_WIDTH * y);

  // sine table for 256 degrees in a circle
  // with values from -128 to +127 in a ShortInt
  
  // sin(x, n) � returns n*sin(x*PI/128)
  sinTable : array of ShortInt = for i := 0 to 256 - 1 do 
             (sin((256*i)/256,127));
             
var
  screen : Byte absolute SCREEN0_ADDRESS;
  
procedure fillScreen(screen : Pointer; value : Byte);
var
  i : Word;
begin
  i := 0;
  while i < SCREEN_WIDTH*SCREEN_HEIGHT do begin
    screen^[0] := value;
    inc(screen);
    inc(i);
  end;
end;

procedure plotChar(x,y,c : Byte);
var
  index : Word;
begin
  if x >= SCREEN_WIDTH  then Exit;
  if y >= SCREEN_HEIGHT then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  screen[index] := c;
end;

procedure main;
var
  sx      : Byte;
  sy      : Byte;
  i       : Byte;
  radiusx : ShortInt;
  radiusy : ShortInt;
  sValue  : ShortInt;
  cValue  : ShortInt;
  s       : Integer;
  cIndex  : Byte;
begin
  radiusy := SCREEN_HEIGHT/2;
  radiusx := radiusy*2;
  
  cIndex := 64;
  for i := 0 to 255 do begin
    cIndex := i + 64; // cos is 90 degrees out of phase with sin
    
    sValue := sinTable[i];    
    sy := SCREEN_HEIGHT/2 - (sValue * radiusy) / 128;
    
    cValue := sinTable[cIndex]; 
    sx := SCREEN_WIDTH/2 + (cValue * radiusx) / 128;
    
    plotChar(sx,sy,49);
  end;
  
  while true do;
end;

begin
  fillScreen(SCREEN0_ADDRESS,48);
  main;
end.