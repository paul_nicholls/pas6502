program PET_test;

target = pet;

uses
  random;
  
const
  SCREEN0_ADDRESS = $8000;
  SCREEN_WIDTH    = 80;
  SCREEN_HEIGHT   = 25;
  
  rowOffset : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (y * SCREEN_WIDTH);
  
  hex : String = '0123456789abcdef';
  
var
  screen : Byte absolute SCREEN0_ADDRESS;
  x,y,c  : Byte;
  ofs    : Word;
  
begin
  randInit(30);
  
  while true do begin
    x := randRange(SCREEN_WIDTH);
    y := randRange(SCREEN_HEIGHT);
    c := randRange(16)+1;
    
    ofs := rowOffset[y] + x;
    
    screen[ofs] := hex[c];
  end;
end.