program vdcHelloWorld;

target = c128;

uses
  c128,c128_vdcText;

var
  i : Byte;  
begin
  vdc_fillScreen(160);
  for i := 0 to 24 do begin
    vdc_setScreenRamPntrXY(i,i);
    vdc_printStr('hello world');
  end;
end.