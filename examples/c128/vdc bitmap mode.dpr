program vdcHelloWorld;

target = c128;

uses
  c128,c128_vdcBitmap;

const
  // sin(x, n) � returns n*sin(x*PI/128)
  sinTable : array of ShortInt = for i := 0 to 256 - 1 do 
             (sin((256*i)/256,127));
             
var
  sx      : Word;
  sy      : Byte;
  i       : Byte;
  radiusX : Byte;
  radiusY : Byte;
  sValue  : ShortInt;
  cValue  : ShortInt;
  cIndex  : Byte;
begin
  vdc_setBitmapMode;
  vdc_clearBitmapRam;
  vdc_setColors(vdc_color.white,vdc_color.dark_gray);

  cIndex := 64;
  radiusY := 5;
  radiusX := radiusY*2;
  while radiusY <= (VDC_YRES/2) do begin
    for i := 0 to 255 do begin
      sValue := sinTable[i];    
      sy := (VDC_YRES/2) - (sValue * radiusY) / 128;
    
      cValue := sinTable[cIndex]; 
      sx := (VDC_XRES/2) + (cValue * radiusX) / 128;
    
      vdc_plotPixel(sx,sy,1); 
    
      cIndex := cIndex + 1;
    end;
    radiusY := radiusY + 5;
    radiusX := radiusY*2;
  end;
  
  while true do;
end.