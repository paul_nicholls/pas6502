program helloWorld;

target = c128;

uses
  c128;
  
const
  name : petscii = 'paul';

begin
  // set to bank 15 to access Kernal routines
  selectBank15;
  writeLn('hello from pas6502, ',name);
end.