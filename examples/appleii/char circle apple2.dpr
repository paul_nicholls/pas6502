program char_circle_appleii;

target = appleii;

* = $0c00;

const  
  SCREEN0_ADDRESS = 1024;
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 24;
  
  ASCII_INVERSE  = 0;
  ASCII_FLASHING = 64;
  ASCII_NORMAL   = 128;
  
  screenRow : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (((y-(y/8)*8)*128+SCREEN_WIDTH*(y/8)));

  // sin(x, n) � returns n*sin(x*PI/128)
  sinTable : array of ShortInt = for i := 0 to 256 - 1 do 
             (sin((256*i)/256,127));
             
var
  screen : byte absolute SCREEN0_ADDRESS;

procedure setCharAt(x,y,c : Byte);
var
  ofs : Word;
begin
  if x >= SCREEN_WIDTH  then Exit;
  if y >= SCREEN_HEIGHT then Exit;
  
  ofs := screenRow[y];
  ofs := ofs + x;
  
  screen[ofs] := c;
end;

procedure main;
var
  sx     : Byte;
  sy     : Byte;
  i      : Byte;
  radius : ShortInt;
  sValue : ShortInt;
  cValue : ShortInt;
  s      : Integer;
  cIndex : Byte;
begin
  radius := SCREEN_HEIGHT/2-1;
  
  cIndex := 64;
  for i := 0 to 255 do begin
    cIndex := i + 64; // cos is 90 degrees out of phase with sin
    
    sValue := sinTable[i];    
    sy := SCREEN_HEIGHT/2 - 1 - (sValue * radius) / 128;
    
    cValue := sinTable[cIndex]; 
    sx := SCREEN_WIDTH/2 + (cValue * radius) / 128;
    
    setCharAt(sx,sy,49 or ASCII_NORMAL);
  end;
  
  while true do;
end;

procedure fillScreen(addr : Word; value : Byte);
var
  x : Byte;
  y : Byte;
  p : Pointer;
begin
  for y := 0 to SCREEN_HEIGHT - 1 do begin
    p := addr + screenRow[y];
    
    for x := 0 to SCREEN_WIDTH - 1 do begin
      p^[0] := value;
      Inc(p);
    end;
  end;
end;


begin
  fillScreen(SCREEN0_ADDRESS,48 or ASCII_INVERSE);

  main;
end.