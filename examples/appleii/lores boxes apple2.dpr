program lores_boxes_appleii;
// adapted from here:
// https://www.appleoldies.ca/azgraphics33/lodemo.htm

target = appleii;

* = $0c00;

uses
  appleii_graphics,appleii_lores,appleii_input;
  
const
  PAGE = 0;
  
  SCREEN_WIDTH  = 40;
  SCREEN_HEIGHT = 48;

var
  x1,y1,x2,y2, color : Byte;
begin
  setcrtmode(LORES);
  clearkey();
 
  loclear();
  
  lobox(0,0,39,47,LOPURPLE,PAGE);

  while true do begin
    if (kbhit() > 0) then begin
      clearkey();
      break;
    end;
  end;
  
  loclear();

  x1 := 0;
  x2 := SCREEN_WIDTH - 1;
  y1 := 0;
  y2 := SCREEN_HEIGHT - 1;

  for color := 1 to 15 do begin
    lobox(x1,y1,x2,y2, color,PAGE);
    Inc(y1);
    Dec(y2);
    Inc(x1);
    Dec(x2);
  end;

  while true do begin
    if (kbhit() > 0) then begin
      clearkey();
      break;
    end;
  end;

  loclear();
  setcrtmode(TEXTMODE);
  scr_apple();
  reboot();
end.