program lores_random_plot_appleii;
// graphics stuff from adapted from here:
// https://www.appleoldies.ca/azgraphics33/lodemo.htm

target = appleii;

* = $0c00;

uses
  appleii_graphics,appleii_lores,appleii_input,random;
  
const
  PAGE = 0;
  
  SCREEN_WIDTH  = 40;
  SCREEN_HEIGHT = 48;

var
  x,y,c : Byte;
begin
  setcrtmode(LORES);
  clearkey();
 
  loclear();
  
  randInit(30);
  
  while true do begin
    x := randRange(SCREEN_WIDTH);
    y := randRange(SCREEN_HEIGHT);
    c := randRange(16);
    
    loplot(x,y,c,PAGE);
  end;
end.