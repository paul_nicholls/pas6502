program hello_world_appleii;

target = appleii;

* = $0c00;

const
  SCREEN0_ADDRESS = 1024;
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 24;
  
  screenRow : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (((y-(y/8)*8)*128+SCREEN_WIDTH*(y/8)));

var
  screen : byte absolute SCREEN0_ADDRESS;

procedure setCharAt(x,y,c : Byte);
var
  ofs : Word;
begin
  ofs := screenRow[y];
  ofs := ofs + x;
  
  screen[ofs] := c;
end;

procedure drawTextAt(x,y : Byte; txt : Pointer);
var
  ofs : Word;
  i   : Byte;
begin
  ofs := screenRow[y];
  ofs := ofs + x;
  
  for i := 1 to txt^[0] do begin
    screen[ofs] := txt^[i] or 128;
    Inc(ofs);
  end;
end;

var
  x,y : Byte;
begin
  x := 5;
  y := 3;
  
  drawTextAt(x,y,'HELLO WORLD!'a);
end.