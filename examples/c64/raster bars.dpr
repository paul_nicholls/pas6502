program rasterBars;

uses
  c64_vic;
  
const
  colours : array of Byte = (
    $06, $06, $06, $0e, $06, $0e, 
    $0e, $06, $0e, $0e, $0e, $03, 
    $0e, $03, $03, $0e, $03, $03, 
    $03, $01, $03, $01, $01, $03, 
    $01, $01, $01, $03, $01, $01, 
    $03, $01, $03, $03, $03, $0e, 
    $03, $03, $0e, $03, $0e, $0e, 
    $0e, $06, $0e, $0e, $06, $0e, 
    $06, $06, $06, $00, $00, $00
  );
  
  BAR_START = 59;
  BAR_END   = $a0;
  BAR_SPEED = 2;

macro stabilize(x); assembler;
asm
  ldx #x
!burn:
  dex
  bne !burn-
end;

procedure main;
var
  colour,colour_index,wait_for,bar_start,velocity : Byte;
begin
  colour := 0;
  disable_irq();
  vic.cr1 := 0;
  vic.border := black;
  bar_start := BAR_START;
  velocity  := BAR_SPEED;
  
  while true do begin
    wait_for := bar_start;
    
    for colour_index := 0 to length(colours) - 1 do begin
      colour := colours[colour_index];
        
      while wait_for <> vic.raster do;
      stabilize(8);
      vic.border := colour;
      inc(wait_for,2);
    end;
    
    inc(bar_start,velocity);
    if bar_start <= BAR_START then velocity := BAR_SPEED;
    if bar_start >= BAR_END   then velocity := 255-BAR_SPEED;
  end;
end;


begin
  main;
end.