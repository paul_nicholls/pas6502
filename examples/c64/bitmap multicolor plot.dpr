program bitmapPlot;
uses
  c64_vic;

const
  BITMAP_ADDRESS  = $2000;
  SCREEN0_ADDRESS = 1024;

  // screen size in characters
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  
  // screen size in pixels (direct access)
  XRES            = SCREEN_WIDTH  * 8 / 2;
  YRES            = SCREEN_HEIGHT * 8;
  
  // screen character address information
  // to speed up access
  
  // screen y address
  screenRow  : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (BITMAP_ADDRESS + 320 * y);
  // screen x offset to add to the y address
  screenXOfs : array of Word = for x := 0 to SCREEN_WIDTH - 1  do (x*8);

  MIN_SIN   = 0;
  MAX_SIN   = YRES div 2;
  
  SIN_AMPL  = MAX_SIN - MIN_SIN - 1;
  SIN_WIDTH = XRES;
  
  // sine table to match width and height of multicolor screen
  // sin(x, n) � returns n*sin(x*PI/128)
  sinTable : array of Byte = for i := 0 to SIN_WIDTH - 1 do 
             (sin((256*i)/SIN_WIDTH,SIN_AMPL));
             
var
  bitmap : Byte absolute BITMAP_ADDRESS;
  screen : Byte absolute SCREEN0_ADDRESS;
  
procedure setBitmapMode; 
begin
  // bitmap mode
  Poke(53265,Peek(53265) or 32);

  // bitmap at $2000
  Poke($d018,Peek($d018) or 8);

  // multicolor bitmap mode
  Poke(53270,Peek(53270) or 16);
end;

procedure fillBitmapData(a : Byte); assembler;
  // clear bitmap plot data
asm
  ldx #0
!loop:
  sta BITMAP_ADDRESS + $0000,x
  sta BITMAP_ADDRESS + $0100,x
  sta BITMAP_ADDRESS + $0200,x
  sta BITMAP_ADDRESS + $0300,x
  sta BITMAP_ADDRESS + $0400,x
  sta BITMAP_ADDRESS + $0500,x
  sta BITMAP_ADDRESS + $0600,x
  sta BITMAP_ADDRESS + $0700,x

  sta BITMAP_ADDRESS + $0800,x
  sta BITMAP_ADDRESS + $0900,x
  sta BITMAP_ADDRESS + $0a00,x
  sta BITMAP_ADDRESS + $0b00,x
  sta BITMAP_ADDRESS + $0c00,x
  sta BITMAP_ADDRESS + $0d00,x
  sta BITMAP_ADDRESS + $0e00,x
  sta BITMAP_ADDRESS + $0f00,x

  sta BITMAP_ADDRESS + $1000,x
  sta BITMAP_ADDRESS + $1100,x
  sta BITMAP_ADDRESS + $1200,x
  sta BITMAP_ADDRESS + $1300,x
  sta BITMAP_ADDRESS + $1400,x
  sta BITMAP_ADDRESS + $1500,x
  sta BITMAP_ADDRESS + $1600,x
  sta BITMAP_ADDRESS + $1700,x

  sta BITMAP_ADDRESS + $1800,x
  sta BITMAP_ADDRESS + $1900,x
  sta BITMAP_ADDRESS + $1a00,x
  sta BITMAP_ADDRESS + $1b00,x
  sta BITMAP_ADDRESS + $1c00,x
  sta BITMAP_ADDRESS + $1d00,x
  sta BITMAP_ADDRESS + $1e00,x
  sta BITMAP_ADDRESS + $1f00,x
  inx
  bne !loop-
end;  

procedure fillBitmapColor(color0,color1,color2,color3 : Byte);
  // setup bitmap colours
begin
  vic.bg_color0 := color0;
  vic_fillScreen(SCREEN0_ADDRESS,(color1 shl 4) or color2);
  vic_fillScreen($d800,color3);
end;

function getTileBitmapAddress(tx,ty : Byte) : Word;
begin
  Result := screenRow[ty];
  Result := Result + screenXOfs[tx];
end;

procedure plotPixel_multicolor(x : Word; y : Byte; c : Byte; dontOverwrite : Boolean);
const
  cMask : array[0..3] of Byte = (
    %00111111,
    %11001111,
    %11110011,
    %11111100
  );

  cColorSet : array[0..15] of Byte = (
  // shift pos
    %00000000,%00000000,%00000000,%00000000, // color
    %01000000,%00010000,%00000100,%00000001,
    %10000000,%00100000,%00001000,%00000010,
    %11000000,%00110000,%00001100,%00000011
  );

  cColorMask : array[0..3] of Byte = (
    %11000000,%00110000,%00001100,%00000011
  );

var
  cx,cy : Byte;
  ox,oy : Byte;
  addr  : Pointer;
  data  : Byte;
begin
  if x >= XRES then Exit;
  if y >= YRES then Exit;
  
  x := x * 2;
  
  ox := (x and 7) / 2; // offs x; 0..3
  oy := y and 7;       // offs y; 0..7

  cx := x shr 3;       // cell x
  cy := y shr 3;       // cell y
  
  addr := getTileBitmapAddress(cx,cy);
  
  data := addr^[oy];
  
  if dontOverwrite then begin
    if data and cColorMask[ox] then
    // don't plot this pixel 
      Exit;
  end;

  data := data and cMask[ox];

  if c = 0 then Exit;
  c := (c * 4) + ox;
  c := cColorSet[c];
  
  addr^[oy] := data or c
end;

  var
  wOX,wOY : Byte;  
  
procedure getCharDataFromROM(data : Pointer; chrCode : Byte);
var
  i       : Byte;
  srcData : Pointer;
begin
  disable_irq;
  // make the CPU see the Character Generator ROM
  poke($01,$33);
  
  // setup source data pointer
  srcData := chrCode * 8;
  srcData := srcData + $d000+2048;
  
  // copy data to buffer
  for i := 0 to 7 do
    data^[i] := srcData^[i];
  
  // set ROM to normal
  poke($01,$37);
  
  enable_irq;
end;
  
procedure plotWarpedPixel(x,y,c : Byte);
var
  sx,sy : Byte;
begin
  // y vector offset (1 double pixel right, 1 down per y) 
  sx := y;//(y+1) div 2;
  sy := y;// div 2;

  // x vector offset next (1 double pixel right, 1 up per x)
  sx := sx + x;
  sy := sy - x; 

  // add together with world offset
  sx := sx + wOX;  
  sy := sy + wOY;  
  
  plotPixel_multicolor(sx,sy,c,true);
end; 

const
  voxelData : array of Byte = (
    0,0,1,1,1,0,0,0,
    0,1,1,4,4,1,0,0,
    1,1,4,1,1,4,1,1,
    0,0,1,4,4,1,1,0,
    0,0,0,1,1,1,0,0
  );

procedure plotVoxel(x,y,z : Byte);
const
  VOXEL_HEIGHT = 2;
  
var
  sx,sy,i : Byte;
begin
  // y-vector
  sx := y;
  sy := y*2;
  
  // x-vector
  sx := sx + x;
  sy := sy - x*2;
  
  // height offset  
  sy := sy - (z*VOXEL_HEIGHT);
//  sx := sx - (z);
  
  // add to world origin
  sx := sx + wOX;  
  sy := sy + wOY;
  
  for i := 0 to VOXEL_HEIGHT - 1 do begin
    // draw left sides of voxel
    plotPixel_multicolor(sx,sy+2+i,2,true);
    plotPixel_multicolor(sx+1,sy+3+i,2,true);
  
    // draw right sides of voxel
    plotPixel_multicolor(sx+2,sy+3+i,3,true);
    plotPixel_multicolor(sx+3,sy+2+i,3,true);
  end;

  // draw top of voxel
  plotPixel_multicolor(sx+1,sy+0,1,false);
  plotPixel_multicolor(sx+2,sy+0,1,false);
  
  for i := 0 to 3 do
    plotPixel_multicolor(sx+i,sy+1,1,false);
  
  plotPixel_multicolor(sx+1,sy+2,1,false);
  plotPixel_multicolor(sx+2,sy+2,1,false);
end; 

procedure main;
const
  TEXT : String = 'Froody!';
  BIT  : array[0..7] of Byte = (128,64,32,16,8,4,2,1);
  HEIGHT = 8;
  
var
  i      : Byte;
  c      : Byte;
  screen : byte absolute SCREEN0_ADDRESS;
  color  : byte absolute $d800;
  data   : array[0..7] of Byte;
  x,y    : Byte;
  ox,oy  : Byte;
  b      : Byte;
  len    : Byte;
  clr    : Byte;
  tx     : Byte;
  sx,wx  : Byte;
  addr   : Pointer;
begin
  wOX := 40;
  wOY := YRES - HEIGHT;
  
  len := TEXT[0];
  tx  := len*8;
  wx := tx;

  for i := len downto 1 do begin  
    b := TEXT[i];
    getCharDataFromROM(@data,b);
    
    for y := 15 downto 0 do begin
      c := data[y/2];
      
      for x := 7 downto 0 do begin
        b := (c and BIT[x]);
        
        if b then begin
          plotVoxel(wx+x,y,15 - y)
        end;
      end;
    end;
    dec(wx,8);
  end;
  
  while true do;
end;

begin
  setBitmapMode;
  fillBitmapColor(black,white,purple,blue);
  fillBitmapData(0);
  main;
end.