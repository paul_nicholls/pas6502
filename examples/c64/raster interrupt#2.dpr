program rasterInterrupt2;

uses
  c64_vic,c64_irq;
  
const
  cRASTER_LINE1 = 0;
  cRASTER_LINE2 = 150;
  
procedure IRQ_raster2; Interrupt; forward;  
  
procedure IRQ_raster1; Interrupt;
begin
  vic.border := red;
  
  vic.raster := cRASTER_LINE2;
  IRQ_vector := @IRQ_raster2; 
end;

procedure IRQ_raster2; Interrupt;
begin
  vic.border := white;
  
  vic.raster := cRASTER_LINE1;
  IRQ_vector := @IRQ_raster1; 
end;

begin
  setupRasterIRQ(@IRQ_raster1,cRASTER_LINE1);
  
  while true do;
end.