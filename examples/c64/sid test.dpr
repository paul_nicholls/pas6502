program sid_test;
// some notes from "flight of the bumblebee"
//
// adapted from: 
// The Commodore 64 Music Book
//   A guide to programming music and sound
//     James Vogel and Nevin B. Scrimshaw
//     https://c64.xentax.com/images/The_Commodore_64_Music_Book.pdf

uses
  c64_vic,c64_sid,c64_irq;
 
const
  M          = 54272;
  FRAME_TIME = 50;
  MS         = 1000;
  
  PW         = 100;
  
  DELAY      = 250*FRAME_TIME/MS; // 12 jiffies
  
  WAVEFORM   = WAVE_TRIANGLE;
  
  IRQ_MUSIC_RASTER = 251;
  
  noteData : array of Byte = (
    255,12,255,12, 9, 4,  24, 9, 4,  24, 7, 4,  24, 7, 4,  24, 5, 4,  12, 10, 4,  24, 5, 4,  12, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  24, 9, 4,  24, 7, 4,  24, 7, 4,  24, 5, 4,  12, 10, 4,  24, 5, 4,  12, 9, 4,  48, 9, 4,  48, 9, 4,  48, 0, 5,  48, 2, 5,  48, 9, 4,  24, 9, 4,  24, 7, 4,  24, 7, 4,  24, 5, 4,  12, 10, 4,  24, 5, 4,  12, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  24, 9, 4,  24, 7, 4,  24, 7, 4,  24, 5, 4,  12, 10, 4,  24, 5, 4,  12, 9, 4,  48, 9, 4,  48, 9, 4,  48, 0, 5,  48, 2, 5,  24, 2, 5,  24,128

  ); 

  FreqTablePalLo : array[0..95] of byte = (
//   C   C#  D   D#  E   F   F#  G   G#  A   A#  B
    $16,$27,$39,$4b,$5f,$74,$8a,$a1,$ba,$d4,$f0,$0e,  // 0
    $2d,$4e,$71,$96,$be,$e7,$14,$42,$74,$a9,$e0,$1b,  // 1
    $5a,$9c,$e2,$2d,$7b,$cf,$27,$85,$e8,$51,$c1,$37,  // 2
    $b4,$38,$c4,$59,$f7,$9d,$4e,$0a,$d0,$a2,$81,$6d,  // 3
    $67,$70,$89,$b2,$ed,$3b,$9c,$13,$a0,$45,$02,$da,  // 4
    $ce,$e0,$11,$64,$da,$76,$39,$26,$40,$89,$04,$b4,  // 5
    $9c,$c0,$23,$c8,$b4,$eb,$72,$4c,$80,$12,$08,$68,  // 6
    $39,$80,$45,$90,$68,$d6,$e3,$99,$00,$24,$10,$ff   // 7
  );

  FreqTablePalHi : array[0..95] of byte = (
//   C   C#  D   D#  E   F   F#  G   G#  A   A#  B
    $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$02,  // 0
    $02,$02,$02,$02,$02,$02,$03,$03,$03,$03,$03,$04,  // 1
    $04,$04,$04,$05,$05,$05,$06,$06,$06,$07,$07,$08,  // 2
    $08,$09,$09,$0a,$0a,$0b,$0c,$0d,$0d,$0e,$0f,$10,  // 3
    $11,$12,$13,$14,$15,$17,$18,$1a,$1b,$1d,$1f,$20,  // 4
    $22,$24,$27,$29,$2b,$2e,$31,$34,$37,$3a,$3e,$41,  // 5
    $45,$49,$4e,$52,$57,$5c,$62,$68,$6e,$75,$7c,$83,  // 6
    $8b,$93,$9c,$a5,$af,$b9,$c4,$d0,$dd,$ea,$f8,$ff  // 7
  );
  
  octaveOffset : array of Byte = for i := 0 to 11 do (i * 12);
  
procedure sid_setFrequency(note, octave : byte; sid_voice : word);
var
  index : byte;
begin
  index := octaveOffset[octave] + note;
  
  poke(sid_voice + voice_frequency + 0,FreqTablePalLo[index]);
  poke(sid_voice + voice_frequency + 1,FreqTablePalHi[index]);
end;

function sid_getFrequency( note, octave : byte) : Word;
var
  index : byte;
begin
  index := octaveOffset[octave] + note;
  
  Result := FreqTablePalLo[index] + FreqTablePalHi[index] * 256;
end;

type
  PWord = ^Word;
  
var
  i,N : Byte;
  Z   : Word;
  H,L : Byte;
  O   : Byte;
  counter : Word;
  
procedure resetNoteData;
begin
  i := 0;
end;

function getNoteData : Byte;
begin
  Result := noteData[i];
  Inc(i);
end;

procedure IRQ_music; kernalInterrupt;
var
  L,H : Byte;
begin
  if counter = 0 then begin
  // not counting so get next note
    N := getNoteData;
//    H := getNoteData;
//    L := getNoteData;

    if N = 128 then
      resetNoteData()
    else
    if N = 255 then begin
    // pause
      counter := getNoteData();
    // hard reset
      SID.voice1.atk_dec := 0;
      SID.voice1.sus_rel := 0;
      
      // gate bit off
      SID.voice1.wave_form := WAVEFORM and GATE_BIT_OFF;
    end
    else begin
      O       := getNoteData+2; // octave
      counter := getNoteData();
      
      SID.voice1.atk_dec := attack_2mSec shl 4 + decay_1500mSec;   //168 -> 10 / 8
      SID.voice1.sus_rel := 10           shl 4 + release_750mSec; // 169 -> 10 / 9
  
//      sid_setFrequency(N,O,sid_v1);
      SID.voice1.fq := sid_getFrequency(N,O);
    
      // wave form gate bit on
      SID.voice1.wave_form := WAVEFORM or GATE_BIT_ON;
    end;
  end
  else begin
    Dec(counter);
    
    if counter = 2 then begin
    // hard reset
      SID.voice1.atk_dec := 0;
      SID.voice1.sus_rel := 0;
      
      // gate bit off
      SID.voice1.wave_form := WAVEFORM and GATE_BIT_OFF;
    end;
  end;
end;
  
begin
  sid_clearRegisters();
  
  // max volume
  SID.vol_mode     := 15;
  SID.voice1.pw.b0 := PW;
  SID.voice1.pw.b1 := PW;

  resetNoteData;
  
  counter := 0;
  
  setupKernalIRQ(@IRQ_music);
end.