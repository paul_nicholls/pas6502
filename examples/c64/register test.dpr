program registerTest;
uses
  c64,c64_vic;
  
const 
  SCREEN0_ADDRESS = 1024;
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  
  screen_lo: array of Byte = for i := 0 to SCREEN_WIDTH do <(SCREEN0_ADDRESS + i * SCREEN_WIDTH);
  screen_hi: array of Byte = for i := 0 to SCREEN_WIDTH do >(SCREEN0_ADDRESS + i * SCREEN_WIDTH);
  
procedure chrout(a : Byte) external $ffd2;

procedure plot(a,x,y : Byte); assembler;
// plot char a at x,y on screen
var
  p : Pointer;
asm
  pha
  
  lda screen_lo,y
  sta p + 0
  lda screen_hi,y
  sta p + 1

  txa
  tay
  pla
  sta (p),y
  rts
end;

var
  i,x,y : Byte;  
begin
  i := 0;
  for y := 0 to 5 - 1 do begin
    for x := 0 to 10 - 1 do begin
      plot(i,x,y);
      i := i + 1;
    end;
  end;

  for i := 0 to 26 - 1 do chrout(65+i);
end.