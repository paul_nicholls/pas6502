program actorsTest;

uses
  c64_vic,c64_actors,c64_joy;
  
const
  SCREEN0_ADDRESS = $0400;
  
  sprite : array of Byte = (
    0,127,0,1,255,192,3,255,224,3,231,224,
    7,217,240,7,223,240,7,217,240,3,231,224,
    3,255,224,3,255,224,2,255,160,1,127,64,
    1,62,64,0,156,128,0,156,128,0,73,0,0,73,0,
    0,62,0,0,62,0,0,62,0,0,28,0  
  );
    
  SPRITE_PLAYER = 0;
  SPRITE_ENEMY  = 1;
    
procedure init;
var
  i : Byte;
begin
  actors_init;
  
  // can be ignored for default $0400 screen location
  actors_initSpritePointers(SCREEN0_ADDRESS);
  
  // can be ignored for default offset of 0
  actors_setSpritePointer0offset(0);
  
  for i := 0 to length(sprite) - 1 do
    poke(832+i,sprite[i]);
    
  actor_enabled[SPRITE_PLAYER] := 1;
  actor_color[SPRITE_PLAYER]   := cyan;
  actor_frame[SPRITE_PLAYER]   := 13; // added to prite Pointer 0 offset value  

  actor_enabled[SPRITE_ENEMY]    := 1;
  actor_color[SPRITE_ENEMY]      := yellow;
  actor_frame[SPRITE_ENEMY]      := 13;
  actor_multicolor[SPRITE_ENEMY] := 1;  
end;

var
  i : Byte;
begin
  init;
  
  disable_irq;
  
  i := 0;
  while True do begin
    vic_waitForRaster(251);
    
    actor_setX(SPRITE_PLAYER,i); //UPDATE X COORDINATES
    actor_setY(SPRITE_PLAYER,i); //UPDATE Y COORDINATES
    
    actor_setX(SPRITE_ENEMY,255-i); //UPDATE X COORDINATES
    actor_setY(SPRITE_ENEMY,i); //UPDATE Y COORDINATES

    copyActorsToHardware;
      
    Inc(i);
  end;    
end.