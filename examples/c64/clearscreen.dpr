program test;
uses
  c64,c64_vic;
  
const
  SCREEN0 = 1024;
  COLOR   = $d800;
  
begin
  vic_clearScreen(SCREEN0,1);
  vic_clearScreen(COLOR,light_red);
end.