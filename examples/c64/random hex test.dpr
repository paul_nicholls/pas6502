program random_hex_test;
uses
  c64_vic,random;
  
const
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  SCREEN0_ADDRESS = $0400;
  
  cHexDigits : String = '0123456789abcdef';

  screenRow : array of word = for y := 0 to SCREEN_HEIGHT - 1 do (SCREEN0_ADDRESS + y * SCREEN_WIDTH);
  colorRow  : array of word = for y := 0 to SCREEN_HEIGHT - 1 do ($d800 + y * SCREEN_WIDTH);
      
var
  x,y,b     : Byte;
  screen    : Byte absolute SCREEN0_ADDRESS;
  color     : Byte absolute vic_color_ram;
  ptr       : Pointer;
begin
  randInit(100);
  
  vic.bg_color0 := black;
  vic.border    := black;
  
  vic_clearScreen(SCREEN0_ADDRESS,32);
  vic_clearScreen($d800,white);
  
  while true do begin
    x := randRange(SCREEN_WIDTH);
    y := randRange(SCREEN_HEIGHT);
    b := rand();
    
    ptr := screenRow[y];    
    ptr^[x] := cHexDigits[(rand and 15) + 1];
    
    ptr := colorRow[y];
    ptr^[x] := (b and 14) + 1;    
  end;
  
  while true do;
end.