program diamondSquare;

uses
  c64_vic;

var
  x,y    : ShortInt;
  t      : Byte;
  b      : Boolean;
  
  screen : Byte absolute 1024;
  index  : Word;
begin
  vic_clearScreen($0400,32);
  
  index := 0;
  
  for y := -9 to 9 do begin
    for x := 0 to 18 do begin
      t:= (x*x - y*y ) mod 6;
      
      b := (t <> 0) and 1;
      
      screen[index] := 42-10*b;
      inc(index);
    end;
    // set index back to new line and edge of screen
    index := index - 19 + 40;
  end;

  while true do;
end.