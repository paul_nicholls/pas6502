program pixelPlot128x128Charset;
uses
  c64_vic;
  
const
  SCREEN0_ADDRESS = 1024;
  CHARSET_ADDRESS = $2000;

var
  x,y : Byte;

procedure setScreenAndCharset; assembler;
asm
  // set the screen and character set locations 
  // (relative to the current bank)
  lda #[[SCREEN0_ADDRESS & $3FFF] / 64] | [[CHARSET_ADDRESS & $3FFF] / 1024]
  sta $D018
end;

macro drawCharset(x,y); assembler;
asm
.const icharhigh       = 16
.const icharwidth      = 16
.const scrwidth        = 40
  
  lda #<[SCREEN0_ADDRESS+x+y*40]
  sta i2 + 1
  lda #>[SCREEN0_ADDRESS+x+y*40]
  sta i2 + 2

  ldx #0
initic:
  txa
  ldy #0
i2:
  sta $ffff,y
  clc
  adc #icharhigh
  iny
  cpy #icharwidth
  bne i2

  lda i2+1
  clc
  adc #scrwidth
  sta i2+1
  bcc *+5
  inc i2+2

  inx
  cpx #icharhigh
  bne initic
end;

procedure clearCharset; assembler;
asm
  ldx #0
  lda #0
!:
  sta CHARSET_ADDRESS + 256*0,x
  sta CHARSET_ADDRESS + 256*1,x
  sta CHARSET_ADDRESS + 256*2,x
  sta CHARSET_ADDRESS + 256*3,x
  sta CHARSET_ADDRESS + 256*4,x
  sta CHARSET_ADDRESS + 256*5,x
  sta CHARSET_ADDRESS + 256*6,x
  sta CHARSET_ADDRESS + 256*7,x
  inx
  bne !-
end;

procedure plotPixel(x,y: Byte); assembler;
asm
  lda #<CHARSET_ADDRESS
  sta $fb
  
  // ptr = (x / 8) * 128
  txa
  lsr                     // x / 8
  lsr
  lsr
  
  lsr                     // * 128 (16-bit)
  ror $fb
  adc #>CHARSET_ADDRESS
  sta $fc
  
  // mask = 2 ^ (x & 3)
  txa
  and #%00000111
  tax
  lda ($fb),y
  ora bitmask,x
  sta ($fb),y
  rts

bitmask:     
  .byte $80,$40,$20,$10,$08,$04,$02,$01
end;

procedure drawHLine(x1,x2,y: Byte);
var
  t : Byte;
  x : Byte;
begin
  if x1 > x2 then begin
    t  := x1;
    x1 := x2;
    x2 := t;
  end;
  
  for x := x1 to x2 do
    plotPixel(x,y);
end;

macro setBorder(color); assembler;
asm
  lda #color
  sta vic.border
end;

begin
  disable_irq;
  
  setScreenAndCharset;
  clearCharset;

  vic_clearScreen(SCREEN0_ADDRESS,0);

  drawCharset(0,0);  

  for y := 0 to 127 do begin
    drawHLine(0,127,y);
  end;

//  for y := 0 to 127 do begin
//    for x := 0 to 127 do begin
//      plotPixel(x,y);
//    end;
//  end;
  
  while true do;
end.