program circleCharacterMode;
uses
  c64_vic;

const
  SCREEN0_ADDRESS = 1024;

  // screen size in characters
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  
  // screen size in characters
  XRES            = SCREEN_WIDTH;
  YRES            = SCREEN_HEIGHT;
  
  // screen character address information
  // to speed up access
  
  // screen y row address
  screenRow  : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (SCREEN_WIDTH * y);

  // sine table for 256 degrees in a circle
  // with values from -128 to +127 in a ShortInt
  
  // sin(x, n) � returns n*sin(x*PI/128)
  sinTable : array of ShortInt = for i := 0 to 256 - 1 do 
             (sin((256*i)/256,127));
             
var
  screen : Byte absolute SCREEN0_ADDRESS;
  color  : Byte absolute vic_color_ram;
  
procedure plotChar(x,y,c,col : Byte);
var
  index : Word;
begin
  if x >= 40 then Exit;
  if y >= 25 then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  screen[index] := c;
  color[index]  := col;
end;

procedure main;
var
  sx     : Byte;
  sy     : Byte;
  i      : Byte;
  radius : ShortInt;
  sValue : ShortInt;
  cValue : ShortInt;
  s      : Integer;
  cIndex : Byte;
begin
  radius := SCREEN_HEIGHT/2;
  
  cIndex := 64;
  for i := 0 to 255 do begin
    cIndex := i + 64; // cos is 90 degrees out of phase with sin
    
    sValue := sinTable[i];    
    sy := SCREEN_HEIGHT/2 - (sValue * radius) / 128;
    
    cValue := sinTable[cIndex]; 
    sx := SCREEN_WIDTH/2 + (cValue * radius) / 128;
    
    plotChar(sx,sy,49,white);
  end;
  
  while true do;
end;

begin
  vic_clearScreen(SCREEN0_ADDRESS,48);
  vic_clearScreen(vic_color_ram,cyan);
  main;
end.