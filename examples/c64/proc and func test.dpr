program procAndFuncTest;

function swap(x : Word) : Word;
// swaps the lo/high bytes around
begin
  result.b0 := x.b1;
  result.b1 := x.b0;
end;

function swap_asm(xa : Word) : Word; assembler;
// a = lsb
// x = msb
// swaps the lo/high bytes around
asm
  stx b
  tax
  lda b
  rts
b: .byte 0
end;

function f(a: word) : word; 
begin
  Result := a;
end;

function f2(a: byte) : byte; 
begin
  Result := a;
end;

function f2_asm(a: byte) : byte; assembler; 
asm
end;

procedure p2(ax : Word); assembler;
// x = lsb
// a = msb
asm
  stx 1024
  sta 1025
end;

procedure p3(x,y : Byte); assembler;
asm
  stx 1027
  sty 1028
end;

function fmul(x,y : Byte) : Byte;
begin
  Result := x*y;
end;

procedure chrout(a : Byte) external $ffd2;

function getSomeChar() : Byte;
begin
  Result := asc('a');
end;

var
  a,b : Word;
  c   : Byte;
begin
  // output 2 characters using KERNAL routine
  chrout(getSomeChar());
  chrout(asc('z'));
  // store lsb,msb at screen pos 0,1
  p2(f($c000));
  // store 1,2 at screen 3,4
  p3(1,2);
  writeLn(',',fmul(f2(4),f2_asm(5)));
end.