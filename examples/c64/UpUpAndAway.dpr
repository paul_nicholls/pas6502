program UpUpAndAway;
uses
  c64_vic;

const
  SCREEN_ADDRESS   = 1024;
  SPR_PNTR_ADDRESS = SCREEN_ADDRESS + (2040 - 1024);
  SCREEN_ORIGIN_X  = 24;
  SCREEN_ORIGIN_Y  = 50;
   
  sprite : array of Byte = (
    0,127,0,1,255,192,3,255,224,3,231,224,
    7,217,240,7,223,240,7,217,240,3,231,224,
    3,255,224,3,255,224,2,255,160,1,127,64,
    1,62,64,0,156,128,0,156,128,0,73,0,0,73,0,
    0,62,0,0,62,0,0,62,0,0,28,0  
  );
    
var
  i            : Byte;
  vic_spr_pntr : Byte absolute SPR_PNTR_ADDRESS;
begin
  vic.spr_ena := vic.spr_ena or 4; //ENABLE SPRITE 2
  
  vic_spr_pntr[2] := 13;//SPRITE 2 DATA FROM BLOCK 13
  
  // copy sprite data into block 13
  for i := 0 to Length(sprite) - 1 do begin
    poke(832+i,sprite[i]);
  end;
  
  disable_irq;
  
  while True do begin
    vic_waitForRaster(251);
    
    Inc(vic.spr2_x); //UPDATE X COORDINATES
    Inc(vic.spr2_y); //UPDATE Y COORDINATES
  end;    
end.