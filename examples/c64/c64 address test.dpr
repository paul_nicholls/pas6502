program c64_addressTest;

target = c64; // optional, defaults to c64

* = $c000;

uses 
  c64_vic;

begin
  disable_irq;
  
  while true do begin
    vic.border := vic.border + 1;
  end;  
end.