program rasterInterrupt1;

uses
  c64_vic,c64_irq;
  
const
  rasterLines : array[0..3] of Byte = (    
    $00, // 0,
    $40, // 64
    $80, // 128
    $c0  // 192
  );
  
  rasterColours : array[0..3] of Byte = (
    blue,
    red,
    white,
    cyan
  );

var
  rasterIndex : Byte = 0;
  
procedure IRQ_rasterColours; Interrupt;
begin
  vic.border := rasterColours[rasterIndex];
  
  Inc(rasterIndex);
  rasterIndex := rasterIndex and 3;
  
  vic.raster := rasterLines[rasterIndex]; 
end;

begin
  setupRasterIRQ(@IRQ_rasterColours,rasterLines[rasterIndex]);
  
  while true do;
end.