program textTest;

uses
  c64_vic;
  
const
  SCREEN0_ADDRESS = 1024;
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  
  rowOffset : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (SCREEN_WIDTH * y);
  
  MY_TEXT  = 'this is more text!';
  MY_TEXT1 = '------------------';
  stringTypedConst : String = 'hi there';
  
var
  screen : byte absolute SCREEN0_ADDRESS;
  color  : byte absolute vic_color_ram;
  
procedure drawText(x,y : Byte; msg : Pointer; c : Byte);
var
  addr : word;
  i    : Byte;
begin
  addr := rowOffset[y];
  addr := addr + x;
  
  for i := 1 to msg^[0] do begin
    screen[addr] := msg^[i];
    color[addr]  := c;
    Inc(addr);
  end;
end;

var
  i : Byte;
begin
  for i := 1 to 10 do
    drawText(i,i,'hello world',i-1);

  drawText(10,SCREEN_HEIGHT-7,@stringTypedConst,cyan);
    
  drawText(10,SCREEN_HEIGHT-5,MY_TEXT,light_grey);
  drawText(10,SCREEN_HEIGHT-4,MY_TEXT1,white);
  
  while true do;
end.