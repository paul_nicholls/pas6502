program recordTest;
type  
  TInt24 = record
    frac,lsb,msb : Byte;
  end;

  TTest = record
    a : Byte;
    b : Byte;
    c : TInt24;
  end;
  
var
  t : TTest;
begin
  t.a := 1;
  t.b := 2;
  t.c.frac := 4;
end.