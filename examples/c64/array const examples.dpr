program arrayConstExamples;
  
uses
  c64_vic;
  
const
  SCREEN0_ADDRESS = 1024;
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  a1           : array[0..3] of Byte = (1,2,3,4);
  a2           : array       of Word = (1,4,3,2,5,76,5,4,2,3);
  screenOffset : array       of Word = for y := 0 to SCREEN_HEIGHT - 1 do (y*40);

  lsbValues    : array of Byte = for i := 0 to 5 do <(i*100);
  msbValues    : array of Byte = for i := 0 to 5 do >(i*100);
  
  MIN_SIN   = 0;
  MAX_SIN   = SCREEN_HEIGHT div 2;
  
  SIN_AMPL  = MAX_SIN - MIN_SIN;
  SIN_WIDTH = SCREEN_WIDTH;
  
// sin(x, n) � returns n*sin(x*PI/128)
// cos(x, n) � returns n*cos(x*PI/128)
// tan(x, n) � returns n*tan(x*PI/128)
  sinTable : array of Byte = for i := 0 to SIN_WIDTH - 1 do 
             (sin((256*i)/SIN_WIDTH,SIN_AMPL));
var
  x,y    : Byte;
  screen : byte absolute SCREEN0_ADDRESS;
  color  : Byte absolute vic_color_ram;
  ofs    : Word;
begin
  vic_clearScreen(SCREEN0_ADDRESS,32);
  
  for x := 0 to SCREEN_WIDTH - 1 do begin
    y := (SCREEN_HEIGHT div 2) - sinTable[x];
    ofs := screenOffset[y];
    ofs := ofs + x; 
    
    screen[ofs] := 32+128;
    color[ofs]  := white;
  end;
  
  while true do;
end.