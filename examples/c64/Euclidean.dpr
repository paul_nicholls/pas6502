program Euclidean;
uses
  c64; // includes the write/writeLn routines
  
function eculidean(a,b : Byte) : Byte;
begin
  while (a <> b) do
    if a > b then
      a := a-b
    else
      b := b-a;

  Result := a;
end;

begin
  writeLn(eculidean(54,24));
end.