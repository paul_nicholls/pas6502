program bitmapCicleHires;
uses
  c64_vic;

const
  BITMAP_ADDRESS  = $2000;
  SCREEN0_ADDRESS = 1024;

  // screen size in characters
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  
  // screen size in pixels (direct access)
  XRES            = SCREEN_WIDTH  * 8;
  YRES            = SCREEN_HEIGHT * 8;
  
  // screen character address information
  // to speed up access
  
  // screen y address
  screenRow  : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (BITMAP_ADDRESS + 320 * y);
  // screen x offset to add to the y address
  screenXOfs : array of Word = for x := 0 to SCREEN_WIDTH - 1  do (x*8);

  MIN_SIN   = 0;
  MAX_SIN   = YRES div 2;
  
  SIN_AMPL  = MAX_SIN - MIN_SIN - 1;
  SIN_WIDTH = XRES;
  
  // sine table to match width and height of hi-res screen
  // sin(x, n) � returns n*sin(x*PI/128)
  sinTable : array of ShortInt = for i := 0 to 256 - 1 do 
             (sin((256*i)/256,127));
             
var
  bitmap : Byte absolute BITMAP_ADDRESS;
  screen : Byte absolute SCREEN0_ADDRESS;
  
procedure setBitmapMode; 
begin
  POKE(53272,PEEK(53272) or 8);
  POKE(53265,PEEK(53265) or 32);
end;

procedure fillBitmapData(a : Byte); assembler;
  // clear bitmap plot data
asm
  ldx #0
!loop:
  sta BITMAP_ADDRESS + $0000,x
  sta BITMAP_ADDRESS + $0100,x
  sta BITMAP_ADDRESS + $0200,x
  sta BITMAP_ADDRESS + $0300,x
  sta BITMAP_ADDRESS + $0400,x
  sta BITMAP_ADDRESS + $0500,x
  sta BITMAP_ADDRESS + $0600,x
  sta BITMAP_ADDRESS + $0700,x

  sta BITMAP_ADDRESS + $0800,x
  sta BITMAP_ADDRESS + $0900,x
  sta BITMAP_ADDRESS + $0a00,x
  sta BITMAP_ADDRESS + $0b00,x
  sta BITMAP_ADDRESS + $0c00,x
  sta BITMAP_ADDRESS + $0d00,x
  sta BITMAP_ADDRESS + $0e00,x
  sta BITMAP_ADDRESS + $0f00,x

  sta BITMAP_ADDRESS + $1000,x
  sta BITMAP_ADDRESS + $1100,x
  sta BITMAP_ADDRESS + $1200,x
  sta BITMAP_ADDRESS + $1300,x
  sta BITMAP_ADDRESS + $1400,x
  sta BITMAP_ADDRESS + $1500,x
  sta BITMAP_ADDRESS + $1600,x
  sta BITMAP_ADDRESS + $1700,x

  sta BITMAP_ADDRESS + $1800,x
  sta BITMAP_ADDRESS + $1900,x
  sta BITMAP_ADDRESS + $1a00,x
  sta BITMAP_ADDRESS + $1b00,x
  sta BITMAP_ADDRESS + $1c00,x
  sta BITMAP_ADDRESS + $1d00,x
  sta BITMAP_ADDRESS + $1e00,x
  sta BITMAP_ADDRESS + $1f00,x
  inx
  bne !loop-
end;  

procedure fillBitmapColor(pen,background : Byte);
  // setup bitmap colours; pen, background
var
  i : Byte;
  b : Byte;
begin
  b := (pen shl 4) or background;
  
  for i := 0 to 249 do begin
    screen[i + 000] := b;
    screen[i + 250] := b;
    screen[i + 500] := b;
    screen[i + 750] := b;
  end;  
end;

const
  // bit of pixel to light up (single pixel only)
  bits : array of Byte = (128,64,32,16,8,4,2,1);

procedure plotPixel_hires(x,y : Word);
var
  cx,cy : Byte;
  ox,oy : Byte;
  addr  : pointer;
begin
  cx := x shr 3;
  cy := y shr 3;
  ox := x and 7;
  oy := y and 7;
    
  addr := screenRow[cy];
  addr := addr + screenXOfs[cx];

  addr^[oy] := addr^[oy] or bits[ox];
end;

procedure main;
var
  sx     : Word;
  sy     : Byte;
  i,j    : Byte;
  radius : ShortInt;
  v      : ShortInt;
  sValue : ShortInt;
  cValue : ShortInt;
  s      : Integer;
  cIndex : Byte;
begin
  radius := 95;
  
  cIndex := 64;
  for i := 0 to 255 do begin
    sValue := sinTable[i];    
    sy := (YRES/2) - (sValue * radius) / 128;
    
    cValue := sinTable[cIndex]; 
    sx := word(XRES/2) + (cValue * radius) / 128;
    
    plotPixel_hires(sx,sy); 
    
    cIndex := (cIndex + 1) and $ff;
  end;
  
  while true do;
end;

begin
  setBitmapMode;
  fillBitmapColor(black,cyan);
  fillBitmapData(0);
  main;
end.