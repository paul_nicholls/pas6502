program pointersAndFriends;

type
  TMyRec = record
    a,b : Byte;
    c   : Word;
  end;
  
  PMyRec = ^TMyRec;
  PByte  = ^Byte;
  
procedure fillRec(r : PMyRec);
begin
  r^.a    := 3;
  r^.c.b0 := $00;
  r^.c.b1 := $c0;
end;
  
var
  m : TMyRec;
  p : PMyRec;
  b : PByte;
  
  a : array[0..3] of TMyRec;
  c : Word;
  
begin
  p := @m;   // pointer to the record m
  b := @m.b; // pointer to the byte b in the record
  
  b^      := 54;
 
  fillRec(p);
 
  writeLn(m.b); 
  writeLn(m.a);
  writeLn(m.c);
  
  a[3].c := $2000;
  
  c := a[3].c;
  writeLn(c);
end.