program screenFill;
const
  CHAR           = 1;
  SCREEN_ADDRESS = 1024;
  SCREEN_WIDTH   = 40;
  ROW            = 3;
var
  screen : Byte absolute SCREEN_ADDRESS;
  i      : Byte;
begin
  // clear screen
  i := 0;
  while i < 250 do begin
    screen[i + 0]   := CHAR;
    screen[i + 250] := CHAR;
    screen[i + 500] := CHAR;
    screen[i + 750] := CHAR;
    Inc(i);
  end;
  
  while true do;
end.