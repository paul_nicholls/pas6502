program arrayTest;

const
  c : array of Byte = (1,2,3,4,5,6,7);

var
  a : array[0..5] of Byte;
  b : array[0..50] of Word;
  
  screen : Byte absolute $0400;
  
  i : Byte;
begin
  i := 0;
  screen[i + 0] := 'a';
  
  a[4] := 5;
  b[4] := 3;
  a[3] := c[5];
end.