program chunkyPixelPlot;
// plots 80 x 50 resulution pixels using 
// built-in petscii characters!
  
uses
  c64_vic,petsciiPixels;
  
const
  SCREEN0_ADDRESS = 1024;
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  
const
  rowOffset : array  of Word = for y := 0 to SCREEN_HEIGHT - 1 do (y*40);

  MIN_SIN   = 0;
  MAX_SIN   = (RES_HEIGHT / 2) - 1;
  
  SIN_AMPL  = MAX_SIN - MIN_SIN;
  SIN_WIDTH = RES_WIDTH;
  
// sin(x, n) � returns n*sin(x*PI/128)
// cos(x, n) � returns n*cos(x*PI/128)
// tan(x, n) � returns n*tan(x*PI/128)
  sinTable : array of Byte = for i := 0 to SIN_WIDTH - 1 do 
             (sin((256*i)/SIN_WIDTH,SIN_AMPL));
             
var
  x,y    : Byte;
  screen : byte absolute SCREEN0_ADDRESS;
  color  : Byte absolute vic_color_ram;
  
// draw a screen code string at x,y with color c
procedure drawText(x,y : Byte; msg : Pointer; c : Byte);
var
  addr : word;
  i    : Byte;
begin
  addr := rowOffset[y];
  addr := addr + x;
  
  for i := 1 to msg^[0] do begin
    screen[addr] := msg^[i];
    color[addr]  := c;
    Inc(addr);
  end;
end;

begin
  initPlotCharIndex();
  
  for x := 0 to RES_WIDTH - 1 do begin
    y := (RES_HEIGHT div 2) - sinTable[x];
    
    plotPetsciiPixel(SCREEN0_ADDRESS,x,y,x);
  end;

  drawText(26,8,'80 x 50',light_grey);
  drawText(25,10,'text mode',white);
  drawText(5,SCREEN_HEIGHT - 5,'pixel plotting!',cyan);

  while true do;
end.