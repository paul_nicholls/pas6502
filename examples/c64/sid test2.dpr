program sid_test2;
// "popcorn"
//
uses
  c64_sid,c64_vic;
 
const
  octave = 6;
  
  octaveOffset : array of Byte = for i := 0 to 7 do (i * 12);  

  //  popcorn
  noteData : array of Byte = (
    255,12,255,12, 9, 4,  24, 9, 4,  24, 7, 4,  24, 7, 4,  24, 5, 4,  12, 10, 4,  24, 5, 4,  12, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  24, 9, 4,  24, 7, 4,  24, 7, 4,  24, 5, 4,  12, 10, 4,  24, 5, 4,  12, 9, 4,  48, 9, 4,  48, 9, 4,  48, 0, 5,  48, 2, 5,  48, 9, 4,  24, 9, 4,  24, 7, 4,  24, 7, 4,  24, 5, 4,  12, 10, 4,  24, 5, 4,  12, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  48, 9, 4,  24, 9, 4,  24, 7, 4,  24, 7, 4,  24, 5, 4,  12, 10, 4,  24, 5, 4,  12, 9, 4,  48, 9, 4,  48, 9, 4,  48, 0, 5,  48, 2, 5,  24, 2, 5,  24,128

  ); 
  
  FreqTablePalLo : array[0..95] of byte = (
//   C   C#  D   D#  E   F   F#  G   G#  A   A#  B
    $16,$27,$39,$4b,$5f,$74,$8a,$a1,$ba,$d4,$f0,$0e,  // 0
    $2d,$4e,$71,$96,$be,$e7,$14,$42,$74,$a9,$e0,$1b,  // 1
    $5a,$9c,$e2,$2d,$7b,$cf,$27,$85,$e8,$51,$c1,$37,  // 2
    $b4,$38,$c4,$59,$f7,$9d,$4e,$0a,$d0,$a2,$81,$6d,  // 3
    $67,$70,$89,$b2,$ed,$3b,$9c,$13,$a0,$45,$02,$da,  // 4
    $ce,$e0,$11,$64,$da,$76,$39,$26,$40,$89,$04,$b4,  // 5
    $9c,$c0,$23,$c8,$b4,$eb,$72,$4c,$80,$12,$08,$68,  // 6
    $39,$80,$45,$90,$68,$d6,$e3,$99,$00,$24,$10,$ff   // 7
  );

  FreqTablePalHi : array[0..95] of byte = (
//   C   C#  D   D#  E   F   F#  G   G#  A   A#  B
    $01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$01,$02,  // 0
    $02,$02,$02,$02,$02,$02,$03,$03,$03,$03,$03,$04,  // 1
    $04,$04,$04,$05,$05,$05,$06,$06,$06,$07,$07,$08,  // 2
    $08,$09,$09,$0a,$0a,$0b,$0c,$0d,$0d,$0e,$0f,$10,  // 3
    $11,$12,$13,$14,$15,$17,$18,$1a,$1b,$1d,$1f,$20,  // 4
    $22,$24,$27,$29,$2b,$2e,$31,$34,$37,$3a,$3e,$41,  // 5
    $45,$49,$4e,$52,$57,$5c,$62,$68,$6e,$75,$7c,$83,  // 6
    $8b,$93,$9c,$a5,$af,$b9,$c4,$d0,$dd,$ea,$f8,$ff  // 7
  );
  
const
  MS         = 1000;
  FRAME_TIME = 50;
  
type
  TInstrument = record
    waveform : Byte;
    attack   : Byte;
    decay    : Byte;
    sustain  : Byte;
    release  : Byte;
  end;
  
var
  notePntr   : Pointer;
  instruments : array[0..0] of TInstrument;
  
procedure delayMS(v : Word);
begin
  while v > 0 do begin
    vic_waitForRaster(251);
    Dec(v);
  end;
end;

procedure resetNoteData;
begin
  notePntr := @noteData;
end;

function getNoteData : Byte; 
begin
  Result := notePntr^;
  
  Inc(notePntr);
end;
  
function sid_getFrequency( note, octave : byte) : Word;
var
  index : byte;
begin
  index := octaveOffset[octave] + note;
  
  Result := FreqTablePalLo[index] + FreqTablePalHi[index] * 256;
end;

procedure sid_setFrequency(note, octave : byte; sid_voice : word);
var
  index : byte;
begin
  index := (octave * 12) + note;
  poke(sid_voice + voice_frequency + 0,FreqTablePalLo[index]);
  poke(sid_voice + voice_frequency + 1,FreqTablePalHi[index]);
end;

procedure sid_playNote(voice,instrument,note,octave : Byte);
begin
  sid_disableVoice(voice, instruments[instrument].waveform);
  sid_setFrequency(note,octave,voice);
  sid_setAttackDecay(instruments[instrument].attack,instruments[instrument].decay,voice);
  sid_setSustainRelease(instruments[instrument].sustain,instruments[instrument].release,voice);
  sid_enableVoice(voice, instruments[instrument].waveform);
  
  delayMS(10*FRAME_TIME/MS);
end;

var
  Z          : Word;
  H,L        : Byte;
  n,o,d      : Byte;
begin
  sid_clearRegisters();
  
  // max volume
  SID.vol_mode := 15;
  
  instruments[0].waveform := WAVE_TRIANGLE;
  instruments[0].attack   := attack_8mSec;
  instruments[0].decay    := decay_6mSec;
  instruments[0].sustain  := 15;
  instruments[0].release  := release_6mSec;
  
  resetNoteData;
  
  while true do begin
    n := getNoteData;

    if n = 128 then 
    // end of data, reset
      Exit
    else begin
      if n = 255 then begin
      // pause
        d := getNoteData div 2;
        
        delayMS(d);
      end
      else begin
      // normal note, get the octave next
        o := getNoteData;
        d := getNoteData div 2;
        
        sid_playNote(sid_v1,0,n,o);
        
        // duration
        delayMS(d);
      end;
    end;
    
    // time for release
//    delayMS(14*FRAME_TIME/MS);
  end;
end.