program macroTest;
uses
  c64_vic;

macro myMacro(color); assembler;
asm
  lda #color
  sta vic.border
end;

begin  
  myMacro(red);

  while true do;
end.