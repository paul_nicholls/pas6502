program textInput;

uses
  c64_input;

const
  nameFilter : petscii = ' abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

var
  name  : string[10];
  
begin
  POKE (53272,23);
  writeLn('Please enter your name');
  getTextInput(@nameFilter,@name,10);
  writeLn();
  writeLn;
  
  writeLn('Nice to meet you, ',name,'!');
  writeLn('Welcome to Pas6502 :)');
end.