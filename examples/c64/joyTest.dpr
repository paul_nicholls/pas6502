program joyTest;
uses
  c64_vic,c64_joy;

const  
  SCREEN0_ADDRESS       = $0400;
  
  SPR_PNTR_ADDRESS = SCREEN0_ADDRESS + (2040 - 1024);
  SCREEN_ORIGIN_X  = 24;
  SCREEN_ORIGIN_Y  = 50;
  SPRITE_SPEED     = 5;
  VBLANK_LINE      = 251;
  
  PLAYER_SPRITE = 2; // sprite 2
  
  // int24 velocity (16.8 format)
  // $MMLLFF, where M = msb, L = lsb, F - fractial part
  SPRITE_VEL = SPRITE_SPEED * 256 / 2;
   
  sprite : array of Byte = (
    0,127,0,1,255,192,3,255,224,3,231,224,
    7,217,240,7,223,240,7,217,240,3,231,224,
    3,255,224,3,255,224,2,255,160,1,127,64,
    1,62,64,0,156,128,0,156,128,0,73,0,0,73,0,
    0,62,0,0,62,0,0,62,0,0,28,0  
  );
    
var
  i            : Byte;
  vic_spr_pntr : Byte absolute SPR_PNTR_ADDRESS;

  // int24 position (16.8 format)
  // $MMLLFF, where M = msb, L = lsb, F - fractial part
  sprX         : Int24;
  sprY         : Int24;
begin
  
  //ENABLE SPRITE 2
  vic.spr_ena := vic.spr_ena or (1 shl PLAYER_SPRITE); 

  //SPRITE 2 DATA FROM BLOCK 13
  vic_spr_pntr[PLAYER_SPRITE] := 13;           
  
  // copy sprite data into block 13
  for i := 0 to Length(sprite) - 1 do begin
    poke(832 + i,sprite[i]);
  end;
 
  sprX := 256*(SCREEN_ORIGIN_X + (320-24)/2);
  sprY := 256*(SCREEN_ORIGIN_Y + (200-21)/2);
  
  vic.spr2_x := sprX.b1;
  vic.spr2_y := sprY.b1;
  
  vic.spr2_color := 1;
  
  disable_irq;
  
  while true do begin
    vic_waitForRaster(VBLANK_LINE);

    read_joy1and2;
    
    if      input_dx = 1  then sprX := sprX + SPRITE_VEL
    else if input_dx = -1 then sprX := sprX - SPRITE_VEL;
      
    if      input_dy = 1  then sprY := sprY + SPRITE_VEL
    else if input_dy = -1 then sprY := sprY - SPRITE_VEL;
    
    // copy LSBs to sprite pos (ignore fractional part)
    vic.spr2_x := sprX.b1;
    vic.spr2_y := sprY.b1;
    
    // sprite x MSB calc
    if sprX.b2 = 0 then
      vic.spr_hi_x := vic.spr_hi_x and (255 - (1 shl PLAYER_SPRITE))
    else
      vic.spr_hi_x := vic.spr_hi_x or (1 shl PLAYER_SPRITE); 
  end;
end.