program peekAndPokeTest;

var
  a : Word;
  i : Byte;
begin
  // 38-column mode  
  poke($d016,peek($d016) and %11110111);
  // 24-row mode
  poke(53265,peek(53265) and 240);

  poke(1025+40,peek(1025+40) or 1);
  
  a := 1025+40*2;
  for i := 0 to 10 - 1 do
    poke(a+i,i+1);
end.