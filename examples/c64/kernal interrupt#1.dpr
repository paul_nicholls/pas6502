program kernalInterrupt1;

uses
  c64_vic,c64_irq;
  
const
  cTEN = 48+10;
  
var
  frame  : Word = 0;
  count  : Byte = 0;
  screen : Byte absolute $0400;
  
procedure IRQ_counterTest; KernalInterrupt;
begin
  frame := frame + 1;
  if frame >= 50 then begin
    frame := 0;
   
    decimalModeOn;
    count := count + 1;
    decimalModeOff;

    screen[0] := 48+(count and $f0) shr 4;
    screen[1] := 48+(count and $0f);
  end;
end;

begin
  count := 0;
  screen[0] := 48+(count and $f0) shr 4;
  screen[1] := 48+(count and $0f);

  // point kernal interrupt to the routine
  setupKernalIRQ(@IRQ_counterTest);
  
  writeLn('hello world, while doing a kernal interrupt timer');
end.