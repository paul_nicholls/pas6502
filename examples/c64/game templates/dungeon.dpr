program dungeon;

uses
  c64_vic,c64_actors,c64_joy,c64_irq;

const
  SCREEN0_ADDRESS = 1024;
  COLOR_ADDRESS   = $d800;
  
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  
  IRQ_MAIN        = SCREEN_ORIGIN_Y+185;
  
  PLAYER_RIGHT_VEL = $0001EB;
  PLAYER_LEFT_VEL  = (PLAYER_RIGHT_VEL xor $ffffff) + 1;

  PLAYER_DOWN_VEL = $0001EB;
  PLAYER_UP_VEL   = (PLAYER_DOWN_VEL xor $ffffff) + 1;
  
  PLAYER_CHAR      = 0;
  BLANK_CHAR       = 32;
  SOLID_CHAR       = 160;
  TREASURE_CHAR    = 36;
  
  SPRITE_PLAYER   = 0;
  
  tileH           = 8;
  tileW           = 8;
  
  screenData : array of byte = (
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,160,160,160,160,160,160,160,160,160,160,160,160,160,160,160,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,160,160,160,160,160,160,160,160,160,160,160,160,160,160,160,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,
32,32,32,32,32,160,160,160,160,160,160,160,160,160,32,32,32,160,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,
32,32,32,32,32,160,160,160,160,160,160,160,160,160,32,32,32,160,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,
32,32,32,32,32,160,32,36,32,32,32,32,32,160,32,32,32,160,32,32,32,32,32,36,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,
32,32,32,32,32,160,32,32,32,32,32,32,32,160,32,32,32,160,32,32,32,32,32,32,32,32,160,160,160,160,160,160,160,32,32,32,32,32,32,32,
32,32,32,32,32,160,32,32,32,32,32,32,32,160,160,160,160,160,32,32,32,32,32,32,32,32,160,160,160,160,160,160,160,32,32,32,32,32,32,32,
32,32,32,32,32,160,32,32,32,32,32,32,32,160,160,160,160,160,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,160,32,32,32,32,32,32,32,32,32,32,32,32,32,32,0,32,32,32,32,32,160,160,160,160,160,160,160,160,160,160,160,160,160,32,
32,160,160,160,160,160,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,160,160,160,160,160,160,160,160,160,160,160,160,32,
32,160,160,160,160,160,32,32,32,32,32,160,160,160,160,160,160,160,160,160,160,32,32,32,32,32,32,32,32,32,32,32,36,32,32,32,32,32,160,32,
32,160,32,32,32,32,32,32,32,32,32,160,160,160,160,160,160,160,160,160,160,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,32,
32,160,32,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,32,160,32,32,32,32,32,160,160,160,160,160,160,32,32,32,32,32,32,160,32,
32,160,32,32,32,32,32,32,36,32,32,160,32,32,32,32,32,32,32,32,160,32,32,32,32,32,160,160,160,160,160,160,32,32,32,32,32,32,160,32,
32,160,32,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,32,160,32,32,36,32,32,160,32,32,32,32,160,32,32,32,32,32,32,160,32,
32,160,32,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,32,160,32,32,32,32,32,160,32,32,32,32,160,32,32,32,32,32,32,160,32,
32,160,32,32,36,32,32,32,32,32,32,160,32,32,32,32,32,32,32,32,160,32,32,32,32,32,160,32,32,32,32,160,32,32,32,32,32,32,160,32,
32,160,32,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,32,160,32,32,32,32,32,160,32,32,32,32,160,32,32,36,32,32,32,160,32,
32,160,32,32,32,32,32,32,32,32,32,160,32,32,32,32,32,32,32,32,160,36,32,32,32,32,160,32,32,32,32,160,32,32,32,32,32,32,160,32,
32,160,160,160,160,160,160,160,160,160,160,160,32,32,32,32,32,32,32,32,160,32,32,32,32,32,160,32,32,32,32,160,32,32,32,32,32,32,160,32,
32,160,160,160,160,160,160,160,160,160,160,160,32,32,32,32,32,32,32,32,160,160,160,160,160,160,160,32,32,32,32,160,160,160,160,160,160,160,160,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,160,160,160,160,160,160,32,32,32,32,160,160,160,160,160,160,160,160,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,
14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,14,14,14,14,14,14,14,
14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,15,12,12,12,12,12,12,12,12,12,12,12,12,12,12,15,14,14,14,14,14,14,14,
14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,
14,14,14,14,14,15,15,15,15,15,15,15,15,15,14,14,14,15,14,14,14,14,14,14,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,
14,14,14,14,14,15,12,12,12,12,12,12,12,15,14,14,14,15,14,14,14,14,14,14,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,
14,14,14,14,14,15,14,7,14,14,14,14,14,15,14,14,14,15,14,14,14,14,14,7,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,
14,14,14,14,14,15,14,14,14,14,14,14,14,15,14,14,14,15,14,14,14,14,14,14,14,14,15,15,15,15,15,15,15,14,14,14,14,14,14,14,
14,14,14,14,14,15,14,14,14,14,14,14,14,15,15,15,15,15,14,14,14,14,14,14,14,14,15,12,12,12,12,12,12,14,14,14,14,14,14,14,
14,14,14,14,14,15,14,14,14,14,14,14,14,12,12,12,12,12,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,14,14,14,14,14,14,
14,14,14,14,14,15,14,14,14,14,14,14,14,14,14,14,14,14,14,14,1,14,14,14,14,14,15,15,15,15,15,15,15,15,15,15,15,15,15,14,
14,15,15,15,15,15,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,12,12,12,12,12,12,12,12,12,12,12,12,15,14,
14,15,12,12,12,12,14,14,14,14,14,15,15,15,15,15,15,15,15,15,15,14,14,14,14,14,14,14,14,14,14,14,7,14,14,14,14,14,15,14,
14,15,15,14,14,14,14,14,14,14,14,15,12,12,12,12,12,12,12,12,15,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,15,14,
14,15,15,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,14,15,14,14,14,14,14,15,15,15,15,15,15,14,14,14,14,14,14,15,14,
14,15,14,14,14,14,14,14,7,14,14,15,14,14,14,14,14,14,14,14,15,14,14,14,14,14,15,12,12,12,12,15,14,14,14,14,14,14,15,14,
14,15,14,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,14,15,14,14,7,14,14,15,14,14,14,14,15,14,14,14,14,14,14,15,14,
14,15,14,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,15,15,14,14,14,14,14,15,14,14,14,14,15,14,14,14,14,14,14,15,14,
14,15,14,14,7,14,14,14,14,14,14,15,14,14,14,14,14,14,14,15,15,14,14,14,14,14,15,14,14,14,14,15,14,14,14,14,14,14,15,14,
14,15,14,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,15,15,14,14,14,14,14,15,14,14,14,14,15,14,14,7,14,14,14,15,14,
14,15,14,14,14,14,14,14,14,14,14,15,14,14,14,14,14,14,14,14,15,7,14,14,14,14,15,14,14,14,14,15,14,14,14,14,14,14,15,14,
14,15,15,15,15,15,15,15,15,15,15,15,14,14,14,14,14,14,14,14,15,14,14,14,14,14,15,14,14,14,14,15,14,14,14,14,14,14,15,14,
14,12,12,12,12,12,12,12,12,12,12,12,12,14,14,14,14,14,14,14,15,15,15,15,15,15,15,14,14,14,14,15,15,15,15,15,15,15,15,14,
14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,12,12,12,12,12,12,12,14,14,14,14,12,12,12,12,12,12,12,12,14,
14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14
  );
  

type
  TPlayer = record
    xPos : Int24;
    yPos : Int24;
    xVel : Int24;
    yVel : Int24;
    
    tx   : Byte;           // 0 // object's centre map tile index
    ty   : Byte;           // 0 // object's centre map tile index
    ltx  : Byte;           // 0 // object's left edge map tile index
    rtx  : Byte;           // 0 // object's right edge map tile index
    uty  : Byte;           // 0 // object's top edge map tile index
    dty  : Byte;           // 0 // object's bottom edge map tile index

    WallUL    : Boolean;   // FALSE // is or isn't a wall at object's top left corner
    WallUR    : Boolean;   // FALSE // is or isn't a wall at object's top right corner
    WallDL    : Boolean;   // FALSE // is or isn't a wall at object's bottom left corner
    WallDR    : Boolean;   // FALSE // is or isn't a wall at object's bottom right corner

    movingLeft  : Boolean;
    movingRight : Boolean;
    movingUp    : Boolean;
    movingDown  : Boolean;
    
    treasure    : Byte;
  end;
  
const
  screenRow : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (y*SCREEN_WIDTH);

var
  player : TPlayer;
  screen : byte absolute SCREEN0_ADDRESS;
  color  : byte absolute vic_color_ram;
  
  // interrupt sync variable
  sync   : Byte;
  
  
procedure getCharDataFromROM(data : Pointer; chrCode : Byte);
var
  i       : Byte;
  srcData : Pointer;
begin
  disable_irq;
  // make the CPU see the Character Generator ROM
  poke($01,$33);
  
  // setup source data pointer
  srcData := chrCode * 8;
  srcData := srcData + $d000+2048;
  
  // copy data to buffer
  for i := 0 to 7 do
    data^[i] := srcData^[i];
  
  // set ROM to normal
  poke($01,$37);
  
  enable_irq;
end;
  
procedure drawTextAt(x,y : Byte; txt : Pointer);
var
  i     : Byte;
  index : Word;
begin
  if txt^[0] = 0 then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  for i := 1 to txt^[0] do begin
    screen[index] := txt^[i];
    Inc(index);
  end;
end;

function getTileAt(x,y : Byte) : Byte;
var
  c     : Byte;
  index : Word;
begin
  Result := 0;
  
  if x >= SCREEN_WIDTH  then Exit; 
  if y >= SCREEN_HEIGHT then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  Result := screen[index]; 
end;

function isWallAt(x,y : Byte) : Boolean;
var
  c     : Byte;
  index : Word;
begin
  Result := True;
  
  if x >= SCREEN_WIDTH  then Exit; 
  if y >= SCREEN_HEIGHT then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  c := screen[index]; 
  
  Result := c = SOLID_CHAR;
end;  

procedure setCharAt(x,y,c : Byte);
var
  index : Word;
begin
  if x >= SCREEN_WIDTH  then Exit; 
  if y >= SCREEN_HEIGHT then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  screen[index] := c;
  color[index]  := 3;
end;
  
function toFixedPoint(v : Word) : Int24;
begin
  Result := v shl 8;
end;

function signedDiv8(v : Int24) : Byte;
begin
asm
ldx #2
!:
//lda v.b2
//asl
bit v.b2
ror v.b2
ror v.b1
ror v.b0
dex
bpl !-
lda v.b1
sta result
end;
end;

procedure getPlayerCentre;
var
  temp : Int24;
begin
  // get player centre tile
  temp := (player.xPos + 4*256);
  player.tx := signedDiv8(temp);

  temp := (player.yPos + 4*256);
  player.ty := signedDiv8(temp);
end;

procedure getPlayerCorners(cornersOfsX,cornersOfsY : Int24);
// get player tile corners, taking into account velocity
var
  temp : Int24;
begin
  getPlayerCentre;
  
  // get upper tile y
  temp := (player.yPos + cornersOfsY);  
  player.uty := signedDiv8(temp);
  
  // get lower tile y
  temp := (player.yPos + cornersOfsY + 7*256);
  player.dty := signedDiv8(temp);
  
  // get left tile x
  temp := (player.xPos + cornersOfsX);
  player.ltx := signedDiv8(temp);
  
  // get right tile x
  temp := (player.xPos + cornersOfsX + 7*256);
  player.rtx := signedDiv8(temp);
end;

procedure doUpDownCollisions;
begin
  // check up/down
  getPlayerCorners(0,player.yVel);
  
  player.WallUL := isWallAt(player.ltx,player.uty);
  player.WallUR := isWallAt(player.rtx,player.uty);
  player.WallDL := isWallAt(player.ltx,player.dty);
  player.WallDR := isWallAt(player.rtx,player.dty);
  
  if player.WallUL or player.WallDL or player.WallUR or player.WallDR then begin
    // hit tile up/down
    // set y to centre tile
    player.yPos := toFixedPoint(player.ty*tileH);
    // set velocity to 0
    player.yVel    := 0;
  end
  else begin
    // can move this direction
    player.yPos := player.yPos + player.yVel;
  end;
end;  

procedure doLeftRightCollisions;
begin
  // check left/right
  getPlayerCorners(player.xVel,0);
  
  player.WallUL := isWallAt(player.ltx,player.uty);
  player.WallUR := isWallAt(player.rtx,player.uty);
  player.WallDL := isWallAt(player.ltx,player.dty);
  player.WallDR := isWallAt(player.rtx,player.dty);
  
  if player.WallUL or player.WallDL or player.WallUR or player.WallDR then begin
    // hit tile left/right
    // set x to centre tile
    player.xPos := toFixedPoint(player.tx*tileW);
    // set velocity to 0
    player.xVel := 0;
  end
  else
    // can move this direction
    player.xPos := player.xPos + player.xVel;
end;

procedure doTileTypeChecks;
var
  playerTile : Byte;
begin
  playerTile := getTileAt(player.tx,player.ty);
  
  if playerTile = TREASURE_CHAR then begin
    Inc(player.treasure);
    
    setCharAt(10,1,48+player.treasure);
    setCharAt(player.tx,player.ty,BLANK_CHAR);  
  end;
end;
  
procedure doCollisions;
begin
  doUpDownCollisions;
  doLeftRightCollisions;
  doTileTypeChecks;
end;

procedure getPlayerInput;
begin
  read_joy1and2;
  
  player.movingLeft  := false;
  player.movingRight := false;
  player.movingUp    := False;
  player.movingDown  := false;
  
  player.xVel := 0;
  player.yVel := 0;
    
  if      input_dx = JOY_MINUS   then player.movingLeft  := true
  else if input_dx = JOY_PLUS    then player.movingRight := true;
  
  if      (input_dy = JOY_MINUS) then player.movingUp    := true
  else if (input_dy = JOY_PLUS)  then player.movingDown  := true;
  
  if      player.movingLeft  then player.xVel := PLAYER_LEFT_VEL
  else if player.movingRight then player.xVel := PLAYER_RIGHT_VEL;
  
  if      player.movingUp    then player.yVel := PLAYER_UP_VEL
  else if player.movingDown  then player.yVel := PLAYER_DOWN_VEL;
end;

procedure mainIRQ; interrupt;
begin
  getPlayerInput;
    
  vic.raster := IRQ_MAIN;
  
  sync := true;
end;
  
procedure waitForSync;
begin
  while not sync do;
  sync := false;
end;
  
procedure main;
begin
  sync := false;
  
  setupRasterIRQ(@mainIRQ,IRQ_MAIN);
//  enable_irq;

  while true do begin
    waitForSync;
    
    doCollisions();
    actor_setX(SPRITE_PLAYER,player.xPos.hiword);
    actor_setY(SPRITE_PLAYER,player.yPos.hiword);
    
    copyActorsToHardware;
  end;  
end;
  
procedure init;
var
  i : Byte;
  index  : Word;
  cIndex : Word;
  x,y : Byte;
  data : array[0..7] of Byte;
begin
  vic_clearScreen(SCREEN0_ADDRESS,32);
  vic_clearScreen(COLOR_ADDRESS,cyan);
  
  actors_init;

  actor_enabled[SPRITE_PLAYER] := 1;
  actor_color[SPRITE_PLAYER]   := white;
  actor_frame[SPRITE_PLAYER]   := 13;

  player.xPos := 0;
  player.yPos := 256*8;
  player.xPos := 256*8*20;
  player.yPos := 256*8*10;
  player.treasure := 0;
  
  actor_setX(SPRITE_PLAYER,player.xPos.hiword);
  actor_setY(SPRITE_PLAYER,player.yPos.hiword);
  
  for i := 0 to 63 do
  // clear sprite
    Poke(832+i,0); 

  getCharDataFromROM(@data,0);

  for i := 0 to 7 do
  // set sprite to a 8x8 pixel square
    Poke(832+(i*3),data[i]);
    
  // copy data to the screen
  index  := 0;
  cIndex := 1000;
  for y := 0 to SCREEN_HEIGHT - 1 do begin
    for x := 0 to SCREEN_WIDTH - 1 do begin
      i := screenData[index];
    
      if i = PLAYER_CHAR then begin
        i := 32;
      end;        
      
      screen[index] := i;
      color[index]  := screenData[cIndex];      
      Inc(index);
      Inc(cIndex);
    end;
  end;
  
  drawTextAt(0,0,'lives:    3');
  drawTextAt(0,1,'treasure: 0');
end;
  
begin
  init;
  main;
end.