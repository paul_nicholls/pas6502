program platformer_2xPlayers;

uses
  c64_vic,c64_actors,c64_joy,c64_irq;

const
  SCREEN0_ADDRESS = 1024;
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  
  IRQ_MAIN        = 251;//SCREEN_ORIGIN_Y+180;
  
  PLAYER_RIGHT_VEL = $0001EB;
  PLAYER_LEFT_VEL  = (PLAYER_RIGHT_VEL xor $ffffff) + 1;
  PLAYER_JUMP_VEL  = $FFFE14*3/2;
  CLIMB_DOWN_VEL   = $000133;
  CLIMP_UP_VEL     = (CLIMB_DOWN_VEL xor $ffffff) + 1;
  GRAVITY          = $00002F;
  
  BLANK_CHAR       = 32;
  SOLID_CHAR       = 160;
  LADDER_CHAR      = 35;
  
  SPRITE_PLAYER1   = 0;
  SPRITE_PLAYER2   = 1;
  
  tileH           = 8;
  tileW           = 8;
  
  screenData : array of byte = (
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,160,160,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,32,32,160,160,160,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,160,160,160,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,160,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,160,160,160,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,160,160,160,160,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,160,160,160,32,32,32,32,32,32,32,35,32,160,160,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,160,160,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,160,160,160,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,160,160,160,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,160,160,160,35,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,
32,32,32,32,160,160,160,32,35,32,32,32,32,32,32,32,32,35,32,32,32,160,160,160,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
  );
  

type
  TPlayer = record
    xPos : Int24;
    yPos : Int24;
    xVel : Int24;
    yVel : Int24;
    
    tx   : Byte;           // 0 // object's centre map tile index
    ty   : Byte;           // 0 // object's centre map tile index
    ltx  : Byte;           // 0 // object's left edge map tile index
    rtx  : Byte;           // 0 // object's right edge map tile index
    uty  : Byte;           // 0 // object's top edge map tile index
    dty  : Byte;           // 0 // object's bottom edge map tile index

    UpLeft    : Byte;      // FALSE // can move up left
    UpRight   : Byte;      // FALSE // can move up right
    DownLeft  : Byte;      // FALSE // can move down left
    DownRight : Byte;      // FALSE // can move down right

    WallUL    : Boolean;   // FALSE // is or isn't a wall at object's top left corner
    WallUR    : Boolean;   // FALSE // is or isn't a wall at object's top right corner
    WallDL    : Boolean;   // FALSE // is or isn't a wall at object's bottom left corner
    WallDR    : Boolean;   // FALSE // is or isn't a wall at object's bottom right corner

    movingLeft  : Boolean;
    movingRight : Boolean;
    movingUp    : Boolean;
    movingDown  : Boolean;
    
    inAir       : Boolean;
    onLadder    : Boolean;
  end;
  
  PPlayer = ^TPlayer;
  
const
  screenRow : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (y*SCREEN_WIDTH);

var
  player1,player2 : TPlayer;
  playerPntr      : PPlayer;
  
  screen : byte absolute SCREEN0_ADDRESS;
  color  : byte absolute vic_color_ram;
  
  // interrupt sync variable
  sync   : Byte;
  
  
procedure getCharDataFromROM(data : Pointer; chrCode : Byte);
var
  i       : Byte;
  srcData : Pointer;
begin
  disable_irq;
  // make the CPU see the Character Generator ROM
  poke($01,$33);
  
  // setup source data pointer
  srcData := chrCode * 8;
  srcData := srcData + $d000+2048;
  
  // copy data to buffer
  for i := 0 to 7 do
    data^[i] := srcData^[i];
  
  // set ROM to normal
  poke($01,$37);
  
  enable_irq;
end;
  
procedure drawTextAt(x,y : Byte; txt : Pointer);
var
  i     : Byte;
  index : Word;
begin
  if txt^[0] = 0 then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  for i := 1 to txt^[0] do begin
    screen[index] := txt^[i];
    Inc(index);
  end;
end;

function isNegative(v : Int24) : Boolean;
begin
  Result := v.b2 > 127;
end;  

function getTileAt(x,y : Byte) : Byte;
var
  c     : Byte;
  index : Word;
begin
  Result := 0;
  
  if x >= SCREEN_WIDTH  then Exit; 
  if y >= SCREEN_HEIGHT then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  Result := screen[index]; 
end;

function isWallAt(x,y : Byte) : Boolean;
var
  c     : Byte;
  index : Word;
begin
  Result := True;
  
  if x >= SCREEN_WIDTH  then Exit; 
  if y >= SCREEN_HEIGHT then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  c := screen[index]; 
  
  Result := c = SOLID_CHAR;
end;  

procedure setTileAt(x,y,v,c : Byte);
var
  index : Word;
begin
  if x >= SCREEN_WIDTH  then Exit; 
  if y >= SCREEN_HEIGHT then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  screen[index] := v;
  color[index]  := c;
end;
  
function toFixedPoint(v : Word) : Int24;
begin
  Result := v * 256;
end;

function signedDiv8(v : Int24) : Byte; assembler;
asm
  ldx #2
!:
  lda v.b2
  asl
  ror v.b2
  ror v.b1
  ror v.b0
  dex
  bpl !-
  lda v.b1
end;

procedure getPlayerCentre;
var
  temp : Int24;
begin
  // get player centre tile
  temp := (playerPntr^.xPos + 4*256);
  playerPntr^.tx := signedDiv8(temp);

  temp := (playerPntr^.yPos + 4*256);
  playerPntr^.ty := signedDiv8(temp);
end;

procedure getPlayerCorners(cornersOfsX,cornersOfsY : Int24);
// get player tile corners, taking into account velocity
var
  temp : Int24;
begin
  getPlayerCentre;
  
  // get upper tile y
  temp := (playerPntr^.yPos + cornersOfsY);  
  playerPntr^.uty := signedDiv8(temp);
  
  // get lower tile y
  temp := (playerPntr^.yPos + cornersOfsY + 7*256);
  playerPntr^.dty := signedDiv8(temp);
  
  // get left tile x
  temp := (playerPntr^.xPos + cornersOfsX);
  playerPntr^.ltx := signedDiv8(temp);
  
  // get right tile x
  temp := (playerPntr^.xPos + cornersOfsX + 7*256);
  playerPntr^.rtx := signedDiv8(temp);
end;

procedure doUpDownCollisions;
begin
  // check up/down
  getPlayerCorners(0,playerPntr^.yVel);
  
  playerPntr^.WallUL := isWallAt(playerPntr^.ltx,playerPntr^.uty);
  playerPntr^.WallUR := isWallAt(playerPntr^.rtx,playerPntr^.uty);
  playerPntr^.WallDL := isWallAt(playerPntr^.ltx,playerPntr^.dty);
  playerPntr^.WallDR := isWallAt(playerPntr^.rtx,playerPntr^.dty);
  
  if isNegative(playerPntr^.yVel) then begin
    // moving up
    if playerPntr^.WallUL or playerPntr^.WallUR then begin
      // hit tile above
      // set y to centre tile
      playerPntr^.yPos := toFixedPoint(playerPntr^.ty*tileH);
      // set velocity to 0
      playerPntr^.yVel := 0;
    end
    else
      // can move this direction
      playerPntr^.yPos := playerPntr^.yPos + playerPntr^.yVel;
  end
  else begin
    // moving down
    if playerPntr^.WallDL or playerPntr^.WallDR then begin
      // hit tile below
      // set y to centre tile
      playerPntr^.yPos := toFixedPoint(playerPntr^.ty*tileH);
      // set velocity to 0
      playerPntr^.yVel      := 0;

      if playerPntr^.inAir then begin
        playerPntr^.inAir     := False;
      end;
    end
    else begin
      // can move this direction
      playerPntr^.yPos := playerPntr^.yPos + playerPntr^.yVel;
      playerPntr^.inAir := True;
    end;
  end;

  if playerPntr^.onLadder then 
  // don't do gravity if on ladder
    exit;
  
  playerPntr^.yVel := playerPntr^.yVel + GRAVITY;
  
  getPlayerCorners(0,playerPntr^.yVel);
 
  playerPntr^.WallDL := isWallAt(playerPntr^.ltx,playerPntr^.dty);
  playerPntr^.WallDR := isWallAt(playerPntr^.rtx,playerPntr^.dty);
  
  if playerPntr^.WallDL or playerPntr^.WallDR then begin
    // hit tile below
    // set y to tile
    playerPntr^.yPos := toFixedPoint(playerPntr^.ty*tileH);
    // set velocity to 0
    playerPntr^.yVel  := 0;
    playerPntr^.inAir := False;
  end
  else
  begin
    playerPntr^.inAir := True;
  end;
end;  

procedure doLeftRightCollisions;
begin
  // check left/right
  getPlayerCorners(playerPntr^.xVel,0);
  
  playerPntr^.WallUL := isWallAt(playerPntr^.ltx,playerPntr^.uty);
  playerPntr^.WallUR := isWallAt(playerPntr^.rtx,playerPntr^.uty);
  playerPntr^.WallDL := isWallAt(playerPntr^.ltx,playerPntr^.dty);
  playerPntr^.WallDR := isWallAt(playerPntr^.rtx,playerPntr^.dty);
  
  if playerPntr^.WallUL or playerPntr^.WallDL or playerPntr^.WallUR or playerPntr^.WallDR then begin
    // hit tile left/right
    // set x to centre tile
    playerPntr^.xPos := toFixedPoint(playerPntr^.tx*tileW);
    // set velocity to 0
    playerPntr^.xVel := 0;
  end
  else
    // can move this direction
    playerPntr^.xPos := playerPntr^.xPos + playerPntr^.xVel;
end;

procedure doTileTypeChecks;
var
  playerTile : Byte;
begin
  playerTile := getTileAt(playerPntr^.tx,playerPntr^.ty);
  
  if playerPntr^.onLadder then begin
    if playerTile = LADDER_CHAR then begin
      // still on ladder so climb up/down

      playerPntr^.yVel := 0;

      if playerPntr^.movingUp then
        playerPntr^.yVel := CLIMP_UP_VEL
      else
      if playerPntr^.movingDown then
        playerPntr^.yVel := CLIMB_DOWN_VEL;

{      if not(playerPntr^.movingLeft or playerPntr^.movingRight) then begin
        // set x to centre tile
        playerPntr^.xPos := toFixedPoint(playerPntr^.tx*tileW);
        // set velocity to 0
        playerPntr^.xVel := 0;
      end;}
    end
    else
    if playerPntr^.movingLeft or playerPntr^.movingRight then begin
      // has left ladder on left or right side
      playerPntr^.onLadder := False;
      playerPntr^.inAir    := True;
    end
    else
    if playerPntr^.movingUp then begin
      // player has left top of ladder
      playerPntr^.onLadder := False;
      playerPntr^.yPos := toFixedPoint(playerPntr^.ty*tileH);
      // set velocity to 0
//      playerPntr^.yVel := 0;
    end;
  end
  else
  if playerPntr^.movingUp or playerPntr^.movingDown then begin
    if playerTile = LADDER_CHAR then begin
    // started climbing on ladder
      playerPntr^.onLadder := true;

      // set x to centre tile
      playerPntr^.xPos := toFixedPoint(playerPntr^.tx*tileW);
      // set velocity to 0
      playerPntr^.xVel := 0;
    end;
  end;
end;
  
procedure doCollisions;
begin
  doUpDownCollisions;
  doLeftRightCollisions;
  doTileTypeChecks;
end;

procedure getPlayerInput(input : Byte);
begin
  if input = 2 then
    read_joy2()
  else
    read_joy1();
    
  playerPntr^.movingLeft  := false;
  playerPntr^.movingRight := false;
  playerPntr^.movingUp    := False;
  playerPntr^.movingDown  := false;
  
  playerPntr^.xVel := 0;
    
  if      input_dx = JOY_MINUS   then playerPntr^.movingLeft  := true
  else if input_dx = JOY_PLUS    then playerPntr^.movingRight := true;
  
  if      (input_dy = JOY_MINUS) then playerPntr^.movingUp    := true
  else if (input_dy = JOY_PLUS)  then playerPntr^.movingDown  := true;
  
  if      playerPntr^.movingLeft  then playerPntr^.xVel := PLAYER_LEFT_VEL
  else if playerPntr^.movingRight then playerPntr^.xVel := PLAYER_RIGHT_VEL;
  
  if playerPntr^.movingUp and (playerPntr^.onLadder = false) and (playerPntr^.inAir = false) then begin
    playerPntr^.yVel      := PLAYER_JUMP_VEL;
    playerPntr^.inAir     := True;
  end;
end;

procedure mainIRQ; interrupt;
begin
  getPlayerInput(2);
    
  vic.raster := IRQ_MAIN;
    
  copyActorsToHardware;
  
  sync := true;
end;
  
procedure waitForSync; assembler;
asm
!:
  lda sync
  beq !-
  lda #$00
  sta sync
end;

procedure main;
begin
  sync := false;
  
  playerPntr := @player1;
    
  setupRasterIRQ(@mainIRQ,IRQ_MAIN);

  while true do begin
    waitForSync;
    
    playerPntr := @player1;
    getPlayerInput(1);
    doCollisions;

    playerPntr := @player2;
    getPlayerInput(2);
    doCollisions;

    actor_setX(SPRITE_PLAYER1,player1.xPos.hiword);
    actor_setY(SPRITE_PLAYER1,player1.yPos.hiword);

    actor_setX(SPRITE_PLAYER2,player2.xPos.hiword);
    actor_setY(SPRITE_PLAYER2,player2.yPos.hiword);
  end;  
end;
  
procedure init;
var
  i : Byte;
  c : Word;
  data : array[0..7] of Byte;
begin
  vic_clearScreen(SCREEN0_ADDRESS,32);

  actors_init;

  actor_enabled[SPRITE_PLAYER1] := 1;
  actor_color[SPRITE_PLAYER1]   := white;
  actor_frame[SPRITE_PLAYER1]   := 13;

  actor_enabled[SPRITE_PLAYER2] := 1;
  actor_color[SPRITE_PLAYER2]   := cyan;
  actor_frame[SPRITE_PLAYER2]   := 13;

  player1.xPos := 256*8*26;
  player1.yPos := 256*8*12;
  player1.xVel := 0;
  player1.yVel := 0;
  player1.inAir   := false;
  player1.onLadder := False;
  
  player2.xPos := 256*8*28;
  player2.yPos := 256*8*12;
  player2.xVel := 0;
  player2.yVel := 0;
  player2.inAir   := false;
  player2.onLadder := False;
  
  actor_setX(SPRITE_PLAYER1,player1.xPos.hiword);
  actor_setY(SPRITE_PLAYER1,player1.yPos.hiword);

  actor_setX(SPRITE_PLAYER2,player2.xPos.hiword);
  actor_setY(SPRITE_PLAYER2,player2.yPos.hiword);
  
  for i := 0 to 63 do
  // clear sprite
    Poke(832+i,0); 

  // get '@' data from ROM
  getCharDataFromROM(@data,0);

  for i := 0 to 7 do
  // set sprite to an '@'
    Poke(832+(i*3),data[i]);
    
  // copy data to the screen
  c := 0;
  while c < 1000 do begin 
    i := screenData[c];
    
    screen[c] := screenData[c];
    
    if i = LADDER_CHAR then
      color[c] := yellow
    else
      color[c] := cyan;
      
    Inc(c);
  end;

  drawTextAt(1,0,'  = platform');
  drawTextAt(0,2,'  = ladder');
  setTileAt(0,0,160,cyan);  
  setTileAt(0,2,$23,yellow);  
end;
 
begin
  init;
  main;
end.