program platformer_2xPlayers;

uses
  c64_vic,c64_actors,c64_joy,c64_irq;

const
  SCREEN0_ADDRESS = 1024;
  SCREEN_WIDTH    = 40;
  SCREEN_HEIGHT   = 25;
  
  IRQ_MAIN        = 251;//SCREEN_ORIGIN_Y+180;
  
  PLAYER_RIGHT_VEL = $0001EB;
  PLAYER_LEFT_VEL  = (PLAYER_RIGHT_VEL xor $ffffff) + 1;
  PLAYER_JUMP_VEL  = $FFFE14*3/2;
  CLIMB_DOWN_VEL   = $000133;
  CLIMP_UP_VEL     = (CLIMB_DOWN_VEL xor $ffffff) + 1;
  GRAVITY          = $00002F;
  
  BLANK_CHAR       = 32;
  SOLID_CHAR       = 160;
  LADDER_CHAR      = 35;
  
  SPRITE_PLAYER1   = 0;
  SPRITE_PLAYER2   = 1;
  
  tileH           = 8;
  tileW           = 8;
  
  screenData : array of byte = (
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,160,160,160,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,32,32,160,160,160,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,160,160,160,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,160,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,160,160,160,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,160,160,160,160,32,32,32,32,32,32,35,32,32,32,
32,32,32,32,32,32,32,160,160,160,32,32,32,32,32,32,32,35,32,160,160,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,160,160,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,160,160,160,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,160,160,160,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,160,160,160,35,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,
32,32,32,32,160,160,160,32,35,32,32,32,32,32,32,32,32,35,32,32,32,160,160,160,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,35,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,
32,32,32,32,32,32,32,32,35,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32
  );
  

type
  TPlayer = record
    xPos : Int24;
    yPos : Int24;
    xVel : Int24;
    yVel : Int24;
    
    tx   : Byte;           // 0 // object's centre map tile index
    ty   : Byte;           // 0 // object's centre map tile index
    ltx  : Byte;           // 0 // object's left edge map tile index
    rtx  : Byte;           // 0 // object's right edge map tile index
    uty  : Byte;           // 0 // object's top edge map tile index
    dty  : Byte;           // 0 // object's bottom edge map tile index

    UpLeft    : Byte;      // FALSE // can move up left
    UpRight   : Byte;      // FALSE // can move up right
    DownLeft  : Byte;      // FALSE // can move down left
    DownRight : Byte;      // FALSE // can move down right

    WallUL    : Boolean;   // FALSE // is or isn't a wall at object's top left corner
    WallUR    : Boolean;   // FALSE // is or isn't a wall at object's top right corner
    WallDL    : Boolean;   // FALSE // is or isn't a wall at object's bottom left corner
    WallDR    : Boolean;   // FALSE // is or isn't a wall at object's bottom right corner

    movingLeft  : Boolean;
    movingRight : Boolean;
    movingUp    : Boolean;
    movingDown  : Boolean;
    
    inAir       : Boolean;
    onLadder    : Boolean;
  end;
  
const
  screenRow : array of Word = for y := 0 to SCREEN_HEIGHT - 1 do (y*SCREEN_WIDTH);

var
  player : TPlayer;
  
  screen : byte absolute SCREEN0_ADDRESS;
  color  : byte absolute vic_color_ram;
  
  // interrupt sync variable
  sync   : Byte;
  
  
procedure getCharDataFromROM(data : Pointer; chrCode : Byte);
var
  i       : Byte;
  srcData : Pointer;
begin
  disable_irq;
  // make the CPU see the Character Generator ROM
  poke($01,$33);
  
  // setup source data pointer
  srcData := chrCode * 8;
  srcData := srcData + $d000+2048;
  
  // copy data to buffer
  for i := 0 to 7 do
    data^[i] := srcData^[i];
  
  // set ROM to normal
  poke($01,$37);
  
  enable_irq;
end;
  
procedure drawTextAt(x,y : Byte; txt : Pointer);
var
  i     : Byte;
  index : Word;
begin
  if txt^[0] = 0 then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  for i := 1 to txt^[0] do begin
    screen[index] := txt^[i];
    Inc(index);
  end;
end;

function isNegative(v : Int24) : Boolean;
begin
  Result := v.b2 > 127;
end;  

function getTileAt(x,y : Byte) : Byte;
var
  c     : Byte;
  index : Word;
begin
  Result := 0;
  
  if x >= SCREEN_WIDTH  then Exit; 
  if y >= SCREEN_HEIGHT then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  Result := screen[index]; 
end;

function isWallAt(x,y : Byte) : Boolean;
var
  c     : Byte;
  index : Word;
begin
  Result := True;
  
  if x >= SCREEN_WIDTH  then Exit; 
  if y >= SCREEN_HEIGHT then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  c := screen[index]; 
  
  Result := c = SOLID_CHAR;
end;  

procedure setTileAt(x,y,v,c : Byte);
var
  index : Word;
begin
  if x >= SCREEN_WIDTH  then Exit; 
  if y >= SCREEN_HEIGHT then Exit;
  
  index := screenRow[y];
  index := index + x;
  
  screen[index] := v;
  color[index]  := c;
end;
  
function toFixedPoint(v : Word) : Int24;
begin
  Result := v * 256;
end;

function signedDiv8(v : Int24) : Byte; assembler;
asm
  ldx #2
!:
  lda v.b2
  asl
  ror v.b2
  ror v.b1
  ror v.b0
  dex
  bpl !-
  lda v.b1
end;

procedure getPlayerCentre;
var
  temp : Int24;
begin
  // get player centre tile
  temp := (player.xPos + 4*256);
  player.tx := signedDiv8(temp);

  temp := (player.yPos + 4*256);
  player.ty := signedDiv8(temp);
end;

procedure getPlayerCorners(cornersOfsX,cornersOfsY : Int24);
// get player tile corners, taking into account velocity
var
  temp : Int24;
begin
  getPlayerCentre;
  
  // get upper tile y
  temp := (player.yPos + cornersOfsY);  
  player.uty := signedDiv8(temp);
  
  // get lower tile y
  temp := (player.yPos + cornersOfsY + 7*256);
  player.dty := signedDiv8(temp);
  
  // get left tile x
  temp := (player.xPos + cornersOfsX);
  player.ltx := signedDiv8(temp);
  
  // get right tile x
  temp := (player.xPos + cornersOfsX + 7*256);
  player.rtx := signedDiv8(temp);
end;

procedure doUpDownCollisions;
begin
  // check up/down
  getPlayerCorners(0,player.yVel);
  
  player.WallUL := isWallAt(player.ltx,player.uty);
  player.WallUR := isWallAt(player.rtx,player.uty);
  player.WallDL := isWallAt(player.ltx,player.dty);
  player.WallDR := isWallAt(player.rtx,player.dty);
  
  if isNegative(player.yVel) then begin
    // moving up
    if player.WallUL or player.WallUR then begin
      // hit tile above
      // set y to centre tile
      player.yPos := toFixedPoint(player.ty*tileH);
      // set velocity to 0
      player.yVel := 0;
    end
    else
      // can move this direction
      player.yPos := player.yPos + player.yVel;
  end
  else begin
    // moving down
    if player.WallDL or player.WallDR then begin
      // hit tile below
      // set y to centre tile
      player.yPos := toFixedPoint(player.ty*tileH);
      // set velocity to 0
      player.yVel      := 0;

      if player.inAir then begin
        player.inAir     := False;
      end;
    end
    else begin
      // can move this direction
      player.yPos := player.yPos + player.yVel;
      player.inAir := True;
    end;
  end;

  if player.onLadder then 
  // don't do gravity if on ladder
    exit;
  
  player.yVel := player.yVel + GRAVITY;
  
  getPlayerCorners(0,player.yVel);
 
  player.WallDL := isWallAt(player.ltx,player.dty);
  player.WallDR := isWallAt(player.rtx,player.dty);
  
  if player.WallDL or player.WallDR then begin
    // hit tile below
    // set y to tile
    player.yPos := toFixedPoint(player.ty*tileH);
    // set velocity to 0
    player.yVel  := 0;
    player.inAir := False;
  end
  else
  begin
    player.inAir := True;
  end;
end;  

procedure doLeftRightCollisions;
begin
  // check left/right
  getPlayerCorners(player.xVel,0);
  
  player.WallUL := isWallAt(player.ltx,player.uty);
  player.WallUR := isWallAt(player.rtx,player.uty);
  player.WallDL := isWallAt(player.ltx,player.dty);
  player.WallDR := isWallAt(player.rtx,player.dty);
  
  if player.WallUL or player.WallDL or player.WallUR or player.WallDR then begin
    // hit tile left/right
    // set x to centre tile
    player.xPos := toFixedPoint(player.tx*tileW);
    // set velocity to 0
    player.xVel := 0;
  end
  else
    // can move this direction
    player.xPos := player.xPos + player.xVel;
end;

procedure doTileTypeChecks;
var
  playerTile : Byte;
begin
  playerTile := getTileAt(player.tx,player.ty);
  
  if player.onLadder then begin
    if playerTile = LADDER_CHAR then begin
      // still on ladder so climb up/down

      player.yVel := 0;

      if player.movingUp then
        player.yVel := CLIMP_UP_VEL
      else
      if player.movingDown then
        player.yVel := CLIMB_DOWN_VEL;

{      if not(player.movingLeft or player.movingRight) then begin
        // set x to centre tile
        player.xPos := toFixedPoint(player.tx*tileW);
        // set velocity to 0
        player.xVel := 0;
      end;}
    end
    else
    if player.movingLeft or player.movingRight then begin
      // has left ladder on left or right side
      player.onLadder := False;
      player.inAir    := True;
    end
    else
    if player.movingUp then begin
      // player has left top of ladder
      player.onLadder := False;
      player.yPos := toFixedPoint(player.ty*tileH);
      // set velocity to 0
//      player.yVel := 0;
    end;
  end
  else
  if player.movingUp or player.movingDown then begin
    if playerTile = LADDER_CHAR then begin
    // started climbing on ladder
      player.onLadder := true;

      // set x to centre tile
      player.xPos := toFixedPoint(player.tx*tileW);
      // set velocity to 0
      player.xVel := 0;
    end;
  end;
end;
  
procedure doCollisions;
begin
  doUpDownCollisions;
  doLeftRightCollisions;
  doTileTypeChecks;
end;

procedure getPlayerInput(input : Byte);
begin
  if input = 2 then
    read_joy2()
  else
    read_joy1();
    
  player.movingLeft  := false;
  player.movingRight := false;
  player.movingUp    := False;
  player.movingDown  := false;
  
  player.xVel := 0;
    
  if      input_dx = JOY_MINUS   then player.movingLeft  := true
  else if input_dx = JOY_PLUS    then player.movingRight := true;
  
  if      (input_dy = JOY_MINUS) then player.movingUp    := true
  else if (input_dy = JOY_PLUS)  then player.movingDown  := true;
  
  if      player.movingLeft  then player.xVel := PLAYER_LEFT_VEL
  else if player.movingRight then player.xVel := PLAYER_RIGHT_VEL;
  
  if player.movingUp and (player.onLadder = false) and (player.inAir = false) then begin
    player.yVel      := PLAYER_JUMP_VEL;
    player.inAir     := True;
  end;
end;

procedure mainIRQ; interrupt;
begin
  getPlayerInput(2);
    
  vic.raster := IRQ_MAIN;
    
  copyActorsToHardware;
  
  sync := true;
end;
  
procedure waitForSync; assembler;
asm
!:
  lda sync
  beq !-
  lda #$00
  sta sync
end;

procedure main;
begin
  sync := false;
  
  setupRasterIRQ(@mainIRQ,IRQ_MAIN);

  while true do begin
    waitForSync;
    
    getPlayerInput(2);
    doCollisions;

    actor_setX(SPRITE_PLAYER1,player.xPos.hiword);
    actor_setY(SPRITE_PLAYER1,player.yPos.hiword);
  end;  
end;
  
procedure init;
var
  i : Byte;
  c : Word;
  data : array[0..7] of Byte;
begin
  vic_clearScreen(SCREEN0_ADDRESS,32);

  actors_init;

  actor_enabled[SPRITE_PLAYER1] := 1;
  actor_color[SPRITE_PLAYER1]   := white;
  actor_frame[SPRITE_PLAYER1]   := 13;

  player.xPos := 256*8*26;
  player.yPos := 256*8*12;
  player.xVel := 0;
  player.yVel := 0;
  player.inAir   := false;
  player.onLadder := False;
  
  actor_setX(SPRITE_PLAYER1,player.xPos.hiword);
  actor_setY(SPRITE_PLAYER1,player.yPos.hiword);
  
  for i := 0 to 63 do
  // clear sprite
    Poke(832+i,0); 

  // get '@' data from ROM
  getCharDataFromROM(@data,0);

  for i := 0 to 7 do
  // set sprite to an '@'
    Poke(832+(i*3),data[i]);
    
  // copy data to the screen
  c := 0;
  while c < 1000 do begin 
    i := screenData[c];
    
    screen[c] := screenData[c];
    
    if i = LADDER_CHAR then
      color[c] := yellow
    else
      color[c] := cyan;
      
    Inc(c);
  end;

  drawTextAt(1,0,'  = platform');
  drawTextAt(0,2,'  = ladder');
  setTileAt(0,0,160,cyan);  
  setTileAt(0,2,$23,yellow);  
end;
 
begin
  init;
  main;
end.