program fullColorScrollWithFloorCeiling;

uses
  c64_vic,c64_hardware,c64_irq,random;
  
const
  SCREEN0_ADDRESS     = 1024;
  SCREEN1_ADDRESS     = $3000;
  
  SCREEN_WIDTH        = 40;
  SCREEN_HEIGHT       = 25;
  
  ROWS_PER_COPY       = SCREEN_HEIGHT div 2;

  TOP_SECTION_SIZE    = 6;
  BOTTOM_SECTION_SIZE = TOP_SECTION_SIZE;
  MIDDLE_SECTION_SIZE = 25-TOP_SECTION_SIZE-BOTTOM_SECTION_SIZE;

  raster_top_line    = 50;
  raster_bottom_line = 251-32;
  raster_vblank_line = 251;

var
  i         : Byte;
  screen    : Byte absolute SCREEN0_ADDRESS;
  xscroll   : Byte;
  frame     : Byte;
  
  //
  ceil_row  : Byte;
  floor_row : Byte;
  
  //copyRowsLeft variables
  srcrow  : Pointer;
  dstrow  : Pointer;
  numrows : Byte;
  //------------------------

macro stabilize(x); assembler;
asm
  ldx #x
!:
burn:
  dex
  bne burn
end;

macro copyScreen(srcScreen,dstScreen); assembler;
asm
// copy srcScreen to dstScreen
  ldx #0
!:
  lda srcScreen,x
  sta dstScreen,x
  lda srcScreen + 250,x
  sta dstScreen + 250,x
  lda srcScreen + 500,x
  sta dstScreen + 500,x
  lda srcScreen + 750,x
  sta dstScreen + 750,x
  inx
  cpx #250
  bne !-
end;

procedure fillScreen(a : Byte); assembler;
asm
  ldx #0
!:
  sta screen,x
  sta screen + 250,x
  sta screen + 500,x
  sta screen + 750,x
  inx
  cpx #250
  bne !-
end;

procedure copyRowsLeft; assembler;
asm
// Copies numrows rows of data from srcrow
// to dstrow, shifting the characters in each row one spot 
// to the left each time.

  inc srcrow      // source address = dst address + 1 for <- left

  ldx numrows

  ldy #0

video_ram_copy_line:
  // loop unrolling ftw? saves about 24 scan lines of time?
  lda (srcrow),y
  sta (dstrow),y
  iny
  lda (srcrow),y
  sta (dstrow),y
  iny
  lda (srcrow),y
  sta (dstrow),y
  iny
  lda (srcrow),y
  sta (dstrow),y
  iny
  lda (srcrow),y
  sta (dstrow),y
  iny
  lda (srcrow),y
  sta (dstrow),y
  iny
  lda (srcrow),y
  sta (dstrow),y
  iny
  lda (srcrow),y
  sta (dstrow),y
  iny
  lda (srcrow),y
  sta (dstrow),y
  iny
  
  cpy #36
  bne video_ram_copy_line
  
  lda (srcrow),y          // these next 3 are the remainder
  sta (dstrow),y
  iny
  lda (srcrow),y
  sta (dstrow),y
  iny
  lda (srcrow),y
  sta (dstrow),y
  
  dex
  beq video_ram_copy_done

  // reset column
  ldy #0

  // update srcrow and dstrow.
  clc
  lda srcrow
  adc #40
  sta srcrow
  lda srcrow+1
  adc #0
  sta srcrow+1
  
  clc
  lda dstrow
  adc #40
  sta dstrow
  lda dstrow+1
  adc #0
  sta dstrow+1

  // copy another row
  jmp video_ram_copy_line

video_ram_copy_done:
end;

procedure initScreen;
begin  
  vic_38_columns;

  randInit(100);
  
  vic_setScreenMemory(SCREEN0_ADDRESS);
  // 0 = screen 0, 1 = screen 1
  frame := 0;
  
  copyScreen(SCREEN0_ADDRESS,SCREEN1_ADDRESS);
  
  xscroll := 7;
end;

procedure fillColumn_Screen0; assembler;
asm
  .for (var y = 0; y < 25; y++) {
    lda #y
    sta [SCREEN0_ADDRESS + 40*y + 39]
  }
end;

procedure fillColumn_Screen1; assembler;
asm
  .for (var y = 0; y < 25; y++) {
    lda #y
    sta [SCREEN1_ADDRESS + 40*y + 39]
  }
end;

procedure fillRightHandColumn;
begin
  if frame = 0 then
    fillColumn_Screen1()
  else
    fillColumn_Screen0();     

  asm
    jsr random
    .for (var y = 0; y < 25; y++) {
      sta [$d800 + 40*y + 39]
    }
  end;
end;

procedure shiftScreenRam_UpperHalf;
begin
  if frame = 0 then begin
    srcrow  := SCREEN0_ADDRESS;
    dstrow  := SCREEN1_ADDRESS;
    numrows := TOP_SECTION_SIZE; 
    copyRowsLeft;
  end
  else begin
    srcrow  := SCREEN1_ADDRESS;
    dstrow  := SCREEN0_ADDRESS;
    numrows := TOP_SECTION_SIZE; 
    copyRowsLeft;
  end;
end;

procedure shiftScreenRam_LowerHalf;
begin
  if frame = 0 then begin
    srcrow  := SCREEN0_ADDRESS + 40 * (TOP_SECTION_SIZE+MIDDLE_SECTION_SIZE);
    dstrow  := SCREEN1_ADDRESS + 40 * (TOP_SECTION_SIZE+MIDDLE_SECTION_SIZE);
    numrows := BOTTOM_SECTION_SIZE; 
    copyRowsLeft;
  end
  else begin
    srcrow  := SCREEN1_ADDRESS + 40 * (TOP_SECTION_SIZE+MIDDLE_SECTION_SIZE);
    dstrow  := SCREEN0_ADDRESS + 40 * (TOP_SECTION_SIZE+MIDDLE_SECTION_SIZE);
    numrows := BOTTOM_SECTION_SIZE; 
    copyRowsLeft;
  end;
end;

procedure scrollColorRam_UpperHalf;
begin
  srcrow  := $d800;
  dstrow  := $d800;
  numrows := TOP_SECTION_SIZE; 
  copyRowsLeft;
end;

procedure scrollColorRam_LowerHalf;
begin
  srcrow  := $d800 + 40 * (TOP_SECTION_SIZE+MIDDLE_SECTION_SIZE);
  dstrow  := $d800 + 40 * (TOP_SECTION_SIZE+MIDDLE_SECTION_SIZE);
  numrows := BOTTOM_SECTION_SIZE; 
  copyRowsLeft;
end;

procedure swapScreens;
begin
  if frame = 0 then
    vic_setScreenMemory(SCREEN1_ADDRESS)
  else
    vic_setScreenMemory(SCREEN0_ADDRESS);
    
  frame := frame xor 1;
end;

procedure updateAndSwapScreens;
begin
  swapScreens();

  scrollColorRam_LowerHalf;

  fillRightHandColumn;
end;

procedure raster_vblank; Interrupt; forward;
procedure raster_bottom; Interrupt; forward;
  
procedure raster_top; Interrupt;
begin
  if xscroll = 0 then
    scrollColorRam_UpperHalf;
  
  vic.raster := raster_bottom_line;
  IRQ_vector := @raster_bottom;
end;

procedure raster_bottom; Interrupt;
begin
  vic.raster := raster_vblank_line;
  IRQ_vector := @raster_vblank;
end;

procedure raster_vblank; Interrupt;
begin
  xscroll := xscroll - 1;
  
  if xscroll = -1 then begin
    xscroll := 7;

    // set horiz scroll to current value
    vic_set_scrollx(xscroll);

    updateAndSwapScreens();
  end
  else begin
    // set horiz scroll to current value
    vic_set_scrollx(xscroll);
    
    if (xscroll = 4) then
      shiftScreenRam_UpperHalf()
    else
    if (xscroll = 2) then
      shiftScreenRam_LowerHalf();
  end;
  
  vic.raster := raster_top_line;
  IRQ_vector := @raster_top;
end;

begin
  disable_irq;
  
  initScreen;
  
  // turn off ROM
  poke($01,$35);
  
  setupRasterIRQ(@raster_top,raster_top_line);
  
  enable_irq;
  
  while true do;
end.