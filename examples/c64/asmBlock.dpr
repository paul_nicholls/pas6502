program asmBlock;
const
  a = 1;
var
  b : Byte;
begin
  b := 2;
  asm
    lda #a
    sta 1024
    lda b
    sta 1024+1
  end;
end.