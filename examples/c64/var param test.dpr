program varParamTest;

procedure v1(vx,vy : Pointer);
begin
  vx^ := 3;
  vy^ := 5;
end;

var
  a,b : Byte;
begin
  v1(@a,@b);
  writeLn(a,',',b);
end.