lda {m2}
sta divmod_s16.dividend
lda {m2} + 1
sta divmod_s16.dividend+1

lda #{c1}
jsr _signExtendAtoAX
stx divmod_s16.divisor
sta divmod_s16.divisor+1

lda #$ff
sta divmod_s16.isMod

jsr divmod_s16._main_

stx {m1}
sta {m1} + 1
