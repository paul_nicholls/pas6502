lda #({c1} >> 16)
cmp {m2} + 2
bne !isFalse+
lda #>{c1}
cmp {m2} + 1
bne !isFalse+
lda #<{c1}
cmp {m2} + 0
bne !isFalse+
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
sta {m1}