lda {m2}
sta TEMP_PNTR
lda {m2}+1
sta TEMP_PNTR+1
ldx #{c1}
!:
lda TEMP_PNTR
asl
ror TEMP_PNTR+1
ror TEMP_PNTR
dex
bne !-
lda TEMP_PNTR
sta {m1}
