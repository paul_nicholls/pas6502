clc
lda {m2}
adc #<{a1}
sta {m1}
lda {m2}+1
adc #>{a1}
sta {m1}+1