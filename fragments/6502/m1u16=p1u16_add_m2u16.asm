clc
lda {p1}
adc {m2}
sta {m1}
lda {p1}+1
adc {m2}+1
sta {m1}+1