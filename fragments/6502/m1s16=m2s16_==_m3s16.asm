lda {m3}
cmp {m2}
bne !isFalse+
lda {m2}+1
cmp {m3}+1
bne !isFalse+
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
sta {m1}
sta {m1}+1