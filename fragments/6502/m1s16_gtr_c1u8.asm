sec
lda {m1} + 0
sbc #{c1}
tax
lda {m1} + 1
sbc #0
// test for <
bmi !isFalse+ // {m1} < {c1}
// test for =
orax
beq !isFalse+ // {m1} == {c1}
lda #$ff
jmp !+
!isFalse:
lda #$00
!: