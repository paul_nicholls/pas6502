lda {m2} + 1; cmp #>{c1}; bne !isTrue+
lda {m2} + 0; cmp #<{c1}; bne !isTrue+
!isFalse:
lda #$00
jmp !+
!isTrue:
lda #$ff
!:
sta {m1} + 0; sta {m1} + 1