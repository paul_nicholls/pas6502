lda {m2}+1
bne !isFalse+
lda {m2}
cmp #{c1}
bcc !isTrue+
!isFalse:
lda #$00
jmp !+
!isTrue:
lda #$ff
!:
sta {m1}