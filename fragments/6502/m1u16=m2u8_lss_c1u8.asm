lda {m2}
cmp #{c1} // <
lda #$ff
adc #0 // if C = 1 (false, $ff -> $00)
//lda {m2}
//cmp #{c1}
//bcc !isTrue+ // <
//lda #$00
//jmp !+
//!isTrue:
//lda #$ff
//!:
sta {m1}
sta {m1} + 1