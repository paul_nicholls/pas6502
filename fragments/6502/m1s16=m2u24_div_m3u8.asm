lda {m2}
sta divmod_u24_16.dividend
lda {m2} + 1
sta divmod_u24_16.dividend+1
lda {m2} + 2
sta divmod_u24_16.dividend+2

lda {m3}
sta divmod_u24_16.divisor
lda {m3}+1
sta divmod_u24_16.divisor+1

lda #$00
sta divmod_u24_16.isMod
jsr divmod_u24_16._main_
stx {m1}
sta {m1} + 1
