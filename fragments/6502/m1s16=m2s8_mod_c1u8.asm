lda {m2}
tay // save original signed version to check later
bpl !+
eor #$ff
clc
adc #1
!:
ldx #{c1}
jsr _mod8_8u._main_
cpy #128
bcc !+ // original not negative so don't negate value

!:
sta {m1}
lda #0
sta {m1} + 1