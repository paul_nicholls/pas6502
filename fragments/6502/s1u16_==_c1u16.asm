ldy _sp
lda {s1},y
cmp #{c1}
bne !isFalse+
lda {s1}+1,y
cmp #>{c1}
bne !isFalse+
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
