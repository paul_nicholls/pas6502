clc
lda #<{m2}
adc {m3}
sta TEMP_PNTR
lda #>{m2}
adc {m3}+1
sta TEMP_PNTR+1
ldy #0
lda (TEMP_PNTR),y
sta {m1}
lda #0
sta {m1}+1
