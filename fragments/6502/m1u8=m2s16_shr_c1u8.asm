lda {m2}
sta _TEMP_
lda {m1}+1
sta _TEMP_+1
.for(var i = 1; i <= {c1}; i++) {
lda _TEMP_ + 1      // load the msb
asl                 // copy the sign bit into c
ror _TEMP_ + 1      // and back into the msb
ror _TEMP_ + 0      // rotate the lsb as normal
}
lda _TEMP_
sta {m1}