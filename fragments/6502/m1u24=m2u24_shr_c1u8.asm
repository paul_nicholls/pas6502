lda {m2} + 0
sta {m1} + 0
lda {m2} + 1
sta {m1} + 1
lda {m2} + 2
sta {m1} + 2
.for(var i = 1; i <= {c1}; i++) {
  lsr {m1} + 2
  ror {m1} + 1
  ror {m1} + 0
}