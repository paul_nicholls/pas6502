  lda {m1} + 1
  cmp {m2} + 1
  bne !+
  lda {m1} + 0
  cmp {m2} + 0
!:
  bne !+        // is >
  bcc !isFalse+ // is <
  beq !isFalse+ // is =

!isTrue:
  jmp !+
!isFalse:
  jmp {l1}
!:
