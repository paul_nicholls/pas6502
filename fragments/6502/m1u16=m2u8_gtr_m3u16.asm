lda {m3} + 1
bne !isFalse+
lda {m2}
cmp {m3} + 0
bcc !isFalse+
beq !isFalse+
// true
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
sta {m1} + 0
sta {m1} + 1
