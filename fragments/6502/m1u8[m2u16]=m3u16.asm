clc
lda #<{m1}; adc {m2} + 0; sta TEMP_PNTR + 0
lda #>{m1}; adc {m2} + 1; sta TEMP_PNTR + 1
ldy #0; lda {m3}; sta (TEMP_PNTR),y