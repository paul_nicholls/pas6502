clc
lda {p1}
adc {m1}
sta TEMP_PNTR
lda {p1}+1
adc {m1}+1
sta TEMP_PNTR+1
ldy #0
lda {m2}
sta ({p1}),y
