lda {m2}
ldx #{c1}
!:
cmp #$80  //copy bit 7 to carry
ror       //shift right as before
bpl !+    //if positive number then skip
adc #$00  //else round up
!:
dex
bne !--
sta {m2}