ldx #$ff
lda {m2}
cmp {m3}
bcc !isTrue+ // <
beq !isTrue+ // =
inx
!isTrue:
txa
!:
sta {m1}

//lda {m2}
//cmp {m3}
//bcc !isTrue+ // <
//beq !isTrue+ // =
//lda #$00
//jmp !+
//!isTrue:
//lda #$ff
//!:
//sta {m1}