lda {m2} + 0; sta _TEMP_ + 0
lda {m2} + 1; sta _TEMP_ + 1

.for(var i = 1; i <= {c1}; i++) {
  lsr _TEMP_ + 1; ror _TEMP_ + 0
}
lda _TEMP_ + 0; sta {m1}