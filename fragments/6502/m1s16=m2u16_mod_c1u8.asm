lda #{c1}
sta divmod_u16.divisor
lda #0
sta divmod_u16.divisor+1
lda {m2}
sta divmod_u16.dividend
lda {m2} + 1
sta divmod_u16.dividend+1
lda #$ff
sta divmod_u16.isMod
jsr divmod_u16._main_
stx {m1}
sta {m1} + 1
