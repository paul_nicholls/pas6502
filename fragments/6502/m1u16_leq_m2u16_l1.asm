lda {m1} + 1
cmp {m2} + 1
bne !+
lda {m1} + 0
cmp {m2} + 0
!:

bcc !isTrue+  // is <
beq !isTrue+  // is = (false)
jmp {l1}
//bne !isFalse+ // is > (false)

!isTrue:
