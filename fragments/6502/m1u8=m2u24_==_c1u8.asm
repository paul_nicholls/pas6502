lda {m2} + 2
bne !isFalse+
lda {m2} + 1
bne !isFalse+
lda {m2} + 0
cmp #<{c1}
bne !isFalse+
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
sta {m1}