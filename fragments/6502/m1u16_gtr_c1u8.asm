  lda {m1} + 1
  bne !isTrue+
  lda {m1} + 0
  cmp #{c1}
  beq !isFalse+
  bcc !isFalse+
!isTrue:
  lda #$ff
  jmp !+
!isFalse:
  lda #$00
!: