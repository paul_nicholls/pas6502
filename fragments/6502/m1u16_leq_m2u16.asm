lda {m1} + 1
cmp {m2} + 1
bne !+
lda {m1} + 0
cmp {m2} + 0
!:

bcc !isTrue+  // is <
bne !isFalse+ // is > (false)
beq !isTrue+  // is = (false)

!isTrue:
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
