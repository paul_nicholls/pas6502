lda #{c1}
jsr _signExtendAtoAX
cmp {m2} + 0
bne !isFalse+
cpx {m2} + 1
bne !isFalse+
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
sta {m1} + 0
sta {m1} + 1