  lda {m1} + 1
  cmp #>{c1}
  bne !+
  lda {m1} + 0
  cmp #{c1}
!:
  bne !+        // is >
  bcc !isFalse+ // is <
  beq !isFalse+ // is =

!isTrue:
  jmp !+
!isFalse:
  jmp {l1}
!:
