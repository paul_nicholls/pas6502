lda {m2} + 1
cmp {m3} + 1
bne !+
lda {m2} + 0
cmp {m3} + 0
!:

bcc !isFalse+ // is <
bne !isTrue+  // is > (false)
beq !isTrue+ // is = (false)

!isTrue:
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
sta {m1}