lda {m1} + 1
cmp #>{c1}
bne !+
lda {m1} + 0
cmp #<{c1}
!:

bcc !isTrue+  // is <
bne !isFalse+ // is > (false)
beq !isFalse+ // is = (false)
!isTrue:
lda #$ff
jmp !+
!isFalse:
lda #$00
!: