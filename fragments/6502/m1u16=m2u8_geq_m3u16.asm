lda {m1} + 1
cmp #0
bne !+
lda {m1} + 0
cmp {m3} + 0
!:

bcc !isFalse+ // is <
bne !isTrue+  // is > (false)
beq !isTrue+  // is = (false)
!isTrue:
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
sta {m1} + 0; sta {m1} + 1