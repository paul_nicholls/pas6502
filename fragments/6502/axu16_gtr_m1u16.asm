stx TEMP_PNTR
sta TEMP_PNTR + 1
sec
lda TEMP_PNTR + 0
sbc {m1}
tax
lda TEMP_PNTR + 1
sbc {m1}+1
// test for <
bmi !isFalse+ // {TEMP_PNTR} < {m1}
// test for =
orax
beq !isFalse+ // {TEMP_PNTR} == {m1}
lda #$ff
jmp !+
!isFalse:
lda #$00
!: