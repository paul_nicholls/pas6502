pha
ldx #{c1}; jsr mul_8x8
lda mul_product_lo; sta {m1}
lda mul_product_hi; sta {m1}+1
pla
ldx #>{c1}; jsr mul_8x8
clc
lda {m1}+1
adc mul_product_lo
sta {m1}+1
lda #0
adc mul_product_hi
sta {m1}+2