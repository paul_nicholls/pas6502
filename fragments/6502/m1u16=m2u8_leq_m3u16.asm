cmp {m3}+1
bne !isTrue+

lda {m2}
cmp {m3}
gtr !isFalse+
!isTrue:
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
sta {m1} + 0
sta {m1} + 1