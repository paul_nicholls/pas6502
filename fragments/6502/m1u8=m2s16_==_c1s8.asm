lda #{c1}
jsr _signExtendAtoAX
tay
lda {m2}+1
cmpy // y contains hi byte
bne !isFalse+
lda {m2}
cmpx // x contains lo byte
bne !isFalse+
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
sta {m1}