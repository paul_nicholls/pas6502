lda {m2}
cmp #{c1}

bcc !isTrue+  // is <
bne !isFalse+ // is > (false)
beq !isFalse+ // is = (false)

!isTrue:
lda #$ff
jmp !+
!isFalse:
lda #$00
!:
sta {m1} + 0
sta {m1} + 1