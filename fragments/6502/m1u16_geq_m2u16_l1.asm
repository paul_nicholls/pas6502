lda {m1} + 1
cmp {m2} + 1
bne !+
lda {m1} + 0
cmp {m2} + 0
!:

bcc !isFalse+ // is <
bne !+  // is >
beq !+  // is =
!isFalse:
jmp {l1}
!:
