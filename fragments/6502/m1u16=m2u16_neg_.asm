clc
lda {m2} + 0
eor #$ff
adc #1
sta {m1} + 0

lda {m2} + 1
eor #$ff
adc #0
sta {m1} + 1
