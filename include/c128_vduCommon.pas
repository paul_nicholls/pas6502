unit c128_vdcCommon;
//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------
//
// adapted from:
// https://gitlab.com/aaron-baugher/6502-assembly-language-programming/-/blob/master/vdc80lib.a?ref_type=heads

const
  vdc80ram  : array of Byte = (0,0);
  vdc80attr : array of Byte = (0,8);

procedure vdc_regWrite(x : Byte; a : Byte); assembler; volatile;
// x - register to write to , a - value
asm
  stx $d600
!:
  bit $d600
  bpl !-
  sta $d601
end;

function vdc_regRead(x : Byte) : Byte; assembler; volatile;
// x - register to read from, a return value
asm
  stx $d600
!:
  bit $d600
  bpl !-
  lda $d601
end;

procedure vdc_writeByte(a : Byte); assembler; volatile;
// write byte to current ram pointer
asm
  ldx #31
  jsr vdc_regWrite
end;

procedure vdc_setRamPointer(ya : Word); assembler; volatile;
// set VDC RAM pointer
asm
  ldx #19
  jsr vdc_regWrite
  tya
  dex
  jsr vdc_regWrite
end;

