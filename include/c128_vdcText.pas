unit c128_vdcText;
//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------
//
// adapted from:
// https://gitlab.com/aaron-baugher/6502-assembly-language-programming/-/blob/master/vdc80lib.a?ref_type=heads

uses
  c128_vdcCommon;
  
procedure vdc_setTextMode; assembler;
asm
  jsr vdcInitColors
  ldx #25
  jsr vdc_regRead
  and #$7f
  jsr vdc_regWrite         // set text mode
  ldx #12
  jsr vdc_regRead
  sta vdc80ram+1
  inx
  jsr vdc_regRead
  sta vdc80ram            // save screen RAM address
  ldx #20
  jsr vdc_regRead
  sta vdc80attr+1
  inx
  jsr vdc_regRead
  sta vdc80attr           // save attribute RAM address
end;

procedure vdc_printStr(txt : Pointer); assembler;
// prints string at pointer txt (length at txt^[0]
asm
  // load string length and store
  ldy #0
  lda (txt),y
  sta count
!:
  iny
  lda (txt),y
  jsr vdc_writeByte
  dec count
  bne !-
  rts
count: 
  .byte 0
end;        
{
procedure vdc_setScreenRamPntr00; assembler; volatile;
// move ram pointer to 0,0 in video RAM
asm
  lda vdc80ram     
  ldx #19
  jsr vdc_regWrite
  dex
  lda vdc80ram+1
  jsr vdc_regWrite
end;

procedure vdc_setAttribRamPntr00; assembler; volatile;
// move ram pointer to 0,0 in attribute RAM
asm
  lda vdc80attr
  ldx #19
  jsr vdc_regWrite
  dex
  lda vdc80attr+1
  jsr vdc_regWrite
end;

procedure vdc_setScreenAttribPntrXY(x,y : Byte); assembler;
// position the RAM pointer at .X/.Y coordinates in attrib memory
asm
  lda #0
  sta TEMP_PNTR+1
  sty TEMP_PNTR
  asl TEMP_PNTR
  asl TEMP_PNTR               // .Y times 4
  tya
  clc
  adc TEMP_PNTR
  sta TEMP_PNTR               // .Y times 5
  ldy #4
!:
  asl TEMP_PNTR
  rol TEMP_PNTR+1
  dey
  bne !-                   // .Y times 80 in TEMP_PNTR/TEMP_PNTR+1
  txa
  clc
  adc TEMP_PNTR
  sta TEMP_PNTR
  bcc !+
  inc TEMP_PNTR+1              // added .X offset across screen
!:
  lda vdc80attr
  clc
  adc TEMP_PNTR
  sta TEMP_PNTR
  lda vdc80attr+1
  adc TEMP_PNTR+1
  sta TEMP_PNTR+1              // added offset for start of attribute RAM
 	ldx #18
  lda TEMP_PNTR+1
  jsr vdc_regWrite
  inx
  lda TEMP_PNTR
  jsr vdc_regWrite
end;

procedure vdc_fillScreen(a : Byte); assembler; volatile;
asm
  pha
  jsr vdc_setScreenRamPntr00
  pla
  jsr vdc_writeByte
  lda #249
  jsr vdc_repeatByte
  lda #250
  ldy #7
!:       
  jsr vdc_repeatByte
  dey
  bne !-
end;
}