unit c128_vdcCommon;
//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------
//
// adapted from:
// https://gitlab.com/aaron-baugher/6502-assembly-language-programming/-/blob/master/vdc80lib.a?ref_type=heads

const
  vdc80ram  : array[0..1] of Byte = (0,0);
  vdc80attr : array[0..1] of Byte = (0,8);

type
  TVDC_color = record
    black        : byte;
    dark_gray    : byte;
    dark_blue    : byte;
    light_blue   : byte;
    dark_green   : byte;
    light_green  : byte;
    dark_cyan    : byte;
    light_cyan   : byte;
    dark_red     : byte;
    light_red    : byte;
    dark_purple  : byte;
    light_purple : byte;
    dark_yellow  : byte;
    light_yellow : byte;
    light_grey   : byte;
    white        : byte;
  end;

var
  vdc_color : TVDC_color;

procedure vdcInitColors; assembler; volatile;
asm
  lda #0; sta vdc_color.black
  lda #1; sta vdc_color.dark_gray
  lda #2; sta vdc_color.dark_blue
  lda #3; sta vdc_color.light_blue
  lda #4; sta vdc_color.dark_green
  lda #5; sta vdc_color.light_green
  lda #6; sta vdc_color.dark_cyan
  lda #7; sta vdc_color.light_cyan
  lda #8; sta vdc_color.dark_red
  lda #9; sta vdc_color.light_red
  lda #$a; sta vdc_color.dark_purple
  lda #$b; sta vdc_color.light_purple
  lda #$c; sta vdc_color.dark_yellow
  lda #$d; sta vdc_color.light_yellow
  lda #$e; sta vdc_color.light_grey
  lda #$f; sta vdc_color.white
end;

procedure vdc_regWrite(x,a : Byte); assembler; volatile;
asm
// x - register to write to , a - value
  stx $d600
!:
  bit $d600
  bpl !-
  sta $d601
end;

function  vdc_regRead(x : Byte) : Byte; assembler; volatile;
asm
// x - register to read from, a return value
  stx $d600
!:
  bit $d600
  bpl !-
  lda $d601
end;

// pass the number of times in A
procedure repeat80byte; assembler;
asm
  pha
  ldx #24
  jsr vdc_regRead
  and #$7f
  jsr vdc_regWrite
  pla
  ldx #30
  jsr vdc_regWrite
end;

procedure vdc_setScreenRamPntr00; assembler; volatile;
// move ram pointer to 0/0 in video RAM
asm
  lda vdc80ram     
  ldx #19
  jsr vdc_regWrite
  dex
  lda vdc80ram+1
  jsr vdc_regWrite
end;

procedure vdc_setAttribRamPntr00; assembler; volatile;   
// move ram pointer to 0/0 in attribute RAM
asm
  lda vdc80attr
  ldx #19
  jsr vdc_regWrite
  dex
  lda vdc80attr+1
  jsr vdc_regWrite
end;

procedure vdc_setScreenRamPntrXY(x,y : Byte); assembler;
// position the RAM pointer at .X/.Y coordinates in screen memory
asm
  lda #0
  sta TEMP_PNTR+1
  sty TEMP_PNTR
  asl TEMP_PNTR
  asl TEMP_PNTR               // .Y times 4
  tya
  clc
  adc TEMP_PNTR
  sta TEMP_PNTR               // .Y times 5
  ldy #4
!:
  asl TEMP_PNTR
  rol TEMP_PNTR+1
  dey
  bne !-                   // .Y times 80 in TEMP_PNTR/TEMP_PNTR+1
  txa
  clc
  adc TEMP_PNTR
  sta TEMP_PNTR
  bcc !+
  inc TEMP_PNTR+1              // added .X offset across screen
!:
  lda vdc80ram
  clc
  adc TEMP_PNTR
  sta TEMP_PNTR
  lda vdc80ram+1
  adc TEMP_PNTR+1
  sta TEMP_PNTR+1              // added offset for start of screen RAM
  ldx #18
  lda TEMP_PNTR+1
  jsr vdc_regWrite
  inx
  lda TEMP_PNTR
  jsr vdc_regWrite
end;

procedure vdc_fillScreen(a : Byte); assembler; volatile;
asm
  pha
  jsr vdc_setScreenRamPntr00
pla
  jsr vdc_writeByte
  lda #249
  jsr repeat80byte
  lda #250
  ldy #7
!:       
  jsr repeat80byte
  dey
  bne !-
end;

procedure vdc_writeByte(a : Byte); assembler; volatile;
// write byte to current ram pointer
asm
  ldx #31
  jsr vdc_regWrite
end;

function vdc_readByte: Byte; assembler; volatile;
// write byte to current ram pointer
asm
  ldx #31
  jsr vdc_regRead
end;

// pass the number of times in A
procedure vdc_repeatByte(a : Byte); assembler;
// repeat last written byte (count in A)
asm
  pha
  ldx #24
  jsr vdc_regRead
  and #$7f
  jsr vdc_regWrite
  pla
  ldx #30
  jsr vdc_regWrite
end;

procedure vdc_setRamPointer(ya : Word); assembler; volatile;
// set VDC RAM pointer
asm
  ldx #19
  jsr vdc_regWrite
  tya
  dex
  jsr vdc_regWrite
end;
