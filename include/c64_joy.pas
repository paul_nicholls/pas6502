//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    this file was translated (and modified) from: 
//    https://github.com/KarolS/millfork
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit c64_joy;

uses
  joy,c64_cia;
  
procedure read_joyInputs(value : byte);
begin
  if      (value and 1) = 0 then input_dy := JOY_MINUS 
  else if (value and 2) = 0 then input_dy := JOY_PLUS;

  if      (value and 4) = 0 then input_dx := JOY_MINUS 
  else if (value and 8) = 0 then input_dx := JOY_PLUS; 
  
  if (value and 16) = 0     then input_btn := JOY_DOWN; 
  
  input_btnPressed  := ((input_lastBtn = JOY_UP)   and  (input_btn = JOY_DOWN));
  input_btnReleased := ((input_lastBtn = JOY_DOWN) and  (input_btn = JOY_UP));
  
  input_pushedLeft  := ((input_lastDx = 0)  and  (input_dx = JOY_MINUS));
  input_pushedRight := ((input_lastDx = 0)  and  (input_dx = JOY_PLUS));
  input_pushedUp    := ((input_lastDy = 0)  and  (input_dy = JOY_MINUS));
  input_pushedDown  := ((input_lastDy = 0)  and  (input_dy = JOY_PLUS));
end;

procedure read_joy2;
var
  value : byte;
begin
  reset_joy();

  asm
  // disable keyboard
  lda #224
  sta 56322
  end;

  cia1_ddra := 0;
  value := cia1_pra;
  asm
  // re-enable keyboard
  lda #255
  sta 56322
  end;
  read_joyInputs(value);
end;

procedure read_joy1;
var
  value : byte;
begin
  reset_joy();
  cia1_ddrb := 0;

  asm
  // disable keyboard
  lda #224
  sta 56322
  end;

  value := cia1_prb;
  asm
  // re-enable keyboard
  lda #255
  sta 56322
  end;
  read_joyInputs(value);
end;

procedure read_joy1and2;
var
  value : byte;
begin
  reset_joy();
  cia1_ddrb := 0;
  asm
  // disable keyboard
  lda #224
  sta 56322
  end;

  value := cia1_pra and cia1_prb;
  asm
  // re-enable keyboard
  lda #255
  sta 56322
  end;
  read_joyInputs(value);
end;
