//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

#importonce

.const TRUE  = -1
.const FALSE = 0

.var _saveAddress
.var arg1 = $fb
.var arg2 = $fd

//----------------------------------------------------------
// Code for creating the breakpoint file sent to Vice.
//----------------------------------------------------------
.var _useBinFolderForBreakpoints = cmdLineVars.get("usebin") == "true"
.var _createDebugFiles = cmdLineVars.get("afo") == "true"
.print "File creation " + [_createDebugFiles
 ? "enabled (creating breakpoint file)"
 : "disabled (no breakpoint file created)"]
.var brkFile
.if(_createDebugFiles) {
 .if(_useBinFolderForBreakpoints)
 .eval brkFile = createFile("bin/breakpoints.txt")
 else
 .eval brkFile = createFile("breakpoints.txt")
}

.macro break() {
.if(_createDebugFiles) {
 .eval brkFile.writeln("break " + toHexString(*))
 }
}

.macro copy16(src,dst) {
  lda src + 0
  sta dst + 0
  lda src + 1
  sta dst + 1
}

.macro copy16At(src,dst) {
  // load pointer in dst and store in zero page
  lda dst + 0
  sta $fb + 0
  lda dst +1
  sta $fb + 1

  ldy #0
  // load 16-bit value in src and store at pointer location
  lda src + 0
  sta ($fb),y
  iny
  lda src + 1
  sta ($fb),y
}

.macro set16im(value,dst) {
  lda #<value
  sta dst + 0
  lda #>value
  sta dst + 1
}

.macro set16imAt(value,dst) {
  // load pointer in dst and store in zero page
  lda dst + 0
  sta $fb + 0
  lda dst +1
  sta $fb + 1

  ldy #0
  // load 16-bit value and store at pointer location
  lda #<value
  sta ($fb),y
  iny
  lda #>value
  sta ($fb),y
}

.macro copy8(src,dst) {
  lda src + 0
  sta dst + 0
}

.macro copy8At(src,dst) {
  // load pointer in dst and store in zero page
  lda dst + 0
  sta $fb + 0
  lda dst +1
  sta $fb + 1

  ldy #0
  // load 8-bit value in src and store at pointer location
  lda src + 0
  sta ($fb),y
}

.macro set8im(value,dst) {
  lda #<value
  sta dst + 0
}

.macro set8imAt(value,dst) {
  // load pointer in dst and store in zero page
  lda dst + 0
  sta $fb + 0
  lda dst +1
  sta $fb + 1

  ldy #0
  // load 8-bit value and store at pointer location
  lda #<value
  sta ($fb),y
}

.macro incMem(address) {
  inc address + 0
  bne label
  inc address + 1
label:
}

.macro decMem(address) {
  lda address + 0
  bne label
  dec address + 1
label:
  dec address + 0
}

.macro incMemAt(address) {
  lda address + 0
  sta $fb + 0
  lda address +1
  sta $fb + 1

  clc
  lda ($fb),y
  adc #1
  sta ($fb),y

  iny
  lda ($fb),y
  adc #0
  sta ($fb),y
}

.macro incMem8(address) {
  inc address
}

.macro decMem8(address) {
  dec address
}

.macro incMem8At(address) {
  lda address + 0
  sta $fb + 0
  lda address +1
  sta $fb + 1

  ldy #0
  clc
  lda ($fb),y
  adc #1
  sta ($fb),y
}

.macro decMem8At(address) {
  lda address + 0
  sta $fb + 0
  lda address +1
  sta $fb + 1

  ldy #0
  clc
  lda ($fb),y
  adc #-1
  sta ($fb),y
}

.macro div8_24bit(address) {
  :div2_24bit(address)
  :div2_24bit(address)
  :div2_24bit(address)
}

.macro div8_16bit(address) {
  ldx #3
!:
  :div2_16bit(address)
  dex
  bne !-
}

.macro div2_24bit(address) {
  lda address + 2      // load the msb
  asl                  // copy the sign bit into c
  ror address + 2      // and back into the msb
  ror address + 1      // rotate the next lsb as normal
  ror address + 0      // rotate the first lsb as normal
}

.macro div2_16bit(address) {
  lda address + 1      // load the msb
  asl                  // copy the sign bit into c
  ror address + 1      // and back into the msb
  ror address + 0      // rotate the lsb as normal
}

.macro twosComplement16bit(address) {
  sec               // ensure carry is set
  lda #0            // load constant zero
  sbc address + 0   // ... subtract the least significant byte
  sta address + 0   // ... and store the result

  lda #0            // load constant zero again
  sbc address + 1   // ... subtract the most significant byte
  sta address + 1   // ... and store the result
}

INTEGER_REGISTER_ADDRESS: .fill 32*2,0

INTEGER_STACK_LSB:   .fill 32,0
INTEGER_STACK_MSB:   .fill 32,0
INTEGER_STACK_INDEX: .byte -1

//-------------------------------
// pushIntIm
// input: value (immediate)
//-------------------------------
.macro pushIntIm(value) {
  inc INTEGER_STACK_INDEX
  ldx INTEGER_STACK_INDEX
  lda #<value
  sta INTEGER_STACK_LSB,x
  lda #>value
  sta INTEGER_STACK_MSB,x
}

.macro pushIntIm8(value) {
  inc INTEGER_STACK_INDEX
  ldx INTEGER_STACK_INDEX
  lda #<value
  sta INTEGER_STACK_LSB,x
  lda #0
  sta INTEGER_STACK_MSB,x
}

.macro pushIntMem(srcAddr) {
  inc INTEGER_STACK_INDEX
  ldx INTEGER_STACK_INDEX
  lda srcAddr + 0
  sta INTEGER_STACK_LSB,x
  lda srcAddr + 1
  sta INTEGER_STACK_MSB,x
}

.macro pushIntMem8(srcAddr) {
  inc INTEGER_STACK_INDEX
  ldx INTEGER_STACK_INDEX

  lda srcAddr + 0
  sta INTEGER_STACK_LSB,x
  lda #0
  sta INTEGER_STACK_MSB,x
}

.macro popIntMem(dstAddr) {
  ldx INTEGER_STACK_INDEX

  lda INTEGER_STACK_LSB,x
  sta dstAddr + 0
  lda INTEGER_STACK_MSB,x
  sta dstAddr + 1

  dec INTEGER_STACK_INDEX
}

.macro popIntMem8(dstAddr) {
  ldx INTEGER_STACK_INDEX

  lda INTEGER_STACK_LSB,x
  sta dstAddr + 0

  dec INTEGER_STACK_INDEX
}

//.function getIntRegAddress(regIndex) {
//  .return INTEGER_REGISTER_ADDRESS + regIndex * 2
//}

.macro setScreenAndCharsetLocation(_screen,_charset,_setVideoBank) {
/*
Address	Mask	Bank	Yields
DD00	%xxxxxx11	bank0	0000 - 3FFF
DD00	%xxxxxx10	bank1	4000 - 7FFF
DD00	%xxxxxx01	bank2	8000 - BFFF
DD00	%xxxxxx00	bank3	C000 - FFFF
*/
  .var videoBank = 0;

  .var VIDEO_BANK_0_START = $0000
  .var VIDEO_BANK_0_END   = $3FFF

  .var VIDEO_BANK_1_START = $4000
  .var VIDEO_BANK_1_END   = $7FFF

  .var VIDEO_BANK_2_START = $8000
  .var VIDEO_BANK_2_END   = $BFFF

  .var VIDEO_BANK_3_START = $C000
  .var VIDEO_BANK_3_END   = $FFFF

  .if ((_screen >= VIDEO_BANK_0_START) && (_screen < VIDEO_BANK_0_END)) .eval videoBank = 0;
  .if ((_screen >= VIDEO_BANK_1_START) && (_screen < VIDEO_BANK_1_END)) .eval videoBank = 1;
  .if ((_screen >= VIDEO_BANK_2_START) && (_screen < VIDEO_BANK_2_END)) .eval videoBank = 2;
  .if ((_screen >= VIDEO_BANK_3_START) && (_screen < VIDEO_BANK_3_END)) .eval videoBank = 3;

  .if ((_charset >= VIDEO_BANK_0_START) && (_charset < VIDEO_BANK_0_END)) .eval videoBank = 0;
  .if ((_charset >= VIDEO_BANK_1_START) && (_charset < VIDEO_BANK_1_END)) .eval videoBank = 1;
  .if ((_charset >= VIDEO_BANK_2_START) && (_charset < VIDEO_BANK_2_END)) .eval videoBank = 2;
  .if ((_charset >= VIDEO_BANK_3_START) && (_charset < VIDEO_BANK_3_END)) .eval videoBank = 3;

  .if (_setVideoBank) :setVideoBank(videoBank)

  // set the screen and character set locations
  // (relative to the current bank)
  lda #[[_screen & $3FFF] / 64] | [[_charset & $3FFF] / 1024]
  sta $D018
}

.macro setVideoBank(_VideoBank) {
/*
Address	Mask	Bank	Yields
DD00	%xxxxxx11	bank0	0000 - 3FFF
DD00	%xxxxxx10	bank1	4000 - 7FFF
DD00	%xxxxxx01	bank2	8000 - BFFF
DD00	%xxxxxx00	bank3	C000 - FFFF
*/
  lda $DD00
  and #%11111100
  ora #((_VideoBank - 3) & %00000011)
  sta $DD00
}

.macro clearScreen(_screen, clearByte) {
	lda #clearByte
	ldx #0
!loop:
	sta _screen, x
	sta _screen + $100, x
	sta _screen + $200, x
	sta _screen + $300, x
	inx
	bne !loop-
}

.macro clearScreenIntReg(screenRegIndex, valueRegIndex) {
//  .var screenReg = getIntRegAddress(screenRegIndex)
//  .var valueReg  = getIntRegAddress(valueRegIndex)

  :popIntMem(arg2) // value
  :popIntMem(arg1) // screen

	lda arg2 + 0
	ldx #3 // do 4 x 256 bytes (0 = 1 x 256 bytes)

  ldy #0
!innerloop:
	sta (arg1),y
	iny
	bne !innerloop-

  cpx #0
	beq !finished+
	// increase MSB same as adding $0100 to the arg1 index
	inc arg1 + 1
	dex
	jmp !innerloop-
!finished:
}

.macro copyData8(srcAddr,dstAddr,count) {
  ldx #count
  beq !finished+
!:
  lda srcAddr,x
  sta dstAddr,x
  dex
  beq !finished+
  jmp !-
!finished:
}

.macro copyDataIntReg8(srcIndex,dstIndex,countIndex) {
/*
  ldx #countIndex
  beq !finished+
!:
  lda srcAddr,x
  sta dstAddr,x
  dex
  beq !finished+
  jmp !-
!finished:
*/
}

.macro loadIntRegIm(value) {
  :pushIntIm(value)
}

.macro loadIntRegIm8(value) {
  :pushIntIm8(value)
}

.macro loadIntRegMem(address) {
  :pushIntMem(address)
}

.macro loadIntRegMem8(address) {
  :pushIntMem8(address)
}

.macro loadIntRegMemIndirect8() {
  :popIntMem(arg1)

  ldy #0
  lda (arg1),y
  sta arg2 + 0

  lda #0
  sta arg2 + 1

  :pushIntMem(arg2)
}

.macro loadIntRegMemAddr8(address) {
  :pushIntMem8(address)
}

.macro storeIntRegMem(addr) {
  :popIntMem(addr)
}

.macro storeIntRegMemAt(addr) {
  :popIntMem(arg1)

  lda addr + 0
  sta arg2 + 0
  lda addr + 1
  sta arg2 + 1

  ldy #0
  lda arg1 + 0
  sta (arg2),y
  iny
  lda arg1 + 1
  sta (arg2),y
}

.macro storeIntRegMem8(addr) {
  :popIntMem8(addr)
}

.macro storeIntRegMem8At(addr) {
  :popIntMem8(arg1)

  lda addr + 0
  sta arg2 + 0
  lda addr + 1
  sta arg2 + 1

  ldy #0
  lda arg1 + 0
  sta (arg2),y

  iny
  lda #0
  sta (arg2),y
}

.macro storeIntRegMemArray(addr) {
//  .var srcReg = getIntRegAddress(valueIndex)     arg1
//  .var ofsReg = getIntRegAddress(offsetIndex)    arg2

  :popIntMem(arg2) // ofsReg
  :popIntMem(arg1) // srcReg

  clc
  lda #<addr
  adc arg2 + 0
  sta arg2 + 0

  lda #>addr
  adc arg2 + 1
  sta arg2 + 1

  ldy #0
  lda arg1 + 0
  sta (arg2),y
  iny
  lda arg1 + 1
  sta (arg2),y
}

.macro storeIntRegMemArrayAt(addr) {
//  .var srcReg = getIntRegAddress(valueIndex)     arg1
//  .var ofsReg = getIntRegAddress(offsetIndex)    arg2

  :popIntMem(arg2) // ofsReg
  :popIntMem(arg1) // srcReg

  // add offset to address
  clc
  lda addr + 0
  adc arg2 + 0
  sta arg2 + 0

  lda addr + 1
  adc arg2 + 1
  sta arg2 + 1

  ldy #0
  lda arg1 + 0
  sta (arg2),y
  iny
  lda arg1 + 1
  sta (arg2),y
}

.macro storeIntRegMemArray8(addr) {
//  .var srcReg = getIntRegAddress(valueIndex)     arg1
//  .var ofsReg = getIntRegAddress(offsetIndex)    arg2
  .var srcReg = arg1
  .var ofsReg = arg2

  :popIntMem(ofsReg) // ofsReg
  :popIntMem(srcReg) // srcReg

  clc
  lda #<addr
  adc ofsReg + 0
  sta ofsReg + 0

  lda #>addr
  adc ofsReg + 1
  sta ofsReg + 1

  ldy #0
  lda srcReg + 0
  sta (ofsReg),y
}

.macro loadIntRegMemArray8(addr,offsetIndex,dstIndex) {
//  .var dstReg = getIntRegAddress(dstIndex)
//  .var ofsReg = getIntRegAddress(offsetIndex)

  .var dstReg = arg1
  .var ofsReg = arg2

  :popIntMem(arg2) // ofsReg
  :popIntMem(arg1) // dstReg

  clc
  lda #<addr
  adc ofsReg + 0
  sta $fb + 0

  lda #>addr
  adc ofsReg + 1
  sta $fb + 1

  lda ($fb),y
  sta dstReg + 0
}

.macro loadIntRegMemArray(addr,offsetIndex,dstIndex) {
//  .var dstReg = getIntRegAddress(dstIndex)
//  .var ofsReg = getIntRegAddress(offsetIndex)

  .var dstReg = arg1
  .var ofsReg = arg2

  :popIntMem(arg2) // ofsReg
  :popIntMem(arg1) // dstReg

  clc
  lda #<addr
  adc ofsReg + 0
  sta $fb + 0

  lda #>addr
  adc ofsReg + 1
  sta $fb + 1

  lda ($fb),y
  sta dstReg + 0
  iny
  lda ($fb),y
  sta dstReg + 1
}

.macro storeIntRegMemArray8At(valueIndex,addr,offsetIndex) {
//  .var srcReg = getIntRegAddress(valueIndex)     arg1
//  .var ofsReg = getIntRegAddress(offsetIndex)    arg2
  .var srcReg = arg1
  .var ofsReg = arg2

  :popIntMem(arg2) // ofsReg
  :popIntMem(arg1) // srcReg

  clc
  lda addr + 0
  adc ofsReg + 0
  sta $fb + 0

  lda addr + 1
  adc ofsReg + 1
  sta $fb + 1

  ldy #0
  lda srcReg + 0
  sta ($fb),y
}

.macro poke() {
  :popIntMem(arg2) // value
  :popIntMem(arg1) // address

  ldy #0
  lda arg2 + 0
  sta (arg1),y
}

.macro poke16() {
  :popIntMem(arg2) // value
  :popIntMem(arg1) // address

  ldy #0
  lda arg2 + 0
  sta (arg1),y
  iny
  lda arg2 + 1
  sta (arg1),y
}

.macro addIntReg() {
  :popIntMem(arg2) // arg2
  :popIntMem(arg1) // arg1

  clc
  lda arg1 + 0
  adc arg2 + 0
  sta arg2 + 0

  lda arg1 + 1
  adc arg2 + 1
  sta arg2 + 1

  :pushIntMem(arg2)
}

.macro subIntReg() {
  :popIntMem(arg2) // arg2
  :popIntMem(arg1) // arg1

  sec
  lda arg1 + 0
  sbc arg2 + 0
  sta arg2 + 0

  lda arg1 + 1
  sbc arg2 + 1
  sta arg2 + 1

  :pushIntMem(arg2)
}

.macro mulIntReg() {
  :popIntMem(arg2) // arg2
  :popIntMem(arg1) // arg1

/*    sec
    lda srcReg1 + 0
    sbc srcReg2 + 0
    sta dstReg  + 0

    lda srcReg1 + 1
    sbc srcReg2 + 1
    sta dstReg  + 1*/
}

.macro divIntReg() {
  :popIntMem(arg2) // arg2
  :popIntMem(arg1) // arg1

/*    sec
    lda srcReg1 + 0
    sbc srcReg2 + 0
    sta dstReg  + 0

    lda srcReg1 + 1
    sbc srcReg2 + 1
    sta dstReg  + 1*/

  :pushIntMem(arg2)
}

.macro andIntReg() {
// arg1 = arg1 and arg2
  :popIntMem(arg2)
  :popIntMem(arg1)

  lda arg1 + 0
  and arg2 + 0
  sta arg1  + 0

  lda arg1 + 1
  and arg2 + 1
  sta arg1  + 1
  
  :pushIntMem(arg1)
}

.macro orIntReg() {
// arg1 = arg1 or arg2
  :popIntMem(arg2)
  :popIntMem(arg1)

  lda arg1 + 0
  ora arg2 + 0
  sta arg1  + 0

  lda arg1 + 1
  ora arg2 + 1
  sta arg1  + 1

  :pushIntMem(arg1)
}

.macro xorIntReg() {
// arg1 = arg1 xor arg2
  :popIntMem(arg2)
  :popIntMem(arg1)

  lda arg1 + 0
  eor arg2 + 0
  sta arg1  + 0

  lda arg1 + 1
  eor arg2 + 1
  sta arg1  + 1

  :pushIntMem(arg1)
}

.macro notIntReg() {
// arg1 = not arg1
  :popIntMem(arg1)

  lda arg1 + 0
  eor #$ff
  sta arg1  + 0

  lda arg1 + 1
  eor #$ff
  sta arg1  + 1

  :pushIntMem(arg1)
}

.macro shlIntReg() {
// arg1 = arg1 << arg2
  :popIntMem(arg2)
  :popIntMem(arg1)

  ldx arg2  + 0
!:
  asl arg1 + 0
  rol arg1 + 1

  dex
  bne !-

  :pushIntMem(arg1)
}

.macro shrIntReg() {
// arg1 = arg1 >> arg2
  :popIntMem(arg2)
  :popIntMem(arg1)

  ldx arg2  + 0
!:
  :div2_16bit(arg1)

  dex
  bne !-

  :pushIntMem(arg1)
}

.macro cmpIntRegEql() {
// arg1 = arg1 = arg2
  :popIntMem(arg2)
  :popIntMem(arg1)

  lda arg1 + 0
  cmp arg2 + 0
  bne notEqual
  lda arg1 + 1
  cmp arg2 + 1
  bne notEqual

isEqual:
  lda #TRUE
  jmp storeBoolean
notEqual:
  lda #FALSE
storeBoolean:
  sta arg1 + 0
  sta arg1 + 1

  :pushIntMem(arg1)
}

.macro cmpIntRegNeq() {
// arg1 = arg1 <> arg2
  :popIntMem(arg2)
  :popIntMem(arg1)

  lda arg1 + 0
  cmp arg2 + 0
  bne notEqual
  lda arg1 + 1
  cmp arg2 + 1
  bne notEqual

isEqual:
  lda #FALSE
  jmp storeBoolean
notEqual:
  lda #TRUE
storeBoolean:
  sta arg1 + 0
  sta arg1 + 1

  :pushIntMem(arg1)
}

.macro cmpIntRegGtr() {
// arg1 = arg1 > arg2

  :subIntReg()
  :popIntMem(arg1)

  lda arg1 + 0
  bne notZero
  lda arg1 + 1
  bne notZero
  // is zero (equal) so store FALSE
  lda #FALSE
  jmp storeBoolean

notZero:
  // check for +ve msb
  lda arg1 + 1
  bpl isTrue

  lda #FALSE
  jmp storeBoolean
isTrue:
  lda #TRUE
storeBoolean:
  sta arg1 + 0
  sta arg1 + 1

  :pushIntMem(arg1)
}

.macro cmpIntRegGeq() {
// arg1 = arg1 >= arg2

  :subIntReg()
  :popIntMem(arg1)

  lda arg1 + 0
  bne notZero
  lda arg1 + 1
  bne notZero
  // is zero (equal) so store TRUE
  lda #TRUE
  jmp storeBoolean

notZero:
  // check for +ve msb
  lda arg1 + 1
  bpl isTrue

  lda #FALSE
  jmp storeBoolean
isTrue:
  lda #TRUE
storeBoolean:
  sta arg1 + 0
  sta arg1 + 1

  :pushIntMem(arg1)
}

.macro cmpIntRegLss() {
// arg1 = arg1 < arg2
  :subIntReg()
  :popIntMem(arg1)

  lda arg1 + 0
  bne notZero
  lda arg1 + 1
  bne notZero
  // is zero (equal) so store FALSE
  lda #FALSE
  jmp storeBoolean

notZero:
  // check for -ve msb
  lda arg1 + 1
  bmi isTrue

  lda #FALSE
  jmp storeBoolean
isTrue:
  lda #TRUE
storeBoolean:
  sta arg1 + 0
  sta arg1 + 1

  :pushIntMem(arg1)
}

.macro cmpIntRegLeq() {
// arg1 = arg1 <= arg2

  :subIntReg()
  :popIntMem(arg1)

  lda arg1 + 0
  bne notZero
  lda arg1 + 1
  bne notZero
  // is zero (equal) so store TRUE
  lda #TRUE
  jmp storeBoolean

notZero:
  // check for -ve msb
  lda arg1 + 1
  bmi isTrue

  lda #FALSE
  jmp storeBoolean
isTrue:
  lda #TRUE
storeBoolean:
  sta arg1 + 0
  sta arg1 + 1

  :pushIntMem(arg1)
}


.macro intRegToBoolean() {
//  .var addr   = getIntRegAddress(regIndex)
  :popIntMem(arg1)

  lda arg1 + 0
  bne isTrue
  lda arg1 + 1
  bne isTrue
isFalse:
  lda #FALSE
  jmp storeBoolean
isTrue:
  lda #TRUE
storeBoolean:
  sta arg1 + 0
  sta arg1 + 1

  :pushIntMem(arg1)
}

.macro jmpIntRegFalse(falseAddress) {
  :popIntMem(arg1)

  lda arg1 + 0
  bne isTrue
  lda arg1 + 1
  bne isTrue
isFalse:
  jmp falseAddress
isTrue:
}

.macro setEchoScreenAddress(screenAddr) {
.const screenPntr = $fb
  lda #<screenAddr
  sta screenPntr + 0
  lda #>screenAddr
  sta screenPntr + 1
}

.macro echoIntToHexAt(intAddr,x,screenAddr) {
  :setEchoScreenAddress(screenAddr)
  ldx #x
  jsr setcurx
  lda intAddr + 1
  jsr prhex2
  lda intAddr + 0
  jsr prhex2
}

.macro echoByteToHexAt(intAddr,x,screenAddr) {
  :setEchoScreenAddress(screenAddr)
  ldx #x
  jsr setcurx
  lda intAddr + 0
  jsr prhex2
}

.var DPL = $fb
.var DPH = DPL + 1

//-------------------------------------------------------------------------
//  Put the string following in-line until a NULL out to the console
//  source: http://6502.org/source/io/primm.htm
//-------------------------------------------------------------------------
//
PUTSTRI: 
  pla			// Get the low part of "return" address
                          // (data start address)
  sta     DPL
  pla
  sta     DPH             // Get the high part of "return" address
                          // (data start address)
  // Note: actually we're pointing one short
PSINB:   
  ldy     #1
  lda     (DPL),y         // Get the next string character
  inc     DPL             // update the pointer
  bne     PSICHO          // if not, we're pointing to next character
  inc     DPH             // account for page crossing
PSICHO:
  ora     #0              // Set flags according to contents of
                          //    Accumulator
  beq     PSIX1           // don't print the final NULL
  jsr     $ffd2           // write it out
  jmp     PSINB           // back around
PSIX1:   
  inc     DPL             //
  bne     PSIX2           //
  inc     DPH             // account for page crossing
PSIX2:
  jmp     (DPL)           // return to byte following final NULL

//-------------------------------------------------------------------------
//  subroutine to set screen address xpos
//-------------------------------------------------------------------------
setcurx: {
  stx echo.xpos
  rts
}

//-------------------------------------------------------------------------
//  subroutine to print a screen code to the screen
//-------------------------------------------------------------------------
prchar: {
  jsr echo
  rts
}
//-------------------------------------------------------------------------
//  subroutine to print a 2 digit hexadecimal number in A
//-------------------------------------------------------------------------
prhex2: {
  pha
  lsr
  lsr
  lsr
  lsr
  // msd
  jsr prhex
  pla
  // fall through to prhex for lsd
}
//-------------------------------------------------------------------------
//  subroutine to print a hexadecimal digit
//-------------------------------------------------------------------------
prhex: {
.const ZERO         = 48
.const NINE_PLUSONE = ZERO + 10
  and #%00001111      //mask lsd for hex print
  ora #ZERO           //add zero
  cmp #NINE_PLUSONE   //is it a decimal digit?
  bcc echo            //yes! output it
  sbc #57             //add offset for letter a-f
                      //and fall through to print routine
}
//-------------------------------------------------------------------------
//  subroutine to print a character to the screen
//-------------------------------------------------------------------------
echo: {
.const screenPntr = $fb
  ldy xpos
  sta (screenPntr),y             //output character
  inc xpos                       //inc cursor pos
  rts
xpos: .byte 0
}