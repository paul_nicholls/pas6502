//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

/*
other ways include taking the raster position or
using the SID chip but this one suits my needs.
*/

randomCIA:  //Generate a random number (8-bit)
  lda $DC04       //CIA#1 Timer A Lo byte
  eor $DC05       //CIA#1 Timer A Hi byte
  eor $DD04       //CIA#2 Timer A Lo byte
  adc $DD05       //CIA#2 Timer A Hi byte
  eor $DD06       //CIA#2 Timer B Lo byte
  eor $DD07       //CIA#2 Timer B Hi byte
  sta random8bit
  rts

  random8bit: .byte 0

#define RAND16LFSR
initRandomLFSR:
// input: A = 8-bit seed for random number generator
  sta _rand8
#if RAND16LFSR
  eor #$ff
  sta _rand16
#endif
  rts

randomLFSR:
// output: A holds 8-bit random number
  lda _rand8
  lsr
#if RAND16LFSR
  rol _rand16      // this command is only used if _rand16 has been defined
#endif
  bcc noeor
  eor #$B4 
noeor: 
  sta _rand8
#if RAND16LFSR
  eor _rand16      // this command is only used if _rand16 has been defined
#endif
  rts
_rand8:  .byte 0
#if RAND16LFSR
_rand16: .byte 0
#endif
