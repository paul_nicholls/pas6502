//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit appleii_lores;
// source:
// https://www.appleoldies.ca/azgraphics33/rod33.htm
//
//base addresses for primary text page 
//also the base addresses for the 48 scanline pairs 
//for lores graphics mode 40 x 48 x 16 colors 
const
  textbase : array of Word = (
    $0400,
    $0480,
    $0500,
    $0580,
    $0600,
    $0680,
    $0700,
    $0780,
    $0428,
    $04A8,
    $0528,
    $05A8,
    $0628,
    $06A8,
    $0728,
    $07A8,
    $0450,
    $04D0,
    $0550,
    $05D0,
    $0650,
    $06D0,
    $0750,
    $07D0
  );

procedure loplot(px,py,color,page : Byte);
var
  crt   : Pointer;
  c1,c2 : Byte;
  y1    : Byte;
  pixel : Byte;
begin
   y1 := py / 2;
   if (page <> 0) then px := px + 1024;

   c2 := (color and 15);

   if (py and 1 = 0) then begin
     //even rows in low nibble 
     //mask value to preserve high nibble 
     c1 := 240;
   end
   else begin
     //odd rows in high nibble 
     //mask value to preserve low nibble 
     c1 := 15;
     c2 := c2 * 16;
   end;

   crt := textbase[y1] + px;
   pixel := crt^;
   pixel := (pixel and c1) or c2;
   crt^ := pixel;
end;

procedure lohlin(ly, lx1, lx2,color,page : Byte);
var
  x : Byte;
begin
  for x := lx1 to lx2 do 
    loplot(x,ly,color,page);  
end; 

procedure lovlin(lx, ly1, ly2,color,page : Byte); 
var
  y : Byte;
begin
  for y := ly1 to ly2 do 
    loplot(lx,y,color,page);  
end; 

procedure lobox(bx1,by1,bx2,by2,color,page : Byte); 
begin
  lohlin(by1, bx1, bx2,color,page);
  Inc(by1);
  Dec(by2);
  lovlin(bx1, by1, by2,color,page);
  lovlin(bx2, by1, by2,color,page);
  Inc(by2);
  lohlin(by2, bx1, bx2,color,page);
end;