unit Atari8Bit_io;

type 
  TIOCB = record
    ICHID : Byte;  //DEVICE HANDLER
    ICDNO : Byte;  //DEVICE NUMBER
    ICCOM : Byte;  //I/O COMMAND
    ICSTA : Byte;  //I/O STATUS
    ICBAL : Byte;  //LSB BUFFER ADDR
    ICBAH : Byte;  //MSB BUFFER ADDR
    ICPTL : Byte;  //LSB PUT ROUTINE
    ICPTH : Byte;  //MSB PUT ROUTINE
    ICBLL : Byte;  //LSB BUFFER LEN
    ICBLH : Byte;  //MSB BUFFER LEN
    ICAX1 : Byte;  //AUX BYTE 1
    ICAX2 : Byte;  //AUX BYTE 1
  end;

var
  IOCB   : TIOCB absolute $0340;
   