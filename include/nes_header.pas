//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit nes_header;

const
  MAPPER_HORIZONTAL = $2;
  MAPPER_VERTICAL   = $3;
  MAPPER_FOURSCREEN = $a;
  NTSC              = $0;
  PAL               = $1;
//   NES               = 0;
//   NESVS             = 1;
  PLAYCHOICE10      = 2;
  NAMETABLE_0       = $2000;
  NAMETABLE_1       = $2400;
  NAMETABLE_2       = $2800;
  NAMETABLE_3       = $2c00;

macro _NESHEADER(mapper,system,country,prgrom_banks,chrrom_banks); assembler;
asm
  .byte $4E,$45,$53,$1A  //  NES
  .byte prgrom_banks
  .byte chrrom_banks
  .byte mapper,system
  .byte $00
  .byte country
  .byte  $00,$00,$00,$00,$00,$00
end;
