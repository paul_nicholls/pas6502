unit c128;

procedure selectBank0; assembler;
asm
  lda #0
  sta $FF01
end;

procedure selectBank1; assembler;
asm
  lda #0
  sta $FF02
end;

procedure selectBank14; assembler;
asm
  lda #0
  sta $FF03
end;

procedure selectBank15; assembler;
asm
  lda #0
  sta $FF00
end;

procedure selectBankCustom(a : Byte); assembler;
asm
  sta $FF04
end;