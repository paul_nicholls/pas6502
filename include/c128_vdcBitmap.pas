//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------
//
// adapted from:
// https://gitlab.com/aaron-baugher/6502-assembly-language-programming/-/blob/master/vdc80glib.a?ref_type=heads

unit c128_vdcBitmap;

uses
  c128_vdcCommon;

const
  VDC_XRES = 640;
  VDC_YRES = 200;
  
  VDC_SCREEN_WIDTH  = VDC_XRES / 8;
  VDC_SCREEN_HEIGHT = VDC_YRES / 8;

procedure vdc_setBitmapMode; assembler;
asm
  jsr vdcInitColors
  ldx #25
  jsr vdc_regRead
  and #%10111111
  ora #%10000000
  jsr vdc_regWrite
end;

procedure vdc_setColors(pen,background : Byte); assembler;
// set foreground/background color for graphics mode (in A reg)
asm
  lda pen
  asl
  asl
  asl
  asl
  ora background
  ldx #26
  jsr vdc_regWrite
end;
        

procedure vdc_setBitmapRamPntrXY(value : Word); assembler;
asm
  ldx #19
  lda value
  jsr vdc_regWrite
  dex
  lda value+1
  jsr vdc_regWrite
end;

procedure vdc_setBitmapRamPntr00; assembler; volatile;
// move ram pointer to 0/0 in RAM
asm
  lda #0
  ldx #19
  jsr vdc_regWrite
  dex
  lda #0
  jsr vdc_regWrite
end;

procedure vdc_fillBitmapRam(a : Byte); assembler;
asm
  pha
  jsr vdc_setBitmapRamPntr00
  pla
  jsr vdc_writeByte
  lda #249
  jsr vdc_repeatByte
  lda #250
  ldy #63
!:
  jsr vdc_repeatByte
  dey
  bne !-
end;

procedure vdc_setBitmapRamPointer; assembler; volatile;
asm
//set VDC RAM pointer
// pass address in _TEMP_/_TEMP_+1
  ldx #19
  lda _TEMP_
  jsr vdc_regWrite
  dex
  lda _TEMP_+1
  jsr vdc_regWrite
end;

procedure vdc_plotPixel(x : Word; y : Byte; value : Byte); assembler;
const
  ontable   : array of Byte = ($80,$40,$20,$10,$08,$04,$02,$01);
  offtable  : array of Byte = ($7f,$bf,$df,$ef,$f7,$fb,$fd,$fe);

  pixelRowOffsetY_lo : array of Word = for y := 0 to VDC_YRES - 1 do <(y * 80);
  pixelRowOffsetY_hi : array of Word = for y := 0 to VDC_YRES - 1 do >(y * 80);

//  write/clear a point at .X/.A / .Y coordinates in bitmap
//  set carry flag to write, clear it to clear
asm
  lda value
  // rotate value into carry for clear/set
  lsr
  ldx x
  lda x+1
	php                     // save carry flag on stack
	sta highx
  stx lowx
  txa
  and #%00000111          // mask to get modulo 8
  pha                     // save low byte of X coordinate on stack
  
  lda pixelRowOffsetY_lo,y
  sta _TEMP_
  lda pixelRowOffsetY_hi,y
  sta _TEMP_+1
{
	lda #0
  sta _TEMP_+1
  sty _TEMP_
  asl _TEMP_
  rol _TEMP_+1
  asl _TEMP_
  rol _TEMP_+1              // .Y times 4
  tya
  clc
  adc _TEMP_
  sta _TEMP_               // .Y times 5
  bcc !+
  inc _TEMP_+1
!:
  ldy #4
!:
  asl _TEMP_
  rol _TEMP_+1
  dey
  bne !-                   // .Y times 80 in _TEMP_/_TEMP_+1
}
  lsr highx
  ror lowx
  lsr highx
  ror lowx
  lsr highx
  ror lowx                // divide X coordinate by 8 to get horizontal byte #
  lda lowx
  clc
  adc _TEMP_
  sta _TEMP_
  lda highx
  adc _TEMP_+1
  sta _TEMP_+1              // address of byte to work on in _TEMP_/_TEMP_+1
  
  jsr vdc_setBitmapRamPointer
  jsr vdc_readByte          // get value currently at byte in RAM
  tay
  pla
  tax
  tya
  plp
  bcc !+                   // jump ahead if clearing a bit
  ora ontable,x
  bne !++
!:
  and offtable,x
!:
  tay
  jsr vdc_setBitmapRamPointer
  tya
  jsr vdc_writeByte
  rts
highx: .byte 0
lowx:  .byte 0
end;

procedure vdc_clearBitmapRam; assembler; volatile;
asm
  lda #0
  jsr vdc_fillBitmapRam
end;
(*
;;; routines for the 80-column display
        ;;
        ;; vdc_clearRam    -- clear 80-column graphics screen. No arguments
	;; vdc_fillRam     -- fill graphics ram with value in .A
        ;; position80xy  -- position the RAM pointer at .X/.Y coordinates in screen memory FIXME
        ;; vdc_repeatByte  -- repeat the last byte written to VDC RAM
        ;;                  pass the number of times in .A
	;; vdc_writeByte   -- write a byte to current VDC RAM pointer
        ;;                  pass byte to write in .A
        ;; vdc_setBitmapRamPntr -- set VDC RAM pointer
        ;;                  pass low byte in A, high byte in Y
        ;; init80graphics-- initialize VDC for bitmap graphics display
        ;; vdc_regRead       -- read a VDC register
        ;;                  pass register # in .X, value returned in .A
        ;; vdc_regWrite      -- write to a VDC register
        ;;                  pass register # in .X, value to write in .A

!zone vdc80glib {
	!addr count80 = $fa
        !addr _TEMP_   = $fb
        !addr _TEMP_+1  = $fc
	!addr lowx    = $fd       ; $fd-$fe X coordinate of bitmap
	!addr highx   = $fe 
        
	;; set foreground/background color for graphics mode
        ;;    pass the color set in .A
set80gcolors:
        ldx #26
        jsr vdc_regWrite
        rts
        
vdc_clearRam:
        lda #0
        jsr vdc_fillRam
        rts
        
	;; fill screen ram with char in .A
vdc_fillRam:
	pha
        jsr vdc_setBitmapRamPntr00
        pla
        jsr vdc_writeByte
        lda #249
        jsr vdc_repeatByte
	lda #250
        ldy #63
-       jsr vdc_repeatByte
        dey
        bne -
        rts

        ;; move ram pointer to 0/0 in RAM
vdc_setBitmapRamPntr00:
        lda #0
        ldx #19
        jsr vdc_regWrite
        dex
        lda #0
        jsr vdc_regWrite
        rts
	
        ;; write80gxy -- write/clear a point at .X/.A / .Y coordinates in bitmap
        ;;               set carry flag to write, clear it to clear
write80gxy:
	php                     ; save carry flag on stack
	sta highx
        stx lowx
	txa
        and #%00000111          ; mask to get modulo 8
        pha                     ; save low byte of X coordinate on stack
	lda #0
        sta _TEMP_+1
        sty _TEMP_
        asl _TEMP_
        rol _TEMP_+1
        asl _TEMP_ 
        rol _TEMP_+1              ; .Y times 4
        tya
        clc
        adc _TEMP_
        sta _TEMP_               ; .Y times 5
        bcc +
        inc _TEMP_+1
+       ldy #4
-       asl _TEMP_
        rol _TEMP_+1
        dey
        bne -                   ; .Y times 80 in _TEMP_/_TEMP_+1
        
	lsr highx
        ror lowx
	lsr highx
        ror lowx
	lsr highx
        ror lowx                ; divide X coordinate by 8 to get horizontal byte #
        lda lowx
        clc
        adc _TEMP_
        sta _TEMP_
        lda highx
        adc _TEMP_+1
        sta _TEMP_+1              ; address of byte to work on in _TEMP_/_TEMP_+1

	jsr vdc_setBitmapRamPntr
        jsr vdc_readByte          ; get value currently at byte in RAM
        tay
	pla
        tax
	tya
        plp
        bcc +                   ; jump ahead if clearing a bit
        ora ontable,x
        bne ++
+       and offtable,x
++      tay
        jsr vdc_setBitmapRamPntr
        tya
        jsr vdc_writeByte
        rts


        ;; pass the number of times in A
vdc_repeatByte:
	pha
	ldx #24
        jsr vdc_regRead
        and #$7f
        jsr vdc_regWrite
        pla
        ldx #30
        jsr vdc_regWrite
        rts

	;; pass the byte in A, will write to current ram pointer
vdc_writeByte:
        ldx #31
        jsr vdc_regWrite
        rts

      	;; will return byte at current ram pointer in .A
vdc_readByte:
        ldx #31
        jsr vdc_regRead
        rts

        ;; pass address in _TEMP_/_TEMP_+1
vdc_setBitmapRamPntr:
        ldx #19
        lda _TEMP_
        jsr vdc_regWrite
        dex
	lda _TEMP_+1
        jsr vdc_regWrite
        rts

init80graphics:
        ldx #25
        jsr vdc_regRead
	and #%10111111
        ora #%10000000
        jsr vdc_regWrite
        rts
}
        


vdc80ram:    !hex 00 00
ontable:     !hex 80 40 20 10 08 04 02 01
offtable:    !hex 7f bf df ef f7 fb fd fe
*)
