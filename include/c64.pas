//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    this file was translated from: https://github.com/KarolS/millfork
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit c64;
// floating point routines
  procedure MOVFM                          external $bba2; // MOVFM. Move a Floating Point Number from Memory to FAC1
  procedure FOUT                           external $bddd; // FOUT. Convert Contents of FAC1 to ASCII String
  procedure MOV2F                          external $bbc7; // MOV2F. Move a Floating Point Number from FAC1 to Memory

var
// interrupt routines and vectors
  IRQVECLO   : byte absolute $0314; // hardware interrupt (IRQ) vector, low byte
  IRQVECHI   : byte absolute $0315; // hardware interrupt (IRQ) vector, high byte

  procedure STDIRQ external $ea31; //start address of standard interrupt routines

var
// keyboard
  CURRKEY  : byte absolute $cb;   // currently pressed keycode is stored to $cb; $40  = no key pressed

  // color ram
  colorRAM : byte absolute $d800;