.segment HEADER
////////////////////////
//                    //
//       HEADER       //
//                    //
////////////////////////

.const h_PRG_ROM_SIZE   = 16   // PRG-ROM size in 16k banks        // (for UNROM-512: May be 16/32 for 256/512 KiB, though undersize ROMs such as 2/4/8 should still function)
.const h_CHR_ROM_SIZE   = 0    // CHR-ROM size in 8k banks
.const h_MAPPER         = 30   // Mapper number
.const h_MIRRORING      = 1    // Mirroring (Vertical = 1)
.const h_FOUR_SCREEN    = 0    // Four screen vram
.const h_BATTERY        = 1    // Non-volatile memory present?        // (for UNROM-512: 1 = self flashing, no bus conflicts// 0 = not self flashing, bus conflicts)
.const h_SUBMAPPER      = 0    // Submapper
.const h_PRG_RAM_SIZE   = 0    // PRG-RAM size (64 * 2^n)
.const h_PRG_NVRAM_SIZE = 0    // Non-volatile PRG-RAM size (64 * 2^n)
.const h_CHR_RAM_SIZE   = 7    // CHR-RAM size (64 * 2^n)        // (for UNROM-512: May be 7/8/9 for 8/16/32 KiB respectively)
.const h_CHR_NVRAM_SIZE = 0    // Non-volatile CHR-RAM size (64 * 2^n)
.const h_CONSOLE_TYPE   = 0    // 0 = NTSC, 1 = PAL, 2 = Multi-region, 3 = Dendy

//////////////////////////////////////////////////////////////////////////////////

.byte $4E,$45,$53,$1A // NES, break
.byte h_PRG_ROM_SIZE & $FF
.byte h_CHR_ROM_SIZE & $FF
.byte ((h_MAPPER & $0F) << 4) | (h_FOUR_SCREEN << 3) | (h_BATTERY << 1) | (h_MIRRORING)
.byte (h_MAPPER & $F0) | %1000
.byte (h_SUBMAPPER << 4) | ((h_MAPPER & $F00) >> 8)
.byte ((h_PRG_ROM_SIZE & $F00) >> 4) | ((h_CHR_ROM_SIZE & $F00) >> 8)
.byte (h_PRG_NVRAM_SIZE << 4) | (h_PRG_RAM_SIZE)
.byte (h_CHR_NVRAM_SIZE << 4) | (h_CHR_RAM_SIZE)
.byte h_CONSOLE_TYPE
.byte $00
.byte $00
.byte $00
