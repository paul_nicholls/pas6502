.segmentout [segments ="HEADER"]
.segmentout [segments ="BANK1"]
.segmentout [segments ="BANK2"]
.segmentout [segments ="CHARS"]

.segment HEADER [start=$00,max=$10]
.segment ZP [start=$00,max=$ff]
//	free ram
.segment RAM [start=$140,max=$6ff]
//	sprite ram
.segment OAM [start=$700,max=$7ff]
.segment BANK1 [start=$8000,min=$8000,max=$bfff,fill]
.segment BANK2 [start=$c000,min=$c000,max=$ffff,fill]
.segment CHARS [start=$0000,min=$0000,max=$1fff,fill]

