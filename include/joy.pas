//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    this file was inspired from: https://github.com/KarolS/millfork
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit joy;

const
  JOY_PLUS          = 1;
  JOY_MINUS         = 255;
  JOY_DOWN          = 1;
  JOY_UP            = 0;

var
  input_dx, 
  input_lastDx, 
  input_dy, 
  input_lastDy      : byte;

  input_pushedLeft, 
  input_pushedRight, 
  input_pushedUp, 
  input_pushedDown  : Boolean;

  input_btn, 
  input_lastBtn     : byte;

  input_btnPressed, 
  input_btnReleased : Boolean;

procedure reset_joy;
begin
  input_lastDx      := input_dx;
  input_lastDy      := input_dy;
  input_dx          := 0;
  input_dy          := 0;
  input_lastBtn     := input_btn;
  input_btn         := JOY_UP;
  input_btnPressed  := false;
  input_btnReleased := false;
  input_pushedLeft  := false;
  input_pushedRight := false;
  input_pushedUp    := false;
  input_pushedDown  := false;
end;

