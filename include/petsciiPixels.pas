unit petsciiPixels;

// chunky pixel info 80x50 pixel resolution
//
// see links below for original information on topic:
// https://www.defiancestudios.com/2018/08/17/80-x-50-plotting-utility/
// https://www.defiancestudios.com/2018/09/21/making-the-cplot-utility/
//
const
  RES_WIDTH      = 80;
  RES_HEIGHT     = 50;

// offset -> bit
  qlookup : array of Byte = (1,2,4,8);

// char index (0..15) -> plot char
  plotChar : array of Byte = (
    $20,$7E,$7C,$E2,
    $7B,$61,$FF,$EC,
    $6C,$7F,$E1,$FB,
    $62,$FC,$FE,$A0
  );

var
// quick plot char -> char index (0..15)         
  plotCharIndex : array[0..256 - 1] of Byte;

procedure initPlotCharIndex();
var
  i : Byte;
begin
  for i := 0 to 255 do plotCharIndex[i] := 0;

  for i := 0 to 15 do plotCharIndex[plotChar[i]] := i;
end;
             
procedure plotPetsciiPixel(screen : Pointer; x,y,color : byte);
const
  screenOffset : array  of Word = for y := 0 to RES_HEIGHT/2 - 1 do (y*40);

var
  tx,ty : Byte;
  ox,oy : Byte;
  ofs   : Byte;
  bit   : Byte;
  c     : Byte;
begin
  if x >= RES_WIDTH then Exit;
  if y >= RES_HEIGHT then Exit;
  
  // screen tile
  tx := x / 2;
  ty := y / 2;
  // screen tile offset (0,1)
  ox := x and 1;
  oy := y and 1;
  // offset
  oy := oy shl 1;

  ofs := ox + oy;
  bit := qlookup[ofs];

  screen := screen + screenOffset[ty];
  // get char at tx,ty on screen
  c := screen^[tx];
  // convert to plot char index
  c := plotCharIndex[c];

  // turn on bit in this plot char index
  c := c or bit;
  // convert index to plot char and write to screen
  screen^[tx] := plotChar[c];
  
  screen := $d800;
  screen := screen + screenOffset[ty];
  screen^[tx] := color;
end;
