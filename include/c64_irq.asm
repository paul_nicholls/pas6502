//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    this file was translated from: https://github.com/KarolS/millfork
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

#importonce

.macro SetupRasterIRQ(IRQaddr,IRQline) {
    lda #$7f        // Disable CIA IRQ's
    sta $dc0d
    sta $dd0d

    lda #<IRQaddr   // Install RASTER IRQ
    ldx #>IRQaddr   // into Hardware
    sta $fffe       // Interrupt Vector
    stx $ffff

    .var IRQ_TYPE = 1
//    .if (enableSpriteSpriteCollision = true) {
//      .eval IRQ_TYPE = IRQ_TYPE + 4
//    }
    lda #IRQ_TYPE  // Enable RASTER IRQs
    sta $d01a
    lda #IRQline    // IRQ raster line
    sta $d012
    .if (IRQline > 255) {
        .error "supports only less than 256 lines"
    }
    lda $d011   // clear IRQ raster line bit 8
    and #$7f
    sta $d011

    asl $d019  // Ack any previous raster interrupt
    bit $dc0d  // reading the interrupt control registers
    bit $dd0d  // clears them
}
//----------------------------------------------------------

.macro EndIRQ(nextIRQaddr,nextIRQline) {
    asl $d019
    lda #<nextIRQaddr
    sta $fffe
    lda #>nextIRQaddr
    sta $ffff
    lda #nextIRQline
    sta $d012
/*   .if(IRQlineHi) {
        lda $d011
        ora #$80
        sta $d011
    }*/
}

.macro EndIRQ2(nextIRQaddr) {
    asl $d019
    lda #<nextIRQaddr
    sta $fffe
    lda #>nextIRQaddr
    sta $ffff
}

.macro EndIRQ_var(nextIRQaddr,nextIRQline) {
    asl $d019
    lda #<nextIRQaddr
    sta $fffe
    lda #>nextIRQaddr
    sta $ffff
    lda nextIRQline
    sta $d012
/*   .if(IRQlineHi) {
        lda $d011
        ora #$80
        sta $d011
    }*/
}

.macro irq_start(end_lbl) {
    sta end_lbl-6
    stx end_lbl-4
    sty end_lbl-2
}

.macro irq_end(next, line) {
    :EndIRQ(next, line)
    lda #$00
    ldx #$00
    ldy #$00
    rti
}

.macro irq_end2(next) {
    :EndIRQ2(next)
    lda #$00
    ldx #$00
    ldy #$00
    rti
}