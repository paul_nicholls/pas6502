//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit c64_irq;

var
  IRQ_vector : Word absolute $fffe;

procedure setupRasterIRQ(addr : Word; raster : Byte);
begin
  asm
    sei        //disable maskable IRQs
    
    lda #$7f
    sta $dc0d  //disable timer interrupts which can be generated by the two CIA chips
    sta $dd0d  //the kernal uses such an interrupt to flash the cursor and scan the keyboard, so we better
               //stop it.
    
    lda $dc0d  //by reading this two registers we negate any pending CIA irqs.
    lda $dd0d  //if we don't do this, a pending CIA irq might occur after we finish setting up our irq.
               //we don't want that to happen.
    
    lda #$01   //this is how to tell the VICII to generate a raster interrupt
    sta $d01a
    
    lda raster   //this is how to tell at which rasterline we want the irq to be triggered
    sta $d012
    
    lda #$1b   //as there are more than 256 rasterlines, the topmost bit of $d011 serves as
    sta $d011  //the 9th bit for the rasterline we want our irq to be triggered.
               //here we simply set up a character screen, leaving the topmost bit 0.
    
    lda #$35   //we turn off the BASIC and KERNAL rom here
    sta $01    //the cpu now sees RAM everywhere except at $d000-$e000, where still the registers of
               //SID/VICII/etc are visible
    
    lda addr.b0  //this is how we set up
    sta $fffe  //the address of our interrupt code
    lda addr.b1
    sta $ffff
    
    cli        //enable maskable interrupts again
  end;
end;

procedure setupKernalIRQ(addr : Word);
begin
  asm
    sei        //disable maskable IRQs

    lda addr.b0  //this is how we set up
    sta $0314    //the address of our interrupt code
    lda addr.b1
    sta $0315
    
    cli        //enable maskable interrupts again
  end;
end;

procedure setupNMI(addr : Word);
begin
  asm
    sei
  
    lda addr.b0
    sta $fffa + 0
    lda addr.b1
    sta $fffa + 1
  
    cli  
  end;
end;
  
macro irq_start; assembler;
asm
  pha
  txa
  pha
  tya
  pha
  asl $d019
end;

macro kernal_irq_start; assembler;
asm
  asl $d019
end;

macro irq_end; assembler;
asm
  pla
  tay
  pla
  tax
  pla

  rti
end;

macro kernal_irq_end; assembler;
asm
  jmp $ea31
//  jmp $EABF
end;

//import 'c64_irq.asm';