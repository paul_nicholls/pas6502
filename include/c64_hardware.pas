//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    this file was translated from: https://github.com/KarolS/millfork
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit c64_hardware;

uses
  c64_vic,
  c64_sid,
  c64_cia,
  cpu6510;

macro c64_ram_only;
begin
  cpu6510_ddr  := 7;
  cpu6510_port := 0;
end;

macro c64_ram_io;
begin
  cpu6510_ddr  := 7;
  cpu6510_port := 5;
end;

macro c64_ram_io_kernal;
begin
  cpu6510_ddr  := 7;
  cpu6510_port := 6;
end;

macro c64_ram_io_basic;
begin
  cpu6510_ddr  := 7;
  cpu6510_port := 7;
end;

macro c64_ram_charset;
begin
  cpu6510_ddr  := 7;
  cpu6510_port := 1;
end;

macro c64_ram_charset_kernal;
begin
  cpu6510_ddr  := 7;
  cpu6510_port := 2;
end;

macro c64_ram_charset_basic;
begin
  cpu6510_ddr  := 7;
  cpu6510_port := 3;
end;

