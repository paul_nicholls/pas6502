// A flexible sprite multiplexer routine for 32 sprites.
// decoded and modified from kickc
// Usage:
// - Once:
// - plexInit(screen): Initialize the data structures and set the screen address
// Each frame:
// - Set x-pos, y-pos and pointer in PLEX_XPOS[id], PLEX_YPOS[id], PLEX_PTR[id]
// - set enabled, color and multicolor in PLEX_ENABLED[id], PLEX_COLOR[id], PLEX_MULTICOLOR[id]
// - plexSort() Sorts the sprites according to y-positions and prepares for showing them. This uses an insertion sort that is quite fast when the relative order of the sprites does not change very much.
// - plexShowSprite() Shows the next sprite by copying values from PLEX_XXX[] to an actual sprite. Actual sprites are used round-robin. This should be called once for each of the 24 virtual sprites.
// - plexFreeNextYpos() Returns the Y-position where the next sprite is available to be shown (ie. the next pos where the next sprite is no longer in use showing something else).
// - plexShowNextYpos() Returns the Y-position of the next sprite to show.
//
// In practice a good method is to wait until the raster is beyond plexFreeNextYpos() and then call plexShowSprite(). Repeat until all 32 sprites have been shown.
// TODO: Let the caller specify the number of sprites to use (or add PLEX_ENABLE : array[0..PLEX_COUNT - 1] of Byte)
 
//#include <c64-multiplexer2.h>
//#include <c64.h>

unit c64_multiplexer;

uses
  c64_vic;

// The number of sprites in the multiplexer
const PLEX_COUNT = 32;

var
//--------------------------------------------
//--------------------------------------------
// Contains the Y-position where each sprite is free again. PLEX_FREE_YPOS[s] holds the Y-position where sprite s is free to use again.
PLEX_FREE_YPOS : array[0..7] of Byte;

// The index of the sprite that is free next. Since sprites are used round-robin this moves forward each time a sprite is shown.
plex_free_next : Byte = 0;
 
// new sprite stuff
// The sprite enable for each multiplexer sprite; 0 = 0ff, 1 = on
PLEX_ENABLED : array[0..PLEX_COUNT - 1] of Byte;

// The sprite color for each multiplexer sprite
PLEX_COLOR : array[0..PLEX_COUNT - 1] of Byte;
 
// The sprite multicolor enable for each multiplexer sprite; 0 = hi-res, 1 = multicolor
PLEX_MULTICOLOR : array[0..PLEX_COUNT - 1] of Byte;
//--------------------------------------------    
//--------------------------------------------    
 
// The x-positions of the multiplexer sprites (0x000-0x1ff)
PLEX_XPOS : array[0..PLEX_COUNT - 1] of Word;
 
// The y-positions of the multiplexer sprites.
PLEX_YPOS : array[0..PLEX_COUNT - 1] of Byte;
 
// The sprite pointers for the multiplexed sprites
PLEX_PTR : array[0..PLEX_COUNT - 1] of Byte;
 
// The address of the sprite pointers on the current screen (screen+0x3f8).
PLEX_SCREEN_PTR : Pointer;// = $0400+$03f8;
 
// Indexes of the plex-sprites sorted by sprite y-position. Each call to plexSort() will fix the sorting if changes to the Y-positions have ruined it.
PLEX_SORTED_IDX : array[0..PLEX_COUNT - 1] of Byte;
 
// Variables controlling the showing of sprites
 
// The index in the PLEX tables of the next sprite to show
plex_show_idx : Byte = 0;
// The index the next sprite to use for showing (sprites are used round-robin)
plex_sprite_idx : Byte = 0;
// The MSB bit of the next sprite to use for showing
plex_sprite_msb  : Byte = 1;
// the mask of the next sprite
plex_sprite_mask : Byte = 254;
 
// Set the address of the current screen used for setting sprite pointers (at screen+0x3f8)
procedure plexSetScreen(screen : Word);
begin
  PLEX_SCREEN_PTR := screen+$03f8;
end;
 
// Initialize the multiplexer data structures
procedure plexInit(screen : Word);
var
  i : Byte;
begin
  plexSetScreen(screen);

  for i := 0 to PLEX_COUNT - 1 do
    PLEX_SORTED_IDX[i] := i;

  plex_show_idx := 0;
  // The index the next sprite to use for showing (sprites are used round-robin)
  plex_sprite_idx := 0;
  // The MSB bit of the next sprite to use for showing
  plex_sprite_msb  := 1;
end;

// Prepare for a new frame. Initialize free to zero for all sprites.
procedure plexFreePrepare(); volatile;
var
  s : Byte;
begin
  asm
    ldx #7
    lda #0
  !:
    sta PLEX_FREE_YPOS,x
    dex
    bpl !-
  end;
//   for s := 0 to 7 do
//     PLEX_FREE_YPOS[s] := 0;
// 
//   plex_free_next := 0;
end;
 
// Get the Y-position where the next sprite to be shown is free to use.
function plexFreeNextYpos() : Byte;
begin
   Result := PLEX_FREE_YPOS[plex_free_next];
end;
 
// Update the data structure to reflect that a sprite has been shown. This sprite will be free again after 21 lines.
procedure plexFreeAdd(ypos : Byte);
begin
  asm
    ldx plex_free_next
    lda ypos
    clc
    adc #22
    sta PLEX_FREE_YPOS,x
    
    inc plex_free_next
    lda plex_free_next
    and #7
    sta plex_free_next
  end;
//   PLEX_FREE_YPOS[plex_free_next] :=  ypos+22;
// 
//   plex_free_next := plex_free_next + 1;
//   plex_free_next := plex_free_next and 7;
end;

// Ensure that the indices in PLEX_SORTED_IDX is sorted based on the y-positions in PLEX_YPOS
// Assumes that the positions are nearly sorted already (as each sprite just moves a bit)
// Uses an insertion sort:
// 1. Moves a marker (m) from the start to end of the array. Every time the marker moves forward all elements before the marker are sorted correctly.
// 2a. If the next element after the marker is larger that the current element
//     the marker can be moved forwards (as the sorting is correct).
// 2b. If the next element after the marker is smaller than the current element:
//     elements before the marker are shifted right one at a time until encountering one smaller than the current one.
//      It is then inserted at the spot. Now the marker can move forward.
procedure plexSort();
var
  m,nxt_idx,nxt_y,s : Byte;
  sortedIDX         : Byte;
begin
  for m := 0 to PLEX_COUNT - 2 do begin
    nxt_idx := PLEX_SORTED_IDX[m+1];
    nxt_y   := PLEX_YPOS[nxt_idx];

    sortedIDX := PLEX_SORTED_IDX[m];

    if(nxt_y < PLEX_YPOS[sortedIDX]) then begin
      //Shift values until we encounter a value smaller than nxt_y
      s := m;
      while (true) do begin
        PLEX_SORTED_IDX[s+1] := PLEX_SORTED_IDX[s];
        dec(s);//s--;
        sortedIDX := PLEX_SORTED_IDX[s];

        if (s = $ff) then break;
        if (nxt_y >= PLEX_YPOS[sortedIDX]) then break;
      end;
      // store the mark at the found position
      inc(s);//s++;
      PLEX_SORTED_IDX[s] := nxt_idx;
    end;
  end;
  //Prepare for showing the sprites
  plex_show_idx := 0;
  plex_sprite_idx := 0;
  plex_sprite_msb := 1;
  plex_sprite_mask := $ff xor plex_sprite_msb;

  plexFreePrepare();
//   asm
//     lda #0
//     sta m
//   !forLoop:
//     // nxt_idx = PLEX_SORTED_IDX[m+1];
//     ldx m
//     lda PLEX_SORTED_IDX + 1,x
//     sta nxt_idx
// 
//     // nxt_y = PLEX_YPOS[nxt_idx];
//     tax
//     lda PLEX_YPOS,x
//     sta nxt_y
// 
//     // sortedIDX = PLEX_SORTED_IDX[m];
//     ldx m
//     lda PLEX_SORTED_IDX,x
//     sta sortedIDX
// 
//     // if nxt_y >= PLEX_YPOS[sortedIDX]; if true, skip while loop
//     tax
//     lda nxt_y
//     cmp PLEX_YPOS,x
//     bcs !endOfIfThen+ // skip while loop
// 
//     // Shift values until we encounter a value smaller than nxt_y
//     //            s = m;
//     lda m
//     sta s
// 
//   !whileLoop:
//     // PLEX_SORTED_IDX[s+1] = PLEX_SORTED_IDX[s];
//     ldx s
//     lda PLEX_SORTED_IDX,x
//     sta PLEX_SORTED_IDX + 1,x
//     // s--;
//     dec s
// 
//     // sortedIDX = PLEX_SORTED_IDX[s]
//     ldx s
//     lda PLEX_SORTED_IDX,x
//     sta sortedIDX
// 
//     // if (s == 0xff) {break;}
//     txa
//     cmp #$ff
//     beq !whileEnd+
// 
//     // if (nxt_y >= PLEX_YPOS[sortedIDX]) {break;}
//     ldx sortedIDX
//     lda nxt_y
//     cmp PLEX_YPOS,x
//     bcs !whileEnd+ // >= exit loop
//     jmp !whileLoop-
//   !whileEnd:
//     // end of while loop
//     // store the mark at the found position
//     // s++;
//     inc s
// 
//     // PLEX_SORTED_IDX[s] = nxt_idx;
//     ldx s
//     lda nxt_idx
//     sta PLEX_SORTED_IDX,x
//   !endOfIfThen:
//     // for-loop if m <= PLEX_COUNT - 2
//     inc m
//     lda m
//     cmp #PLEX_COUNT-1
//     beq !+
//     // not finished for loop yet...
//     jmp !forLoop-
//   !:
//     lda #0
//     sta plex_show_idx
//     sta plex_sprite_idx
//     lda #1
//     sta plex_sprite_msb
//     eor #$ff
//     sta plex_sprite_mask
// 
//     jsr plexFreePrepare._main_
//   end;
end;

// Show the next sprite.
// plexSort() prepares showing the sprites
procedure plexShowSprite();
var
  plex_sprite_idx2 : Byte;
  xpos_idx         : Byte;
  ypos             : Byte;
  sx               : word;
  sortedIDX        : Byte;
begin
  plex_sprite_idx2 := plex_sprite_idx*2;
  ypos := PLEX_YPOS[PLEX_SORTED_IDX[plex_show_idx]];
  vic.spr0_y[plex_sprite_idx2] := ypos;
  plexFreeAdd(ypos);
  PLEX_SCREEN_PTR^[plex_sprite_idx] := PLEX_PTR[PLEX_SORTED_IDX[plex_show_idx]];
  xpos_idx := PLEX_SORTED_IDX[plex_show_idx];
  vic.spr0_x[plex_sprite_idx2] := PLEX_XPOS[xpos_idx];

  //--------------------------------------------    
  //--------------------------------------------    
  // new sprite stuff
  vic_spr_color[plex_sprite_idx] := PLEX_COLOR[xpos_idx];

  if(PLEX_MULTICOLOR[xpos_idx] <> 0) then
    vic.spr_mcolor := vic.spr_mcolor or plex_sprite_msb
  else
    vic.spr_mcolor := vic.spr_mcolor and plex_sprite_mask;

  if(PLEX_ENABLED[xpos_idx] <> 0) then
    vic.spr_ena := vic.spr_ena or plex_sprite_msb
  else
    vic.spr_ena := vic.spr_ena and plex_sprite_mask;

  //--------------------------------------------    
  //--------------------------------------------    

  sx := PLEX_XPOS[xpos_idx];
  if(sx.b1 <> 0) then
    vic.spr_hi_x := vic.spr_hi_x or plex_sprite_msb
  else
    vic.spr_hi_x := vic.spr_hi_x and plex_sprite_mask;

  plex_sprite_idx := (plex_sprite_idx+1) and 7;
  Inc(plex_show_idx);
  plex_sprite_msb := plex_sprite_msb shl 1;
  if(plex_sprite_msb = 0) then
      plex_sprite_msb := 1;
      
  plex_sprite_mask := $ff xor plex_sprite_msb;

//     asm
// //     plex_sprite_idx2 := plex_sprite_idx*2;
// //     sortedIDX := PLEX_SORTED_IDX[plex_show_idx];
// 
//       lda plex_sprite_idx
//       asl
//       sta plex_sprite_idx2
// 
//       ldx plex_show_idx
//       lda PLEX_SORTED_IDX,x
//       sta sortedIDX
// 
// 
// //    ypos := PLEX_YPOS[sortedIDX];
//       tax
//       lda PLEX_YPOS,x
//       sta ypos
// 
//     // SPRITES_YPOS[plex_sprite_idx2] = ypos;
//       ldx plex_sprite_idx2
//       lda ypos
//       sta vic_spr0_y,x
// 
//       lda ypos
//       jsr plexFreeAdd._main_
// end;
//      plexFreeAdd(ypos);
// asm
// 
//     // PLEX_SCREEN_PTR[plex_sprite_idx] = PLEX_PTR[sortedIDX];
//     // xpos_idx = sortedIDX;
//     // SPRITES_XPOS[plex_sprite_idx2] = (char)PLEX_XPOS[xpos_idx];
// 
//       ldx sortedIDX
//       lda PLEX_PTR,x
//       ldy plex_sprite_idx
//       sta (PLEX_SCREEN_PTR),y
// 
//       lda sortedIDX
//       sta xpos_idx
// 
//       // * 2 for access to PLEX_XPOS word-sized array
//       asl
//       tax
//       ldy plex_sprite_idx2
//       lda PLEX_XPOS,x
//       sta vic_spr0_x,y
// 
//     //--------------------------------------------
//     //--------------------------------------------    
//     // new sprite stuff
// //    vic_spr0_color[plex_sprite_idx] := PLEX_COLOR[xpos_idx];
//     
//       ldx xpos_idx
//       ldy plex_sprite_idx
//       
//       lda PLEX_COLOR,x
//       sta vic_spr0_color,y
// 
// //     if(PLEX_MULTICOLOR[xpos_idx] <> 0) then
// //       vic_spr_mcolor |= plex_sprite_msb
// //     else
// //       vic_spr_mcolor &= plex_sprite_mask;
// 
//        ldx xpos_idx
//        lda PLEX_MULTICOLOR,x
//        bne !isSet+
//        lda vic_spr_mcolor
//        and plex_sprite_mask
//        jmp !+
//      !isSet:
//        lda vic_spr_mcolor
//        ora plex_sprite_msb
//      !:
//        sta vic_spr_mcolor
// 
// //     if(PLEX_ENABLED[xpos_idx]!=0) {
// //       VICII->SPRITES_ENABLE |= plex_sprite_msb;
// //     } else {
// //       VICII->SPRITES_ENABLE &= plex_sprite_mask;
// //     }
//       ldx xpos_idx
//       lda PLEX_ENABLED,x
//       bne !isSet+
//       lda vic_spr_ena
//       and plex_sprite_mask
//       jmp !+
//     !isSet:
//       lda vic_spr_ena
//       ora plex_sprite_msb
//     !:
//       sta vic_spr_ena
//     //--------------------------------------------
//     //--------------------------------------------    
// 
//     // if(BYTE1(PLEX_XPOS[xpos_idx])!=0) {
//     //     *SPRITES_XMSB |= plex_sprite_msb;
//     // } else {
//     //     *SPRITES_XMSB &= plex_sprite_mask;
//     // }
// 
//       lda xpos_idx
//       // * 2 for access to PLEX_XPOS word-sized array
//       asl
//       tax
//       lda PLEX_XPOS + 1,x
//       bne !isSet+
//       lda vic_spr_hi_x
//       and plex_sprite_mask
//       jmp !+
//     !isSet:
//       lda vic_spr_hi_x
//       ora plex_sprite_msb
//     !:
//       sta vic_spr_hi_x
// 
//     // plex_sprite_idx = (plex_sprite_idx + 1 ) & 7;
//     // plex_show_idx++;
//       inc plex_sprite_idx
//       lda plex_sprite_idx
//       and #7
//       sta plex_sprite_idx
// 
//       inc plex_show_idx
// 
//     // plex_sprite_msb <<= 1;
//     // if(plex_sprite_msb == 0) {
//     //     plex_sprite_msb = 1;
//     // }
//       lda plex_sprite_msb
//       cmp #128
//       rol
//       sta plex_sprite_msb
// 
// //    plex_sprite_mask = 0xff ^ plex_sprite_msb;
//       eor #$ff
//       sta plex_sprite_mask
//     end;
end;
 
// Get the y-position of the next sprite to show
function plexShowNextYpos() : Byte;
begin
//    Result := PLEX_YPOS[PLEX_SORTED_IDX[plex_show_idx]];
asm
  ldx plex_show_idx
  lda PLEX_SORTED_IDX,x
  tax
  lda PLEX_YPOS,x
  sta result
end;
end;

// wait for the Y-position where the next sprite to be shown is free to use
procedure plexWaitForNextYRaster(); assembler;
// var
//   rasterY : Byte;
// begin
//    rasterY := PLEX_FREE_YPOS[plex_free_next];
// 
//    while (vic_raster < rasterY) do;
// end;
asm
  ldx plex_free_next
  lda PLEX_FREE_YPOS
!:
  cmp vic.raster
  bcc !-
end;

