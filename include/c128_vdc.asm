//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------
//
// adapted from:
// https://gitlab.com/aaron-baugher/6502-assembly-language-programming/-/blob/master/vdc80lib.a?ref_type=heads

#importonce

vdcInitColors: {
  lda #0; sta vdc_color.black
  lda #1; sta vdc_color.dark_gray
  lda #2; sta vdc_color.dark_blue
  lda #3; sta vdc_color.light_blue
  lda #4; sta vdc_color.dark_green
  lda #5; sta vdc_color.light_green
  lda #6; sta vdc_color.dark_cyan
  lda #7; sta vdc_color.light_cyan
  lda #8; sta vdc_color.dark_red
  lda #9; sta vdc_color.light_red
  lda #$a; sta vdc_color.dark_purple
  lda #$b; sta vdc_color.light_purple
  lda #$c; sta vdc_color.dark_yellow
  lda #$d; sta vdc_color.light_yellow
  lda #$e; sta vdc_color.light_grey
  lda #$f; sta vdc_color.white
  rts
}