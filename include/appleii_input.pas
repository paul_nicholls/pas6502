//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit appleii_input;
// addapted from here:
// https://www.appleoldies.ca/azgraphics33/keyclr.c.txt
// https://www.appleoldies.ca/azgraphics33/kbhit.c.txt

function kbhit() : Byte;
var 
  KP : Byte absolute $c000;
  c  : Byte;
begin
//  char *KP = (char*)0xC000;
//  char c;

  // read the keyboard buffer    
  // and return 0 if no character is waiting 

  Result := 0;

  c := KP;
   
  if (c < 128) then Exit;
   
  Result := c;
end;

procedure clearkey();
var
  KEYPRESS : Byte absolute $c000; // return the last key press
  KEYCLEAR : Byte absolute $c010; // clear the last key press
begin
  // clear stragglers from the keyboard buffer 
  while(KEYPRESS > 127) do KEYCLEAR := 0;
end;