#importonce

.macro vic_clearScreen(_screen, clearByte) {
	lda #clearByte
	ldx #0
!loop:
	sta _screen + $000, x
	sta _screen + $100, x
	sta _screen + $200, x
	sta _screen + $300, x
	inx
	bne !loop-
}

.macro vic_38_columns() {
  // enable 38-column mode
  lda $d016
  and #%11110111
  sta $d016
  //
}

.macro vic_40_columns() {
  // enable 40-column mode
  lda $d016
  ora #%00001000
  sta $d016
  //
}

.macro vic_24_rows() {
  // enable 24-row mode
  lda $D011
  and #%11110111
  sta $D011
  //
}

.macro vic_25_rows() {
  // enable 24-row mode
  lda $D011
  ora #%00001000
  sta $D011
  //
}

.macro vic_setVideoBank(_VideoBank) {
/*
Address	Mask	Bank	Yields
DD00	%xxxxxx11	bank0	0000 - 3FFF
DD00	%xxxxxx10	bank1	4000 - 7FFF
DD00	%xxxxxx01	bank2	8000 - BFFF
DD00	%xxxxxx00	bank3	C000 - FFFF
*/
  lda $DD00
  and #%11111100
  ora #((_VideoBank - 3) & %00000011)
  sta $DD00
}
                    
//
// Enter hires bitmap mode (a.k.a. standard bitmap mode)
//
.macro vic_setHiresBitmapMode() {
  //
  // Clear extended color mode (bit 6) and set bitmap mode (bit 5)
  //
  lda $d011
  and #%10111111
  ora #%00100000
  sta $d011

  //
  // Clear multi color mode (bit 4)
  //
  lda $d016
  and #%11101111
  sta $d016
}

.macro vic_setScreenMemory(address) {
  //
  // The most significant nibble of $D018 selects where the screen is
  // located in the current VIC-II bank.
  .var bits = (address / $0400) << 4

  lda $d018
  and #%00001111
  ora #bits
  sta $d018
}

.macro vic_setScreenAndCharsetLocation(_screen,_charset) {
/*
Address	Mask	Bank	Yields
DD00	%xxxxxx11	bank0	0000 - 3FFF
DD00	%xxxxxx10	bank1	4000 - 7FFF
DD00	%xxxxxx01	bank2	8000 - BFFF
DD00	%xxxxxx00	bank3	C000 - FFFF
*/
  .var videoBank = 0;
  .var _setVideoBank = true;

  .var VIDEO_BANK_0_START = $0000
  .var VIDEO_BANK_0_END   = $3FFF

  .var VIDEO_BANK_1_START = $4000
  .var VIDEO_BANK_1_END   = $7FFF

  .var VIDEO_BANK_2_START = $8000
  .var VIDEO_BANK_2_END   = $BFFF

  .var VIDEO_BANK_3_START = $C000
  .var VIDEO_BANK_3_END   = $FFFF

  .if ((_screen >= VIDEO_BANK_0_START) && (_screen < VIDEO_BANK_0_END)) .eval videoBank = 0;
  .if ((_screen >= VIDEO_BANK_1_START) && (_screen < VIDEO_BANK_1_END)) .eval videoBank = 1;
  .if ((_screen >= VIDEO_BANK_2_START) && (_screen < VIDEO_BANK_2_END)) .eval videoBank = 2;
  .if ((_screen >= VIDEO_BANK_3_START) && (_screen < VIDEO_BANK_3_END)) .eval videoBank = 3;

  .if ((_charset >= VIDEO_BANK_0_START) && (_charset < VIDEO_BANK_0_END)) .eval videoBank = 0;
  .if ((_charset >= VIDEO_BANK_1_START) && (_charset < VIDEO_BANK_1_END)) .eval videoBank = 1;
  .if ((_charset >= VIDEO_BANK_2_START) && (_charset < VIDEO_BANK_2_END)) .eval videoBank = 2;
  .if ((_charset >= VIDEO_BANK_3_START) && (_charset < VIDEO_BANK_3_END)) .eval videoBank = 3;

  .if (_setVideoBank)
    :vic_setVideoBank(videoBank)

  // set the screen and character set locations
  // (relative to the current bank)
  lda #[[_screen & $3FFF] / 64] | [[_charset & $3FFF] / 1024]
  sta $D018
}


