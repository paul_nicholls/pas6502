//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    this file was translated from: https://github.com/KarolS/millfork
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit c64_kernal;

// Standard Kernal ROM routines
  procedure CHKIN(x : Byte)                external $ffc6; // CHKIN. Define file as default input. (Must call OPEN beforehand.)
  procedure CHKOUT(x : Byte)               external $ffc9; // CHKOUT. Define file as default output. (Must call OPEN beforehand.)
  procedure CHRIN                          external $ffcf; // CHRIN. Read byte from default input (for keyboard, read a line from the screen). (If not keyboard, must call OPEN and CHKIN beforehand.)
  procedure CHROUT(a : Byte)               external $ffd2; // CHROUT. Write byte to default output. (If not screen, must call OPEN and CHKOUT beforehand.)
  procedure CLALL                          external $ffe7; // CLALL. Clear file table; call CLRCHN.
  procedure CLOSE(a : Byte)                external $ffc3; // CLOSE. Close file.
  procedure CLRCHN                         external $ffcc; // CLRCHN. Close default input/output files (for serial bus, send UNTALK and/or UNLISTEN); restore default input/output to keyboard/screen.
  procedure GETIN                          external $ffe4; // GETIN. Read byte from default input. (If not keyboard, must call OPEN and CHKIN beforehand.)
  procedure IECIN                          external $ffa5; // IECIN. Read byte from serial bus. (Must call TALK and TALKSA beforehand.)
  procedure IECOUT(a : Byte)               external $ffa8; // IECOUT. Write byte to serial bus. (Must call LISTEN and LSTNSA beforehand.)
  procedure IOBASE                         external $fff3; // IOBASE. Fetch CIA #1 base address.
  procedure IOINIT                         external $ff84; // IOINIT. Initialize CIA's, SID volume; setup memory configuration; set and start interrupt timer.
  procedure LISTEN(a : Byte)               external $ffb1; // LISTEN. Send LISTEN command to serial bus.
  procedure LOAD                           external $ffd5; // LOAD. Load or verify file. (Must call SETLFS and SETNAM beforehand.)
  procedure LSTNSA(a : Byte)               external $ff93; // LSTNSA. Send LISTEN secondary address to serial bus. (Must call LISTEN beforehand.)
  procedure MEMBOT                         external $ff99; // MEMBOT. Save or restore start address of BASIC work area.
  procedure MEMTOP                         external $ff9c; // MEMTOP. Save or restore end address of BASIC work area.
  procedure OPEN                           external $ffc0; // OPEN. Open file. (Must call SETLFS and SETNAM beforehand.)
  procedure PLOT(x,y : Byte)               external $fff0; // PLOT. Save or restore cursor position.
  procedure RAMTAS                         external $ff87; // RAMTAS. Clear memory addresses $0002-$0101 and $0200-$03FF; run memory test and set start and end address of BASIC work area accordingly; set screen memory to $0400 and datasette buffer to $033C.
  procedure RDTIM                          external $ffde; // RDTIM. read Time of Day, at memory address $00A0-$00A2.
  procedure READST                         external $ffb7; // READST. Fetch status of current input/output device, value of ST variable. (For RS232, status is cleared.)
  procedure RESTOR                         external $ff8a; // RESTOR. Fill vector table at memory addresses $0314-$0333 with default values.
  procedure SAVE(a : Byte; yx : Integer)   external $ffd8; // SAVE. Save file. (Must call SETLFS and SETNAM beforehand.)
  procedure SCINIT                         external $ff81; // SCINIT. Initialize VIC; restore default input/output to keyboard/screen; clear screen; set PAL/NTSC switch and interrupt timer.
  procedure SCNKEY                         external $ff9f; // SCNKEY. Query keyboard; put current matrix code into memory address $00CB, current status of shift keys into memory address $028D and PETSCII code into keyboard buffer.
  procedure SCREEN                         external $ffed; // SCREEN. Fetch number of screen rows and columns.
  procedure SETLFS(a,x,y : Byte)           external $ffba; // SETLFS. Set file parameters.
  procedure SETMSG(a : Byte)               external $ff90; // SETMSG. Set system error display switch at memory address $009D.
  procedure SETNAM(a : Byte; yx : Integer) external $ffbd; // SETNAM. Set file name parameters.
  procedure SETTIM(a,x,y : Byte)           external $ffdb; // SETTIM. Set Time of Day, at memory address $00A0-$00A2.
  procedure SETTMO                         external $ffa2; // SETTMO. Unknown. (Set serial bus timeout.)
  procedure STOP                           external $ffe1; // STOP. Query Stop key indicator, at memory address $0091; if pressed, call CLRCHN and clear keyboard buffer.
  procedure TALK(a : Byte)                 external $ffb4; // TALK. Send TALK command to serial bus.
  procedure TALKSA(a : Byte)               external $ff96; // TALKSA. Send TALK secondary address to serial bus. (Must call TALK beforehand.)
  procedure UDTIM                          external $ffea; // UDTIM. Update Time of Day, at memory address $00A0-$00A2, and Stop key indicator, at memory address $0091.
  procedure UNLSTN                         external $ffae; // UNLSTN. Send UNLISTEN command to serial bus.
  procedure UNTALK                         external $ffab; // UNTALK. Send UNTALK command to serial bus.
  procedure VECTOR                         external $ff8d; // VECTOR. Copy vector table at memory addresses $0314-$0333 from or into user table.
