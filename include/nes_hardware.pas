//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit nes_hardware;

const
  CTRL_NT_2000   = %00000000;
  CTRL_NT_2400   = %00000001;
  CTRL_NT_2800   = %00000010;
  CTRL_NT_2c00   = %00000011;
  CTRL_INC_1     = %00000000;
  CTRL_INC_32    = %00000100;
  CTRL_SPR_0000  = %00000000;
  CTRL_SPR_1000  = %00001000;
  CTRL_BG_0000   = %00000000;
  CTRL_BG_1000   = %00010000;
  CTRL_SPR_8x8   = %00000000;
  CTRL_SPR_8x16  = %00100000;
  CTRL_NMI_OFF   = %00000000;
  CTRL_NMI_ON    = %10000000;

  MASK_COLOR     = %00000000;
  MASK_BW        = %00000001;
  MASK_HIDE_BG8  = %00000000;
  MASK_BG8       = %00000010;
  MASK_HIDE_SP8  = %00000000;
  MASK_SP8       = %00000100;
  MASK_BG_OFF    = %00000000;
  MASK_BG_ON     = %00001000;
  MASK_SP_OFF    = %00000000;
  MASK_SP_ON     = %00010000;
  MASK_EM_RED    = %00100000;
  MASK_EM_GREEN  = %01000000;
  MASK_EM_BLUE   = %10000000;

type
  TOAM = record
     address    : Byte absolute $2003;
     data       : Byte absolute $2004;
     dma        : Byte absolute $4014;
     buffer     : Byte absolute $700;
  end;

  TPPU = record
     control    : Byte absolute $2000;
     mask       : Byte absolute $2001;
     status     : Byte absolute $2002;
     scroll     : Byte absolute $2005;
     address    : Byte absolute $2006;
     data       : Byte absolute $2007;
     framecount : Byte absolute $4017;
  end;

  TIO = record
   square1_vol     : Byte absolute $4000;
   square1_sweep   : Byte absolute $4001;
   square1_lo      : Byte absolute $4002;
   square1_hi      : Byte absolute $4003;
   square2_vol     : Byte absolute $4004;
   square2_sweep   : Byte absolute $4005;
   square2_lo      : Byte absolute $4006;
   square2_hi      : Byte absolute $4007;
   triangle_linear : Byte absolute $4008;
   triangle_lo     : Byte absolute $400a;
   triangle_hi     : Byte absolute $400b;
   noise_linear    : Byte absolute $400c;
   noise_lo        : Byte absolute $400e;
   noise_hi        : Byte absolute $400f;

   dmc_freq        : Byte absolute $4010;
   dmc_raw         : Byte absolute $4011;
   dmc_start       : Byte absolute $4012;
   dmc_length      : Byte absolute $4013;
   oam_dma         : Byte absolute $4014;
   sound_channels  : Byte absolute $4015;
   input_0         : Byte absolute $4016;
   input_1         : Byte absolute $4017;
  end;

var
  joypad1_ctrl : Byte absolute $4016;
  joypad2_ctrl : Byte absolute $4017;
  
  PPU : TPPU;
  OAM : TOAM;
  IO  : TIO;

macro NMI_START; assembler;
asm
  pha
  txa
  pha
  tya
  pha
end;

macro NMI_END; assembler;
asm
  pla
  tay
  pla
  tax
  pla
  rti
end;

macro SPRITE_SETX_VV(SPRITE,VALUE); assembler;
asm
  lda SPRITE
  asl
  asl
  tax
  lda VALUE
  sta $0203,x
end;

macro SPRITE_SETY_VV(SPRITE,VALUE); assembler;
asm
  lda SPRITE
  asl
  asl
  tax
  lda VALUE
  sta $0200,x
end;

macro VWAIT; assembler;
asm
_vblankwait1:
  bit PPU.status
  bpl _vblankwait1
end;

macro OAM_SETADDR(address); assembler;
asm
  lda #<address
  sta OAM.address
  lda #>address
  sta OAM.address
end;

macro PPU_SETADDR(address); assembler;
asm
  lda #>address
  sta PPU.address
  lda #<address
  sta PPU.address
end;

procedure strobe_joypad; assembler;
asm
  lda #1
  sta joypad1_ctrl
  lsr
  sta joypad1_ctrl
end;

function read_joypad1 : Byte; assembler;
asm
  lda joypad1_ctrl
end;

function read_joypad2 : Byte; assembler;
asm
  lda joypad2_ctrl
end;

procedure initRAM; assembler;
asm
  sei               // ignore IRQs
  cld               // disable decimal mode
  ldx #$40
  stx IO.input_1    // disable APU frame IRQ

  //  set stack
  ldx #$3f
  txs

  ldx #$00
  stx PPU.control   // disable NMI
  stx PPU.mask      // disable rendering
  stx IO.dmc_freq   // disable DMC IRQs

  bit PPU.status

  :VWAIT()
    // We now have about 30,000 cycles to burn before the PPU stabilizes.
    // One thing we can do with this time is put RAM in a known state.
  ldx #$00
  lda #$00
!clrmem:
  sta $000,x
  sta $100,x
  sta $200,x
  sta $300,x
  sta $400,x
  sta $500,x
  sta $600,x
  txa
  sta $700,x
  inx
  bne !clrmem-
end;
