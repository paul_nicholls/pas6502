unit 6502_rtl;

procedure poke(ax : Word; y : Byte); assembler; volatile;
asm
  stx $fb + 0
  sta $fb + 1
  ldy #0
  lda y
  sta ($fb),y
end;

function  peek(ax : Word) : Byte; assembler; volatile;
asm
  stx $fb + 0
  sta $fb + 1
  ldy #0
  lda ($fb),y
end;
