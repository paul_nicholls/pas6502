//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit c64_sid;

const SID_BASE = $d400;

type
  TSIDvoice = record
    fq        : Word;
    pw        : Word;
    wave_form : Byte;
    atk_dec   : Byte;
    sus_rel   : Byte;
  end;
  
  PSIDvoice = ^TSIDvoice;
  
  TSID = record
    voice1 : TSIDvoice;
    voice2 : TSIDvoice;
    voice3 : TSIDvoice;
    
    fc_lo    : Byte;  
    fc_hi    : Byte; 
    res_filt : Byte; 
    vol_mode : Byte;
    paddle_x : Byte;
    paddle_y : Byte;
    osc3     : Byte;
    env3     : Byte;
  end;

var
  SID : TSID absolute SID_BASE;

const sid_v1       = $D400;
// const word sid_v1_freq  = $D400;
// const word sid_v1_pulse = $D402;
// const word sid_v1_cr    = $D404;
// const word sid_v1_ad    = $D405;
// const word sid_v1_sr    = $D409;
//
const sid_v2       = $D407;
// const word sid_v2_freq  = $D407;
// const word sid_v2_pulse = $D409;
// const word sid_v2_cr    = $D40B;
// const word sid_v2_ad    = $D40C;
// const word sid_v2_sr    = $D40D;
//
const sid_v3       = $D40E;
// const word sid_v3_freq  = $D40E;
// const word sid_v3_pulse = $D410;
// const word sid_v3_cr    = $D412;
// const word sid_v3_ad    = $D413;
// const word sid_v3_sr    = $D414;
//
// const word sid_paddle_x = $D419;
// const word sid_paddle_y = $D41A;

// attack decay release values
const attack_2mSec   = 0;
const attack_8mSec   = 1;
const attack_16mSec  = 2;
const attack_24mSec  = 3;
const attack_38mSec  = 4;
const attack_56mSec  = 5;
const attack_68mSec  = 6;
const attack_80mSec  = 7;
const attack_100mSec = 8;
const attack_250mSec = 9;
const attack_500mSec = 10;
const attack_800mSec = 11;
const attack_1Sec    = 12;
const attack_3Sec    = 13;
const attack_5Sec    = 14;
const attack_8Sec    = 15;

const decay_6mSec    = 0;
const decay_14mSec   = 1;
const decay_48mSec   = 2;
const decay_72mSec   = 3;
const decay_114mSec  = 4;
const decay_168mSec  = 5;
const decay_204mSec  = 6;
const decay_240mSec  = 7;
const decay_300mSec  = 8;
const decay_750mSec  = 9;
const decay_1500mSec = 10;
const decay_2400mSec = 11;
const decay_3Sec     = 12;
const decay_9Sec     = 13;
const decay_15Sec    = 14;
const decay_24Sec    = 15;

const release_6mSec    = 0;
const release_14mSec   = 1;
const release_48mSec   = 2;
const release_72mSec   = 3;
const release_114mSec  = 4;
const release_168mSec  = 5;
const release_204mSec  = 6;
const release_240mSec  = 7;
const release_300mSec  = 8;
const release_750mSec  = 9;
const release_1500mSec = 10;
const release_2400mSec = 11;
const release_3Sec     = 12;
const release_9Sec     = 13;
const release_15Sec    = 14;
const release_24Sec    = 15;

const WAVE_TRIANGLE  = %0001 shl 4;
const WAVE_SAW       = %0010 shl 4;
const WAVE_RECTANGLE = %0100 shl 4;
const WAVE_NOISE     = %1000 shl 4;

const GATE_BIT_ON    = %00000001;
const GATE_BIT_OFF   = %11111110;

const voice_frequency       = 0;
const voice_pulse_width     = 2;
const voice_control         = 4;
const voice_attack_decay    = 5;
const voice_sustain_release = 6;

const TIMER_VOICE = sid_v3;
const SFX_VOICE   = sid_v3;

procedure sid_enableVoice(sid_voice : Word; wave_type : Byte);
begin
  poke(sid_voice + voice_control,wave_type or GATE_BIT_ON); 
end;

procedure sid_disableVoice(sid_voice : Word; wave_type : Byte); 
begin
  poke(sid_voice + voice_control,wave_type and GATE_BIT_OFF); 
end;

procedure sid_setAttackDecay( attack, decay : byte; sid_voice : word);
begin
  poke(sid_voice + voice_attack_decay,(attack  shl  4) or decay);
end;

procedure sid_setSustainRelease( sustain, release : byte; sid_voice : word);
begin
  poke(sid_voice + voice_sustain_release,(sustain  shl  4) or release);
end;

procedure sid_clearRegisters(); assembler;
asm
  ldx #25
  lda #0
loop:
  sta SID_BASE - 1, x
  dex
  bne loop
end;

procedure sid_setVolume(a : Byte); assembler;
asm
  and #%1111
  sta SID_BASE + 24
end;

