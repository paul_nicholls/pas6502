readJoysticksInput: {
  jsr resetJoysticks
  jsr checkJoysticks
  rts
}

joysticks: {
  up:     .byte 0
  down:   .byte 0
  left:   .byte 0
  right:  .byte 0
  button: .byte 0
  dx:     .byte 0
  dy:     .byte 0
}

resetJoysticks: {
  lda #0
  sta joysticks.up
  sta joysticks.down
  sta joysticks.left
  sta joysticks.right
  sta joysticks.button
  sta joysticks.dx
  sta joysticks.dy
  rts
}

checkJoysticks: {
  // disable keyboard
  lda #224
  sta 56322

  lda $dc01          //read joystick port 1
  and $dc00          //and with joystick port 2
  lsr                //get switch bits
  ror joysticks.up   //switch_history = switch_history/2 + 128*current_switch_state
  lsr                //update the other switches' history the same way
  ror joysticks.down
  lsr
  ror joysticks.left
  lsr
  ror joysticks.right
  lsr
  ror joysticks.button

  // re-enable keyboard
  lda #255
  sta 56322

  rts
}
