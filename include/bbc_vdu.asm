//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

#importonce

//.const OSWRCH = $FFEE

.macro vdu_mode(value) {
  lda #22
  jsr OSWRCH
  lda #value
  jsr OSWRCH
}

.macro vdu_plot(mode,x,y) {
  lda #25
  jsr OSWRCH
  lda #mode
  jsr OSWRCH
  lda #<x
  jsr OSWRCH
  lda #>x
  jsr OSWRCH
  lda #<y
  jsr OSWRCH
  lda #>y
  jsr OSWRCH
}

.macro vdu_plot_vv(mode,x,y) {
// v = word variable source
  lda #25
  jsr OSWRCH
  lda #mode
  jsr OSWRCH
  lda x + 0
  jsr OSWRCH
  lda x + 1
  jsr OSWRCH
  lda y + 0
  jsr OSWRCH
  lda y + 1
  jsr OSWRCH
}

.macro vdu_move(x,y) {
  lda #25
  jsr OSWRCH
  lda #4 // code for move
  jsr OSWRCH
  lda #<x
  jsr OSWRCH
  lda #>x
  jsr OSWRCH
  lda #<y
  jsr OSWRCH
  lda #>y
  jsr OSWRCH
}

.macro vdu_move_vv(x,y) {
// v = word variable source
  lda #25
  jsr OSWRCH
  lda #4 // code for move
  jsr OSWRCH
  lda x + 0
  jsr OSWRCH
  lda x + 1
  jsr OSWRCH
  lda y + 0
  jsr OSWRCH
  lda y + 1
  jsr OSWRCH
}

.macro vdu_draw(x,y) {
  lda #25
  jsr OSWRCH
  lda #5 // code for draw
  jsr OSWRCH
  lda #<x
  jsr OSWRCH
  lda #>x
  jsr OSWRCH
  lda #<y
  jsr OSWRCH
  lda #>y
  jsr OSWRCH
}

.macro vdu_draw_vv(x,y) {
// v = word variable source
  lda #25
  jsr OSWRCH
  lda #5 // code for draw
  jsr OSWRCH
  lda x + 0
  jsr OSWRCH
  lda x + 1
  jsr OSWRCH
  lda y + 0
  jsr OSWRCH
  lda y + 1
  jsr OSWRCH
}

.macro vdu_19(logical,physical) {
  lda #19
  jsr OSWRCH
  lda #logical
  jsr OSWRCH
  lda #physical
  jsr OSWRCH
  lda #0
  jsr OSWRCH
  jsr OSWRCH
  jsr OSWRCH
}