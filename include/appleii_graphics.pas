//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit appleii_graphics;
// adapted from here:
// https://www.appleoldies.ca/azgraphics33/g3.h.txt

uses
  appleii_call;
  
const
  TEXTMODE = 0;
  LORES    = 1;
  GRAFMODE = 2;  // graphics only
  DHRMODE  = 3;  // double hi-res graphics
  DLORES   = 4;  // double lo-res graphics
  TEXT80   = 5;  // 80 column textmode
  
  LOBLACK         = 0;
  LORED           = 1;
  LODKBLUE        = 2;
  LOPURPLE        = 3;
  LODKGREEN       = 4;
  LOGRAY          = 5;
  LOMEDBLUE       = 6;
  LOLTBLUE        = 7;
  LOBROWN         = 8;
  LOORANGE        = 9;
  LOGREY          = 10;
  LOPINK          = 11;
  LOLTGREEN       = 12;
  LOYELLOW        = 13;
  LOAQUA          = 14;
  LOWHITE         = 15;


procedure PAGEONE;
begin
  poke(49236,0);
end;

procedure PAGETWO;
begin
  poke(49237,0);
end;

procedure setcrtmode(crtMode : Byte);
begin
  if crtMode = TEXT80 then 
    loclear()
  else
  if crtMode = TEXTMODE then begin // unset double hires-lores
    poke(49247,0);
    poke(49233,0);
    PAGEONE;
    if (crtMode = TEXTMODE) then Exit;
    crt80();
    Exit;
  end
  else
  if crtMode = DLORES then begin
    // set double lores
    poke(49246,0);
  end
  else
  if crtMode = LORES then begin
    poke(49238,0);   // lo res
    PAGEONE;
    poke(49232,0);   // set graphics
    poke(49234,0);   // full graphics
    loclear();
  end
  else
  if crtMode = DHRMODE then
    // set double hires
    poke(49246,0)
  else begin
//    case GRAFMODE:
//    default  :
    poke(49239,0);   // hi res
    PAGEONE();
    poke(49232,0);   // set graphics
    poke(49234,0);   // full graphics
  end;
end;