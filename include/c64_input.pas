//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    this file was translated (and modified) from: 
//    https://github.com/KarolS/millfork
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit c64_input;

procedure getTextInput(textFilter,textOutput : Pointer; inputLimit : Byte); assembler;
//======================================================================
// Input a string and store it in textOutput (with length received in textOutput[0].
// textFilter is a pointer to the allowed list of petscii characters starting at textFilter[1]
// inputLimit is max characters allowed to be entered
//======================================================================

const
  GETIN  = $ffe4;
  CHROUT = $ffd2;
  DELETE = $14;
  RETURN = $0d;
  CURSOR = 228;
  PROMPT = 62;

asm
  // draw prompt
  lda #PROMPT
  jsr CHROUT

  // draw cursor
  lda #CURSOR
  jsr CHROUT
  lda #0
  sta inputCount
getInput:
  jsr GETIN
  beq getInput

  sta lastChar

  cmp #RETURN
  beq finished

  cmp #DELETE
  beq delete
  
  // check for valid chars in textFilter
  ldy textFilter
filterLoop:
  cmp (textFilter),y
  beq isValidChar
  dey
  bne filterLoop
  jmp getInput    // wasn't in the list of approved values so loop back

isValidChar:
  // hit char input limit?
  lda inputCount
  cmp inputLimit
  bcs getInput // hit limit so don't add character

  // not yet
  inc inputCount
  
  // delete cursor
  lda #DELETE
  jsr CHROUT
  // draw new character
  lda lastChar
  jsr CHROUT
  
  // draw cursor
  lda #CURSOR
  jsr CHROUT

  ldy inputCount
  lda lastChar
  sta (textOutput),y
  jmp getInput
delete:
  lda inputCount
  beq getInput // no text so back to input

  // put space at current character in string
  ldy inputCount
  lda #32
  sta (textOutput),y

  // rewind input count
  dec inputCount
  // delete cursor
  lda #DELETE
  jsr CHROUT

  // delete last char
  lda #DELETE
  jsr CHROUT

  // draw cursor
  lda #CURSOR
  jsr CHROUT
  jmp getInput
finished:
  ldy #0
  lda inputCount
  clc
  adc #1
  sta (textOutput),y
  rts
  jmp getInput
//------------------------------------------
lastChar:
  .byte $00

inputCount:
  .byte $00
end;

function waitForChar(textFilter : Pointer) : Byte; assembler;
//======================================================================
// waits for a keypress that matches the textFilter string and returns int in A.
// textFilter is a pointer to the allowed list of petscii characters starting at textFilter[1]
//======================================================================
const
  GETIN  = $ffe4;
  HEX    : String = '0123456789abcdef';
  SCREEN = $c000 + 2048;

asm
getInput:
  jsr GETIN
  beq getInput

  sta lastChar

  // check for valid chars in textFilter
  ldy textFilter
filterLoop:
  cmp (textFilter),y
  beq isValidChar
  dey
  bne filterLoop
  jmp getInput    // wasn't in the list of approved values so loop back

isValidChar:
  // return found keypress
  lda lastChar
  rts
//------------------------------------------
lastChar:
  .byte $00
end;

function waitForAnyKey : Byte; assembler;
//======================================================================
// waits for any keypress and return as result in A
//======================================================================
const
  GETIN  = $ffe4;

asm
getInput:
  jsr GETIN
  beq getInput
end;

function getAnyKey : Byte; assembler;
//======================================================================
// reads keypress and return as result in A
//======================================================================
const
  GETIN  = $ffe4;

asm
  jsr GETIN
end;
