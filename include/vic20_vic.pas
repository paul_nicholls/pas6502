//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit vic20_vic;

type
  TVIC = record
    interlace_originX : Byte; // Interlace mode / Screen origin (horizontal)
    originY           : Byte; // Screen origin (vertical)
    VA9_colCount      : Byte; // Screen memory offset / Number of columns
    rst0_rowCount     : Byte; // Raster value (lowest bit)/ Number of rows / Double character size
    rst1to8           : Byte; // Raster value
    scrCharAddr       : Byte; // Screen memory location / Character memory location
    lightPenX         : Byte; // Light pen position (horizontal)
    lightPenY         : Byte; // Light pen position (vertically)
    paddleX           : Byte; // Paddle 1
    paddleY           : Byte; // Paddle 2
    osc1Freq          : Byte; // Bass sound switch & frequency
    osc2Freq          : Byte; // Alto sound switch & frequency
    osc3Freq          : Byte; // Soprano sound switch & frequency
    noiseFreq         : Byte; // Noise switch & frequency
    auxColor_vol      : Byte; // Auxiliary color / Composite sound volume
    bg_border         : Byte; // Screen color / Reverse mode / Border color
  end;
  
var
  vic : TVIC absolute $9000;
  
const
  black         = 0;
  white         = 1;
  red           = 2;
  cyan          = 3;
  magenta       = 4;
  green         = 5;
  blue          = 6;
  yellow        = 7;

  orange        = 8;
  light_orange  = 9;
  pink          = 10;
  light_cyan    = 11;
  light_magenta = 12;
  light_green   = 13;
  light_blue    = 14;
  light_yellow  = 15;

procedure vic_clearScreen(screen : Pointer; count : Word; clearByte : Byte);
var
  ofs : Word;
begin
  ofs := 0;
  while ofs < count do begin
    screen^ := clearByte;
    Inc(screen);
    Inc(ofs);
  end;
end;

