//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit atari8Bit_graphics;

type
  TScreenInfo = record
    screenRam : Word;
  end;

var
  screenInfo : TScreenInfo;
{
procedure graphicsMode(mode : Byte); assembler;
// A register holds graphics mode
// if mode 0
asm
  lda mode
  cmp #0
  bne !notModeZero+
  lda #$C0 // put MSB of highest RAM
  sta $6A // location :C000 into RAMtop (106)
  jmp !+
  !notModeZero:
  lda #>_origin // get MSB of program starting location
  sta $6A // and store in RAMtop (106).
  !:
  ldx #$60
  lda #$0C
  sta $342,X
  jsr $E456
  lda #>$4000
  sta $2E5
  lda #<$4000
  sta $2E6
  lda #>$2000
  sta $E
  lda #<$2000
  sta $F
  ldx #$60
  lda #$03 //OPEN
  sta $342,X
  lda #>FILEN
  sta $344,X
  lda #<FILEN
  sta $345,X
  lda mode
  sta $34B,X
  and #$F0
  eor #$10
  ora #$0C
  sta $34A,X
  jsr $E456
  bpl GR1
  rts//jmp $9406
FILEN:
  .byte $53 //S
  .byte $3A //:
  .byte $9B
GR1:
  lda 560
  sta TEMP_PNTR
  lda 561
  sta TEMP_PNTR+1
  ldy #4
  lda (TEMP_PNTR),y
  sta screenInfo.screenRam
  iny
  lda (TEMP_PNTR),y
  sta screenInfo.screenRam+1
end;
}
procedure __setDisplayList(dl : Pointer; len : Byte; dlRAM : Word);
var
  sdlstl : Word absolute $0230; // SET DISPLAY LIST ADRESS
  i      : Byte;
begin
  // set display list to point to dlRAM ram
  sdlstl := dlRAM;

  // store display list in ram at sdlstl
  for i := 0 to len - 1 do begin
    poke(dlRAM+i,dl^[i]);
  end;
    
asm
  lda dlRAM
  sta TEMP_PNTR
  lda dlRAM+1
  sta TEMP_PNTR+1
  ldy #4
  lda (TEMP_PNTR),y
  sta screenInfo.screenRam
  iny
  lda (TEMP_PNTR),y
  sta screenInfo.screenRam+1
end;
end;

procedure graphics0;
const
  dlist : array of Byte = (
    112,112,112,66,64,156,2,2,2,2,2,2,2,2,
    2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,65,32,156
  );
begin
  __setDisplayList(@dlist,length(dlist),$2000);//39968);
end;

procedure graphics1_16;
const
  dlist : array of Byte = (
    112,112,112,70,128,157,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,65,96,157,112,112,112,70,128,157,6,6,6,6,6,6,
    6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,65,96,157
  );
begin
  __setDisplayList(@dlist,length(dlist),$2000);//39968);
end;

procedure graphics7_0;
const
  dlist : array of Byte = (
    112,112,112,77,96,144,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
    13,13,13,13,13,13,13,13,13,13,13,13,13,
    13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
    13,13,13,13,13,13,13,13,13,13,13,13,13,
    13,13,13,13,13,13,13,13,13,13,66,96,159,2,2,2,65,162,143
  );
begin
  __setDisplayList(@dlist,length(dlist),$2000);//39968);
end;

procedure graphics7_16;
const
  dlist : array of Byte = (
    112,112,112,77,96,144,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
    13,13,13,13,13,13,13,13,13,13,13,13,13,
    13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
    13,13,13,13,13,13,13,13,13,13,13,13,13,
    13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,
    13,65,152,143
  );
begin
  __setDisplayList(@dlist,length(dlist),$2000);//,39968);
end;

procedure graphics8_16;
const
  dlist : array of Byte = (
    112,112,112,79,80,129,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
    15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
    15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,79,
    0,144,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
    15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,
    15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,65,54,128
  );
begin
  __setDisplayList(@dlist,length(dlist),$2000);//,39968);
end;


