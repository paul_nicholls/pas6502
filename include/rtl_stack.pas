unit rtl_stack;
(*
_STACK_
------------------
$ff	msb        +2
$fe	lsb	       +1
$fd	    <- _sp +0
$fc
$fb
$fa
$f9
$f8
$f7
$f6
$f5
$f4
*)

macro shrinkStack(size); assembler;
asm
  pha
  clc
  lda _sp
  adc #size
  sta _sp
  pla
end;

macro expandStack(size); assembler;
asm
  pha
  sec
  lda _sp
  sbc #size
  sta _sp
  pla
end;

procedure resetSP; assembler;
asm
  lda #$ff
  sta _sp
end;


//----------------------------
// Pushes post-decrement SP
// Pulls pre-increment SP.
//----------------------------
procedure pusha(a : Byte); assembler;
asm
  ldy _sp

  sta _STACK_,y
  dey

  sty _sp
end;

procedure pushax(ax : Word); assembler;
asm
  ldy _sp

  sta _STACK_,y
  dey

  txa
  sta _STACK_,y
  dey

  sty _sp
end;

function  popa : Byte; assembler;
asm
  ldy _sp

  iny
  lda _STACK_,y

  sty _sp
end;

function  popax : Word; assembler;
asm
  ldy _sp

  iny
  ldx _STACK_,y

  iny
  lda _STACK_,y

  sty _sp
end;

procedure pushsp; assembler;
asm
  lda _sp
  jsr pusha
end;

procedure popsp; assembler;
asm
  jsr popa
  sta _sp
end;

function peeka(y : Byte) : Byte; assembler;
// read a from in stack at y-offset
asm
  tya
  clc
  adc _sp
  tay
  lda _STACK_+1,y // read lsb
end;

function peekax(y : Byte) : Word; assembler;
// read ax from in stack at y-offset
asm
  tya
  clc
  adc _sp
  tay
  ldx _STACK_+1,y // read lsb
  lda _STACK_+2,y // read msb
end;

procedure pokea(a,y : Byte); assembler;
// write a to stack at y-offset
asm
  pha
  tya
  clc
  adc _sp
  tay
  pla
  sta _STACK_+1,y // write lsb
end;

procedure pokeax(ax : Word; y : Byte); assembler;
// write ax to stack at y-offset
asm
  pha
  tya
  clc
  adc _sp
  tay
  pla
  sta _STACK_+2,y // write msb
  txa
  sta _STACK_+1,y // write lsb
end;
