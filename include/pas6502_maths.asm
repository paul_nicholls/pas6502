#importonce

.const TRUE  = -1
.const FALSE = 0

.macro copy16(src,dst) {
  lda src + 0
  sta dst + 0
  lda src + 1
  sta dst + 1
}

.macro copy16At(src,dst) {
  // load pointer in dst and store in zero page
  lda dst + 0
  sta $fb + 0
  lda dst +1
  sta $fb + 1

  ldy #0
  // load 16-bit value in src and store at pointer location
  lda src + 0
  sta ($fb),y
  iny
  lda src + 1
  sta ($fb),y
}

.macro set16im(value,dst) {
  lda #<value
  sta dst + 0
  lda #>value
  sta dst + 1
}

.macro set16imAt(value,dst) {
  // load pointer in dst and store in zero page
  lda dst + 0
  sta $fb + 0
  lda dst +1
  sta $fb + 1

  ldy #0
  // load 16-bit value and store at pointer location
  lda #<value
  sta ($fb),y
  iny
  lda #>value
  sta ($fb),y
}

.macro copy8(src,dst) {
  lda src + 0
  sta dst + 0
}

.macro copy8At(src,dst) {
  // load pointer in dst and store in zero page
  lda dst + 0
  sta $fb + 0
  lda dst +1
  sta $fb + 1

  ldy #0
  // load 8-bit value in src and store at pointer location
  lda src + 0
  sta ($fb),y
}

.macro set8im(value,dst) {
  lda #<value
  sta dst + 0
}

.macro set8imAt(value,dst) {
  // load pointer in dst and store in zero page
  lda dst + 0
  sta $fb + 0
  lda dst +1
  sta $fb + 1

  ldy #0
  // load 8-bit value and store at pointer location
  lda #<value
  sta ($fb),y
}

.macro incMem(address) {
  inc address + 0
  bne label
  inc address + 1
label:
}

.macro incMemAt(address) {
  lda address + 0
  sta $fb + 0
  lda address +1
  sta $fb + 1

  clc
  lda ($fb),y
  adc #1
  sta ($fb),y

  iny
  lda ($fb),y
  adc #0
  sta ($fb),y
}

.macro incMem8(address) {
  inc address
}

.macro incMem8At(address) {
  lda address + 0
  sta $fb + 0
  lda address +1
  sta $fb + 1

  ldy #0
  clc
  lda ($fb),y
  adc #1
  sta ($fb),y
}

.macro div8_24bit(address) {
  :div2_24bit(address)
  :div2_24bit(address)
  :div2_24bit(address)
}

.macro div8_16bit(address) {
  ldx #3
!:
  :div2_16bit(address)
  dex
  bne !-
}

.macro div2_24bit(address) {
  lda address + 2      // load the msb
  asl                  // copy the sign bit into c
  ror address + 2      // and back into the msb
  ror address + 1      // rotate the next lsb as normal
  ror address + 0      // rotate the first lsb as normal
}

.macro div2_16bit(address) {
  lda address + 1      // load the msb
  asl                  // copy the sign bit into c
  ror address + 1      // and back into the msb
  ror address + 0      // rotate the lsb as normal
}

.macro twosComplement16bit(address) {
  sec               // ensure carry is set
  lda #0            // load constant zero
  sbc address + 0   // ... subtract the least significant byte
  sta address + 0   // ... and store the result

  lda #0            // load constant zero again
  sbc address + 1   // ... subtract the most significant byte
  sta address + 1   // ... and store the result
}

INTEGER_REGISTER_ADDRESS: .fill 32*2,0

.function getIntRegAddress(regIndex) {
  .return INTEGER_REGISTER_ADDRESS + regIndex * 2
}

.macro clearScreen(_screen, clearByte) {
	lda #clearByte
	ldx #0
!loop:
	sta _screen, x
	sta _screen + $100, x
	sta _screen + $200, x
	sta _screen + $300, x
	inx
	bne !loop-
}

.macro clearScreenIntReg(screenRegIndex, valueRegIndex) {
  .var screenReg = getIntRegAddress(screenRegIndex)
  .var valueReg  = getIntRegAddress(valueRegIndex)

  lda screenReg + 0
  sta $fb + 0
  lda screenReg + 1
  sta $fb + 1

	lda valueReg + 0
	ldx #4 // do 4 x 256 bytes

  ldy #0
!innerloop:
	sta ($fb),y
	iny
	bne !innerloop-

	dex
	beq !finished+
	// increase MSB same as adding $0100 to the $fb index
	inc $fb + 1
	jmp !innerloop-
!finished:
}

.macro loadIntRegIm(value,regIndex) {
  .var dstReg = getIntRegAddress(regIndex)

  lda #<value
  sta dstReg + 0
  lda #>value
  sta dstReg + 1
}

.macro loadIntRegIm8(value,regIndex) {
  .var dstReg = getIntRegAddress(regIndex)

  lda #<value
  sta dstReg + 0
  lda #0
  sta dstReg + 1
}

.macro loadIntRegMem(address,regIndex) {
  .var dstReg = getIntRegAddress(regIndex)

  lda address + 0
  sta dstReg  + 0
  lda address + 1
  sta dstReg  + 1
}

.macro loadIntRegMem8(address,regIndex) {
  .var dstReg = getIntRegAddress(regIndex)

  lda address + 0
  sta dstReg  + 0
  lda #0
  sta dstReg  + 1
}


.macro loadIntRegMemIndirect8(srcIndex,dstIndex) {
  .var srcReg = getIntRegAddress(srcIndex)
  .var dstReg = getIntRegAddress(dstIndex)

  lda srcReg + 0
  sta $fb + 0
  lda srcReg + 1
  sta $fb + 1

  ldy #0
  lda ($fb),y
  sta dstReg + 0
  
  lda #0
  sta dstReg + 1
}


.macro loadIntRegMemAddr8(address,dstIndex) {
  .var dstReg = getIntRegAddress(dstIndex)

  lda address + 0
  sta dstReg  + 0
  lda #0
  sta dstReg  + 1
}

.macro storeIntRegMem(regIndex,addr) {
  .var srcReg = getIntRegAddress(regIndex)

  lda srcReg + 0
  sta addr   + 0
  lda srcReg + 1
  sta addr   + 1
}

.macro storeIntRegMemAt(regIndex,addr) {
  .var srcReg = getIntRegAddress(regIndex)

  lda addr + 0
  sta $fb + 0
  lda addr + 1
  sta $fb + 1

  ldy #0
  lda srcReg + 0
  sta ($fb),y
  iny
  lda srcReg + 1
  sta ($fb),y
}

.macro storeIntRegMem8(regIndex,addr) {
  .var srcReg = getIntRegAddress(regIndex)

  lda srcReg + 0
  sta addr   + 0
}

.macro storeIntRegMem8At(regIndex,addr) {
  .var srcReg = getIntRegAddress(regIndex)

  lda addr + 0
  sta $fb + 0
  lda addr + 1
  sta $fb + 1

  ldy #0
  lda srcReg + 0
  sta ($fb),y
}

.macro storeIntRegMemArray(valueIndex,addr,offsetIndex) {
  .var srcReg = getIntRegAddress(valueIndex)
  .var ofsReg = getIntRegAddress(offsetIndex)

  clc
  lda #<addr
  adc ofsReg + 0
  sta $fb + 0

  lda #>addr
  adc ofsReg + 1
  sta $fb + 1

  ldy #0
  lda srcReg + 0
  sta ($fb),y
  iny
  lda srcReg + 1
  sta ($fb),y
}

.macro storeIntRegMemArrayAt(valueIndex,addr,offsetIndex) {
  .var srcReg = getIntRegAddress(valueIndex)
  .var ofsReg = getIntRegAddress(offsetIndex)

  clc
  lda addr + 0
  adc ofsReg + 0
  sta $fb + 0

  lda addr + 1
  adc ofsReg + 1
  sta $fb + 1

  ldy #0
  lda srcReg + 0
  sta ($fb),y
  iny
  lda srcReg + 1
  sta ($fb),y
}

.macro storeIntRegMemArray8(valueIndex,addr,offsetIndex) {
  .var srcReg = getIntRegAddress(valueIndex)
  .var ofsReg = getIntRegAddress(offsetIndex)

  clc
  lda #<addr
  adc ofsReg + 0
  sta $fb + 0

  lda #>addr
  adc ofsReg + 1
  sta $fb + 1

  ldy #0
  lda srcReg + 0
  sta ($fb),y
}

.macro loadIntRegMemArray8(addr,offsetIndex,dstIndex) {
  .var dstReg = getIntRegAddress(dstIndex)
  .var ofsReg = getIntRegAddress(offsetIndex)

  clc
  lda #<addr
  adc ofsReg + 0
  sta $fb + 0

  lda #>addr
  adc ofsReg + 1
  sta $fb + 1

  lda ($fb),y
  sta dstReg + 0
}

.macro loadIntRegMemArray(addr,offsetIndex,dstIndex) {
  .var dstReg = getIntRegAddress(dstIndex)
  .var ofsReg = getIntRegAddress(offsetIndex)

  clc
  lda #<addr
  adc ofsReg + 0
  sta $fb + 0

  lda #>addr
  adc ofsReg + 1
  sta $fb + 1

  lda ($fb),y
  sta dstReg + 0
  iny
  lda ($fb),y
  sta dstReg + 1
}

.macro storeIntRegMemArray8At(valueIndex,addr,offsetIndex) {
  .var srcReg = getIntRegAddress(valueIndex)
  .var ofsReg = getIntRegAddress(offsetIndex)

  clc
  lda addr + 0
  adc ofsReg + 0
  sta $fb + 0

  lda addr + 1
  adc ofsReg + 1
  sta $fb + 1

  ldy #0
  lda srcReg + 0
  sta ($fb),y
}

.macro poke(addrindex,valueIndex) {
  .var addr  = getIntRegAddress(addrindex)
  .var value = getIntRegAddress(valueIndex)

  clc
  lda addr + 0
  sta $fb + 0

  lda addr + 1
  sta $fb + 1

  ldy #0
  lda value + 0
  sta ($fb),y
}

.macro poke16(addrindex,valueIndex) {
  .var addr  = getIntRegAddress(addrindex)
  .var value = getIntRegAddress(valueIndex)

  clc
  lda addr + 0
  sta $fb + 0

  lda addr + 1
  sta $fb + 1

  ldy #0
  lda value + 0
  sta ($fb),y
  iny
  lda value + 1
  sta ($fb),y
}

.macro addIntReg(arg1,arg2,dst) {
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  clc
  lda srcReg1 + 0
  adc srcReg2 + 0
  sta dstReg  + 0

  lda srcReg1 + 1
  adc srcReg2 + 1
  sta dstReg  + 1
}

.macro subIntReg(arg1,arg2,dst) {
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  sec
  lda srcReg1 + 0
  sbc srcReg2 + 0
  sta dstReg  + 0

  lda srcReg1 + 1
  sbc srcReg2 + 1
  sta dstReg  + 1
}

.macro mulIntReg(arg1,arg2,dst) {
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

/*    sec
    lda srcReg1 + 0
    sbc srcReg2 + 0
    sta dstReg  + 0

    lda srcReg1 + 1
    sbc srcReg2 + 1
    sta dstReg  + 1*/
}

.macro divIntReg(arg1,arg2,dst) {
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

/*    sec
    lda srcReg1 + 0
    sbc srcReg2 + 0
    sta dstReg  + 0

    lda srcReg1 + 1
    sbc srcReg2 + 1
    sta dstReg  + 1*/
}

.macro andIntReg(arg1,arg2,dst) {
// dst = arg1 and arg2
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  lda srcReg1 + 0
  and srcReg2 + 0
  sta dstReg  + 0

  lda srcReg1 + 1
  and srcReg2 + 1
  sta dstReg  + 1
}

.macro orIntReg(arg1,arg2,dst) {
// dst = arg1 or arg2
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  lda srcReg1 + 0
  ora srcReg2 + 0
  sta dstReg  + 0

  lda srcReg1 + 1
  ora srcReg2 + 1
  sta dstReg  + 1
}

.macro xorIntReg(arg1,arg2,dst) {
// dst = arg1 xor arg2
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  lda srcReg1 + 0
  eor srcReg2 + 0
  sta dstReg  + 0

  lda srcReg1 + 1
  eor srcReg2 + 1
  sta dstReg  + 1
}

.macro notIntReg(arg1,dst) {
// dst = not arg1
  .var srcReg1 = getIntRegAddress(arg1)
  .var dstReg  = getIntRegAddress(dst)

  lda srcReg1 + 0
  eor #$ff
  sta dstReg  + 0

  lda srcReg1 + 1
  eor #$ff
  sta dstReg  + 1
}

.macro shlIntReg(arg1,arg2,dst) {
// dst = arg1 << arg2
  .var value   = getIntRegAddress(arg1)
  .var shift   = getIntRegAddress(arg2)
  .var result  = getIntRegAddress(dst)

  lda value  + 0
  sta result + 0
  lda value  + 1
  sta result + 1

  ldx shift  + 0
!:
  asl result + 0
  rol result + 1

  dex
  bne !-
}

.macro shrIntReg(arg1,arg2,dst) {
// dst = arg1 >> arg2
  .var value   = getIntRegAddress(arg1)
  .var shift   = getIntRegAddress(arg2)
  .var result  = getIntRegAddress(dst)

  lda value  + 0
  sta result + 0
  lda value  + 1
  sta result + 1

  ldx shift  + 0
!:
  :div2_16bit(result)

  dex
  bne !-
}

.macro cmpIntRegEql(arg1,arg2,dst) {
// dst = arg1 = arg2
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  lda srcReg1 + 0
  cmp srcReg2 + 0
  bne notEqual
  lda srcReg1 + 1
  cmp srcReg2 + 1
  bne notEqual

isEqual:
  lda #TRUE
  jmp storeBoolean
notEqual:
  lda #FALSE
storeBoolean:
  sta dstReg + 0
  sta dstReg + 1
}

.macro cmpIntRegNeq(arg1,arg2,dst) {
// dst <> arg1 = arg2
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  lda srcReg1 + 0
  cmp srcReg2 + 0
  bne notEqual
  lda srcReg1 + 1
  cmp srcReg2 + 1
  bne notEqual

isEqual:
  lda #FALSE
  jmp storeBoolean
notEqual:
  lda #TRUE
storeBoolean:
  sta dstReg + 0
  sta dstReg + 1
}

.macro cmpIntRegGtr(arg1,arg2,dst) {
// dst = arg1 > arg2
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  :subIntReg(arg1,arg2,dst)

  lda dstReg + 0
  bne notZero
  lda dstReg + 1
  bne notZero
  // is zero (equal) so store FALSE
  lda #FALSE
  jmp storeBoolean

notZero:
  // check for +ve msb
  lda dstReg + 1
  bpl isTrue

  lda #FALSE
  jmp storeBoolean
isTrue:
  lda #TRUE
storeBoolean:
  sta dstReg + 0
  sta dstReg + 1
}

.macro cmpIntRegGeq(arg1,arg2,dst) {
// dst = arg1 >= arg2
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  :subIntReg(arg1,arg2,dst)

  lda dstReg + 0
  bne notZero
  lda dstReg + 1
  bne notZero
  // is zero (equal) so store TRUE
  lda #TRUE
  jmp storeBoolean

notZero:
  // check for +ve msb
  lda dstReg + 1
  bpl isTrue

  lda #FALSE
  jmp storeBoolean
isTrue:
  lda #TRUE
storeBoolean:
  sta dstReg + 0
  sta dstReg + 1
}

.macro cmpIntRegLss(arg1,arg2,dst) {
// dst = arg1 < arg2
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  :subIntReg(arg1,arg2,dst)

  lda dstReg + 0
  bne notZero
  lda dstReg + 1
  bne notZero
  // is zero (equal) so store FALSE
  lda #FALSE
  jmp storeBoolean

notZero:
  // check for -ve msb
  lda dstReg + 1
  bmi isTrue

  lda #FALSE
  jmp storeBoolean
isTrue:
  lda #TRUE
storeBoolean:
  sta dstReg + 0
  sta dstReg + 1
}

.macro cmpIntRegLeq(arg1,arg2,dst) {
// dst = arg1 <= arg2
  .var srcReg1 = getIntRegAddress(arg1)
  .var srcReg2 = getIntRegAddress(arg2)
  .var dstReg  = getIntRegAddress(dst)

  :subIntReg(arg1,arg2,dst)

  lda dstReg + 0
  bne notZero
  lda dstReg + 1
  bne notZero
  // is zero (equal) so store TRUE
  lda #TRUE
  jmp storeBoolean

notZero:
  // check for -ve msb
  lda dstReg + 1
  bmi isTrue

  lda #FALSE
  jmp storeBoolean
isTrue:
  lda #TRUE
storeBoolean:
  sta dstReg + 0
  sta dstReg + 1
}

.macro intRegToBoolean(regIndex) {
  .var addr   = getIntRegAddress(regIndex)

  lda addr + 0
  bne isTrue
  lda addr + 1
  bne isTrue
isFalse:
  lda #FALSE
  jmp storeBoolean
isTrue:
  lda #TRUE
storeBoolean:
  sta addr + 0
  sta addr + 1
}

.macro jmpIntRegFalse(regIndex,falseAddress) {
  .var addr   = getIntRegAddress(regIndex)

  lda addr + 0
  bne isTrue
  lda addr + 1
  bne isTrue
isFalse:
  jmp falseAddress
isTrue:
}

.macro setEchoScreenAddress(screenAddr) {
.const screenPntr = $fb
  lda #<screenAddr
  sta screenPntr + 0
  lda #>screenAddr
  sta screenPntr + 1
}

.macro echoIntToHexAt(intAddr,x,screenAddr) {
  :setEchoScreenAddress(screenAddr)
  ldx #x
  jsr setcurx
  lda intAddr + 1
  jsr prhex2
  lda intAddr + 0
  jsr prhex2
}

//-------------------------------------------------------------------------
//  subroutine to set screen address xpos
//-------------------------------------------------------------------------
setcurx: {
  stx echo.xpos
  rts
}

//-------------------------------------------------------------------------
//  subroutine to print a screen code to the screen
//-------------------------------------------------------------------------
prchar: {
  jsr echo
  rts
}
//-------------------------------------------------------------------------
//  subroutine to print a 2 digit hexadecimal number in A
//-------------------------------------------------------------------------
prhex2: {
  pha
  lsr
  lsr
  lsr
  lsr
  // msd
  jsr prhex
  pla
  // fall through to prhex for lsd
}
//-------------------------------------------------------------------------
//  subroutine to print a hexadecimal digit
//-------------------------------------------------------------------------
prhex: {
.const ZERO         = 48
.const NINE_PLUSONE = ZERO + 10
  and #%00001111      //mask lsd for hex print
  ora #ZERO           //add zero
  cmp #NINE_PLUSONE   //is it a decimal digit?
  bcc echo            //yes! output it
  sbc #57             //add offset for letter a-f
                      //and fall through to print routine
}
//-------------------------------------------------------------------------
//  subroutine to print a character to the screen
//-------------------------------------------------------------------------
echo: {
.const screenPntr = $fb
  ldy xpos
  sta (screenPntr),y             //output character
  inc xpos                       //inc cursor pos
  rts
xpos: .byte 0
}