//----------------------------------------------------------
// example usage
// xa frame.asm -o frame.bin
// cartconv -t normal -i frame.bin -n 'my cart' -o frame.crt
// x64 -cartcrt frame.crt
//----------------------------------------------------------

//no load-adress for bin-file, so no header here

*=$8000

.word launcher //cold start
.word launcher //warm start
.byte $c3	//c
.byte $c2	//b
.byte $cd	//m
.byte $38	//8
.byte $30	//0

launcher:
  stx $d016
  jsr $fda3	//prepare irq
  jsr $fd50	//init memory
  jsr $fd15	//init i/o
//  jsr $ff5b //Init video

// instead of 'jsr $ff5b' use:

  ldx #$2f
!loop:
  lda vic_regs_data, x
  sta $d000, x
  dex
  bpl !loop-
  jsr $e51b
  cli

  ldx #$fb
  txs

//set up starting code outside of cartridge-area
move_starter:
  ldx #(starter_end-starter_start)
loop1:
  lda starter_start,x
  sta $100,x
  dex
  bpl loop1
  jmp $100
//---------------------------------

starter_start:
.pseudopc ($0100) {
  ldx #$40 //64 pages = 256 * 64 = 16384 Bytes
  ldy #0
loop:
src:
  lda exomized_data,y
dst:
  sta $801,y
  iny
  bne loop
  inc src+2-starter_start+$100
  inc dst+2-starter_start+$100
  dex
  bpl loop

//make sure settings for $01 and IRQ etc are correct for your code
//remember THIS table from AAY64:

//       Bit+-------------+-----------+------------+
//       210| $8000-$BFFF |$D000-$DFFF|$E000-$FFFF |
//  +---+---+-------------+-----------+------------+
//  | 7 |111| Cart.+Basic |    I/O    | Kernal ROM |
//  +---+---+-------------+-----------+------------+
//  | 6 |110|     RAM     |    I/O    | Kernal ROM |
//  +---+---+-------------+-----------+------------+
//  | 5 |101|     RAM     |    I/O    |    RAM     |
//  +---+---+-------------+-----------+------------+
//  | 4 |100|     RAM     |    RAM    |    RAM     |
//  +---+---+-------------+-----------+------------+
//  | 3 |011| Cart.+Basic | Char. ROM | Kernal ROM |
//  +---+---+-------------+-----------+------------+
//  | 2 |010|     RAM     | Char. ROM | Kernal ROM |
//  +---+---+-------------+-----------+------------+
//  | 1 |001|     RAM     | Char. ROM |    RAM     |
//  +---+---+-------------+-----------+------------+
//  | 0 |000|     RAM     |    RAM    |    RAM     |
//  +---+---+-------------+-----------+------------+

  sei
  lda #$35 //cart is always on instead of BASIC unless it can be switched off via software
  sta $01
  cli
  jmp $80d //for exomizer, i.e.
}

starter_end:

vic_regs_data:
.fill 16,0 // set x,y of all sprites to 0
.byte $00, $1b, $00, $00, $00, $00, $c8, $00, $15, $71, $f0, $00, $00, $00, $00, $00
//border colors 0,0 and then the spritecolors (just 4 bit anyway).
.byte $00, $00, $f1,$f2,$f3,$f4,$f0,$f1,$f2,$f3,$f4,$f5,$f6,$f7,$fc,$ff

//----------------------------------
exomized_data:
.var file = LoadBinary("<EXOMIZED DATA>")

.fill file.getSize(), file.get(i)

//syntax for exomizer 2.0.1:
//exomizer sfx sys game.prg -o data.exo
main_file_end:
//fill up full $4000 bytes for bin file ($c000-$8000=$4000)
.fill ($c000-main_file_end),0