//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit bbc_vdu;

import 'bbc_vdu.asm';

procedure vdu_cursorOff; assembler;
asm
  lda #23; jsr OSWRCH
  lda #1;  jsr OSWRCH
  lda #0;  jsr OSWRCH
  lda #0;  jsr OSWRCH
  lda #0;  jsr OSWRCH
  lda #0;  jsr OSWRCH
  lda #0;  jsr OSWRCH
  lda #0;  jsr OSWRCH
  lda #0;  jsr OSWRCH
  lda #0;  jsr OSWRCH
end;

macro vdu_gcol(mode,color); assembler;
asm
  lda #18;    jsr OSWRCH
  lda #mode;  jsr OSWRCH
  lda #color; jsr OSWRCH
end;