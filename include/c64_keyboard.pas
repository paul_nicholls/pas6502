unit c64_keyboard;

const
  VK_NO_KEY            = 64;
  VK_INSERT_DELETE     =  0;
  VK_RETURN            =  1;
  VK_CURSOR_LEFT_RIGHT =  2;
  VK_F7_F8             =  3;
  VK_F1_F2             =  4;
  VK_F3_F4             =  5;
  VK_F5_F6             =  6;
  VK_CURSOR_UP_DOWN    =  7;
  VK_0                 =  35;
  VK_1                 =  56;
  VK_2                 =  59;
  VK_3                 =  8;
  VK_4                 =  11;
  VK_5                 =  16;
  VK_6                 =  19;
  VK_7                 =  24;
  VK_8                 =  27;
  VK_9                 =  32;
  VK_A                 =  10;
  VK_B                 =  28;
  VK_C                 =  20;
  VK_D                 =  18;
  VK_E                 =  14;
  VK_F                 =  21;
  VK_G                 =  26;
  VK_H                 =  29;
  VK_I                 =  33;
  VK_J                 =  34;
  VK_K                 =  37;
  VK_L                 =  42;
  VK_M                 =  36;
  VK_N                 =  39;
  VK_O                 =  38;
  VK_P                 =  41;
  VK_Q                 =  62;
  VK_R                 =  17;
  VK_S                 =  13;
  VK_T                 =  22;
  VK_U                 =  30;
  VK_V                 =  31;
  VK_W                 =  9;
  VK_X                 =  23;
  VK_Y                 =  25;
  VK_Z                 =  12;
  VK_ASTERISK          =  49;
  VK_AT                =  46;
  VK_CLEAR_HOME        =  51;
  VK_COLON             =  45;
  VK_COMMA             =  47;
  VK_COMMODORE         =  61;
  VK_CONTROL           =  58;
  VK_EQUAL             =  53;
  VK_LEFT_ARROW        =  57;
  VK_LEFT_SHIFT        =  15;
  VK_MINUS             =  43;
  VK_PERIOD            =  44;
  VK_PLUS              =  40;
  VK_POUND             =  48;
  VK_RIGHT_SHIFT       =  52;
  VK_RUN_STOP          =  63;
  VK_SEMICOLON         =  50;
  VK_SLASH             =  55;
  VK_SPACE             =  60;
  VK_UP_ARROW          =  54;

var
  _lastKey_ : Byte = VK_NO_KEY;
  
procedure waitForAnyKey;
begin
  while CURRKEY = VK_NO_KEY do;
end;

procedure waitForKey(a : Byte);
begin
  while CURRKEY <> a do; 
end;

{function  getKey : Byte;
begin
    result := CURRKEY;
    
    while _lastKey_ = result do begin
      _lastKey_ := result;
      result := CURRKEY;
    end;
end;}

function  getKey : Byte; assembler;
// copied from ROM
asm
  lda $c6
  beq noChar
  sei
  jmp getChar
noChar:
  clc
  rts

// get character from keyboard buffer
getChar:
  ldy $0277
  ldx #$00
loop:
  lda $0278,x
  sta $0277,x
  inx
  cpx $c6
  bne loop
  dec $c6
  tya
  cli
  clc
  rts
end;

