#importonce

.macro invokeStackBegin(dstAddress) {
  // pull return address off stack and store
  :pullParam8(dstAddress + 0)
  :pullParam8(dstAddress + 1)
}

.macro invokeStackEnd(srcAddress) {
  // push return address onto stack
  :pushParam8(srcAddress + 1)
  :pushParam8(srcAddress + 0)
}

.macro pushParam8im(value) {
  lda #value
  pha
}

.macro pushParam8(srcAddress) {
  lda srcAddress
  pha
}

.macro pushParam16im(value) {
  lda #<value
  pha
  lda #>value
  pha
}

.macro pullParam8(dstAddress) {
  pla
  sta dstAddress
}

.macro pullParam8im() {
  pla
}

.macro pullParam16(dstAddress) {
  pla
  sta dstAddress + 1
  pla
  sta dstAddress + 0
}