#importonce

#define RAND16LFSR

random: {
  lda _rand8
  lsr
#if RAND16LFSR
  rol _rand16      // this command is only used if _rand16 has been defined
#endif
  bcc noeor
  eor #$B4
noeor:
  sta _rand8
#if RAND16LFSR
  eor _rand16      // this command is only used if _rand16 has been defined
#endif
  rts
}

  _rand8:  .byte 0;
#if RAND16LFSR
  _rand16: .byte 0;
#endif
