//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit random;

import 'random.asm';

type
  TRND = record
    rand8  : Byte;
    rand16 : Byte;
  end;
  
  PRND = ^TRND;

procedure rnd_init(rnd : PRND; seed : Byte);// assembler;
begin
  rnd^.rand8  := seed;
  rnd^.rand16 := seed xor $ff;
end;
{asm
  ldy #0
  lda seed
  sta (rnd),y
  eor #$ff
  iny
  sta (rnd),y
end;}

procedure rnd_init2(rnd : Pointer; seed1,seed2 : Byte); assembler;
asm
  ldy #0
  lda seed1
  sta (rnd),y
  iny
  ora seed2
  bne !+ // both seed1 & seed2 <> 0
  lda seed1
  eor #$ff
  sta (rnd),y // rand16 = seed1 xor true
  rts
!:
  lda seed2
  sta (rnd),y // rand16 = seed2
end;

function  rnd_rand(rnd : Pointer) : Byte; assembler;
var
  rand8:  byte;
  rand16: byte;
asm
  ldy #0
  lda (rnd),y
  sta rand8
  iny
  lda (rnd),y
  sta rand16

  lda rand8
  lsr
  rol rand16
  bcc noeor
  eor #$B4
noeor:
  sta rand8
  eor rand16
  
  pha
  
  ldy #0
  lda rand8
  sta (rnd),y
  iny
  lda rand16
  sta (rnd),y

  pla
end;

function  rnd_d6(rnd : Word) : Byte;
const
  LO_VAL = 1;
  HI_VAL = 6;
  DELTA  = HI_VAL - LO_VAL + 1;
begin
//result = loRange + random# * (hiRange - loRange + 1) / 256
  Result := rnd_rand(rnd);
  Result := Result * DELTA / 256;
  Result := Result + LO_VAL;
end;

function  rnd_d20(rnd : Word) : Byte;
const
  LO_VAL = 1;
  HI_VAL = 20;
  DELTA  = HI_VAL - LO_VAL + 1;
begin
//result = loRange + random# * (hiRange - loRange + 1) / 256
  Result := rnd_rand(rnd);
  Result := Result * DELTA / 256;
  Result := Result + LO_VAL;
end;

function  rnd_randRange(rnd : Word; range : Byte) : Byte;
var
  delta : Byte;
begin
//result = random# * range / 256
  delta  := range + 1;
  Result := rnd_rand(rnd);
  Result := Result * delta / 256;
end;

function  rnd_randRange2(rnd : Word; loRange,hiRange : Byte) : Byte;
var
  delta : Byte;
begin
//result = loRange + random# * (hiRange - loRange + 1) / 256
  delta  := (hiRange - loRange) + 1;
  Result := rnd_rand(rnd);
  Result := Result * delta / 256;
  Result := Result + loRange;
end;

procedure randInit(a : Byte); assembler;
asm
// input: init = 8-bit seed for random number generator
  sta _rand8
#if RAND16LFSR
  eor #$ff
  sta _rand16
#endif
end;

function  rand : Byte; assembler;
asm
// gets a random 8-bit value and stores it in result
  jsr random
end;

function  randRange(range: Byte) : Byte; assembler;
// returns a random number in the range [0..range-1]
asm
  // result = random# * range / 256
  //
  // load x with range
  ldx range

  // load a with random number (0-255) for scaling
  jsr random
  jsr mul_8x8 // multiply a * x

  // load hi byte of product (same as / 256)
  lda mul_product_hi
end;

function  randRange2(loRange,hiRange : Byte) : Byte; assembler;
// returns a random number in the range [loRange..hiRange]
asm
  // result = loRange + random# * (hiRange - loRange + 1) / 256
  //
  // load x with hiRange - loRange
  lda hiRange
  sec
  sbc loRange
  clc
  adc #1
  tax

  // load a with random number (0-255) for scaling
  jsr random
  jsr mul_8x8 // multiply a * x

  // load hi byte of product (same as / 256) and add loRange to it as result
  lda mul_product_hi
  clc
  adc loRange
end;



