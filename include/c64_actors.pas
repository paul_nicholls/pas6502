//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit c64_actors;

const 
  MAX_ACTORS       = 8;
  SCREEN_ORIGIN_X  = 24;
  SCREEN_ORIGIN_Y  = 50;

var
  actor_enabled     : array[0..MAX_ACTORS - 1] of Byte;
  
  // actor x pos information
  actor_xfrac       : array[0..MAX_ACTORS - 1] of Byte;
  actor_xlsb        : array[0..MAX_ACTORS - 1] of Byte;
  actor_xmsb        : array[0..MAX_ACTORS - 1] of Byte;
  
  // actor y pos information
  actor_yfrac       : array[0..MAX_ACTORS - 1] of Byte;
  actor_ylsb        : array[0..MAX_ACTORS - 1] of Byte;
  actor_ymsb        : array[0..MAX_ACTORS - 1] of Byte;
  
  // actor xvel pos information
  actor_xvelfrac    : array[0..MAX_ACTORS - 1] of Byte;
  actor_xvellsb     : array[0..MAX_ACTORS - 1] of Byte;
  actor_xvelmsb     : array[0..MAX_ACTORS - 1] of Byte;
  
  // actor yvel pos information
  actor_yvelfrac    : array[0..MAX_ACTORS - 1] of Byte;
  actor_yvellsb     : array[0..MAX_ACTORS - 1] of Byte;
  actor_yvelmsb     : array[0..MAX_ACTORS - 1] of Byte;
  
  actor_multicolor  : array[0..MAX_ACTORS - 1] of Byte;
  actor_color       : array[0..MAX_ACTORS - 1] of Byte;
  
  // actor frame information
  actor_frame       : array[0..MAX_ACTORS - 1] of Byte;
  
  actor_offset_x    : Byte;
  actor_offset_y    : Byte;

procedure actors_setOriginOffset(x,y : Byte);
// sets the actors x,y origin offset from 0,0
// defaults to top/left of the visible screen
begin
  actor_offset_x := x;
  actor_offset_y := y;
end; 

procedure actors_initSpritePointers(screenAddr : Word);
// sets where the screen the sprite pointers live at.
// defaults to $0400
begin
  asm
    clc
    lda screenAddr.b0; adc #<$03f8; sta copyActorsToHardware.screen + 1
    lda screenAddr.b1; adc #>$03f8; sta copyActorsToHardware.screen + 2
  end;
end;

procedure actors_setSpritePointer0offset(a : Byte); assembler;
// offset to first sprite pointer in the active bank.
// defaults to 0
asm
  sta copyActorsToHardware.shape + 1
end;

procedure actors_init;
var
  i : Byte;
begin
  for i := 0 to MAX_ACTORS - 1 do begin
    actor_enabled[i]    := 0;
    actor_frame[i]      := 0;
    actor_multicolor[i] := 0;
    actor_xlsb[i]       := 0;
    actor_xmsb[i]       := 0;
    actor_ylsb[i]       := 255;
    actor_ymsb[i]       := 255;
  end;
  
  poke($d015,0);

  actor_offset_x := SCREEN_ORIGIN_X;
  actor_offset_y := SCREEN_ORIGIN_Y;
end;

procedure actor_setX(s: Byte; x : word);
begin
  actor_xlsb[s] := x.b0;
  actor_xmsb[s] := x.b1;
end;

procedure actor_setY(s: Byte; y : word);
begin
  actor_ylsb[s] := y.b0;
  actor_ymsb[s] := y.b1;
end;

function  actor_getX(s: Byte): word;
begin
  result.b0 := actor_xlsb[s];
  result.b1 := actor_xmsb[s];
end;

function  actor_getY(s: Byte) : word;
begin
  result.b0 := actor_ylsb[s];
  result.b1 := actor_ymsb[s];
end;

//----------------------------------------------------------------------------------
//                                    copyActorsToHardware
// copies the actor's data to the hardware VIC registers
// (includes the player sprite (always sprite0))
//----------------------------------------------------------------------------------

procedure copyActorsToHardware(); assembler;

asm
  // do sprites 7 downto 0
  ldx #$07 // actor index
  ldy #$0e // sprite register index

spriteLoop:
  lda actor_ylsb,x
  clc
  adc actor_offset_y
  sta $d001,y		            //write y

  lda actor_xlsb,x
  clc
  adc actor_offset_x
  sta $d000,y		            //write x

  lda actor_xmsb,x
  adc #0
  ror                       // rotate lsb into carry flag
  rol $d010		              // rotate carry flag into $d010 (sprite msb bit enabled), repeat 8 times and all bits are set

  lda actor_enabled,x
  ror                       // rotate lsb into carry flag
  rol $d015		    // rotate carry flag into $d015 (sprite enabled), repeat 8 times and all bits are set

  lda actor_multicolor,x
  ror                       // rotate lsb into carry flag
  rol $d01c		    // rotate carry flag into $d01c (sprite multicolor enabled), repeat 8 times and all bits are set

  // colors
  lda actor_color,x
  sta $d027,x

  // sprite pointers
  lda actor_frame,x
  clc
shape:
  adc #0//#SPRITE_SHAPE_POINTER0
screen:
  sta $0400+$03f8,x//SCREEN0_ADDRESS + $03f8,x

  // decrement y 2x as sprite x/y are staggered
  dey
  dey
  dex
  // not wrapped around to 255 yet, jump back to loop
  bpl spriteLoop
end;
