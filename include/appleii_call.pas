//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit appleii_call;
// adapted from here:
// https://www.appleoldies.ca/azgraphics33/call33.c.txt

procedure catalog(); assembler;
asm
  clc
  jsr $a56e
end;

procedure reboot(); assembler;
asm
  clc
  jsr $faa6
end;

procedure bell(); assembler;
asm
  clc
  jsr $ff3a
end;

procedure bellerr(); assembler;
asm
  clc
  jsr $ff2d
end;

procedure scr_apple(); assembler;
asm
  clc
  jsr $fb60
end;

procedure loclear(); assembler;
asm
  clc
  jsr $f832
end;

procedure crt80(); assembler;
asm
  clc
  jsr $c300
end;
