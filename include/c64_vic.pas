//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here :
//    https ://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    this file was translated from : https ://github.com/KarolS/millfork
//
//    Pas6502 is free software : you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https ://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit c64_vic;

//import 'c64_vic.asm';

type
  TVIC = record
    spr0_x      : Byte;
    spr0_y      : Byte;
    spr1_x      : Byte;
    spr1_y      : Byte;
    spr2_x      : Byte;
    spr2_y      : Byte;
    spr3_x      : Byte;
    spr3_y      : Byte;
    spr4_x      : Byte;
    spr4_y      : Byte;
    spr5_x      : Byte;
    spr5_y      : Byte;
    spr6_x      : Byte;
    spr6_y      : Byte;
    spr7_x      : Byte;
    spr7_y      : Byte;
    spr_hi_x    : Byte;
    cr1         : Byte;
    raster      : Byte;
    lp_x        : Byte;
    lp_y        : Byte;
    spr_ena     : Byte;
    cr2         : Byte;
    spr_exp_y   : Byte;
    mem         : Byte;
    irq         : Byte;
    irq_ena     : Byte;
    spr_dp      : Byte;
    spr_mcolor  : Byte;
    spr_exp_x   : Byte;
    spr_ss_col  : Byte;
    spr_sd_col  : Byte;
    border      : Byte;
    bg_color0   : Byte;
    bg_color1   : Byte;
    bg_color2   : Byte;
    bg_color3   : Byte;
    spr_color1  : Byte;
    spr_color2  : Byte;
    spr0_color  : Byte;
    spr1_color  : Byte;
    spr2_color  : Byte;
    spr3_color  : Byte;
    spr4_color  : Byte;
    spr5_color  : Byte;
    spr6_color  : Byte;
    spr7_color  : Byte;
  end;

var
  vic : TVIC absolute $d000;

//   VIC-II
/// $D011 Control Register #1
/// $D011 Control Register #1 Bit#7: RST8 9th Bit for $D012 Rasterline counter
const VICII_RST8 = %10000000;
/// $D011 Control Register #1 Bit#6: ECM Turn Extended Color Mode on/off
const VICII_ECM =  %01000000;
/// $D011 Control Register #1  Bit#5: BMM Turn Bitmap Mode on/off
const VICII_BMM =  %00100000;
/// $D011 Control Register #1  Bit#4: DEN Switch VIC-II output on/off
const VICII_DEN =  %00010000;
/// $D011 Control Register #1  Bit#3: RSEL Switch betweem 25 or 24 visible rows
///          RSEL|  Display window height   | First line  | Last line
///          ----+--------------------------+-------------+----------
///            0 | 24 text lines/192 pixels |   55 ($37)  | 246 ($f6)
///            1 | 25 text lines/200 pixels |   51 ($33)  | 250 ($fa)
const VICII_RSEL = %00001000;

var
//   vic_spr0_x      : byte absolute $D000;
//   vic_spr0_y      : byte absolute $D001;
//   vic_spr1_x      : byte absolute $D002;
//   vic_spr1_y      : byte absolute $D003;
//   vic_spr2_x      : byte absolute $D004;
//   vic_spr2_y      : byte absolute $D005;
//   vic_spr3_x      : byte absolute $D006;
//   vic_spr3_y      : byte absolute $D007;
//   vic_spr4_x      : byte absolute $D008;
//   vic_spr4_y      : byte absolute $D009;
//   vic_spr5_x      : byte absolute $D00A;
//   vic_spr5_y      : byte absolute $D00B;
//   vic_spr6_x      : byte absolute $D00C;
//   vic_spr6_y      : byte absolute $D00D;
//   vic_spr7_x      : byte absolute $D00E;
//   vic_spr7_y      : byte absolute $D00F;
//   vic_spr_hi_x    : byte absolute $D010;
//   vic_cr1         : byte absolute $D011;
//   vic_raster      : byte absolute $D012;
//   vic_lp_x        : byte absolute $D013;
//   vic_lp_y        : byte absolute $D014;
//   vic_spr_ena     : byte absolute $D015;
//   vic_cr2         : byte absolute $D016;
//   vic_spr_exp_y   : byte absolute $D017;
//   vic_mem         : byte absolute $D018;
//   vic_irq         : byte absolute $D019;
//   vic_irq_ena     : byte absolute $D01A;
//   vic_spr_dp      : byte absolute $D01B;
//   vic_spr_mcolor  : byte absolute $D01C;
//   vic_spr_exp_x   : byte absolute $D01D;
//   vic_spr_ss_col  : byte absolute $D01E;
//   vic_spr_sd_col  : byte absolute $D01F;
//   vic_border      : byte absolute $D020;
//   vic_bg_color0   : byte absolute $D021;
//   vic_bg_color1   : byte absolute $D022;
//   vic_bg_color2   : byte absolute $D023;
//   vic_bg_color3   : byte absolute $D024;
//   vic_chr_mcolor1 : byte absolute $D022;
//   vic_chr_mcolor2 : byte absolute $D023;
//   vic_spr_color1  : byte absolute $D025;
//   vic_spr_color2  : byte absolute $D026;
//   vic_spr0_color  : byte absolute $D027;
//   vic_spr1_color  : byte absolute $D028;
//   vic_spr2_color  : byte absolute $D029;
//   vic_spr3_color  : byte absolute $D02A;
//   vic_spr4_color  : byte absolute $D02B;
//   vic_spr5_color  : byte absolute $D02C;
//   vic_spr6_color  : byte absolute $D02D;
//   vic_spr7_color  : byte absolute $D02E;

  // other way of accessing sprite coord/color; array
  // array of 8 x (x,y) pairs
  vic_spr_coord   : byte absolute $D000;
  // array of 8
  vic_spr_color   : byte absolute $D027;

  vic_color_ram   : byte absolute $d800;

const
  black       = 0;
  white       = 1;
  red         = 2;
  cyan        = 3;
  purple      = 4;
  green       = 5;
  blue        = 6;
  yellow      = 7;
  orange      = 8;
  brown       = 9;
  light_red   = 10;
  dark_grey   = 11;
  dark_gray   = 11;
  medium_grey = 12;
  medium_gray = 12;
  light_green = 13;
  light_blue  = 14;
  light_grey  = 15;
  light_gray  = 15;

procedure vic_set_scroll(xscroll,yscroll  : Byte); assembler;
// this routine sets the hardware x and y scroll values
// from xscroll & yscroll respectively
asm
  lda vic.cr1
  and #$f8
  ora yscroll
  sta vic.cr1
  lda vic.cr2
  and #$f8
  ora xscroll
  sta vic.cr2
end;

procedure vic_set_scrollx(xscroll  : Byte); assembler;
// this routine sets the hardware x scroll value
// from xscroll
asm
  lda vic.cr2
  and #$f8
  ora xscroll
  sta vic.cr2
end;

procedure vic_set_scrolly(yscroll  : Byte); assembler;
// this routine sets the hardware y scroll value
// from yscroll
asm
  lda vic.cr1
  and #$f8
  ora yscroll
  sta vic.cr1
end;

procedure vic_waitForRaster(a : Byte); assembler;
asm
!:
  cmp vic.raster
  bne !- // a < raster
end;

procedure vic_fillScreen(screen : Pointer; value : Byte);
var
  i : Word;
begin
  i := 0;
  while i < 1000 do begin
    screen^[0] := value;
    inc(screen);
    inc(i);
  end;
end;

macro vic_screenOff;
begin
  vic.cr1 := vic.cr1 or %01100000;
end;

macro vic_screenOn;
begin
  vic.cr1 := vic.cr1 and %10011111;
end;

macro vic_clearScreen(_screen, clearByte); assembler;
asm
	lda #clearByte
	ldx #0
!loop:
	sta _screen + $000, x
	sta _screen + $100, x
	sta _screen + $200, x
	sta _screen + $300, x
	inx
	bne !loop-
end;

macro vic_38_columns(); assembler;
asm
  // enable 38-column mode
  lda $d016
  and #%11110111
  sta $d016
  //
end;

macro vic_40_columns(); assembler;
asm
  // enable 40-column mode
  lda $d016
  ora #%00001000
  sta $d016
  //
end;

macro vic_24_rows(); assembler;
asm
  // enable 24-row mode
  lda $D011
  and #%11110111
  sta $D011
  //
end;

macro vic_25_rows(); assembler;
asm
  // enable 24-row mode
  lda $D011
  ora #%00001000
  sta $D011
  //
end;

macro vic_setVideoBank(_VideoBank); assembler;
{
Address	Mask	Bank	Yields
DD00	%xxxxxx11	bank0	0000 - 3FFF
DD00	%xxxxxx10	bank1	4000 - 7FFF
DD00	%xxxxxx01	bank2	8000 - BFFF
DD00	%xxxxxx00	bank3	C000 - FFFF
}
asm
  lda $DD00
  and #%11111100
  ora #((3 - _VideoBank) & %00000011)
  sta $DD00
end;
                    
//
// Enter hires bitmap mode (a.k.a. standard bitmap mode)
//
macro vic_setHiresBitmapMode(); assembler;
asm
  //
  // Clear extended color mode (bit 6) and set bitmap mode (bit 5)
  //
  lda $d011
  and #%10111111
  ora #%00100000
  sta $d011

  //
  // Clear multi color mode (bit 4)
  //
  lda $d016
  and #%11101111
  sta $d016
end;

macro vic_setScreenMemory(address); assembler;
asm
  //
  // The most significant nibble of $D018 selects where the screen is
  // located in the current VIC-II bank.
  .var bits = (address / $0400) << 4

  lda $d018
  and #%00001111
  ora #bits
  sta $d018
end;

macro vic_setScreenAndCharsetLocation(_screen,_charset); assembler;
{
Address	Mask	Bank	Yields
DD00	%xxxxxx11	bank0	0000 - 3FFF
DD00	%xxxxxx10	bank1	4000 - 7FFF
DD00	%xxxxxx01	bank2	8000 - BFFF
DD00	%xxxxxx00	bank3	C000 - FFFF
}
asm
  .var videoBank = 0;
  .var _setVideoBank = true;

  .var VIDEO_BANK_0_START = $0000
  .var VIDEO_BANK_0_END   = $3FFF

  .var VIDEO_BANK_1_START = $4000
  .var VIDEO_BANK_1_END   = $7FFF

  .var VIDEO_BANK_2_START = $8000
  .var VIDEO_BANK_2_END   = $BFFF

  .var VIDEO_BANK_3_START = $C000
  .var VIDEO_BANK_3_END   = $FFFF

  .if ((_screen >= VIDEO_BANK_0_START) && (_screen < VIDEO_BANK_0_END)) .eval videoBank = 0;
  .if ((_screen >= VIDEO_BANK_1_START) && (_screen < VIDEO_BANK_1_END)) .eval videoBank = 1;
  .if ((_screen >= VIDEO_BANK_2_START) && (_screen < VIDEO_BANK_2_END)) .eval videoBank = 2;
  .if ((_screen >= VIDEO_BANK_3_START) && (_screen < VIDEO_BANK_3_END)) .eval videoBank = 3;

  .if ((_charset >= VIDEO_BANK_0_START) && (_charset < VIDEO_BANK_0_END)) .eval videoBank = 0;
  .if ((_charset >= VIDEO_BANK_1_START) && (_charset < VIDEO_BANK_1_END)) .eval videoBank = 1;
  .if ((_charset >= VIDEO_BANK_2_START) && (_charset < VIDEO_BANK_2_END)) .eval videoBank = 2;
  .if ((_charset >= VIDEO_BANK_3_START) && (_charset < VIDEO_BANK_3_END)) .eval videoBank = 3;

  .if (_setVideoBank)
    :vic_setVideoBank(videoBank)

  // set the screen and character set locations
  // (relative to the current bank)
  lda #[[_screen & $3FFF] / 64] | [[_charset & $3FFF] / 1024]
  sta $D018
end;

