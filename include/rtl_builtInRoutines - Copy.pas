//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit rtl_builtInRoutines;

procedure writeb(addr : Pointer; value : Byte); assembler; volatile;
asm
  ldy #0
  lda value
  sta (addr),y
end;

function  readb(addr : Pointer) : Byte; assembler; volatile;
asm
  ldy #0
  lda (addr),y
end;

function neg8(a : Byte) : Byte; assembler; 
// a := 0 - a
asm
  eor #$ff
  clc
  adc #1
end;

function neg16(ax : Word) : Word; assembler; 
// ax := 0 - ax
asm
  tay
  sec
  lda #0
  sbcx // A - X
  tax
  lda #0
  sbcy // A - Y
end;

function mul8x8s(a,b : Byte) : Word; 
var
  s : Byte;
begin
  asm
    lda a
    eor b
    sta s

    lda a
    bpl !+
    jsr neg8
    sta a
    !:
    lda b
    bpl !+
    jsr neg8
    sta b
    !:
  end;

  Result := a*b;

  asm
    lda s
    bpl !+
    ldx result
    lda result + 1
    jsr neg16
    stx result
    sta result + 1
    !:
  end;
end;

function mul_8x16s(a : Byte; b : Word) : Int24; 
var
  sign : Byte;
  temp : Int24;
begin
  sign := a xor b.b1;
  asm
    lda a
    eor b+1
    sta sign
    lda a
    bpl !+
    jsr neg8
    sta a
    !:
    lda b
    bpl !+
    tax
    lda b+1
    jsr neg16
    sta b+1
    stx b
    !:
  end;

  result      := a*b.b0;
  temp.b0     := 0;
  temp.hiword := a*b.b1;

  result := result + temp;

  asm
    lda sign
    bpl !+
    sec
    lda #0
    sbc result
    sta result

    lda #0
    sbc result+1
    sta result+1

    lda #0
    sbc result+2
    sta result+2
    !:
  end;
end;

function mul_8ux16s(a : Byte; b : Word) : Int24; 
var
  sign : Byte;
  temp : Int24;
begin
  sign := b.b1;
  asm
    lda b
    bpl !+
    tax
    lda b+1
    jsr neg16
    sta b+1
    stx b
    !:
  end;

  result      := a*b.b0;
  temp.b0     := 0;
  temp.hiword := a*b.b1;

  result := result + temp;

  asm
    lda sign
    bpl !+
    sec
    lda #0
    sbc result
    sta result

    lda #0
    sbc result+1
    sta result+1

    lda #0
    sbc result+2
    sta result+2
    !:
  end;
end;

function _signExtendAtoAX(a : Byte) : Word; assembler; 
asm
  tax
// calculates upper byte of sign-extension of A
  bmi !neg+
  lda #$00
  jmp !+
!neg:
  lda #$ff
!:
  // msb now in a reg
end;

function abs8(a : Byte) : Byte; assembler; 
asm
  bmi !neg+
  rts
!neg:
  eor #$ff
  clc
  adc #1
end;

function abs16(ax : word) : word; assembler; 
asm
  bmi !neg+
  rts
!neg:
  tay
  sec
  lda #0
  sbcx // A - X
  tax
  lda #0
  sbcy // A - Y
end;

procedure _printMinus; assembler; 
asm
  pha
  lda #'-'
  jsr $ffd2 // print char
  pla
end;

procedure _printSignedA(a : Byte); assembler; 
asm
  bpl !+
  // display '-'
  jsr _printMinus
  // do absolute of number
  jsr abs8
!:
  // display variable
  tax
  lda #0
  jsr $bdcd // PRINTWORD');
end;

procedure _printSignedAX(ax : Word); assembler; 
asm
  bpl !+
  // display '-'
  jsr _printMinus
  // do absolute of number
  jsr abs16
!:
  // display variable
  jsr $bdcd // PRINTWORD');
end;

function _addDelta8ToWord(y : Byte; ax : Word) : Word; assembler; 
// add signed 8bit delta to word
var
  delta : Byte;
  value : Word;
asm
  sty delta
  stx value + 0
  sta value + 1
  // Precalculate the sign-extended high byte in .X
  ldx #$00
  lda delta
  bpl !+
  dex        // decrement high byte to $ff for a negative delta
!:

  // Normal 16-bit addition
  clc
  adc value + 0   // .A still holds delta
  sta value + 0
  txa         // .X is the high byte
  adc value + 1
  sta value + 1
  ldx value + 0
end;

function _div8_8u : Byte; assembler; 
// input:  dividend in A, divisor in X
// output: result in A, if divisor = 0, result = 0
//
var
  dividend, divisor : Byte;
asm
  cpx #0
  bne !+
  // return 0 as result if divisor = 0
  lda #0
  rts
  !:
  sta dividend
  stx divisor
// 8bit/8bit division
// by White Flame
// https://codebase64.org/doku.php?id=base:8bit_divide_8bit_product
//
// Input: dividend, divisor
// Output: dividend = quotient, .A = remainder
 
 lda #$00
 ldx #$07
 clc
!:
  rol dividend
  rol
  cmp divisor
  bcc !+
  sbc divisor
!:
 dex
 bpl !--
 rol dividend
 
 lda dividend
end;

function _mod8_8u : Byte; assembler; 
// input:  dividend in A, divisor in X
// output: remainder in A, if divisor = 0, result = 0
//
var
  dividend, divisor : Byte;
asm
  cpx #0
  bne !+
  // return 0 as result if divisor = 0
  lda #0
  rts
  !:
  sta dividend
  stx divisor
// 8bit/8bit division
// by White Flame
// https://codebase64.org/doku.php?id=base:8bit_divide_8bit_product
//
// Input: dividend, divisor
// Output: dividend = quotient, .A = remainder

 lda #$00
 ldx #$07
 clc
!:
  rol dividend
  rol
  cmp divisor
  bcc !+
  sbc divisor
!:
 dex
 bpl !--
 rol dividend
end;

function divmod_s16(dividend,divisor : Integer; isMod : Boolean) : Integer; assembler; 
//result = dividend ;save memory by reusing dividend to store the result
var
  remainder : Integer;
  s         : Byte; // XOR sign of dividend and divisor
  d         : Byte; // sign of dividend
asm
  // XOR signs of operands and store
  lda divisor+1
  eor dividend+1
  sta s
  // save sign of dividend
  lda dividend+1
  sta d
  // abs(dividend)
  ldx dividend
  lda dividend+1
  bpl !+
  jsr neg16
  stx dividend
  sta dividend+1
   !:
  // abs(divisor)
  ldx divisor
  lda divisor+1
  bpl !+
  jsr neg16
  stx divisor
  sta divisor+1
   !:
divide:
  lda #0	          //preset remainder to 0
	sta remainder
	sta remainder+1
	ldx #16	          //repeat for each bit: ...

divloop:	
  asl dividend	    //dividend lb & hb*2, msb -> Carry
	rol dividend+1
	rol remainder	    //remainder lb & hb * 2 + msb from carry
	rol remainder+1
	lda remainder
	sec
	sbc divisor	      //substract divisor to see if it fits in
	tay	              //lb result -> Y, for we may need it later
	lda remainder+1
	sbc divisor+1
	bcc skip	        //if carry=0 then divisor didn't fit in yet

	sta remainder+1	  //else save substraction result as new remainder,
	sty remainder
	inc dividend      //and INCrement result cause divisor fit in 1 times

skip:
	dex
	bne divloop
	
	lda isMod
	beq isDiv         //false, so return div result
	// return mod result
	lda d
	bmi negateModResult // match sign of original dividend sign
	// return positive mod result
	ldx remainder
	lda remainder+1
	rts
isDiv:
  lda s
  bpl !+
  ldx dividend
  lda dividend+1
  jsr neg16
!:
  rts
negateModResult:
  ldx remainder
  lda remainder+1
  jsr neg16
  rts
end;

function divmod_u16(dividend,divisor : Integer; isMod : Boolean) : Integer; assembler;
//result = dividend ;save memory by reusing dividend to store the result
var
  remainder : Integer;
asm
divide:
  lda #0	          //preset remainder to 0
	sta remainder
	sta remainder+1
	ldx #16	          //repeat for each bit: ...

divloop:	
  asl dividend	    //dividend lb & hb*2, msb -> Carry
	rol dividend+1
	rol remainder	    //remainder lb & hb * 2 + msb from carry
	rol remainder+1
	lda remainder
	sec
	sbc divisor	      //substract divisor to see if it fits in
	tay	              //lb result -> Y, for we may need it later
	lda remainder+1
	sbc divisor+1
	bcc skip	        //if carry=0 then divisor didn't fit in yet

	sta remainder+1	  //else save substraction result as new remainder,
	sty remainder
	inc dividend      //and INCrement result cause divisor fit in 1 times

skip:
	dex
	bne divloop
	
	lda isMod
	beq isDiv         //false, so return div result
	// return mod result
	ldx remainder
	lda remainder+1
	rts
isDiv:
  ldx dividend
  lda dividend+1
  rts
end;