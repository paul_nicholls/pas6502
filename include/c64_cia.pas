//------------------------------------------------------------------------
//    This file is part of Pas6502.
//
//    You can find Pas6502 here:
//    https://bitbucket.org/paul_nicholls/pas6502/src/master/
//
//    this file was translated from: https://github.com/KarolS/millfork
//
//    Pas6502 is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    Pas6502 is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with Pas6502.  If not, see <https://www.gnu.org/licenses/>.
//------------------------------------------------------------------------

unit c64_cia;

//   CIA1
var
  cia1_pra       : byte absolute $DC00;
  cia1_prb       : byte absolute $DC01;
  cia1_ddra      : byte absolute $DC02;
  cia1_ddrb      : byte absolute $DC03;
  cia2_pra       : byte absolute $DD00;
  cia2_prb       : byte absolute $DD01;
  cia2_ddra      : byte absolute $DD02;
  cia2_ddrb      : byte absolute $DD03;
  
procedure cia_disable_irq(); assembler;
asm
  lda #$7f
  sta $dc0d
  sta $dd0d
  sta $dc0d
  sta $dd0d
end;

